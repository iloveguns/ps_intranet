<?php

defined('YII_DEBUG') or define('YII_DEBUG', true);

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../config/console.php');

// запуск консольного приложения через http запрос
// /console.php?r=user/test
if( isset($_GET['r']) and !empty($_GET['r'])) {
    $_SERVER['argv'] = [ 0 => __FILE__, 1 => $_GET['r'] ];
    $_SERVER['argc'] = 2;
}

$application = new yii\console\Application($config);
$exitCode = $application->run();
exit($exitCode);