var mapTag = 'draggable-map';
var mapid = 'canvas';

/*
 * сохранить html
 */
$('#savemap').click(function(){
    //сохранить связи
    var connections = [];

    $.each(instance.getConnections(), function (idx, connection) {
        connections.push({
        connectionId: connection.id,
        pageSourceId: connection.sourceId,
        pageTargetId: connection.targetId,
        anchors: $.map(connection.endpoints, function(endpoint) {

          return [[endpoint.anchor.x, 
          endpoint.anchor.y, 
          endpoint.anchor.orientation[0], 
          endpoint.anchor.orientation[1],
          endpoint.anchor.offsets[0],
          endpoint.anchor.offsets[1]]];

        })
      });
    });
    
    localStorage.setItem(mapTag, JSON.stringify(connections));
    
    instance.detachEveryConnection();
    
    //html
    window.htmlmap = $('#draggable-map #canvas').html();
    $('#'+mapTag+' #canvas').html('');
});

/*
 * вернуть json соединений блоков
 */
function prepareJson(){
    var connections = [];

    $.each(instance.getConnections(), function (idx, connection) {
        connections.push({
        connectionId: connection.id,
        pageSourceId: connection.sourceId,
        pageTargetId: connection.targetId,
        anchors: $.map(connection.endpoints, function(endpoint) {

          return [[endpoint.anchor.x, 
          endpoint.anchor.y, 
          endpoint.anchor.orientation[0], 
          endpoint.anchor.orientation[1],
          endpoint.anchor.offsets[0],
          endpoint.anchor.offsets[1]]];

        })
      });
    });
    
    instance.detachEveryConnection();//удалить, чтоб в html не попали связи
    
    return JSON.stringify(connections);
}

/*
 * вернуть html блоков
 */
function prepareHtml(){
    return $('#'+mapTag+' #canvas').html();
}

/*
 * загрузить html
 */
$('#loadmap').click(function(){loadDataToMap(window.htmlmap, window.htmlmap);});

function loadDataToMap(html, connections){
    //загрузить html
    $('#'+mapTag+' #canvas').html(html);
    startplumb();
    
    //связи    
    $.each(connections, function( index, elem ) {
        
        var connection1 = instance.connect({
            source: elem.pageSourceId,
            target: elem.pageTargetId,
        });
    });
}

/*
 * восстановить данные если есть
 */
function loadDataMap(){
    var html = localStorage.getItem(mapTag);
    return JSON.parse(html);
}

/*
 * получить последний ид из списка существующих блоков
 */
function getLastId(){
    var id = $('.jsplumb-draggable.smallWindow:last').attr('data-id');
    if(!id) id = 1;
    return id;
}

function startplumb() {
    //хуй пойми
    var sourceAnchors = [
        [ 0, 1, 0, 1 ],
        [ 0.25, 1, 0, 1 ],
        [ 0.5, 1, 0, 1 ],
        [ 0.75, 1, 0, 1 ],
        [ 1, 1, 0, 1 ]
    ];

    var instance = window.instance = jsPlumb.getInstance({
        DragOptions: { cursor: "pointer", zIndex: 2000 },
        PaintStyle: {
            gradient: { stops: [
                [ 0, "#0d78bc" ],
                [ 1, "#558822" ]
            ] },
            strokeStyle: "#558822",
            lineWidth: 10
        },
        Container: mapid
    });

    instance.bind("connection", function (i, c) {
        if (typeof console !== "undefined")
            console.log("connection", i.connection);
    });

    var smallWindows = jsPlumb.getSelector(".smallWindow");
    instance.draggable(smallWindows);

    instance.batch(function () {    
        $(".batching").each(function(i){
            instance.makeSource($(this).attr("id"), {
                filter:"a",
                filterExclude:true,
                maxConnections: -1,
                endpoint:[ "Dot", { radius: 7, cssClass:"small-blue" } ],
                anchor:sourceAnchors
            });
        });
        
        instance.makeTarget(smallWindows, {
            dropOptions: { hoverClass: "hover" },
            anchor:"Top",
            endpoint:[ "Dot", { radius: 11, cssClass:"large-green" } ]
        });
         
        /*instance.connect({ source: "sourceWindow1", target: "targetWindow5" });
        instance.connect({ source: "sourceWindow1", target: "targetWindow2" });*/
    });
}

startplumb();
