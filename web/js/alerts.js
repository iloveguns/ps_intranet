/*
 * вывод алертов бутстрапных через js
 */
function bootstrapAlert(type,msg){
    let alert = '<div class="alert alert-'+type+'">'
                +'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
                +msg
                +'</div>'
    $('.content').prepend(alert);
    $('body').scrollTop(0);//вверх подняться
};


