function handleAjaxLink(e) {
    e.preventDefault();
    let
        $link = $(e.target),
        callUrl = $link.attr('href'),
        formId = $link.data('formId'),
        onDone = $link.data('onDone'),
        onFail = $link.data('onFail'),
        onAlways = $link.data('onAlways'),
        ajaxRequest;
    ajaxRequest = $.ajax({
        type: "post",
        dataType: 'json',
        url: callUrl,
        data: (typeof formId === "string" ? $('#' + formId).serializeArray() : null)
    });
    // успешное выполнение
    if (typeof onDone === "string" && ajaxCallbacks.hasOwnProperty(onDone)) {
        ajaxRequest.done(ajaxCallbacks[onDone]);
    }
    // выполнение с ошибкой
    if (typeof onFail === "string" && ajaxCallbacks.hasOwnProperty(onFail)) {
        ajaxRequest.fail(ajaxCallbacks[onFail]);
    }
    // всегда после выполнения
    if (typeof onAlways === "string" && ajaxCallbacks.hasOwnProperty(onAlways)) {
        ajaxRequest.always(ajaxCallbacks[onAlways]);
    }
};
/*
 * простой колбак, который принимает true-false
 * переменная объявлена здесь, в остальных случаях просто добавлять функцию ajaxCallbacks.NAMEFUNCTION = function(response){}
 */
let ajaxCallbacks = {
    'simpeResponse': function (response) {
        console.log(response);
    },
};