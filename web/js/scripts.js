/* global tasks_m_late */

/*
 * правила именования глобальных переменных
 * @module_@type_@varname
 */

/*
 * удаление уведомлений ajax
 */
$('#notices-delete').click(() => {
    if(!confirm('Действительно удалить?')) return false;
    
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: $('#notices-delete').attr('href'),
        success: function(data) {
            if(data.success === true) {//очистить все
                Tinycon.setBubble(0);
                $('#notificationPlace .label').html('');
                $('#notificationPlace .dropdown-menu .menu').html('');
                $('#notices-delete').hide();
                jsNotify(data.message);
            }
        },
        complete: function() {
            $('#notificationPlace').removeClass('open');
        }
    });
    
    return false;
});

/**
 * получение get параметров
 * 
 * @param {String} name - название параметра
 */
function getParamFromUrl(name){
   if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
      return decodeURIComponent(name[1]);
}

/*
 * запустить гиф лоадер на весь экран
 */
function gifLoader(){
    $('.center-over-all').toggleClass('hidden');
};
$('.long-load').click(function(){
    gifLoader();
});

//функции не от меня--
function placeCaretAtEnd(el) {
    el.focus();
    if (typeof window.getSelection != "undefined" && typeof document.createRange != "undefined") {
        var range = document.createRange();
        range.selectNodeContents(el);
        range.collapse(false);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    } else if (typeof document.body.createTextRange != "undefined") {
        var textRange = document.body.createTextRange();
        textRange.moveToElementText(el);
        textRange.collapse(false);
        textRange.select();
    }
};

//аналог php
function strip_tags(str){
    return str.replace(/<\/?[^>]+>/gi, '');
};

//удалить определенный тег
var removeTags = function(text, selector) {
    var wrapped = $("<div>" + text + "</div>");
    wrapped.find(selector).remove();
    return wrapped.html();
};
/*
 * смена url без перезагрузки(диалоги)
 * 1 параметр только, ниже функция норм
 * 
 * @param {type} url
 * @param {type} param
 * @param {type} value
 * @param {type} history_push
 * @returns {String}
 */
function ChangeUrl(title, url, param, val) {
    if (typeof (history.pushState) != "undefined") {
        var obj = { Title: title, Url: url };
        if(param !== undefined && val !== undefined) {//параметры get
            history.pushState(obj, obj.Title, obj.Url+'?'+param+'='+val);
        } else {
            history.pushState(obj, obj.Title, obj.Url);
        }
    } else {
        alert("Browser does not support HTML5.");
    }
};

/**
 * изменение get параметров без изменения урл
 * 
 * @param {String} url              - url страницы
 * @param {String} param            - параметр для вставки/изменения
 * @param {String} value            - значение для вставки/изменения
 * @param {Boolean} history_push    - изменить ли url без перезагрузки
 */
function changeUrl(url, param, value, history_push) {
    if(url.length === 0) {
        url = window.location.href;
    }
    
    var changed_url = '';
    var val = new RegExp('(\\?|\\&)' + param + '=.*?(?=(&|$))'),
        qstring = /\?.+$/;
    
    if (val.test(url)) {
        changed_url = url.replace(val, '$1' + param + '=' + value);
    } else if (qstring.test(url)) {
        changed_url = url + '&' + param + '=' + value;
    } else {
        changed_url = url + '?' + param + '=' + value;
    }
    
    if(history_push === true) {
        history.pushState('', '', changed_url);//простенько
    }
    return changed_url;
}

//2 функции для получения массива дат между датами
Date.prototype.addDays = function(days) {
    var dat = new Date(this.valueOf())
    dat.setDate(dat.getDate() + days);
    return dat;
};
function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
      dateArray.push(currentDate)
      currentDate = currentDate.addDays(1);
    }
    return dateArray;
};

function formatDate(date) {//вернуть дату в Y-m-d
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
};

function formatDateFull(date) {//вернуть дату в Y-m-d
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hour = '' + d.getHours(),
        minutes = '' + d.getMinutes(),
        seconds = '' + d.getSeconds();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if (hour.length < 2) hour = '0' + hour;
    if (minutes.length < 2) minutes = '0' + minutes;
    if (seconds.length < 2) seconds = '0' + seconds;

    return [year, month, day].join('-') + ' ' + [hour, minutes, seconds].join(':');
};

//1 буква заглавная
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
};

function textAreaAdjust(o) {
    o.style.height = "1px";
    o.style.height = (5+o.scrollHeight)+"px";
}
//end функции не от меня--

//скрытие collapsible виджетов
$('.btn-box-tool[data-widget=collapse]').click(function(){
    if($(this).parent().parent().parent().hasClass('collapsed-box')){
        $.removeCookie($(this).attr('data-id-widg'));
    } else {
        $.cookie($(this).attr('data-id-widg'), 1, { expires: 365 });
    }
});

/*
 * уведомление основа
 */
function jsNotify(message, title, urlstrig, type, target){
    if(!type) type = 'success';
    if(title === undefined || title.length < 1) title = 'Новое уведомление';
    if(!target) target = '_blank';
    
    $.notify({
        title: '<strong>'+title+'</strong><br/>',
        message: message,
        url: urlstrig,
        animate: {
            enter: 'animated bounceIn',
            exit: 'animated bounceOut'
        }
    },{
        type: type,
        offset : 70,
        delay : 2000,
        z_index: 9999, // должен быть самым большим, выше всех
        placement : {align : 'center'},
        target : target,
    });
}

/*
 * вывод ошибок моделей клиенту
 * @param {Object} errors = getErrors()
 * @returns {jsNotify}
 */
function modelErrorShow(errors) {
    let t = '';
    $.each(errors, function(i, value) {
        t += value + '<br>';
    });
    console.error(t);
    jsNotify(t,'','','danger');
}

/*
 * скроллить див в конец, если есть прокрутка(сообщения)
 */
function scrollDivToBottom(elem, latency){
    if(latency) setTimeout(function() { elem.animate({ scrollTop: elem[0].scrollHeight+500}, 0); }, latency);
    elem.animate({ scrollTop: elem[0].scrollHeight+500}, 0);
};

/*
 * скролл до указанного елемента
 * @param {Jquery element id} elem
 */
function scrollToElementId(elem, latency){
    if(!latency) latency = 0;
    $('html, body').animate({
        scrollTop: elem.offset().top-60
    }, latency);
    //$(document).scrollTop(elem.offset().top-60);
}

/*
 * подсказки для просмотра инфы сотрудника
 */
function setCluetips(){
    $('a[data-cluetip=true]').each(function(event) {
        $(this).click(function() {
            return false; //disables the the link from redirecting
        });

        $(this).qtip({
            show: 'click',
            hide: 'unfocus',
            content: {
                text: function(event, api) {
                    $.ajax({
                        url: api.elements.target.attr('href')
                    })
                    .then(function(content) {
                        api.set('content.text', content);
                    }, function(xhr, status, error) {
                        api.set('content.text', status + ': ' + error);
                    });

                    return 'Loading...';
                }
            },
            position: {
                viewport: $('html'),
                my : 'top left',
                at: 'bottom right'
            },
            style: 'qtip-bootstrap'
        });
    });
}

//кнопка наверх
$(document).ready(function() {
    $('a[href="#totop"]').hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 150) $('a[href="#totop"]').fadeIn();
        else $('a[href="#totop"]').fadeOut();
    });
    $('a[href="#totop"]').click(function () {
        $('body, html').animate({
            scrollTop: 0
        }, 1000);
    });
    setCluetips();
    
    loadListTask();//загрузка задач
    
    initTinyMce();
});

/*
 * вызов списка сотрудников
 * toggle {bool} - вызов открытия/закрытия окна
 */
function getStructure(toggle = true){
    if(window.haveStructure === undefined){//window.haveStructure - метка того, были ли получены данные о структуре
        $.ajax({
            url: getstructureurl,
            type: 'post',
            success: function (data) {
                window.haveStructure = true;
                $(".modal-header").append('<h1>'+msgchoosestructureunits+'</h1>');
                $(".modal-body").html(data);
                if(toggle){
                   $('#myModal').modal('toggle');
                }
            }
        });
    }
    else{
        $('#myModal').modal('toggle');
    }
    return false;
}

/*
 * навесить просмотрщик фото для картинок и запустить(обернуть в ссылку для colorbox)
 * после удаления в комментариях, не используется нигде
 * @param {string} images       - селектор поиска(корневой элемент)
 * @param {string} classPhoto   - класс, который будет задан всем ссылкам
 */
function prepareAndSetColorbox(images, classPhoto) {
    if(!classPhoto) classPhoto = 'group-undef';
    let classForLink = 'colorbox-photo-msg ' + classPhoto,
        tag_img = document.querySelectorAll(images + ' img:not([colorbox="true"])');
    
    for(let i = 0; i < tag_img.length; i++) {
        tag_img[i].setAttribute('colorbox', 'true');
        tag_img[i].outerHTML = `<a href="${tag_img[i].getAttribute('src')}" class="${classForLink}">${tag_img[i].outerHTML}</a>`;
    }
    
    $('.' + classPhoto).colorbox({rel: classPhoto, transition: 'fade', maxHeight: '100%'});
}

/*
 * сделать качественный ассоциативный массив для отправки в php
 * множественные значения не передает(заменяет последним)
 */
$.fn.serializeAssoc = function() {
    var model = {};
    
    $.each( $(this).serializeArray(), ( k, v ) => {
        let m = v.name.match(/\[(.*?)\]/);
        if(m) {
            model[m[1]] = v.value;
        } else {
            model[v.name] = v.value;
        }
    });
    
    return model;
}

/*множественные значения таблиц*/
/*
 * клик на кнопку добавления строки
 */
$('.multiple-data div[name=clone-multiple]').click(function(){
    cloneMultiple($(this).attr('id'));
});

/*
 * клик на кнопку удаления строки
 */
function setRemove(){
    $('.remove-clone-multiple').click(function(){
        $(this).parent().parent().remove();
    });
}

/*
 * функция добавления строки через временное хранилище
 * @param {int} id - ид таблицы
 */
function cloneMultiple(id){
    $('body').append('<div class="hidden" id="temp-stack"></div>');
    
    let multiplemid = $('#' + id +' .multiple-data [data-mid]').length,
        temp = $('#temp-stack'),//место, где производится операция
        countr = $('#' + id +' .multiple-data .ctr').length;//кол-во атрибутов
    
    
    temp.append($('#' + id +' .multiple-data [data-mid]').clone());
    temp.children().first().addClass('prepared-multiple');
    $('.prepared-multiple').attr('data-mid', ++multiplemid);
    
    //произвести очистку
    for(let i = 1; i <= countr; i++){
        let id = $('.prepared-multiple .val'+i+'class').attr('id'),
            name = $('.prepared-multiple .val'+i+'class').attr('name');

        $('.prepared-multiple .val'+i+'class').attr('id', id.substr(0, id.length-1) + multiplemid);
        $('.prepared-multiple .val'+i+'class').attr('name', name.substr(0, name.length-2) + multiplemid+']');
        $('.prepared-multiple .val'+i+'class').val('');
    }
    $('#' + id +' .multiple-data').append($('.prepared-multiple').clone());
    $('.prepared-multiple').removeClass('prepared-multiple');
    temp.html('');

    $('#temp-stack').remove();
    setRemove();
}
/*end множественные значения таблиц*/


/*
 * действие после загрузки файла, api, убрано из сокета
 */
function loadfile(item_class, item_id, type, exclude_users) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        data: {
            item_class,
            item_id,
            type,
            exclude_users
        },
        url: window.apiurl + 'api/notification_all_send'
    });
}

/*задачи ajax*/
/*
 * получение информации с сервера
 */
function loadListTask() {
    if(typeof tasks_api_list === 'undefined') return false;//может и не быть
    //после загрузки попросить данные для списка задач
    $.ajax({
        type: 'get',
        dataType: 'json',
        data: {sort: $.cookie('widget-task-sort')},
        url: tasks_api_list,
        success: (data) => {
            $('#tasks-list ul').html('');
            if(data.success === true) {
                tasksToHtml(data.tasks);
                refreshCounters();
            } else {
                $.each( data.errors, ( key, error ) => {
                    $('#tasks-list ul').append('<li class="item">' + error + '</li>');
                });
            }
        }
    });
}

/*
 * обновить инфу о типах задач
 * переменные можно и удалить тут же
 */
function refreshCounters(late_c, unlimited_c, endsoon_c, current_c){
    $('#task-unlimited').html(unlimited_c);
    $('#task-late').html(late_c);
    $('#task-endsoon').html(endsoon_c);
    $('#task-current').html(current_c);
}

/*
 * вставка информации задач везде, куда нужно
 * @returns {html}
 */
function tasksToHtml(tasks) {
    //счетчики типов задач
    let late_c = 0,
        unlimited_c = 0,
        endsoon_c = 0,
        current_c = 0;

    $('.tasks-all-count').html(tasks.length);//кол-во задач общее
    
    for(let i = 0; i < tasks.length; i++) {
        let html = '',
            task = tasks[i],
            addClass = 'current',//изначально - текущие
            afterHtml = '',
            classLink = '';

        if(task.is_author) {//автор задачи
            html += '<i class="fa fa-hand-paper-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Вы автор"></i>';
        }

        if(task.is_head) {//ответственный задачи
            html += '<i class="fa fa-star" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Вы ответственный"></i>';
        }

        if(task.is_watcher) {//наблюдатель задачи
            html += '<i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Вы наблюдатель"></i>';
        } else {//участник задачи
            html += '<i class="fa fa-male" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Вы участник"></i>';
        }

        if(task.is_ends_soon) {//скоро закончится
            endsoon_c++;
            classLink = 'text-yellow';
            addClass = 'endsoon';
            html += '<i class="fa fa-exclamation-triangle text-yellow" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Скоро закончится"></i>';
        }

        if(task.is_unlimited) {//задача без конечного времени
            unlimited_c++;
            classLink = 'text-green';
            addClass = 'unlimited';
        } else {
            afterHtml += ' (' + task.date_start + ' - ' + task.date_end + ')';
        }

        if(task.is_late) {//просрочена задача
            late_c++;
            addClass = 'late';
            classLink = 'text-red';
            afterHtml += ' <span class="label label-danger">' + tasks_m_late + ' c ' + task.date_end + '</span>';
        }

        if(!task.is_late && !task.is_ends_soon && !task.is_unlimited) {//обычная задача(текущая)
            current_c++;
        }

        html += ' <a href=' + task.url + ' title="' + task.title + '" class="' + classLink + '">' + task.title + '<span class="label label-info pull-right">' + task.count_comments + '</span></a>';

        //вставка в виджет задач
        $('#tasks-list ul').append( '<li class="item ' + addClass + '" id="widget-task-id-' + task.pk + '">' + html + afterHtml + '</li>' );
        
        $('#header-list-tasks ul.menu').append( '<li><a href=' + task.url + ' title="' + task.title + '" class="' + classLink + '">' + task.title + '<span class="label label-info pull-right">' + task.count_comments + '</span></a></li>' );
        
        refreshCounters(late_c, unlimited_c, endsoon_c, current_c);
    }
}
/*end задачи ajax*/

/**
 * инициализация tinymce
 */
function initTinyMce() {
    tinymce.init({
        selector: 'textarea[tinymce=tinymce]',
        menubar: false,
        skin: false,
        statusbar: false,
        language: 'ru',
        plugins: 'link image code autoresize paste media lists autolink',
        toolbar: 'bold italic | link image media | bullist numlist | removeformat | code',
        autoresize_bottom_margin: 0,
        autoresize_max_height: 500,
        autoresize_min_height: $('textarea[tinymce=tinymce]').attr('autoresize_min_height') || 60,
        remove_script_host: true,//хост не важен, картинки лежат на локале
        relative_urls: false,
        
        // видео
        media_alt_source: false,
        media_poster: false,
        /** video_template_callback: function(data) { // custom template video tag
            return '<video width="' + data.width + '" height="' + data.height + '"' + (data.poster ? ' poster="' + data.poster + '"' : '') + ' controls="controls">\n' + '<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + (data.source2 ? '<source src="' + data.source2 + '"' + (data.source2mime ? ' type="' + data.source2mime + '"' : '') + ' />\n' : '') + '</video>';
        },*/
        
        setup: function (editor) { //не хочет сохранять иначе
            editor.on('change', function () {
                editor.save();
            });
        },
        file_picker_callback: function (callback, value, meta) {
            if (meta.filetype === 'image') { // загрузка и вставка изображений
                let input = document.getElementById('tinymce-file-upload-all');
                input.click();
                input.onchange = () => {
                    //загрузить на файл сервер и отдать ссылку
                    let file = input.files[0];
                    let data = new FormData();
                    data.append('file', file);

                    $.ajax({
                        method: 'POST',
                        url: apiurl + 'api/upload',
                        data: data,
                        contentType: false,
                        processData: false,
                        beforeSend: function() {
                            gifLoader();
                        },
                        success: function(data) {
                            gifLoader();
                            
                            if(data.success === true) {
                                callback(data.files[0].path, {
                                    alt: file.name
                                });
                            } else {
                                callback('Невозможно загрузить изображение', {
                                    alt: 'Невозможно загрузить изображение'
                                });
                            }
                        }
                    });
                };
            } else if (meta.filetype === 'media') { // загрузка и вставка видео
                let input = document.getElementById('tinymce-media-upload-all');
                input.click();
                input.onchange = () => {
                    //загрузить на файл сервер и отдать ссылку
                    let file = input.files[0];
                    let data = new FormData();
                    data.append('file', file);

                    $.ajax({
                        method: 'POST',
                        url: apiurl + 'api/upload',
                        data: data,
                        contentType: false,
                        processData: false, 
                        xhr: function() { // отслеживать процент загрузки
                            var xhr = new window.XMLHttpRequest();
                            var percentComplete = 0;
                            
                            xhr.upload.addEventListener( "progress", function ( evt ) {
                            if ( evt.lengthComputable ) {
                                    percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);
                                    console.log(percentComplete);
                                }
                            }, false );
                            return xhr;
                        },
                        beforeSend: function() {
                            gifLoader();
                        },
                        success: function(data) {
                            gifLoader();
                            
                            if(data.success === true) {
                                callback(data.files[0].path, {
                                    alt: file.name
                                });
                            } else {
                                callback('Невозможно загрузить видеофайл', {
                                    alt: 'Невозможно загрузить видеофайл'
                                });
                            }
                        }
                    });
                };
            }
        }
    });
}