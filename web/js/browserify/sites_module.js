/* global home_url_cookie, axios, sel_site, API_URL, tinymce, tinyMCE, sitesmodule_id, PUBLIC_API_URL, sitesModule, cropper */

/**
 * нужен разнос по разным файлам, подмодулям
 */
require('./sites_module_requires/sitesModule');
require('./sites_module_requires/tags');
require('./sites_module_requires/cropper');
require('./sites_module_requires/socketio');

let content; // для плагина
let contentPage = {
    tags: [],
    head_img_src: '',
    text: '',
    title: '',
    pk_content: 0
}; // все здесь
const get_content_limit = 20;
let countall, countstatus1, countstatus2; // общее кол-во контента(при первой загрузке)
let pagination;
let updating_gallery = []; // для сравнение обновления галереи
let widget_lc_data = {}; // 

window.contentPage = contentPage;
window.publishLaterTime = publishLaterTime;
window.loadStories = loadStories;
window.registerLoadContent = registerLoadContent;

//заголовок для доступа к апи
$.ajaxSetup({
    headers: {'auth_id': sitesmodule_id},
    error(xhr) {
        if(xhr.status === 0) { // сервер не отвечает
            console.error('Нет соединения с сервером');
            alert('Нет соединения с сервером');
        }
        // другие статусы
    }
});

// показывать загрузку
$( document ).ajaxSend(function( event, request, settings ) {
    gifLoader();
});
$( document ).ajaxComplete(function( event, request, settings ) {
    gifLoader();
});

$(document).ready(() => {
    /**
     * получить данные о сайтах и вставить
     */
    $.ajax({
        dataType: "json",
        url: `${API_URL}/sites`,
        success(response) {
            for(let i = 0; i < response.length; i ++) {
                let option = document.createElement("option");
                option.text = response[i].name_site;
                option.value = response[i].pk_site;

                if(sel_site.id === parseInt(option.value)) {
                    sel_site.url_site = response[i].url_site;
                    option.selected = 'selected';
                }
                document.getElementById("selected_site").add(option);
            }
            
            //если ни один не выбран
            if(!sel_site.id) {
                sel_site.id = parseInt(document.getElementById('selected_site').value);
            }
            
            sitesModule.afterLoad();
        }
    });
    
    initTinyMceSites();
    
    pagination = parseInt(getParamFromUrl('pagination'), 10) || 0;
    
    const st = parseInt(getParamFromUrl('status'), 10) || 0;
    $(`.btn[data-status=${st}]`).removeClass('btn-link');
    $(`.btn[data-status=${st}]`).addClass('btn-primary');
    
    // cropper
    $("#headimg_crop").on("shown.bs.modal", function() { // после открытия модального окна
        cropper.start($('.file-selected').attr('data-id'));
    });
    
    $('#headimg_crop').on('hidden.bs.modal', function () {
        cropper.destroy();
    });
    // end cropper
});

/**
 * смена сайта
 */
$('#selected_site').change(function(){
    window.location.href = home_url_cookie + '=' + $(this).val();
});

// загрузка сюжетов
function loadStories() {
    $.ajax({
        dataType: "json",
        data: {
            fk_site: sel_site.id
        },
        method: "GET",
        url: `${API_URL}/content_stories`,
        success({data}) {
            for(let i = 0; i < data.length; i++) {
                $('.pk_content_stories_list .panel-body').append(`<div class="checkbox"><label><input data-id="${data[i].pk_content_story}" type="checkbox"> ${data[i].title_content_story}</label></div>`);
            }
        }
    });
};

/**
 * создание тегов
 */
$('#btn-create-tags').click(function(){
    let val = $.trim($('#name_tag').val());
    
    if(val.length === 0) {
        $('#name_tag').parent().addClass('has-error');
    } else {
        $.ajax({
            dataType: "json",
            data: {
                tags: val.split(';'),
                fk_site: sel_site.id
            },
            method: "POST",
            url: `${API_URL}/tags`,
            success(response) {
                $('#name_tag').val('');
                
                for(let i = 0; i < response.length; i ++) {
                    tags.addRowTagsList(response[i], true);
                }
            }
        });
    }
    
    return false;
});

/**
 * сохранение сюжетов
 */
if(document.getElementById('save_content_story')) {
    document.getElementById('save_content_story').addEventListener("click", function() {
        let title_content_story = document.getElementById("title_content_story").value;

        if(title_content_story.length < 2) return;

        $.ajax({
            dataType: "json",
            data: {
                content_story: {title_content_story},
                fk_site: sel_site.id
            },
            method: "POST",
            url: `${API_URL}/content_story`,
            success(response) {
                if(response.success) {
                    jsNotify('Успешно сохранено');
                    setTimeout(function(){ location.reload() }, 1500);
                }
            }
        });
    });
}

/**
 * валидация, сохранение контента
 */
$('#content_publish').click(function(){
    let validate = {}, hasErrors = 0;
    let status = document.getElementById("status_content");
    let author = document.getElementById("author_content");
    let type_content = document.getElementById("type_content");
    let rubric = document.getElementById("content_material_rubric");
    
    //сбор данных
    contentPage.title               = $.trim(document.getElementById('title_content').value);
    contentPage.seo_title_content   = $.trim(document.getElementById('seo_title_content').value);
    contentPage.text                = tinymce.get('text_content').getContent().replace(/(\r\n|\n|\r)/gm,"");
    contentPage.intro               = document.getElementById('intro_content').value;
    contentPage.headimglabel_content = document.getElementById('headimglabel_content').value;
    contentPage.head_img_src        = document.getElementById('head_img_src').children[0].getAttribute('src');
    contentPage.status              = parseInt(status.options[status.selectedIndex].value, 10);
    contentPage.type_material       = parseInt(type_content.options[type_content.selectedIndex].value, 10);
    contentPage.fk_user_created     = parseInt(author.options[author.selectedIndex].value, 10);//window.uniqid;
    contentPage.later_publish_time  = $('#laterPublishTime').val();
    // можно проверять, есть ли уже избранная и уведомлять о перезаписи
    contentPage.is_chosen           = (document.getElementById('content_is_chosen').checked) ? 1 : 0;
    contentPage.rubric              = parseInt(rubric.options[rubric.selectedIndex].value, 10);
    contentPage.exclude_rss_yandex  = (document.getElementById('exclude_rss_yandex').checked) ? 1 : 0;
    contentPage.caption_content     = document.getElementById('caption_content').value;
    contentPage.content_stories     = [];
    
    // выбранные сюжеты
    let content_stories_checkboxes = $('.pk_content_stories_list .panel-body input[type=checkbox]:checked');
    for(let i = 0; i < content_stories_checkboxes.length; i++) {
        contentPage.content_stories.push(Number(content_stories_checkboxes[i].getAttribute('data-id')));
    }
        
    // валидация отложенной даты публикации
    if(contentPage.status === 3 && contentPage.later_publish_time.length < 5) {
        $('#laterPublishTime').parent().addClass('has-error');
        validate.later_publish_time = false;
    } else {
        $('#laterPublishTime').parent().removeClass('has-error');
        validate.later_publish_time = true;
    }
    
    // валидация заголовка
    if(contentPage.title.length < 1) {
        $('#title_content').parent().addClass('has-error');
        validate.title = false;
    } else {
        $('#title_content').parent().removeClass('has-error');
        validate.title = true;
    }
    
    // валидация seo заголовка
    if(contentPage.seo_title_content.length < 1) {
        $('#seo_title_content').parent().addClass('has-error');
        validate.seo_title_content = false;
    } else {
        $('#seo_title_content').parent().removeClass('has-error');
        validate.seo_title_content = true;
    }
    
    // валидация содержимого
    if(contentPage.text.length < 1) {
        $('#text_content').parent().addClass('has-error');
        validate.text = false;
    } else {
        $('#text_content').parent().removeClass('has-error');
        validate.text = true;
    }
    
    // валидация краткого содержимого (нужно для meta description)
    if(contentPage.intro.length < 1) {
        $('#intro_content').parent().addClass('has-error');
        validate.intro = false;
    } else {
        $('#intro_content').parent().removeClass('has-error');
        validate.intro = true;
    }
    
    for (let val in validate) {
        if(validate[val] === false) {
            ++hasErrors;
        }
    }
    
    if(hasErrors === 0) { // можно сохранять        
        $.ajax({
            dataType: "json",
            data: {
                content: contentPage,
                fk_site: sel_site.id
            },
            method: "POST",
            url: `${API_URL}/content`,
            success(response) {
                if(response.success) {
                    jsNotify('Успешно сохранено');
                }
            }
        });
    }
});

/**
 * загрузить контент по ид
 */
function registerLoadContent() {
    $.ajax({
        dataType: "json",
        data: {
            pk_content: getParamFromUrl('pk_content'),
            fk_site: sel_site.id
        },
        method: "GET",
        url: `${API_URL}/contentone`,
        success({data}) {
            if(!isEmpty(data)) {
                //заполнить данные
                contentPage.pk_content = data.pk_content;

                document.getElementById('title_content').value = data.title_content;

                $('#status_content').val(data.status_content).trigger("change");
                
                $('#author_content').val(data.fk_user_created).trigger("change");
                
                $('#laterPublishTime').val(data.publish_date);
                
                document.getElementById('headimglabel_content').value = data.headimglabel_content;
                
                document.getElementById('content_is_chosen').checked = data.is_chosen;
                
                document.getElementById('exclude_rss_yandex').checked = data.exclude_rss_yandex;
                
                document.getElementById('caption_content').value = data.caption_content;
                
                $('#content_material_rubric').val(data.fk_material_rubric).trigger("change");
                
                if(data.seo_title_content) {
                    document.getElementById('seo_title_content').value = data.seo_title_content;
                }

                //если есть изображение
                if(data.headimgsrc_content && data.headimgsrc_content.length) {
                    contentPage.head_img_src = data.headimgsrc_content;
                    $('#head_img_src>img').attr('src', contentPage.head_img_src);
                }

                //теги
                for(let i = 0; i < data.tags.length; i ++) {
                    contentPage.tags.push({
                        id: data.tags[i].id,
                        label: data.tags[i].label
                    });
                }
                
                // сюжеты
                for(let i = 0; i < data.content_stories.length; i ++) {
                    document.querySelector(`.pk_content_stories_list input[data-id="${data.content_stories[i].id}"]`).setAttribute('checked', true);
                }

                $('#tags_content_add_tags').click();
                //end теги
                
                //tinymce.get('intro_content').insertContent($('#title_content').html(data.intro_content).text());
                document.getElementById('intro_content').value = $('#title_content').html(data.intro_content).text();

                //проблемы с шорткодом, решение не изящное
                let texttiny = $('#title_content').html(data.text_content).text();
                
                // старые эксперименты
                /**if(texttiny.includes('[gallery')) {
                    let expl = texttiny.split('</p>');

                    for(let i = 0; i < expl.length; i++) {
                        if(expl[i].length) {
                            tinymce.get('text_content').insertContent(expl[i] + '</p>');
                        }
                    }
                } else {*/
                    tinymce.get('text_content').insertContent(texttiny);
                //}
                //конец проблем......
                
                // после загрузки контента
                document.getElementById('content_preview').classList.remove('hidden');
                /**
                 * клик предпросмотра
                 */
                $('#content_preview').click(function(){
                    window.open(`${sel_site.url_site}/news/${data.slug_content}`, '_blank');
                });
                // end после загрузки контента
            } else {
                jsNotify('Невозможно получить данные', '', '', 'danger');
            }
        },
        beforeSend() {
            //если нет параметра - отменить
            if(!getParamFromUrl('pk_content')) {
                jsNotify('Нет параметра для получения контента', '', '', 'danger');
                return false;
            }
        }
    });
}

/**
 * слушатель на загрузка контента в список для вывода ajax
 */
$('.ajax-list-btns').click(function(){
    const status = $(this).attr('data-status');
    
    $('.ajax-list-btns.btn-primary').addClass('btn-link');
    $('.ajax-list-btns').removeClass('btn-primary');
    $(this).removeClass('btn-link');
    $(this).addClass('btn-primary');
    
    changeUrl('', 'status', status, true);
    changeUrl('', 'pagination', 0, true);
    
    ajaxGetListContent(status);
    
    return false;
});

/**
 * загрузка контента в список для вывода ajax
 */
function ajaxGetListContent(status) {    
    $.ajax({
        dataType: "json",
        data: {
            fk_site: sel_site.id,
            limit: get_content_limit,
            status
        },
        method: "GET",
        url: `${API_URL}/content`,
        success(response) {
            renderListContent(response);
        }
    });
}

/**
 * открытие поля времени при выборе отложенной публикации
 */
function publishLaterTime() {
    $('#status_content').change(function(){
        if(parseInt($(this).val(), 10) === 3) {
            $('#lpt').removeClass('hidden');
        } else {
            $('#lpt').addClass('hidden');
        }
    });
}

/**
 * загрузить картинку и вставить в предпоказ
 */
$('#file_add_inner_view').click(function(){
    const link = document.getElementById('file_innerlink_link').value;
    
    //загрузить картинку и вставить в предпоказ
    let img = document.createElement('img');

    img.src = link;

    document.getElementById('load_file_innerlink').innerHTML = '';
    document.getElementById('load_file_innerlink').appendChild(img);
});

/**
 * загрузка файлов в библитеку
 * добавление по ссылке - отсутствие физического файла
 */
$('#file_add_inner_link').click(function(){
    const name = document.getElementById('file_innerlink_name').value,
        link = document.getElementById('file_innerlink_link').value;
        
    if(link.length > 5) {        
        $.ajax({
            dataType: "json",
            data: {
                fk_site: sel_site.id,
                name,
                link
            },
            method: "POST",
            url: `${API_URL}/upload_link`,
            success(response) {
                if(response.success === true) {
                    document.getElementById('file_innerlink_link').value = '';
                    document.getElementById('file_innerlink_name').value = '';
                    
                    jsNotify('Успешно сохранено');
                    
                    $('#library-tab-li').click();
                } else {
                    $('#library-files-modal').modal('hide');
                    
                    jsNotify('Не удалось сохранить', '', '', 'danger');
                }
            }
        });
    }
});

/**
 * загрузка файлов в библитеку
 * может быть везде
 */
$('#file_upload').click(function(){
    let formData = new FormData(),
        files = document.getElementById('files').files,
        progressbar = document.getElementById("library-progress");
        
    if(files.length < 1) return false;
        
    formData.append("fk_site", sel_site.id);
        
    //подготовить
    for(let i = 0; i < files.length; i++) {
        formData.append('file', files[i]);
    }
    
    //отправить
    let xhr = new XMLHttpRequest();

    //слушатели
    xhr.upload.addEventListener('loadstart', function() {
        progressbar.children[0].innerHTML = '0';
        progressbar.style.display = 'block';
    }, false);
    
    xhr.upload.addEventListener('progress', function(evt) {
        let percent = parseInt(evt.loaded/evt.total*100) + '%';
        progressbar.children[0].innerHTML = percent;
        progressbar.children[0].style.width = percent;
    }, false);

    xhr.addEventListener('readystatechange', function(evt) {
        let status, text, readyState;

        try {
          readyState = evt.target.readyState;
          text = evt.target.responseText;
          status = evt.target.status;
        }
        catch(e) {
          return;
        }

        if (readyState == 4 && status == '200' && evt.target.responseText) {
            setTimeout(function(){
                //очистить инпут
                document.getElementById('files').value = '';
                
                progressbar.style.display = 'none';
                progressbar.children[0].innerHTML = 0;
                progressbar.children[0].style.width = 0;
            }, 1500);
        }
    }, false);

    xhr.open('POST', `${API_URL}/upload`, true);
    xhr.setRequestHeader('auth_id', sitesmodule_id);

    xhr.send(formData);
});

/**
 * класс для предзагрузки библиотеки файлов
 */
$('.open-library').click(function(){
    let cl = $(this).attr('data-click');
    
    if(cl) { // выбрать режим во всплывахе
        $(`#${cl}`).click();
    }
    
    $('#library-tab-li').click();
});

/**
 * выбрать файл для обрезки
 */
$('#files-set-image-btn').click(function() {    
    document.getElementById('blah').setAttribute('src', $('.file-selected').attr('src'));
    
    $('#library-files-modal').modal('hide');
    $('#headimg_crop').modal();
}); 

/**
 * при закрытии - сбросить метку
 */
$("#widget-linked-content").on("hidden.bs.modal", function () {
    $('#widget-linked-content-btn').attr('data-new', 1);
});

/**
 * добавить виджет
 * строгий формат
 */
$('#widget-linked-content-btn').click(function(){
    const pk_content = $('#linked_content').val();
    //const widget_position = $('.widget-lc-position:checked').val();
    
    if(isEmpty(pk_content)) { // пусто - отмена
        return false;
    } else {
        if(parseInt($(this).attr('data-new'), 10) === 1) { // создание
            tinymce.get('text_content').insertContent(`<p>[widgetLinkedContent id="${pk_content}" pos="left"]</p>`);
            $('#widget-linked-content').modal('hide');
        } else { // обновление
            let tinytext = tinymce.get('text_content').getContent().replace(/(\r\n|\n|\r)/gm,"");

            // замена шорткода виджета
            tinytext = tinytext.replace( /\[widgetLinkedContent([^\]]*)\]/g, function( match, data ) {
                const id = parseInt(data.match(/\d+/)[0], 10),
                    pos = data.match(/pos="((?:\\.|[^"\\])*)"/)[1];

                // найти подходящий, не должно быть абсолютно одинаковых виджетов
                if(widget_lc_data.id === id && widget_lc_data.pos === pos) {
                    data = `id="${pk_content}" pos="left"`;
                }

                return `[widgetLinkedContent ${data}]`;
            });

            //и вставить новый
            tinymce.get('text_content').setContent('');
            tinymce.get('text_content').insertContent(tinytext);

            $('#widget-linked-content').modal('hide');
            setTimeout(function(){
                tinymce.get('text_content').focus();
            }, 500);
        }   
    }
});

/**
 * создать галерею - вставка шорткода
 */
$('#files-create-gallery-btn').click(function(){
    let files = document.querySelectorAll('.file-selected'),
        ids = [];
    
    //проверка
    if(files.length === 0) {
        return false;
    }
    
    for(let i = 0; i < files.length; i++) {
        ids.push(files[i].getAttribute('data-id'));
    }
    
    tinymce.get('text_content').insertContent(`<p>[gallery ids="${ids.join(',')}"]</p>`);
    
    $('#library-files-modal').modal('hide');
    setTimeout(function(){
        tinymce.get('text_content').focus();
    }, 500);
});

/**
 * создать галерею - обновить шорткод
 */
$('#files-change-gallery-btn').click(function(){
    let files = document.querySelectorAll('.file-selected'),
        ids = [];
    
    //проверка
    if(files.length === 0) {
        return false;
    }
    
    for(let i = 0; i < files.length; i++) {
        ids.push(files[i].getAttribute('data-id'));
    }
    
    //взять текст, изменить парамерты
    let tinytext = tinymce.get('text_content').getContent().replace(/(\r\n|\n|\r)/gm,"");

    // замена шорткода галерей
    tinytext = tinytext.replace( /\[gallery([^\]]*)\]/g, function( m, oldids ) {        
        oldids = oldids.split('"');
        let compare_old = oldids[1].split(',').map(function(n) {return +n});
        
        // если равны - значит в эту галерею нужно вставить новые ид фоток
        if(array_equals(compare_old, updating_gallery)) {
            oldids[1] = ids.join(',');
            updating_gallery = [];
        }

        return `[gallery${oldids.join('"')}]`;
    });
    
    //console.log(tinymce.get('text_content').selection.getRng().startOffset); // позиция курсора
    
    //и вставить новый
    tinymce.get('text_content').setContent('');    
    tinymce.get('text_content').selection.setContent(tinytext);
    
    $('#library-files-modal').modal('hide');
});

/**
 * клик на выбор в медиафайлах
 * не знаю как лучше, уже запутался
 */
$('.files-choice').click(function(){
    $('.files-choice').removeClass('active');
    $(this).addClass('active');
    
    if($(this).attr('id') == 'files-set-image') { // изобр записи        
        $('#files-add-files-btn').hide();
        $('#files-set-image-btn').show();
        $('#files-create-gallery-btn').hide();
        $('#files-change-gallery-btn').hide();
    } else if($(this).attr('id') == 'files-add-files') { // добавить файлы        
        $('#files-add-files-btn').show();
        $('#files-set-image-btn').hide();
        $('#files-create-gallery-btn').hide();
        $('#files-change-gallery-btn').hide();
    } else {//галерея
        $('#files-add-files-btn').hide();
        $('#files-set-image-btn').hide();
        $('#files-create-gallery-btn').show();
        $('#files-change-gallery-btn').hide();
    }
    
    return false;
});

/**
 * выбрать файл в редактор
 */
$('#files-add-files-btn').click(function(){
    let files = document.querySelectorAll('.file-selected');
    
    for(let i = 0; i < files.length; i++) {
        //вставка изображения в редактор
        tinymce.get('text_content').insertContent(`<img alt="" src="${files[i].getAttribute('src')}"/>`);
    }
    
    $('#library-files-modal').modal('hide');
    setTimeout(function(){
        tinymce.get('text_content').focus();
    }, 500);
});

/**
 * принудительная очистка кэша
 */
$('#clear-cache').click(function() {
    if(confirm('Действительно очистить кэш сайта?')) {
        $.ajax({
            dataType: "json",
            url: `${API_URL}/cache/`,
            method: "DELETE",
            data: {
                fk_site: sel_site.id
            },
            success(response) {
                if(response.success) {
                    jsNotify('Кэш успешно очищен');
                }
            },
            error() {
                jsNotify('Невозможно очистить кэш', '', '', 'danger');
            }
        });
    }
});

/**
 * нажатие кнопки удаления выбранных в списке
 */
$('#delete-selected').click(function(){
    let checked = $('#content-grid').yiiGridView('getSelectedRows');
    
    if(checked < 1) {
        return;
    }
    
    if(confirm(`Выбранные ${checked.length} записей будут удалены`)) {
        $.ajax({
            dataType: "json",
            url: `${API_URL}/content/`,
            method: "DELETE",
            data: {
                fk_site: sel_site.id,
                delArr: checked
            },
            success(response) {
                if(response.success) {
                    jsNotify('Успешно удалено');
                    
                    //удалить строки
                    for(let i = 0; i < checked.length; i++) {
                        $(`tr[data-key=${checked[i]}]`).remove();
                    }
                }
            },
            error() {
                jsNotify('Невозможно удалить', '', '', 'danger');
            }
        });
    };
});

/**
 * удаление файла/ов
 */
$('#file_delete').click(function(){
    let files = document.querySelectorAll('.file-selected'),
        delIds = [];
        
    if(confirm(`Выбранные ${files.length} файлов будут удалены`)) {
        for(let i = 0; i < files.length; i++) {
            delIds.push(files[i].getAttribute('data-id'));
        }

        $.ajax({
            dataType: "json",
            url: `${API_URL}/upload/`,
            method: "DELETE",
            data: {
                fk_site: sel_site.id,
                pk_files: delIds
            },
            success(response) {
                for(let i = 0; i < delIds.length; i++) {
                    $(`.img-square[data-id=${delIds[i]}]`).remove();
                }
            },
            error(err) {
                alert(err);
            }
        });
    }
});

/**
 * сброс выбранных
 */
$('#files-selected-clear').click(function(){
    $('.file-selected').removeClass('file-selected');
    $('#files-selected-count').html($('.file-selected').length);
});

/**
 * сохранение автора
 */
$('#create_author_content').click(function(){
    const fio = $("#pk_content_author option:selected").html().split(' ');
    
    $.ajax({
        dataType: "json",
        url: `${API_URL}/content_authors`,
        method: 'POST',
        data: {
            fk_site: sel_site.id,
            pk_content_author : Number($('#pk_content_author').val()),
            lastname_content_author : fio[0],
            name_content_author : fio[1],
            secondname_content_author : fio[2]
        },
        success(response) {
            if(response.success === true) {
                jsNotify('Автор добавлен');
            } else if(response.success === 'exists') {
                jsNotify('Автор существует');
            } else {
                jsNotify('Проблемы при сохранении', '', '', 'danger');
            }
        },
        error() {
            jsNotify('Проблемы при сохранении', '', '', 'danger');
        }
    });
});

/**
 * при клике - обновлять список
 */
$('#library-tab-li').click(function(){
    $.ajax({
        dataType: "json",
        url: `${API_URL}/upload/list`,
        data: {
            fk_site: sel_site.id
        },
        success(response) {
            //file-selected - для выбранных через ctrl, file-chosen - для подсветки выбранного
            $('#library-tab').html('');
            
            for(let i = 0; i < response.length; i ++) {
                let srcfile;
                
                if(response[i].name_file === null) {
                    srcfile = response[i].link;
                } else {
                    srcfile = `${PUBLIC_FILES_URL}/upload/f/${response[i].name_file}`;
                }
                $('#library-tab').append(`<img class="img-thumbnail img-square" src="${srcfile}" data-id="${response[i].pk_file}">`);
            }
            
            //клик на файл
            $('.img-square').click(function(event) {
                let srcimg = $(this).attr('src');
                
                // 100% просмотр изображения по ширине
                document.getElementById('file_preview').setAttribute('src', srcimg);

                //при ctrl - добавлять в выбранные
                if(event.ctrlKey) {
                    $('.file-chosen').addClass('file-selected');
                    $(this).addClass('file-selected');
                } else {
                    $('.file-selected').removeClass('file-selected');
                }
                
                $('.file-chosen').removeClass('file-chosen');
                $(this).addClass('file-chosen');
                $(this).addClass('file-selected');
                
                //кол-во выбранных
                $('#files-selected-count').html($('.file-selected').length);

                $.ajax({
                    dataType: "json",
                    url: `${API_URL}/upload/infoFile`,
                    data: {
                        fk_site: sel_site.id,
                        pk_file: $(this).attr('data-id')
                    },
                    success(resp) {
                        //вывести данные в параметры файла
                        let fsize = parseFloat(resp[0].size / 1000, 1).toFixed(1) + ' kb'; // килобайты

                        if(parseInt(fsize) > 1000) { // мегабайты
                            fsize = parseFloat(parseFloat(fsize) / 1000, 1).toFixed(1) + ' mb';
                        }

                        //определение размеров
                        let tmpImg = new Image();
                        tmpImg.src = srcimg;
                        $(tmpImg).one('load',function(){
                            $('#library-files-options .dimensions').html(tmpImg.width + " x " + tmpImg.height);
                        });

                        //дата
                        let d = new Date(resp[0].upload_date);
                        
                        $('#library-files-options .filename').html(resp[0].original_name_file);
                        $('#library-files-options .file-size').html(fsize);
                        $('#library-files-options .uploaded').html((d.getDate() > 10 ? d.getDate() : '0' + d.getDate()) + '.' + (d.getMonth() > 10 ? d.getMonth() : '0' + d.getMonth()) + '.' + d.getFullYear());
                    },
                    error() {
                        // очистить все поля
                        let rows = document.querySelector('#library-files-options .details').children;

                        for(let i = 0; i < rows.length; i++) {
                            rows[i].innerHTML = '';
                        }
                    }
                });
            });
        }
    });
});

/**
 * инициализация tinymce
 */
function initTinyMceSites() {
    /**
     * виджет читайте также
     */
    tinymce.PluginManager.add('readMore', function(editor) {
        const onopen = function(innerurl = '', innertitle = '') {
            return editor.windowManager.open({
                title: 'Виджет читайте также',
                body: [
                    {   type: 'textbox',
                        size: 80,
                        height: '100px',
                        name: 'readmore_url',
                        label: 'Ссылка:',
                        value: innerurl
                    },
                    {   type: 'textbox',
                        size: 80,
                        height: '100px',
                        name: 'readmore_title',
                        label: 'Подпись(вручную, но можно автоматически брать заголовок новости из ссылки):',
                        value: innertitle
                    }
                ],
                onsubmit: function(e) {
                    const url = e.data.readmore_url;
                    const title = e.data.readmore_title;
                    
                    if(innerurl.length) {
                        tinyMCE.activeEditor.setContent(tinyMCE.activeEditor.getContent().replace(
                            `[widgetReadMore href="${innerurl}" title="${innertitle}"]`,
                            `[widgetReadMore href="${url}" title="${title}"]`
                        ));
                    } else {
                        tinyMCE.activeEditor.insertContent(`[widgetReadMore href="${url}" title="${title}"]`);
                    }
                }
            });
        }
        
        editor.addButton('readMore', {
            text: 'Читайте также',
            icon: false,
            onclick: function() {
                onopen()
            }
        });
        
        editor.on('DblClick',function(e) {
            if(e.target.innerHTML.indexOf('[widgetReadMore href=') > -1) {
                let innerurl = '';
                let innertitle = '';
                
                e.target.innerHTML.replace(/\[widgetReadMore\s+href="([^"]+)"\s+title="([^"]+)"\]/, function( m, href, title ) {
                    innerurl = href;
                    innertitle = title;
                });
                
                onopen(innerurl, innertitle);
            }
        });
    });
    
    // текстовый блок из типографики, внутри может быть что угодно
    tinymce.PluginManager.add('typography_text_block', function(editor) {
        editor.addButton('typography_text_block', {
            text: 'Текст.блок',
            icon: false,
            onclick: function() {
                tinyMCE.activeEditor.insertContent('<div class="b-article__chapter"><h3>Заголовок</h3><p>Текст...</p></div>');
            }
        });
    });
    
    // сноска 
    tinymce.PluginManager.add('typography_label', function(editor) {
        editor.addButton('typography_label', {
            text: 'Сноска',
            icon: false,
            onclick: function() {
                tinyMCE.activeEditor.insertContent('<div class="b-article__text"><i>Текст...</i></div>');
            }
        });
    });
    
    // embed plugin instagram
    // можно и размеры указывать iframe
    // https://www.picodash.com/instagram/embed#https://www.instagram.com/p/BFKjVxkBsCC/
    tinymce.PluginManager.add('instagram', function(editor) {
        editor.addButton('instagram', {
            text: 'Instagram',
            icon: false,
            onclick: function() {
                editor.windowManager.open({
                    title: 'Виджет instagram',
                    body: [
                        {   type: 'textbox',
                            size: 40,
                            height: '100px',
                            name: 'instagram_url',
                            label: 'instagram url:'
                        }
                    ],
                    onsubmit: function(e) {
                        let url = e.data.instagram_url;
                        url = url.replace( /(...+)\?...+/g, function( m, u) {
                            return u;
                        });
                        
                        url = (url.endsWith('/')) ? url : url + '/';
                        
                        if(url.length && url.match(/...+\/\/www.instagram.com\/p\//)) {
                            tinyMCE.activeEditor.insertContent(`<iframe src="${url}embed/captioned" width="640" height="760" frameborder="0" scrolling="no" allowtransparency="true"></iframe>`);
                        } else {
                            console.error(`Некорректный url: ${url}`);
                        }
                    }
                });
            }
        });
    });
    
    //popupwindow
    tinymce.PluginManager.add('widgetLinkedContent', function( editor, url ) {
        var sh_tag = 'widgetLinkedContent';//тег шорткода
        
        //helper functions 
        function getAttr(s, n) {
            n = new RegExp(n + '=\"([^\"]+)\"', 'g').exec(s);
            return n ?  window.decodeURIComponent(n[1]) : '';
        };
        
        function html(data) {
            data = window.encodeURIComponent(data);

            return `<div class="widget_lc" data-sh-attr="${data}">Виджет связанной новости(редактировать)</div><br>`;//${imgshtml} - тогда регулярку надо менять ниже
        }
        
        function replaceShortcodes(content) {
            // редактор превращает <br> в <p>&nbsp;</p>            
            // дополнение ошибок
            let a = content.replace( /]\/p>/g, function() {
                return ']</p>';
            });
            
            // замена шорткода галерей
            a = a.replace( /\[widgetLinkedContent([^\]]*)\]/g, function( match, attr) {
                return html(attr);
            });
            return a;
        }
        
        function restoreShortcodes( content ) {
            return content.replace( /(?:<p(?: [^>]+)?>)*(<div class="widget_lc" [^>]+>)[^<>]*(<\/div>)(?:<\/p>)*/g, function( match, image ) {
                var data = getAttr( image, 'data-sh-attr' );

                if ( data ) {
                    return '<p>[' + sh_tag + data + ']</p>';
                }
                return match;
            });
        }
        
        //замена шорткода на html
        /*editor.on('BeforeSetcontent', function(event){ 
            event.content = replaceShortcodes(event.content);
        });

        //замена с html на шорткод
        editor.on('GetContent', function(event){
            event.content = restoreShortcodes(event.content);
        });*/
        
        editor.on('DblClick',function(e) {
            //if ( e.target.className.indexOf('widget_lc') > -1 ) { // только с этим классом искать
            if(e.target.innerHTML.indexOf('[widgetLinkedContent') > -1) {
                /*let title = window.decodeURIComponent(e.target.attributes['data-sh-attr'].value),
                    id = parseInt(title.match(/\d+/)[0], 10),
                    pos = title.match(/pos="((?:\\.|[^"\\])*)"/)[1];*/
                
                let id = 0,
                    pos = '';
                
                e.target.innerHTML.replace(/id="(\d+)" pos="(\w+)"/, function( m, idl, posl ) {
                    id = +idl;
                    pos = posl;
                });
                    
                widget_lc_data = {
                    id,
                    pos
                };
                                        
                $('#widget-linked-content').modal('show');
                
                $(`.widget-lc-position[value=${pos}]`).prop("checked", true);
                
                // получить инфо о контенте по ид и вставить в просмотр
                $.ajax({
                    dataType: "json",
                    url: `${API_URL}/contentone/`,
                    method: "GET",
                    data: {
                        fk_site: sel_site.id,
                        pk_content: id
                    },
                    success(response) {
                        // метка обновления виджета
                        $('#widget-linked-content-btn').attr('data-new', 0);
                        
                        sitesModule.setPreviewWidgetLC({
                            text        : response.data.title_content,
                            img         : response.data.headimgsrc_content,
                            slug_content: response.data.slug_content
                        });
                    }
                });
            }
        });
    });
    
    tinymce.PluginManager.add('gallery', function( editor, url ) {
        var sh_tag = 'gallery';//тег шорткода

        //helper functions 
        function getAttr(s, n) {
            n = new RegExp(n + '=\"([^\"]+)\"', 'g').exec(s);
            return n ?  window.decodeURIComponent(n[1]) : '';
        };

        function html(data) {
            /*let id_imgs = data.split('"')[1].split(','); // массив ид изображений в галерее
            let imgshtml = '';

            //получить ссылки на них по data-id
            for(let i = 0; i < id_imgs.length; i++) {
                let srcimg = document.querySelector('img[data-id="' + id_imgs[i] + '"]').getAttribute('src');
                imgshtml += `<img src='${srcimg}' width='100'>`;
            }*/

            data = window.encodeURIComponent(data);

            return `<div class="mceItem" data-sh-attr="${data}">Галерея(редактировать)</div><br>`;//${imgshtml} - тогда регулярку надо менять ниже
        }

        function replaceShortcodes(content) {
            // редактор превращает <br> в <p>&nbsp;</p>            
            // дополнение ошибок
            let a = content.replace( /]\/p>/g, function() {
                return ']</p>';
            });
            
            // замена шорткода галерей
            a = a.replace( /\[gallery([^\]]*)\]/g, function( match, attr) {
                return html(attr);
            });
            return a;
        }

        function restoreShortcodes( content ) {
            return content.replace( /(?:<p(?: [^>]+)?>)*(<div class="mceItem" [^>]+>)[^<>]*(<\/div>)(?:<\/p>)*/g, function( match, image ) {
                var data = getAttr( image, 'data-sh-attr' );

                if ( data ) {
                    return '<p>[' + sh_tag + data + ']</p>';
                }
                return match;
            });
        }

        //замена шорткода на html
        /*editor.on('BeforeSetcontent', function(event){ 
            event.content = replaceShortcodes(event.content);
        });

        //замена с html на шорткод
        editor.on('GetContent', function(event){ // есть проблемы с заменой шорткода
            event.content = restoreShortcodes(event.content);
        });*/

        //открывать создание галереи при клике на шорткод
        editor.on('DblClick',function(e) {
            //if ( e.target.className.indexOf('mceItem') > -1 ) { // только с этим классом искать
            if(e.target.innerHTML.indexOf('[gallery ids=') > -1) {
                let ids = [];
                
                e.target.innerHTML.replace(/ids="(...+)"/, function( match, idsl ) {
                    ids = idsl.split(',').map(function (currentValue) {return +currentValue});
                });
                
                /*let title = window.decodeURIComponent(e.target.attributes['data-sh-attr'].value),
                    ids = title.match(/(?:"[^"]*"|^[^"]*$)/)[0].replace(/"/g, "").split(',');*/
                    
                    // сохранить для сравнения при обновлении галереи
                    updating_gallery = ids;

                $('#library-tab-li').click();
                $('#files-create-gallery').click();
                $.when($('#library-files-modal').modal('show')).then(function(){
                    setTimeout(function(){//после открытия, без этого никак
                        $('#files-change-gallery-btn').show();
                        $('#files-create-gallery-btn').hide();

                        for(let i = 0; i < ids.length; i++) {
                            $(`#library-tab [data-id=${parseInt(ids[i], 10)}]`).addClass('file-selected');
                        }
                    }, 500);
                });
            }
        });
    });
    
    tinymce.PluginManager.add('stylebuttons', function(editor, url) {
        ['h1', 'h2', 'h3'].forEach(function(name){
            editor.addButton("style-" + name, {
                tooltip: "Toggle " + name,
                text: name.toUpperCase(),
                onClick: function() { editor.execCommand('mceToggleFormat', false, name); },
                onPostRender: function() {
                    var self = this, setup = function() {
                        editor.formatter.formatChanged(name, function(state) {
                            self.active(state);
                        });
                    };
                    editor.formatter ? setup() : editor.on('init', setup);
                }
            });
        });
    });
    
    /**
     * подсчет символов(без тегов)
     * лучше шорткоды игнорировать
     */
    tinymce.PluginManager.add('charactercount', function (editor) {
        var _self = this;

        function update() {
          editor.theme.panel.find('#charactercount').text(['Количество символов: {0}', _self.getCount()]);
        }

        editor.on('init', function () {
          var statusbar = editor.theme.panel && editor.theme.panel.find('#statusbar')[0];

          if (statusbar) {
            window.setTimeout(function () {
              statusbar.insert({
                type: 'label',
                name: 'charactercount',
                text: ['Количество символов: {0}', _self.getCount()],
                classes: 'charactercount',
                disabled: editor.settings.readonly
              }, 0);

              editor.on('setcontent beforeaddundo keyup', update);
            }, 0);
          }
        });

        _self.getCount = function () {
          var tx = editor.getContent({ format: 'raw' });
          var decoded = decodeHtml(tx);
          var decodedStripped = decoded.replace(/(<([^>]+)>)/ig, '');
          var tc = decodedStripped.length;
          return tc;
        };

        function decodeHtml(html) {
          var txt = document.createElement('textarea');
          txt.innerHTML = html;
          return txt.value;
        }
      });
        
    tinymce.init({
        selector: 'textarea[tinymce=tinymcesites]',
        menubar: true,
        skin: false,
        statusbar: true,
        language: 'ru',
        image_advtab: true,
        plugins: 'link image code autoresize paste media lists autolink imagetools table stylebuttons wordcount charactercount gallery widgetLinkedContent instagram readMore typography_text_block typography_label',
        imagetools_toolbar: "imageoptions",
        toolbar: 'formatselect | bold italic blockquote | link media | bullist numlist | alignleft aligncenter alignright alignjustify fontsizeselect | outdent indent | removeformat | table | instagram readMore typography_text_block typography_label | code',
        autoresize_bottom_margin: 0,
        autoresize_max_height: 500,
        autoresize_min_height: 200,
        gecko_spellcheck:true,
        image_caption: true,
        media_alt_source: false,
        media_poster: false,
        default_link_target: "_blank",
        content_style: 'img { max-width: 100%; }', // чтоб картинки не разрывали
            content_css: '/css/build/sites.css?v=' + parseInt(Date.now()/100000, 10), // доп стили для сайт модуля
        table_default_styles: {
            width: '100%'
        },
        valid_elements : "@[id|class|style|title|dir<ltr?rtl|lang|xml::lang|onclick|ondblclick|"
            + "onmousedown|onmouseup|onmouseover|onmousemove|onmouseout|onkeypress|"
            + "onkeydown|onkeyup],a[rel|rev|charset|hreflang|tabindex|accesskey|type|"
            + "name|href|target|title|class|onfocus|onblur],strong/b,i,strike,u,"
            + "#p,-ol[type|compact],-ul[type|compact],-li,br,img[longdesc|usemap|"
            + "src|border|alt=|title|hspace|vspace|width|height|align],-sub,-sup,"
            + "-blockquote,-table[border=0|cellspacing|cellpadding|width|frame|rules|"
            + "height|align|summary|bgcolor|background|bordercolor],-tr[rowspan|width|"
            + "height|align|valign|bgcolor|background|bordercolor],tbody,thead,tfoot,"
            + "#td[colspan|rowspan|width|height|align|valign|bgcolor|background|bordercolor"
            + "|scope],#th[colspan|rowspan|width|height|align|valign|scope],caption,figure[class],figcaption,+div,"
            + "-span,-code,-pre,address,-h1,-h2,-h3,-h4,-h5,-h6,hr[size|noshade],-font[face"
            + "|size|color],dd,dl,dt,cite,abbr,acronym,del[datetime|cite],ins[datetime|cite],"
            + "object[classid|width|height|codebase|*],param[name|value|_value],embed[type|width"
            + "|height|src|*],script[src|type],map[name],area[shape|coords|href|alt|target],bdo,"
            + "button,col[align|char|charoff|span|valign|width],colgroup[align|char|charoff|span|"
            + "valign|width],dfn,fieldset,form[action|accept|accept-charset|enctype|method],"
            + "input[accept|alt|checked|disabled|maxlength|name|readonly|size|src|type|value],"
            + "kbd,label[for],legend,noscript,optgroup[label|disabled],option[disabled|label|selected|value],"
            + "q[cite],samp,select[disabled|multiple|name|size],small,"
            + "textarea[cols|rows|disabled|name|readonly],tt,var,big,iframe[src|frameborder|style|scrolling|class|width|height|name|align]",
        rel_list: [
            {title: 'Нет', value: ''},
            {title: 'nofollow', value: 'nofollow'}
        ],
        formats: {
            italic: {
                inline: 'i'
            }
        },
        block_formats: 'Paragraph=p;Heading 1=h1;Heading 2=h2;Heading 3=h3;Heading 4=h4;',
        setup: function (editor) { //не хочет сохранять иначе
            editor.on('change', function () {
                editor.save();
            });
            
            editor.on('init', function () {
                sitesModule.afterLoadEditor();
                
                // чтобы редактор не пролистывался вниз. при добавлении кнопок менять на +1
                window.onscroll = function() {                    
                    if(document.getElementById('mceu_22').getBoundingClientRect().top <= 40 && document.getElementById('intro_content').getBoundingClientRect().top < 0) {
                        document.getElementById('mceu_22').style.position = 'fixed';
                        document.getElementById('mceu_22').style.top = '50px';
                        document.getElementById('mceu_22').style.width = '60%';
                    } else if(document.getElementById('intro_content').getBoundingClientRect().top > 0) {
                        document.getElementById('mceu_22').style.position = 'relative';
                        document.getElementById('mceu_22').style.top = '';
                        document.getElementById('mceu_22').style.width = '';
                    }
                }
            });
        }
    });
}

/** текстовые блоки */

/**
 * изменение типа текстового поля
 */
$('.types_text_block').change(function(){
    if($(this).attr('id') === 'type_html') { // создать tinymce
        $('#text_text_block').attr('tinymce', 'tinymcesites');
        initTinyMceSites();
    } else { // оставить только textarea
        $('#text_text_block').attr('tinymce', '');
        tinymce.get('text_text_block').remove();
    }
})

/**
 * создание/обновление текстового блока
 */
$('#text_block_publish').click(function(){
    let validate = {},
        hasErrors = 0,
        textBlock = {};
    let sel = document.getElementById("isactive");
    sel = sel.options[sel.selectedIndex].getAttribute('value');

    //сбор данных
    textBlock.pk_text_block = parseInt(getParamFromUrl('pk_text_block'), 10) || 0;
    
    textBlock.type = $('.types_text_block:checked').attr('id');    
    textBlock.label = $.trim(document.getElementById('label_text_block').value);
    
    if(textBlock.type === 'type_html') {
        textBlock.type = 2;
        textBlock.text = tinymce.get('text_text_block').getContent().replace(/(\r\n|\n|\r)/gm,"");
    } else {
        textBlock.type = 1;
        textBlock.text = document.getElementById('text_text_block').value;
    }
    
    textBlock.isactive = parseInt(sel, 10);
    textBlock.fk_user_created = window.uniqid;
    
    //валидация метки
    if(textBlock.label.length < 1) {
        $('#label_text_block').parent().addClass('has-error');
        validate.title = false;
    } else {
        $('#label_text_block').parent().removeClass('has-error');
        validate.title = true;
    }
    
    //валидация содержимого
    if(textBlock.text.length < 1) {
        $('#text_text_block').parent().addClass('has-error');
        validate.text = false;
    } else {
        $('#text_text_block').parent().removeClass('has-error');
        validate.text = true;
    }
    
    for (let val in validate) {
        if(validate[val] === false) {
            ++hasErrors;
        }
    }
    
    if(hasErrors === 0) { // можно сохранять        
        $.ajax({
            dataType: "json",
            data: {
                type_block      : textBlock.type,
                label_block     : textBlock.label,
                text_block      : textBlock.text,
                isactive        : textBlock.isactive,
                pk_text_block   : textBlock.pk_text_block,
                fk_user_created : textBlock.fk_user_created,
                fk_site         : sel_site.id
            },
            method: "POST",
            url: `${API_URL}/text_blocks`,
            success(response) {
                if(response.success) {
                    jsNotify('Успешно сохранено');
                } else { // здесь только одна ошибка - метка не уникальна
                    $('#label_text_block').parent().addClass('has-error');
                    validate.title = false;
                    jsNotify('Даная метка уже используется. Выберите другую', '', '', 'danger');
                }
            }
        });
    }
});

/**
 * загрузка текстового блока
 */
function registerLoadTextBlock() {
    const pk_text_block = parseInt(getParamFromUrl('pk_text_block'), 10);
    
    $.ajax({
        dataType: "json",
        url: `${API_URL}/text_block/`,
        method: "GET",
        data: {
            fk_site: sel_site.id,
            pk_text_block
        },
        success(response) {
            // текст раскодированный
            const text_block = $('#label_text_block').html(response.text_block).text();
            
            //инициализировать текст
            if(response.type_block === 1) { // text
                document.getElementById('text_text_block').value = text_block;
                $('#type_text').click();
            } else { // html
                $('#type_html').click();
                
                // дождаться инициализации редактора и вставить
                sitesModule.afterloadEditor.push(function() {
                    tinymce.get('text_text_block').insertContent(text_block)
                });
            }
            
            document.getElementById('label_text_block').value = response.label_text_block;
            
            $('#isactive').val(response.isactive).trigger("change");
        },
        error(err) {
            jsNotify('Проблемы загрузки текстового блока', '', '', 'danger');
            console.error(err);
        }
    });
}
window.registerLoadTextBlock = registerLoadTextBlock;

/**
 * нажатие кнопки удаления выбранных в списке
 */
$('#delete_selected_text_blocks').click(function(){
    let checked = $('#content-grid').yiiGridView('getSelectedRows');
    
    if(checked < 1) {
        return;
    }
    
    if(confirm(`Выбранные ${checked.length} записей будут удалены`)) {
        $.ajax({
            dataType: "json",
            url: `${API_URL}/text_blocks/`,
            method: "DELETE",
            data: {
                fk_site: sel_site.id,
                delArr: checked
            },
            success(response) {
                if(response.success) {
                    jsNotify('Успешно удалено');
                    
                    //удалить строки
                    for(let i = 0; i < checked.length; i++) {
                        $(`tr[data-key=${checked[i]}]`).remove();
                    }
                }
            },
            error() {
                jsNotify('Невозможно удалить', '', '', 'danger');
            }
        });
    };
});

/** end текстовые блоки */

/** доп функции мои */

/**
 * вводить только латиницу(для меток)
 * не дает вводить другие символы, заменяет их на пустоту
 */
function onlyLatinText(jq_ident) {
    $(jq_ident).keyup(function() {
        this.value = this.value.replace(/[^0-9a-zA-Z]/, "");
    });
}
window.onlyLatinText = onlyLatinText;

/**
 * доп функции npm пакеты
 */

/**
 * https://github.com/ianstormtaylor/is-empty
 */
function isEmpty(val) {
  // Null and Undefined...
  if (val == null) return true

  // Booleans...
  if ('boolean' == typeof val) return false

  // Numbers...
  if ('number' == typeof val) return val === 0

  // Strings...
  if ('string' == typeof val) return val.length === 0

  // Functions...
  if ('function' == typeof val) return val.length === 0

  // Arrays...
  if (Array.isArray(val)) return val.length === 0

  // Errors...
  if (val instanceof Error) return val.message === ''

  // Objects...
  if (val.toString == Object.prototype.toString) {
    switch (val.toString()) {

      // Maps, Sets, Files and Errors...
      case '[object File]':
      case '[object Map]':
      case '[object Set]': {
        return val.size === 0
      }

      // Plain objects...
      case '[object Object]': {
        for (var key in val) {
          if (Object.prototype.hasOwnProperty.call(val, key)) return false
        }

        return true
      }
    }
  }

  // Anything else...
  return false
}
window.isEmpty = isEmpty;

/**
 * равенство массивов
 * 
 * https://github.com/component/array-equal
 */
function array_equals(arr1, arr2) {
  var length = arr1.length
  if (length !== arr2.length) return false
  for (var i = 0; i < length; i++)
    if (arr1[i] !== arr2[i])
      return false
  return true
}