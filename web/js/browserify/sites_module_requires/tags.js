/* global API_URL, sel_site, contentPage */

let f = {
    /**
    * получить список тегов
    */
    getTagsList: function() {
       $.ajax({
           dataType: "json",
           data: {
               fk_site: sel_site.id,
               offset: 0
           },
           method: "GET",
           url: `${API_URL}/tags`,
           success(response) {
               $('#count-tags').html(response.length);

               for(let i = 0; i < response.length; i ++) {
                   tags.addRowTagsList(response[i]);
               }
           }
       });
    },
    
    /**
    * добавление строки в таблицу
    */
    addRowTagsList: function(data, top = false) {
       let l;

       if(top === true) {
           l = 0;
       } else {
           l = document.querySelector('#list-tags tbody').rows.length;
       }

       let row = document.querySelector('#list-tags tbody').insertRow(l);
       let cell = row.insertCell(0);
       cell.innerHTML = data.name_tag;
    },
    
    /**
     * поиск и добавление тегов
     */ 
    registerAutocompleteTags: function() {
        function split( val ) {
            return val.split( /,\s*/ );
        }
        function extractLast( term ) {
            return split( term ).pop();
        }

        $( "#tags_content" )
        .on( "keydown", function( event ) {
          if ( event.keyCode === $.ui.keyCode.TAB && $( this ).autocomplete( "instance" ).menu.active ) {
            event.preventDefault();
          }
        })
        .autocomplete({
          source: function( request, response ) {
            $.getJSON(`${API_URL}/tags/findByName`, {
              name: extractLast( request.term ),
              fk_site: sel_site.id
            }, response );
          },
          search: function() {
            var term = extractLast( this.value );
            if ( term.length < 2 ) {
              return false;
            }
          },
          focus: function() {
            return false;
          },
          select: function( event, ui ) {
            var terms = split( this.value );
            terms.pop();
            terms.push( ui.item.label );
            terms.push( "" );
            this.value = terms.join( "," );

            //не добавлять одно и то же
            let has = false;
            for(let i = 0; i < contentPage.tags.length; i++) {
                if(ui.item.id === contentPage.tags[i].id) {
                    has = true;
                }
            }
            if(has === false) {
                contentPage.tags.push({
                    id: ui.item.id,
                    label: ui.item.label
                });
            }

            return false;
          }
        });

        /**
         * вывести теги и очистить input
         */
        $('#tags_content_add_tags').click(function() {
            let input_tags = $('#tags_content').val().split(',').filter(v=>v!='');
            input_tags = [...new Set(input_tags)]; // убрать повторяющиеся

            $('#tags_list').html('');

            for(let i = 0; i < contentPage.tags.length; i++) {
                let key = input_tags.indexOf(contentPage.tags[i].label);
                if(key !== -1) {
                    input_tags.splice(key, 1);
                }

                $('#tags_list').append(`<li data-id='${contentPage.tags[i].id}'><span class='${contentPage.tags[i].id === -1 ? 'text-green' : ''}'>${contentPage.tags[i].label}</span> <i class="fa fa-times text-red"></i></li>`);
            }

            //новые теги
            for(let i = 0; i < input_tags.length; i++) {
                contentPage.tags.push({
                    id: -1,
                    label: input_tags[i].trim()
                });

                $('#tags_list').append(`<li data-id='-1'><span class='text-green'>${input_tags[i].trim()}</span> <i class="fa fa-times text-red"></i></li>`);
            }

            $('#tags_content').val('');

            setDelTags();
        });

        /**
         * удаление тега полностью
         */
        function setDelTags() {
            $('#tags_list .fa-times').click(function(){
                let id_tag = parseInt($(this).parent().attr('data-id'));

                for(let i = 0; i < contentPage.tags.length; i++) {
                    if(contentPage.tags[i].id === -1) {
                        if(contentPage.tags[i].label === $(this).parent().children('span').html()) {
                            contentPage.tags.splice(i, 1);
                            $(this).parent().remove();
                        }
                    } else if(contentPage.tags[i].id === id_tag) {
                        contentPage.tags.splice(i, 1);
                        $(this).parent().remove();
                    }
                }
            });
        }
    }
}

module.exports = f;

global.tags = f;