/* global socket, uniqid */

/**
 * человек уходит из редактирования
 * @param {int} uniqid      - ид пользователя
 * @param {int} pk_content  - ид редактируемого контента
 * @param {int} rand        - уник число, для нескольких вкладок
 */
function onLeave(uniqid, pk_content, rand) {
    window.addEventListener("beforeunload", () => {
        socket.emit('sitesClosedContent', {
            uniqid,
            pk_content,
            rand
        });
    });
}

let f = {
    /**
     * сохранять в сокете кто что редактирует, независимо от сайта
     * можно уведомлять, если открыто 2 вкладки с одинаковым контентом(ид)
     */
    onLoad: function() {
        let pk_content = parseInt(getParamFromUrl('pk_content'), 10);
        let rand = new Date().getMilliseconds();
        
        if(Number.isInteger(pk_content) && pk_content > 0) {
            socket.emit('sitesOpenedContent', {
                uniqid:window.uniqid,
                pk_content,
                rand
            });
            
            // проверка
            socket.on('sitesHasOpenedContent', (data) => {
                if(data.has === true) {
                    document.getElementById('notif').innerHTML = `<div class="alert alert-warning" role="alert">Этот контент уже редактирует '${data.lastname} ${data.name}'. Редактирование недоступно</div>`;
                    
                    // удалить кнопку сохранить
                    document.getElementById('content_publish').remove();
                } else { // раз не сохраняет, то и можно не напрягать при удалении
                    onLeave(uniqid, pk_content, rand);
                }
            });
        }
    },
    onLeave:onLeave
};

module.exports = f;

global.socketio = f;