/* global sel_site */

let f = {
    /**
     * просто превью для загруженного контента
     */
    setPreviewWidgetLC: function(content) {
        if(isEmpty(content)) {
            alert('empty parameter content');
        } else {
            $('#preview-widget-lc').html('');

            if(content.text) {
                $('#preview-widget-lc').html(`<a href="${sel_site.url_site}/news/${content.slug_content}" target="_blank"><img src="${content.img}" style="width:200px"></a><p>${content.text}</p>`);
            }
        }
    },
    
    menu: require('./menu'),
    
    stories: require('./stories'),
    
    /**
     * хранение функций после загрузки
     */
        afterload: [],
        afterloadEditor: [],
        /**
         * проверка для выполнения 1 раз при загрузке tinymce
         */
        afterLoadEditorOnce: 0,
        /**
         * регистрация функций после загрузки редактора
         * можно передавать в виде анонимной фунции для удобства
         */
        afterLoadEditor: function() {
            if(this.afterLoadEditorOnce === 1) {
                return;
            } else {
                this.afterLoadEditorOnce = 1;

                for(let i = 0; i < this.afterloadEditor.length; i ++) {
                    this.afterloadEditor[i]();
                }
            }
        },
        /**
         * регистрация функций после загрузки
         */
        afterLoad: function() {
            for(let i = 0; i < this.afterload.length; i ++) {
                this.afterload[i]();
            }
        }
    /**
     * end хранение функций после загрузки
     */
};

module.exports = f;

// глобальные функции
global.sitesModule = f;


