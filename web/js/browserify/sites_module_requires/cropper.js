/* global sel_site, API_URL */

let cropper, file_id;
let image = document.getElementById('blah');
let options = {
    aspectRatio: 16 / 9,
    zoomable: false,
    viewMode: 2,
    preview: '.crop_img-preview',
    minContainerWidth: 300,
    minContainerHeight: 200,
    minCanvasWidth: 300,
    minCanvasHeight: 200,
    crop: function(event) {
        document.getElementById('crop_dimensions').innerHTML = `${event.detail.width.toFixed(2)} x ${event.detail.height.toFixed(2)}`;
    }
};

// нажатие кнопок вращения изображения
function setRotatable() {
    document.querySelector('.docs-rotates').onclick = (event) => {
        if (!cropper) {
            return;
        }

        let e = event || window.event;
        let target = e.target || e.srcElement;
        let val;

        // плохо сделано
        if (target.tagName.toLowerCase() === 'label') {
            target = target.querySelector('input');
        } else {
            target = target.parentElement.parentElement.querySelector('input');
        }

        val = +target.value;

        if(target.getAttribute('id') === 'rotateRight') {
            val = -val;
        }

        cropper.rotate(val);
    }
}

function onClickSave() {
    document.getElementById('files-saveset-image-btn').onclick = () => {
        if(confirm('Обрезка перезапишет исходное изображение. Продолжить?')) {            
            let img = cropper.getCroppedCanvas().toDataURL('image/jpeg');
            let blobfile = dataURItoBlob(img);
            // blobfile.size - размер файла здесь
            
            let formData = new FormData();
            formData.append('croppedImage', blobfile, 'cropped.jpg');
            formData.append("fk_site", sel_site.id);
            formData.append("file_id", file_id); // имя или ид файла для замены/пересохранения
            
            $.ajax(`${API_URL}/upload`, {
                method: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    if(data.success === true) {
                        jsNotify('Успешно сохранено');
                        $('#head_img_src>img').attr('src', $('.file-selected').attr('src')); // имя не изменяется
                    } else {
                        jsNotify('Ошибка сохранения изображения', '', '', 'danger');
                    }
                },
                error: function () {
                    jsNotify('Ошибка сохранения изображения', '', '', 'danger');
                },
                complete: function() {
                    $('#headimg_crop').modal('hide');
                }
            });
        }
    }
}

/**
 * операции с обрезкой изображений
 */
let f = {
    start: function(f_id) {
        file_id = f_id;
        
        if(cropper) {
            cropper.destroy();
        }

        cropper = new Cropper(image, options);
        
        setRotatable();
        
        onClickSave();
    },
    destroy: function(){
        if(cropper) {
            cropper.destroy();
        }
    }
};

module.exports = f;

global.cropper = f;


/**
 * говорящее название
 * @param {type} dataURI
 * @returns {Blob|dataURItoBlob.blob}
 */
function dataURItoBlob(dataURI) {
  // convert base64 to raw binary data held in a string
  // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
  var byteString = atob(dataURI.split(',')[1]);

  // separate out the mime component
  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

  // write the bytes of the string to an ArrayBuffer
  var ab = new ArrayBuffer(byteString.length);

  // create a view into the buffer
  var ia = new Uint8Array(ab);

  // set the bytes of the buffer to the correct values
  for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
  }

  // write the ArrayBuffer to a blob, and you're done
  var blob = new Blob([ab], {type: mimeString});
  return blob;
}