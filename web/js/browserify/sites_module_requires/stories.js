/* global API_URL, sel_site */

/**
 * операции с меню
 */

module.exports = {
    /**
     * загрузка информации о меню
     */
    registerLoadStoryInfo: function() {
       const pk_menu = parseInt(getParamFromUrl('pk_menu'), 10) || 0;

       $.ajax({
           dataType: "json",
           url: `${API_URL}/menu/`,
           method: "GET",
           data: {
               fk_site: sel_site.id,
               pk_menu
           },
           success(response) {
               document.getElementById('name_menu').value = response.zzname_menu;
               document.getElementById('label_menu').value = response.label_menu;
           },
           error(err) {
               jsNotify('Проблемы загрузки меню', '', '', 'danger');
               console.error(err);
           }
       });
    },

    /**
     * сохранение/обновление меню
     */
    /**registerSaveStory: function() {
       const pk_menu = parseInt(getParamFromUrl('pk_menu'), 10) || 0;

       $('#save_menu').submit(() => {        
           $.ajax({
               dataType: "json",
               url: `${API_URL}/menu/`,
               method: "POST",
               data: {
                   fk_site: sel_site.id,
                   menu: {
                       pk_menu,
                       name_menu: document.getElementById('name_menu').value,
                       label_menu: document.getElementById('label_menu').value
                   }
               },
               success(response) {
                   if(response.success) {
                       jsNotify('Успешно сохранено');
                   } else {
                       jsNotify('Проблемы при сохранении', '', '', 'danger');
                       console.error(response);
                   }
               },
               error(err) {
                   jsNotify('Проблемы сохранения сюжета', '', '', 'danger');
                   console.error(err);
               }
           });

           return false;
       });
    },*/

    /**
     * загрузк меню
     */
    registerLoadStories: function() {
        const pk_menu = parseInt(getParamFromUrl('pk_menu'), 10);

        $.ajax({
            dataType: "json",
            url: `${API_URL}/content_stories/`,
            method: "GET",
            data: {
                fk_site: sel_site.id
            },
            success(response) {
                const table = document.querySelector('#menu-table tbody');

                //собрать данные для nestable
                let arrnest = [];

                //очистить
                table.innerHTML = '';

                //вставлять
                const items = response.data;
                for(let i = 0; i < items.length; i ++) {
                    if(items[i].is_active === 1) { // составлять только из активных
                        arrnest.push({
                            id: items[i].pk_content_story,
                            content: items[i].title_content_story
                        });
                    }

                    let row = table.insertRow();
                    row.id = `tr-${items[i].pk_content_story}`;

                    row.insertCell(0).innerHTML = `<div class="form-group"><input type="text" value="${items[i].title_content_story}" class="form-control title_content_story"></div>`;
                    row.insertCell(1).innerHTML = `<select id="is_active-${items[i].pk_content_story}">
    <option value="1" ${(items[i].is_active === 1) ? 'selected' : ''}>Да</option>
    <option value="0" ${(items[i].is_active === 0) ? 'selected' : ''}>Нет</option>
    </select>`;
                    row.insertCell(2).innerHTML = `<a href="${sel_site.url_site}/story/${items[i].pk_content_story}" target="_blank" class="btn btn-link btn-sm">Ссылка</a>`;
                    row.insertCell(3).innerHTML = `<button class="btn btn-success btn-sm save-story-one">Сохранить</button><button class="fa fa-times btn btn-danger delete-story-one"></button>`;

                    $(`#is_active-${items[i].pk_content_story}`).select2({"theme":"krajee","width":"100%","language":"ru-RU"});
                }

                //пустую строку для новых пунктов
                let row = table.insertRow();
                row.id = `tr-0`;

                row.insertCell(0).innerHTML = `<div class="form-group"><input type="text" value="" class="form-control title_content_story"></div>`;
                row.insertCell(1).innerHTML = `<select id="is_active-0">
    <option value="1" selected>Да</option>
    <option value="0">Нет</option>
    </select>`;
                row.insertCell(2).innerHTML = ``;
                row.insertCell(3).innerHTML = `<button class="btn btn-success btn-sm save-story-one">Создать</button>`;

                $(`#is_active-0`).select2({"theme":"krajee","width":"100%","language":"ru-RU"});
                //end пустую строку для новых пунктов            

                /**$('.delete-story-one').click(function(){
                    const pk_content_story = parseInt($(this).parent().parent().attr('id').substr(3), 10);

                    if(confirm('Действительно удалить?')) {
                        $.ajax({
                            dataType: "json",
                            url: `${API_URL}/menu_item/`,
                            method: "DELETE",
                            data: {
                                fk_site: sel_site.id,
                                pk_menu,
                                pk_content_story
                            },
                            success(response) {
                                if(response.success) {
                                    $(`#tr-${pk_content_story}`).remove();
                                    $('.dd').nestable('remove', pk_content_story);
                                    jsNotify('Успешно удалено');
                                } else {
                                    jsNotify('Проблемы при удалении', '', '', 'danger');
                                    console.error(response);
                                }
                            }
                        });
                    }
                });*/

                //повесить событие клика(сохранение)
                $('.save-story-one').click(function(){
                    let validate = {}, hasErrors = 0;

                    const pk_content_story = $(this).parent().parent().attr('id');
                    let content_story = {
                        pk_content_story    : parseInt(pk_content_story.substr(3), 10),
                        title_content_story  : document.querySelector(`#${pk_content_story} .title_content_story`).value,
                        //path_menu_item  : document.querySelector(`#${pk_content_story} .path_menu_item`).value,
                        is_active        : $(`#${pk_content_story} select`).val()
                    };

                    //валидация
                    if(content_story.title_content_story.length < 1) {
                        $(`#tr-${content_story.pk_content_story} .title_content_story`).parent().addClass('has-error');
                        validate.title_content_story = false;
                    } else {
                        $(`#tr-${content_story.pk_content_story} .title_content_story`).parent().removeClass('has-error');
                        validate.title_content_story = true;
                    }

                    /**if(content_story.path_menu_item.length < 1) {
                        $(`#tr-${menu_item.pk_content_story} .path_menu_item`).parent().addClass('has-error');
                        validate.path_menu_item = false;
                    } else {
                        $(`#tr-${menu_item.pk_content_story} .path_menu_item`).parent().removeClass('has-error');
                        validate.path_menu_item = true;
                    }*/
                    
                    for (let val in validate) {
                        if(validate[val] === false) {
                            ++hasErrors;
                        }
                    }
                    //end валидация

                    if(hasErrors === 0) {
                        $.ajax({
                            dataType: "json",
                            url: `${API_URL}/content_story/`,
                            method: "POST",
                            data: {
                                fk_site: sel_site.id,
                                content_story
                            },
                            success(response) {
                                if(response.success) {
                                    jsNotify('Успешно сохранено');
                                } else {
                                    jsNotify('Проблемы при сохранении', '', '', 'danger');
                                    console.error(response);
                                }

                                setTimeout(function(){
                                    location.reload()
                                }, 500);
                            }
                        });
                    }
                });

                //создать nestable
                $("#nestable-json").nestable({
                    json: arrnest,
                    maxDepth: 2
                });

                //отслеживать изменения
                $('.dd').change(() => {
                    let saveSort = [];
                    let sorts = $('.dd').nestable('serialize');

                    for(let i = 0; i < sorts.length; i++) {
                        saveSort.push({
                            id  : sorts[i].id,
                            sort: i + 1 
                        });
                    }

                    $.ajax({
                        dataType: "json",
                        url: `${API_URL}/content_stories_sort/`,
                        method: "POST",
                        data: {
                            fk_site: sel_site.id,
                            saveSort
                        },
                        success(response) {
                            if(response.success) {
                                jsNotify('Успешно сохранено');
                            } else {
                                jsNotify('Проблемы при сохранении', '', '', 'danger');
                                console.error(response);
                            }
                        }
                    });
                });
            },
            error(err) {
                alert(err);
            }
        });
    }
};