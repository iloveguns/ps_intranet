/* global API_URL, sel_site */

/**
 * операции с меню
 */

module.exports = {
    /**
     * удаление меню
     */
    registerRemoveMenu: function() {
        $('.removemenu').click(function(){
            if(confirm('Точно удалить?')) {
               $.ajax({
                   dataType: "json",
                   url: `${API_URL}/menu/`,
                   method: "DELETE",
                   data: {
                       fk_site: sel_site.id,
                       pk_menu: parseInt($(this).attr('data-id'), 10)
                   },
                   success(response) {
                       if(response.success) {
                           jsNotify('Успешно удалено');
                           setTimeout(function(){
                               location.reload();
                           }, 500);
                       } else {
                           jsNotify('Проблемы при удалении', '', '', 'danger');
                           console.error(response);
                       }
                   },
                   error(err) {
                       jsNotify('Проблемы при удалении', '', '', 'danger');
                       console.error(err);
                   }
               });
            }

           return false;
        });
    },

    /**
     * загрузка информации о меню
     */
    registerLoadMenuInfo: function() {
       const pk_menu = parseInt(getParamFromUrl('pk_menu'), 10) || 0;

       $.ajax({
           dataType: "json",
           url: `${API_URL}/menu/`,
           method: "GET",
           data: {
               fk_site: sel_site.id,
               pk_menu
           },
           success(response) {
               document.getElementById('name_menu').value = response.zzname_menu;
               document.getElementById('label_menu').value = response.label_menu;
           },
           error(err) {
               jsNotify('Проблемы загрузки меню', '', '', 'danger');
               console.error(err);
           }
       });
    },

    /**
     * сохранение/обновление меню
     */
    registerSaveMenu: function() {
       const pk_menu = parseInt(getParamFromUrl('pk_menu'), 10) || 0;

       $('#save_menu').submit(() => {        
           $.ajax({
               dataType: "json",
               url: `${API_URL}/menu/`,
               method: "POST",
               data: {
                   fk_site: sel_site.id,
                   menu: {
                       pk_menu,
                       name_menu: document.getElementById('name_menu').value,
                       label_menu: document.getElementById('label_menu').value
                   }
               },
               success(response) {
                   if(response.success) {
                       jsNotify('Успешно сохранено');
                   } else {
                       jsNotify('Проблемы при сохранении', '', '', 'danger');
                       console.error(response);
                   }
               },
               error(err) {
                   jsNotify('Проблемы сохранения меню', '', '', 'danger');
                   console.error(err);
               }
           });

           return false;
       });
    },

    /**
     * загрузк меню
     */
    registerLoadMenu: function() {
        const pk_menu = parseInt(getParamFromUrl('pk_menu'), 10);

        $.ajax({
            dataType: "json",
            url: `${API_URL}/menu_items/`,
            method: "GET",
            data: {
                fk_site: sel_site.id,
                pk_menu
            },
            success(response) {
                const table = document.querySelector('#menu-table tbody');

                //собрать данные для nestable
                let arrnest = [];

                //очистить
                table.innerHTML = '';

                //вставлять
                const items = response.data.menu_items;
                for(let i = 0; i < items.length; i ++) {
                    if(items[i].isactive === 1) { // составлять только из активных
                        arrnest.push({
                            id: items[i].pk_menu_item,
                            content: items[i].name_menu_item
                        });
                    }

                    let row = table.insertRow();
                    row.id = `tr-${items[i].pk_menu_item}`;

                    row.insertCell(0).innerHTML = `<div class="form-group"><input type="text" value="${items[i].name_menu_item}" class="form-control name_menu_item"></div>`;
                    row.insertCell(1).innerHTML = `<div class="form-group"><input type="text" value="${items[i].path_menu_item}" class="form-control path_menu_item"></div>`;
                    row.insertCell(2).innerHTML = `<select id="isactive-${items[i].pk_menu_item}">
    <option value="1" ${(items[i].isactive === 1) ? 'selected' : ''}>Да</option>
    <option value="0" ${(items[i].isactive === 0) ? 'selected' : ''}>Нет</option>
    </select>`;
                    row.insertCell(3).innerHTML = `<a href="${sel_site.url_site}${items[i].path_menu_item}" target="_blank" class="btn btn-link btn-sm">Ссылка</a>`;
                    row.insertCell(4).innerHTML = `<button class="btn btn-success btn-sm save-menu-one">Сохранить</button><button class="fa fa-times btn btn-danger delete-menu-one"></button>`;

                    $(`#isactive-${items[i].pk_menu_item}`).select2({"theme":"krajee","width":"100%","language":"ru-RU"});
                }

                //пустую строку для новых пунктов
                let row = table.insertRow();
                row.id = `tr-0`;

                row.insertCell(0).innerHTML = `<div class="form-group"><input type="text" value="" class="form-control name_menu_item"></div>`;
                row.insertCell(1).innerHTML = `<div class="form-group"><input type="text" value="" class="form-control path_menu_item"></div>`;
                row.insertCell(2).innerHTML = `<select id="isactive-0">
    <option value="1" selected>Да</option>
    <option value="0">Нет</option>
    </select>`;
                row.insertCell(3).innerHTML = ``;
                row.insertCell(4).innerHTML = `<button class="btn btn-success btn-sm save-menu-one">Создать</button>`;

                $(`#isactive-0`).select2({"theme":"krajee","width":"100%","language":"ru-RU"});
                //end пустую строку для новых пунктов            

                $('.delete-menu-one').click(function(){
                    const pk_menu_item = parseInt($(this).parent().parent().attr('id').substr(3), 10);

                    if(confirm('Действительно удалить?')) {
                        $.ajax({
                            dataType: "json",
                            url: `${API_URL}/menu_item/`,
                            method: "DELETE",
                            data: {
                                fk_site: sel_site.id,
                                pk_menu,
                                pk_menu_item
                            },
                            success(response) {
                                if(response.success) {
                                    $(`#tr-${pk_menu_item}`).remove();
                                    $('.dd').nestable('remove', pk_menu_item);
                                    jsNotify('Успешно удалено');
                                } else {
                                    jsNotify('Проблемы при удалении', '', '', 'danger');
                                    console.error(response);
                                }
                            }
                        });
                    }
                });

                //повесить событие клика(сохранение)
                $('.save-menu-one').click(function(){
                    let validate = {}, hasErrors = 0;

                    const pk_menu_item = $(this).parent().parent().attr('id');
                    let menu_item = {
                        pk_menu_item    : parseInt(pk_menu_item.substr(3), 10),
                        name_menu_item  : document.querySelector(`#${pk_menu_item} .name_menu_item`).value,
                        path_menu_item  : document.querySelector(`#${pk_menu_item} .path_menu_item`).value,
                        isactive        : $(`#${pk_menu_item} select`).val()
                    };

                    //валидация
                    if(menu_item.name_menu_item.length < 1) {
                        $(`#tr-${menu_item.pk_menu_item} .name_menu_item`).parent().addClass('has-error');
                        validate.name_menu_item = false;
                    } else {
                        $(`#tr-${menu_item.pk_menu_item} .name_menu_item`).parent().removeClass('has-error');
                        validate.name_menu_item = true;
                    }

                    if(menu_item.path_menu_item.length < 1) {
                        $(`#tr-${menu_item.pk_menu_item} .path_menu_item`).parent().addClass('has-error');
                        validate.path_menu_item = false;
                    } else {
                        $(`#tr-${menu_item.pk_menu_item} .path_menu_item`).parent().removeClass('has-error');
                        validate.path_menu_item = true;
                    }

                    for (let val in validate) {
                        if(validate[val] === false) {
                            ++hasErrors;
                        }
                    }
                    //end валидация

                    if(hasErrors === 0) {
                        $.ajax({
                            dataType: "json",
                            url: `${API_URL}/menu_item/`,
                            method: "POST",
                            data: {
                                fk_site: sel_site.id,
                                pk_menu,
                                menu_item
                            },
                            success(response) {
                                if(response.success) {
                                    jsNotify('Успешно сохранено');
                                } else {
                                    jsNotify('Проблемы при сохранении', '', '', 'danger');
                                    console.error(response);
                                }

                                //если новый создан - обновить страницу для генерации нового пустого поля
                                if(response.isnew) {
                                    setTimeout(function(){
                                        location.reload()
                                    }, 500);
                                }
                            }
                        });
                    }
                });

                //создать nestable
                $("#nestable-json").nestable({
                    json: arrnest,
                    maxDepth: 2
                });

                //отслеживать изменения
                $('.dd').change(() => {
                    let saveSort = [];
                    let sorts = $('.dd').nestable('serialize');

                    for(let i = 0; i < sorts.length; i++) {
                        saveSort.push({
                            id  : sorts[i].id,
                            sort: i + 1 
                        });
                    }

                    $.ajax({
                        dataType: "json",
                        url: `${API_URL}/menu_sort/`,
                        method: "POST",
                        data: {
                            fk_site: sel_site.id,
                            pk_menu,
                            saveSort
                        },
                        success(response) {
                            if(response.success) {
                                jsNotify('Успешно сохранено');
                            } else {
                                jsNotify('Проблемы при сохранении', '', '', 'danger');
                                console.error(response);
                            }
                        }
                    });
                });
            },
            error(err) {
                alert(err);
            }
        });
    }
};