/**
 * выбор и подсветка элемента, блока
 */
document.getElementById('listenToFill').addEventListener("click", function(e) {
    let el = e.target;
    
    // не хочу рекурсивно искать
    if(!el.classList.contains('tofill')) {
        el = e.target.parentElement;
    }
    
    if(el.classList.contains('tofill')) {
        let sh = document.getElementsByClassName('showing');
        
        for(let i = 0; i < sh.length; i++) {
            sh[i].classList.remove('showing');
        }
        
        el.classList.add('showing');
    }
});

// сохранение
document.getElementById('save-mainPage').addEventListener("click", function() {
    const id_index_page = Number(getParamFromUrl('id_mp')); // из url брать
    
    let sh = document.getElementsByClassName('tofill');
    
    let jsonArr = []; // ну по порядку собрать в массив и на сохр
    
    let date = document.getElementById('date_mainPage').value;

    for(let i = 0; i < sh.length; i++) {
        jsonArr.push(parseInt(sh[i].id, 10) || 0);
    }
    
    if(!date.length) {
        alert('Не выбрана дата');
    } else {
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: {
                date,
                data: JSON.stringify(jsonArr),
                fk_site: sel_site.id,
                update: (id_index_page) ? 1 : 0,
                id_index_page
            },
            url: `${API_URL}/mainpage`,
            success: function(data) {
                if(data.success === true) {// переадресовать на обновление
                    jsNotify('Успешно сохранено');
                } else {
                    jsNotify('Проблемы сохранения', '', '', 'danger');
                }
            }
        });
    }
});

/*
 * вставить данные в блоки после загрузки страницы, при просмотре/обновлении главной страницы
 */
function setMainPageData(data) {
    const els = document.getElementsByClassName('tofill');
    
    for(let i = 0; i < els.length; i++) {
        els[i].id = data[i].pk_content;
        els[i].innerHTML = `<img class="gr-image-widget" data-pk_content="${data[i].pk_content}" src="${data[i].headimgsrc_content}"><div class="gr-text-widget">${data[i].title_content}</div>`;
    }
}

/**
 * загрузка данных по клику на выбор
 */
var onSetContentToBlock = function(t) {
    let el = document.getElementsByClassName('showing')[0];
    
    if(el) {
        el.id = $(this).attr('data-pk_content');
        el.innerHTML = `<img class="gr-image-widget" data-pk_content="${$(this).attr('data-pk_content')}" src="${$(this).attr('data-img_content')}"><div class="gr-text-widget">${$(this).attr('data-title_content')}</div>`;
    }
}

/**
 * после ajax снова вещать
 */
$(document).on('pjax:success', function() {
    $('.gr-set-content-to-block').unbind( "click", onSetContentToBlock );
    $('.gr-set-content-to-block').bind( "click", onSetContentToBlock );
});

// сразу повешить слушатель
$('.gr-set-content-to-block').bind( "click", onSetContentToBlock );