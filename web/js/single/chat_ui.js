/* global io, ioconnect */

"use strict";

window.socketonline = false;//метка онлайна(соединения с сервером)
let socket;

if(typeof io === 'undefined') {
    console.error('не указаны данные для подключения');
} else {
    socket = io.connect(ioconnect);

    //соединиться
    socket.on('connect', () => {
        socket.emit('connectListener', {'uniqid':window.uniqid});
    });

    //обновить уведомления у пользователей
    socket.on('updateNotify', (data) => {
        let id;
        if(!data) id = window.uniqid;
        else id = data.id;
        updateNotify(id);
    });

    //вывод в консоль ответа
    socket.on('console', (data) => {
        console.log(data);
    });

    //чтение сообщения
    socket.on('readMessage', (data) => {
        readMessageClient(data.idm, 'unread');
    });

    //чтение сообщения
    socket.on('readYourMessage', (data) => {
        readMessageClient(data.idm, 'unreadReceiver');
    });

    //пришло новое сообщение
    socket.on('getNewMessage', (data) => {
        getNewMessage(data.idm);
    });

    //закрыть или закрыто с той стороны
    socket.on('disconnect', () => {
        window.socketonline = false;
        checkOnline();
    });

    //успешно ли соединение
    socket.on('connectStatus', (data) => {
        if(data == "ok") {
            window.socketonline = true;
            afterConnectSocketio();
            checkOnline();
        } else {
            console.error('Проблема входа в сокет сервера');
        }
    });

    //уведомление
    socket.on('notify', (data) => {
        if(data.updateNotify !== undefined) updateNotify();//обновить плашку
        //нет данных для уведомления, нужно подумать, нужны ли они
        //jsNotify(strip_tags(data.message), data.title, data.url, data.type, data.target);
    });

    //уведомление, видное для всех(обновляемое)
    socket.on('notifyRefreshing', (data) => {
        $('#warning-alert-notify').removeClass('hidden');
        $('#warning-alert-notify').html(data);
    });
    //убрать уведомление
    socket.on('delnotifyRefreshing', () => {
        $('#warning-alert-notify').remove();
    });
}

// выполнение операций после соединения сокета
let afterConnectSocket = [];

function afterConnectSocketio() {
    for(let i = 0; i < afterConnectSocket.length; i ++) {
        afterConnectSocket[i]();
    }
}
// end выполнение операций после соединения сокета

/* --------------
 * функции хелперы
 */

/*
 * отправка сообщения newMessage('<p>34</p>', 1, 3)
 * @param {string} text - текст сообщения
 * @param {int} sender - ид отправитель
 * @param {int} receiver - ид получателя
 */
function newMessage(text, sender, receiver){
    if(!text || !sender || !receiver || text.length < 9) {
        console.error('Пустое сообщение');
        return false;
    }
    
    socket.emit('newMessage', { 'text' : text, 'sender' : sender, 'receiver' : receiver });
    
    return true;
}

/*
 * метка и чтении сообщения
 * @param {int} idm - ид сообщения
 */
function readMessage(idm){
    socket.emit('readMessage', {'idm' : idm});
}

/*
 * ajax обновление ленты с уведомлениями
 * @returns {html}
 */
function updateNotify() {
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: getupdatenotifyurl,
        success: function(data) {
            if(data.html.length) {
                Tinycon.setBubble(data.html.length);
                $('#notificationPlace .label').html(parseInt(data.html.length));
                $('#notificationPlace .dropdown-menu .menu').html('');

                $.each( data.html, function( key, value ) {
                    $('#notificationPlace .dropdown-menu .menu').append('<li>' + value + '</li>');
                });
                
                $('#notices-delete').show();
            }
        }
    });
}

/*
 * отображение онлайна графически
 */
function checkOnline(){
    if(window.socketonline == true){
        $('#userStatusWebN').children().first().removeClass('text-danger');
        $('#userStatusWebN').children().first().addClass('text-success');
        $('#userStatusWebN').children().last().html('Online');
    } else {
        $('#userStatusWebN').children().first().addClass('text-danger');
        $('#userStatusWebN').children().first().removeClass('text-success');
        $('#userStatusWebN').children().last().html('Offline');
    }
}
