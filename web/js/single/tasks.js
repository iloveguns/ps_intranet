/*
 * клик на кнопки типов задач
 * нужно сделать общедоступной и для любых контейнеров
 */
$('.btns-shows i').click(function(){
    let classClicked = $(this).attr('data-class');
    if(!classClicked) return;
    let hasClass = $(this).hasClass('active-fontaw');//определить повторное нажатие
    
    $('#tasks-list .item').show();
    $('.btns-shows i.active-fontaw').removeClass('active-fontaw');
    
    if(!hasClass) {
        $(this).addClass('active-fontaw');        
        $('#tasks-list .item:not(.' + classClicked + ')').hide();
    }
});

/*
 * сортировка задач - получение с сервера и перемешивание по полученным ид
 */
$('#sort_task').change(function(){
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: tasks_api_sort,
        data: {mode: $(this).val() },
        success: (data) => {
            $.each( data, function( key, value ) {
                //поднять строку выше
                $('#tasks-list ul').prepend($('#widget-task-id-' + value));
            });
        }
    });

    $.cookie('widget-task-sort', $(this).val(), { expires: 365, path: '/' });//на ближайший год поставить кукис
});