/*
* отправка ajax с параметрами
* selectedRows - информация на сервер
*/
function sendAjaxActions(url, data, confirmMessage){
    $.ajax({
       url: url,
       type: 'post',
       data: { data: data},
       beforeSend: function() {
           if(data.length == 0 || !confirm(confirmMessage)){
               return false;
           }
           gifLoader();
       }
    });
}

//показать всплывающее окно с сотрудниками отдела
function showCrmUsersList() {
    $('#crm-users-list-modal').modal('show');
}

//показать всплывающее окно с тегами отдела
function showCrmTagsList() {
    $('#crm-tags-list-modal').modal('show');
}

function showCrmCreateTask() {
    $('#crm-create-task-modal').modal('show');
}

$(document).ready(function() {
    //нажатие кнопки выбора сотрудника
    $('#crm-users-list-modal .btn').click(function(){
        let select = $('#crm-users-list-modal select[name=crm-users-list]');
        let id_sel_user = select.val();
        if(!id_sel_user){
            select.parent().addClass('has-error');
        } else {
            sendAjaxActions(window.act_url, {id_user: id_sel_user, selectedRows: window.act_data}, window.act_cmess);
            select.parent().removeClass('has-error');
        }
    });
    
    //теги
    $('#crm-tags-list-modal .btn').click(function(){
        let select = $('#crm-tags-list-modal select');
        let tags = select.val();
        if(!tags){
            select.parent().addClass('has-error');
        } else {
            sendAjaxActions(window.act_url, {tags: tags, selectedRows: window.act_data}, window.act_cmess);
            select.parent().removeClass('has-error');
        }
    });
});