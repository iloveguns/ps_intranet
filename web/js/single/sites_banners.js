let data_banner = [];

/**
 * выбор и подсветка элемента, блока
 */
document.getElementById('listenToFill').addEventListener("click", function(e) {
    let el = e.target;
    
    // не хочу рекурсивно искать
    if(!el.classList.contains('tofill')) {
        el = e.target.parentElement;
    }
    
    if(el.classList.contains('tofill')) {
        let sh = document.getElementsByClassName('showing');
        
        for(let i = 0; i < sh.length; i++) {
            sh[i].classList.remove('showing');
        }
        
        el.classList.add('showing');
        
        clearDataBanner();
    }
    
    // найти инфу о баннере
    const banner_pos = parseInt(el.getAttribute('data-id'));
    
    if(banner_pos) {
        $.ajax({
            type: 'get',
            dataType: 'json',
            data: {
                banner_pos,
                fk_site: sel_site.id
            },
            url: `${API_URL}/banner`,
            success: function(data) {
                if(data.success === true) {// переадресовать на обновление
                    $('#info_banner').html('');
                    
                    data_banner = data.data;

                    for(let i = 0; i < data.data.length; i++) { 
                        $('#info_banner').append(`<li data-id='${i}' onclick="setDataBanner(${i})">Дата с '${data.data[i].date_since}', дата до '${data.data[i].date_to}'<span onclick="deleteBanner(${i})" class="glyphicon glyphicon-remove text-red" aria-hidden="true"></span></li>`);
                    }

                } else {
                    jsNotify('Проблемы загрузки', '', '', 'danger');
                }
            }
        });
    }
});

// сохранение
document.getElementById('save_banner').addEventListener("click", function() {
    let el = document.getElementsByClassName('showing');
    
    if(el.length < 1) return false;
    
    const banner_pos = parseInt(el[0].getAttribute('data-id'));
    
    const date_since = $('#date_since').val();
    
    const date_to = $('#date_to').val();
    
    const script_banner = document.getElementById('script_banner').value;
    
    const id_banner = Number(document.getElementById('id_banner').value);
    
    if(!date_since.length || !date_to.length || !script_banner.length) {
        return false;
    }
    
    $.ajax({
        type: 'post',
        dataType: 'json',
        data: {
            banner_pos,
            date_since,
            date_to,
            script_banner,
            id_banner,
            fk_site: sel_site.id
        },
        url: `${API_URL}/banner`,
        success: function(data) {
            if(data.success === true) {
                clearDataBanner();
                jsNotify('Успешно сохранено');
            } else {
                jsNotify('Проблемы сохранения', '', '', 'danger');
            }
        }
    });
});

/**
 * удаление баннера по ид
 * @param {int} i - позиция баннера в массиве баннеров
 * @returns {undefined}
 */
function deleteBanner(i) {
    if(!confirm('Удалить баннер?')) return;
    
    $.ajax({
        type: 'delete',
        dataType: 'json',
        data: {
            id_banner : data_banner[i].id_banner,
            fk_site: sel_site.id
        },
        url: `${API_URL}/banner`,
        success: function(data) {
            if(data.success === true) {
                if(data.affectedRows === 0) {
                    jsNotify('Уже удалено');
                } else {
                    jsNotify('Успешно удалено');
                }
                
                document.querySelector(`li[data-id='${i}']`).remove();
                clearDataBanner();
            } else {
                jsNotify('Проблемы удаления', '', '', 'danger');
            }
        }
    });
}

function setDataBanner(i) {
    $('#date_since').val(data_banner[i].date_since);
    
    $('#date_to').val(data_banner[i].date_to);
    
    document.getElementById('script_banner').value = $("<div/>").html(data_banner[i].script_banner).text();
    
    document.getElementById('id_banner').value = data_banner[i].id_banner;
    
    $('#clear_data_banner').show();
}

// сбросить введенные данные
document.getElementById('clear_data_banner').addEventListener("click", function(e) {
    clearDataBanner();
});

function clearDataBanner() {
    $('#date_since').val('');
    
    $('#date_to').val('');
    
    document.getElementById('script_banner').value = '';
    
    document.getElementById('id_banner').value = '';
    
    $('#clear_data_banner').hide();
}