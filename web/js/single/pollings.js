/*
 * @param {int} min_checked - минимальное кол-во выбранных вариантов
 * @param {int} max_checked - максимальное кол-во выбранных вариантов
 */

/* global api_url, stage, stagetype_radio, stagetype_checkbox, stagetype_multiple_text, stagetype_textarea, polling_form_id, polling_url, polling_html_url, min_checked, polling_model_answer, max_checked */

let ctx = document.getElementById("chart"),//canvas для графиков
    labeltext = [];//доп информация в голосовании
if(ctx) ctx = ctx.getContext("2d");

const backgroundColor = [
    "#2ecc71",
    "#3498db",
    "#95a5a6",
    "#9b59b6",
    "#f1c40f",
    "#e74c3c",
    "#34495e"
];//цвета для графиков по порядку идут

/*
 * клик кнопки показа данных, получение данных и вывод информации в зависимости от типа
 */
$('#show-results').click(() => {
    let btn = $('#show-results');
    let labels, data;
    
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: api_url,
        data: { stage: stage },
        success: (api_data) => {
            switch (api_data.type) {
                case stagetype_radio:                    
                    labels = api_data.preanswers,
                    data = api_data.count_answers;
                    
                    $("#chart").removeClass('hidden');
                    
                    new Chart(ctx, { 
                        type: "pie",
                        data: {
                            labels: labels,
                            datasets: [{
                                backgroundColor: backgroundColor,
                                data: data
                            }]
                        }
                    });
                
                    break;
                    
                case stagetype_checkbox:
                    labels = api_data.preanswers,
                    data = api_data.count_answers;
                    
                    $("#chart").removeClass('hidden');
                        
                    new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: labels,
                            datasets: [{
                                label: 'График',
                                data: data,
                                backgroundColor: backgroundColor[0]
                            }]
                        }
                    });
                  
                    break;
                    
                case stagetype_multiple_text:
                case stagetype_textarea:
                    $.each(api_data.answers, function(index, answers) {
                        for (let i = 0, len = answers.length; i < len; i++) {
                            $('#for_text_answer').append('<div class="well">' + answers[i] + '</div>');
                        }
                        $('#for_text_answer').append('<hr>');
                        $('#text_answer').removeClass('hidden');
                    });
                    
                    break;
            }
            
            if(api_data.add_answers) {//если есть доп текст - вывести в конце
                for (let i = 0, len = api_data.add_answers.length; i < len; i++) {
                    $('#for_add_text').append('<div class="well">' + api_data.add_answers[i] + '</div>');
                }
                $('#add_text').removeClass('hidden');
            }
            
            btn.remove();
        },
        error: (err) => {
            console.error(err);
        }
    });
});

/*
 * обработка нажатия кнопки голосования
 */
function regBeforeSubmit() {
    $('#' + polling_form_id).yiiActiveForm();//динамически сделать форму доступной для событий
    $('#' + polling_form_id).on('beforeSubmit', beforeSubmitForm);
}
regBeforeSubmit();

function beforeSubmitForm() {
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: polling_url,
        data: $('#' + polling_form_id).serialize(),
        success: function(data){
            if(data.success) {
                if(data.next) {//есть еще
                    nextStage();
                } else {//конец голосования
                    $('#polling_root').html('<div class="alert alert-success" role="alert">Голосование завершено</div>');
                }                
            } else {
                modelErrorShow(data.errors);
            }
        }
    });
    return false;
}

/*
 * получить данные для следующего этапа
 */
function nextStage() {
    $.ajax({
        dataType: 'json',
        method: 'POST',
        url: polling_html_url,
        data: { stage: nextPollingStage },
        success: function(data) {
            $('#polling_root').html(data.html);
            
            //зарегать разное
            regBeforeSubmit();
            $('.multiple-data div[name=clone-multiple]').click(function(){
                cloneMultiple($(this).attr('id'));
            });
            
            nextPollingStage = data.next;
        }
    });
}


//проверить минимальное значение
if(typeof min_checked !== 'undefined') {
    labeltext.push('минимум: ' + min_checked);
    
    $('#' + polling_form_id).on('beforeValidate', function (e) {
        if($('#' + polling_model_answer + ' input[type=checkbox]:checked').length < min_checked) {
            modelErrorShow({'e':'необходимо выбрать минимум ' + min_checked + ' варианта ответов'});
            return false;
        }
        return true;
    });
}

//проверить максимальное значение
if(typeof max_checked !== 'undefined') {
    labeltext.push('максимум: ' + max_checked);
    
    $('#' + polling_model_answer + ' input[type=checkbox]').on('change', function() {
        if($('#' + polling_model_answer + ' input[type=checkbox]:checked').length > max_checked) {
            this.checked = false;
        }
    });
}

$('#label-add-data').html(labeltext.join(', '));
$('#label-add-data').removeClass('hidden');