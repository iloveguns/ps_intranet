/*
 * включить все
 */
function structStartAll(){
    setOnClickStruct();
    setOnClickSubmitBtnStruct();
    setBtnRemoveAllStruct();
    setBtnRemoveStruct();
}
/*
 * решено, что структура будет выбираться на уровне сотрудников
 * отношение организаций, дивизионов и отделов остается под сомнением * 
 */
function setOnClickStruct(){
    $('.dd3-content').click(function(){//клик на элемент
        var id = $(this).parent().attr('data-id');
        var dataStruct = $(this).parent().attr('data-struct');

        if($(this).hasClass('selected')){//метка того, выбран ли этот элемент уже
            $("#selectedStructuresList li[data-struct="+dataStruct+"][data-id="+id+"]").remove();
            $("#selectedStructuresListForm li[data-struct="+dataStruct+"][data-id="+id+"]").remove();//удалить в выбранном
        }
        else{
            $('#selectedStructuresList').nestable({});
            $('#selectedStructuresListForm').nestable({});
            $('#selectedStructuresList').append($(this).parent().clone());//выбранные
            $('#selectedStructuresListForm').append($(this).parent().clone());//выбранные в форме для просмотра

            setBtnRemoveStruct();
        }

        toggleSelectStruct($(this).parent());
    });
}

/*
 * клик на кнопку
 */
function setOnClickSubmitBtnStruct(){
    $('#getSelectedUnits').click(function(){//функция выбора структур. возвращает json 
        var json_struct = {'organization':'','divizion':'','department':'','user':''};
        $('#selectedStructuresList .dd-item').each(function(i){
            json_struct[$(this).attr('data-struct')] += $(this).attr('data-id')+',';
        });
        if(json_struct.organization.slice(-1) == ',') json_struct.organization = json_struct.organization.slice(0,-1);
        if(json_struct.divizion.slice(-1) == ',') json_struct.divizion = json_struct.divizion.slice(0,-1);
        if(json_struct.department.slice(-1) == ',') json_struct.department = json_struct.department.slice(0,-1);
        if(json_struct.user.slice(-1) == ',') json_struct.user = json_struct.user.slice(0,-1);
        var all = JSON.stringify(json_struct);

        $('#selectedStructuresData').val(all);
        $('#myModal').modal('hide');
    });
}

/*
 * кнопка удалить все выбранные структуры
 */
function setBtnRemoveAllStruct(){
    $('#struct-delete-all').click(function(){
        $('.selected').removeClass('selected');
        $('#selectedStructuresList li').remove();
        $('#selectedStructuresListForm li').remove();
    });
}

/*
 * выбрать структуру по данным
 */
function toggleSelectStruct(target){
    target.children('.dd3-content').toggleClass('selected');
    target.children('.dd-list').children().each(function(i,item){//пройтись внутри и всем понавешать класс выбран
        $(this).children('.dd3-content').toggleClass('selected');
        //внутренние вручную находить
        $(this).children('.dd3-content').parent().children('.dd-list').children().each(function(i,item){//пройтись внутри и всем понавешать класс выбран
            $(this).children('.dd3-content').toggleClass('selected');
            $(this).children('.dd3-content').parent().children('.dd-list').children().each(function(i,item){//пройтись внутри и всем понавешать класс выбран
                $(this).children('.dd3-content').toggleClass('selected');
            });
        });
    });
}

/*
 * удаление выбранной структуры(нажатие крестика)
 */
function setBtnRemoveStruct(){
    $( ".structure-remove" ).on( "click", function() {
        var id = $(this).parent().parent().attr('data-id');
        var dataStruct = $(this).parent().parent().attr('data-struct');
        $('.dd-'+dataStruct+'[data-id='+id+']').children('.dd3-content').removeClass('selected');//убрать выделение            
        $(this).parent().parent().remove();//удалить в списке
        $('#selectedStructuresListForm .dd-'+dataStruct+'[data-id='+id+']').remove();//удалить в форме
    });
}

/*
 * копирование структуры
 * например при обновлении проекта создается структура и ее надо скопировать в модальное окно
 * from - Jquery откуда
 * to - Jquery куда
 */
function copyStruct(from, to){
    to.append(from.clone());//выбранные
}

/*
 * поиск в структуре сотрудников
 * в точном совпадении символов
 * searchString - строка
 */
function searchStruct(searchString){
    $('.dd-list').show();
    
    $('.structure-list-main .dd-department').hide();
    $('.structure-list-main .dd-divizion').hide();
    $('.structure-list-main .dd-user').hide();

    $( ".struct-name:contains("+searchString+")" ).parent().parent().parent().parent().show();
    $( ".struct-name:contains("+searchString+")" ).parent().parent().parent().parent().parent().parent().show();
    $( ".struct-name:contains("+searchString+")" ).parent().parent().parent().parent().parent().parent().parent().parent().show();
    
    var searchstringtwo = capitalizeFirstLetter(searchString);
    $( ".struct-name:contains("+searchstringtwo+")" ).parent().parent().parent().parent().show();
    $( ".struct-name:contains("+searchstringtwo+")" ).parent().parent().parent().parent().parent().parent().show();
    $( ".struct-name:contains("+searchstringtwo+")" ).parent().parent().parent().parent().parent().parent().parent().parent().show();
}
