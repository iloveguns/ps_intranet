<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\modules\intranet\models\Events;
use app\models\FunctionModel;

class CalendarController extends Controller {
    public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions'=>['get-calendar-ajax', 'jsoncalendar'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'get-calendar-ajax'  => ['post'],
                ],
            ],
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON
                ],
            ]
        ];
    }
    
    /*
     * ajax задачи в календарь главной
     */
    public function actionGetCalendarAjax() {        
        $date = Yii::$app->request->post()['date'];
        if(!$date) throw new \yii\base\InvalidParamException();
        
        $returnArr = [];
        
        $tasks = Yii::$app->user->identity->getEvents($date);
        
        if($tasks){
            foreach ($tasks as $task) {
                $tempTask = ['link' => $task->htmlLink];                
                $tempTask['event'] = true;
                $returnArr[] = $tempTask;
            }
        }
        
        return $returnArr;
    }
    
    /*
     * вывод в календарь всго что нужно (события, задачи)
     * по месяцам хапать инфу
     * по дням недели определять, а не по числам.
     * событие стоит 6(пнд, ежемес), значит след событие например 8(пнд), а не 6(сб)
     */
    public function actionJsoncalendar($id, $start=NULL, $end=NULL, $_=NULL){        
        $uniqid = 0;//просто уникальный ид у каждого события
        
        $events = [];
        
        $found_tasks = [];
        
        if($id == Yii::$app->user->id){//себе
            $found_events = Yii::$app->user->identity->getEvents('all', $start, $end);
            
            //$found_tasks = Yii::$app->user->identity->getTasksEndInPeriod($start, $end); // люди не оценили
        } else { //для других только публичные
            $user = \app\modules\user\models\User::findOne($id);
            $found_events = $user->getEvents(true, $start, $end);
        }
        
        // запушить задачи
        foreach ($found_tasks as $task) {
            $event = new \yii2fullcalendar\models\Event();//новое событие календаря
            $event->id = ++$uniqid;
            $event->className = 'btn-danger'; // ну чтоб заметно было, в общей стилистике
            $event->url = $task->url;
            $event->title = substr(Yii::$app->formatter->asTime($task->end_date),0,-3).' '.$task->title;
            $event->start = $task->end_date;
            $event->end = $task->end_date;
            
            $events[] = $event;
        }
        
        unset($found_tasks);
        // end запушить задачи
        
        // запушить события
        foreach ($found_events as $task) {
            $initStart = $task->start_date;
            if($task->start_date >= $start && $task->end_date <= $end) {
                $events[] = $this->createCalendarEvent($task, ++$uniqid);//основное событие всегда добавляется - отсеивать сразу
            }
            
            //указано повторение события
            if($repeat = json_decode($task->json_repeat, TRUE)) {
                $start_date = $task->start_date;
                $end_date = $task->end_date;
                $modif = '';
                $count = 0;
                $forever = isset($repeat['forever']);//всегда ли повторять
                $timeStart = explode(' ', $task->start_date);
                $timeEnd = explode(' ', $task->end_date);
                
                switch ($repeat['select']) {
                    case Events::REPEAT_WEEKLY:
                        if($forever) {
                            $count = 6;
                            
                            //начинать с данного промежутка, с того дня недели
                            $weekDay = FunctionModel::getWeekday($task->start_date);
                            $daystart = date(Yii::$app->params['saveModelDateTime'], strtotime("next $weekDay", strtotime($start)));
                            $daystart = FunctionModel::dateModify($daystart, "-7 days", 'Y-m-d');
                            
                            $start_date = $daystart. ' '.$timeStart[1];
                            $end_date = $daystart . ' '.$timeEnd[1];
                        }
                        $modif = 'week';
                        break;
                    
                    case Events::REPEAT_DAILY:
                        if($forever) {
                            $count = 6*7;

                            //отсчитать каждый день промежутка
                            $daystart = date(Yii::$app->params['saveModelDateTime'], strtotime($start));
                            $daystart = FunctionModel::dateModify($daystart, "-1 day", 'Y-m-d');
                            $start_date = $daystart. ' '.$timeStart[1];
                            $end_date = $daystart . ' '.$timeEnd[1];
                        }
                        $modif = 'day';
                        break;
                    
                    case Events::REPEAT_DATE:
                        $timeStart = explode(' ', $task->start_date);
                        $timeEnd = explode(' ', $task->end_date);

                        $task->start_date = $repeat['date'] . ' '.$timeStart[1];
                        $task->end_date = $repeat['date'] . ' '.$timeEnd[1];
                        if($task->end_date >= $start && $task->end_date <= $end){//дата соотв диапазону
                            $events[] = $this->createCalendarEvent($task, ++$uniqid);
                        }
                        continue;
                }
                
                if(isset($repeat['count'])) {//указано четкое кол-во повторений
                    $count = $repeat['count'];
                }
                
                //просчитать и выдать в календарь
                for($i = 1; $i <= $count; $i ++) {
                    $task->start_date =  FunctionModel::dateModify($start_date, '+'.$i.' '.$modif);
                    if($task->start_date <= $initStart) continue;//не ранее, чем событие начнется
                    $task->end_date =  FunctionModel::dateModify($end_date, '+'.$i.' '.$modif);
                    
                    if($task->start_date >= $start && $task->end_date <= $end) {//дата соотв диапазону
                        $events[] = $this->createCalendarEvent($task, ++$uniqid);
                    }
                }
            }
        }
        
        return $events;
    }
    
    protected function createCalendarEvent($task, $uniqid) {
        $event = new \yii2fullcalendar\models\Event();//новое событие календаря
        $event->id = $uniqid;
        $event->url = $task->url;//url на который можно сразу перебрасывать
        //$event->className = 'classname';//css класс
        $event->title = substr(Yii::$app->formatter->asTime($task->start_date),0,-3).' - '.substr(Yii::$app->formatter->asTime($task->end_date),0,-3).' '.$task->title;
        //$event->allDay = true;//типа на весь день
        $event->start = $task->start_date;//время начала события
        $event->end = $task->end_date;//время конца
        
        return $event;
    }
}
