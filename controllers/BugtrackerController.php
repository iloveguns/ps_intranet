<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class BugtrackerController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions'=>['change-watch'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }
    
    /*
     * сохранение выполнения 
     */
    public function actionChangeWatch() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $res = ['success' => false];
        
        $post = Yii::$app->request->post();
        
        if($post['value'] == 'true'){
            $value = 1;
        } else {
            $value = NULL;
        }
        
        $q = Yii::$app->db->createCommand()->update(\app\modules\intranet\models\Bugtracker::tableName(), ['admin_response' => $value, 'update_date' => \app\models\FunctionModel::getDateTimestamp(), 'done_date' => \app\models\FunctionModel::getDateTimestamp()], '`pk` = '.(int)$post['item_id'])->execute();
        if($q){
            $res = [
                'success' => true
            ];
        }
        
        return $res;
    }
}
