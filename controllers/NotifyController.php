<?php

namespace app\controllers;

use Yii;
use app\models\UnreadNotice;
use app\modules\crm\CrmModule;
/*
 * контроллер для уведомлений
 */
class NotifyController extends \app\components\Controller {
    public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions'=>['get-list', 'subscribe', 'unsubscribe', 'middle-notice', 'delete-all-notices'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'middle-notice' => ['get'],
                    'get-list'  => ['post'],
                    'subscribe'  => ['post'],
                    'unsubscribe'  => ['post'],
                    'delete-all-notices' => ['post']
                ],
            ],
        ];
    }
    
    /*
     * промежуток между переходом по ссылке в уведомлении, засчитывает просмотр
     * может не быть конечной страницы - тогда отправлять обратно
     */
    public function actionMiddleNotice($id) {
        if($notice = UnreadNotice::findOne($id)) {//найдено уведомление - найти модель сущности
            UnreadNotice::setReadedNotice($notice->item_id, $notice->class);
            
            //найти модель
            if($findModel = \app\modules\user\models\Units::getModelByNameid($notice->class, $notice->item_id)) {
                $redirect_url = $findModel->getUrl();
                
                //дополнить наконец ссылку ид комментария
                if($notice['to_name'] == \app\models\Comments::getName() && $notice['to_id']) {
                    $redirect_url .= '#comment-' . $notice['to_id'];
                }
                
                /**
                 * для црм(задач) - определить отдел и перевести на него
                 * по ид компании или контакта определить отдел
                 */
                if($notice->class === \app\modules\crm\models\CrmTasks::tableName()) {
                    if($findModel->fk_crmcompany) {
                        CrmModule::setCookieDep($findModel->fkCrmcompany->fk_department);
                    } else if($findModel->fk_crmcontact) {
                        CrmModule::setCookieDep($findModel->fkCrmcontact->fk_department);
                    }
                }
                
                $this->redirect($redirect_url);
            } else {
                $this->back();
            }
        } else {
            return $this->redirect('/');
        }
    }
    /*
     * получить уведомления ,так ну уж очень удобно
     * @param {int} $id - ид пользователя
     */
    public function actionGetList($id = NULL) {
        $this->jsonResponse();
        
        if($id === NULL) $id = Yii::$app->user->id;
        
        $returnHtml = [];
        
        if($notices = UnreadNotice::ApiNoticesUser($id, true)) {
            foreach ($notices['notices'] as $notice) {
                $returnHtml[] = \app\modelsView\ViewUnreadNotice::newHtmlNotify($notice);
            }
        }
        
        return ['html' => $returnHtml];
    }
    
    /*
     * удалить уведомления пользователя
     */
    public function actionDeleteAllNotices() {
        $this->jsonResponse();
        
        if(UnreadNotice::setReadeAllNoticeByIdUser(Yii::$app->user->id)){
            return ['success' => true, 'message' => Yii::t('app', 'All Notices Deleted')];
        }
        
        return ['success' => false];
    }
    
    /*
     * подписка на уведомления
     */
    public function actionSubscribe() {
        $success = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $post = Yii::$app->request->post();
        
        $class = $post['class'];
        $item_id = (int)$post['id'];
        
        if(!$class || !$item_id) throw new \yii\base\InvalidParamException('Нужны параметры');
        
        $insert = Yii::$app->db->createCommand()->insert('subscribes', [
            'class' => $class,
            'item_id' => $item_id,
            'fk_user' => Yii::$app->user->id,
        ])->execute();
        
        if($insert){
            $success = 'subscribed';
        }        
        
        return ['success' => $success];
    }
    
    /*
     * отписка от уведомлений
     */
    public function actionUnsubscribe() {
        $success = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;        
        $post = Yii::$app->request->post();
        
        $class = $post['class'];
        $item_id = (int)$post['id'];
        
        if(!$class || !$item_id) throw new \yii\base\InvalidParamException('Нужны параметры');
        
        $deleted = Yii::$app->db->createCommand('DELETE FROM `subscribes` WHERE class = :class AND item_id = :item_id AND fk_user = :fk_user')
            ->bindValue(':class', $class)
            ->bindValue(':item_id', $item_id)
            ->bindValue(':fk_user', Yii::$app->user->id)
            ->execute();
        
        if($deleted){
            $success = 'unsubscribed';
        }        
        
        return ['success' => $success];
    }
}
