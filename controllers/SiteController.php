<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\modules\user\models\UserRegister;
use app\modules\user\UserModule;
use app\models\UnreadNotice;
use app\modules\user\models\User;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }
    
    public function actionFb() {
        $fb = new \Facebook\Facebook([
            'app_id' => '167482817214623',
            'app_secret' => 'fb5aea8cf7723846b61baa28af30ebe5',
            'default_graph_version' => 'v2.2',
            ]);

          $helper = $fb->getCanvasHelper();

          try {
            $accessToken = $helper->getAccessToken();
          } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
          } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
          }

          if (! isset($accessToken)) {
            echo 'No OAuth data could be obtained from the signed request. User has not authorized your app yet.';
            exit;
          }

          // Logged in
          echo '<h3>Signed Request</h3>';
          var_dump($helper->getSignedRequest());

          echo '<h3>Access Token</h3>';
          var_dump($accessToken->getValue());
    }
    
    public function actionTestemail() {
        var_dump(\app\helpers\Mail::send('slayermass@mail.ru', 'тестовое письмо', ['text' => 'текст внутри'], '@app/mail/notify/notify'));
    }

    /*
     * вход в систему
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    
    /*
     * восстановление пароля
     */
    public function actionRequestPasswordReset()
    {
        $model = new \app\models\PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Password Will Send to your email'));

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Problemes send password email'));
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /*
     * выход из системы
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    public function actionRegister()
    {
        $model = new UserRegister();
        UserModule::registerTranslations();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            foreach (User::getAdmins() as $admin) {
                //отправить админам уведомление
                $notice = new UnreadNotice();
                $notice->class = $model->getName();
                $notice->item_id = $model->id_user;
                $notice->id_user = $admin;//ид админа
                $notice->type = UnreadNotice::NEW_REGISTER_USER;
                $notice->save();
            }
            //\Yii::$app->getSession()->setFlash('success', 'Вы отправили свои данные админу. Ждите подтверждения');
            return $this->redirect(['register', 'success' => true]);
        } else {
            return $this->render('register', [
                'model' => $model,
            ]);
        }
    }
}
