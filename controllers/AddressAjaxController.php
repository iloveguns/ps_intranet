<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\modules\crm\models\AddressCountry;
use app\modules\crm\models\AddressStreet;
use app\modules\crm\models\AddressCity;
use yii\helpers\ArrayHelper;

/*
 * контроллер для сохранения отчетов
 */
class AddressAjaxController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions'=>['getstreet', 'getcitys'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }
    
    /*
     * отдать улицы по ид города
     */
    public function actionGetstreet() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $res = ['success' => false];
        
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {                
                $div_id = $parents[0];//ид города
                if(!$div_id) return $res;
                $model = AddressCity::findOne($div_id);
                $div = ArrayHelper::toArray($model->addressStreets);
                $div = array_map(function($div) {
                    return array(
                        'id' => $div['pk_street'],
                        'name' => $div['name_street']
                    );
                }, $div);
                echo \yii\helpers\Json::encode(['output'=>$div, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
        
        return $res;
    }
    
    /*
     * отдать города по ид страны
     */
    public function actionGetcitys() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $res = ['success' => false];
        
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $div_id = $parents[0];//ид города
                if(!$div_id) return $res;
                $model = AddressCountry::findOne($div_id);
                $div = ArrayHelper::toArray($model->addressCitys);
                $div = array_map(function($div) {
                    return array(
                        'id' => $div['pk_city'],
                        'name' => $div['name_city']
                    );
                }, $div);
                echo \yii\helpers\Json::encode(['output'=>$div, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
        
        return $res;
    }
}
