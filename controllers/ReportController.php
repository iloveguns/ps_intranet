<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\modules\intranet\models\Tasks;
use app\models\EntityReport;
use app\models\UnreadNotice;

/*
 * контроллер для сохранения отчетов
 */
class ReportController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions'=>['save-report', 'task-change-author'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }
    
    /**
     * смена автора у ЗАДАЧИ + коммент + уведомления
     */
    public function actionTaskChangeAuthor() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $post = Yii::$app->request->post();
        
        $pk_task = (int)$post['pk_task'];
        $id_author = (int)$post['id_author'];
        
        if(!empty($pk_task) && !empty($id_author)) {
            $old_author = Yii::$app->db->createCommand('SELECT `lastname`,`name` FROM ' . Tasks::tableName() . ' LEFT JOIN ' . \app\modules\user\models\User::tableName() . ' ON id_author = id_user  WHERE pk_task = '. $pk_task)->queryOne();
            $old_author = $old_author['lastname'] . ' ' . $old_author['name'];
            
            $new_author = Yii::$app->db->createCommand('SELECT `lastname`,`name` FROM ' . \app\modules\user\models\User::tableName() . ' WHERE id_user = '. $id_author)->queryOne();
            $new_author = $new_author['lastname'] . ' ' . $new_author['name'];
                        
            if(Yii::$app->db->createCommand()->update(Tasks::tableName(), ['id_author' => $id_author], 'pk_task = '. $pk_task)->execute()) {
                $ch = curl_init("http://".Yii::$app->params['ipServerWebSocket'].":".Yii::$app->params['ipServerApiPort']."/api/syscomment");
                curl_setopt_array($ch, array(
                    CURLOPT_POST => TRUE,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json'
                    ),
                    CURLOPT_POSTFIELDS => json_encode([
                        'text' => "Изменен автор с '$old_author' на '$new_author'",
                        'item_id' => $pk_task,
                        'item_class' => Tasks::getName(),
                        'excl_users' => Yii::$app->user->id,
                        'incl_users' => $id_author
                    ])
                ));            
                curl_exec($ch);
            }
            
            return ['success' => true];
        }
        
        return ['success' => false];
    }
    
    /*
     * загрузка файлов
     * и сохранение данных в таблицу
     */
    public function actionSaveReport() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $post = Yii::$app->request->post();
        
        $model = new EntityReport();
        $model->load($post);
        
        if(isset($post['scenario']) && $post['scenario'] == 'updatetext') {//обновить отчет
            //найти последний, который и должен редактироваться
            $modelu = EntityReport::find()->where(['class' => $model->class, 'item_id' => $model->item_id])->orderBy('id_report DESC')->one();
            $modelu->text_report = $model->text_report;
            if($modelu->update()){
                $res = [
                    'success' => true,
                ];
                return $res;
            }
        } else {
            if($model->text_report){//создание отчета - сохранить
                //создать комментарий дополнительно
                $comment = new \app\models\Comments();
                $comment->item_id = $model->item_id;
                $comment->class = $model->class;
                $comment->id_user = Yii::$app->user->id;
                $comment->text = htmlentities('<p>Отчет:</p>' . $model->text_report);
                $comment->save();
                
                $tmp = $model->save();
                $status = 'report';
            }
            elseif($model->text_deniedreport){//отмена отчета - обновить последнюю запись
                $modelUpdateEntityAll = EntityReport::findAll(['class' => $model->class, 'item_id' => $model->item_id]);
                $modelUpdateEntity = array_pop($modelUpdateEntityAll);

                $modelUpdateEntity->text_deniedreport = $model->text_deniedreport;

                $tmp = $modelUpdateEntity->update();
                $status = 'deniedreport';
            }
            else{//если ни того, ни другого нет - ошибка
                $res = [
                    'success' => false,
                    'data' => [Yii::t('app/models', 'EntityReportError texts not set')],
                ];
                return $res;
            }
        }
        
        if($tmp){
            $res = [
                'success' => true,
            ];
        }
        else{
            $res = [
                'success' => false,
                'data' => $model->getErrors(),
            ];
        }
        
        $c = explode('\\', $model->class);//php 7
        $class = array_pop($c);//последнее слово класса модели
        
        switch ($class) {
            case Tasks::getName()://задачи
                $modelUpdate = Tasks::findOne($model->item_id);
                $modelUpdate->status = ($status == 'report') ? Tasks::STATUS_REPORT : Tasks::STATUS_DENIEDREPORT;
                break;

            default:
                $res = [
                    'success' => false,
                    'data' => [Yii::t('app/models', 'EntityReportError class not set')],
                ];
                return $res;
        }
        
        $modelUpdate->update();
        
        if($res['success'] === true){//сохранено
            UnreadNotice::saveByIds($modelUpdate, UnreadNotice::CHANGE_STATUS, false, true);
        }
        
        return $res;
    }
}
