<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\modules\intranet\models\Tasks;
use app\models\EntityUserDone;

/*
 * контроллер для сохранения метки выполнения и комментария(задач, подзадач, проектов и тд)
 */
class UserDoneController extends Controller {
    public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions'=>['executer-complete'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }
    
    /*
     * установить выполнение задачи сотрудником+коммент
     * @return arr json
     */
    public function actionExecuterComplete() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $model = new EntityUserDone();
        $model->load(Yii::$app->request->post());
        
        if(!$model->complete){//сохранить
            $model->complete = EntityUserDone::TASK_COMPLETE;
            if($model->save()){
                $res = [
                    'success' => true,
                    'mode' => 'save',
                    'id' => $model->id_done,
                    'text' => $model->text,
                    'date' => Yii::$app->formatter->asDatetime($model->date), // время выполнения, сразу дать
                    'user' => \app\modules\user\models\User::getAll($model->id_user, '{lastname} {name}')
                ];            
            } else {
                $res = [
                    'success' => FALSE,
                    'data' => $model->getErrors(),
                ];
            }
        } else {//удалить
            if($model = $model->findOne($model->id_done)->delete()){
                $res = [
                    'success' => true,
                    'mode' => 'delete',
                ];
            }else{
                $res = [
                    'success' => FALSE,
                    'data' => $model->getErrors(),
                ];
            }
            
        }
        return $res;
    }
}
