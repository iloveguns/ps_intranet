<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\modules\intranet\models\Tasks;
use app\models\EntityReport;

/*
 * контроллер для сохранения отчетов
 */
class ChangeWatchController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions'=>['change-watch'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }
    
    /*
     * загрузка файлов
     * и сохранение данных в таблицу
     */
    public function actionChangeWatch() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $res = ['success' => false];
        
        $post = Yii::$app->request->post();
        
        $q = Yii::$app->db->createCommand()->update('relation_to_user', ['watcher' => (int)$post['value']], '`id_user` = '.$post['id_user'].' AND `class` = "'.$post['classname'].'" AND `item_id` = '.$post['item_id'])->execute();
        if($q){
            $res = [
                'success' => true
            ];
        }
        
        return $res;
    }
}
