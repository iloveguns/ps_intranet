<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\UploadFiles;
/*
 * контроллер для файловых операций(общий)
 */
class FileController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions'=>['upload', 'file-upload', 'delete-by-id'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }
    
    /*
     * загрузка файлов
     * и сохранение данных в таблицу
     * UploadFiles::uploadFiles()
     */
    public function actionFileUpload($id, $name, $class) {
        $model = new UploadFiles;
        $model->item_id = (int)$id;
        $model->id_user = Yii::$app->user->id;
        $model->class = $class;
        
        $imageFile = \yii\web\UploadedFile::getInstanceByName('user_agent');
        $model->name = $imageFile->name;
        $directory = Yii::getAlias('@webroot') . Yii::$app->params['uploadFilesFolder'] .DIRECTORY_SEPARATOR;
        
        if ($imageFile) {
            $uid = uniqid(time(), true);
            $fileName = $uid . '.' . $imageFile->extension;
            $filePath = $directory . $fileName;
            $model->path = Yii::$app->params['uploadFilesFolder'] . DIRECTORY_SEPARATOR . $fileName;
            $model->save();
            if ($imageFile->saveAs($filePath)) {
                return \yii\helpers\Json::encode([
                    'response' => 'ok'
                ]);
            }
        }
    }
    
    /*
     * выгрузка файлов с сервера
     */    
    public function actionUpload($id_file) {
        $model = UploadFiles::findOne($id_file);
        
        if(!$model) throw new \yii\web\NotFoundHttpException(Yii::t('app/models', 'File not found'));
        
        if(ini_get('zlib.output_compression'))
            ini_set('zlib.output_compression', 'Off');

        $file_extension = strtolower(substr(strrchr($model->name,"."),1));
        $pathfile = \Yii::getAlias('@webroot').$model->path;
        
        if (!file_exists($pathfile)) throw new \yii\web\NotFoundHttpException(Yii::t('app/models', 'File not found'));
        
        //все что можно открыть в браузере - открыть(nginx итак открывает)
        switch( $file_extension )
        {
            case "pdf": 
            case "gif": 
            case "png": 
            case "jpeg":
            case "jpg": 
                header('Location: '.$model->path);
                break;
        }
        
        //все на скачку
        switch( $file_extension )
        {
            case "pdf": $ctype="application/pdf"; break;
            case "exe": $ctype="application/octet-stream"; break;
            case "zip": $ctype="application/zip"; break;
            case "doc": $ctype="application/msword"; break;
            case "xls": $ctype="application/vnd.ms-excel"; break;
            case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
            case "mp3": $ctype="audio/mp3"; break;
            case "gif": $ctype="image/gif"; break;
            case "png": $ctype="image/png"; break;  
            case "jpeg":
            case "jpg": $ctype="image/jpg"; break;
            default: $ctype="application/force-download";
        }
        
        header("Pragma: public"); 
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false); // нужен для некоторых браузеров
        header("Content-Type: $ctype");
        header("Content-Disposition: attachment; filename=\"".$model->name."\";" );
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: ".filesize($pathfile));
        readfile($pathfile);
        exit();
    }
    
    /*
     * удаление файла по ид
     */
    public function actionDeleteById($id_file) {
        $model = UploadFiles::findOne($id_file);
                
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            
            if(!$model){
                $res = [
                    'success' => false,
                    'error' => Yii::t('app/models', 'File not found'),
                ];
                return $res;
            }
            
            $pathfile = \Yii::getAlias('@webroot').$model->path;
            
            if($model->delete() && @unlink($pathfile)){
                $res = [
                    'success' => true,
                    'id_file' => $id_file
                ];
            }
            else{
                $res = [
                    'success' => false,
                    'error' => $model->getErrors()
                ];
            }

            return $res;
        }
    }
}
