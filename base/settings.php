<?php
namespace app\base;

use Yii;
use yii\base\BootstrapInterface;
use app\models\Settings as dbSettings;

class settings implements BootstrapInterface {    
    /*
     * загрузить параметры приложения из бд
     */
    public function bootstrap($app) {
        $settings = dbSettings::getDb()->cache(function ($db) {
            return dbSettings::find()->asArray()->all();
        });
        
        foreach ($settings as $setting) {
            Yii::$app->params[$setting['name']] = $setting['value'];
        }
    }
}