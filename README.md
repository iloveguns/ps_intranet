необходимы: php, mysql, npm + sass
************************************
AJAX КНОПКА
handleAjaxLink можно в ajax.js задать, там он будет на все события
-----------
echo Html::a(Yii::t('app', 'Update'), ['user/update','id' => $model->id], [
    'id' => 'update-user',
    'data-on-done' => 'simpeResponse',//js функция после выполнения
    'data-form-id' => 'user-change',//ид формы, к которой привязано submit
    'class' => 'btn btn-primary',
]);
$this->registerJs("$('#update-user').click(handleAjaxLink);
ajaxCallbacks.simpeResponse = function (response) {
    if(response.success){
        bootstrapAlert('success','".UserModule::t('all', 'User data saved')."');
        $('.modal').modal('hide');
    }
    else{
        bootstrapAlert('danger','".UserModule::t('all', 'User data not saved')."');
        $('.modal').modal('hide');
    },
}", \yii\web\View::POS_END);
************************************
в базе все хранится в текущем поясе
Yii::$app->formatter->timeZone = 'Etc/GMT-0';//обнулить для вывода даты/времени специально стоит, чтоб без морок
************************************
если выбор сотрудников начинает глючить - значит какие то сотрудники попадают не туда(стоят не в том отделе например)
************************************
проверка на админа
Yii::$app->user->identity->isAdmin()
************************************
инструкция по добавлению прав ---
modules/user/models/GeneralRights.php - пошагово обозначено. добавить в public, rules() и attributeLabels()+перевод
************************************
сделать модель легко загружаемой файлы -
1) public $files;//добавить поле для файлов
2) в форму обязательно добавить ['options'=>['enctype'=>'multipart/form-data']]
3) <?= app\models\UploadFiles::showUploadFormField($form, $model, 'files') ?>//в форме добавить поле загрузки
4) добавить в модель
public function afterSave($insert, $changedAttributes) {
    parent::afterSave($insert, $changedAttributes);
    \app\models\UploadFiles::uploadFiles($this, 'files', true);//загрузка файлов
}
5*)//при удалении модели
public function beforeDelete() {
    if (parent::beforeDelete()) {
        UploadFiles::removeFiles($this, '', true);//удалить все файлы            
        return true;
    } else {
        return false;
    }
}
************************************
запуск гифки при долгой загрузке(чтоб не создавали по 2 объявления например)
$('#идформы').on('afterValidate', function () {
    gifLoader();
});
************************************
<?= DatePicker::widget([
    'name' => 'calendar_done_tasks',
    'type' => DatePicker::TYPE_INLINE,
    'value' => date('D, d-m-Y'),
    'pluginOptions' => [
        'format' => 'D, dd-M-yyyy',
        'convertFormat' => true,
        'todayHighlight' => true
    ],
    'pluginEvents' => [
        "changeDate" => "function(e) {
            console.log(e.date.toMysqlFormat(true));
        }",
    ]
]);?>
/*
* js дата в нормальную дату
*/
Date.prototype.toMysqlFormat = function(full) {
   let date = this.getFullYear() + "-" + twoDigits(1 + this.getMonth()) + '-' + twoDigits(this.getDate());
   if(full === true) {
       date += ' ' + twoDigits(this.getHours()) + ':' + twoDigits(this.getMinutes()) + ':' + twoDigits(this.getSeconds());
   }
   return date;
};
function twoDigits(d) {
   if(0 <= d && d < 10) return '0' + d.toString();
   if(-10 < d && d < 0) return '-0' + (-1*d).toString();
   return d.toString();
}