<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle {
    
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'holidays/christmas/christmas.css',//+main.php
        'css/build/production.min.css',
        'tinymcen/skins/lightgray/skin.min.css'
    ];
    public $js = [
        'js/build/production.min.js',
        'tinymcen/tinymce.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
