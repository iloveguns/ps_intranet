<?php
namespace app\assets;

use yii\web\AssetBundle;

/*
 * assets для вебсокета io
 */
class IoAsset extends AssetBundle {
    //добавить динамические данные
    public function init() {
        array_push($this->js, '//' . \Yii::$app->params['ipServerWebSocket'] . ':' . \Yii::$app->params['ipServerWebSocketPort'] . '/socket.io/socket.io.js');
        array_push($this->js, 'js/build/chat_ui.js');
    }
    
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
