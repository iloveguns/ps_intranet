<?php
/**
 * @author Semenov Alexander <semenov@skeeks.com>
 * @link http://skeeks.com/
 * @copyright 2010 SkeekS (СкикС)
 * @date 16.07.2016
 */
namespace app\helpers;
use yii\helpers\ArrayHelper;

class HtmlCompressor
{
    public static function compress($data, $options = null)
    {
        return (new static)
            ->htmlCompress($data, $options);
    }
    
    private function htmlCompress($data, $options = null)
    {
        if (!isset($options)) {
            $options = [];
        }
        $data .= "\n";
        $out = '';
        $inside_pre = false;
        $inside_textarea = false;
        $bytecount = 0;
        while ($line = $this->getLine($data)) {
            $bytecount += strlen($line);
            if ($inside_pre) {
                list($line, $inside_pre) = $this->checkInsidePre($line);
            } elseif ($inside_textarea) {
                list($line, $inside_textarea) = $this->checkInsideTextarea($line);
            } else {
                if (strpos($line, '<pre') !== false) {
                    // Only trim the beginning since we just entered a <pre> block...
                    $line = ltrim($line);
                    // If the <pre> ends on the same line, don't turn on $inside_pre...
                    list($line, $inside_pre) = $this->checkInsidePre($line);
                } elseif (strpos($line, '<textarea') !== false) {
                    // Only trim the beginning since we just entered a <textarea> block...
                    $line = ltrim($line);
                    // If the <textarea> ends on the same line, don't turn on $inside_textarea...
                    list($line, $inside_textarea) = $this->checkInsideTextarea($line);
                } else {
                    // Since we're not inside a <pre> block, we can trim both ends of the line
                    $line = trim($line);
                    // And condense multiple spaces down to one
                    $line = preg_replace('/\s\s+/', ' ', $line);
                }
            }
            // Filter out any blank lines that aren't inside a <pre> block...
            if ($inside_pre || $inside_textarea) {
                $out .= $line;
            } elseif ($line != '') {
                $out .= $line . "\n";
            }
        }
        // Perform any extra (unsafe) compression techniques...
        if (array_key_exists('x', $options) || ArrayHelper::getValue($options, 'extra') === true) {
            // Can break layouts that are dependent on whitespace between tags
            $out = str_replace(">\n<", '><', $out);
        }
        // Remove HTML comments...
        if (array_key_exists('c', $options) || ArrayHelper::getValue($options, 'no-comments') === true) {
            $out = preg_replace('/(<!--.*?-->)/ms', '', $out);
            $out = str_replace('<!>', '', $out);
        }
        // Remove the trailing \n
        $out = trim($out);
        // Output either our stats or the compressed data...
        if (array_key_exists('s', $options) || ArrayHelper::getValue($options, 'stats') === true) {
            $echo = '';
            $echo .= "Original Size: $bytecount\n";
            $echo .= "Compressed Size: " . strlen($out) . "\n";
            $echo .= "Savings: " . round((1 - strlen($out) / $bytecount) * 100, 2) . "%\n";
            echo $echo;
        } else {
            return $out;
        }
        return false;
    }
    
    private function checkInsidePre($line)
    {
        $inside_pre = true;
        if ((strpos($line, '</pre') !== false) && (strripos($line, '</pre') >= strripos($line, '<pre'))) {
            $line = rtrim($line);
            $inside_pre = false;
        }
        return [$line, $inside_pre];
    }
    
    private function checkInsideTextarea($line)
    {
        $inside_textarea = true;
        if ((strpos($line, '</textarea') !== false) && (strripos($line, '</textarea') >= strripos($line, '<textarea'))) {
            $line = rtrim($line);
            $inside_textarea = false;
        }
        return [$line, $inside_textarea];
    }
    
    private function getLine(&$data)
    {
        if (is_resource($data)) {
            return fgets($data);
        }
        if (is_string($data)) {
            if (strlen($data) > 0) {
                $pos = strpos($data, "\n");
                $return = substr($data, 0, $pos) . "\n";
                $data = substr($data, $pos + 1);
                return $return;
            } else {
                return false;
            }
        }
        return false;
    }
    
    private function html_compress($data, $options = null)
    {
        \Yii::warning(sprintf('You are using an deprecated method `%s`.', 'html_compress'));
        return $this->htmlCompress($data, $options);
    }
    
    private function get_line(&$data)
    {
        \Yii::warning(sprintf('You are using an deprecated method `%s`.', 'get_line'));
        return $this->getLine($data);
    }
}