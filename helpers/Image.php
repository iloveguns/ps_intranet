<?php
namespace app\helpers;

use Yii;
use yii\web\HttpException;
use yii\helpers\FileHelper;
use app\helpers\GD;

/*
 * echo Html::img(Image::thumb(Yii::$app->user->identity->avatar, 136, 136));
 */
class Image
{
    static function thumb($filename, $width = null, $height = null, $crop = true)
    {
        if($filename && file_exists(($filename = Yii::getAlias('@webroot') . $filename)))
        {
            $info = pathinfo($filename);
            $thumbName = $info['filename'] . '-' . md5( filemtime($filename) . (int)$width . (int)$height . (int)$crop ) . '.' . $info['extension'];
            $thumbFile = Yii::getAlias('@webroot') . Yii::$app->params['uploadsThumbsFolder'] . DIRECTORY_SEPARATOR . $thumbName;
            $thumbWebFile = '/uploads/thumbs/' . $thumbName;
            if(file_exists($thumbFile)){
                return $thumbWebFile;
            }
            elseif(FileHelper::createDirectory(dirname($thumbFile), 0777) && self::copyResizedImage($filename, $thumbFile, $width, $height, $crop)){
                return $thumbWebFile;
            }
        }
        return '';
    }

    static function copyResizedImage($inputFile, $outputFile, $width, $height = null, $crop = true)
    {
        if(extension_loaded('imagick'))
        {
            $image = new \Imagick($inputFile);

            if($height && !$crop) {
                $image->resizeImage($width, $height, \Imagick::FILTER_LANCZOS, 1, true);
            }
            else{
                $image->resizeImage($width, null, \Imagick::FILTER_LANCZOS, 1);
            }

            if($height && $crop){
                $image->cropThumbnailImage($width, $height);
            }

            return $image->writeImage($outputFile);
        } else if (extension_loaded('gd')) {
            $image = new GD($inputFile);

            if($height) {
                if($width && $crop){
                    $image->cropThumbnail($width, $height);
                } else {
                    $image->resize($width, $height);
                }
            } else {
                $image->resize($width);
            }
            return $image->save($outputFile);
        }
        else {
            throw new HttpException(500, 'Please install GD or Imagick extension');
        }
    }
}