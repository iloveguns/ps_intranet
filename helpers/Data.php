<?php
namespace app\helpers;

use Yii;

class Data
/*
 * кеширование значений
 */
{
    public static function cache($key, $duration, $callable)
    {
        $cache = Yii::$app->cache;
        if($cache->exists($key)){
            $data = $cache->get($key);
        }
        else{
            $data = $callable();

            if($data) {
                $cache->set($key, $data, $duration);
            }
        }
        return $data;
    }
    
    /*
     * удаление всяких спец символов типа \xF0\
     */
    function strip_bad_multibyte_chars($str) {
	$result = '';
	$length = strlen($str);
	
	for ($i = 0; $i < $length; $i++) {
            if ((ord($str[$i]) & 0xF8) == 0xF0) {
                $result .= '';
                $i += 3;
            } else {
                $result .= $str[$i];
            }
	}
	
	return $result;
    }
}