<?php
namespace app\helpers;

use Yii;

/*
 * отправка 
 */
class Mail {    
    /*
     * @easyii helpers
     * $toEmail - email комук отправить
     * $subject - тема письма
     * $data - array передаваемых данных в вид
     * $template - шаблон письма(рендер вид)
     * опции(replyTo)
     */
    public static function send($toEmail, $subject, $data = [], $template = '@app/mail/default/index', $options = [])
    {
        if(Yii::$app->params['send_email'] == 0) return 1;//если стоит не слать письма
        
        $senderEmail = \Yii::$app->components['mailer']['transport']['username'];
        
        if(!filter_var($toEmail, FILTER_VALIDATE_EMAIL) || !$subject || !$template){
            return false;
        }
        $data['subject'] = trim($subject);//заголовок отдельно
        
        $message = Yii::$app->mailer->compose($template, ['data' => $data])
            ->setTo($toEmail)
            ->setSubject($data['subject']);

        if(filter_var($senderEmail, FILTER_VALIDATE_EMAIL)){
            $message->setFrom($senderEmail);
        }

        //не очень нужно
        if(!empty($options['replyTo']) && filter_var($options['replyTo'], FILTER_VALIDATE_EMAIL)){
            $message->setReplyTo($options['replyTo']);
        }
        //var_dump($message);die();
        
        return $message->send();
    }
}