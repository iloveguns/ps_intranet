<?php
namespace app\helpers;

use yii\data\BaseDataProvider;
use Yii;
use app\modules\sites\SitesModule;

/**
 * пример провайдера для вывода в виджетах yii2 через json rest api + curl
 */
class ApiSitesDataProvider extends BaseDataProvider {
    public $key;
     
    public $allModels;
    
    public $params = []; // основные параметры для апи
    
    public $urlpath = '';
    
    public $pk = ''; // первичный ключ для виджетов

    private $counts; // для хранения общего кол-ва записей, может по-разному вычисляться
    
    private $errors = []; // накопление ошибок
    
    public $simple = false; // true - для простых запросов
    
    public function init() {
        parent::init();
    }
 
    protected function prepareModels() {
        $modelsapi = [];
        $errors = [];
        
        if(empty($this->urlpath)) {
            $errors[] = 'Не указан адрес(путь)';
            return [];
        }
        
        //не получается offset брать
        $p = new \yii\data\Pagination();
        $get = Yii::$app->request->get();
        $offset = 0;
        
        if(isset($get[$p->pageParam]) && isset($get[$p->pageSizeParam])) {
            $offset = ((int)$get[$p->pageParam] - 1) * (int)$get[$p->pageSizeParam];
        }
        //end не получается offset брать
        
        //доп параметры
        $this->params['limit'] = $this->getPagination()->getPageSize();
        $this->params['offset'] = $offset;
        
        $curl = curl_init(SitesModule::API_URL . $this->urlpath . '?' . http_build_query($this->params));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['auth_id: ' . Yii::$app->params['sitesmodule_id']]);
        $curl_response = curl_exec($curl);
        $info = curl_getinfo($curl);
        if ($curl_response === false || $info['http_code'] === 404) {
            $errors[] = 'Нет соединения с сервером';
        } else {
            $modelsapi = json_decode($curl_response, true);
        }
        curl_close($curl);
        //json api
        
        if($modelsapi === NULL) {
            $errors[] = 'Нет данных от сервера';
            return [];
        }
        
        if(count($errors) > 0) {
            $this->errors = $errors;
            return [];
        }
        
        if($this->simple === true) {
            $models = $modelsapi;
        } else {
            //для первичного ключа data-key в виджете GridView
            $mt = [];

            foreach ($modelsapi['data'] as $m) {
                $mt[$m[$this->pk]] = $m;
            }

            $models = $mt;

            $this->allModels = $models; // вот так

            $this->counts = $modelsapi['count']; // вот так

            if (($pagination = $this->getPagination()) !== false) {
                $pagination->totalCount = $this->getTotalCount();
            }
        }
        
        return $models;
    }
    
    /**
     * кол-во записей контента
     */
    public function getCounts() {
        return $this->counts;
    }
    
    /**
     * ошибки при обращении к апи
     */
    public function getErrors() {
        return $this->errors;
    }
 
    /**
     * получение общего ко-ва записей
     */
    protected function prepareTotalCount() {
        if(isset($this->params['status'])) {
            return $this->counts['countstatus' . $this->params['status']];
        }
        return $this->counts['count'];//стандартный(одиночный) вывод кол-ва записей
    }

    protected function prepareKeys($models): array {
        if ($this->key !== null) {
            $keys = [];
 
            foreach ($models as $model) {
                if (is_string($this->key)) {
                    $keys[] = $model[$this->key];
                } else {
                    $keys[] = call_user_func($this->key, $model);
                }
            }
 
            return $keys;
        } else {
            return array_keys($models);
        }
    }

}