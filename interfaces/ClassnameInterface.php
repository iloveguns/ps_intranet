<?php
namespace app\interfaces; 

/*
 * необходимы эти функции тем моделям, кто хочет хранить данные в уведомлениях(почти всем)
 */
interface ClassnameInterface {
    
    /*
     * имя класса вместо ::className() для удобства и однозначного определения
     * максимум 20 символов
     */
    public function getName();
    
}