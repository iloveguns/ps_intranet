<?php

use yii\db\Migration;

/*
 * внедрение каскадных зависимостей от сотрудника, можно truncate table user
 */
class m000000_100000_db extends Migration {    
    public function up() {
        $this->execute("
            ALTER TABLE `entity_view` DROP FOREIGN KEY `entity_view_pkuser_fk`; ALTER TABLE `entity_view` ADD CONSTRAINT `entity_view_pkuser_fk` FOREIGN KEY (`id_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `messages` DROP FOREIGN KEY `messages_receiver_user`; ALTER TABLE `messages` ADD CONSTRAINT `messages_receiver_user` FOREIGN KEY (`receiver`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT; ALTER TABLE `messages` DROP FOREIGN KEY `messages_sender_user`; ALTER TABLE `messages` ADD CONSTRAINT `messages_sender_user` FOREIGN KEY (`sender`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `relation_to_user` DROP FOREIGN KEY `project_pkuser_project_fk`; ALTER TABLE `relation_to_user` ADD CONSTRAINT `project_pkuser_project_fk` FOREIGN KEY (`id_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `unread_notice` DROP FOREIGN KEY `unread_notice_pkuser_fki`; ALTER TABLE `unread_notice` ADD CONSTRAINT `unread_notice_pkuser_fki` FOREIGN KEY (`id_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `users_online` DROP FOREIGN KEY `users_online_pk_user`; ALTER TABLE `users_online` ADD CONSTRAINT `users_online_pk_user` FOREIGN KEY (`pk_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE NO ACTION;
            ALTER TABLE `bugtracker` DROP FOREIGN KEY `bugtracker_fk_author_user`; ALTER TABLE `bugtracker` ADD CONSTRAINT `bugtracker_fk_author_user` FOREIGN KEY (`pk_author`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `callboard` DROP FOREIGN KEY `callboard_pkauthor_fk`; ALTER TABLE `callboard` ADD CONSTRAINT `callboard_pkauthor_fk` FOREIGN KEY (`id_author`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `comments` DROP FOREIGN KEY `comment_pkuser_fk`; ALTER TABLE `comments` ADD CONSTRAINT `comment_pkuser_fk` FOREIGN KEY (`id_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `crm_deals` DROP FOREIGN KEY `deals_fk_crm_deals_res_user`; ALTER TABLE `crm_deals` ADD CONSTRAINT `deals_fk_crm_deals_res_user` FOREIGN KEY (`responsible_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT; ALTER TABLE `crm_deals` DROP FOREIGN KEY `deals_fk_crm_deals_updated_user`; ALTER TABLE `crm_deals` ADD CONSTRAINT `deals_fk_crm_deals_updated_user` FOREIGN KEY (`updated_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `crm_deals` DROP FOREIGN KEY `deals_fk_crm_deals_created_user`; ALTER TABLE `crm_deals` ADD CONSTRAINT `deals_fk_crm_deals_created_user` FOREIGN KEY (`created_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `crm_contact` DROP FOREIGN KEY `fk_crm_contact_pk_dep_fk_resuser`; ALTER TABLE `crm_contact` ADD CONSTRAINT `fk_crm_contact_pk_dep_fk_resuser` FOREIGN KEY (`responsible_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `crm_contact` DROP FOREIGN KEY `fk_crm_contact_pk_dep_fk_user`; ALTER TABLE `crm_contact` ADD CONSTRAINT `fk_crm_contact_pk_dep_fk_user` FOREIGN KEY (`updated_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT; ALTER TABLE `crm_contact` DROP FOREIGN KEY `fk_crm_contact_user_created`; ALTER TABLE `crm_contact` ADD CONSTRAINT `fk_crm_contact_user_created` FOREIGN KEY (`created_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `crm_tasks` DROP FOREIGN KEY `fk_crm_created_user`; ALTER TABLE `crm_tasks` ADD CONSTRAINT `fk_crm_created_user` FOREIGN KEY (`created_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `crm_tasks` DROP FOREIGN KEY `fk_crm_task_user_fk`; ALTER TABLE `crm_tasks` ADD CONSTRAINT `fk_crm_task_user_fk` FOREIGN KEY (`fk_user_task`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `crm_tasks` DROP FOREIGN KEY `fk_fk_crmcompany_iduser`; ALTER TABLE `crm_tasks` ADD CONSTRAINT `fk_fk_crmcompany_iduser` FOREIGN KEY (`updated_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `upload_files` DROP FOREIGN KEY `project_pkfiles_user_project_fk`; ALTER TABLE `upload_files` ADD CONSTRAINT `project_pkfiles_user_project_fk` FOREIGN KEY (`id_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `ratings` DROP FOREIGN KEY `ratings_pkuser_fk`; ALTER TABLE `ratings` ADD CONSTRAINT `ratings_pkuser_fk` FOREIGN KEY (`id_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `crm_company` DROP FOREIGN KEY `fk_crm_company_user_created`; ALTER TABLE `crm_company` ADD CONSTRAINT `fk_crm_company_user_created` FOREIGN KEY (`created_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `crm_company` DROP FOREIGN KEY `fk_crm_company_user_responsible`; ALTER TABLE `crm_company` ADD CONSTRAINT `fk_crm_company_user_responsible` FOREIGN KEY (`responsible_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `crm_company` DROP FOREIGN KEY `fk_crm_company_user_updatd`; ALTER TABLE `crm_company` ADD CONSTRAINT `fk_crm_company_user_updatd` FOREIGN KEY (`updated_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `tasks` DROP FOREIGN KEY `task_author_fk`; ALTER TABLE `tasks` ADD CONSTRAINT `task_author_fk` FOREIGN KEY (`id_author`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `tasks` DROP FOREIGN KEY `task_head_fk`; ALTER TABLE `tasks` ADD CONSTRAINT `task_head_fk` FOREIGN KEY (`id_head`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `user_children` DROP FOREIGN KEY `fk_user_children`; ALTER TABLE `user_children` ADD CONSTRAINT `fk_user_children` FOREIGN KEY (`fk_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `personal_notes` DROP FOREIGN KEY `fk_user_pnote`; ALTER TABLE `personal_notes` ADD CONSTRAINT `fk_user_pnote` FOREIGN KEY (`fk_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `blogs` DROP FOREIGN KEY `blog_pkauthor_fk`; ALTER TABLE `blogs` ADD CONSTRAINT `blog_pkauthor_fk` FOREIGN KEY (`id_author`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `analcenter_hall` DROP FOREIGN KEY `analcenter_hall_pkuser_fk`; ALTER TABLE `analcenter_hall` ADD CONSTRAINT `analcenter_hall_pkuser_fk` FOREIGN KEY (`id_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `conference_hall` DROP FOREIGN KEY `conference_hall_pkuser_fk`; ALTER TABLE `conference_hall` ADD CONSTRAINT `conference_hall_pkuser_fk` FOREIGN KEY (`id_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `tasks` DROP FOREIGN KEY `project_id_task`; ALTER TABLE `tasks` ADD CONSTRAINT `project_id_task` FOREIGN KEY (`project_id`) REFERENCES `projects`(`pk_project`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `subtasks` DROP FOREIGN KEY `subtask_pk_subtask`; ALTER TABLE `subtasks` ADD CONSTRAINT `subtask_pk_subtask` FOREIGN KEY (`fk_task`) REFERENCES `tasks`(`pk_task`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `projects` DROP FOREIGN KEY `project_author_fk`; ALTER TABLE `projects` ADD CONSTRAINT `project_author_fk` FOREIGN KEY (`id_author`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT; ALTER TABLE `projects` DROP FOREIGN KEY `project_head_fk`; ALTER TABLE `projects` ADD CONSTRAINT `project_head_fk` FOREIGN KEY (`id_head`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `events` DROP FOREIGN KEY `events_pkuser_fk`; ALTER TABLE `events` ADD CONSTRAINT `events_pkuser_fk` FOREIGN KEY (`id_author`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `events_company` DROP FOREIGN KEY `event_company_pkuser_fk`; ALTER TABLE `events_company` ADD CONSTRAINT `event_company_pkuser_fk` FOREIGN KEY (`id_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `dbpartners_client` DROP FOREIGN KEY `fk_user_dbpartners_client`; ALTER TABLE `dbpartners_client` ADD CONSTRAINT `fk_user_dbpartners_client` FOREIGN KEY (`fk_user`) REFERENCES `user`(`id_user`) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE `dbpartners_relations` DROP FOREIGN KEY `fk_client`; ALTER TABLE `dbpartners_relations` ADD CONSTRAINT `fk_client` FOREIGN KEY (`fk_client`) REFERENCES `dbpartners_client`(`pk_client`) ON DELETE CASCADE ON UPDATE RESTRICT;
        ");
    }
    
    /*
     * код для очищения всех таблиц+инкремент 0
     * не до конца
     */
    public function truncate() {
        $t = "
            ALTER TABLE user DROP FOREIGN KEY user_appointment_fk;
            ALTER TABLE callboard DROP FOREIGN KEY callboard_pkauthor_fk;
            ALTER TABLE callboard DROP FOREIGN KEY fk_callboard_category_callboard;
            ALTER TABLE crm_contacts_info DROP FOREIGN KEY crm_contact_fk_crmcompany;
            ALTER TABLE crm_deals DROP FOREIGN KEY deals_fk_crm_company;
            ALTER TABLE crm_contact DROP FOREIGN KEY fk_crm_contact_company;
            ALTER TABLE crm_tasks DROP FOREIGN KEY fk_fk_crmcompany_fk;
            ALTER TABLE crm_relation_tags DROP FOREIGN KEY relation_tags_fk_company;
            ALTER TABLE crm_contacts_info DROP FOREIGN KEY crm_contact_fk_crmcontact;
            ALTER TABLE crm_deals DROP FOREIGN KEY deals_fk_crm_contact;
            ALTER TABLE crm_tasks DROP FOREIGN KEY fk_fk_crmcontact_fk;
            ALTER TABLE crm_relation_tags DROP FOREIGN KEY relation_tags_fk_contact;
            ALTER TABLE crm_access_settings DROP FOREIGN KEY crm_acc_sett_fk_crm_role;
            ALTER TABLE crm_relation_tags DROP FOREIGN KEY relation_tags_fk_tag;
            ALTER TABLE crm_tasks DROP FOREIGN KEY fk_crm_task_resulttask;
            ALTER TABLE crm_tasks DROP FOREIGN KEY fk_crm_task_typetask;

            TRUNCATE TABLE analcenter_hall;
            TRUNCATE TABLE appointments;
            TRUNCATE TABLE blogs;
            TRUNCATE TABLE bugtracker;
            TRUNCATE TABLE callboard;
            TRUNCATE TABLE callboard_category;
            TRUNCATE TABLE comments;
            TRUNCATE TABLE conference_hall;
            TRUNCATE TABLE crm_access_settings;
            TRUNCATE TABLE crm_company;
            TRUNCATE TABLE crm_contact;
            TRUNCATE TABLE crm_contacts_info;
            TRUNCATE TABLE crm_deals;
            TRUNCATE TABLE crm_faq;
            TRUNCATE TABLE crm_relation_tags;
            TRUNCATE TABLE crm_roles;
            TRUNCATE TABLE crm_tags;
            TRUNCATE TABLE crm_tasks;
            TRUNCATE TABLE crm_tasks_results;
            TRUNCATE TABLE crm_typedeals;
            TRUNCATE TABLE crm_typetask;
            TRUNCATE TABLE dbpartners_client;
            TRUNCATE TABLE dbpartners_congratulations;
            TRUNCATE TABLE dbpartners_relations;
            TRUNCATE TABLE dbpartners_type_client;
            TRUNCATE TABLE department;
            TRUNCATE TABLE divizion;
            TRUNCATE TABLE entity_report;
            TRUNCATE TABLE entity_user_done;
            TRUNCATE TABLE entity_view;
            TRUNCATE TABLE events;
            TRUNCATE TABLE events_company;
            TRUNCATE TABLE general_roles;
            TRUNCATE TABLE graphic_relate;
            TRUNCATE TABLE holidays;
            TRUNCATE TABLE log;
            TRUNCATE TABLE loginform;
            TRUNCATE TABLE messages;
            TRUNCATE TABLE migration;
            TRUNCATE TABLE organization;
            TRUNCATE TABLE personal_notes;
            TRUNCATE TABLE projects;
            TRUNCATE TABLE ratings;
            TRUNCATE TABLE relation_to_department;
            TRUNCATE TABLE relation_to_divizion;
            TRUNCATE TABLE relation_to_organization;
            TRUNCATE TABLE relation_to_user;
            TRUNCATE TABLE subscribes;
            TRUNCATE TABLE subtasks;
            TRUNCATE TABLE system_settings;
            TRUNCATE TABLE tasks;
            TRUNCATE TABLE technique;
            TRUNCATE TABLE unread_notice;
            TRUNCATE TABLE upload_files;
            TRUNCATE TABLE user;
            TRUNCATE TABLE user_children;
            TRUNCATE TABLE user_status;
            TRUNCATE TABLE users_online;
            
            ALTER TABLE `user` ADD CONSTRAINT `user_appointment_fk` FOREIGN KEY (`fk_appointment`) REFERENCES `appointments` (`pk_appointment`);
            ALTER TABLE `callboard` ADD CONSTRAINT `callboard_pkauthor_fk` FOREIGN KEY (`id_author`) REFERENCES `user` (`id_user`), ADD CONSTRAINT `fk_callboard_category_callboard` FOREIGN KEY (`fk_callboard_category`) REFERENCES `callboard_category` (`pk_category`);
            ALTER TABLE `crm_contacts_info` ADD CONSTRAINT `crm_contact_fk_crmcompany` FOREIGN KEY (`fk_crmcompany`) REFERENCES `crm_company` (`pk_company`) ON DELETE CASCADE;
            ALTER TABLE `crm_deals` ADD CONSTRAINT `deals_fk_crm_company` FOREIGN KEY (`fk_crmcompany`) REFERENCES `crm_company` (`pk_company`) ON DELETE CASCADE;
            ALTER TABLE `crm_contact` ADD CONSTRAINT `fk_crm_contact_company` FOREIGN KEY (`fk_crm_company`) REFERENCES `crm_company` (`pk_company`) ON DELETE CASCADE;
            ALTER TABLE `crm_tasks` ADD CONSTRAINT `fk_fk_crmcompany_fk` FOREIGN KEY (`fk_crmcompany`) REFERENCES `crm_company` (`pk_company`) ON DELETE CASCADE;
            ALTER TABLE `crm_relation_tags` ADD CONSTRAINT `relation_tags_fk_company` FOREIGN KEY (`fk_crmcompany`) REFERENCES `crm_company` (`pk_company`) ON DELETE CASCADE;
            ALTER TABLE `crm_contacts_info` ADD CONSTRAINT `crm_contact_fk_crmcontact` FOREIGN KEY (`fk_crmcontact`) REFERENCES `crm_contact` (`pk_contact`) ON DELETE CASCADE;
            ALTER TABLE `crm_deals` ADD CONSTRAINT `deals_fk_crm_contact` FOREIGN KEY (`fk_crmcontact`) REFERENCES `crm_contact` (`pk_contact`) ON DELETE CASCADE;
            ALTER TABLE `crm_tasks` ADD CONSTRAINT `fk_fk_crmcontact_fk` FOREIGN KEY (`fk_crmcontact`) REFERENCES `crm_contact` (`pk_contact`) ON DELETE CASCADE;
            ALTER TABLE `crm_relation_tags` ADD CONSTRAINT `relation_tags_fk_contact` FOREIGN KEY (`fk_crmcontact`) REFERENCES `crm_contact` (`pk_contact`) ON DELETE CASCADE;
            ALTER TABLE `crm_access_settings` ADD CONSTRAINT `crm_acc_sett_fk_crm_role` FOREIGN KEY (`fk_role`) REFERENCES `crm_roles` (`pk_role`) ON DELETE CASCADE;
            ALTER TABLE `crm_relation_tags` ADD CONSTRAINT `relation_tags_fk_tag` FOREIGN KEY (`fk_tag`) REFERENCES `crm_tags` (`pk_tag`);
            ALTER TABLE `crm_tasks` ADD CONSTRAINT `fk_crm_task_resulttask` FOREIGN KEY (`fk_task_result`) REFERENCES `crm_tasks_results` (`pk_task_result`), ADD CONSTRAINT `fk_crm_task_typetask` FOREIGN KEY (`fk_typetask`) REFERENCES `crm_typetask` (`pk_typetask`);
        ";
    }
}
