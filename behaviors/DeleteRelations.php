<?php
namespace app\behaviors;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use app\models\UnreadNotice;
use app\models\UploadFiles;
use app\models\EntityView;
use app\models\EntityUserDone;
use app\models\Comments;

/**
 * поведение удаление привязки сотрудников, отделов и тд из связочных таблиц и тд при удалении
 * чистящий класс от говна при удалении
 */
class DeleteRelations extends \yii\base\Behavior
{
    public $className;//имя удаляемого класса
    public $pk;//надо
    
    private $structs = ['department', 'divizion', 'organization', 'user'];//массив названий таблиц(названий структур)

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_DELETE => 'deleteRelations',
        ];
    }

    public function deleteRelations()
    {
        $item_id = ArrayHelper::toArray($this)['owner'][$this->pk[0]];//pk сущности($this->pk_task например)
        foreach ($this->structs as $structName) {//связочные
            Yii::$app->db->createCommand('DELETE FROM `relation_to_'.$structName.'` WHERE `item_id` = :id AND `class` = :class', [':id' => $item_id, ':class' => $this->className])->execute();
        }
        
        //удалить уведомления(из за них могут быть ошибки - пытается получить несуществующую)
        UnreadNotice::deleteAll(['class' => $this->className, 'item_id' => $item_id]);
        
        //физически удалить файлы
        UploadFiles::deleteFiles($this->className, $item_id);
        UploadFiles::deleteAll(['item_id' => $item_id, 'class' => $this->className]);//загруженные файлы
        
        //удаление просмотров
        EntityView::deleteAll(['class' => $this->className, 'item_id' => $item_id]);
        
        //удаление отчетов
        \app\models\EntityReport::deleteAll(['class' => $this->className, 'item_id' => $item_id]);
        
        //метки о выполнении
        EntityUserDone::deleteAll(['class' => $this->className, 'item_id' => $item_id]);
        
        //удаление комментов по одному(beforeSave)
        foreach (Comments::findAll(['class' => $this->className, 'item_id' => $item_id]) as $comment) {
            $comment->delete();
        }
    }
}