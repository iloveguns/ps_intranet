<?php
return [
    'Crm count created tasks' => 'Создано задач',
    'Crm count done tasks' => 'Завершено задач',
    'Crm count created contacts' => 'Создано контактов',
    'Crm count created companies' => 'Создано компаний',
    'Crm count contacts to archive' => 'Помещено контактов в архив',
    'Crm count companies to archive' => 'Помещено компаний в архив',
    'Crm count contacts from archive' => 'Восстановлено контактов из архива',
    'Crm count companies from archive' => 'Восстановлено компаний из архива',
    'Crm count company done tasks' => 'Кол-во отработанных компаний',
    'Crm count contact done tasks' => 'Кол-во отработанных контактов',
    'Crm count written comments' => 'Кол-во написанных комментариев',
    'Crm count all actions' => 'Всего сделанный действий'
];
