<?php

return [
    'New Comment {title}' => 'Комментарий "{title}"',
    '{count_repeats} New Comments' => '{count_repeats} новых комментариев',
    'New Comment' => 'Комментарий',
    'New Callboard {title}' => 'Новое объявление "{title}"',
    'New Task {title}' => 'У вас новая задача "{title}"',
    'You had been added' => 'Вас добавили',
    'New Reply Comment' => 'Ответ на ваш комментарий',
    'New Registered User' => 'Новый сотрудник ожидает модерации',
    'New File added' => 'Добавлен новый файл',
    'Changed Status' => 'Сменен статус',
    'New Event {title}' => 'Новое событие "{title}"',
    'New Notify About Event {title}' => 'Оповещение о событии "{title}"',
    'New message bug tracker' => 'Новое сообщение в баг трекере',
    'New query system admin' => 'Новое сообщение в журнале сисадмина',
    'New query steward' => 'Новое сообщение в журнале завхоза',
    'New done assignment task {title}' => 'Порученная задача выполнена "{title}"',
    'API_MAIL_SENDING_DONE' => 'Email рассылка завершена',
    
    'linked {name} {title}' => '{name} "{title}"',
    'from {name}' => 'от {name}',
];
