<?php
return [
    'Oops! Page not found' => 'Кажется страница не найдена',
    'We could not find the page you were looking for.' => 'То, что вы искали не может быть отображено.',
    'Meanwhile, you may try using the search form or' => 'Воспользуйтесь формой поиска или ',
    'Param must be more the 0' => 'Параметр должен быть больше 0',
];
