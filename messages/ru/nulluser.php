<?php
/*
 * NULL'евой сотрудник - оповещения от системы
 */
return [
    'Today, {date} {username} celebrates its birthday!' => '{date} {username} празднует свой день рождения!',
    'Today, {date} {holiday}' => 'Сегодня, {date} {holiday}',
    'Сongratulations' => 'Поздравляем!',
];
