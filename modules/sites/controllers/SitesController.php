<?php

namespace app\modules\sites\controllers;

use Yii;
use yii\filters\VerbFilter;
use app\modules\sites\SitesModule;

class SitesController extends \app\components\Controller {
    
    /**
     * поиск контента по строке(части), обращение к API
     * @param {String} $q
     */
    public function actionContentbystring($q) {
        $this->jsonResponse();
        
        $search['title_content'] = ['val' => $q, 'type' => 'string'];
        
        $dataProvider = new \app\helpers\ApiSitesDataProvider([
            'params' => [
                'fk_site' => \Yii::$app->getRequest()->getCookies()->getValue(SitesModule::COOKIE_SITE), // ид сайта
                'orderby' => 'pk_content DESC',
                'status' => 1, // опубликован
                'withcount' => 1,
                'isdeleted' => 0,
                'search' => $search
            ],
            'pagination' => [
                'pageSize' => Yii::$app->params['default_Pagination_PageSize']
            ],
            'urlpath' => '/content',
            'pk' => 'pk_content'
        ]);
        
        $dataProvider->prepare();
        
        $return = [];
        
        foreach ($dataProvider->getModels() as $model) {
            $return['results'][] = [
                'text' => $model['title_content'],
                'id' => $model['pk_content'],
                'img' => $model['headimgsrc_content']
            ];
        }
        
        return $return;
    }
    
    public function actionIndex() {
        return $this->render('index');
    }
    
    /**
     * список сюжетов
     */
    public function actionStories() {
        return $this->render('stories');
    }
    
    /**
     * главная страница, index
     */
    public function actionMainpageindex() {
        $dataProvider = new \app\helpers\ApiSitesDataProvider([
            'params' => [
                'fk_site' => \Yii::$app->getRequest()->getCookies()->getValue(SitesModule::COOKIE_SITE), // ид сайта
            ],
            'urlpath' => '/mainpageInfo',
            'pk'    => 'id_index_page',
            'simple' => true
        ]);
        
        return $this->render('/mainpage/index', [
            'dataProvider' => $dataProvider
        ]);
    }
    
    /**
     * блочная главная страница
     * создание и редактирование блоков
     * gridstack.js
     */
    public function actionMainpagecrud(int $id_mp = NULL) {
        $mode = ($id_mp) ? 'update' : 'create';
        
        // получение инфы о контенте
        $search = [];
        //правила валидации в удобном виде
        $rules = [
            'title_content' => 'string'
        ];
        
        $searchModel = new \yii\base\DynamicModel([
            'title_content'
        ]);
        foreach ($rules as $rule => $type) {
           $searchModel->addRule($rule, $type);
        }
        
        if($searchModel->load(Yii::$app->request->get())) {
            foreach ($searchModel->attributes as $attr => $value) {
                if(strlen($value) >= 1) {
                    $search[$attr] = ['val' => $value, 'type' => $rules[$attr]];
                }
            };
        }
        
        $dataProvider = new \app\helpers\ApiSitesDataProvider([
            'params' => [
                'fk_site' => \Yii::$app->getRequest()->getCookies()->getValue(SitesModule::COOKIE_SITE), // ид сайта
                'orderby' => 'pk_content DESC',
                'status' => 1, // опубликован
                'withcount' => 1,
                'isdeleted' => 0,
                'search' => $search
            ],
            'pagination' => [
                'pageSize' => Yii::$app->params['default_Pagination_PageSize']
            ],
            'urlpath' => '/content',
            'pk' => 'pk_content'
        ]);
        
        // $dataProvider->prepare();
        // end получение инфы о контенте
        
        
        // получение данных о главной странице
        $mainPageData = [];
        
        if($id_mp) {
            $dataProviderMainpage = new \app\helpers\ApiSitesDataProvider([
                'params' => [
                    'fk_site' => \Yii::$app->getRequest()->getCookies()->getValue(SitesModule::COOKIE_SITE), // ид сайта,
                    'id'      => $id_mp
                ],
                'urlpath' => '/mainpageByDateOrId',
                'pk'    => 'id_index_page',
                'simple' => true
            ]);
            
            $mainPageData = $dataProviderMainpage->getModels()['data'];
        }
        // end получение данных о главной странице
        
        return $this->render('/mainpage/view', [
            'dataProvider'  => $dataProvider,
            'counts'        => $dataProvider->getCounts(),
            'searchModel'   => $searchModel,
            'mode'          => $mode,
            'mainPageData'  => $mainPageData
        ]);
    }
    
    /**
     * список текстовых блоков
     */
    public function actionTextblocks() {
        $search = [];
        
        //правила валидации в удобном виде
        $rules = [
            'label_text_block' => 'string',
            'create_date' => 'string',
            'text' => 'string',
            'fk_user_created' => 'integer',
            'isactive' => 'integer'
        ];
        
        $searchModel = new \yii\base\DynamicModel([
            'label_text_block', 'text', 'fk_user_created', 'create_date', 'isactive'
        ]);
        foreach ($rules as $rule => $type) {
           $searchModel->addRule($rule, $type);
        }
        
        if($searchModel->load(Yii::$app->request->get())) {
            foreach ($searchModel->attributes as $attr => $value) {
                if(strlen($value) >= 1) {
                    $search[$attr] = ['val' => $value, 'type' => $rules[$attr]];
                }
            };
        }
        
        $dataProvider = new \app\helpers\ApiSitesDataProvider([
            'params' => [
                'fk_site' => \Yii::$app->getRequest()->getCookies()->getValue(SitesModule::COOKIE_SITE), // ид сайта
                'search' => $search
            ],
            'pagination' => [
                'pageSize' => Yii::$app->params['default_Pagination_PageSize']
            ],
            'urlpath' => '/text_blocks',
            'pk' => 'pk_text_block'
        ]);
        
        $dataProvider->prepare();
        
        return $this->render('textblocks', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
    
    /**
     * создание/редактирование текстовых блоков
     */
    public function actionUpdatetextblocks($pk_text_block = NULL) {
        return $this->render('updatetextblocks', [
            'update' => $pk_text_block
        ]);
    }
    
    /**
     * пункты меню
     */
    public function actionMenuitems($pk_menu) {
        return $this->render('menuitems');
    }
    
    /**
     * редактирование пункта меню
     */
    public function actionUpdatemenu($pk_menu = NULL) {
        return $this->render('updatemenu', [
            'update' => $pk_menu
        ]);
    }
    
    /**
     * список, выбор меню
     */
    public function actionMenu() {
        $search = [];
        
        //правила валидации в удобном виде
        $rules = [
            'name_menu' => 'string'
        ];
        
        $searchModel = new \yii\base\DynamicModel([
            'name_menu'
        ]);
        foreach ($rules as $rule => $type) {
           $searchModel->addRule($rule, $type);
        }
        
        if($searchModel->load(Yii::$app->request->get())) {
            foreach ($searchModel->attributes as $attr => $value) {
                if(strlen($value) >= 1) {
                    $search[$attr] = ['val' => $value, 'type' => $rules[$attr]];
                }
            };
        }
        
        $dataProvider = new \app\helpers\ApiSitesDataProvider([
            'params' => [
                'fk_site' => \Yii::$app->getRequest()->getCookies()->getValue(SitesModule::COOKIE_SITE), // ид сайта
                'search' => $search                
            ],
            'pagination' => [
                'pageSize' => Yii::$app->params['default_Pagination_PageSize']
            ],
            'urlpath' => '/menus',
            'pk' => 'pk_menu'
        ]);
        
        return $this->render('menu', [
            'dataProvider' => $dataProvider,
            'counts' => $dataProvider->getCounts(),
            'searchModel' => $searchModel
        ]);
    }
    
    public function actionCreatetags() {
        return $this->render('createtags');
    }
    
    public function actionCreatecontent() {
        return $this->render('createcontent');
    }
    
    public function actionViewcontent() {
        $status = isset($_GET['status']) ? (int)$_GET['status'] : 0;        
        
        $search = [];
        //правила валидации в удобном виде
        $rules = [
            'fk_user_created' => 'integer',
            'create_date' => 'string',
            'update_date' => 'string',
            'publish_date' => 'string',
            'title_content' => 'string',
            'text_content' => 'string'
        ];
        
        $searchModel = new \yii\base\DynamicModel([
            'title_content', 'fk_user_created', 'create_date', 'update_date', 'publish_date', 'text_content'
        ]);
        foreach ($rules as $rule => $type) {
           $searchModel->addRule($rule, $type);
        }
        
        if($searchModel->load(Yii::$app->request->get())) {
            foreach ($searchModel->attributes as $attr => $value) {
                if(strlen($value) >= 1) {
                    $search[$attr] = ['val' => $value, 'type' => $rules[$attr]];
                }
            };
        }
        
        $dataProvider = new \app\helpers\ApiSitesDataProvider([
            'params' => [
                'fk_site' => \Yii::$app->getRequest()->getCookies()->getValue(SitesModule::COOKIE_SITE), // ид сайта
                'orderby' => 'publish_date DESC',
                'status' => $status,
                'withcount' => 1,
                'isdeleted' => 0,
                'search' => $search
            ],
            'pagination' => [
                'pageSize' => Yii::$app->params['default_Pagination_PageSize']
            ],
            'urlpath' => '/content',
            'pk' => 'pk_content'
        ]);
        
        $dataProvider->prepare();
        
        return $this->render('viewcontent', [
            'dataProvider' => $dataProvider,
            'counts' => $dataProvider->getCounts(),
            'searchModel' => $searchModel,
            'status' => $status
        ]);
    }
    
    /**
     * просмотр авторов контента
     */
    public function actionContentauthors() {
        $dataProvider = new \app\helpers\ApiSitesDataProvider([
            'params' => [
                'fk_site' => \Yii::$app->getRequest()->getCookies()->getValue(SitesModule::COOKIE_SITE), // ид сайта
            ],
            'pagination' => [
                'pageSize' => Yii::$app->params['default_Pagination_PageSize']
            ],
            'urlpath' => '/content_authors/list',
            'pk' => 'pk_content_author'
        ]);
        
        $dataProvider->prepare();
        
        return $this->render('content_authors', [
            'dataProvider' => $dataProvider,
            'counts' => $dataProvider->getCounts()
        ]);
    }
    
    /**
     * добавление авторов контента
     */
    public function actionCreatecontentauthor() {
        return $this->render('create_content_authors');
    }
    
    
    /**
     * баннеры
     * 
     * @return type
     */
    public function actionBanners() {
        return $this->render('/banners/index');
    }
}