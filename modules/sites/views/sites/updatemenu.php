<?php
$this->title = Yii::t('app/views', ($update === NULL) ? 'sites Create menu' : 'sites Update menu');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'sites Menu'), 'url' => ['menu']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= $this->title ?></h1>

<form id="save_menu">
    <div class="form-group">
        <label for="name_menu">Название</label>
        <input type="text" class="form-control" id="name_menu" placeholder="Название">
    </div>
    
    <div class="form-group">
        <label for="label_menu">Метка</label>
        <input type="text" class="form-control" id="label_menu" placeholder="Метка">
    </div>
    
    <button type="submit" class="btn btn-primary">Сохранить</button>
</form>

<?php
$add = ($update === NULL) ? '' : 'sitesModule.afterload.push(sitesModule.menu.registerLoadMenuInfo);';
$this->registerJs("
    sitesModule.afterload.push(sitesModule.menu.registerSaveMenu);
    onlyLatinText('#label_menu');
    " . $add . "
", \yii\web\View::POS_READY);
?>