<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'sites Create tags');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-6">
        <h3><?= Html::encode($this->title) ?></h3>
        <form>
            <div class="form-group">
                <label class="control-label" for="name_tag">Название</label>
                <input type="text" id="name_tag" class="form-control">
                <p class="help-block">Несколько тегов через разделитель ";".</p>
            </div>

            <br>
            <button class="btn btn-primary" id="btn-create-tags">Создать тег</button>
        </form>
    </div>
    <div class="col-md-6">
        <h3>Список тегов (<span id="count-tags">0</span>)</h3>
        <table class="table table table-striped table-bordered box" id="list-tags">
            <thead>
                <tr>
                    <th>Название тега</th>
                </tr>                    
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

<?php
$this->registerJs("
    sitesModule.afterload.push(tags.getTagsList);
", \yii\web\View::POS_READY);
?>