<?php

use yii\helpers\Html;
use kartik\select2\Select2;

$this->title = Yii::t('app/views', ($update === NULL) ? 'sites Create Text blocks' : 'sites Update Text blocks');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'sites Text blocks'), 'url' => ['textblocks']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-9">
        <h3><?= Html::encode($this->title) ?></h3>
        <form>
            <div class="form-group">
                <label class="radio-inline">
                    <input type="radio" name="types_text_block" class="types_text_block" id="type_text" value="option1" checked="checked"> Текст
                </label>
                <label class="radio-inline">
                    <input type="radio" name="types_text_block" class="types_text_block" id="type_html" value="option2"> Html
                </label>
            </div>
            <div class="form-group">
                <label class="control-label" for="text_text_block">Содержимое</label><br />
                <textarea id="text_text_block" class="form-control" rows="5"></textarea>
            </div>
            
            <div class="form-group">
                <label class="control-label" for="label_text_block">Метка(Только латиница)</label>
                <input type="text" id="label_text_block" class="form-control" maxlength="45">
            </div>
        </form>
    </div>
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Сохранить</h3>
            </div>
            <div class="box-body">
                <form>
                    <div class="form-group">
                        <label class="control-label">Статус</label>
                        <?= Select2::widget([
                            'name' => 'isactive',
                            'data' => \app\modules\sites\SitesModule::DEFAULT_STATUSES,
                            'value' => 1,
                            'options' => [
                                'id' => 'isactive'
                            ],
                        ]) ?>
                    </div>
                    <button type="button" id="text_block_publish" class="btn btn-primary pull-right btn-sm">Сохранить</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
$add = ($update === NULL) ? '' : 'sitesModule.afterload.push(registerLoadTextBlock);';
$this->registerJs("
    onlyLatinText('#label_text_block');
    " . $add
, \yii\web\View::POS_READY);
?>