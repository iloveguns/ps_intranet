<?php
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('app/views', 'sites Menu');
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= $this->title ?></h1>

<p>
    <?= Html::a(Yii::t('app/views', 'sites Create menu'), ['updatemenu'], ['class' => 'btn btn-success']) ?>
</p>

<?= GridView::widget([
    'id' => 'content-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [        
        [
            'attribute' => 'name_menu',
            'label' => 'Название',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a($model['name_menu'], ['menuitems', 'pk_menu' => $model['pk_menu']]);
            }
        ],
        [
            'attribute' => 'label_menu',
            'label' => 'Метка',
            'format' => 'raw',
            'value' => function ($model) {
                return $model['label_menu'];
            }
        ],
        
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons'=>[
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['updatemenu', 'pk_menu' => $model['pk_menu']], ['title' => 'Редактировать меню']);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-remove text-red removemenu" data-id="'. $model['pk_menu'] . '"></span>', [''], ['title' => 'Удалить меню']);
                }
            ],
        ]
    ],
]);

$this->registerJs('
    sitesModule.afterload.push(sitesModule.menu.registerRemoveMenu);
', \yii\web\View::POS_READY);

