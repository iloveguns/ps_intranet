<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Sites module');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-polling-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app/views', 'sites Create tags'), ['createtags'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app/views', 'sites Create content'), ['createcontent'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app/views', 'sites Content'), ['viewcontent'], ['class' => 'btn btn-success']) ?>
    </p>
</div>