<?php
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('app/views', 'sites Create Content Author');
$this->params['breadcrumbs'][] = $this->title;
?>

<h2><?= $this->title ?></h2>

<div class="form-group">
    <label class="control-label">Статус</label>
    <?= \kartik\select2\Select2::widget([
        'name' => 'pk_content_author',
        'data' => \app\modules\sites\SitesModule::getAllUsers(),
        'value' => 2,
        'options' => [
            'id' => 'pk_content_author'
        ],
    ]) ?>
</div>

<button class="btn btn-primary" id="create_author_content">Сохранить</button>