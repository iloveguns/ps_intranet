<?php
use yii\grid\GridView;
use app\modules\sites\SitesModule;
use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use app\modules\user\models\User;

$this->title = Yii::t('app/views', 'sites Text blocks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">        
    <div class="col-md-9">
        <?php if($dataErrors = $dataProvider->getErrors()) : ?>
            <div class="alert alert-danger" role="alert"><?= implode(' ', $dataErrors) ?></div>
        <?php endif ?>
        
        <div class="mb-1em">
            <?= Html::a(Yii::t('app/views', 'sites Create Text blocks'), ['updatetextblocks'], ['class' => 'btn btn-success btn-sm']) ?>
            <button id="delete_selected_text_blocks" class="btn btn-danger btn-sm">Удалить выбранные</button>
        </div>
    </div>
</div>

<?= GridView::widget([
    'id' => 'content-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\CheckboxColumn'],
        
        [
            'attribute' => 'label_text_block',
            'label' => 'Метка',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a($model['label_text_block'], ['updatetextblocks', 'pk_text_block' => $model['pk_text_block']]);
            }
        ],
        [
            'attribute' => 'isactive',
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'isactive',
                'data' => SitesModule::DEFAULT_STATUSES,
                'options' => ['placeholder' => ''],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]),
            'label' => 'Статус',
            'value' => function ($model) {
                return SitesModule::DEFAULT_STATUSES[$model['isactive']];
            }
        ],
        [
            'attribute' => 'fk_user_created',
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'fk_user_created',
                'data' => User::getAllActive(),
                'options' => ['placeholder' => ''],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]),
            'label' => 'Автор',
            'value' => function ($model) {
                return User::getAll($model['fk_user_created'], '{lastname} {name}');
            }
        ],
        [
            'attribute' => 'create_date',
            'filter' => DatePicker::widget([
                'model' => $searchModel, 
                'attribute' => 'create_date',
                'removeButton' => [
                    'icon'=>'remove',
                ],
                'options' => ['placeholder' => ''],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]),
            'label' => 'Создано',
            'value' => function ($model) {
                return Yii::$app->formatter->asDatetime($model['create_date']);
            }
        ],
        
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}',
            'buttons'=>[
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['updatetextblocks', 'pk_text_block' => $model['pk_text_block']]);
                }
            ],
        ]
    ],
]); 