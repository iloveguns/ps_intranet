<?php

use yii\helpers\Html;
use kartik\select2\Select2;

$this->title = Yii::t('app/views', 'sites Menu items');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'sites Menu'), 'url' => ['menu']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hidden">
    <?= Select2::widget(['name' => 't','data' => [],'options' => ['id' => 't']]) ?>
</div>

<h1><?= $this->title ?></h1>

<div class="row">
    <div class="col-md-8">
        <table class="table table-striped" id="menu-table">
            <thead>
                <tr>
                    <th>Название</th>
                    <th>Путь</th>
                    <th>Активен</th>
                    <th>Ссылка</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <div class="col-md-4">
        <h3>Структура(только активные)</h3>
        <div class="dd" id="nestable-json"></div>
    </div>
</div>
<?php
$this->registerJs('
    sitesModule.afterload.push(sitesModule.menu.registerLoadMenu);
', \yii\web\View::POS_READY);