<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\datetime\DateTimePicker;
use yii\web\JsExpression;
use yii\bootstrap\Collapse;

$this->title = Yii::t('app/views', 'sites Create content');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row" style="position: relative">
    <div class="col-md-9">
        <div id="notif"></div><!-- для alert (занято редактирование) -->
        
        <h3><?= Html::encode($this->title) ?></h3>
        <form>
            <div class="form-group">
                <label class="control-label" for="title_content">Заголовок</label>
                <input type="text" id="title_content" class="form-control">
            </div>
            
            <div class="form-group">
                <label class="control-label" for="seo_title_content">Заголовок(SEO title)</label>
                <input type="text" id="seo_title_content" class="form-control" maxlength="80">
            </div>
            
            <div class="form-group">
                <label class="control-label" for="intro_content">Краткое содержимое (meta name='description')</label>
                <textarea id="intro_content" class="form-control" rows="4"></textarea>
            </div>
            
            <div class="form-group" id="redactor_content">
                <label class="control-label" for="text_content">Содержимое</label><br />
                <button type="button" data-click="files-add-files" class="btn btn-default btn-sm open-library" data-toggle="modal" data-target="#library-files-modal">
                    Добавить медиафайл
                </button>
                <button type="button" data-click="widget-add-linked-content" class="btn btn-default btn-sm open-library" data-toggle="modal" data-target="#widget-linked-content">
                    Добавить виджет
                </button>
                <textarea id="text_content" class="form-control" tinymce="tinymcesites"></textarea>
            </div>
        </form>
    </div>
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Опубликовать</h3>
            </div>
            <div class="box-body">
                <form>
                    <div class="form-group">
                        <label class="control-label">Статус</label>
                        <?= Select2::widget([
                            'name' => 'status_content',
                            'data' => \app\modules\sites\SitesModule::CONTENT_STATUSES,
                            'value' => 2,
                            'options' => [
                                'id' => 'status_content'
                            ],
                        ]) ?>
                    </div>
                    <div id="lpt" class="form-group hidden">
                        <label class="control-label">Дата/время отложенной публикации</label>
                        <?= DateTimePicker::widget([
                            'name' => 'laterPublishTime',
                            'options' => ['id' => 'laterPublishTime'],
                            'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                            'removeButton' => false,
                            'value' => date('Y-m-d h:i'),
                            'pluginOptions' => [
                                'weekStart' => 1,
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd hh:ii'
                            ]
                        ]) ?>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label">Тип материала</label>
                        <?= Select2::widget([
                            'name' => 'type_content',
                            'data' => \app\modules\sites\SitesModule::getMaterialTypes(),
                            'options' => [
                                'id' => 'type_content'
                            ],
                        ]) ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Рубрика материала</label>
                        <?= Select2::widget([
                            'name' => 'content_material_rubric',
                            'data' => \app\modules\sites\SitesModule::getMaterialRubrics(),
                            'options' => [
                                'id' => 'content_material_rubric'
                            ],
                        ]) ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Автор</label>
                        <?= Select2::widget([
                            'name' => 'author_content',
                            'data' => \app\modules\sites\SitesModule::getAllUsers(),
                            'value' => Yii::$app->user->id,
                            'options' => [
                                'id' => 'author_content'
                            ],
                        ]) ?>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label" for="caption_content">Подпись: (фото, видео)</label>
                        <input type="text" id="caption_content" class="form-control" maxlength="150">
                    </div>
                    
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                              <input id="exclude_rss_yandex" type="checkbox" /> Исключить из экспорта в Яндекс RSS
                            </label>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                              <input id="content_is_chosen" type="checkbox" /> Избранная новость
                            </label>
                        </div>
                    </div>
                    
                    <div class="pull-right btn-group">
                        <button type="button" id="content_preview" class="btn btn-default btn-sm hidden">Предпросмотр</button>
                        <button type="button" id="content_publish" class="btn btn-primary btn-sm">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
        
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Теги</h3>
            </div>
            <div class="box-body">
                <?= yii\jui\AutoComplete::widget(['id' => 'tags_content']) ?>
                <button type="button" id="tags_content_add_tags" class="btn btn-default btn-sm">Добавить</button>
                <p class="text-muted">Теги разделяются запятыми</p>
                <ul id="tags_list"></ul>
            </div>
        </div>
        
        <div class="box box-primary">
            <?= Collapse::widget([
                'items' => [
                    [
                        'label' => 'Сюжет',
                        'content' => '',
                        'contentOptions' => ['class' => 'pk_content_stories_list']
                    ]
                ]
            ]);
            ?>
        </div>
        
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Изображение записи</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label class="control-label" for="headimglabel_content">Подпись</label>
                    <input type="text" id="headimglabel_content" class="form-control">
                </div>
                <div class="form-group">
                    <div id="head_img_src">
                        <img class="full-width" src="#" />
                    </div>
                </div>
                <button type="button" data-click="files-set-image" class="btn btn-default btn-sm open-library" data-toggle="modal" data-target="#library-files-modal">
                    Установить изображение записи
                </button>
            </div>
        </div>
    </div>
</div>

<!-- обрезка основного изображения https://fengyuanchen.github.io/jquery-cropper/ -->
<style>
.docs-preview {
    margin-right: -1rem;
}
.crop_img-preview {
    float: left;
    margin-bottom: .5rem;
    margin-right: .5rem;
    overflow: hidden;
}
.crop_preview-lg {
  width: 16rem;
  height: 9rem;
}

.crop_preview-md {
  width: 8rem;
  height: 4.5rem;
}
.image_container {
    max-height: 500px;
}
.image_container>img{
    max-width: 100%;
}
</style>
<div class="modal fade" id="headimg_crop" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-full-width" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Обрезка главного изображения</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="image_container">
                            <img id="blah" src="#" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="uploaded-files"></div>
                        
                        <div class="docs-preview clearfix">
                            <div class="crop_img-preview crop_preview-lg"></div>
                            <div class="crop_img-preview crop_preview-md"></div>
                            <div class="crop_img-preview crop_preview-sm"></div>
                        </div>
                        
                        <div class="">
                            <div id="crop_dimensions"></div>
                        </div>
                        

                        <div id="actions">
                            <div class="col-md-3 docs-rotates">
                                <div class="btn-group docs-aspect-ratios" data-toggle="buttons">

                                <label class="btn btn-primary">            
                                  <input type="radio" id="rotateRight" value="90">
                                  <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="Повернуть на 90 градусов">
                                    <span class="fa fa-rotate-left"></span>
                                  </span>
                                </label>
                                <label class="btn btn-primary">            
                                  <input type="radio" id="rotateLeft" value="90">
                                  <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="Повернуть на 90 градусов">
                                    <span class="fa fa-rotate-right"></span>
                                  </span>
                                </label>            

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" id="files-saveset-image-btn">Обрезать и выбрать</button>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerJsFile('@web/js/cropperjs/cropper.min.js',['depends' => 'app\assets\AppAsset','position' => yii\web\View::POS_END]);
Yii::$app->view->registerCssFile('@web/js/cropperjs/cropper.min.css');
?>
<!-- обрезка основного изображения -->

<div class="modal fade" id="widget-linked-content" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Вставка виджета связанной новости
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label">Новость:</label>
                    <?= Select2::widget([
                        'name' => 'linked_content',
                        'initValueText' => '',
                        'pluginOptions' => [
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => \yii\helpers\Url::to(['contentbystring']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(content) { return content.text; }'),
                            'templateSelection' => new JsExpression('function (content) { sitesModule.setPreviewWidgetLC(content); return content.text; }')
                        ],
                        'options' => [
                            'id' => 'linked_content'
                        ],
                    ]) ?>
                </div>
                
                <div class="form-group" id="preview-widget-lc"></div>
                
                <?php /*<div class="form-group">
                    <label class="control-label">Позиция:</label>
                    <br>
                    <label class="radio-inline">
                        <input type="radio" name="widget-lc-position" class="widget-lc-position" value="left">Слева
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="widget-lc-position" class="widget-lc-position" value="right">Справа
                    </label>
                </div>*/?>
                
                <button id="widget-linked-content-btn" class="btn btn-primary btn-sm" data-new="1">Добавить</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="library-files-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-full-width" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title pull-left" id="myModalLabel">
                    Медиафайлы
                </h4>
                <div class="progress" id="library-progress" style="margin-left: 125px; width: 50%;">
                    <div class="progress-bar progress-bar-success" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-1 files-choices">
                        <ul>
                            <li><a href="#" class="files-choice" id="files-add-files">Вставить медиафайл</a></li>                            
                            <li><a href="#" class="files-choice" id="files-create-gallery">Создать/обновить галерею</a></li>
                            <li><a href="#" class="files-choice" id="files-set-image">Установить изображение записи</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-9">
                        <ul class="nav nav-tabs" role="tablist">
                            <li>
                                <a href="#upload-tab" role="tab" data-toggle="tab">Загрузить файлы</a>
                            </li>
                            <li>
                                <a href="#innerlink-tab" role="tab" data-toggle="tab">Добавить по ссылке</a>
                            </li>
                            <li class="active">
                                <a href="#library-tab" id="library-tab-li" role="tab" data-toggle="tab">Библиотека файлов</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane" id="upload-tab">
                                <input type="file" accept="*" id="files" multiple="multiple">
                                <button id="file_upload" class="btn btn-primary">Загрузить</button>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="innerlink-tab">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="file_innerlink_link">Ссылка</label>
                                        <input id="file_innerlink_link" type="text" class="form-control" size="50">
                                    </div>
                                    <div class="form-group">
                                        <label for="file_innerlink_name">Название</label>
                                        <input id="file_innerlink_name" type="text" class="form-control" size="50">
                                    </div>
                                    <div class="form-group">
                                        <button id="file_add_inner_view" class="btn btn-default mr-5">Посмотреть</button>
                                        <button id="file_add_inner_link" class="btn btn-primary">Добавить</button>
                                    </div>
                                </div>
                                <div id="load_file_innerlink" class="col-md-6"></div>
                            </div>
                            <div role="tabpanel" class="tab-pane active" id="library-tab"></div>
                        </div>
                    </div>
                    <div class="col-lg-2" id="library-files-options">
                        <h5>Параметры файла</h5>
                        <div class="details">
                            <img src="#" id="file_preview" style="width:100%" />
                            <div class="filename text-bold"></div>
                            <div class="uploaded"></div>
                            <div class="file-size"></div>
                            <div class="dimensions"></div>
                        </div>
                        <button id="file_delete" class="btn btn-danger btn-xs">Удалить</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="pull-left">
                    Выбрано: <span id="files-selected-count"></span><br>
                    <button id="files-selected-clear" class="btn btn-danger btn-xs pull-left">Сброс</button>
                </div>
                
                <button id="files-add-files-btn" class="btn btn-primary btn-sm">Добавить</button>
                <button id="files-set-image-btn" class="btn btn-primary btn-sm">Перейти к 16/9</button>
                <button id="files-create-gallery-btn" class="btn btn-primary btn-sm">Создать галерею</button>
                <button id="files-change-gallery-btn" class="btn btn-primary btn-sm">Обновить галерею</button>
            </div>
        </div>
    </div>
</div>

<?php
$add = (isset($_GET['pk_content'])) ? 'sitesModule.afterloadEditor.push(registerLoadContent);' : '';
$this->registerJs("
    sitesModule.afterload.push(tags.registerAutocompleteTags, publishLaterTime, loadStories);
    afterConnectSocket.push(socketio.onLoad);
    " . $add . "
", \yii\web\View::POS_READY);