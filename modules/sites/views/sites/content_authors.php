<?php
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('app/views', 'sites Content Authors');
$this->params['breadcrumbs'][] = $this->title;
?>

<h2><?= $this->title ?></h2>

<div class="row">        
    <div class="col-md-9">
        <div class="mb-1em">
            <?= Html::a(Yii::t('app/views', 'sites Create Content Author'), ['createcontentauthor'], ['class' => 'btn btn-success btn-sm']) ?>
        </div>
    </div>
</div>

<?= GridView::widget([
    'id' => 'content-grid',
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'lastname_content_author',
            'label' => 'Фамилия',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a($model['lastname_content_author'], ['/user/user/profile/', 'id' => $model['pk_content_author']], ['target' => '_blank']);
            }
        ],
        [
            'attribute' => 'name_content_author',
            'label' => 'Имя',
            'format' => 'raw',
            'value' => function ($model) {
                return $model['name_content_author'];
            }
        ],
        [
            'attribute' => 'secondname_content_author',
            'label' => 'Отчество',
            'format' => 'raw',
            'value' => function ($model) {
                return $model['secondname_content_author'];
            }
        ],
        
        /*[
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}',
            'buttons'=>[
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['createcontent', 'pk_content' => $model['pk_content']]);
                }
            ],
        ]*/
    ],
]); ?>