<?php
use yii\grid\GridView;
use app\modules\sites\SitesModule;
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use app\modules\user\models\User;
use yii\widgets\Pjax;

$this->title = Yii::t('app/views', 'sites View content');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">        
    <div class="col-md-9">
        <?php if($dataErrors = $dataProvider->getErrors()) : ?>
            <div class="alert alert-danger" role="alert"><?= implode(' ', $dataErrors) ?></div>
        <?php endif ?>
        
        <div class="mb-1em">
            <?= Html::a(Yii::t('app/views', 'sites Create content'), ['createcontent'], ['class' => 'btn btn-success btn-sm']) ?>
            <button id="delete-selected" class="btn btn-danger btn-sm">Удалить выбранные</button>
            <button id="clear-cache" class="btn btn-danger btn-sm ml-1em"><?= Yii::t('app/views', 'sites Clear cache') ?></button>
        </div>
        
        <div>
            <a href="<?= Url::to(['', 'status' => 0]) ?>" class="btn <?= ($status === 0) ? 'btn-primary' : 'btn-link' ?> btn-sm">Все <span class="badge"><?= $counts['countstatus0'] ?></span></a>
            <a href="<?= Url::to(['', 'status' => 1]) ?>" class="btn <?= ($status === 1) ? 'btn-primary' : 'btn-link' ?> btn-sm"><?= SitesModule::CONTENT_STATUSES[1] ?> <span class="badge"><?= $counts['countstatus1'] ?></span></a>
            <a href="<?= Url::to(['', 'status' => 2]) ?>" class="btn <?= ($status === 2) ? 'btn-primary' : 'btn-link' ?> btn-sm"><?= SitesModule::CONTENT_STATUSES[2] ?> <span class="badge"><?= $counts['countstatus2'] ?></span></a>
            <a href="<?= Url::to(['', 'status' => 3]) ?>" class="btn <?= ($status === 3) ? 'btn-primary' : 'btn-link' ?> btn-sm"><?= SitesModule::CONTENT_STATUSES[3] ?> <span class="badge"><?= $counts['countstatus3'] ?></span></a>
        </div>
    </div>
</div>

<?php
Pjax::begin() ;
echo GridView::widget([
    'id' => 'content-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\CheckboxColumn'],
        
        [
            'attribute' => 'title_content',
            'label' => 'Заголовок',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a($model['title_content'], ['createcontent', 'pk_content' => $model['pk_content']]);
            }
        ],
        [
            'attribute' => 'text_content',
            'label' => 'Текст',
            'format' => 'raw',
            'value' => function ($model) {
                return '-//-'; // текст большой, не буду выводить
            }
        ],
        [
            'attribute' => 'fk_user_created',
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'fk_user_created',
                'data' => User::getAllActive(),
                'options' => ['placeholder' => ''],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]),
            'label' => 'Автор',
            'value' => function ($model) {
                return User::getAll($model['fk_user_created'], '{lastname} {name}');
            }
        ],
        [
            'attribute' => 'create_date',
            'filter' => DatePicker::widget([
                'model' => $searchModel, 
                'attribute' => 'create_date',
                'removeButton' => [
                    'icon'=>'remove',
                ],
                'options' => ['placeholder' => ''],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]),
            'label' => 'Создано',
            'value' => function ($model) {
                return Yii::$app->formatter->asDatetime($model['create_date']);
            }
        ],
        [
            'attribute' => 'publish_date',
            'filter' => DatePicker::widget([
                'model' => $searchModel, 
                'attribute' => 'publish_date',
                'removeButton' => [
                    'icon'=>'remove',
                ],
                'options' => ['placeholder' => ''],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]),
            'label' => 'Время публикации',
            'value' => function ($model) {
                return Yii::$app->formatter->asDatetime($model['publish_date']);
            }
        ],
        [
            'attribute' => 'update_date',
            'filter' => DatePicker::widget([
                'model' => $searchModel, 
                'attribute' => 'update_date',
                'removeButton' => [
                    'icon'=>'remove',
                ],
                'options' => ['placeholder' => ''],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]),
            'label' => 'Обновлено',
            'format' => 'raw',
            'value' => function ($model) {
                return Yii::$app->formatter->asDatetime($model['update_date']);
            }
        ],
        [
            'label' => 'Статус',
            'value' => function ($model) {
                return SitesModule::shownStatuses($model['status_content'], $model['publish_date']);
            }
        ],
        
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}',
            'buttons'=>[
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['createcontent', 'pk_content' => $model['pk_content']]);
                }
            ],
        ]
    ],
]); 
Pjax::end(); ?>