<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\date\DatePicker;

$this->title = Yii::t('app/views', 'sites Banners');
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
.tof {
    height: 160px;
    border: 1px solid;
    margin-bottom: 15px;
}

.tofill {
    background-color: #f2d577;
}

.showing {
    background: rgba(92, 0, 255, 0.09);
}
</style>

<div class="container-fluid">
    <h1><?= $this->title ?></h1>
    
    <div class="col-md-6 pull-left">
        <div class="row" id="listenToFill">
            <div class="col-md-12 tofill tof" data-id="1">№1 сквозной (908х140)</div>
            <div class="col-md-12 tof"></div>
            <div class="col-md-12 tofill tof" data-id="2">№2 (908x100)</div>
            <div class="col-md-4 tofill tof" data-id="3">№3 замена новости ~(287x451)</div><div class="col-md-4 tof"></div><div class="col-md-4 tof"></div>
            <div class="col-md-8 tof"></div><div class="col-md-4 tofill tof" data-id="4">№4 замена новости ~(287x451)</div>
            <div class="col-md-4 tof"></div><div class="col-md-4 tof"></div><div class="col-md-4 tof"></div>
            <div class="col-md-12 tofill tof" data-id="5">№5 сквозной (908x143)</div>
            <div class="col-md-6 tof"></div><div class="col-md-6 tof"></div>
        </div>
    </div>
    
    <div class="col-md-6">
        <h3>Информация о баннере:</h3>
        
        <ul id="info_banner">
            
        </ul>
        
        <div class="clearfix"></div>
        
        <h3>Форма заполнения</h3>
            <button id="clear_data_banner" class="btn btn-default" style="display: none;">Сбросить</button>
        
            <div class="form-group">
              <label for="exampleInputEmail1">Скрипт</label>
              <textarea id="script_banner" class="form-control" rows="6" placeholder="Скрипт"></textarea>
            </div>
            <input type="hidden" value="" id="id_banner">
            <div class="form-group">
              <?php
                echo '<label class="control-label">Дата с</label>';
                echo DatePicker::widget([
                    'name' => 'date_since',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'value' => date('Y-m-d'),
                    'options' => [
                        'id' => 'date_since'
                    ],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]);
            ?>
            </div>
            <div class="form-group">
              <?php
                echo '<label class="control-label">Дата до</label>';
                echo DatePicker::widget([
                    'name' => 'date_to',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'value' => date('Y-m-d'),
                    'options' => [
                        'id' => 'date_to'
                    ],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]);
            ?>
            </div>
            
            <button id="save_banner" class="btn btn-primary">Сохранить</button>
    </div>
</div>
<?php
Yii::$app->view->registerJsFile('@web/js/build/sites_banners.js', ['depends' => 'app\assets\AppAsset', 'position' => yii\web\View::POS_END]);