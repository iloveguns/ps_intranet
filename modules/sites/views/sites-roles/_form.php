<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="sites-roles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    
    <p><?= Yii::t('app/models', 'List rights') ?> :</p>
    <?php
    foreach ($modelRights->attributeLabels() as $name => $attr) {
        echo $form->field($modelRights, $name)->checkBox();
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>