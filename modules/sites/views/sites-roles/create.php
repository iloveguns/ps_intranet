<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Create Sites Roles');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Sites Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sites-roles-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelRights' => $modelRights
    ]) ?>

</div>