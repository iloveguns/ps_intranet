<?php
use yii\helpers\Url;

$this->title = Yii::t('app/views', 'sites Main page');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="grid-view">
    
    <h2>
        Последние главные страницы:
    </h2>
    
    <a class="btn btn-success" href="<?= Url::to('mainpagecrud') ?>">Создать новую</a>
    
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>Дата</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dataProvider->getModels()['data'] as $info) : ?>
            <tr>
                <td>
                    <a href="<?= Url::to(['mainpagecrud', 'id_mp' => $info['id_index_page']]) ?>">
                        <?= Yii::$app->formatter->asDate($info['date']) ?>
                    </a>
                </td>
            </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>