<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\date\DatePicker;

$this->title = Yii::t('app/views', 'sites Main page');
$this->params['breadcrumbs'][] = $this->title;
/*
?>
<div class="container-fluid">
    <h1><?= $this->title ?></h1>

    <div>
        <a class="btn btn-default" id="save-grid" href="#">Save Grid</a>
        <a class="btn btn-default" id="load-grid" href="#">Load Grid</a>
        <a class="btn btn-default" id="clear-grid" href="#">Clear Grid</a>
        
        <button class="btn btn-primary gr-add_widget" data-x="12" data-y="1">12x1</button>
        <button class="btn btn-primary gr-add_widget" data-x="6" data-y="1">6x1</button>
        <button class="btn btn-primary gr-add_widget" data-x="4" data-y="1">4x1</button>
        <button class="btn btn-primary gr-add_widget" data-x="3" data-y="1">3x1</button>
    </div>

    <br/>

    <div class="grid-stack pull-left"></div>
    <div class="col-md-4 pull-left">
        <?php
        Pjax::begin() ;
        echo \yii\grid\GridView::widget([
            'id' => 'content-grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'title_content',
                    'label' => 'Заголовок',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a($model['title_content'], ['createcontent', 'pk_content' => $model['pk_content']], ['target' => '_blank']);
                    }
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update}',
                    'buttons'=>[
                        'update' => function ($url, $model) {
                            return Html::button('<span class="glyphicon glyphicon-ok"></span>', 
                                [
                                    'class' => 'btn btn-primary gr-set-content-to-block',
                                    'data-pk_content' => $model['pk_content'],
                                    'data-title_content' => $model['title_content'],
                                    'data-img_content' => $model['headimgsrc_content']
                                ]
                            );
                        }
                    ],
                ]
            ],
        ]);
        Pjax::end();
    ?>
    </div>

    <div class="clearfix"></div>

    <textarea id="saved-data" cols="100" rows="20" readonly="readonly"></textarea>
</div>

<?php
/*yii\jui\JuiAsset::register($this); // jquery ui
Yii::$app->view->registerJsFile('@web/js/build/lodash.js',['depends' => 'app\assets\AppAsset', 'position' => yii\web\View::POS_END]);
Yii::$app->view->registerJsFile('@web/js/build/gridstack.js',['depends' => 'app\assets\AppAsset', 'position' => yii\web\View::POS_END]);
 */?>


<div class="container-fluid">
    <h1><?= $this->title ?></h1>
    
    <div class="col-md-6 pull-left">
        <div class="row" id="listenToFill">
            <div class="col-md-12 tofill"></div>
            <div class="col-md-4 tofill"></div><div class="col-md-4 tofill"></div><div class="col-md-4 tofill"></div>
            <div class="col-md-8 tofill"></div><div class="col-md-4 tofill"></div>
            <div class="col-md-4 tofill"></div><div class="col-md-4 tofill"></div><div class="col-md-4 tofill"></div>
            <div class="col-md-6 tofill"></div><div class="col-md-6 tofill"></div>
        </div>
    </div>
    
    <div class="col-md-4 pull-left">
        <?php
            echo '<label class="control-label">Дата</label>';
            echo DatePicker::widget([
                'name' => 'date_mainPage',
                'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                'value' => date('Y-m-d'),
                'options' => [
                    'id' => 'date_mainPage'
                ],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);
        ?>
        <hr>
        
        <?php
        Pjax::begin() ;
        echo \yii\grid\GridView::widget([
            'id' => 'content-grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'title_content',
                    'label' => 'Заголовок',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a($model['title_content'], ['createcontent', 'pk_content' => $model['pk_content']], ['target' => '_blank']);
                    }
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update}',
                    'buttons'=>[
                        'update' => function ($url, $model) {
                            return Html::button('<span class="glyphicon glyphicon-ok"></span>', 
                                [
                                    'class' => 'btn btn-primary gr-set-content-to-block',
                                    'data-pk_content' => $model['pk_content'],
                                    'data-title_content' => $model['title_content'],
                                    'data-img_content' => $model['headimgsrc_content']
                                ]
                            );
                        }
                    ],
                ]
            ],
        ]);
        Pjax::end();
    ?>
    </div>

    <div class="clearfix"></div>
    
    <button id="save-mainPage" class="btn btn-primary">Сохранить</button>
</div>

<style>
.tofill {
    height: 160px;
    border: 1px solid;
    margin-bottom: 15px;
}

.showing {
    background: rgba(92, 0, 255, 0.09);
}
</style>

<?php if($mode === 'update') : // выкинуть данные в js ?>
    <script>
        var mainPageData = JSON.parse('<?= json_encode($mainPageData) ?>');
    </script>
    <?php Yii::$app->view->registerJs('setMainPageData(mainPageData);', \yii\web\View::POS_READY); ?>
<?php endif ?>

<?php
Yii::$app->view->registerJsFile('@web/js/build/sites_main_page.js', ['depends' => 'app\assets\AppAsset', 'position' => yii\web\View::POS_END]);
