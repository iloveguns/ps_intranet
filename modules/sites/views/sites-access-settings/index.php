<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use app\modules\sites\models\SitesRoles;

$this->title = Yii::t('app/views', 'Sites Access Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sites-access-settings-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app/views', 'Create Sites Access Settings'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'fk_user',
                'value' => function ($model, $key, $index, $column) {
                    return Html::a(app\modules\user\models\User::getAllActive($model->fk_user), ['/user/user/profile', 'id' => $model->fk_user], ['target' => '_blank']);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'fk_user',
                    'data' => app\modules\user\models\User::getAllActive(),
                    'options' => ['placeholder' => Yii::t('app', 'Select'),],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'format' => 'raw',
            ],
            [
                'attribute' => 'fk_role',
                'value' => function ($model, $key, $index, $column) {
                    return SitesRoles::getAll($model->fk_role);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'fk_role',
                    'data' => SitesRoles::getAll(),
                    'options' => ['placeholder' => Yii::t('app', 'Select'),],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'format' => 'raw',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?>
</div>