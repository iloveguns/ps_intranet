<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Create Sites Access Settings') . ' : ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Sites Access Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app/views', 'Update');
?>
<div class="sites-access-settings-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>