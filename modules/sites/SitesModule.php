<?php

namespace app\modules\sites;

use Yii;
use app\modules\sites\models\SitesAccessSettings;
use app\modules\user\models\User;

class SitesModule extends \yii\base\Module {
    public static $idSite;
    
    public static $_usersCanAccess;
    
    public $controllerNamespace = 'app\modules\sites\controllers';
    
    const COOKIE_SITE = 'sel_site'; // апи с проверкой доступа
    const LAPI_URL = 'http://localhost:3001/api'; // для разработки
    const API_URL = 'http://109.195.33.205/api'; // закрытый api
    //const API_URL = self::LAPI_URL;
    const PUBLIC_API_URL = 'http://109.195.33.205/papi'; // публичный api
    const PUBLIC_FILES_URL = 'http://static.politsib.ru/papi'; // публичный api
    const CONTENT_STATUSES = [1 => 'Опубликован', 2 => 'Черновик', 3 => 'Отложенная публикация'];
    
    const DEFAULT_STATUSES = [0 => 'Не активен', 1 => 'Активен'];
    
    public function init() {
        parent::init();
        Yii::$app->homeUrl = '/sites/sites/index';
        
        if(isset($_GET[self::COOKIE_SITE])) {//метка смены сайта
            self::$idSite = $_GET[self::COOKIE_SITE];
            self::setCookieSite(self::$idSite);
            return Yii::$app->response->redirect(Yii::$app->request->referrer);
        } else if($selected_site_cookie = \Yii::$app->getRequest()->getCookies()->getValue(self::COOKIE_SITE)) {//выбран сайт, возможно другой
            self::$idSite = $selected_site_cookie;
        } else if(\Yii::$app->getRequest()->getCookies()->getValue(SitesModule::COOKIE_SITE) === NULL) { // куки нет
            self::$idSite = 1; // какой-то начальный сайт, должен с таким ид быть
            self::setCookieSite(self::$idSite);
        }
        
        //все необходимое сразу
        Yii::$app->view->registerJsFile('@web/js/build/sites_module.js?v=' . date('Ymdh'),['depends' => 'app\assets\AppAsset', 'position' => yii\web\View::POS_END]);
        Yii::$app->view->registerJsFile('@web/js/build/nestable.js',['depends' => 'app\assets\AppAsset', 'position' => yii\web\View::POS_END]);
        Yii::$app->view->registerCssFile('@web/css/build/nestable.css');
    }
    
    public function setCookieSite($val){
        $cookie = new \yii\web\Cookie([
            'name' => self::COOKIE_SITE,
            'value' => $val,
            'expire' => time() + 86400 * 3,
        ]);
        \Yii::$app->getResponse()->getCookies()->add($cookie);
    }
    
    /*
     * найти всех, кто может заходить в сайт, из настроек доступа
     */
    public function getAllUsers($id = NULL, $format = '{lastname} {name} {secondname}') {
        $dependency = new \yii\caching\ExpressionDependency();
        $dependency->expression = $format === '{lastname} {name} {secondname}';
        
        $b = User::getDb()->cache(function ($db) use ($format) {
            $b = [];
            
            $allAdded = SitesAccessSettings::findWDep()->all();
            foreach ($allAdded as $oneAdded) {
                $b[$oneAdded['fk_user']] = $oneAdded->fkUser->getFullName($format);
            };
            asort($b);
            
            return $b;
        }, 3600, $dependency);
        
        if(!empty($id) && !isset($b[$id])) return User::getAll($id);//уволен например - все равно выдать инфу, во избежание ошибки
        if($id) return $b[$id];
        return $b;
    }
    
    /*
     * проверка на доступ в црм
     * UPD настройка входа отдельно от интранета
     */
    public function haveAccess($id_user = NULL) {
        if(Yii::$app->user->isGuest) return false;
        if(Yii::$app->user->identity->isAdmin()) return true;
        
        if(!$id_user) $id_user = Yii::$app->user->id;
        
        //найти совпадение в таблице допуска, если есть - значит разрешить
        $access = models\SitesAccessSettings::find()
            ->andWhere(['fk_user' => $id_user])
            ->one();
        
        if($access) return true;
        return false;
    }
    
    /*
     * имеет ли все права на доступ
     * поиск сразу по роли руководителя отдела
     */
    public function canAccess($id_user = NULL) {
        if($id_user === NULL) $id_user = Yii::$app->user->id;
        
        // может и так быть
        if($id_user === NULL) return false;
        if(Yii::$app->user->identity->isAdmin()) return true;
        
        if(!isset(self::$_usersCanAccess[$id_user])) {
            $access = models\SitesAccessSettings::find()
                ->asArray()
                ->andWhere(['fk_site' => self::$idSite, 'fk_user' => $id_user])
                //->andWhere(['like', 'json_data', 'head_dep_crm'])//CrmRights
                ->one();
            
            // если к этому нет доступа - выяснить куда есть и сделать сайт активным
            if($access === NULL) {            
                $access = models\SitesAccessSettings::find()
                    ->asArray()
                    ->andWhere(['fk_user' => $id_user])
                    ->one();

                self::$idSite = (int)$access['fk_site'];
                self::setCookieSite(self::$idSite);
            }
            
            self::$_usersCanAccess[$id_user] = ($access) ? true : false;
        }
        
        return self::$_usersCanAccess[$id_user];
    }
    
    /**
     * получить типы материалов
     * @return {Array}
     */
    public function getMaterialTypes() {
        $dataProvider = new \app\helpers\ApiSitesDataProvider([
            'urlpath' => '/material_types',
            'pk' => 'pk_content',
            'simple' => true
        ]);
        
        $dataProvider->prepare();
        
        $return = [];
        
        foreach ($dataProvider->getModels() as $m) {
            $return[$m['pk_material_type']] = $m['name_material_type'];
        }
        
        return $return;
    }
    
    /**
     * получить темы материалов
     * @return {Array}
     */
    public function getMaterialRubrics() {
        $dataProvider = new \app\helpers\ApiSitesDataProvider([
            'params' => [
                'fk_site' => self::$idSite
            ],
            'urlpath' => '/material_rubrics',
            'pk' => 'pk_content',
            'simple' => true
        ]);
        
        $dataProvider->prepare();
        
        $return = [];
        
        foreach ($dataProvider->getModels() as $m) {
            $return[$m['pk_material_rubric']] = $m['name_material_rubric'];
        }
        
        return $return;
    }
    
    /**
     * статусы на вывод
     * @param {int} $pk_status - ид статуса
     * @return String
     */
    public function shownStatuses(int $pk_status, $publish_date) {
        $r = self::CONTENT_STATUSES[$pk_status];
        
        if($pk_status === 3) {
            $r .= ' (' . Yii::$app->formatter->asDateTime($publish_date) . ')';
        }
        return $r;
    }
}
