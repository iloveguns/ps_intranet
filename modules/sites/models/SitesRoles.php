<?php

namespace app\modules\sites\models;

use Yii;

/*
 * Класс для ролей
 */
class SitesRoles extends \yii\db\ActiveRecord {
    public static function tableName() {
        return 'sites_roles';
    }

    public function rules() {
        return [
            [['name', 'json_data'], 'required'],
            [['json_data'], 'string'],
            [['name'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels() {
        return [
            'pk_role' => Yii::t('app/models', 'Pk Role'),
            'name' => Yii::t('app/views', 'sites Name role'),
            'json_data' => Yii::t('app/models', 'Json Data'),
        ];
    }

    public function getSitesAccessSettings() {
        return $this->hasMany(SitesAccessSettings::className(), ['fk_role' => 'pk_role']);
    }
    
    /*
     * список всех ролей
     * $id - получить по ид
     */
    public function getAll($id = null) {
        $all = SitesRoles::find()->asArray()->all();
        $b = \yii\helpers\ArrayHelper::map($all, 'pk_role', 'name');
        if($id) return $b[$id];
        return $b;
    }
}