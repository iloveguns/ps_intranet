<?php

namespace app\modules\sites\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\sites\models\SitesRoles;

class SitesRolesSearch extends SitesRoles{
    public function rules() {
        return [
            [['pk_role'], 'integer']
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = SitesRoles::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'pk_role' => $this->pk_role,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}