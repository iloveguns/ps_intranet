<?php

namespace app\modules\sites\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\sites\models\SitesAccessSettings;

class SitesAccessSettingsSearch extends SitesAccessSettings {
    public function rules() {
        return [
            [['id', 'fk_user', 'fk_site', 'fk_role'], 'integer'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = SitesAccessSettings::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'fk_user' => $this->fk_user,
            'fk_site' => $this->fk_site,
            'fk_role' => $this->fk_role,
        ]);

        return $dataProvider;
    }
}