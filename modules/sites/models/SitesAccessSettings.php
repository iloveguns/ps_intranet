<?php

namespace app\modules\sites\models;

use Yii;
use app\modules\user\models\User;
use app\modules\sites\models\SitesRoles;

/*
 * Класс для настроек доступа
 */
class SitesAccessSettings extends \yii\db\ActiveRecord {
    public static function tableName() {
        return 'sites_access_settings';
    }

    public function rules() {
        return [
            [['fk_user', 'fk_site', 'fk_role'], 'required'],
            [['fk_user', 'fk_site', 'fk_role'], 'integer'],
            [['fk_role'], 'exist', 'skipOnError' => true, 'targetClass' => SitesRoles::className(), 'targetAttribute' => ['fk_role' => 'pk_role']],
            [['fk_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_user' => 'id_user']],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'fk_user' => Yii::t('app/models', 'Id User'),
            'fk_site' => Yii::t('app/models', 'Fk Site'),
            'fk_role' => Yii::t('app/models', 'Fk Role'),
        ];
    }

    public function getFkRole() {
        return $this->hasOne(SitesRoles::className(), ['pk_role' => 'fk_role']);
    }

    public function getFkUser() {
        return $this->hasOne(User::className(), ['id_user' => 'fk_user']);
    }
    
    public function beforeValidate() {
        if (parent::beforeValidate()) {
            $this->fk_site = \app\modules\sites\SitesModule::$idSite;
            return true;
        }
        
        return false;
    } 
    
    /*
     * найти с отделом
     */
    public static function findWDep() {
        return self::find()->andFilterWhere([self::tableName().'.fk_site' => \app\modules\sites\SitesModule::$idSite]);
    }
}