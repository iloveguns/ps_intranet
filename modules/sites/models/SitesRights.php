<?php
namespace app\modules\sites\models;

use Yii;

class SitesRights extends \yii\base\Model {
    public //1 шаг
        $access
        ;
    
    public function rules()
    {
        return [
            [['access'], 'integer'],//2шаг 
        ];
    }

    public function attributeLabels()
    {
        return [//3 шаг
            'access' => Yii::t('app/models', 'sites Role access')
        ];
    }
    
    /*
     * убрать пустые значения перед сохранением в json ролей
     */
    public function deleteEmptysToJson() {
        $a = [];
        foreach ($this->attributes as $name => $val) {
            if($val){//если есть значение
                $a[$name] = $val;
            }
        }
        return json_encode($a);
    }
}