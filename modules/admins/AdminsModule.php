<?php

namespace app\modules\admins;
use Yii;

class AdminsModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\admins\controllers';

    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }
    
    public function registerTranslations()
    {
        Yii::$app->i18n->translations['modules/admins/*'] = [
            'class'          => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath'       => '@app/modules/admins/messages',
            'fileMap'        => [
                'modules/admins/all' => 'all.php',
            ],
        ];
    }
    
    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('modules/admins/' . $category, $message, $params, $language);
    }
}
