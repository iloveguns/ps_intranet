<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Create Holidays');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Holidays'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="holidays-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>