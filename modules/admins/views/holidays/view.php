<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admins\models\Holidays */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Holidays'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="holidays-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app/views', 'Update'), ['update', 'id' => $model->pk_holiday], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app/views', 'Delete'), ['delete', 'id' => $model->pk_holiday], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app/views', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'pk_holiday',
            'title:ntext',
            'text:ntext',
            'img:ntext',
            'date',
        ],
    ]) ?>

</div>