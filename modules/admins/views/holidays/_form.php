<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\FunctionModel;

?>

<div class="holidays-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    изображения пока недоступны
    <?php // $form->field($model, 'img')->textarea(['rows' => 6]) ?>

    <div class="row">
        <?= $form->field($model, 'monthDate', ['options' => ['class' => 'col-md-3']])->dropDownList(FunctionModel::getMonths()) ?>

        <?= $form->field($model, 'dayDate', ['options' => ['class' => 'col-md-3']])->dropDownList(FunctionModel::getDays()) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app/views', 'Create') : Yii::t('app/views', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>