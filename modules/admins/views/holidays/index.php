<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = Yii::t('app/models', 'Holidays');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row row-cols holidays">
    <h1><?= $this->title ?></h1>
    
    <div class="col-md-6">
        <div class="box">
            <?= \yii2fullcalendar\yii2fullcalendar::widget([
                'options' => [
                    'lang' => 'ru',
                ],
                'ajaxEvents' => Url::to(['jsoncalendar']),
                'eventRender' => "function(event, element, view) { return $('<div class=\'fc-content\'>' + event.title + '</div>'); }",//фотки выдает норм
                'eventAfterAllRender' => "function(){ $(window).resize(); }",//WTF?
            ]);?>
        </div>
    </div>
    <div class="col-md-6">
        <?php if(Yii::$app->user->identity->can('crud_holidays')) : ?>
        <p>
            <?= Html::a(Yii::t('app/views', 'Create Holidays'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'pk_holiday',
                'title:ntext',
                'text:ntext',
                //'img:ntext',
                'date',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php endif ?>
    </div>
</div>