<?php
use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\admins\AdminsModule;

use app\modules\intranet\models\DbpartnersClient;
use app\modules\intranet\models\DbpartnersCongratulations;

$this->title = Yii::t('app/models', 'System');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <div class="container-fluid">       
        <p>
            <a href="/admins/system/flush-cache" class="btn btn-default long-load"><i class="glyphicon glyphicon-flash"></i> Сбросить кэш</a>
        </p>
        <p>
            <a href="/admins/system/clear-filesfolder" class="btn btn-default long-load"><i class="fa fa-file-text-o"></i> Очистить потерянные файлы</a>
        </p>
        <p>
            <a href="/admins/system/clear-assets" class="btn btn-default long-load"><i class="fa fa-external-link-square" aria-hidden="true"></i> Очистить assets</a>
        </p>
        <p>
            <a href="/admins/system/delete-unuse-photo-users" class="btn btn-default long-load"><i class="fa fa-users" aria-hidden="true"></i> Удалить потерянные фото сотрудников</a>
        </p>
        <p>
            <a href="/admins/system/delete-thumbs" class="btn btn-default long-load"><i class="fa fa-picture-o" aria-hidden="true"></i> Очистить миниатюры</a>
        </p>
        <p>
            <a href="/admins/system/backup-create" class="btn btn-default long-load"><i class="fa fa-floppy-o" aria-hidden="true"></i> Создать бэкап</a>
        </p>
    </div>
</div>
