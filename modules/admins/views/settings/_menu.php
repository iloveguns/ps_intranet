<?php
use yii\helpers\Url;
use app\modules\admins\AdminsModule;

$action = $this->context->action->id;
?>
<ul class="nav nav-pills">
    <li <?= ($action === 'index') ? 'class="active"' : '' ?>>
        <a href="<?=  Url::toRoute(['/admins/settings'])?>">
            <?php if($action === 'edit') : ?>
                <i class="glyphicon glyphicon-chevron-left font-12"></i>
            <?php endif; ?>
            <?= AdminsModule::t('all', 'List settings') ?>
        </a>
    </li>
    <li <?= ($action === 'create') ? 'class="active"' : '' ?>><a href="<?= Url::to(['/admins/settings/create']) ?>"><?= AdminsModule::t('all', 'Create setting') ?></a></li>
</ul>
<br/>