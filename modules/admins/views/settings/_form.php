<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Settings;
?>
<?php $form = ActiveForm::begin(['enableAjaxValidation' => true]); ?>
<?= $form->field($model, 'name')->textInput(!$model->isNewRecord ? ['disabled' => 'disabled'] : []) ?>
<?= $form->field($model, 'visibility')->checkbox(['uncheck' => Settings::VISIBLE_ALL]) ?>
<?= $form->field($model, 'title')->textarea() ?>
<?= $form->field($model, 'value')->textarea() ?>

<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>