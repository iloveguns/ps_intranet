<?php
use app\modules\admins\AdminsModule;
$this->title = AdminsModule::t('all', 'Edit setting');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>