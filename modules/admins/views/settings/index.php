<?php
use app\models\Settings;
use yii\helpers\Url;
use app\modules\admins\AdminsModule;

$model = new Settings();
$this->title = AdminsModule::t('all', 'Settings');
?>

<?= $this->render('_menu') ?>

<?php if($data->count > 0) : ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th width="50">#</th>
            <th><?= $model->getAttributeLabel('name') ?></th>
            <th><?= $model->getAttributeLabel('title') ?></th>
            <th><?= $model->getAttributeLabel('value') ?></th>
            <th width="30"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($data->models as $setting) : ?>
            <tr <?php if($setting->visibility == Settings::VISIBLE_ROOT) echo 'class="warning"'?>>
                <td><?= $setting->primaryKey ?></td>
                <td><?= $setting->name ?></td>
                <td><a href="<?= Url::to(['/admins/settings/update', 'id' => $setting->primaryKey]) ?>" title="<?= AdminsModule::t('all', 'Edit') ?>"><?= $setting->title ?></a></td>
                <td style="overflow: hidden"><?= $setting->value ?></td>
                <td><a href="<?= Url::to(['/admins/settings/delete', 'id' => $setting->primaryKey]) ?>" class="glyphicon glyphicon-remove confirm-delete" title="<?= AdminsModule::t('all', 'Delete item') ?>"></a></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
        <?= yii\widgets\LinkPager::widget([
            'pagination' => $data->pagination
        ]) ?>
    </table>
<?php else : ?>
    <p><?= AdminsModule::t('all', 'No records found') ?></p>
<?php endif; ?>