<?php
use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\admins\AdminsModule;

$this->title = AdminsModule::t('all', 'Login Forms');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-form-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model, $index, $widget, $grid){
            return (!$model->success) ? ['class'=>'text-red'] : ['class'=>'text-green'];            
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'log_id',
                'label' => 'ID',
            ],
            [
                'attribute' => 'input_email',
                'label' => AdminsModule::t('all','input_email'),
            ],
            [
                'attribute' => 'input_password',
                'label' => AdminsModule::t('all','input_password'),
            ],
            [
                'attribute' => 'ip',
                'label' => 'IP',
            ],
            [
                'attribute' => 'user_agent',
                'label' => AdminsModule::t('all','user_agent'),
            ],
            'time:datetime',
            [
                'attribute' => 'success',
                'label' => AdminsModule::t('all','success'),
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
            ],
        ],
    ]); ?>

</div>
