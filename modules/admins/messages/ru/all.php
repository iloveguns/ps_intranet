<?php
return[
    'Login Forms' => 'Логи входа',
    'input_email' => 'Email',
    'input_password' => 'Пароль',
    'user_agent' => 'User Agent',
    'success' => 'Успешный вход',
    
    'Settings' => 'Настройки',
    'List settings' => 'Список настроек',
    'No records found' => 'Ничего не найдено',
    'Create setting' => 'Создание настройки',
    'Setting created' => 'Настройка создана',
    'Create error' => 'Ошибка создания',
    'Edit setting' => 'Редактирование настройки',
    'Setting deleted' => 'Настройка удалена',
    'Setting updated' => 'Настройка отредактирована',
    'Update error' => 'Ошибка редактирования'
];

