<?php
namespace app\modules\admins\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;
use app\models\Settings;
use app\modules\admins\AdminsModule;
use app\behaviors\CacheFlush;

class SettingsController extends Controller
{
    public $rootActions = ['create', 'delete'];
    
    public function actionIndex()
    {
        $data = new ActiveDataProvider([
            'query' => Settings::find()->where(['>=', 'visibility', Settings::VISIBLE_ROOT]),
        ]);

        return $this->render('index', [
            'data' => $data
        ]);
    }

    public function actionCreate()
    {
        $model = new Settings();

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
                if($model->save()){
                    Yii::$app->session->addFlash('success', AdminsModule::t('all', 'Setting created'));
                    return $this->redirect('/admins/settings');
                }
                else{
                    Yii::$app->session->addFlash('error', AdminsModule::t('all', 'Create error'));
                    return $this->refresh();
                }
            }
        }
        else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = Settings::findOne($id);

        if($model === null){
            Yii::$app->session->addFlash('error', Yii::t('easyii', 'Not found'));
            return $this->redirect(['/admins/settings']);
        }

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
                if($model->save()){
                    Yii::$app->session->addFlash('success', AdminsModule::t('all', 'Setting updated'));
                }
                else{
                    Yii::$app->session->addFlash('error', AdminsModule::t('all', 'Update error'));
                }
                return $this->redirect(['/admins/settings']);
            }
        }
        else {
            return $this->render('edit', [
                'model' => $model
            ]);
        }
    }

    public function actionDelete($id)
    {
        if(($model = Settings::findOne($id))){
            Yii::$app->session->addFlash('success', AdminsModule::t('all', 'Setting deleted'));
            $model->delete();
        } 
        return $this->redirect(Yii::$app->request->referrer);
    }
}