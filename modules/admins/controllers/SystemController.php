<?php

namespace app\modules\admins\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Html;
use app\models\FunctionModel;
use app\models\UploadFiles;
//socket.emit('letShellServer', {'command' : 'ls', 'delay' : 5000});

set_time_limit(0);

class SystemController extends Controller {    
    public function actionIndex() {
        
        return $this->render('index');
    }
    
    public function actionTest() {
        return $this->render('test');
    }
    
    public function actionDeleteThumbs() {
        $countfiles = $allsizeclear = 0;
        
        //физические файлы
        foreach(glob(Yii::getAlias('@webroot') . Yii::$app->params['uploadsThumbsFolder'] . DIRECTORY_SEPARATOR . '*') as $pathfile){
            ++$countfiles;
            $allsizeclear += filesize($pathfile);
            unlink($pathfile);
        }
        
        Yii::$app->session->addFlash('success', 'Физически удалено файлов: <strong>' . $countfiles . '</strong><br>всего очищено '.Yii::$app->formatter->asShortSize($allsizeclear));
        
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    /*
     * очистка папки файлов от лишних(может занять много времени)
     */
    public function actionClearFilesfolder() {
        $filesfolder = $filesdb = [];
        $countfiles = $countdb = 0;
        
        //физические файлы
        foreach(glob(Yii::getAlias('@webroot') . Yii::$app->params['uploadFilesFolder'] . DIRECTORY_SEPARATOR . '*') as $pathfile){
            $filenameexp = explode(DIRECTORY_SEPARATOR, $pathfile);
            $filename = array_pop($filenameexp);
            $filesfolder[] = $filename;
        }
        
        //файлы из базы
        $rows = (new \yii\db\Query())
            ->select(['path'])
            ->from(UploadFiles::tableName())
            ->all();
        
        foreach ($rows as $row) {
            $filenameexp = explode(DIRECTORY_SEPARATOR, $row['path']);
            $filename = array_pop($filenameexp);
            $filesdb[] = $filename;
        }
        
        //разница между базой и файлами. в базе >
        $diffdb = array_diff($filesdb, $filesfolder);
        
        //разница между файлами и базой. файлов >
        $difffiles = array_diff($filesfolder, $filesdb);
        
        unset($filesfolder);
        unset($filesdb);
        
        //удалить физические файлы
        foreach ($difffiles as $difffile) {
            if(unlink(Yii::getAlias('@webroot') . Yii::$app->params['uploadFilesFolder'] . DIRECTORY_SEPARATOR . $difffile)){
                $countfiles++;
            } else {
                throw new \Exception('Не удается удалить файл');
            }
        }
        
        //удалить файлы из базы
        foreach ($diffdb as $diffdbfile) {
            if(UploadFiles::deleteAll(['like', 'path', $diffdbfile])){
                $countdb++;
            }
        }
        
        Yii::$app->session->addFlash('success', 'Физически удалено файлов: <strong>' . $countfiles . '</strong><br>Из базы удалено записей: <strong>' . $countdb . '</strong>');
        
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    /*
     * очистка кеша
     */
    public function actionFlushCache()
    {
        Yii::$app->cache->flush();
        Yii::$app->session->addFlash('success', Yii::t('app', 'Cache flushed'));
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    public function actionClearAssets()
    {
        foreach(glob(Yii::$app->assetManager->basePath . DIRECTORY_SEPARATOR . '*') as $asset){
            if(is_link($asset)){
                unlink($asset);
            } elseif(is_dir($asset)){
                $this->deleteDir($asset);
            } else {
                unlink($asset);
            }
        }
        
        foreach(glob(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'assetsminify'. DIRECTORY_SEPARATOR .'*') as $asset){
            if(is_link($asset)){
                unlink($asset);
            } elseif(is_dir($asset)){
                $this->deleteDir($asset);
            } else {
                unlink($asset);
            }
        }
        
        Yii::$app->session->addFlash('success', Yii::t('app', 'Assets cleared'));
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    private function deleteDir($directory) {
        $iterator = new \RecursiveDirectoryIterator($directory, \RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator($iterator, \RecursiveIteratorIterator::CHILD_FIRST);
        foreach($files as $file) {
            if ($file->isDir()){
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        return rmdir($directory);
    }
    
    /*
     * очистить неиспользуемые фото в папке сотрудников
     */
    public function actionDeleteUnusePhotoUsers() {
        $dir = Yii::getAlias('@webroot/uploads/users');
        $arrAllDir = scandir($dir);//массив фото физических
        $arrDBPhoto = [];//массив фото из базы
        array_shift($arrAllDir);array_shift($arrAllDir);//. ..
        $allsizeclear = 0;//вычислить размер удаляемых фото
        
        $arrDB = \app\modules\user\models\User::find()->all();//не самый лучший вариант
        $ids = \yii\helpers\ArrayHelper::getColumn($arrDB, 'photo');
        foreach ($ids as $id) {
            $explode = explode(DIRECTORY_SEPARATOR, $id);
            $arrDBPhoto[] = array_pop($explode);
        }
        $arrDBPhoto = array_unique($arrDBPhoto);
        
        $diff = array_diff($arrAllDir, $arrDBPhoto);//разница между физическими и из базы(физических может быть больше)
        
        foreach ($diff as $photoToDel) {
            $file = $dir.'/'.$photoToDel;
            if(file_exists($file)){
                $allsizeclear += filesize($file);
                unlink($file);//физическое удаление
            }
        }
        echo '<br>всего очищено '.Yii::$app->formatter->asShortSize($allsizeclear);
        
        if($allsizeclear > 0){
            Yii::$app->session->addFlash('success', Yii::t('app', 'All Cleared').': '.Yii::$app->formatter->asShortSize($allsizeclear));
        }
        else{
            Yii::$app->session->addFlash('info', Yii::t('app', 'No photo needed'));
        }
        
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    /*
     * создание бекапа через админку
     */
    public function actionBackupCreate() {
        $nameArchive = 'backup_intranet_'.date('d-m-Y').'.zip';//имя архива
        $folderToSave = 'backups';//папка под chmod 777 в корне
        $folderPath = Yii::getAlias('@app');//корневая папка 
        
        $nameSqldump = 'sqldump';//название файла бэкапа базы
        $dbname = FunctionModel::getDsnAttribute('dbname', Yii::$app->getDb()->dsn);//имя базы данных
                
        shell_exec('mysqldump -u'.Yii::$app->db->username.' -p'.Yii::$app->db->password.' '.$dbname.' > '.$folderPath.'/'.$nameSqldump.'.sql;');//бэкап базы в корень, потом удалить. NULL всегда возвращает
        
        if(shell_exec('cd '.$folderPath.'/..;zip -r '.$nameArchive.' html/;')){//забекапить
            shell_exec('cd '.$folderPath.'/..;mv '.$nameArchive.' '.$folderToSave.'/'.$nameArchive);//переместить
            
           unlink($folderPath.'/'.$nameSqldump.'.sql');//удалить бекап базы из корня 
            
            Yii::$app->session->addFlash('success', Yii::t('app', 'Backup ready').' : '.Html::a(Yii::t('app', 'Link to download'),['backup-upload', 'name' => $nameArchive]));
        }
        
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    /*
     * отдача бэкапа по имени
     */
    public function actionBackupUpload($name) {
        $folderToSave = 'backups';//папка под chmod 777 в корне
        $folderPath = Yii::getAlias('@app');//корневая папка 
        
        $pathfile = $folderPath.'/../'.$folderToSave.'/'.$name;
        if(file_exists($pathfile)){            
            if(ini_get('zlib.output_compression'))
                ini_set('zlib.output_compression', 'Off');
            
            header("Pragma: public"); 
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false); // нужен для некоторых браузеров
            header("Content-Type: application/zip");//всегда в зипе надеюсь
            header("Content-Disposition: attachment; filename=\"".$name."\";" );
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: ".filesize($pathfile));
            readfile($pathfile);
        }
        else{
            throw new \yii\web\NotFoundHttpException(Yii::t('app/models', 'File not found'));
        }
    }
}
