<?php

namespace app\modules\admins\controllers;

use Yii;
use yii\web\Controller;
use app\modules\admins\models\Holidays;
use app\models\FunctionModel;
use yii2fullcalendar\models\Event;
use yii\helpers\Html;
use yii\filters\VerbFilter;
use app\modules\user\models\User;
use yii\data\ActiveDataProvider;

class HolidaysController extends Controller
{    
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'jsoncalendar' => ['get'],
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Holidays::find(),
        ]);
        
        return $this->render('index',['dataProvider' => $dataProvider]);
    }
    
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionCreate()
    {
        $model = new Holidays();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pk_holiday]);
        } else {
            
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pk_holiday]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    protected function findModel($id)
    {
        if (($model = Holidays::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /*
     * ajax поиск событий в календарь
     */
    public function actionJsoncalendar($start=NULL,$end=NULL,$_=NULL){//
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $events = [];
        
        $yearSearch = substr($start, 0, 4);
        $dateStart = substr($start, 5);
        $dateEnd = substr($end, 5);
        
        $arrayOfDates = FunctionModel::getDatesBetweenDates($start, $end);
        
        foreach (User::findActive()->andWhere(['<>', 'birthday', '0000-00-00'])->each() as $user){
            if(!in_array(substr($user->birthday, 5), $arrayOfDates)) continue;
            $event = new Event();
            //$event->id = $holiday->pk_holiday;
            $event->title = $user->getLinkAvatar(true);
            $event->start = $yearSearch.'-'.date('m-d\TH:i:s\Z', strtotime($yearSearch.'-'.substr($user->birthday, 5)));
            $events[] = $event;
        }
        
        //праздники
        foreach (Holidays::find()->where(['>=', 'date', $dateStart])->andWhere(['<=', 'date', $dateEnd])->each() as $holiday) {
            $event = new Event();
            $event->id = $holiday->pk_holiday;
            $event->title = $holiday->getImgHtml(true);
            $event->start = $yearSearch.'-'.date('m-d\TH:i:s\Z', strtotime($yearSearch.'-'.$holiday->date));
            $events[] = $event;
        }

        return $events;
    }
}
