<?php

namespace app\modules\admins\models;

use Yii;
use app\models\FunctionModel;
/*
 * праздники
 */
class Holidays extends \yii\db\ActiveRecord
{
    public $monthDate, $dayDate;

    public static function tableName()
    {
        return 'holidays';
    }

    public function rules()
    {
        return [
            [['title', 'monthDate', 'dayDate'], 'required'],
            [['title', 'text', 'img'], 'string'],
            [['date', 'monthDate', 'dayDate'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_holiday' => Yii::t('app/models', 'Pk Holiday'),
            'title' => Yii::t('app/models', 'Title'),
            'text' => Yii::t('app/models', 'Text holiday'),
            'img' => Yii::t('app/models', 'Img holiday'),
            'date' => Yii::t('app/models', 'Date holiday'),
            'dayDate' => Yii::t('app', 'dayDate'),
            'monthDate' => Yii::t('app', 'monthDate'),
        ];
    }
    
    public function beforeValidate() {
        $this->date = sprintf("%02d", $this->monthDate).'-'.sprintf("%02d", $this->dayDate);
        return parent::beforeValidate();
    }
    
    public function afterFind() {
        $this->monthDate = (int)substr($this->date, 0, 2);
        $this->dayDate = (int)substr($this->date, 3, 5);
        $this->date = FunctionModel::getMonths($this->monthDate).', '.$this->dayDate;
    }
    
    /*
     * изображение праздника
     * $tooltip - bootstrap
     */
    public function getImgHtml($tooltip = false) {
        $params = [];
        $params['class'] = 'calendar-img';
        
        if($tooltip){
            $params['data-toggle'] = 'tooltip';
            $params['data-placement'] = 'top';
            $params['title'] = $this->title;
        }
        
        $this->img = ($this->img) ? $this->img : '/uploads/holidays/default.png';//по умолчанию картинка
        
        return \yii\helpers\Html::img($this->img,  $params);
    }
}

