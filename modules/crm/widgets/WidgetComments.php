<?php
/*
 * виджет написания комментариев для црм(простой)
 */
namespace app\modules\crm\widgets;

use yii\base\Widget;

class WidgetComments extends Widget{
    private $model;//модель комментария
    public $modelEntity;//модель сущности, которой необходимы комменты | $this->modelEntity->className() | $this->modelEntity->primaryKey
    public $condition = true;//условие для вывода/не вывода создания комментариев
    private $modelColumn;

    public function init(){
        if($this->modelEntity->className() == \app\modules\crm\models\CrmContact::className()){
            $this->modelColumn = 'fk_crmcontact';
        } else if($this->modelEntity->className() == \app\modules\crm\models\CrmCompany::className()) {
            $this->modelColumn = 'fk_crmcompany';
        } else {
            return;
        }
        
        $this->model = new \app\modules\crm\models\CrmTasks();
    }

    public function run(){
        if($this->condition !== true) return;
        return $this->render('widgetComments', ['modelEntity' => $this->modelEntity, 'model' => $this->model, 'column' => $this->modelColumn]);
    }
}
?>