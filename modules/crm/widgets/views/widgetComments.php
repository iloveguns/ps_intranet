<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$idThis = Yii::$app->security->generateRandomString();//генерация уникального ид на случай использования нескольких форм в одном месте
?>

<div class="comment-form" id="<?=$idThis?>">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status_text')->textarea(['tinymce' => 'tinymce']) ?>
    
    <?= $form->field($model, $column)->hiddenInput(['value' => $modelEntity->primaryKey])->label(false); ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton(Yii::t('app', 'Post comment'), ['class' => 'btn btn-primary']) ?>
    </div>
    
    <?php
    $this->registerJs("
    $('#".$idThis."').on('beforeSubmit', function (e) {
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '".\yii\helpers\Url::to(['/crm/crm-tasks/ajax-add'])."',
            data: $('#'+'".$idThis."'+' form').serializeArray(),
            success: function(data) {
                location.reload();
            },
            error: function(){
                alert('Ошибка сохраенения комментария црм');
            }
        });
        $('#".$idThis." .redactor-editor').html('');
        return false;
    });
    ", \yii\web\View::POS_END);?>
    
    <?php ActiveForm::end(); ?>

</div>