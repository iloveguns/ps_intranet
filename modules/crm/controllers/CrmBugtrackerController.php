<?php

namespace app\modules\crm\controllers;

use Yii;
use app\modules\intranet\models\Bugtracker;
use app\modules\intranet\models\BugtrackerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class CrmBugtrackerController extends Controller
{
    const TYPE = 'crm-bugtracker';
    public $text;
    
    /*
     * условие входа по дивизиону
     */
    public function enterCondition() {
        return true;
    }

    public function __construct($id, $module) {
        parent::__construct($id, $module);
        $this->text = [
            'title' => Yii::t('app/models', 'Crm bugtracker'),
            'text' => 'Старайтесь максимально конкретно описывать проблему. При возможности прикрепляйте скриншот или ссылку. Для вопросов есть <a href="/intranet/messages/index?id=1">Личные сообщения</a>',
            'create' => Yii::t('app/models', 'Create Bug'),
            'update' => Yii::t('app/models', 'Update Bug'),
            'type' => self::TYPE,
            'notice' => \app\models\UnreadNotice::NEW_SYSADMINQUERY,
        ];
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new BugtrackerSearch(self::TYPE);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, self::TYPE);

        return $this->render('@app/modules/intranet/views/bugtracker/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'text' => $this->text,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        
        return $this->render('@app/modules/intranet/views/bugtracker/view', [
            'model' => $model,
            'text' => $this->text,
        ]);
    }

    public function actionCreate()
    {
        $model = new Bugtracker(self::TYPE);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pk]);
        } else {
            return $this->render('@app/modules/intranet/views/bugtracker/create', [
                'model' => $model,
                'text' => $this->text
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pk]);
        } else {
            return $this->render('@app/modules/intranet/views/bugtracker/update', [
                'model' => $model,
                'text' => $this->text
            ]);
        }
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
                
        if(!Yii::$app->user->identity->isAdmin() && !$model->pk_author == Yii::$app->user->id) throw new \yii\web\ForbiddenHttpException(Yii::t ('app', 'You are not allowed to access this page'));
        
        $model->delete();
        
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Bugtracker::find()->where(['type' => self::TYPE])->andWhere(['pk' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
