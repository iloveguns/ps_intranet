<?php

namespace app\modules\crm\controllers;

use Yii;
use app\modules\crm\models\CrmCompany;
use app\modules\crm\models\CrmCompanySearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\crm\helpers\CrmHelper;

class CrmCompanyController extends \app\components\Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'toarchive' => ['POST'],
                    'fromarchive' => ['POST'],
                    'ajax-check' => ['POST'],
                    'changeresp' => ['POST'],
                    'tasks' => ['POST'],
                    'changetags' => ['POST'],
                    'transfer' => ['POST'],
                ],
            ],
        ];
    }
    
    /*
     * проверка вводимых слов на существование в базе(ввод одного и того же)
     */
    public function actionAjaxCheck() {
        $out = [];
        $post = Yii::$app->request->post();
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $rows = (new \yii\db\Query())
            ->select(['pk_company', 'name_company'])
            ->from(CrmCompany::tableName())
            ->where(['fk_department' => \app\modules\crm\CrmModule::$idCrmDep])
            ->andWhere(['like', 'name_company', $post['string']])
            ->limit(5)
            ->all();
        
        if($rows){
            $data = [];
            foreach ($rows as $row) {
                $data[] = \yii\helpers\Html::a($row['name_company'], ['/crm/crm-company/view', 'id' => $row['pk_company']], ['target' => '_blank']);
            }
            $out['success'] = TRUE;
            $out['data'] = $data;
        } else {
            $out['success'] = FALSE;
        }
                
        return $out;
    }

    public function actionIndex() {
        $searchModel = new CrmCompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new CrmCompany();
        $modelContacts = new \app\modules\crm\models\CrmContactsInfo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelContacts->load(Yii::$app->request->post());
            \app\modules\crm\helpers\CrmHelper::saveContacts($modelContacts, $model);//сохранение
            return $this->redirect(['view', 'id' => $model->pk_company]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelContacts = new \app\modules\crm\models\CrmContactsInfo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelContacts->load(Yii::$app->request->post());
            \app\modules\crm\helpers\CrmHelper::saveContacts($modelContacts, $model);//сохранение
            return $this->redirect(['view', 'id' => $model->pk_company]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /*
     * помещение в архив с контактами аяксом по выбранным из списка
     */
    public function actionToarchive() {
        $post = Yii::$app->request->post();
            
        if(!$post['data']) return;
        
        $count = CrmHelper::toArchive($post['data'], new CrmCompany());
        
        $this->flash('success', 'Выбранные компании помещены в архив ('.$count.')');
        return $this->redirect(['index']);
    }
    
    /*
     * восстановить из архива с контактами аяксом по выбранным из списка
     */
    public function actionFromarchive() {
        $post = Yii::$app->request->post();
            
        if(!$post['data']) return;
        
        $count = CrmHelper::fromArchive($post['data'], new CrmCompany());
        
        $this->flash('success', 'Выбранные компании восстановлены из архива ('.$count.')');
        return $this->redirect(['index']);
    }
    
    /*
     * передать другому ответственному
     */
    public function actionChangeresp() {
        $post = Yii::$app->request->post();
            
        if(!$post['data'] || !$post['data']['id_user'] || !$post['data']['selectedRows']) return;
        
        $count = CrmHelper::changeResponsible($post['data']['selectedRows'], $post['data']['id_user'], new CrmCompany());
        
        $this->flash('success', 'У выбранных компаний сменен ответственный ('.$count.')');
        return $this->redirect(['index']);
    }
    
    /*
     * создание задач массово
     */
    public function actionTasks() {
        $post = Yii::$app->request->post();
        
        $this->jsonResponse();
        
        $ids = explode(',', $post['ids']);//ид клиентов
        $type = $post['type_client'];//тип клиента
        
        foreach ($ids as $id) {
            $model = new \app\modules\crm\models\CrmTasks();
            if($type == 'crmcompany') {
                $model->fk_crmcompany = $id;
            } else if($type == 'crmcontact') {
                $model->fk_crmcontact = $id;
            }
            
            $model->scenario = 'create';
            $model->date_task = $post['date_task'];
            $model->time_task = $post['time_task'];
            $model->fk_user_task = $post['id_user'];
            $model->fk_typetask = $post['type_task'];
            $model->text_task = $post['comment'];
            
            if($model->save()) {
                
            } else {
                return ['success' => false, 'errors' => $model->getErrors()];
            }
        }
        
        return ['success' => true];
    }
    
    /*
     * смена тегов
     */
    public function actionChangetags() {
        $post = Yii::$app->request->post();
        
        if(!$post['data'] || !$post['data']['tags'] || !$post['data']['selectedRows']) return;
        
        $count = CrmHelper::changeTags($post['data']['selectedRows'], $post['data']['tags'], new CrmCompany());
        
        $this->flash('success', 'Выбранным компаниям добавлены теги ('.$count.')');
        return $this->redirect(['index']);
    }
    
    /*
     * удаление напрямую + удаление аяксом по выбранным из списка
     */
    public function actionDelete($id = NULL) {
        if(Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            
            if(!$post['data']) return;
            
            $deleted = Yii::$app->db->createCommand()
             ->update(CrmCompany::tableName(), ['is_deleted' => 1], 'pk_company IN (' . implode(',' ,$post['data']) . ')')
             ->execute();
            
            // + контакты
            $deleteContacts = Yii::$app->db->createCommand()
             ->update(\app\modules\crm\models\CrmContact::tableName(), ['is_deleted' => 1], 'fk_crm_company IN (' . implode(',' ,$post['data']) . ')')
             ->execute();            
            
            if($deleted) {
                $this->flash('success', 'Выбранные компании удалены');
            } else {
                $this->flash('danger', 'Невозможно произвести удаление');
            }
        } else {
            $model = $this->findModel($id);
            $model->is_deleted = 1;
            
            if($model->update()) {
                // + контакты
                $deleteContacts = Yii::$app->db->createCommand()
                ->update(\app\modules\crm\models\CrmContact::tableName(), ['is_deleted' => 1], 'fk_crm_company = ' . $id)
                ->execute();
                
                $this->flash('success', 'Компания удалена');
            } else {
                $this->flash('danger', 'Невозможно произвести удаление');
            }
        }
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = CrmCompany::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /*
     * поиск компаний
     */
    public function actionFindCompany($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!is_null($q)) {
            //$ss = \app\modules\crm\models\CrmContact::findWDep()->andWhere(['like', 'name_contact', $q])->limit(5)->all();//контакты
            $sa = \app\modules\crm\models\CrmCompany::findWDep()->andWhere(['like', 'name_company', $q])->limit(5)->all();//компании
            //var_dump($sa);
            $out = [];
            
            //ид состоит из ид и тип(контакт/компания)
            /*foreach ($ss as $value) {
                $out['results'][] = ['id' => $value->pk_contact.'|contact', 'text' => $value->name_contact, 'name' => Yii::t('app/views', 'Crm Contact')];
            }*/
            foreach ($sa as $value) {
                $out['results'][] = ['id' => $value->pk_company, 'text' => $value->name_company, 'name' => Yii::t('app/views', 'Crm Company')];
            }
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => 'empty'];
        }
        return $out;
    }
}