<?php

namespace app\modules\crm\controllers;

use Yii;
use app\modules\crm\models\CrmDeals;
use app\modules\crm\models\CrmDealsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class CrmDealsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new CrmDealsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAjaxCreate() {
        $success = false;
        $error = false;
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $model = new CrmDeals();
        $model->load(Yii::$app->request->post());
        $model->fk_department = \app\modules\crm\CrmModule::$idCrmDep;
        
        if($model->save()){
            $success = true;
        } else {
            $error = $model->getErrors();
        }
        
        return ['success' => $success, 'errors' => $error];
    }
    
    public function actionCreate() {
        $model = new CrmDeals();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id) {
        if (($model = CrmDeals::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}