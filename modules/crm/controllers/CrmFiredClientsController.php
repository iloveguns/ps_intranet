<?php

namespace app\modules\crm\controllers;
use app\modules\crm\models\CrmCompany;
use app\modules\crm\models\CrmContact;
use app\modules\crm\helpers\CrmHelper;

use Yii;

class CrmFiredClientsController extends \app\components\Controller {

    public function actionIndex() {
        $ra = [];
        
        $users = \app\modules\user\models\User::find()->where(['in', 'id_user', \app\modules\crm\CrmModule::haveFiredClients()])->all();
        
        //собрат всю необходимую информацию для вывода
        foreach ($users as $user) {
            $ra[] = [
                'user' => $user, 
                'countContacts' => CrmContact::countAllByUser($user->id_user),
                'countCompanies' => CrmCompany::countAllByUser($user->id_user),
            ];
        }
        
        return $this->render('index', [
            'users' => $ra,
        ]);
    }
    
    /*
     * помещение в архив всех клиентов
     * может быт очень долго
     * @var $id int - ид уволенного сотрудника
     */
    public function actionToArchive($id) {
        //сначала компании перенести, а потом контакты, т.к. контакты могут принадлежать компаниям и перенесутся с ними
        $pk_companies = (new \yii\db\Query())
        ->select(['pk_company'])
        ->from(CrmCompany::tableName())
        ->where(['responsible_user' => $id])
        ->all();
        
        //подготовка
        $arrIds = [];
        foreach ($pk_companies as $pk_company) {
            $arrIds[] = $pk_company['pk_company'];
        }
        
        $count = CrmHelper::toArchive($arrIds, new CrmCompany(), Yii::$app->user->id);
        
        $pk_contacts = (new \yii\db\Query())
        ->select(['pk_contact'])
        ->from(CrmContact::tableName())
        ->where(['responsible_user' => $id])
        ->all();
        
        //подготовка
        $arrIds = [];
        foreach ($pk_contacts as $pk_contact) {
            $arrIds[] = $pk_contact['pk_contact'];
        }
        
        $countс = CrmHelper::toArchive($arrIds, new CrmContact(), Yii::$app->user->id);
        
        $this->flash('success', 'Все клиенты компании помещены вам в архив ('.((int)$count + (int)$countс).')');
        return $this->back();
    }
}