<?php

namespace app\modules\crm\controllers;

use Yii;
use app\modules\crm\models\CrmCompany;
use app\modules\crm\models\CrmContact;

/**
 * в связи с отсутствием рассыльщика - не используется
 */
class MailController extends \app\components\Controller {
    /*
     * писмо
     */
    public function actionIndex($type, $ids){
        $idArr = explode(',', $ids);
        $models = $arrEmails = [];
        
        switch ($type) {
            case 'crmcompany':
                $model = \app\modules\crm\models\CrmContactsInfo::find()
                    ->select(['contact_value', 'fk_crmcompany'])
                    ->where(['fk_crmcompany' => $idArr])
                    ->andWhere(['like', 'contact_field', 'mail'])
                    ->asArray()
                    ->all();
                
                $emails = [];
                
                foreach ($model as $mod) {
                    $emails[$mod['fk_crmcompany']][] = $mod['contact_value'];
                }
                
                //только адреса получить
                foreach ($emails as $email) {
                    if(is_array($email)){
                        foreach ($email as $e) {
                            array_push($arrEmails, $e);
                        }
                    } else {
                        array_push($arrEmails, $email);
                    }
                }
                
                $names = CrmCompany::find()
                    ->select(['pk_company', 'name_company', 'pk_company'])
                    ->where(['pk_company' => $idArr])
                    ->asArray()
                    ->all();
                
                foreach ($names as $name) {
                    $models['data'][$name['pk_company']] = ['name' => CrmCompany::linkTo($name['name_company'], $name['pk_company'], ['target' => '_blank']), 'emails' => (isset($emails[$name['pk_company']])) ? $emails[$name['pk_company']] : []];
                }
                
                $models['names'] = 'Компании';
                
                break;
            
            case 'crmcontact':
                
                break;

            default:
                throw new \InvalidArgumentException('Incorrect type');
                break;
        }
        
        return $this->render('//mail/index', ['models' => $models, 'emails' => $arrEmails]);
    }
    
    /*
     * отдать инфу о шаблоне
     */
    public function actionGetInfoTemplate() {
        $files = '';
        
        $post = Yii::$app->request->post();
        
        $this->jsonResponse();
        
        $model = \app\models\ApiSendmailTemplates::findOne($post['id']);
        if($f = $model->getFilesLink(true)) {//файлы с абсолютным url для передачи ссылки
            $files = json_encode($f);
        }
        
        return ['content' => $model->email_content_html, 'subject' => $model->email_subject, 'files' => $files];
    }
}