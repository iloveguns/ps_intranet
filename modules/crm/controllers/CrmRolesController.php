<?php

namespace app\modules\crm\controllers;

use Yii;
use app\modules\crm\models\CrmRoles;
use app\modules\crm\models\CrmRolesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\crm\models\CrmRights;

class CrmRolesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new CrmRolesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new CrmRoles();
        $modelRights = new CrmRights();
        
        if($modelRights->load(Yii::$app->request->post())){
            $model->json_data = $modelRights->deleteEmptysToJson();//вот так сохраняются права в роли
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', Yii::t('app/views', 'Role created'));
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelRights' => $modelRights,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelRights = new CrmRights();
        
        //падает
        foreach (json_decode($model->attributes['json_data'],true) as $key => $value) {//ну просто вставить значения в свои места
            $modelRights->$key = $value;
        }

        if($modelRights->load(Yii::$app->request->post())){
            $model->json_data = $modelRights->deleteEmptysToJson();//вот так сохраняются права в роли
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', Yii::t('app/views', 'Role updated'));
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelRights' => $modelRights
            ]);
        }
    }

    public function actionDelete($id) {
        try {
            $this->findModel($id)->delete();
        } catch (\yii\db\IntegrityException $e) {
            Yii::$app->session->addFlash('danger', Yii::t('app/views', 'Model doesn\'t deleted').'<br>'.$e->getMessage());
        }

        return $this->redirect(['index']);
    }

    protected function findModel($id) {
        if (($model = CrmRoles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}