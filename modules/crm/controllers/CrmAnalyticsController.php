<?php

namespace app\modules\crm\controllers;

use Yii;
use app\modules\crm\models\CrmAnalytics;

class CrmAnalyticsController extends \app\components\Controller {
    
    public function behaviors() {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'ajax-deal-info' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionIndex() {
        $post = \Yii::$app->request->post();
        
        $searchdones = $searchdepdones = $searchdepdeals = NULL;
        
        if($post) {//обновление ajax
            if(isset($post['searchdones'])){//поиск сделанного по менеджеру
                $searchdones = CrmAnalytics::getSearchDones($post['searchdones']);
            } else if(isset($post['searchdepdones'])) {//поиск сделанного по отделу
                $searchdepdones = CrmAnalytics::getSearchDepDones($post['searchdepdones']);
            } else if(isset($post['searchdepdeals'])) {//общая, сделки
                $searchdepdeals = CrmAnalytics::getSearchDepDeals($post['searchdepdeals']);
            }
        } else {//начальные данные
            $searchdones = CrmAnalytics::getSearchDones();
        }
        
        return $this->render('index', [
            'post' => $post,
            'searchdones' => $searchdones,
            'searchdepdones' => $searchdepdones,
            'searchdepdeals' => $searchdepdeals,
        ]);
    }
    
    /*
     * доп информация по сделкам для аналитики
     */
    public function actionAjaxDealInfo() {
        $out = [];
        $post = Yii::$app->request->post();
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $data = CrmAnalytics::additionalDealInfo($post['model']);
                
        if($data === FALSE) {
            $out['success'] = false;
            $out['errors'] = ['Проблемы в модели аналитики'];
        } else {
            $out['success'] = true;
            $out['data'] = (empty($data)) ? '' : $data;
        }
        
        return $out;
    }
}