<?php

namespace app\modules\crm\controllers;

use Yii;
use app\modules\crm\models\CrmTasks;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\crm\models\CrmTasksSearch;

class CrmTasksController extends \app\components\Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'ajax-add' => ['POST'],
                    'ajax-delete-entity' => ['POST'],
                    'execute' => ['POST'],
                    'ajax-get-tasks' => ['POST'],
                    'ajax-calendar' => ['GET'],
                ],
            ],
        ];
    }
    
    /*
     * ajax выполнение задачи
     */
    public function actionExecute() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $post = Yii::$app->request->post();
        
        $modelPost = new CrmTasks();
        $modelPost->scenario = 'create';
        $modelPost->load($post);
        $newTask = isset($post['wplan_task']) && $post['wplan_task'] == 1;//пришли ли данные для создания новой задачи
        
        $success = $created = false;
        $error = '';
        
        if($modelPost->fk_task_result) {
            $m = \app\modules\crm\models\CrmTasksResults::findOne($modelPost->fk_task_result);//найти какой это результат
            $result_task = $m->denial;
        } else {
            $result_task = NULL;
        }
        
        if($newTask && !$modelPost->validate()){//если новая задача не валидируется, то сразу сообщить
            $error = $modelPost->getErrors();
        } else {
            $model = CrmTasks::findOne((int)$post['id_task']);
            $model->status_text = nl2br(filter_var($post['status_text'], FILTER_SANITIZE_STRING));//+переносы строк перевести в <br>
            $model->status_task = CrmTasks::STATUS_DONE;
            $model->fk_task_result = $modelPost->fk_task_result;
            $model->result_task = $result_task;

            if($model->update()){//задача обновлена, создать новую
                if($newTask) {//запланировать новую
                    $modelPost->fk_task_result = NULL;
                    if($modelPost->save()) $created = true;
                }
                
                //найти порученные задачи и отправить уведомление автору
                if($model->created_user != $model->fk_user_task) {
                    \app\models\UnreadNotice::saveByIds($model, \app\models\UnreadNotice::CRM_DONE_YOUR_TASK, [$model->created_user]);
                }

                $success = true;
            } else {
                $error = $model->getErrors();
            }
        }
        
        return ['success' => $success, 'errors' => $error, 'created' => $created];
    }

    /**
     * обычный список задач
     */
    public function actionTodoline() {
        $searchModel = new CrmTasksSearch();
        $dataProvider = $searchModel->searchLinen(Yii::$app->request->queryParams);

        return $this->render('todoline', [
            'searchModel' => $searchModel,
            'models' => $dataProvider,
        ]);
    }
    
    /*
     * календарь(месяц/неделя)
     */
    public function actionCalendar() {
        $searchModel = new CrmTasksSearch();

        return $this->render('calendar', [
            'searchModel' => $searchModel,
        ]);
    }
    
    /* СТАРЫЙ ФОРМАТ, ГДЕ ПОЛНОСТЬЮ ССЫЛКИ НА КАЖДУЮ ЗАДАЧУ ВЫВОДИЛИСЬ(СОХРАНИТЬ)
     * public function actionAjaxCalendar($start=NULL, $end=NULL, $_=NULL) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $uniqid = 1;//просто уникальный ид у каждого события
        
        $events = [];
        
        $tasks = CrmTasksSearch::searchCalendar($start, $end);
        
        foreach ($tasks as $task) {
            $event = new \yii2fullcalendar\models\Event();//новое событие календаря
            $event->id = $uniqid;
            $event->url = $task->url;//url на который можно сразу перебрасывать            
            if($task->status_task == CrmTasks::STATUS_LATE)
                $event->className = 'bg-danger text-red';//css класс
            $event->title = $task->text_task;            
            if($task->time_task == 'allday')
                $event->allDay = true;//типа на весь день
            $event->start = $task->date_task.' '.$task->time_task;//время начала события
            //$event->end = $task->time_task;//время конца
            $events[] = $event;
            
            ++$uniqid;
        }
        return $events;
    }*/
    
    /*
     * только кол-во задач в день
     */
    public function actionAjaxCalendar($start=NULL, $end=NULL, $_=NULL) {
        $this->jsonResponse();
        
        $uniqid = 1;//просто уникальный ид у каждого события
        
        $events = [];
        
        $tasks = CrmTasksSearch::searchCalendar($start, $end);
        
        //добавить дни рождения клиентов
        $birthdays = 0;
        
        $yearSearch = substr($start, 0, 4);
        $dateStart = substr($start, 5);
        $dateEnd = substr($end, 5);

        $arrayOfDates = \app\models\FunctionModel::getDatesBetweenDates($start, $end);
        
        foreach ($arrayOfDates as $date) {
            $count = 0;
            
            $m = \app\modules\crm\models\CrmCompany::findWDep()
                ->asArray()
                ->select('birthdate_company')
                ->andWhere(['stock' => '0'])
                ->andWhere(['responsible_user' => Yii::$app->user->id])
                ->andWhere(['like', 'birthdate_company', '-' . $date])
                ->count();
            
            $c = \app\modules\crm\models\CrmContact::findWDep()
                ->asArray()
                ->select('birthdate_contact')
                ->andWhere(['stock' => '0'])
                ->andWhere(['responsible_user' => Yii::$app->user->id])
                ->andWhere(['like', 'birthdate_contact', '-' . $date])
                ->count();
            
            $count = $m + $c;
            
            if($count) {
                $event = new \yii2fullcalendar\models\Event();
                $event->id = $uniqid;
                $event->url = \yii\helpers\Url::to(['/crm/default/birthdays', 'date' => $date]);
                $event->className = 'label-warning';
                $event->title = 'Дни рождения клиентов: ' . $count;
                $event->start = $yearSearch.'-'.$date;
                $events[] = $event;
                ++$uniqid;
            }
        }
        //end добавить дни рождения клиентов
        
        $a = [];
        foreach ($tasks as $task) {
            $a[] = $task->date_task;
        }
        foreach (array_count_values($a) as $t => $c) {
            $event = new \yii2fullcalendar\models\Event();//новое событие календаря
            $event->id = $uniqid;
            $event->url = \yii\helpers\Url::to(['/crm/crm-tasks/todolist', 'CrmTasksSearch[date_task]' => $t, 'CrmTasksSearch[fk_user_task]' => Yii::$app->user->id]);//ссылка на просмотр задач в списке
            $event->start = $t;
            $event->title = $c.' задач';
            $events[] = $event;
            ++$uniqid;
        }
        
        return $events;
    }
    
    /*
     * список задач просроч, сегодня, завтра
     */
    public function actionTodolist() {
        $searchModel = new CrmTasksSearch();
        $dataProvider = $searchModel->searchList(Yii::$app->request->queryParams);

        return $this->render('todolist', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /*
     * список порученных. поставленных тобой кому-то
     */
    public function actionErrandlist() {
        $searchModel = new CrmTasksSearch();
        $searchModel->created_user = Yii::$app->user->id;
        $searchModel->fk_user_task = 0;//любого кроме себя
        $dataProvider = $searchModel->searchList(Yii::$app->request->queryParams);

        return $this->render('todolist', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'created_user' => true,//метка вывода доп поля - создано кем
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    /*
     * получить задачи(список задач/история)
     */
    public function actionAjaxGetTasks() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $post = Yii::$app->request->post();
        $pk_company = (int)$post['pk_company'];
        $pk_contact = (int)$post['pk_contact'];
        $lastId = (int)$post['last-id-task'];
        $repeat = (int)$post['repeat'];
        
        if($pk_company && !$pk_contact){//компания
            $model = \app\modules\crm\models\CrmCompany::findOne($pk_company);
        } else if($pk_contact && !$pk_company){
            $model = \app\modules\crm\models\CrmContact::findOne($pk_contact);
        } else {
            return false;
        }
        
        $return = '';
        $haveMore = true;
        $tasks = $model->getDoneTasksAll($repeat);
        foreach ($tasks as $task) {
            $return .= Yii::$app->view->render('/crm-tasks/_task-item', ['model' => $task]);
        }
        if(count($tasks) < Yii::$app->params['default_count_tasks_crm']) $haveMore = false;//если уже закончились задачи
        if(isset($task)) $lastId = $task->pk_task;
        
        return ['html' => $return, 'lastid' => $lastId, 'haveMore' => $haveMore];
    }
    
    /*
     * ajax создание
     */
    public function actionAjaxCreate() {
        $model = new CrmTasks();
        $model->scenario = 'create';
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['success' => true];
        } else {
            return ['success' => false, 'errors' => $model->getErrors()];
        }
    }
    
    public function actionCreate() {
        $model = new CrmTasks();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->back();
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->back();
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->back();
    }

    protected function findModel($id) {
        if (($model = CrmTasks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /*
     * поиск контактов или компаний
     * используется в нескольких местах
     */
    public function actionFindContacts($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!is_null($q)) {
            $ss = \app\modules\crm\models\CrmContact::findWDep()->andWhere(['like', 'name_contact', $q])->limit(5)->all();
            $sa = \app\modules\crm\models\CrmCompany::findWDep()->andWhere(['like', 'name_company', $q])->limit(5)->all();

            $out = [];
            
            //ид состоит из ид и тип(контакт/компания)
            foreach ($ss as $value) {
                $out['results'][] = ['id' => $value->pk_contact.'|contact', 'text' => $value->name_contact, 'name' => Yii::t('app/views', 'Crm Contact')];
            }
            foreach ($sa as $value) {
                $out['results'][] = ['id' => $value->pk_company.'|company', 'text' => $value->name_company, 'name' => Yii::t('app/views', 'Crm Company')];
            }
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => City::find($id)->name];
        }
        return $out;
    }
    
    /*
     * ajax добавление комментария crm
     */
    public function actionAjaxAdd() {
        $success = false;//индикатор сохранения
        $text = '';//возвращаемое сообщение
        $htmlComment = '';//html подготовленный коммент для вставки
        $error = '';//ошибки
        
        $model = new CrmTasks();
        $model->load(Yii::$app->request->post());
        $model->date_task = \app\models\FunctionModel::getDateWParam('Y-m-d');
        $model->status_task = CrmTasks::STATUS_COMMENT;
        $model->fk_department = \app\modules\crm\CrmModule::$idCrmDep;
                
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        if($model->save()){
            $success = true;
            $text = Yii::t('app', 'New comment has success create');
        } else{
            $text = Yii::t('app', 'Can\'t create comment');
            foreach ($model->getErrors() as $err) {
                $errors[] = $err[0];//ошибки
            }
            $error = implode('<br>', $errors);
        }
        
        $response = ['action' => 'newCommentCrm', 'text' => $text, 'id_newcomment' => $model->pk_task, 'success' => $success, 'error' => $error];//ответ
        
        return $response;
    }
    
    /*
     * удаление из журнала задач по ид
     */
    public function actionAjaxDeleteEntity() {
        $id = (int)$_POST['id_task'];
        if($model = CrmTasks::findOne($id)) {        
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
            if($model->delete()){
                return ['success' => true];
            }
        }
    }
}