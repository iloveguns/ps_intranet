<?php

namespace app\modules\crm\controllers;

use Yii;
use app\modules\crm\models\CrmContact;
use app\modules\crm\models\CrmContactSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\crm\models\CrmContactsInfo;
use app\modules\crm\helpers\CrmHelper;

class CrmContactController extends \app\components\Controller
{
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'toarchive' => ['POST'],
                    'fromarchive' => ['POST'],
                    'tasks' => ['POST'],
                    'ajax-check' => ['POST'],
                    'transfer' => ['POST'],
                ],
            ],
        ];
    }
    
    /*
     * проверка вводимых слов на существование в базе(ввод одного и того же)
     */
    public function actionAjaxCheck() {
        $out = [];
        $post = Yii::$app->request->post();
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $rows = (new \yii\db\Query())
            ->select(['pk_contact', 'name_contact'])
            ->from(CrmContact::tableName())
            ->where(['fk_department' => \app\modules\crm\CrmModule::$idCrmDep])
            ->andWhere(['like', 'name_contact', $post['string']])
            ->limit(5)
            ->all();
        
        if($rows){
            $data = [];
            foreach ($rows as $row) {
                $data[] = \yii\helpers\Html::a($row['name_contact'], ['/crm/crm-contact/view', 'id' => $row['pk_contact']], ['target' => '_blank']);
            }
            $out['success'] = TRUE;
            $out['data'] = $data;
        } else {
            $out['success'] = FALSE;
        }
                
        return $out;
    }

    public function actionIndex()
    {
        $searchModel = new CrmContactSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * карточка клиента
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /*
     * ajax создание
     */
    public function actionAjaxCreate()
    {
        $model = new CrmContact();
        $modelContacts = new CrmContactsInfo();
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelContacts->load(Yii::$app->request->post());
            CrmHelper::saveContacts($modelContacts, $model);//сохранение
            return ['success' => true];
        } else {
            return ['success' => false];
        }
    }
    
    public function actionCreate()
    {
        $model = new CrmContact();
        $modelContacts = new CrmContactsInfo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelContacts->load(Yii::$app->request->post());
            CrmHelper::saveContacts($modelContacts, $model);//сохранение
            return $this->redirect(['view', 'id' => $model->pk_contact]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelContacts = new CrmContactsInfo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelContacts->load(Yii::$app->request->post());
            CrmHelper::saveContacts($modelContacts, $model);//сохранение
            return $this->redirect(['view', 'id' => $model->pk_contact]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /*
     * помещение в архив аяксом по выбранным из списка
     */
    public function actionToarchive() {
        $post = Yii::$app->request->post();
            
        if(!$post['data']) return;

        $count = CrmHelper::toArchive($post['data'], new CrmContact());
        
        $this->flash('success', 'Выбранные контакты помещены в архив ('.$count.')');
        return $this->redirect(['index']);
    }
    
    /*
     * восстановить из архива аяксом по выбранным из списка
     */
    public function actionFromarchive() {
        $post = Yii::$app->request->post();
            
        if(!$post['data']) return;
        
        $count = CrmHelper::fromArchive($post['data'], new CrmContact());
        
        $this->flash('success', 'Выбранные контакты восстановлены из архива ('.$count.')');
        return $this->redirect(['index']);
    }
    
    /*
     * передать другому ответственному
     */
    public function actionChangeresp() {
        $post = Yii::$app->request->post();
            
        if(!$post['data'] || !$post['data']['id_user'] || !$post['data']['selectedRows']) return;
        
        $count = CrmHelper::changeResponsible($post['data']['selectedRows'], $post['data']['id_user'], new CrmContact());
        
        $this->flash('success', 'У выбранных контактов сменен ответственный ('.$count.')');
        return $this->redirect(['index']);
    }
    
    /*
     * создание задач массово
     */
    public function actionTasks() {
        $post = Yii::$app->request->post();
        
        $this->jsonResponse();
        
        $ids = explode(',', $post['ids']);//ид клиентов
        $type = $post['type_client'];//тип клиента
        
        foreach ($ids as $id) {
            $model = new \app\modules\crm\models\CrmTasks();
            if($type == 'crmcompany') {
                $model->fk_crmcompany = $id;
            } else if($type == 'crmcontact') {
                $model->fk_crmcontact = $id;
            }
            
            $model->scenario = 'create';
            $model->date_task = $post['date_task'];
            $model->time_task = $post['time_task'];
            $model->fk_user_task = $post['id_user'];
            $model->fk_typetask = $post['type_task'];
            $model->text_task = $post['comment'];
            
            if($model->save()) {
                
            } else {
                return ['success' => false, 'errors' => $model->getErrors()];
            }
        }
        
        return ['success' => true];
    }
    
    /*
     * смена тегов
     */
    public function actionChangetags() {
        $post = Yii::$app->request->post();
        
        if(!$post['data'] || !$post['data']['tags'] || !$post['data']['selectedRows']) return;
        
        $count = CrmHelper::changeTags($post['data']['selectedRows'], $post['data']['tags'], new CrmContact());
        
        $this->flash('success', 'Выбранным контактам добавлены теги ('.$count.')');
        return $this->redirect(['index']);
    }
    
    /*
     * удаление напрямую + удаление аяксом по выбранным из списка
     */
    public function actionDelete($id = NULL) {
        if(Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            
            if(!$post['data']) return;
            
            $deleted = Yii::$app->db->createCommand()
             ->update(CrmContact::tableName(), ['is_deleted' => 1], 'pk_contact IN (' . implode(',' ,$post['data']) . ')')
             ->execute();
            
            if($deleted) {
                $this->flash('success', 'Выбранные контакты удалены');
            } else {
                $this->flash('danger', 'Невозможно произвести удаление');
            }
        } else {
            $model = $this->findModel($id);
            $model->is_deleted = 1;
            
            if($model->update()) {
                $this->flash('success', 'Контакт удален');
            } else {
                $this->flash('danger', 'Невозможно произвести удаление');
            }
        }
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = CrmContact::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}