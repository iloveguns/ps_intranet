<?php

namespace app\modules\crm\controllers;

class DefaultController extends \app\components\Controller
{
    public function actionIndex() {
        return $this->redirect('/crm/crm-faq/index');
        //return $this->render('index');
    }
    
    public function actionBirthdays($date = null) {
        $hasDatum = ($date) ? $date : false;
        $day = ($date) ? '-' . $date : \app\models\FunctionModel::getDateWParam('-m-d');
        
        $companies = \app\modules\crm\models\CrmCompany::findWDep()->asArray()->andWhere(['like', 'birthdate_company', $day])->all();
        $contacts = \app\modules\crm\models\CrmContact::findWDep()->asArray()->andWhere(['like', 'birthdate_contact', $day])->all();
        
        return $this->render('birthdays', ['companies' => $companies, 'contacts' => $contacts, 'hasDatum' => $hasDatum]);
    }
    
    public function actionImport() {
        // output headers so that the file is downloaded rather than displayed
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="companies.csv"');

        // do not cache the file
        header('Pragma: no-cache');
        header('Expires: 0');
        
        $file = fopen('php://output', 'w');
        //$file = fopen('companies.csv', 'w');
        
        // заголовки, верхние строки
        fputcsv($file, array('Название компании', 'Тип компании', 'Ответственный', 'Рабочий телефон', 'Рабочий e-mail', 'Доступен для всех', 'Комментарий'));
        
        $list = [
            ['СУпер компания', 'Клиент', 'Лидия', '8 495 2346187,2352355', 'y@y.ru, slayermass@mail.ru', 'нет', 'просто компашка']
        ];

        

        foreach ($list as $fields) {
            fputcsv($file, $fields);
        }

        fclose($file);
    }
}
