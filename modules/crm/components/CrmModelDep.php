<?php

namespace app\modules\crm\components;

use Yii;
use app\modules\crm\models\CrmTasks;
use app\modules\crm\models\CrmCompany;
use app\modules\crm\models\CrmContact;
use app\models\FunctionModel;
use app\modules\crm\models\CrmContactsInfo;
use app\modules\crm\CrmModule;
use app\modules\crm\models\CrmDeals;
use app\modules\crm\models\CrmTags;
use app\modules\crm\models\CrmRelationTags;
use yii\db\ActiveRecord;

class CrmModelDep extends CrmModelAll
{
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date', 'update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_date'],
                ],
                'value' => function() { return \app\models\FunctionModel::getDateTimestamp();},
            ]
        ];
    }
    
    /*
     * массив значений тегов модели
     */
    public function getTags() {
        $tags = [];
        foreach ($this->tagsModel as $tag) {
            array_push($tags, $tag->pk_tag);
        }
        return $tags;
    }
    
    /*
     * названия совпали(атрибут tags = метод getTags), пришлось делать с другим названием
     */
    public function getTagsModel() {
        return $this->hasMany(CrmTags::className(), ['pk_tag' => 'fk_tag'])
                ->viaTable(CrmRelationTags::tableName(), [$this->fk => $this->pk]);
    }
    
    /*
     * ссылки на теги
     */
    public function getAllTagsLink() {
        $tags = [];
        
        $link1 = $link2 ='';
        if($this->fk == CrmCompany::fk) {
            $link1 = 'crm-company';
            $link2 = 'CrmCompanySearch';
        } else if($this->fk == CrmContact::fk) {
            $link1 = 'crm-contact';
            $link2 = 'CrmContactSearch';
        } else {
            return false;
        }
        
        foreach ($this->tagsModel as $tag) {
            $tags[] = \yii\helpers\Html::a($tag->name_tag, ['/crm/'.$link1.'/index', $link2.'[tags]' => $tag->pk_tag]);
        }
        return (empty($tags)) ? NULL : implode(', ', $tags);
    }
    
    /*
     * получить все телефоны контактной информации
     */
    public function getAllPhones() {
        $string = [];
        foreach ($this->contacts as $contact) {
            if(stripos($contact->contact_field, 'phone') !== FALSE)
               $string[] = $contact->contact_value;
        }
        
        return implode(', ', $string);
    }
    
    /*
     * получить все email контактной информации
     */
    public function getAllEmails() {
        $string = [];
        foreach ($this->contacts as $contact) {
            if(stripos($contact->contact_field, 'email') !== FALSE)
               $string[] = $contact->contact_value;
        }
        
        return implode(', ', $string);
    }
    
    /*
     * связи с контактными данным
     */
    public function getContacts()
    {   
        return $this->hasMany(CrmContactsInfo::className(), [$this->fk => $this->pk]);
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord){
                $this->created_user = Yii::$app->user->id;//создатель
            }
            
            $this->updated_user = Yii::$app->user->id;//редактировавший
            return true;
        }
        return false;
    }
    
    public function beforeDelete() {        
        if (parent::beforeDelete()) {
            //удалить задачи??
            //удалить сделки??
            if($this->className() == CrmContact::className()){//контакт
                CrmTasks::deleteAll(['fk_crmcontact' => $this->getPrimaryKey()]);
                CrmDeals::deleteAll(['fk_crmcontact' => $this->getPrimaryKey()]);
                //CrmDeals
            } else if($this->className() == CrmCompany::className()) {//компания
                CrmTasks::deleteAll(['fk_crmcompany' => $this->getPrimaryKey()]);
                CrmDeals::deleteAll(['fk_crmcompany' => $this->getPrimaryKey()]);
            }
            \app\modules\crm\helpers\CrmHelper::deleteContactsModel($this);
            \app\modules\crm\helpers\CrmHelper::deleteTagsModel($this);
            return true;
        } else {
            return false;
        }
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        
        if($this->isNewRecord){
            //вставить сискоммент о добавлении
            $arrInsert = [
                'date_task' => FunctionModel::getDateWParam('Y-m-d'),
                'create_date' => FunctionModel::getDateTimestamp(),
                'status_task' => CrmTasks::STATUS_SYSCOMMENT,
                'fk_user_task' => $this->created_user,
                'fk_department' => $this->fk_department,
            ];

            if($this->className() == CrmContact::className()){
                $arrInsert['status_text'] = 'Создан контакт ::SELFLINK::';
                $arrInsert['fk_crmcontact'] = $this->pk_contact;
            } else if($this->className() == CrmCompany::className()) {
                $arrInsert['status_text'] = 'Создана компания ::SELFLINK::';
                $arrInsert['fk_crmcompany'] = $this->pk_company;
            }

            Yii::$app->db->createCommand()->insert(CrmTasks::tableName(), $arrInsert)->execute();
            //конец вставки
        }
        
        //сохранение тегов(после, т.к. новые модели могут иметь теги, а там нужен ид)
        $pk = $this->pk;
        \app\modules\crm\helpers\CrmHelper::changeTags([$this->$pk], $this->tags, $this, true);
    }
    
    /*
     * все невыполненные задачи
     */
    public function getWorkTasks() {
        $q = $this->hasMany(CrmTasks::className(), [$this->fk => $this->primaryKey()[0]])
            ->orWhere(['status_task' => CrmTasks::STATUS_LATE])
            ->orWhere(['status_task' => CrmTasks::STATUS_WORK]);
        $q->andWhere(['fk_user_task' => Yii::$app->user->id]);//условие для вывода всех можно
        return $q;
    }
    
    /*
     * все выполненные задачи
     */
    public function getDoneTasks() {
        $q = $this->hasMany(CrmTasks::className(), [$this->fk => $this->primaryKey()[0]])
                ->andWhere(['status_task' => CrmTasks::STATUS_DONE]);
        $q->andWhere(['fk_user_task' => Yii::$app->user->id]);//условие для вывода всех можно
        $q->orderBy('pk_task DESC');
        return $q;
    }
    
    /*
     * только исполненные сделки
     */
    public function getDoneDeals() {
        return self::getDeals(['status_deal' => 6]);
    }
    
    /*
     * Закрыто и не реализовано(потерянные)
     */
    public function getLostDeals() {
        return self::getDeals(['status_deal' => 7]);
    }
    
    /*
     * только неисполненные сделки и не провальные
     */
    public function getNotDoneDeals() {
        return self::getDeals(['not in', 'status_deal', [6,7]]);
    }
    
    /*
     * сделки по статусу все
     */
    public function getDeals($conditionStatus = NULL) {
        $q = CrmDeals::findWDep()->with(['fkCrmcompany', 'fkCrmcontact']);
        if($this->fk == CrmCompany::fk){//компания
            $arrContacts = $this->getDb()->cache(function ($db) {//найти ид всех контактов, чтоб их добавить в выборку
                $arrContacts = [];
                $a = $this->getCrmContacts()->asArray()->all();
                foreach ($a as $contact){
                    $arrContacts[$contact['pk_contact']] = $contact['pk_contact'];
                }
                return $arrContacts;
            });
            
            $q->andWhere(['fk_crmcompany' => $this->getPrimaryKey()])
            ->orFilterWhere(['in', 'fk_crmcontact', $arrContacts]);
            if($conditionStatus) $q->andFilterWhere($conditionStatus);
        } else if($this->fk == CrmContact::fk) {//контакт
            $q->andWhere(['fk_crmcontact' => $this->getPrimaryKey()]);
            if($conditionStatus) $q->andFilterWhere($conditionStatus);
        } else {
            $q->where('0=1');//всегда пустой
        }
        $q->orderBy('pk_deal DESC');
        return $q->all();
    }


    /*
     * вывод всех задач, связанных(контакты и компания+)
     * @param {int} $offset - количество отступов по X строк
     */
    public function getDoneTasksAll($offset = NULL) {        
        $q = CrmTasks::findWDep()->with(['fkCrmcompany', 'fkCrmcontact']);
        //$q->andWhere(['fk_user_task' => Yii::$app->user->id]);//все или каждого сотрудника?
        if($this->fk == CrmCompany::fk){//компания
            $arrContacts = $this->getDb()->cache(function ($db) {//найти ид всех контактов, чтоб их добавить в выборку
                $arrContacts = [];
                $a = $this->getCrmContacts()->asArray()->all();
                foreach ($a as $contact){
                    $arrContacts[$contact['pk_contact']] = $contact['pk_contact'];
                }
                return $arrContacts;
            });
            $q->andWhere(['fk_crmcompany' => $this->getPrimaryKey()])
            ->orFilterWhere(['in', 'fk_crmcontact', $arrContacts]);
        } else if($this->fk == CrmContact::fk) {//контакт
            $q->andWhere(['fk_crmcontact' => $this->getPrimaryKey()])
            ->orFilterWhere(['fk_crmcompany' => $this->fk_crm_company]);
        } else {    
            $q->where('0=1');//всегда пустой
        }
        
        if($offset) $q->offset(Yii::$app->params['default_count_tasks_crm'] * $offset);
        $q->orderBy('date_task DESC, update_date DESC, create_date DESC')->limit(Yii::$app->params['default_count_tasks_crm']);//create_date - update_date - поменял местами
        return $q->all();
    }
    
    /*
     * кол-во объектов сотрудника
     */
    public function countAllByUser($id_user) {
        return self::find()->where(['responsible_user' => $id_user])->count();
    }
}