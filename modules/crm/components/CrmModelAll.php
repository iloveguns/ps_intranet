<?php

namespace app\modules\crm\components;

use app\modules\user\models\User;

class CrmModelAll extends \yii\db\ActiveRecord
{    
    /*
     * найти с отделом
     */
    public static function findWDep() {
        return parent::find()->andFilterWhere([parent::tableName().'.fk_department' => \app\modules\crm\CrmModule::$idCrmDep]);
    }
    
    /*
     * создавший сотрудник
     */
    public function getCreatedUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'created_user']);
    }

    /*
     * ответственный сотрудник
     */
    public function getResponsibleUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'responsible_user']);
    }

    /*
     * изменивший сотрудник
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'updated_user']);
    }
    
    /*
     * проверка, может ли сотрудник админить компанию/контакт
     */
    public function canAdmin() {
        if(\app\modules\crm\CrmModule::canAdminDep(\Yii::$app->user->id)) return true;
        if (\Yii::$app->user->identity->isAdmin() || $this->responsible_user == \Yii::$app->user->id) return true;
        return false;
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->fk_department = \app\modules\crm\CrmModule::$idCrmDep;//сохранение в текущий отдел
            return true;
        }
        return false;
    }
    
    /*
     * 
     * @param $query {object AR}    - текущий запрос 
     * @param $attr {string}        - атрибут модели
     * @param $query {array}        - результат FunctionModel::tryChangeCharsetWord() или любая строка
     */
    public function prepareMultiLangSearchFilter($query, $attr, $multiString) {
        if(!is_array($multiString)) $multiString = (array) $multiString;
        
        $orQuery = [];
        $orQuery[0] = 'OR';
        
        foreach ($multiString as $langString) {
            if(strrpos($langString, " ")){//слова через пробел
                $arrSearch = explode(' ', $langString);
                $orQuery[] = ['like', $attr, $arrSearch];//поиск по каждому слову
            } else {
                $orQuery[] = ['like', $attr, $langString];
            }
        }
        
        $query->andWhere($orQuery);
        
        return $query;
    }
}