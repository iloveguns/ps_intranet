<?php

namespace app\modules\crm;
use Yii;
use app\modules\user\models\User;
use app\modules\user\models\Department;
use app\modules\crm\models\CrmAccessSettings;

class CrmModule extends \yii\base\Module {
    /*
     * уникальный отдел, от которого зависит информация(контакты, задачи, менеджеры, ВСЕ)
     * только внутри отдела можно все видеть, разные отделы не должны даже догадываться о существовании других(руководители иногда хотят иметь доступ к отделам)
     * нельзя менять ни в коем случае(используется для сохранения данных с привязкой к отделу)
     * 
     * \app\modules\crm\CrmModule::$idCrmDep
     */
    public static $idCrmDep;
    
    public static $_usersCanAdmin;

    public static $storeName = 'stock';//склад компаний, контактов, от которых хотят избавиться на какое то время сотрудники(полностью исчезает)
    public static $storeIn = '1';//помещен в архив
    public static $storeOut = '0';//не помещен в архив
    const CRMDEPARTMENT = 1;
    const COOKIE_DEPARTMENT = 'sel_dep';

    public $controllerNamespace = 'app\modules\crm\controllers';

    public function init() {
        parent::init();
        Yii::$app->homeUrl = '/crm/default/index';
        
        //смена текущего отдела на вывод||сложно/запутанно, надо упростить
        if(isset($_GET[self::COOKIE_DEPARTMENT])) {//метка смены отдела
            self::$idCrmDep = $_GET[self::COOKIE_DEPARTMENT];
            self::setCookieDep(self::$idCrmDep);
            return Yii::$app->response->redirect(Yii::$app->request->referrer);
        } else if($selected_department_cookie = \Yii::$app->getRequest()->getCookies()->getValue(self::COOKIE_DEPARTMENT)) {//выбран отдел, возможно другой
            if(!isset(self::getAvailableDepartment()[$selected_department_cookie])){//если кука хранит недопустимый отдел - сбросить в исходный отдел
                self::$idCrmDep = Yii::$app->user->identity->fkDepartment->pk_department;
                self::setCookieDep(self::$idCrmDep);
            } else {
                self::$idCrmDep = $selected_department_cookie;
            }
        } else {//установить текущий отдел из интранета
            if(Yii::$app->user->isGuest) {
                //ничего - выкинуть на вход
            } elseif(Yii::$app->user->identity->isAdmin()) {//админу выбрать первый из доступных отделов
                self::$idCrmDep = key(self::getAvailableDepartment());
                self::setCookieDep(self::$idCrmDep);
            } else {
                self::$idCrmDep = Yii::$app->user->identity->fkDepartment->pk_department;
                self::setCookieDep(self::$idCrmDep);
            }
        }
    }
    
    public function setCookieDep($val){
        $cookie = new \yii\web\Cookie([
            'name' => self::COOKIE_DEPARTMENT,
            'value' => $val,
            'expire' => time() + 86400 * 3,
        ]);
        \Yii::$app->getResponse()->getCookies()->add($cookie);
    }


    /*
     * получить значение архива
     */
    public function getStock($id = NULL) {
        $a = [CrmModule::$storeOut = Yii::t('app', 'No'), CrmModule::$storeIn = Yii::t('app', 'Yes')];
        if($id !== NULL) return $a[$id];
        return $a;
    }
    
    /*
     * все отделы в црм. нужно пометить отдел 
     */
    public function getDepartments($id = NULL) {
        $b = Department::getDb()->cache(function ($db) {
            $all = Department::find()->andFilterWhere(['in_crm' => self::CRMDEPARTMENT])->all();
            $b = [];
            foreach ($all as $dep) {
                $b[$dep['pk_department']] = $dep['name'];
            }
            return $b;
        });
        
        if($id) return $b[$id];
        return $b;
    }
    
    /*
     * найти отделы, в которые сотрудник имеет доступ
     */
    public function getAvailableDepartment() {
        if(Yii::$app->user->identity->isAdmin()) return self::getDepartments();//админ сразу все видит
        
        $r = CrmAccessSettings::getDb()->cache(function ($db) {
            $r = [];
            
            $allDeps = CrmAccessSettings::find()->select('fk_department')->with('fkDepartment')->where(['fk_user' => Yii::$app->user->id])->all();

            //основной отдел может и не быть отделом црм
            if(Yii::$app->user->identity->fkDepartment->in_crm) {
                $r = [Yii::$app->user->identity->fkDepartment->pk_department => Yii::$app->user->identity->fkDepartment->name];
            }
            
            foreach ($allDeps as $dep) {
                $r[$dep['fk_department']] = $dep->fkDepartment->name;
            }
            return $r;
        });
        
        return $r;
    }
    
    /*
     * найти всех, кто может заходить в црм, включая добавленных в отделы
     */
    public function getAllUsersCrm($id = NULL, $wdep = true, $format = '{lastname} {name} {secondname}') {
        $dependency = new \yii\caching\ExpressionDependency();
        $dependency->expression = $format === '{lastname} {name} {secondname}';
        
        $b = User::getDb()->cache(function ($db) use ($wdep, $format) {
            $b = [];
            
            $allAdded = CrmAccessSettings::findWDep()->all();
            foreach ($allAdded as $oneAdded) {
                $b[$oneAdded['fk_user']] = $oneAdded->fkUser->getFullName($format);
            };
            
            if(!$wdep) {//если не указано, то добавить еще админов(то есть вообще всех, кто может заходить)
                $allAdded = User::getAdmins(false);
                foreach ($allAdded as $oneAdded) {
                    $b[$oneAdded['id_user']] = $oneAdded->getFullName($format);
                };
            }
            asort($b);
            
            return $b;
        }, 3600, $dependency);
        
        if(!empty($id) && !isset($b[$id])) return User::getAll($id);//уволен например - все равно выдать инфу, во избежание ошибки
        if($id) return $b[$id];
        return $b;
    }
    
    /*
     * есть ли уволенные сотрудники, у которых есть клиенты
     * чистые запросы, не должен долго выполняться
     */
    public function haveFiredClients() {
        return Yii::$app->db->cache(function ($db) {
            $result = [];
            $firedMan = \Yii::$app->db->createCommand('SELECT id_user FROM `user` LEFT JOIN user_status ON user.fk_status = user_status.pk_status WHERE id_user IN(SELECT DISTINCT(`responsible_user`) FROM `crm_company` WHERE `fk_department` = '.\app\modules\crm\CrmModule::$idCrmDep.') AND status = '.\app\modules\user\models\UserStatus::FIRED)->queryAll();
            $firedManc = \Yii::$app->db->createCommand('SELECT id_user FROM `user` LEFT JOIN user_status ON user.fk_status = user_status.pk_status WHERE id_user IN(SELECT DISTINCT(`responsible_user`) FROM `crm_contact` WHERE `fk_department` = '.\app\modules\crm\CrmModule::$idCrmDep.') AND status = '.\app\modules\user\models\UserStatus::FIRED)->queryAll();

            foreach ($firedMan as $manager) {
                array_push($result, $manager['id_user']);
            }
            foreach ($firedManc as $manager) {
                array_push($result, $manager['id_user']);
            }
            return $result;
        });
    }
    
    
    /*
     * проверка на доступ в црм
     * UPD настройка входа отдельно от интранета
     */
    public function haveAccess($id_user = NULL) {
        if(Yii::$app->user->isGuest) return false;
        if(Yii::$app->user->identity->isAdmin()) return true;
        
        if(!$id_user) $id_user = Yii::$app->user->id;
        
        //найти совпадение в таблице допуска, если есть - значит разрешить
        $access = CrmAccessSettings::find()
            ->andWhere(['fk_user' => $id_user])
            ->one();
        
        if($access) return true;
        return false;
    }
    
    /*
     * имеет ли все права на отдел
     * поиск сразу по роли руководителя отдела
     */
    public function canAdminDep($id_user = NULL) {
        if($id_user === NULL) $id_user = Yii::$app->user->id;
        
        // может и так быть
        if($id_user === NULL) return false;
        
        if(Yii::$app->user->identity->isAdmin()) return true;
        
        if(!isset(self::$_usersCanAdmin[$id_user])) {
            $access = CrmAccessSettings::find()
                ->joinWith('fkRole')
                ->andWhere(['fk_department' => self::$idCrmDep, 'fk_user' => $id_user])
                ->andWhere(['like', 'json_data', 'head_dep_crm'])//CrmRights
                ->one();
            self::$_usersCanAdmin[$id_user] = ($access) ? true : false;
        }
        return self::$_usersCanAdmin[$id_user];
    }
}
