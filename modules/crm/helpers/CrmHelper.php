<?php
namespace app\modules\crm\helpers;

use Yii;
use app\modules\crm\models\CrmContactsInfo;
use app\modules\crm\models\CrmRelationTags;
use app\modules\crm\models\CrmTags;
use app\modules\crm\models\CrmTasks;
use app\modules\crm\models\CrmCompany;
use app\modules\crm\models\CrmContact;
use yii\helpers\Html;

class CrmHelper {
    
    /*
     * удалять и сохранять связи с контактами
     */
    public static function saveContacts($modelContact, $model)
    {        
        CrmHelper::deleteContactsModel($model);
        
        $contactfk = $model->fk;//fk модели для сохранения
        
        //внести новые связи
        foreach ($modelContact->attributes['contact_field'] as $i => $value) {//так счетчик удобнее
            if(isset($modelContact->attributes['contact_field'][$i]) && !empty($modelContact->attributes['contact_value'][$i])){
                $mc = new CrmContactsInfo();
                $mc->$contactfk = $model->getPrimaryKey();
                $mc->contact_field = $modelContact->attributes['contact_field'][$i];
                $mc->contact_value = $modelContact->attributes['contact_value'][$i];
                if(stripos($mc->contact_field, 'phone') !== FALSE) {
                    $mc->contact_value_search = preg_replace("/\D/","",$mc->contact_value);//оставит только монолитные цифры
                }
                $mc->contact_comment = $modelContact->attributes['contact_comment'][$i];
                if(empty($mc->contact_comment)) $mc->contact_comment = NULL;
                $mc->save();
            }
        }
    }
    
    public static function deleteContactsModel($model) {
        foreach ($model->contacts as $old) {
            $old->delete();
        }
    }
    
    /*
     * смена ответственного + оставить системный комментарий в истории(может быть долго)
     * @param $arrIds {array}       - ид объектов
     * @param $new_id_resp {int}    - ид нового ответственного
     * @param $model {Object}       - модель в которой происходят изменения
     */
    public function changeResponsible($arrIds, $new_id_resp, $model) {
        $params = ['responsible_user' => $new_id_resp];
        $responsible_arr = $oldresponsible_arr = [];
        
        if($model->className() === CrmCompany::className()){//у компаний нужно и контакты менять
            //найти ответственных до изменения
            $oldResponsible = (new \yii\db\Query())
                ->select(['responsible_user', 'pk_company'])
                ->from($model::tableName())
                ->where(['in', 'pk_company', $arrIds])
                ->all();
            
            foreach ($oldResponsible as $oidc) {
                $oldresponsible_arr[$oidc['pk_company']] = $oidc['responsible_user'];
            }
            //end найти ответственных до изменения
            
            $count = $model::updateAll($params, ['in', 'pk_company', $arrIds]);//сменить у компаний
            $countContacts = CrmContact::updateAll($params, ['in', 'fk_crm_company', $arrIds]);//сменить у контактов компаний
            
            //найти ответственных после изменения
            $newResponsible = (new \yii\db\Query())
                ->select(['responsible_user', 'pk_company'])
                ->from($model::tableName())
                ->where(['in', 'pk_company', $arrIds])
                ->all();//найти ответственных после изменения
            
            foreach ($newResponsible as $idc) {
                $responsible_arr[$idc['pk_company']] = $oldresponsible_arr[$idc['pk_company']] . '|' . $idc['responsible_user'];
            }
            //end найти ответственных после изменения
        } else if($model->className() === CrmContact::className()) {//все остальное - обычно            
            //найти ответственных до изменения
            $oldResponsible = (new \yii\db\Query())
                ->select(['responsible_user', 'pk_contact'])
                ->from($model::tableName())
                ->where(['in', 'pk_contact', $arrIds])
                ->all();
            
            foreach ($oldResponsible as $oidc) {
                $oldresponsible_arr[$oidc['pk_contact']] = $oidc['responsible_user'];
            }
            //end найти ответственных до изменения
            
            $count = $model::updateAll($params, ['in', 'pk_contact', $arrIds]);
            
            //найти ответственных после изменения
            $newResponsible = (new \yii\db\Query())
                ->select(['responsible_user', 'pk_contact'])
                ->from($model::tableName())
                ->where(['in', 'pk_contact', $arrIds])
                ->all();//найти ответственных после изменения
            
            foreach ($newResponsible as $idc) {
                $responsible_arr[$idc['pk_contact']] = $oldresponsible_arr[$idc['pk_contact']] . '|' . $idc['responsible_user'];
            }
            //end найти ответственных после изменения
        } else {
            return 'Недопустимая модель для смены ответственного';
        }
        
        //создать комментарий
        CrmTasks::createSysComment(CrmTasks::TEXT_CHANGE_RESPONSIBLE, $arrIds, $model, ['addtext' => $responsible_arr]);
        
        return $count;
    }
    
    /*
     * сохранение(добавление) тегов массовое
     * тег может уже быть при сохранении
     * @param $arrIds {array}       - ид объектов
     * @param $tags {array}         - массив тегов(1 или несколько)
     * @param $model {Object}       - модель к которой добавить теги
     * @param $removeOld {bool}   - удалить ли текущие теги(сохранение с нуля, пересохранение)   
     */
    public function changeTags($arrIds, $tags, $model, $removeOld = false) {
        $inserted = [];//вставленные новые теги, чтоб не искать заново
        
        if($removeOld && !$model->isNewRecord) {
            CrmHelper::deleteTagsModel($model);
        }
        
        if(empty($tags)) return false;
            
        $arrToInsert = [];
        foreach ($arrIds as $id) {
            if(empty($id)) continue;
            foreach ($tags as $tag) {
                if((int)($tag) === 0) {//новый тег
                    if(!isset($inserted[$tag])){//нужно создать новый
                        $mt = new CrmTags();
                        $mt->fk_department = \app\modules\crm\CrmModule::$idCrmDep;//сохранение в отдел
                        $mt->name_tag = $tag;
                        $mt->save();
                        $inserted[$tag] = $mt->pk_tag;
                    }
                    $tag = $inserted[$tag];
                }
                
                $arrToInsert[] = [$tag, $id];
            }
        }
                
        $count = Yii::$app->db->createCommand()->batchInsert(CrmRelationTags::tableName(), ['fk_tag', $model->fk], $arrToInsert)->execute();
        return $count;
    }
    
    public static function deleteTagsModel($model) {
        //Yii::error('Удаляет '.$model->getPrimaryKey(), 'tags');
        //отключено временно
        \app\modules\crm\models\CrmRelationTags::deleteAll([$model->fk => $model->getPrimaryKey()]);
    }
    
    /**
     * перенос в архив
     * 
     * @param {Array} $arrIds           - ид объектов
     * @param {Object} $model           - модель объекта
     * @param {int} $responsible_user   - ид сотрудника
     * @return {int} кол-во перенесенных объектов
     */
    public function toArchive($arrIds, $model, $responsible_user = NULL) {
        return self::changeArchive($arrIds, $model, 1, $responsible_user);
    }
    
    public function fromArchive($arrIds, $model, $responsible_user = NULL) {
        return self::changeArchive($arrIds, $model, 0, $responsible_user);
    }
    
    /*
     * действия с архивом.  компании с контактами(полностью логично)
     * @param $arrIds {array}           - ид объектов
     * @param $model {Object}           - модель данных
     * @param $stock {int}              - значение архива(1 - в архиве, 0 - не)
     * @param $responsible_user {int}   - ид сотрудника, которому передать
     */
    public function changeArchive($arrIds, $model, $stock, $responsible_user = NULL) {
        $params = ['stock' => $stock, 'update_date' => \app\models\FunctionModel::getDateTimestamp(), 'stock_date' => \app\models\FunctionModel::getDateTimestamp(), 'updated_user' => Yii::$app->user->id];
        $code = ($stock == 1) ? CrmTasks::TEXT_TO_ARCHIVE : CrmTasks::TEXT_FROM_ARCHIVE;
        $count = 0;
        if($responsible_user) $params['responsible_user'] = $responsible_user;
        
        if($model->className() === CrmCompany::className()){//у компаний нужно и контакты менять
            $count = $model::updateAll($params, ['in', 'pk_company', $arrIds]);//поместить компании
            $countContacts = CrmContact::updateAll($params, ['in', 'fk_crm_company', $arrIds]);//поместить контакты компаний
        } else if($model->className() === CrmContact::className()) {
            $count = $model::updateAll($params, ['in', 'pk_contact', $arrIds]);
        }
        
        CrmTasks::createSysComment($code, $arrIds, $model, ['addtextall' => Yii::$app->user->id]);
        
        return $count;
    }
    
    /*
     * удаление сотрудника из цр полностью(при увольнении)
     * @param {int} $id_user - ид сотрудника
     */
    public function deleteUserFromCrm(int $id_user) {
        try {
            \app\modules\crm\models\CrmAccessSettings::deleteAll(['fk_user' => $id_user]);
        } catch (\yii\base\ExitException $e) {
            Yii::error('Не удаляет сотрудника из црм', 'CrmHelper');
        }
    }
    
    /*
     * перенос компаний в контакты и обратно
     * основные данные (crm_company, crm_contact) - создание новой, удаление старой
     * данные контактной информации(crm_contacts_info) - смена(fk_crmcompany, fk_crmcontact)
     * данные сделок(crm_deals) - смена(fk_crmcompany, fk_crmcontact)
     * данные тегов(crm_relation_tags) - смена(fk_crmcompany, fk_crmcontact)
     * 
     * @param $arrIds {array}           - ид объектов
     * @param $model {Object}           - модель данных, которая переносится
     */
    /*public function transfer($arrIds, $model) {
        if($model->className() === CrmCompany::className()) { // компании
            foreach ($arrIds as $id) {
                var_dump($id);
            }
        } else if($model->className() === CrmContact::className()) {
            
        }
    }*/
}