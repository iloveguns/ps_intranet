<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Create Crm Contact');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Crm Contacts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create');
?>
<div class="crm-contact-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>