<?php

use yii\helpers\Html;

$this->title = $model->name_contact;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Crm Contacts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-contact-view">

    <h2>
        <strong><?= Html::encode($this->title) ?></strong>
        <?php if($model->stock) echo '<span class="text-red">('.Yii::t('app/models', 'In archive').')</span>' ?>
    </h2>
    
    <div class="row">
        <div class="col-md-4">
            <div class="mb-1em">
                <?= $model->allTagsLink ?>
            </div>
            
            <?= $this->render('/_contact_table', ['model' => $model]) ?>
            
            <h4>Компания</h4><hr>
            <?php if($fkCompany = $model->fkCrmCompany) : ?>
                <h3><?= Html::a($fkCompany->name_company, ['/crm/crm-company/view', 'id' => $fkCompany->pk_company])?></strong></h3>
                <?= $this->render('/_company_table', ['model' => $model->fkCrmCompany]) ?>
            <?php else : ?>
                <h4><?= Yii::t('app', 'No data')?></h4>
            <?php endif ?>
                
            <h4>Сделки</h4><hr>
            <?= $this->render('/_deals', ['model' => $model]) ?>
            
            <i class="fa fa-refresh" aria-hidden="true"></i> Изменено: <?= Yii::$app->formatter->asDatetime($model->update_date) ?> : <?php if($model->updatedUser) echo $model->updatedUser->fullName ?>
        </div>
        
        <div class="col-md-8">
            <?= $this->render('/_currenttasks', ['model' => $model]) ?>
            
            <?= app\modules\crm\widgets\WidgetComments::widget(['modelEntity' => $model]) ?>
            
            <?= $this->render('/_donetasks', ['model' => $model]) //выполненные задачи?>
        </div>
    </div>
</div>