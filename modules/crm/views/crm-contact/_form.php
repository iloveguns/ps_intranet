<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\select2\Select2;
use kartik\datecontrol\DateControl;
use yii\helpers\Url;
?>

<div class="crm-contact-form">

    <?php $form = ActiveForm::begin(['id' => 'create-contact-form']); ?>

    <div class="row">
        <?= $form->field($model, 'name_contact', ['options' => ['class' => 'col-lg-6 col-md-12']])->textInput(['maxlength' => true]);
            $this->registerJs("
            $('#crmcontact-name_contact').keyup(function() { 
                if($(this).val().length > 2){                    
                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        url: '".Url::to(['/crm/crm-contact/ajax-check'])."',
                        data: { 'string' : $(this).val() },
                        success: function(data) {
                            if(data.success === true) {
                                $('#possible_variants ul').html('');
                                $('#possible_variants').removeClass('hidden');
                                $.each( data.data, function( key, value ) {
                                    $('#possible_variants ul').append('<li>' + value + '</li>');
                                });
                            } else {
                                $('#possible_variants').addClass('hidden');
                            }
                        }
                    });
                } else {
                    $('#possible_variants').addClass('hidden');
                }
            });", yii\web\View::POS_READY);
        ?>
        <div class="col-lg-6 col-md-12 hidden" id="possible_variants">
            <p><?= Yii::t('app/views', 'Possibly you trying to add existing contacts')?>: </p>
            <ul></ul>
        </div>
    </div>

    <?php
    $model->tags = $model->getTags();
    
    echo $form->field($model, 'tags')->widget(Select2::classname(), [
        'data' => \app\modules\crm\models\CrmTags::getAll(),
        'options' => ['placeholder' => Yii::t('app', 'Select'), 'multiple' => true],
        'pluginOptions' => [
            'tags' => true,
            'maximumInputLength' => 30
        ],
    ]) ?>
    
    <?= $form->field($model, 'post_contact')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'comment_contact')->textArea() ?>
    
    <?= $form->field($model, 'responsible_user')->widget(Select2::classname(), [
        'data' => \app\modules\crm\CrmModule::getAllUsersCrm(),
        'options' => ['placeholder' => Yii::t('app', 'Select')],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]);?>
    
    <?= $form->field($model, 'birthdate_contact')->widget(DateControl::classname(), [
        'type'=>DateControl::FORMAT_DATE,
        'ajaxConversion'=>false,
        'options' => [
            'pluginOptions' => [
                'autoclose' => true
            ]
        ]
    ]);?>
    
    <?php if(!isset($fast)) : ?>
        <?php
$formatJs = <<< 'JS'
let formatCompanies = function (repo) {
    let markup =
    '<div class="row">' + 
        '<div class="col-sm-6">' + repo.text + '</div>' +
        '<div class="col-sm-6">' + repo.name + '</div>' +
    '</div>';
return '<div style="overflow:hidden;">' + markup + '</div>';
};
JS;
        $this->registerJs($formatJs, \yii\web\View::POS_HEAD);
        if($model->fk_crm_company) $initValue = $model->fkCrmCompany->name_company;
        else $initValue = '';
        echo $form->field($model, 'fk_crm_company')->widget(Select2::classname(), [
            'initValueText' => $initValue,
            'language' => 'ru',
            'options' => ['placeholder' => Yii::t('app/views', 'crm find companies')],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'ajax' => [
                    'url' => \yii\helpers\Url::to(['crm-company/find-company']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('formatCompanies'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ],
        ]);
        ?>
    <?php else : ?>
        <?= $form->field($model, 'fk_crm_company')->hiddenInput()->label(FALSE) ?>
    <?php endif ?>
    
    <?= app\components\WidgetMultipleValuesn::widget([
        'title' => 'Контактная информация',
        'relation' => $model->contacts,
        'attrs' => [
            'contact_field' => ['type' => 'dropdown', 'items' => app\modules\crm\models\CrmContactsInfo::PossibleValues()],
            'contact_value' => ['type' => 'text'],
            'contact_comment' => ['type' => 'text'],
        ],
        'model' => new app\modules\crm\models\CrmContactsInfo(),
        'addJs' => "
            //проверка на мобильный - вставить маску
            $('.val1class').click(function(){
            let mask = '+7 999-999-99-99';
                if($(this).val() == 'mobilePhone') {
                    $('#crmcontactsinfo-contact_value-' + $(this).parent().parent().attr('data-mid')).mask(mask);
                } else {
                    $('#crmcontactsinfo-contact_value-' + $(this).parent().parent().attr('data-mid')).unmask(mask);
                }
            });

           $(document).ready(function(){
               $('.val1class').each(function() {
                   $(this).click();
               });
           });"
    ]) ?>
    
    <?= $form->field($model, 'stock', ['labelOptions' => ['data-toggle' => "tooltip", 'data-placement' => "top", 'title' => 'Позволяет поместить объект в хранилище, где будет находиться до момента восстановления.']])->checkbox([], false); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>