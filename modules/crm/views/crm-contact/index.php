<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
use app\modules\crm\models\CrmTags;
use yii\widgets\Pjax;

$this->title = Yii::t('app/views', 'Crm Contacts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-contact-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <div>
        <?= Html::a(Yii::t('app/views', 'Create Crm Contact'), ['create'], ['class' => 'btn btn-success']) ?>
        <i class="ml-1em"></i>
        <?= $this->render('/_buttons', ['searchModel' => $searchModel, 'name' => 'контакты', 'idgrid' => 'contact-grid', 'transfername' => 'компании']) ?>
    </div>
    
    <?= GridView::widget([
        'id' => 'contact-grid',
        'pager' => [
            'firstPageLabel' => Yii::t('app/views', 'firstPageLabel'),
            'lastPageLabel'  => Yii::t('app/views', 'lastPageLabel')
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'checkboxOptions' => ['onclick' => 'js:checkShowBtns(this.checked);']
            ],

            [
                'attribute' => 'name_contact',
                'value' => function ($model, $key, $index, $column) {
                    return Html::a($model->name_contact, ['view', 'id' => $model->pk_contact], ['data-pjax' => '0']);
                },
                'format' => 'raw',
            ],
            'post_contact',
            [
                'attribute' => 'fk_crm_company',
                'value' => function ($model, $key, $index, $column) {
                    if($model->fk_crm_company)
                        return Html::a($model->fkCrmCompany->name_company, ['/crm/crm-company/view', 'id' => $model->fk_crm_company]);
                },
                'filter' => \kartik\select2\Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'fk_crm_company',
                    'data' => \app\modules\crm\models\CrmCompany::getAll(),
                    'options' => [
                        'placeholder' => Yii::t('app', 'Select'),
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
                'format' => 'raw',
            ],
            [
                'attribute' => 'responsible_user',
                'value' => function ($model, $key, $index, $column) {
                    return \app\modules\user\models\User::getAll($model->responsible_user, '{lastname} {name}');
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'responsible_user',
                    'data' => app\modules\crm\CrmModule::getAllUsersCrm(NULL, true, '{lastname} {name}'),
                    'options' => ['placeholder' => Yii::t('app', 'Select'),],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'format' => 'raw',
            ],
            [
                'attribute' => 'contactsPhones',
                'header' => Yii::t('app/models', 'Crm Phones Object').'&nbsp;'.Html::a('(Инфо)', ['/crm/crm-faq/view', 'id' => 8]),//ид заметки про поиск номеров
                'value' => function ($model, $key, $index, $column) {
                    return $model->allPhones;
                },
                'filterInputOptions' => [
                    'placeholder' => 'номера слитно, через запятую',
                    'class' => 'form-control',
                ],
                'format' => 'raw',
            ],
            [
                'attribute' => 'contactsEmails',
                'header' => Yii::t('app/models', 'Crm Emails Object'),//ид заметки про поиск номеров
                'value' => function ($model, $key, $index, $column) {
                    return $model->allEmails;
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'tags',
                'value' => function ($model, $key, $index, $column) {
                    return $model->allTagsLink;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'tags',
                    'data' => CrmTags::getAll(),
                    'options' => ['placeholder' => Yii::t('app', 'Select'), 'multiple' => true],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'format' => 'raw',
            ],
            [
                'attribute' => 'stock',
                'label' => Yii::t('app/models', 'In archive'),
                'value' => function ($model, $key, $index, $column) {
                    return app\modules\crm\CrmModule::getStock($model->stock);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'stock',
                    'data' => app\modules\crm\CrmModule::getStock(),
                    'options' => ['placeholder' => Yii::t('app', 'Select'),],
                    'pluginOptions' => [
                        'tags' => true,
                        'allowClear' => true
                    ],
                ]),
                'headerOptions' => ['data-toggle' => "tooltip", 'data-placement' => "top", 'title' => '"'.Yii::t('app', 'Yes').'" - поиск по объектам в архиве, "'.Yii::t('app', 'No').'" - поиск по активным объектам'],
                'format' => 'raw',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons'=>[
                    'view' => function ($url, $model) {
                        
                    },
                    'update'=>function ($url, $model) {
                        if($model->canAdmin()) return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [$url]);
                    },
                    'delete'=>function ($url, $model) {
                        if($model->canAdmin()) return Html::a('<span class="glyphicon glyphicon-trash"></span>', [$url], ['data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],]);
                    },
                ],
            ],
        ],
        'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
        'pager' => [
            'class' => \liyunfang\pager\LinkPager::className(),
            'pageSizeList' => Yii::$app->params['pageSizeList'],
            'pageSizeOptions' => ['class' => 'form-control pagesizeselect'],
        ],
    ]);
                    
    $this->registerJs("
        //проверка нужно ли показывать/скрывать кнопки
        function checkShowBtns(value){
            let selectedLength = $('#contact-grid').yiiGridView('getSelectedRows').length;

            if(value && selectedLength >= 1) {//показать кнопки
                $('.action-btns').show();
            } else if(!value && selectedLength == 0){//скрыть кнопки
                $('.action-btns').hide();
            }
        }

        //событие клика на все сразу
        $('.select-on-check-all').click(function() {
            let tchecked = this.checked;
            setTimeout(function(){ checkShowBtns(tchecked); }, 1);
        });
    ", yii\web\View::POS_END);
    ?>
    <?php Pjax::end(); ?>
</div>