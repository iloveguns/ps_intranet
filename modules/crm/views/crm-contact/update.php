<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Update Crm Contact');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Crm Contacts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_contact, 'url' => ['view', 'id' => $model->pk_contact]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="crm-contact-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>