<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
$this->title = Yii::t('app/models', 'Crm Tasks Results');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-tasks-results-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app/models', 'Create good Crm Tasks Results'), ['create', 'id' => 'good'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app/models', 'Create bad Crm Tasks Results'), ['create', 'id' => 'bad'], ['class' => 'btn btn-danger']) ?>
    </p>
    
    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'name_task_result',
                    'value' => function ($model, $key, $index, $column) {
                        $t = ($model->denial == 1) ? 'red' : 'green';
                        return '<span class="text-'.$t.'">'.$model->name_task_result.'</span>';
                    },
                    'format' => 'raw',
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
    
</div>