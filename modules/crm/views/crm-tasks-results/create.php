<?php

use yii\helpers\Html;

$this->title = Yii::t('app/models', 'Create '.$_GET['id'].' Crm Tasks Results');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Crm Tasks Results'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-tasks-results-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>