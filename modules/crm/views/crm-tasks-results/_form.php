<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="crm-tasks-results-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_task_result')->textInput() ?>
    
    <?= $form->field($model, 'denial')->hiddenInput(['value' => ($_GET['id'] == 'good') ? 0 : 1])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>