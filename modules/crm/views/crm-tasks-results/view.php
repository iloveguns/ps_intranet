<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->pk_task_result;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Crm Tasks Results'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-tasks-results-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app/models', 'Update'), ['update', 'id' => $model->pk_task_result], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app/models', 'Delete'), ['delete', 'id' => $model->pk_task_result], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app/models', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'pk_task_result',
            'name_task_result:ntext',
            'denial',
            'fk_department',
        ],
    ]) ?>

</div>