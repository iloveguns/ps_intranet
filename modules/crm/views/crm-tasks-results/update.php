<?php

use yii\helpers\Html;

$this->title = Yii::t('app/models', 'Update Crm Tasks Results') . $model->pk_task_result;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Crm Tasks Results'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pk_task_result, 'url' => ['view', 'id' => $model->pk_task_result]];
$this->params['breadcrumbs'][] = Yii::t('app/models', 'Update');
?>
<div class="crm-tasks-results-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>