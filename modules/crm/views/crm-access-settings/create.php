<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Create Crm Access Settings');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Crm Access Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-access-settings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>