<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
use app\modules\crm\models\CrmRoles;
use app\modules\crm\CrmModule;

$this->title = Yii::t('app/views', 'Crm Access Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-access-settings-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app/views', 'Create Crm Access Settings'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'fk_user',
                'value' => function ($model, $key, $index, $column) {
                    return Html::a(CrmModule::getAllUsersCrm($model->fk_user), ['/user/user/profile', 'id' => $model->fk_user], ['target' => '_blank']);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'fk_user',
                    'data' => CrmModule::getAllUsersCrm(NULL),
                    'options' => ['placeholder' => Yii::t('app', 'Select'),],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'format' => 'raw',
            ],
            /*[
                'attribute' => 'fk_department',
                'value' => function ($model, $key, $index, $column) {
                    return CrmModule::getDepartments($model->fk_department);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'fk_department',
                    'data' => CrmModule::getDepartments(),
                    'options' => ['placeholder' => Yii::t('app', 'Select'),],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'format' => 'raw',
            ],*/
            [
                'attribute' => 'fk_role',
                'value' => function ($model, $key, $index, $column) {
                    return CrmRoles::getAll($model->fk_role);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'fk_role',
                    'data' => CrmRoles::getAll(),
                    'options' => ['placeholder' => Yii::t('app', 'Select'),],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'format' => 'raw',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>