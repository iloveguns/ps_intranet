<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\modules\crm\models\CrmRoles;
?>

<div class="crm-access-settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fk_user')->widget(Select2::classname(), [
        'data' => app\modules\user\models\User::getAllActive(),
        'options' => ['placeholder' => Yii::t('app', 'Select')],
    ]);?>
    
    <?= $form->field($model, 'fk_role')->widget(Select2::classname(), [
        'data' => CrmRoles::getAll(),
        'options' => ['placeholder' => Yii::t('app', 'Select')],
    ]);?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>