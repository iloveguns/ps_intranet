<?php if($doneTasksAll = $model->doneTasksAll) : 
    
    if($model->className() == app\modules\crm\models\CrmCompany::className()) {
        $pk_company = $model->pk_company;
        $pk_contact = NULL;
    } else if($model->className() == app\modules\crm\models\CrmContact::className()){
        $pk_contact = $model->pk_contact;
        $pk_company = NULL;
    }
?>
    <h4>История</h4>
    <?php foreach ($doneTasksAll as $task) : ?>
        <?= $this->render('/crm-tasks/_task-item', ['model' => $task]) ?>
    <?php endforeach ?>
    
    <?php if(count($doneTasksAll) >= Yii::$app->params['default_count_tasks_crm']) : //если вывода меньше - не нужна кнопка?>
        <button class="btn btn-primary" data-repeat="1" data-lastid-task="<?= $task->pk_task ?>" data-pkcompany="<?= $pk_company ?>" data-pkcontact="<?= $pk_contact ?>" id="get-more-tasks"><?= Yii::t('app/views', 'Load more')?></button>

        <?php    
        $this->registerJs("
            $('#get-more-tasks').click(function(){
                let btn = $(this);
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: '".\yii\helpers\Url::to(['/crm/crm-tasks/ajax-get-tasks'])."',
                    data: { 'last-id-task' : $(this).attr('data-lastid-task'), 'pk_company' : $(this).attr('data-pkcompany'), 'pk_contact' : $(this).attr('data-pkcontact'), 'repeat' : $(this).attr('data-repeat') },
                    success: function(data) {
                        if(data.html.length){
                            btn.before(data.html);
                            btn.attr('last-id-task', data.lastid);
                            btn.attr('data-repeat', parseInt(btn.attr('data-repeat'))+1);
                        } else {
                            btn.remove();
                        }
                        if(!data.haveMore){
                            btn.remove();
                        }
                    },
                    error: function(){
                        modelErrorShow({1: 'Не удалось загрузить'});
                    }
                });
            });
        ", yii\web\View::POS_END);
        ?>
    <?php endif ?>
<?php endif ?>