<?php
use yii\helpers\Html;
?>

<table class="table table-striped table-bordered">
    <tbody>
        <tr>
            <td>
                <strong><?= $model->getAttributeLabel('responsible_user') ?></strong>
            <td>
                <?= $model->responsibleUser->getLinkToProfile(false, false) ?>
        <?php if($model->birthdate_company) : ?>
        <tr>
            <td>
                <strong><?= $model->getAttributeLabel('birthdate_company') ?></strong>
            <td>
                <?= Yii::$app->formatter->asDate($model->birthdate_company) ?>
        <?php endif ?>
        <?php if($model->fullAddress) : ?>
        <tr>
            <td>
                <strong><?= Yii::t('app/models', 'address') ?></strong>
            <td>
                <?= $model->fullAddress?>
        <?php endif ?>
        <?php if($model->comment_company) : ?>
        <tr>
            <td>
                <strong><?= $model->getAttributeLabel('comment_company') ?></strong>
            <td>
                <?= $model->comment_company ?>
        <?php endif ?>
        <?php foreach ($model->contacts as $contact) : ?>
            <tr>
                <td>
                    <strong class="ml-1em"><?= $contact->PossibleValues($contact->contact_field)?></strong>
                <td>
                    <?php 
                    if(stripos($contact->contact_field, 'email') !== FALSE) {
                        $cf = Html::mailto($contact->contact_value, $contact->contact_value);
                    } else if(stripos($contact->contact_field, 'web') !== FALSE) {
                        $t = (stripos($contact->contact_value, 'http') === FALSE && stripos($contact->contact_value, 'www') === FALSE) ? '//' : ''; // создать нормальный путь
                        $cf =  Html::a($contact->contact_value, $t . $contact->contact_value, ['target' => '_blank']);
                    } else if(stripos($contact->contact_field, 'phone') !== FALSE) {//телефонный номер
                        $cf = Html::a($contact->contact_value, 'tel:' . $contact->contact_value);
                    } else {
                        $cf =  $contact->contact_value;
                    }
                    if($contact->contact_comment) $cf .= ' ('.$contact->contact_comment.')';
                    echo $cf;
                    ?>
        <?php endforeach ?>
</table>

<?php if($model->canAdmin()) : ?>
    <div class="mb-2em">
        <?= Html::a(Yii::t('app', 'Update'), ['crm-company/update', 'id' => $model->pk_company], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['crm-company/delete', 'id' => $model->pk_company], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
<?php endif ?>