<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Update Crm Tasks');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Crm Tasks'), 'url' => ['todoline']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="crm-tasks-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>