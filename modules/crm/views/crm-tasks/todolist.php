<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\crm\CrmModule;
use kartik\select2\Select2;
use kartik\date\DatePicker;

$this->title = Yii::t('app/views', 'Crm Tasks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-tasks-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= $this->render('_buttons', ['searchModel' => $searchModel]) ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'create_date',
                'headerOptions' => ['width' => '220px'],
                'value' => function ($model, $key, $index, $column) {
                    return Yii::$app->formatter->asDateTime($model->create_date);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel, 
                    'attribute' => 'create_date',
                    'removeButton' => false,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]),
            ],
            [
                'attribute' => 'date_task',
                'headerOptions' => ['width' => '220px'],
                'value' => function ($model, $key, $index, $column) {
                    return Yii::$app->formatter->asDate($model->date_task) . ' ' . $model->time_task;
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel, 
                    'attribute' => 'date_task',
                    'removeButton' => false,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]),
            ],
            [
                'attribute' => 'update_date',
                'headerOptions' => ['width' => '220px'],
                'value' => function ($model, $key, $index, $column) {
                    return Yii::$app->formatter->asDateTime($model->update_date);
                },
                'format' => 'raw',
                'filter' => DatePicker::widget([
                    'model' => $searchModel, 
                    'attribute' => 'date_task',
                    'removeButton' => false,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]),
            ],
            [
                'attribute' => 'title_task',
                'value' => function ($model, $key, $index, $column) {
                    return $model->title_task;
                },
                'format' => 'raw',
            ],
            'text_task:ntext',
            'status_text:ntext',
            [
                'attribute' => 'created_user',
                'value' => function ($model, $key, $index, $column) {
                    return Html::a(CrmModule::getAllUsersCrm($model->created_user, true, '{lastname} {name}'), ['/user/user/profile', 'id' => $model->created_user], ['target' => '_blank']);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_user',
                    'data' => CrmModule::getAllUsersCrm(),
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'format' => 'raw',
                'visible' => isset($created_user) && $created_user ? true : false,
            ],
            [
                'attribute' => 'fk_user_task',
                'value' => function ($model, $key, $index, $column) {
                    return Html::a(CrmModule::getAllUsersCrm($model->fk_user_task, true, '{lastname} {name}'), ['/user/user/profile', 'id' => $model->fk_user_task], ['target' => '_blank']);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'fk_user_task',
                    'data' => CrmModule::getAllUsersCrm(),
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'format' => 'raw',
            ],
            
                        
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons'=>[
                    'view' => function ($url, $model) {

                    },
                    'update'=>function ($url, $model) {
                        if($model->canAdmin()) return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [$url]);
                    },
                    'delete'=>function ($url, $model) {
                        if($model->canAdmin()) return Html::a('<span class="glyphicon glyphicon-trash"></span>', [$url], ['data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],]);
                    },
                ],
            ],
        ],
        'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
        'pager' => [
            'class' => \liyunfang\pager\LinkPager::className(),
            'pageSizeList' => Yii::$app->params['pageSizeList'],
            'pageSizeOptions' => ['class' => 'form-control pagesizeselect'],
        ],
    ]); ?>
</div>