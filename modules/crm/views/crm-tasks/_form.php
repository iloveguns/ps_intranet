<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use kartik\datecontrol\DateControl;

if(!isset($rand)) {//для использования многих форм в одном месте
    $rand = 'initial-form';
}

$task = (isset($_GET['comment'])) ? false : true;//коммент или задача
?>

<div class="crm-tasks-form">

    <?php $form = ActiveForm::begin(['id' => 'create-task-form']); ?>
    
    <?php if($task) : ?>
    
<?php
if(!isset($fast)) :
$formatJs = <<< 'JS'
let formatRepo = function (repo) {
let markup =
'<div class="row">' + 
    '<div class="col-sm-6">' + repo.text + '</div>' +
    '<div class="col-sm-6">' + repo.name + '</div>' +
'</div>';
return '<div style="overflow:hidden;">' + markup + '</div>';
};
JS;
    $this->registerJs($formatJs, \yii\web\View::POS_HEAD);
    echo $form->field($model, 'title_task')->widget(Select2::classname(), [
        'initValueText' => strip_tags($model->title_task),
        'language' => 'ru',
        'options' => ['placeholder' => Yii::t('app/views', 'crm find contacts')],
        'pluginOptions' => [
            'allowClear' => false,
            'minimumInputLength' => 3,
            'ajax' => [
                'url' => \yii\helpers\Url::to(['/crm/crm-tasks/find-contacts']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('formatRepo'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ]);
else : 
    echo $form->field($model, 'fk_crmcontact')->hiddenInput()->label(FALSE);
    echo $form->field($model, 'fk_crmcompany')->hiddenInput()->label(FALSE);
endif;
?>
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'date_task')->widget(DateControl::classname(), [
                'options' => [
                    'id' => 'date_task'.$rand,//множественные календари
                ],
                'type'=>DateControl::FORMAT_DATE,
                'ajaxConversion'=>false,
            ]);?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'time_task')->dropDownList(\app\models\FunctionModel::getTimes(true, NULL, false), ['class' => 'form-control', 'id' => 'tt'.$rand]) ?>
        </div>
    </div>

    <?= $form->field($model, 'fk_user_task')->widget(Select2::classname(), [
        'data' => \app\modules\crm\CrmModule::getAllUsersCrm(),
        'options' => ['placeholder' => Yii::t('app', 'Select'), 'id' => 'fkut'.$rand],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]) ?>    
    
    <?= $form->field($model, 'fk_typetask')->widget(Select2::classname(), [
        'data' => \app\modules\crm\models\CrmTypetask::getAll(),
        'options' => ['placeholder' => Yii::t('app', 'Select'), 'id' => 'fktt'.$rand],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]);?>

    <?= $form->field($model, 'text_task')->textarea(['rows' => 3, 'id' => 'txt'.$rand]) ?>

    <?php else : ?>
    
        <?= $form->field($model, 'status_text')->textarea(['tinymce' => 'tinymce']) ?>
    
    <?php endif ?>
    
    <?php if($rand == 'initial-form') : ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php endif ?>

    <?php ActiveForm::end(); ?>
    
</div>