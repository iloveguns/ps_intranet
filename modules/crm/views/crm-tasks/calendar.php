<?php 
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Crm Tasks calendar');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-8">
        <?= $this->render('_buttons', ['searchModel' => $searchModel]) ?>
        <div class="mb-1em"></div>

        <div class="box crm-tasks-calendar">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
                <?= yii2fullcalendar\yii2fullcalendar::widget([
                    'options' => [
                      'lang' => 'ru',
                    ],
                    'header' => [
                        'center'=>'title',
                        'left'=>'prev,next today',        
                        'right'=>'month,agendaWeek,agendaDay',
                    ],
                    'ajaxEvents' => Url::to(['/crm/crm-tasks/ajax-calendar'])
                  ]);?>
            </div>
        </div>
    </div>
</div>