<?php
use app\modules\crm\models\CrmTasks;
use app\models\FunctionModel;
use app\modules\crm\models\CrmTypetask;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\modules\crm\models\CrmTasksResults;

if(!isset($changeStatus)) $changeStatus = false;//метка для того, чтоб выводить данные для закрытия задачи
if(!isset($class)) $class = '';
//создано ли кем то
$created_another = ($model->created_user && ($model->created_user != $model->fk_user_task));
if($created_another) $class = 'box-primary';
if($model->status_task == CrmTasks::STATUS_LATE) $class = 'box-danger text-red';

if($model->status_task == $model::STATUS_COMMENT) {//редактирование комментов и задач
    $uurl = Url::to(['/crm/crm-tasks/update', 'id' => $model->pk_task, 'comment' => true]);
} else {
    $uurl = Url::to(['/crm/crm-tasks/update', 'id' => $model->pk_task]);
}

$url = NULL;
if($model->fk_crmcontact) {
    $url = Url::to(['/crm/crm-contact/view', 'id' => $model->fk_crmcontact]);
} else if($model->fk_crmcompany) {
    $url = Url::to(['/crm/crm-company/view', 'id' => $model->fk_crmcompany]);
}
?>
<div class="task-item box <?= $class?> box-solid" id="task-item-<?= $model->pk_task ?>">
    <?php if($url !== NULL) : ?>
    <a href="<?= $url ?>" class="color-inh">
    <?php endif ?>
        <div class="pull-left">
            <?= $model->iconStatus ?>
            <?php if($model->status_task == CrmTasks::STATUS_COMMENT || $model->status_task == CrmTasks::STATUS_SYSCOMMENT) : ?>
                <?= Yii::$app->formatter->asDateTime($model->create_date)?>, <strong><?= $model->fkUserTask->lastname.' '.$model->fkUserTask->name ?></strong><?php if($model->update_date) : ?>, (<i class="fa fa-pencil" aria-hidden="true"></i><?= Yii::$app->formatter->asDateTime($model->update_date)?>) <?php endif ?>
            <?php else : ?>
                <?php if($model->time_task === NULL) $model->time_task = 'allday'?>
                <?= Yii::$app->formatter->asDate($model->date_task)?>, <?= FunctionModel::getTimes(true, $model->time_task)?>, <strong><?= $model->fkUserTask->lastname.' '.$model->fkUserTask->name ?></strong><?php if($model->update_date) : ?>, (<i class="fa fa-pencil" aria-hidden="true"></i>  <?= Yii::$app->formatter->asDateTime($model->update_date)?>) <?php endif ?>
            <?php endif ?>
        </div>
    <?php if($url !== NULL) : ?>
    </a>
    
    <?php if($created_another) : ?>
        <br>
        <span>
            <?= $model->getAttributeLabel('created_user') ?> : <strong><?= $model->fkCreatedUserTask->lastname.' '.$model->fkCreatedUserTask->name ?></strong>
        </span>
    <?php endif ?>
    
    <?php endif ?>
    <?php if(!$changeStatus) : ?>
        <div class="pull-right">
            <?= $model->title_task ?>
            <?php if($model->canAdmin()) : ?>
                <a href="<?= $uurl ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
            <?php endif ?>
            <?php if($model->canAdmin()) : ?>
                <i class="fa fa-times text-red delete-task-entity" aria-hidden="true" data-entity-id="<?= $model->pk_task ?>"></i>
            <?php endif ?>
        </div>
    <?php endif ?>
    
    <div class="clearfix"></div>
    <div class="<?= ($model->status_task == CrmTasks::STATUS_DONE) ? 'text-stroke' : ''?>">
        <?php if($model->status_task == CrmTasks::STATUS_COMMENT || $model->status_task == CrmTasks::STATUS_SYSCOMMENT) : ?>
            <?= $model->status_text ?>
        <?php else : ?>
            <?php if($model->fk_typetask) echo CrmTypetask::getAll($model->fk_typetask) ?> : <?= $model->text_task?>
        <?php endif ?>
    </div>
    
    <?php if($model->status_task == CrmTasks::STATUS_DONE) : ?>
        <?php if($model->fk_task_result) : ?>
            <div>
                <?= $model->getAttributeLabel('fk_task_result') ?> : <?= CrmTasksResults::getOneString($model->fk_task_result) ?>
            </div>
        <?php endif ?>
        <?php if($model->status_text) : ?>
            <div>
                <?= $model->getAttributeLabel('status_text') ?> : <?= $model->status_text ?>
            </div>
        <?php endif ?>
    <?php endif ?>
    
    <?php if($changeStatus) : ?>
        
        <?php $form = ActiveForm::begin(['id' => 'allform-'.$model->pk_task]); ?>    
            <div class="row">
                <div class="col-lg-3 col-md-12">
                    <?= $form->field($model, 'fk_task_result')->widget(Select2::classname(), [
                        'data' => CrmTasksResults::getAll(),
                        'options' => ['placeholder' => Yii::t('app', 'Select'), 'id' => 'ftr-'.$model->pk_task],
                        'pluginOptions' => [
                            'allowClear' => false,
                        ],
                    ]) ?>
                    <?= Html::hiddenInput('id_task', $model->pk_task) ?>
                </div>
                <div class="col-lg-6 col-md-12">                
                    <?= Html::label($model->getAttributeLabel('status_text')) ?>
                    <?= Html::textarea('status_text', '',  ['class' => 'form-control status_text', 'rows' => 3, 'onkeyup' => 'textAreaAdjust(this)'])?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <?= Html::checkbox('wplan_task', false, ['id' => 'lf'.$model->pk_task, 'class' => 'wplan_task', 'data-id' => $model->pk_task]) ?>
                    <?= Html::label('Запланировать будущую задачу', 'lf'.$model->pk_task)?>
                </div>
            </div>
            <div class="row" id="add-task-future-<?= $model->pk_task ?>" style="display:none">
                <div class="col-md-12">
                    <?php 
                    $modelTask = new app\modules\crm\models\CrmTasks();
                    $modelTask->date_task = \app\models\FunctionModel::getDateWParam('Y-m-d');
                    if($model->fk_crmcompany) {
                        $modelTask->fk_crmcompany = $model->fk_crmcompany;
                    } else {
                        $modelTask->fk_crmcontact = $model->fk_crmcontact;
                    }

                    echo $this->render('/crm-tasks/_form', ['model' => $modelTask, 'fast' => true, 'rand' => $model->pk_task]);
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 col-md-4">
                    <?= Html::button(Yii::t('app/views', 'execute'), ['class' => 'form-control btn btn-success send-task-text', 'data-task-id' => $model->pk_task]) ?>
                </div>
            </div>    
        <?php ActiveForm::end(); ?>
    
        <?php 
        $this->registerJs("
            //checking checkboxes
            $('.wplan_task').change(function() {
                if(this.checked) {
                    $('#add-task-future-' + $(this).attr('data-id')).show();
                } else {
                    $('#add-task-future-' + $(this).attr('data-id')).hide();
                }
            });
            
            //выполнение + возможное создание задачи
            $('.send-task-text').click(function(){
                let idtask = $(this).attr('data-task-id');
                let objToSend = $('#allform-' + idtask).serializeArray();
                let fk_result = $('#ftr-' + idtask).val();

                $.ajax({
                    url: '". Url::toRoute('crm-tasks/execute') ."',
                    type: 'post',
                    data: objToSend,
                    beforeSend: function() {
                        if(!fk_result){
                            $('.field-ftr-'+idtask+' .help-block').html('Обязательно к заполнению');
                            $('.field-ftr-'+idtask).addClass('has-error');
                            return false;
                        }
                    },
                    success: function (data) {
                        if(data.success == true){
                            $('#pjaxbtntask').click();//обновить текущие задачи
                            let text = 'Задача завершена';
                            if(data.created) text += ' и создана новая';
                            jsNotify(text);
                            setTimeout(function(){ location.reload(); }, 1000);//добавил перезагрузку
                        } else {
                            modelErrorShow(data.errors);
                        }
                    }
                });
            });
            ", yii\web\View::POS_END);
        ?>
    <?php endif ?>
    <?php 
        $this->registerJs("
        //удаление
        $('.delete-task-entity').click(function(){
            if(confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."')){
                let idtask = $(this).attr('data-entity-id');
                console.log(idtask);
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: '".\yii\helpers\Url::to(['/crm/crm-tasks/ajax-delete-entity'])."',
                    data: {
                        id_task: idtask,
                    },
                    success: function(data) {
                        if(data.success == true){
                            jsNotify('Удалено');
                            $('#task-item-'+idtask).remove();
                        }
                    },
                    error: function(){
                        alert('Ошибка удаления TASK_CRM');
                    }
                });

            }
        });
        ", yii\web\View::POS_END);
        ?>
</div>