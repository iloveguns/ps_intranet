<?php

use yii\helpers\Html;
use app\modules\crm\models\CrmTasks;
use app\models\FunctionModel;

$this->title = Yii::t('app/views', 'Crm Tasks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-tasks-index">
    
    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= $this->render('_buttons', ['searchModel' => $searchModel]) ?>
    
    <div class="row">
        <?php if($models['late']) : ?>
            <div class="col-md-4">
                <h3 class="text-red"><?= Yii::t('app/views', 'Crm late tasks')?> : <?= count($models['late']) ?> из <?= $models['countLate'] ?></h3>
                <?php foreach ($models['late'] as $task) : ?>
                    <?= $this->render('_task-item', ['model' => $task]); ?>
                <?php endforeach ?>
            </div>
        <?php else : ?>
            <div class="col-md-4">
                <h3><?= Yii::t('app/views', 'Crm done tasks')?> : <?= count($models['done']) ?> из <?= $models['countDone'] ?></h3>
                <?php foreach ($models['done'] as $task) : ?>
                    <?= $this->render('_task-item', ['model' => $task]); ?>
                <?php endforeach ?>
            </div>
        <?php endif ?>
        <div class="col-md-4">
            <h3><?= Yii::t('app/views', 'Crm today tasks')?> : <?= count($models['today']) ?> из <?= $models['countToday'] ?></h3>
            <?php foreach ($models['today'] as $task) : ?>
                <?= $this->render('_task-item', ['model' => $task, 'class' => 'box-success']); ?>
            <?php endforeach ?>
        </div>
        <div class="col-md-4">
            <h3><?= Yii::t('app/views', 'Crm tomorrow tasks')?> : <?= count($models['tomorrow']) ?> из <?= $models['countTomorrow'] ?></h3>
            <?php foreach ($models['tomorrow'] as $task) : ?>
                <?= $this->render('_task-item', ['model' => $task, 'class' => 'box-default']); ?>
            <?php endforeach ?>
        </div>
    </div>
</div>