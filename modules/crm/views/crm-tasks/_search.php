<?php 
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\datecontrol\DateControl;
use yii\helpers\Html;
use kartik\date\DatePicker;

$form = ActiveForm::begin([
    'action' => [Yii::$app->controller->action->id],
    'method' => 'get',
]); ?>

<?php 
if($action != 'todoline') {
    echo $form->field($model, 'created_user')->widget(Select2::classname(), [
        'data' => \app\modules\crm\CrmModule::getAllUsersCrm(),
        'options' => ['placeholder' => Yii::t('app', 'Select')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
} ?>

<?= $form->field($model, 'fk_user_task')->widget(Select2::classname(), [
    'data' => \app\modules\crm\CrmModule::getAllUsersCrm(),
    'options' => ['placeholder' => Yii::t('app', 'Select')],
    'pluginOptions' => [
        'allowClear' => true
    ],
]);?>

<?= $form->field($model, 'date_task')->widget(DateControl::classname(), [
    'type'=>DateControl::FORMAT_DATE,
    'ajaxConversion'=>false,
    'options' => [
        'pluginOptions' => [
            'autoclose' => true
        ]
    ],
]);?>

<?= $form->field($model, 'update_date')->widget(DateControl::classname(), [
    'type'=>DateControl::FORMAT_DATE,
    'ajaxConversion'=>false,
    'options' => [
        'pluginOptions' => [
            'autoclose' => true
        ]
    ],
]);?>

<?= $form->field($model, 'fk_typetask')->widget(Select2::classname(), [
    'data' => \app\modules\crm\models\CrmTypetask::getAll(),
    'options' => ['placeholder' => Yii::t('app', 'Select')],
    'pluginOptions' => [
        'allowClear' => true,
    ],
]);?>

<?= $form->field($model, 'status_task')->widget(Select2::classname(), [
    'data' => app\modules\crm\models\CrmTasks::getAllStatus(),
    'options' => ['placeholder' => Yii::t('app', 'Select')],
    'pluginOptions' => [
        'allowClear' => true,
    ],
]);?>

<?= $form->field($model, 'stock')->checkbox() ?>

<hr>

<?= $form->field($model, 'search_text')->textInput() ?>

<div class="form-group">
    <label class="control-label"><?= Yii::t('app/models', 'Search text crm dates') ?></label>
    <?= DatePicker::widget([
        'model' => $model,
        'attribute' => 'search_range_from',
        'attribute2' => 'search_range_to',
        'options' => ['placeholder' => Yii::t('app/models', 'Start Date')],
        'options2' => ['placeholder' => Yii::t('app/models', 'End Date')],
        'type' => DatePicker::TYPE_RANGE,
        'form' => $form,
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'autoclose' => true,
        ]
    ]);?>
</div>

<div class="form-group">
    <?= Html::submitButton(Yii::t('app/views', 'Search'), ['class' => 'btn btn-primary']) ?>
    <?= Html::a(Yii::t('app/views', 'Reset'), [Yii::$app->controller->action->id], ['class' => 'btn btn-default']) ?>
</div>

<?php ActiveForm::end(); ?>