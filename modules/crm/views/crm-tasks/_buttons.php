<?php 

use yii\helpers\Html;
$action = Yii::$app->controller->action->id;
?>

<div>
    <?= Html::a(Yii::t('app/views', 'Create Crm Tasks'), ['create'], ['class' => 'btn btn-success']) ?>
    <i class="ml-1em"></i>
    <?= Html::a(Yii::t('app/views', 'Crm Tasks line'), ['todoline'], ['class' => ($action == 'todoline') ? 'btn btn-primary' : 'btn btn-default']) ?>
    <?= Html::a(Yii::t('app/views', 'Crm Tasks calendar'), ['calendar'], ['class' => ($action == 'calendar') ? 'btn btn-primary' : 'btn btn-default']) ?>
    <?= Html::a(Yii::t('app/views', 'Crm Tasks list'), ['todolist'], ['class' => ($action == 'todolist') ? 'btn btn-primary' : 'btn btn-default']) ?>
    <?= Html::a(Yii::t('app/views', 'Crm Errand Tasks list'), ['errandlist'], ['class' => ($action == 'errandlist') ? 'btn btn-primary' : 'btn btn-default']) ?>
    <i class="ml-1em"></i>
    
    <?php if($action != 'calendar') : ?>
    <div class="btn-group dropdown" style="width: 30%">
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><?= Yii::t('app/views', 'Search filter')?> <span class="caret"></span></button>
        <div class="dropdown-menu dropdown-menu-right">
            <div class="col-md-12">
            <?= $this->render('_search', ['model' => $searchModel, 'action' => $action]); ?>
            </div>
        </div>
    </div>
    <?php endif ?>
</div>