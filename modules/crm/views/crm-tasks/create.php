<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Create Crm Tasks');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Crm Tasks'), 'url' => ['todoline']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-tasks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>