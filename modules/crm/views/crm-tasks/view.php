<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->pk_task;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Crm Tasks'), 'url' => ['todoline']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-tasks-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->pk_task], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->pk_task], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'pk_task',
            'title_task',
            'date_task',
            'fk_user_task',
            'fk_typetask',
            'text_task:ntext',
            'create_date',
        ],
    ]) ?>

</div>