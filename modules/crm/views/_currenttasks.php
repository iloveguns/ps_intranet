<?php 
use yii\widgets\Pjax;
use yii\helpers\Html;

kartik\switchinput\SwitchInputAsset::register($this);//зарегать assets сразу, чтоб при создании был порядок

Pjax::begin(['linkSelector'=>'#pjaxbtntask', 'timeout' => false]) ?>
    <?php if($model->workTasks) : //невыполненные задачи?>
        <?php foreach ($model->workTasks as $task) : ?>
            <?= $this->render('/crm-tasks/_task-item', ['model' => $task, 'changeStatus' => true]) ?>
        <?php endforeach ?>
    <?php endif ?>

    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapse-create-task" aria-expanded="false" aria-controls="collapse-create-task">
        Быстрое добавление задачи
    </button>
    <div class="collapse" id="collapse-create-task">
        <div class="well">
            <?php 
            //создание сделки
            $modelTask = new app\modules\crm\models\CrmTasks();
            $modelTask->date_task = \app\models\FunctionModel::getDateWParam('Y-m-d');
            $key = $model->fk;
            $modelTask->$key = $model->getPrimaryKey();
            $modelTask->fk_user_task = $model->responsibleUser->id_user; // ответственный компании(контакта) сразу стоит(создание кому-то)

            echo $this->render('/crm-tasks/_form', ['model' => $modelTask, 'fast' => true]);
            ?>
        </div>
    </div>

    <?php
    $this->registerJs("
    $('#create-task-form').on('beforeSubmit', function(e){
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '".\yii\helpers\Url::to(['/crm/crm-tasks/ajax-create'])."',
            data: $(this).serializeArray(),
            success: function(data) {
                if(data.success == true){
                    jsNotify('Задача создана');
                    $('#pjaxbtntask').click();
                } else {//ошибки
                    modelErrorShow(data.errors);
                }
            },
            error: function(){
                alert('Ошибка сохранения задачи црм');
            }
        });
        return false;
    });
    ", yii\web\View::POS_END);
    ?>

    <?= Html::a("Обновить", ['view', 'id' => $model->getPrimaryKey()], ['class' => 'btn hidden', 'id'=>'pjaxbtntask']);?>
    <div class="mb-1em"></div>
<?php Pjax::end(); ?>