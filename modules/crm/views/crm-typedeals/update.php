<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Update Crm Typetask');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Crm Typetasks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pk_typedeal, 'url' => ['view', 'id' => $model->pk_typedeal]];
$this->params['breadcrumbs'][] = Yii::t('app/views', 'Update');
?>
<div class="crm-typetask-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>