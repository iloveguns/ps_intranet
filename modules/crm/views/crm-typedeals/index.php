<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = Yii::t('app/views', 'Crm Typedeals');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-typetask-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app/views', 'Create Crm Typedeal'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'pk_typetask',
            'name_typedeal',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>