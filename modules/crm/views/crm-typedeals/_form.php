<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="crm-typetask-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_typedeal')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app/views', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>