<?php 
use yii\helpers\Html;
use yii\widgets\Pjax;
?>

<?php Pjax::begin(['linkSelector'=>'#pjaxbtndeal', 'timeout' => false]) ?>
    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapse-create-deal" aria-expanded="false" aria-controls="collapse-create-deal">
        Быстрое добавление
    </button>
    <div class="collapse" id="collapse-create-deal">
        <div class="well">
            <?php 
            //создание сделки
            $modelDeals = new app\modules\crm\models\CrmDeals();
            $key = $model->fk;
            $modelDeals->$key = $model->getPrimaryKey();

            echo $this->render('/crm-deals/_form', ['model' => $modelDeals, 'fast' => true]);
            ?>
        </div>
    </div>

    <?php
    $this->registerJs("
    $('#create-deal-form').on('beforeSubmit', function(e){
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '".\yii\helpers\Url::to(['/crm/crm-deals/ajax-create'])."',
            data: $(this).serializeArray(),
            success: function(data) {
                if(data.success == true){
                    jsNotify('Сделка создана');
                    $('#pjaxbtndeal').click();
                } else {
                    modelErrorShow(data.errors);
                }
            },
            error: function(){
                alert('Ошибка создания сделки');
            }
        });
        return false;
    });
    ", yii\web\View::POS_END);
    ?>

    <?= Html::a("Обновить", ['view', 'id' => $model->getPrimaryKey()], ['class' => 'btn hidden', 'id'=>'pjaxbtndeal']);?>

    <div class="mb-1em"></div>
    
    <?php if($deals = $model->notDoneDeals) : ?>
        <p class="pull-left text-orange col-md-6"><strong>Незаконченные</strong></p>
        <div class="clearfix"></div>
        <?php foreach ($deals as $deal) {
            echo $this->render('/crm-deals/_view', ['model' => $deal]);
        } ?>
    <?php endif ?>
    
    <?php if($deals = $model->lostDeals) : ?>
        <p class="pull-left text-orange col-md-6"><strong>Потерянные</strong></p>
        <div class="clearfix"></div>
        <?php foreach ($deals as $deal) {
            echo $this->render('/crm-deals/_view', ['model' => $deal]);
        } ?>
    <?php endif ?>

    <?php if($deals = $model->doneDeals) : ?>
        <?php 
        ob_start();
        $sumDeal = $sums = 0;

        foreach ($deals as $deal) {
            $sumDeal += $deal->price_deal;
            ++$sums;
            echo $this->render('/crm-deals/_view', ['model' => $deal, 'done' => true]);
        }

        $out = ob_get_contents();
        ob_end_clean();
        ?>
        <div>
            <p class="pull-left text-green col-md-6"><strong>Успешные</strong></p>
            <p class="pull-right col-md-6 text-right">
                <strong><?= $sums?></strong> на <strong><?= Yii::$app->formatter->asCurrency($sumDeal)?></strong>
            </p>
        </div>
        <div class="clearfix"></div>
        <?= $out ?>
    <?php endif ?>
<?php Pjax::end(); ?>