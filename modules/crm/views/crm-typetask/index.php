<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = Yii::t('app/views', 'Crm Typetasks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-typetask-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app/views', 'Create Crm Typetask'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name_typetask',
            [
                'attribute' => 'important',
                'value' => function ($model, $key, $index, $column) {
                    return ($model->important) ? Yii::t('app', 'Yes') : Yii::t('app', 'No');
                },
                'format' => 'raw',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>