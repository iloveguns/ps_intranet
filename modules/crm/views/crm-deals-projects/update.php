<?php

use yii\helpers\Html;

$this->title = Yii::t('app/models', 'Update Crm Deals Projects') . ' : ' . $model->pk_dp;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Crm Deals Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="crm-deals-projects-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>