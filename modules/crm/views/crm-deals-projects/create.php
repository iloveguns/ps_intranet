<?php

use yii\helpers\Html;

$this->title = Yii::t('app/models', 'Create Crm Deals Projects');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Crm Deals Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-deals-projects-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>