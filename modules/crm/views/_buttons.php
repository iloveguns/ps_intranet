<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<?= Html::button('<i class="fa fa-trash" aria-hidden="true"></i>', ['id' => 'delete-selected', 'class' => 'btn btn-danger action-btns', 'style' => 'display:none', 'title' => 'Удалить выбранные контакты безвозвратно?']) ?>
<?php if(!$searchModel->stock) echo Html::button('<i class="fa fa-archive" aria-hidden="true"></i>', ['id' => 'toarchive-selected', 'class' => 'btn btn-warning action-btns', 'style' => 'display:none;margin-left:5px', 'title' => 'Поместить выбранные '.$name.' в архив?']) ?>
<?php if($searchModel->stock) echo Html::button('<i class="fa fa-reply" aria-hidden="true"></i>', ['id' => 'fromarchive-selected', 'class' => 'btn btn-warning action-btns', 'style' => 'display:none;margin-left:5px', 'title' => 'Восстановить выбранные '.$name.' из архива?']) ?>
<?= Html::button('<i class="fa fa-hand-scissors-o" aria-hidden="true"></i>', ['id' => 'changeresp-selected', 'class' => 'btn btn-primary action-btns', 'style' => 'display:none;margin-left:5px', 'title' => 'Передать выбранные '.$name.'?']) ?>
<?= Html::button('<i class="fa fa-tags" aria-hidden="true"></i>', ['id' => 'tagging-selected', 'class' => 'btn btn-primary action-btns', 'style' => 'display:none;margin-left:5px', 'title' => 'Тегировать выбранные '.$name.'?']) ?>
<?= Html::button('<i class="fa fa-tasks" aria-hidden="true"></i>', ['id' => 'create-task', 'class' => 'btn btn-primary action-btns', 'style' => 'display:none;margin-left:5px', 'title' => 'Создать задачу?']) ?>

<?php
$js = "
//определить что делать по нажатой кнопке
$('.action-btns').click(function() {
    var url,
        confirmMessage = $(this).attr('title'),
        selectedRows = $('#".$idgrid."').yiiGridView('getSelectedRows');
            
    var type = '';
    if('".$idgrid."' === 'company-grid') {
        type = 'crmcompany';
    } else if('".$idgrid."' === 'contact-grid') {
        type = 'crmcontact';
    }
    
    window.type_client = type;

    switch ($(this).attr('id')) {
        case 'delete-selected' :
            url = '". Url::toRoute('delete') ."';
            break;

        case 'toarchive-selected' :
            url = '". Url::toRoute('toarchive') ."';
            break;

        case 'fromarchive-selected' :
            url = '". Url::toRoute('fromarchive') ."';
            break;

        case 'changeresp-selected' :
            showCrmUsersList();
            window.act_url = '". Url::toRoute('changeresp') ."';
            window.act_data = selectedRows;
            window.act_cmess = confirmMessage;
            break;
            
        case 'create-task' :
            showCrmCreateTask();
            window.act_url = '". Url::toRoute('tasks') ."';
            window.act_data = selectedRows;
            window.act_cmess = confirmMessage;
            break;

        case 'tagging-selected' :
            showCrmTagsList();
            window.act_url = '". Url::toRoute('changetags') ."';
            window.act_data = selectedRows;
            window.act_cmess = confirmMessage;
            break;

        default :
            return false;
    }

    if(url) {//если указан адрес - совершить аякс
        sendAjaxActions(url, selectedRows, confirmMessage);
    }
});
";
$this->registerJs($js, yii\web\View::POS_END);
?>