<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\modules\crm\models\CrmTypedeals;
use app\modules\crm\models\CrmTypesDeal;
use app\modules\crm\models\CrmDealsProjects;

?>

<div class="crm-deals-form">

    <?php $form = ActiveForm::begin(['id' => 'create-deal-form']); ?>

    <?= $form->field($model, 'name_deal')->textInput(['placeholder' => $model->getAttributeLabel('name_deal')])->label(false); ?>

    <?= $form->field($model, 'status_deal')->widget(Select2::classname(), [
        'data' => CrmTypedeals::getAll(),
        'options' => ['placeholder' => Yii::t('app', 'Select')],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]);?>
    
    <?= $form->field($model, 'type_deal')->widget(Select2::classname(), [
        'data' => CrmTypesDeal::getAll(),
        'options' => ['placeholder' => Yii::t('app', 'Select')],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]);?>
    
    <?= $form->field($model, 'fk_deals_project')->widget(Select2::classname(), [
        'data' => CrmDealsProjects::getAll(),
        'options' => ['placeholder' => Yii::t('app', 'Select')],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]);?>

    <?= $form->field($model, 'price_deal')->textInput(['type' => 'number']) ?>
    
    <?= $form->field($model, 'fk_crmcontact')->hiddenInput()->label(false) ?>
    
    <?= $form->field($model, 'fk_crmcompany')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>