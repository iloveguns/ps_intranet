<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Update Crm Deal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Crm Deals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="crm-tasks-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>