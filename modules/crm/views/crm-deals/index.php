<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
use app\modules\crm\models\CrmTypedeals;
use yii\web\JsExpression;
use yii\widgets\Pjax;

//js для вывода найденных
$formatJs = <<< 'JS'
let formatRepo = function (repo) {
let markup =
'<div class="row">' + 
    '<div class="col-sm-6">' + repo.text + '</div>' +
    '<div class="col-sm-6">' + repo.name + '</div>' +
'</div>';
return '<div style="overflow:hidden;">' + markup + '</div>';
};
JS;
$this->registerJs($formatJs, \yii\web\View::POS_HEAD);

$this->title = Yii::t('app/models', 'Crm Deals');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-deals-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
             'name_deal',
            [
                'attribute' => 'title_task',
                'value' => function ($model, $key, $index, $column) {
                    return $model->title_task;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'title_task',
                    'language' => 'ru',
                    'options' => ['placeholder' => Yii::t('app/views', 'crm find contacts')],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'ajax' => [
                            'url' => \yii\helpers\Url::to(['/crm/crm-tasks/find-contacts']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('formatRepo'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ],
                ]),
                'format' => 'raw',
            ],
            [
                'attribute' => 'price_deal',
                'value' => function ($model, $key, $index, $column) {
                    return Yii::$app->formatter->asCurrency($model->price_deal, '', [NumberFormatter::MAX_FRACTION_DIGITS => 0]);//без копеек
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status_deal',
                'value' => function ($model, $key, $index, $column) {
                    return CrmTypedeals::getAll($model->status_deal);
                },
                'headerOptions' => ['style' => 'width:12%'],
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status_deal',
                    'data' => CrmTypedeals::getAll(),
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'format' => 'raw',
            ],
            [
                'attribute' => 'responsible_user',
                'value' => function ($model, $key, $index, $column) {
                    return \app\modules\user\models\User::getAll($model->responsible_user, '{lastname} {name}');
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'responsible_user',
                    'data' => app\modules\crm\CrmModule::getAllUsersCrm(NULL, true, '{lastname} {name}'),
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'format' => 'raw',
            ],
            [
                'attribute' => 'fk_deals_project',
                'value' => function ($model, $key, $index, $column) {
                    if($model->fk_deals_project) {
                        return app\modules\crm\models\CrmDealsProjects::getAll($model->fk_deals_project);
                    }
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'fk_deals_project',
                    'data' => app\modules\crm\models\CrmDealsProjects::getAll(),
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'format' => 'raw',
            ],
            [
                'attribute' => 'create_date',
                'value' => function ($model, $key, $index, $column) {
                    return Yii::$app->formatter->asDatetime($model->create_date);
                },
                'filter' => \kartik\datecontrol\DateControl::widget([
                    'model' => $searchModel,
                    'attribute' => 'create_date',
                    'type'=> \kartik\datecontrol\DateControl::FORMAT_DATE,
                ]),
                'format' => 'raw',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons'=>[
                    'view' => function ($url, $model) {
                        
                    },
                    'update'=>function ($url, $model) {
                        if($model->canAdmin()) return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [$url]);
                    },
                    'delete'=>function ($url, $model) {
                        if($model->canAdmin()) return Html::a('<span class="glyphicon glyphicon-trash"></span>', [$url], ['data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],]);
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>