<div class="panel panel-default crm-deals">
    <div class="row panel-body">
        <div class="pull-left col-md-9">
            <?= \yii\helpers\Html::a($model->name_deal, ['/crm/crm-deals/update', 'id' => $model->pk_deal]) ?>
        </div>
        <div class="pull-right col-md-3 text-right">
            <strong><?= Yii::$app->formatter->asCurrency($model->price_deal) ?></strong>
        </div>
        <div class="col-md-12">
            <?php if(empty($done)) : //статус ?>
                <?= app\modules\crm\models\CrmTypedeals::getAll($model->status_deal) ?>
                <br>
            <?php endif ?>
            
            <?= $model->getAttributeLabel('responsible_user') ?> : <strong><?= $model->responsibleUser->fullName ?></strong> (<?= Yii::$app->formatter->asDate($model->create_date) ?>)
        </div>
    </div>
</div>
