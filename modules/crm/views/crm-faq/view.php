<?php

use yii\helpers\Html;

$this->title = strip_tags($model->question_faq);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Crm Faqs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-faq-view">
    
    <div>
        <strong>
            <?= $model->getAttributeLabel('question_faq') ?>
        </strong>
        <div class="panel panel-default">
            <div class="panel-body">
                <?= $model->question_faq ?>
            </div>
        </div>
        
        <strong>
            <?= $model->getAttributeLabel('answer_faq') ?>
        </strong>
        <div class="panel panel-default">
            <div class="panel-body">
                <?= $model->answer_faq ?>
            </div>
        </div>
    </div>

</div>