<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
$this->title = Yii::t('app/models', 'Crm Faqs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-faq-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if(Yii::$app->user->identity->isAdmin()) : ?>
        <p>
            <?= Html::a(Yii::t('app/models', 'Create Crm Faq'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif ?>
    
    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'question_faq',
                    'value' => function ($model, $key, $index, $column) {
                        return Html::a(strip_tags($model->question_faq), ['view', 'id' => $model->pk_faq]);
                    },
                    'format' => 'raw',
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'header'=>'Действия', 
                    'headerOptions' => ['width' => '20'],
                    'template' => '{update} {delete}',
                    'visible' => Yii::$app->user->identity->isAdmin() ? true : false,
                ]
            ],
        ]); ?>
    <?php Pjax::end(); ?>
    
</div>