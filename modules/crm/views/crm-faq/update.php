<?php

use yii\helpers\Html;

$this->title = Yii::t('app/models', 'Create Crm Faq') . $model->pk_faq;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Crm Faqs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pk_faq, 'url' => ['view', 'id' => $model->pk_faq]];
$this->params['breadcrumbs'][] = Yii::t('app/models', 'Update');
?>
<div class="crm-faq-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>