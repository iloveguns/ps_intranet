<?php

use yii\helpers\Html;

$this->title = Yii::t('app/models', 'Create Crm Faq');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Crm Faqs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-faq-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>