<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\redactor\widgets\Redactor;
?>

<div class="crm-faq-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'question_faq')->widget(Redactor::className()) ?>
    
    <?= $form->field($model, 'answer_faq')->widget(Redactor::className()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>