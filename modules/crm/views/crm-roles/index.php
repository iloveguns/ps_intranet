<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\crm\models\CrmRoles;

$this->title = Yii::t('app/views', 'Crm Roles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-roles-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app/views', 'Create Crm Roles'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'fk_organization',
                'value' => function ($model, $key, $index, $column) {
                    return $model->fkOrganization->name;
                },
                'filter' => \app\modules\user\models\Organization::getAll(),
            ],
            [
                'attribute' => 'name',
                'value' => function ($model, $key, $index, $column) {
                    return CrmRoles::getAll($model->pk_role);
                },
                'filter' => CrmRoles::getAll(),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>