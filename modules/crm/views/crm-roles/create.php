<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Create Crm Roles');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Crm Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-roles-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelRights' => $modelRights,
    ]) ?>

</div>