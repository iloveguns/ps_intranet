<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Update Crm Roles') . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Crm Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->pk_role]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="crm-roles-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelRights' => $modelRights,
    ]) ?>

</div>