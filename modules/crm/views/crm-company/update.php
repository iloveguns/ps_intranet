<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Update Crm Company') . ' : ' . $model->name_company;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Crm Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_company, 'url' => ['view', 'id' => $model->pk_company]];
$this->params['breadcrumbs'][] = Yii::t('app/views', 'Update Crm Company');
?>
<div class="crm-company-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>