<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
use app\modules\crm\models\CrmTags;
use yii\widgets\Pjax;

$this->title = Yii::t('app/views', 'Crm Companies');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-company-index">
    <div>
        <?php
        $model = app\modules\crm\models\CrmCompany::getSearchFormAttributes();
        $form = \yii\widgets\ActiveForm::begin(['id' => 'search-attrs-crmcompany']);
        foreach ($model->getAttrs() as $name => $attr) {
            echo '<div class="col-lg-2 col-md-4 col-sm-6">';
            echo $form->field($model, $name)->checkbox();
            echo '</div>';
        }
        echo '<div class="clearfix"></div>';
        echo \yii\helpers\Html::submitButton(\Yii::t('app/models', 'Change search attributes'), ['class' =>  'btn btn-primary']);
        \yii\widgets\ActiveForm::end();
        
        $attrs = $model->getAttrsToGridView($model->attributes, $searchModel);
        ?>
    </div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?php //Pjax::begin(); // от pjax не загружается select2 ?>
    <div>
        <?= Html::a(Yii::t('app/views', 'Create Crm Company'), ['create'], ['class' => 'btn btn-success']) ?>
        <i class="ml-1em"></i>
        <?= $this->render('/_buttons', ['searchModel' => $searchModel, 'name' => 'компании', 'idgrid' => 'company-grid', 'transfername' => 'контакты']) ?>
    </div>
    
    <?= GridView::widget([
        'id' => 'company-grid',
        'pager' => [
            'firstPageLabel' => Yii::t('app/views', 'firstPageLabel'),
            'lastPageLabel'  => Yii::t('app/views', 'lastPageLabel')
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $attrs,
        'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
        'pager' => [
            'class' => \liyunfang\pager\LinkPager::className(),
            'pageSizeList' => Yii::$app->params['pageSizeList'],
            'pageSizeOptions' => ['class' => 'form-control pagesizeselect'],
        ],
    ]);
    
    $this->registerJs("
        //проверка нужно ли показывать/скрывать кнопки
        function checkShowBtns(value){
            let selectedLength = $('#company-grid').yiiGridView('getSelectedRows').length;

            if(value && selectedLength >= 1) {//показать кнопки
                $('.action-btns').show();
            } else if(!value && selectedLength == 0){//скрыть кнопки
                $('.action-btns').hide();
            }
        }

        //событие клика на все сразу
        $('.select-on-check-all').click(function() {
            let tchecked = this.checked;
            setTimeout(function(){ checkShowBtns(tchecked); }, 1);
        });
    ", yii\web\View::POS_END);    
    ?>
    <?php //Pjax::end(); ?>
</div>