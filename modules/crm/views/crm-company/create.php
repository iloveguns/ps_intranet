<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Create Crm Company');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Crm Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-company-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>