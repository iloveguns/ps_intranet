<?php

use yii\helpers\Html;

$this->title = $model->name_company;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Crm Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-company-view">

    <h2>
        <strong><?= Html::encode($this->title) ?></strong>
        <?php if($model->stock) echo '<span class="text-red">('.Yii::t('app/models', 'In archive').')</span>' ?>
    </h2>

    <div class="row">
        <div class="col-md-6 col-lg-5">
            <div class="mb-1em">
                <?= $model->allTagsLink ?>
            </div>
            
            <?= $this->render('/_company_table', ['model' => $model]) ?>
            
            <h4><?= Yii::t('app/views', 'Crm Contacts')?></h4><hr>
            <?= $this->render('_contacts', ['model' => $model]) ?>
            
            <h4>Сделки</h4><hr>
            <?= $this->render('/_deals', ['model' => $model]) ?>
            
            <i class="fa fa-refresh" aria-hidden="true"></i> Изменено: <?= Yii::$app->formatter->asDatetime($model->update_date) ?> : <?php if($model->updatedUser) echo $model->updatedUser->fullName ?>
        </div>
        <div class="col-md-6 col-lg-7">
            <?= $this->render('/_currenttasks', ['model' => $model]) //невыполненные задачи?>
            
            <?= app\modules\crm\widgets\WidgetComments::widget(['modelEntity' => $model]) ?>
            
            <?= $this->render('/_donetasks', ['model' => $model]) //выполненные задачи?>
        </div>
    </div>
</div>