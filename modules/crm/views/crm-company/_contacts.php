<?php 
use yii\helpers\Html;
use yii\widgets\Pjax;
?>
<?php Pjax::begin(['linkSelector'=>'#pjaxbtncontacts', 'timeout' => false]) ?>
    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapse-create-contact" aria-expanded="false" aria-controls="collapse-create-contact">
        Быстрое добавление контакта
    </button>
    <div class="collapse" id="collapse-create-contact">
        <div class="well">
            <?php 
            $modelDeals = new \app\modules\crm\models\CrmContact();
            $modelDeals->fk_crm_company = $model->getPrimaryKey();

            echo $this->render('/crm-contact/_form', ['model' => $modelDeals, 'fast' => true]);
            ?>
        </div>
    </div>
    <div class="mb-1em"></div>
    
    <?php
    $this->registerJs("
    $('#create-contact-form').on('beforeSubmit', function(e){
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '".\yii\helpers\Url::to(['/crm/crm-contact/ajax-create'])."',
            data: $(this).serializeArray(),
            success: function(data) {
                if(data.success == true){
                    jsNotify('Контакт добавлен');
                    $('#pjaxbtncontacts').click();
                } else {
                    alert('Ошибка создания контакта');
                }
            },
            error: function(){
                alert('Ошибка сохранения комментария црм');
            }
        });
        return false;
    });
    ", \yii\web\View::POS_END);?>
    
    <?= Html::a("Обновить", ['view', 'id' => $model->getPrimaryKey()], ['class' => 'btn hidden', 'id'=>'pjaxbtncontacts']);?>

    <div class="panel-group" role="tablist" id="accordions" aria-multiselectable="true">
        <?php $i = 0; foreach ($model->crmContacts as $crmcontact) : ?>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="crmcontact-<?= $crmcontact->pk_contact ?>">
                <h4 class="panel-title">
                    <a href="#collapse<?= $crmcontact->pk_contact ?>" class="collapsed" role="button" data-toggle="collapse" aria-expanded="true" aria-controls="collapse<?= $crmcontact->pk_contact ?>">
                        <?= $crmcontact->name_contact ?>
                    </a>
                    <?= Html::a('ссылка', ['/crm/crm-contact/view', 'id' => $crmcontact->pk_contact], ['class' => 'pull-right'])?>
                </h4>
            </div>
            <div class="panel-collapse collapse <?php if($i == 0) echo 'in'?>" role="tabpanel" id="collapse<?= $crmcontact->pk_contact ?>" aria-labelledby="crmcontact-<?= $crmcontact->pk_contact ?>" aria-expanded="true">
                <div class="panel-body">
                    <?= $this->render('/_contact_table', ['model' => $crmcontact]) ?>
                </div>
            </div>
        </div>
        <?php ++$i; endforeach ?>
    </div>
<?php Pjax::end(); ?>