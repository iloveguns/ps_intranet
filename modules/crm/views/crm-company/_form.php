<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\datecontrol\DateControl;
?>

<div class="crm-company-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <?= $form->field($model, 'name_company', ['options' => ['class' => 'col-lg-6 col-md-7']])->textInput(['maxlength' => true]);
            $this->registerJs("
            $('#crmcompany-name_company').keyup(function() { 
                if($(this).val().length > 2){                    
                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        url: '".Url::to(['ajax-check'])."',
                        data: { 'string' : $(this).val() },
                        success: function(data) {
                            if(data.success === true) {
                                $('#possible_variants ul').html('');
                                $('#possible_variants').removeClass('hidden');
                                $.each( data.data, function( key, value ) {
                                    $('#possible_variants ul').append('<li>' + value + '</li>');
                                });
                            } else {
                                $('#possible_variants').addClass('hidden');
                            }
                        }
                    });
                } else {
                    $('#possible_variants').addClass('hidden');
                }
            });", yii\web\View::POS_READY);
        ?>
        <div class="col-lg-6 col-md-5 hidden" id="possible_variants">
            <p><?= Yii::t('app/views', 'Possibly you trying to add existing companies')?> : </p>
            <ul></ul>
        </div>
    </div>
    
    <?php
    $model->tags = $model->getTags();
    
    echo $form->field($model, 'tags')->widget(Select2::classname(), [
        'data' => \app\modules\crm\models\CrmTags::getAll(),
        'options' => ['placeholder' => Yii::t('app', 'Select'), 'multiple' => true],
        'pluginOptions' => [
            'tags' => true,
            'maximumInputLength' => 30
        ],
    ]) ?>
    
    <?= $form->field($model, 'responsible_user')->widget(Select2::classname(), [
        'data' => \app\modules\crm\CrmModule::getAllUsersCrm(),
        'options' => ['placeholder' => Yii::t('app', 'Select')],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]);?>

    <?= $form->field($model, 'fk_address_country')->widget(Select2::classname(), [
        'data' => app\modules\crm\models\AddressCountry::getAll(),
        'options' => ['placeholder' => Yii::t('app', 'Select'), 'id' => 'fk_address_country'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]) ?>
    
     <?= $form->field($model, 'fk_address_city')->widget(DepDrop::classname(), [
        'type'=>DepDrop::TYPE_SELECT2,
        'options'=>['id'=>'fk_address_city'],
        'pluginOptions'=>[
            'depends'=>['fk_address_country'],
            'placeholder'=>Yii::t('app', 'Select'),
            'url'=>Url::to(['/address-ajax/getcitys'])
        ],
        'pluginEvents' => [
            "depdrop.afterChange"=>"function(event, id, value) { $('#fk_address_city').val(".$model->fk_address_city.");$('#fk_address_city').trigger('depdrop.change'); }",
        ],
    ])?>
    
    <?= $form->field($model, 'fk_address_street')->widget(DepDrop::classname(), [
        'type'=>DepDrop::TYPE_SELECT2,
        'options'=>['id'=>'fk_address_street'],
        'pluginOptions'=>[
            'depends'=>['fk_address_city'],
            'placeholder'=>Yii::t('app', 'Select'),
            'url'=>Url::to(['/address-ajax/getstreet'])
        ],
        'pluginEvents' => [
            "depdrop.afterChange"=>"function(event, id, value) { $('#fk_address_street').val(".$model->fk_address_street."); }",
        ],
    ]);
    $this->registerJs("$('#fk_address_country').trigger('depdrop.change');", yii\web\View::POS_READY);
    ?>

    <?= $form->field($model, 'address_other')->textarea(['maxlength' => true, 'rows' => 3]) ?>
    
    <?= $form->field($model, 'comment_company')->textarea(['maxlength' => true, 'rows' => 3]) ?>

    <?= $form->field($model, 'birthdate_company')->widget(DateControl::classname(), [
        'type'=>DateControl::FORMAT_DATE,
        'ajaxConversion'=>false,
        'options' => [
            'pluginOptions' => [
                'autoclose' => true
            ]
        ]
    ]);?>
        
    <?= app\components\WidgetMultipleValuesn::widget([
        'title' => 'Контактная информация',
        'relation' => $model->contacts,
        'attrs' => [
            'contact_field' => ['type' => 'dropdown', 'items' => app\modules\crm\models\CrmContactsInfo::PossibleValues()],
            'contact_value' => ['type' => 'text'],
            'contact_comment' => ['type' => 'text'],
        ],
        'model' => new app\modules\crm\models\CrmContactsInfo(),
        'addJs' => "
            $('.val1class').click(function(){
            let mask = '+7 999-999-99-99';
                if($(this).val() == 'mobilePhone') {
                    $('#crmcontactsinfo-contact_value-' + $(this).parent().parent().attr('data-mid')).mask(mask);
                } else {
                    $('#crmcontactsinfo-contact_value-' + $(this).parent().parent().attr('data-mid')).unmask(mask);
                }
            });

           $(document).ready(function(){
               $('.val1class').each(function() {
                   $(this).click();
               });
           });"
    ]) ?>
    
    <?= $form->field($model, 'stock', ['labelOptions' => ['data-toggle' => "tooltip", 'data-placement' => "top", 'title' => 'Позволяет поместить объект в хранилище, где будет находиться до момента восстановления.']])->checkbox([], false); ?>
    
    <?= $form->field($model, 'stockContact')->checkbox([], false); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>