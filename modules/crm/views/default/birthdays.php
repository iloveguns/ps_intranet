<?php 
$this->title = 'Дни рождения клиентов';
?>

<table class="table table-responsive table-striped">
    <tr>
        <th>
            <?php if($hasDatum) : ?>
                <?= Yii::$app->formatter->asDate('2017-' . $hasDatum) ?> день рождения у клиентов:
            <?php else : ?>
                Сегодня день рождения у клиентов:
            <?php endif ?>
        </th>
    </tr>
    <?php foreach ($companies as $model) : ?>
        <tr>
            <td><?= app\modules\crm\models\CrmCompany::linkTo($model['name_company'], $model['pk_company'], ['target' => '_blank']) ?></td>
        </tr>
    <?php endforeach ?>
        
    <?php foreach ($contacts as $model) : ?>
        <tr>
            <td><?= app\modules\crm\models\CrmContact::linkTo($model['name_contact'], $model['pk_contact'], ['target' => '_blank']) ?></td>
        </tr>
    <?php endforeach ?>
</table>