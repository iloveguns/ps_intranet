<?php 
use kartik\datecontrol\DateControl;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\Pjax;
use app\modules\crm\CrmModule;

$this->title = 'Аналитика';

$this->registerJs("
    //ajax запрос к получению инфы, $.when()
    function getDataDealInfo(form, searchtype) {
        form.searchtype = searchtype;
        return $.ajax({
            type: 'post',
            dataType: 'json',
            url: '".\yii\helpers\Url::to(['/crm/crm-analytics/ajax-deal-info'])."',
            data: { model: form }
        });
    }
    
    var errLoadMessage = '" . Yii::t('app', 'Error For Loading Data') . "';//сообщение при ошибке загрузки инфы
    var emptyLoadMessage = '" . Yii::t('app', 'Empty Loaded Data') . "';//пустой ответ
    var loadingMessage = '" . Yii::t('app', 'Info Is Loading') . "';//сообщение при ожидании загрузки
 ", yii\web\View::POS_END);
?>
<div class="row">
    <div class="col-md-12 col-lg-6 row">
        <div class="col-md-12">
            <div class="box box-widget">
                <div class="box-header with-border">
                    <div class="user-block">
                        Полная общая статистика за день по менеджеру
                    </div>
                </div>
                <div class="box-body" id="searchdones">
                    <?php Pjax::begin() ?>
                    <?php 
                    $date = (isset($post['searchdones']['date'])) ? $post['searchdones']['date'] : date('Y-m-d');
                    $user = (isset($post['searchdones']['user'])) ? $post['searchdones']['user'] : Yii::$app->user->id;
                    ?>

                    <?= Html::beginForm(['index'], 'post', ['data-pjax' => '', 'id' => 'search-dones-form']); ?>
                    <div class="row">
                        <div class="col-md-4">
                            <?= Html::label('Выбор даты', 'searchdepdones[date_from]') ?>
                            <?= DateControl::widget([
                                'name'=>'searchdones[date]',
                                'value'=> $date,
                                'type'=>DateControl::FORMAT_DATE,
                            ]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= Html::label('Выбор сотрудника', 'searchdepdones[date_from]') ?>
                            <?= Select2::widget([
                                'name' => 'searchdones[user]',
                                'data' => CrmModule::getAllUsersCrm(),
                                'value' => $user,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Select'),
                                ],
                            ]); ?>
                        </div>
                        <div class="col-md-4">
                            <?= Html::label('&nbsp;') ?><br>
                            <?= Html::submitButton('Обновить', ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                    <?= Html::endForm() ?>

                    <div class="clearfix mb-1em"></div>

                    <?php if($searchdones) : ?>
                    <div class="row btns-shows">
                        <div class="col-md-12">
                            <div class="pull-left">
                                <i class="fa fa-tasks" title="Завершенных задач" data-class="task-done" data-toggle="tooltip" data-placement="top"></i> - <?= $searchdones['counts']['donetask'] ?>
                                <i class="fa fa-commenting-o ml-1em" title="Написанных комментариев" data-class="comment-wrote" data-toggle="tooltip" data-placement="top"></i> - <?= $searchdones['counts']['wrotecomments'] ?>
                                <i class="fa fa-archive ml-1em" title="Помещенных в архив" data-class="tostock" data-toggle="tooltip" data-placement="top"></i> - <?= $searchdones['counts']['countinstock'] ?>
                                <i class="fa fa-reply ml-1em" title="Восстановленных из архива" data-class="outstock" data-toggle="tooltip" data-placement="top"></i> - <?= $searchdones['counts']['countoutstock'] ?>
                                <?php if(CrmModule::canAdminDep()) : //только для руководителя ?>
                                    <i class="fa fa-exclamation ml-1em" title="Полезные действия" data-class="important" data-toggle="tooltip" data-placement="top"></i> - <?= $searchdones['counts']['important'] ?>
                                <?php endif ?>
                            </div>
                            <div class="pull-right">
                                <i class="fa fa-building-o ml-1em" title="Создано компаний" data-class="company-created" data-toggle="tooltip" data-placement="top"></i> - <?= $searchdones['counts']['createdcompanies'] ?>
                                <i class="fa fa-user ml-1em" title="Создано контактов" data-class="contact-created" data-toggle="tooltip" data-placement="top"></i> - <?= $searchdones['counts']['createdcontacts'] ?>
                                <i class="fa fa-tasks" title="Создано задач" data-class="task-created" data-toggle="tooltip" data-placement="top"></i> - <?= $searchdones['counts']['createdtasks'] ?>
                                <i class="fa fa-tasks" title="Создано сделок" data-class="deal-created" data-toggle="tooltip" data-placement="top"></i> - <?= $searchdones['counts']['createddeals'] ?>
                            </div>
                        </div>
                    </div>

                    <?php 
                    $this->registerJs("
                        $('.btns-shows i').click(function(){
                            $('.btns-shows i.active-fontaw').removeClass('active-fontaw');
                            $(this).addClass('active-fontaw');
                            let classClicked = $(this).attr('data-class');
                            if(!classClicked) return;

                            $('#searchdones .box-comment').show();
                            $('#searchdones .box-comment:not(.' + classClicked + ')').hide();
                        });
                    ", yii\web\View::POS_END);
                    ?>

                    <?php endif ?>
                    <div class="box-footer box-comments mt-1em">
                        <?php if($searchdones) : ?>
                            <?php foreach ($searchdones['done'] as $doneArr) : ?>
                                <?php foreach ($doneArr as $done) : ?>
                                    <?php 
                                    $important = false;
                                    if(isset($done['important']) && $done['important']) {//определение полезного действия
                                        $important = true;
                                    } 
                                    ?>
                                    <div class="box-comment <?php if(isset($done['class'])) echo $done['class'] ?><?php if($important) echo ' important' ?>">
                                        <div class="comment-text ml-0">
                                            <div class="pull-right"><?= Yii::$app->formatter->asTime($done['time']) ?></div>
                                            <?= $done['text'] ?>
                                            <?php /*if($important) : ?> <i class="fa fa-exclamation" aria-hidden="true"></i><?php endif*/ ?>
                                            <div class="pull-right"><?= $done['link'] ?></div>
                                        </div>
                                    </div>
                                <?php endforeach ?>
                            <?php endforeach ?>
                        <?php endif ?>
                    </div>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
        
        <?php if(CrmModule::canAdminDep()) : //только для руководителя ?>
        <div class="col-md-12">
            <div class="box box-widget">
                <div class="box-header with-border">
                    <div class="user-block">
                        Общая статистика по сделкам по отделу
                    </div>
                </div>
                <div class="box-body">
                    <?php Pjax::begin();
                    $datefrom = (isset($post['searchdepdeals']['date_from'])) ? $post['searchdepdeals']['date_from'] : date('Y-m-d');
                    $dateto = (isset($post['searchdepdeals']['date_to'])) ? $post['searchdepdeals']['date_to'] : date('Y-m-d');
                    $method = (isset($post['searchdepdeals']['method'])) ? $post['searchdepdeals']['method'] : 1;
                    $user = (isset($post['searchdepdeals']['user'])) ? $post['searchdepdeals']['user'] : '';
                    $project = (isset($post['searchdepdeals']['project'])) ? $post['searchdepdeals']['project'] : '';
                    ?>
                    <?= Html::beginForm(['index'], 'post', ['data-pjax' => '', 'id' => 'search-depdeals-form']) ?>
                    <div class="row">
                        <div class="col-md-4">
                            <?= Html::label('Промежуток от', 'searchdepdeals[date_from]') ?>
                            <?= DateControl::widget([
                                'name' => 'searchdepdeals[date_from]',
                                'value'=> $datefrom,
                                'type' => DateControl::FORMAT_DATE,
                            ]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= Html::label('Промежуток до', 'searchdepdeals[date_to]') ?>
                            <?= DateControl::widget([
                                'name' => 'searchdepdeals[date_to]',
                                'value'=> $dateto,
                                'type' => DateControl::FORMAT_DATE,
                            ]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= Html::label('&nbsp;') ?><br>
                            <?= Html::submitButton('Обновить', ['class' => 'btn btn-primary']) ?>
                        </div>
                        <div class="col-md-4">
                            <?= Html::label('Поиск по', 'searchdepdeals[method]') ?>
                            <?= Select2::widget([
                                'name' => 'searchdepdeals[method]',
                                'id' => 'searchdepdeals_method',
                                'data' => \app\modules\crm\models\CrmAnalytics::getSearchDepDealsMethods(),
                                'value' => $method,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Select'),
                                ],
                            ]); ?>
                        </div>
                        <div class="col-md-4 hidden" id="searchdepdeals_project">
                            <?= Html::label('Проект', 'searchdepdeals[project]') ?>
                            <?= Select2::widget([
                                'name' => 'searchdepdeals[project]',
                                'data' => \app\modules\crm\models\CrmDealsProjects::getAll(),
                                'value' => $project,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Select'),
                                ],
                            ]); ?>
                        </div>
                        <div class="col-md-4">
                            <?= Html::label('Сотрудник(опционально)', 'searchdepdeals[date_from]') ?>
                            <?= Select2::widget([
                                'name' => 'searchdepdeals[user]',
                                'data' => CrmModule::getAllUsersCrm(),
                                'value' => $user,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Select'),
                                ],
                            ]); ?>
                        </div>
                    </div>
                    <?= Html::endForm() ?>

                    <div class="clearfix mb-1em"></div>                

                    <?php if($searchdepdeals) : ?>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>Кол-во сделок</th>
                                    <th>Сумма</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 0; foreach ($searchdepdeals['dones'] as $id => $done) : ?>
                                    <tr <?= ($id !== 'total' && $done['count'] > 0) ? 'class="get-data-deal-info cur-point" data-tr="more-data-'.$i.'" data-searchtype="' . $id . '"' : '' //если есть кол-во сделок и не последняя строка(итого) ?>>
                                        <td><?= $done['name'] ?></td>
                                        <td><?= $done['count'] ?></td>
                                        <td><?= Yii::$app->formatter->asCurrency($done['sum']) ?></td>
                                    </tr>
                                    <?php if($id !== 'total') : //в конце не нужно ?>
                                        <tr class="hidden add-data" id="more-data-<?= $i ?>"></tr>
                                    <?php endif ?>
                                <?php ++$i; endforeach ?>
                            </tbody>
                        </table>
                    <?php endif ?>
                    
                    <?php 
                    $this->registerJs("
                        //скрытие/открытие кнопки проектов
                        $('#searchdepdeals_method').change(function(){
                            if($(this).val() == 3) {
                                $('#searchdepdeals_project').removeClass('hidden');
                            } else {
                                $('#searchdepdeals_project').addClass('hidden');
                                $('#searchdepdeals_project select').val('');//очистить выделение
                            }
                        });
                        $('#searchdepdeals_method').change();
                        
                        //нажатие на доп инфу о сделках
                        $('.get-data-deal-info').click(function() {
                            let id = $(this).attr('data-tr');//ид от нажатой строки

                            if($('#' + id).hasClass('hidden')) {//скрыт. выполнить все действия
                                $('#' + id).html('<td colspan=\"3\">' + loadingMessage + '</td>');//показать загрузку
                                    
                                $.when(getDataDealInfo($('#search-depdeals-form').serializeAssoc(), $(this).attr('data-searchtype'))).then(function( data ) { //есть охуенный вариант в widgetComments
                                    let insText = '';
                                    let ita = [];
                                    
                                    if(data.success) {//есть инфа                                        
                                        $.each( data.data, function( key, value ) {
                                            ita.push(value.title);
                                        });
                                        insText = ita.join(', ');

                                        if(!insText) insText = emptyLoadMessage;//вставить инфу
                                    } else {
                                        modelErrorShow(data.errors);
                                        insText = errLoadMessage;//ошибки
                                    }
                                    $('#' + id).html('<td colspan=\"3\">' + insText + '</td>');
                                }, function() {
                                    $('#' + id).html('<td colspan=\"3\">' + errLoadMessage + '</td>');//ошибки
                                });
                            }
                            
                            $('#' + id).toggleClass('hidden');
                        });
                    ", yii\web\View::POS_END);
                    ?>
                    
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
        <?php endif ?>
    </div>
    <div class="col-md-12 col-lg-6 row">
        <?php if(CrmModule::canAdminDep()) : //только для руководителя ?>
        <div class="col-md-12 pull-right">
            <div class="box box-widget">
                <div class="box-header with-border">
                    <div class="user-block">
                        Общая статистика по отделу
                    </div>
                </div>
                <div class="box-body">
                    <?php Pjax::begin() ?>
                    <?php 
                    $datefrom = (isset($post['searchdepdones']['date_from'])) ? $post['searchdepdones']['date_from'] : date('Y-m-d');
                    $dateto = (isset($post['searchdepdones']['date_to'])) ? $post['searchdepdones']['date_to'] : date('Y-m-d');
                    $user = (isset($post['searchdepdones']['user'])) ? $post['searchdepdones']['user'] : '';
                    ?>

                    <?= Html::beginForm(['index'], 'post', ['data-pjax' => '', 'id' => 'search-alldones-form']) ?>
                    <div class="row">
                        <div class="col-md-4">
                            <?= Html::label('Промежуток от', 'searchdepdones[date_from]') ?>
                            <?= DateControl::widget([
                                'name' => 'searchdepdones[date_from]',
                                'value'=> $datefrom,
                                'type' => DateControl::FORMAT_DATE,
                            ]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= Html::label('Промежуток до', 'searchdepdones[date_to]') ?>
                            <?= DateControl::widget([
                                'name' => 'searchdepdones[date_to]',
                                'value'=> $dateto,
                                'type' => DateControl::FORMAT_DATE,
                            ]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= Html::label('&nbsp;') ?><br>
                            <?= Html::submitButton('Обновить', ['class' => 'btn btn-primary']) ?>
                        </div>
                        <div class="col-md-4">
                            <?= Html::label('Выбор сотрудника(опционально)', 'searchdepdones[date_from]') ?>
                            <?= Select2::widget([
                                'name' => 'searchdepdones[user]',
                                'data' => CrmModule::getAllUsersCrm(),
                                'value' => $user,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Select'),
                                ],
                            ]); ?>
                        </div>
                    </div>
                    <?= Html::endForm() ?>

                    <div class="box-footer box-comments mt-1em">
                        <?php if($searchdepdones) : ?>
                            <?php foreach ($searchdepdones['counts'] as $done) : ?>
                                <div class="box-comment <?php if(isset($done['class'])) echo $done['class'] ?>">
                                    <div class="comment-text ml-0">
                                        <div class="pull-left"><?= $done ?></div>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        <?php endif ?>
                    </div>

                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
        <?php endif ?>
    </div>
</div>