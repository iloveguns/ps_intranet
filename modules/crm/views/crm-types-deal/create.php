<?php

use yii\helpers\Html;

$this->title = Yii::t('app/models', 'Create Crm Types Deal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Crm Types Deal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-types-deal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>