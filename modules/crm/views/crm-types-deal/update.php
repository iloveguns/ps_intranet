<?php

use yii\helpers\Html;

$this->title = Yii::t('app/models', 'Update Crm Types Deal') . ' : ' . $model->pk_types_deal;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Crm Types Deal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pk_types_deal, 'url' => ['view', 'id' => $model->pk_types_deal]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="crm-types-deal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>