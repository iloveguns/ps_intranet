<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="crm-types-deal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_types_deal')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>