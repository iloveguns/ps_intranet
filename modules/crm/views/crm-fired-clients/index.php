<?php

use yii\helpers\Html;

$this->title = Yii::t('app/models', 'Crm Clients of Fired');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-roles-index">

    <h1>Уволенные сотрудники, у которых имеются клиенты</h1>

    <table class="table table-striped">
        <tr>
            <th>ФИО сотрудника</th>
            <th>Ссылки</th>
            <th>Действия</th>
        </tr>
    <?php foreach ($users as $user) : ?>
        <tr>
            <td>
                <?= $user['user']->linkToProfile ?> (<?= $user['countCompanies'] + $user['countContacts'] ?>) клиентов
            </td>
            <td>
                <?= Html::a('Ссылка на компании', ['/crm/crm-company/index', 'CrmCompanySearch[responsible_user]' => $user['user']->id_user, 'CrmCompanySearch[stock]' => ''], ['target' => '_blank']) ?> (<?= $user['countCompanies'] ?>)
                <br><?= Html::a('Ссылка на контакты', ['/crm/crm-contact/index', 'CrmContactSearch[responsible_user]' => $user['user']->id_user, 'CrmContactSearch[stock]' => ''], ['target' => '_blank']) ?> (<?= $user['countContacts'] ?>)
            </td>
            <td>
                <?= Html::a('Поместить всех себе в архив', ['to-archive', 'id' => $user['user']->id_user], ['class' => 'btn btn-primary long-load']) ?>
            </td>
        </tr>
    <?php endforeach ?>
    </table>
</div>