<?php

namespace app\modules\crm\models;

use Yii;
use app\modules\crm\models\CrmCompany;
use app\modules\user\models\User;
use app\modules\crm\CrmModule;

class CrmContact extends \app\modules\crm\components\CrmModelDep
{
    const fk = 'fk_crmcontact';
    public $fk = self::fk;
    public $pk = 'pk_contact';
    public $tags;//теги
    public $contactsPhones;//телефоны контакт информация
    public $contactsEmails;//емайлы контакт информация
    
    public function __construct() {
        if(isset(Yii::$app->user) && !Yii::$app->user->identity->isAdmin()) $this->responsible_user = Yii::$app->user->id;//сразу выбран сотрудник
    }
    
    public static function tableName()
    {
        return 'crm_contact';
    }

    public function rules()
    {
        return [
            [['name_contact', 'responsible_user'], 'required'],
            [['birthdate_contact', 'create_date', 'update_date', 'stock', 'tags', 'stock_date', 'is_deleted'], 'safe'],
            [['fk_crm_company', 'fk_department', 'updated_user'], 'integer'],
            [['name_contact', 'post_contact'], 'string', 'max' => 128],
            [['comment_contact'], 'string'],
            [['responsible_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['responsible_user' => 'id_user']],
            [['created_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_user' => 'id_user']],
            [['fk_crm_company'], 'exist', 'skipOnError' => true, 'targetClass' => CrmCompany::className(), 'targetAttribute' => ['fk_crm_company' => 'pk_company']],
            [['fk_department'], 'exist', 'skipOnError' => true, 'targetClass' => \app\modules\user\models\Department::className(), 'targetAttribute' => ['fk_department' => 'pk_department']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_contact' => Yii::t('app/models', 'Pk crm Contact'),
            'name_contact' => Yii::t('app/models', 'Name crm Contact'),
            'post_contact' => Yii::t('app/models', 'Post crm Contact'),
            'birthdate_contact' => Yii::t('app/models', 'Birthdate crm Contact'),
            'fk_crm_company' => Yii::t('app/models', 'Fk Crm Company'),
            'created_user' => Yii::t('app/models', 'Created crm User'),
            'comment_contact' => Yii::t('app/models', 'Comment crm User'),
            'responsible_user' => Yii::t('app/models', 'Responsible crm User'),
            'stock' => Yii::t('app/models', 'Crm Stock'),
            'tags' => Yii::t('app/models', 'Crm Tags'),
            'contactsPhones' => Yii::t('app/models', 'Crm Phones Object'),
        ];
    }

    /**
     * получить компанию, к которой принадлежит
     */
    public function getFkCrmCompany() {
        return $this->hasOne(CrmCompany::className(), ['pk_company' => 'fk_crm_company']);
    }
    
    public static function linkTo($name, $pk, $options = []) {
        return \yii\helpers\Html::a($name, ['/crm/crm-contact/view', 'id' => $pk], $options);
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord) {
                $this->created_user = Yii::$app->user->id;
            }
            
            //некоторые параметры для сравнения изменений
            $old_attrs = (new \yii\db\Query())
                ->select(['responsible_user', 'stock'])
                ->from($this->tableName())
                ->where(['pk_contact' => $this->getPrimaryKey()])
                ->one();
            
            //изменение архива
            if($this->stock != $old_attrs['stock']) {
                \app\modules\crm\helpers\CrmHelper::changeArchive([$this->getPrimaryKey()], $this, $this->stock);//не очень корректно
            }
            
            //проверка на смену ответственного
            if($old_attrs['responsible_user'] !== $this->responsible_user){
                foreach ($this->workTasks as $wtask) {//активные задачи отдать
                    if($wtask->fk_user_task == $old_attrs['responsible_user']) {//только свои менять
                        $wtask->fk_user_task = $this->responsible_user;//пересохранить задачи
                        $wtask->update();
                    }
                }
            }
            //end проверка на смену ответственного
            
            return true;
        }
        return false;
    }
}