<?php

namespace app\modules\crm\models;

use Yii;
use app\modules\crm\models\CrmTasks;

class CrmTypetask extends \app\modules\crm\components\CrmModelAll
{
    public static function tableName()
    {
        return 'crm_typetask';
    }

    public function rules()
    {
        return [
            [['name_typetask'], 'required'],
            [['fk_department', 'important'], 'integer'],
            [['name_typetask'], 'string', 'max' => 128],
            [['fk_department'], 'exist', 'skipOnError' => true, 'targetClass' => \app\modules\user\models\Department::className(), 'targetAttribute' => ['fk_department' => 'pk_department']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_typetask' => Yii::t('app/models', 'Pk Typetask'),
            'important' => Yii::t('app/models', 'Учитывать как полезное действие'),
            'name_typetask' => Yii::t('app/models', 'Name Typetask'),
        ];
    }
    
    public function getCrmTasks()
    {
        return $this->hasMany(CrmTasks::className(), ['fk_typetask' => 'pk_typetask']);
    }
    
    /*
     * все типы задач
     * можно в базе и не хранить
     */
    public function getAll($id = NULL) {
        $b = CrmTypetask::getDb()->cache(function ($db) {
            $all = CrmTypetask::findWDep()->asArray()->all();
            $b = [];
            foreach ($all as $tt) {
                $b[$tt['pk_typetask']] = $tt['name_typetask'];
            }
            return $b;
        });
        
        if($id && isset($b[$id])) {
            return $b[$id];
        } else if($id) {
            //проблема с просмотрам в разных отделах
            $dep = CrmTypetask::find()->select('fk_department')->asArray()->where(['pk_typetask' => $id])->one();
            //установить отдел
            \app\modules\crm\CrmModule::setCookieDep($dep['fk_department']);
        } else {
            return $b;
        }
        
        /*if($id) return $b[$id];
        return $b;*/
    }
    
    /*
     * найти все типы задач, важные только
     */
    public function getAllImportant() {
        $b = CrmTypetask::getDb()->cache(function ($db) {
            return self::findWDep()->asArray()->where(['important' => 1])->all();
        });
        
        return $b;
    }
}