<?php

namespace app\modules\crm\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\crm\models\CrmCompany;

class CrmCompanySearch extends CrmCompany {    
    public function rules() {
        return [
            [['pk_company', 'fk_address_country', 'fk_address_city', 'fk_address_street', 'fk_department', 'responsible_user', 'stock', 'created_user'], 'integer'],
            [['name_company', 'contactsPhones', 'contactsEmails'], 'string'],
            [['address_other', 'birthdate_company', 'tags'], 'safe'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = CrmCompany::findWDep()->with(['contacts', 'tagsModel']);
        
        $pageSize = isset($_GET['per-page']) ? intval($_GET['per-page']) : Yii::$app->params['default_Pagination_PageSize'];

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['name_company'=>SORT_ASC]],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        $this->load($params);
                
        //собрать конструкцию ['or', ['like', '', ''], ...]
        if($this->contactsPhones) {//поиск по телефонам
            $query->joinWith('contacts');
            $arrcss = [];
            $arrcss[0] = 'OR';
            $contacts_info = explode(',', $this->contactsPhones);
            foreach ($contacts_info as $contact_info) {
                if(strpos($contact_info, '-') !== FALSE || mb_substr($contact_info, 0, 1) == '+') {//если указан '-' или первый символ '+', то искать в исходном номере
                    $arrcss[] = ['like', 'contact_value', trim($contact_info)];
                } else {
                    $arrcss[] = ['like', 'contact_value_search', trim($contact_info)];
                }
            }
            
            $query->andWhere($arrcss);
        }
        
        if($this->contactsEmails) {//поиск по email
            $query->joinWith('contacts');
            $query->andWhere(['and', ['like', 'contact_field', 'email'], ['like', 'contact_value', $this->contactsEmails]]);
        }
        
        if($this->tags) {//поиск по тегу
            $query->joinWith('tagsModel')->andWhere(['pk_tag' => $this->tags]);
        }
        
        if($this->stock === NULL) {
            $this->stock = \app\modules\crm\CrmModule::$storeOut;
        }

        if (!$this->validate()) {;
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'stock' => $this->stock,
            'responsible_user' => $this->responsible_user,
            'created_user' => $this->created_user,
            'pk_company' => $this->pk_company,
            'fk_address_country' => $this->fk_address_country,
            'fk_address_city' => $this->fk_address_city,
            'fk_address_street' => $this->fk_address_street,
            'birthdate_company' => $this->birthdate_company,
        ]);

        if($this->name_company) {//попытка поиска в разных раскладках
            $multiLangNameSearch = \app\models\FunctionModel::tryChangeCharsetWord($this->name_company);
            $query = $this->prepareMultiLangSearchFilter($query, 'name_company', $multiLangNameSearch);
        }
        
        $query->andWhere(['is_deleted' => 0]); // временно удаленные не выводить
        
        $query->andFilterWhere(['like', 'address_other', $this->address_other]);
        
        return $dataProvider;
    }
}