<?php

namespace app\modules\crm\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class CrmAnalytics extends \yii\base\Model {
    static $initParamsLink = ['data-pjax' => ''];
    
    /*
     * найти все сделанное за день сотрудником
     * @param $params {array} - параметры поиска
     */
    public function getSearchDones($params = NULL) {
        if(!$params['date']) $params['date'] = date('Y-m-d');
        if(!isset($params['user'])) $params['user'] = Yii::$app->user->id;
        $ra = [];//возвращаемый массив
        $countt = $countc = $countct = $importantDones = 0;
        
        $importantTypes = CrmTypetask::getAllImportant();//полезные действия
        $importantTypes = ArrayHelper::map($importantTypes, 'pk_typetask', 'pk_typetask');
        
        //поиск созданных задач
        $tasksс = CrmTasks::findWDep()
            ->andWhere(['created_user' => $params['user']])
            ->andWhere(['like', 'create_date', $params['date']])
            ->andWhere(['not in', 'status_task', [CrmTasks::STATUS_COMMENT]])
            ->orderBy('create_date DESC')
            ->all();
        
        foreach ($tasksс as $model) {
            $time = $text = $link = '';
            $link = $model->title_task;
            $time = $model->create_date;
            $text = '<strong>'.Yii::t('app/models', 'Crm Task Created').'</strong>' . $model->text_task;
            if($model->fk_typetask) $text .= '<br>' . Yii::t('app/models', 'Fk crm Typetask') . ' : ' . \app\modules\crm\models\CrmTypetask::getAll($model->fk_typetask);//совместимость со старым говном
            
            $ra[$time][] = ['text' => $text, 'time' => $time, 'link' => $link, 'class' => 'task-created'];
        }
        $countct = count($tasksс);
        //end поиск созданных задач
        
        //поиск завершенных задач
        $tasks = CrmTasks::findWDep()
            ->andWhere(['fk_user_task' => $params['user']])
            ->andWhere(['like', 'update_date', $params['date']])
            ->andWhere(['in', 'status_task', [CrmTasks::STATUS_DONE]])
            ->orderBy('create_date DESC, update_date DESC')//как и в поиске задач в клиенте
            ->all();
        
        foreach ($tasks as $model) {
            $important = false;//полезность выполненной задачи
            if(in_array($model->fk_typetask, $importantTypes)) {
                ++$importantDones;
                $important = true;
            }
            $time = $text = $link = $class = '';
            $class = 'task-done';
            $link = $model->title_task;
            $time = $model->update_date;
            $text = '<strong>'.Yii::t('app/models', 'Crm Task Done').'</strong>' . $model->text_task;
            if($model->fk_typetask) $text .= '<br>' . Yii::t('app/models', 'Fk crm Typetask') . ' : ' . \app\modules\crm\models\CrmTypetask::getAll($model->fk_typetask);//совместимость со старым говном
            if($model->fk_task_result) $text .= '<br>' . $model->getAttributeLabel('fk_task_result') . ' : ' . \app\modules\crm\models\CrmTasksResults::getOneString($model->fk_task_result);
            //добавление комментария выполнения
            if($model->status_text) $text .= '<br>' . $model->getAttributeLabel('status_text') . ' : ' . $model->status_text;
            ++$countt;
            
            $ra[$time][] = ['text' => $text, 'time' => $time, 'link' => $link, 'class' => $class, 'important' => $important];
        }
        unset($tasks);
        //end поиск завершенных задач
        
        //поиск написанных комментов
        $tasks = CrmTasks::findWDep()
            ->andWhere(['fk_user_task' => $params['user']])
            ->andWhere(['like', 'create_date', $params['date']])
            ->andWhere(['in', 'status_task', [CrmTasks::STATUS_COMMENT]])
            ->orderBy('create_date DESC, update_date DESC')//как и в поиске задач в клиенте
            ->all();
        
        foreach ($tasks as $model) {
            $time = $text = $link = $class = '';
            $class = 'comment-wrote';
            $link = $model->title_task;
            $time = $model->create_date;
            $text = '<strong>'.Yii::t('app/models', 'Crm Wroten Comment').'</strong>' . mb_substr(strip_tags($model->status_text), 0, 170);
            ++$countc;
            
            $ra[$time][] = ['text' => $text, 'time' => $time, 'link' => $link, 'class' => $class];
        }
        //end поиск написанных комментов
        
        //поиск сделок
        $deals = CrmDeals::findWDep()
            ->select('name_deal, pk_deal, create_date')
            ->andWhere(['created_user' => $params['user']])
            ->andWhere(['like', 'create_date', $params['date']])
            ->orderBy('create_date DESC')
            ->asArray()
            ->all();
        
        foreach ($deals as $deal) {
            $text = '<strong>'.Yii::t('app/models', 'Crm Deals Created').'</strong>' . Html::a($deal['name_deal'], ['/crm/crm-deals/view', 'id' => $deal['pk_deal']], self::$initParamsLink);
            
            $ra[$deal['create_date']][] = ['text' => $text, 'time' => $deal['create_date'], 'link' => '', 'class' => 'deal-created'];
        }
        $countcdeals = count($deals);
        //end поиск сделок
        
        //поиск созданных контактов
        $contacts = CrmContact::findWDep()
            ->select('name_contact, pk_contact, create_date')
            ->andWhere(['created_user' => $params['user']])
            ->andWhere(['like', 'create_date', $params['date']])
            ->asArray()
            ->all();
        
        foreach ($contacts as $contact) {
            $text = '<strong>'.Yii::t('app/models', 'Crm Contact Created').'</strong>' . Html::a($contact['name_contact'], ['/crm/crm-contact/view', 'id' => $contact['pk_contact']], self::$initParamsLink);
            
            $ra[$contact['create_date']][] = ['text' => $text, 'time' => $contact['create_date'], 'link' => '', 'class' => 'contact-created'];
        }
        $countcont = count($contacts);
        unset($contacts);        
        //end поиск созданных контактов
        
        //поиск созданных компаний
        $companies = CrmCompany::findWDep()
            ->select('name_company, pk_company, create_date')
            ->andWhere(['created_user' => $params['user']])
            ->andWhere(['like', 'create_date', $params['date']])
            ->asArray()
            ->all();
        
        foreach ($companies as $company) {
            $text = '<strong>'.Yii::t('app/models', 'Crm Company Created').'</strong> ' . Html::a($company['name_company'], ['/crm/crm-company/view', 'id' => $company['pk_company']], self::$initParamsLink);
            
            $ra[$company['create_date']][] = ['text' => $text, 'time' => $company['create_date'], 'link' => '', 'class' => 'company-created'];
        }
        $countcomp = count($companies);
        unset($companies);
        //end поиск созданных компаний
        
        //помещение в архив
        $contacts = CrmContact::findWDep()
            ->select('name_contact, pk_contact, stock_date')
            ->andWhere(['updated_user' => $params['user']])
            ->andWhere(['like', 'stock_date', $params['date']])
            ->andWhere(['stock' => '1'])
            ->asArray()
            ->all();
        
        foreach ($contacts as $contact) {
            $text = '<strong>'.Yii::t('app/models', 'Crm Contact In Stock').'</strong> ' . Html::a($contact['name_contact'], ['/crm/crm-contact/view', 'id' => $contact['pk_contact']], self::$initParamsLink);
            
            $ra[$contact['stock_date']][] = ['text' => $text, 'time' => $contact['stock_date'], 'link' => '', 'class' => 'tostock'];
        }
        $countcontinstock = count($contacts);
        unset($contacts);
        
        $companies = CrmCompany::findWDep()
            ->select('name_company, pk_company, stock_date')
            ->andWhere(['updated_user' => $params['user']])
            ->andWhere(['like', 'stock_date', $params['date']])
            ->andWhere(['stock' => '1'])
            ->asArray()
            ->all();
        
        foreach ($companies as $company) {
            $text = '<strong>'.Yii::t('app/models', 'Crm Company In Stock').'</strong> ' . Html::a($company['name_company'], ['/crm/crm-company/view', 'id' => $company['pk_company']], self::$initParamsLink);
            
            $ra[$company['stock_date']][] = ['text' => $text, 'time' => $company['stock_date'], 'link' => '', 'class' => 'tostock'];
        }
        $countcompinstock = count($companies);
        unset($companies);
        //end помещение в архив
        
        //восстановление из архива
        $contacts = CrmContact::findWDep()
            ->select('name_contact, pk_contact, stock_date')
            ->andWhere(['updated_user' => $params['user']])
            ->andWhere(['like', 'stock_date', $params['date']])
            ->andWhere(['stock' => '0'])
            ->asArray()
            ->all();
        
        foreach ($contacts as $contact) {
            $text = '<strong>'.Yii::t('app/models', 'Crm Contact Out Stock').'</strong>' . Html::a($contact['name_contact'], ['/crm/crm-contact/view', 'id' => $contact['pk_contact']], self::$initParamsLink);
            
            $ra[$contact['stock_date']][] = ['text' => $text, 'time' => $contact['stock_date'], 'link' => '', 'class' => 'outstock'];
        }
        $countcontoutstock = count($contacts);
        unset($contacts);
        
        $companies = CrmCompany::findWDep()
            ->select('name_company, pk_company, stock_date')
            ->andWhere(['updated_user' => $params['user']])
            ->andWhere(['like', 'stock_date', $params['date']])
            ->andWhere(['stock' => '0'])
            ->asArray()
            ->all();
        
        foreach ($companies as $company) {
            $text = '<strong>'.Yii::t('app/models', 'Crm Company Out Stock').'</strong> ' . Html::a($company['name_company'], ['/crm/crm-company/view', 'id' => $company['pk_company']], self::$initParamsLink);
            
            $ra[$company['stock_date']][] = ['text' => $text, 'time' => $company['stock_date'], 'link' => '', 'class' => 'outstock'];
        }
        $countcompoutstock = count($companies);
        unset($companies);
        //end восстановление из архива
        
        krsort($ra);//сортировка для хронологии
        
        $dones['done'] = $ra;
        $dones['counts'] = [
            'donetask' => $countt, 'wrotecomments' => $countc, 
            'createdcompanies' => $countcomp, 'createdcontacts' => $countcont, 
            'createdtasks' => $countct, 'countinstock' => $countcompinstock + $countcontinstock,
            'countoutstock' => $countcompoutstock + $countcontoutstock,
            'createddeals' => $countcdeals, 'important' => $importantDones
        ];
         
        return $dones;
    }
    
    /*
     * найти все сделанное за промежуток времени отделом
     * @param $params {array} - параметры поиска
     */
    public function getSearchDepDones($params = NULL) {
        if(!$params['date_from']) $params['date_from'] = date('Y-m-d');
        if(!$params['date_to']) $params['date_to'] = date('Y-m-d');
        $user = ($params['user']) ? $params['user'] : NULL;
        $datefrom = $params['date_from'] . ' 00:00:00';
        $dateto = $params['date_to'] . ' 23:59:59';
        
        //поиск кол-ва созданных задач
        $countCreatedTasks = CrmTasks::findWDep();
        if($user) $countCreatedTasks->andWhere(['created_user' => $user]);
        $countCreatedTasks = $countCreatedTasks->andWhere(['>=', 'create_date', $datefrom])
            ->andWhere(['<=', 'create_date', $dateto])
            ->andWhere(['not in', 'status_task', [CrmTasks::STATUS_COMMENT, CrmTasks::STATUS_SYSCOMMENT]])
            ->orderBy('create_date DESC')
            ->count('pk_task');
        
        $createdtasks = '<strong>'.Yii::t('app/analytics', 'Crm count created tasks').':</strong> ' . 
            Html::a($countCreatedTasks, ['/crm/crm-tasks/todolist',
                'CrmTasksSearch[create_date]' => $datefrom . '|' . $dateto,
                'CrmTasksSearch[status_task]' => 'nocomments',
                'CrmTasksSearch[stock]' => 1,
                'CrmTasksSearch[fk_user_task]' => ($user) ? $user : -1
            ]);
        //end поиск кол-ва созданных задач
        
        //поиск завершенных задач
        $countDoneTasks = CrmTasks::findWDep();
        if($user) $countDoneTasks->andWhere(['created_user' => $user]);
        $countDoneTasks = $countDoneTasks->andWhere(['>=', 'update_date', $datefrom])
            ->andWhere(['<=', 'update_date', $dateto])
            ->andWhere(['in', 'status_task', [CrmTasks::STATUS_DONE]])
            ->orderBy('create_date DESC, update_date DESC')//как и в поиске задач в клиенте
            ->count('pk_task');
        
        $donetask = '<strong>'.Yii::t('app/analytics', 'Crm count done tasks').':</strong> ' . 
            Html::a($countDoneTasks, ['/crm/crm-tasks/todolist',
                'CrmTasksSearch[update_date]' => $datefrom . '|' . $dateto,
                'CrmTasksSearch[status_task]' => CrmTasks::STATUS_DONE,
                'CrmTasksSearch[stock]' => 1,
                'CrmTasksSearch[fk_user_task]' => ($user) ? $user : -1
            ]);
        //end поиск завершенных задач
        
        //подсчет компаний и контактов, в которых есть завершенные задачи
        $countCompanyDoneTasks = CrmTasks::findWDep();
        if($user) $countCompanyDoneTasks->andWhere(['created_user' => $user]);
        $countCompanyDoneTasks = $countCompanyDoneTasks
            ->distinct()
            ->asArray()
            ->andWhere(['not', ['fk_crmcompany' => NULL]])
            ->andWhere(['>=', 'update_date', $datefrom])
            ->andWhere(['<=', 'update_date', $dateto])
            ->andWhere(['in', 'status_task', [CrmTasks::STATUS_DONE]])
            ->orderBy('create_date DESC, update_date DESC')//как и в поиске задач в клиенте
            ->count();
        
        $companyDoneTasks = '<strong>'.Yii::t('app/analytics', 'Crm count company done tasks').':</strong> ' . 
            Html::a($countCompanyDoneTasks, ['/crm/crm-tasks/todolist',
                'CrmTasksSearch[update_date]' => $datefrom . '|' . $dateto,
                'CrmTasksSearch[status_task]' => CrmTasks::STATUS_DONE,
                'CrmTasksSearch[stock]' => 1,
                'CrmTasksSearch[searchbycompanies]' => 1,
                'CrmTasksSearch[fk_user_task]' => ($user) ? $user : -1
            ]);
        
        $countContactDoneTasks = CrmTasks::findWDep();
        if($user) $countContactDoneTasks->andWhere(['created_user' => $user]);
        $countContactDoneTasks = $countContactDoneTasks
            ->distinct()
            ->asArray()
            ->andWhere(['not', ['fk_crmcontact' => NULL]])
            ->andWhere(['>=', 'update_date', $datefrom])
            ->andWhere(['<=', 'update_date', $dateto])
            ->andWhere(['in', 'status_task', [CrmTasks::STATUS_DONE]])
            ->count();
        
        $contactDoneTasks = '<strong>'.Yii::t('app/analytics', 'Crm count contact done tasks').':</strong> ' .
            Html::a($countContactDoneTasks, ['/crm/crm-tasks/todolist',
                'CrmTasksSearch[update_date]' => $datefrom . '|' . $dateto,
                'CrmTasksSearch[status_task]' => CrmTasks::STATUS_DONE,
                'CrmTasksSearch[stock]' => 1,
                'CrmTasksSearch[searchbycontacts]' => 1,
                'CrmTasksSearch[fk_user_task]' => ($user) ? $user : -1
            ]);
        //подсчет компаний и контактов
        
        //поиск созданных контактов
        $countCreatedContacts = CrmContact::findWDep();
        if($user) $countCreatedContacts->andWhere(['created_user' => $user]);
        $countCreatedContacts = $countCreatedContacts->andWhere(['>=', 'create_date', $datefrom])
            ->andWhere(['<=', 'create_date', $dateto])
            ->count();
        
        $createdContacts = '<strong>'.Yii::t('app/analytics', 'Crm count created contacts').':</strong> ' . $countCreatedContacts;
        //end поиск созданных контактов
        
        //поиск созданных компаний
        $countCreatedCompanies = CrmCompany::findWDep();
        if($user) $countCreatedCompanies->andWhere(['created_user' => $user]);
        $countCreatedCompanies = $countCreatedCompanies->andWhere(['>=', 'create_date', $datefrom])
            ->andWhere(['<=', 'create_date', $dateto])
            ->count();
        
        $createdCompanies = '<strong>'.Yii::t('app/analytics', 'Crm count created companies').':</strong> ' . $countCreatedCompanies;
        //end поиск созданных компаний
        
        //помещение в архив
        $countContactsToArchive = CrmContact::findWDep();
        if($user) $countContactsToArchive->andWhere(['updated_user' => $user]);
        $countContactsToArchive = $countContactsToArchive->andWhere(['>=', 'stock_date', $datefrom])
            ->andWhere(['<=', 'stock_date', $dateto])
            ->andWhere(['stock' => '1'])
            ->asArray()
            ->count();
        
        $contactsToArchive = '<strong>'.Yii::t('app/analytics', 'Crm count contacts to archive').':</strong> ' . $countContactsToArchive;
        
        $countCompaniesToArchive = CrmCompany::findWDep();
        if($user) $countCompaniesToArchive->andWhere(['updated_user' => $user]);
        $countCompaniesToArchive = $countCompaniesToArchive->andWhere(['>=', 'stock_date', $datefrom])
            ->andWhere(['<=', 'stock_date', $dateto])
            ->andWhere(['stock' => '1'])
            ->asArray()
            ->count();
        
        $companiesToArchive = '<strong>'.Yii::t('app/analytics', 'Crm count companies to archive').':</strong> ' . $countCompaniesToArchive;
        //end помещение в архив
        
        //восстановление из архива
        $countContactsFromArchive = CrmContact::findWDep();
        if($user) $countContactsFromArchive->andWhere(['updated_user' => $user]);
        $countContactsFromArchive = $countContactsFromArchive->andWhere(['>=', 'stock_date', $datefrom])
            ->andWhere(['<=', 'stock_date', $dateto])
            ->andWhere(['stock' => '0'])
            ->asArray()
            ->count();
        
        $contactsFromArchive = '<strong>'.Yii::t('app/analytics', 'Crm count contacts from archive').':</strong> ' . $countContactsFromArchive;
        
        $countCompaniesFromArchive = CrmCompany::findWDep();
        if($user) $countCompaniesFromArchive->andWhere(['updated_user' => $user]);
        $countCompaniesFromArchive = $countCompaniesFromArchive->andWhere(['>=', 'stock_date', $datefrom])
            ->andWhere(['<=', 'stock_date', $dateto])
            ->andWhere(['stock' => '0'])
            ->asArray()
            ->count();
        
        $companiesFromArchive = '<strong>'.Yii::t('app/analytics', 'Crm count companies from archive').':</strong> ' . $countCompaniesFromArchive;
        //end восстановление из архива
        
        // написанных комментариев (не системных)
        $countWroteComments = CrmTasks::findWDep();
        if($user) $countWroteComments->andWhere(['created_user' => $user]);
        $countWroteComments = $countWroteComments
            ->asArray()
            ->andWhere(['>=', 'create_date', $datefrom])
            ->andWhere(['<=', 'create_date', $dateto])
            ->andWhere(['in', 'status_task', [CrmTasks::STATUS_COMMENT]])
            ->count();
        
        $cwc = $countWroteComments;
        $countWroteComments = '<strong>'.Yii::t('app/analytics', 'Crm count written comments').':</strong> ' . $countWroteComments;
        // end написанных комментариев
        
        //специально для автографа
        $countAllActions = '<i>'.Yii::t('app/analytics', 'Crm count all actions').':</i> ' . ($cwc + $countCompanyDoneTasks + $countContactDoneTasks);
        
        /*
         * для одного запроса надо переписать типа так:
         *  SELECT pk_task, 
            (SELECT count(*) FROM `crm_tasks` WHERE 
            `fk_department` = 1 AND `status_task` = 11 AND `create_date` >= '2017-12-13 00:00:00' AND `create_date` <= '2017-12-13 23:59:59') AS countWrittenComments
            FROM `crm_tasks` LIMIT 1
         */
        
        $dones['counts'] = [
            'createdtasks' => $createdtasks, 'donetask' => $donetask,
            'companyDoneTasks' => $companyDoneTasks, 'contactDoneTasks' => $contactDoneTasks,
            'countWroteComments' => $countWroteComments,
            'createdContacts' => $createdContacts, 'createdCompanies' => $createdCompanies,
            'contactsToArchive' => $contactsToArchive, 'companiesToArchive' => $companiesToArchive,
            'contactsFromArchive' => $contactsFromArchive, 'companiesFromArchive' => $companiesFromArchive,
            'countAllActions' => $countAllActions
        ];
         
        return $dones;
    }
    
    /*
     * варианты поиска для аналитики сделок
     */
    public function getSearchDepDealsMethods() {
        return [1 => 'Продукты', 2 => 'Статусы', 3 => 'Проекты'];//используется getSearchDepDeals + view/index
    }
    
    /*
     * статистика общая по сделкам по отделу
     */
    public function getSearchDepDeals($params = NULL) {
        $types_deal = CrmTypesDeal::getAll();//поиск по продуктам отдела
        
        $type_deals = CrmTypedeals::getAll();//статусы нужны
        
        $deals_project = CrmDealsProjects::getAll();//проекты нужны
        
        if($params['method'] == 1) {//по продуктам
            $f = $types_deal;
            $attr = 'type_deal';
        } else if($params['method'] == 2){//по статусам
            $f = $type_deals;
            $attr = 'status_deal';
        } else if($params['method'] == 3){//по проектам
            $f = $deals_project;
            $attr = 'fk_deals_project';
        } else {//недопустимое значение
            throw new \yii\base\InvalidParamException('Поиск должен быть выбран');
        }
        
        //общий подсчет
        $totalsum = $totalcount = 0;
        //корректная и полная дата
        $datefrom = $params['date_from'] . ' 00:00:00';
        $dateto = $params['date_to'] . ' 23:59:59';
        $user = ($params['user']) ? $params['user'] : NULL;
        $project = ($params['project']) ? $params['project'] : NULL;
        
        if($project) $f = [$project => $f[$project]];//конкретный проект
        
        //собственно поиск
        foreach ($f as $pk => $name) {
            $row = CrmDeals::findWDep()->asArray();
            if($user) $row->andWhere(['created_user' => $user]);
            $row = $row->select(['SUM(price_deal) AS sum, count(price_deal) AS count'])                
                ->andWhere(['>=', 'create_date', $datefrom])
                ->andWhere(['<=', 'create_date', $dateto])
                ->andWhere([$attr => $pk])
                ->one();

            $totalcount += $row['count'];
            $totalsum += $row['sum'];

            $dones['dones'][$pk]['name'] = $name;
            $dones['dones'][$pk]['count'] = $row['count'];            
            $dones['dones'][$pk]['sum'] = $row['sum'];
        }
        
        //общие подсчеты
        $dones['dones']['total']['name'] = 'Всего:';
        $dones['dones']['total']['count'] = $totalcount;
        $dones['dones']['total']['sum'] = $totalsum;
        
        return $dones;
    }
    
    /*
     * доп информация по сделкам для аналитики
     * @param $params['method'] {int} - ид getSearchDepDealsMethods()
     * @param $params['info'] {int}       - ид конкретного метода
     */
    public function additionalDealInfo($params) {
        $out = [];
        
        if($params['method'] == 1) {//по продуктам
            $attr = 'type_deal';
        } else if($params['method'] == 2){//по статусам
            $attr = 'status_deal';
        } else if($params['method'] == 3){//по проектам
            $attr = 'fk_deals_project';
        } else {//недопустимое значение
            throw new \yii\base\InvalidParamException('Поиск должен быть выбран');
        }
        
        $datefrom = $params['date_from'] . ' 00:00:00';
        $dateto = $params['date_to'] . ' 23:59:59';
        $user = ($params['user']) ? $params['user'] : NULL;
        
        //найти по условиям
        $deals = CrmDeals::findWdep()
            ->andWhere(['>=', 'create_date', $datefrom])
            ->andWhere(['<=', 'create_date', $dateto])
            ->andWhere([$attr => $params['searchtype']])
            ->all();
        
        foreach ($deals as $deal) {
            $out[] = ['title' => $deal->title_task, 'pk_deal' => $deal->pk_deal];
        }
        
        
        return $out;
    }
}