<?php

namespace app\modules\crm\models;
use yii\helpers\ArrayHelper;
use Yii;

class AddressCountry extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'address_country';
    }

    public function rules()
    {
        return [
            [['name_country'], 'required'],
            [['name_country'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_country' => Yii::t('app/models', 'Pk Country'),
            'name_country' => Yii::t('app/models', 'Name Country'),
        ];
    }

    public function getAddressCitys()
    {
        return $this->hasMany(AddressCity::className(), ['fk_country' => 'pk_country']);
    }
    
    public function getCrmCompanies()
    {
        return $this->hasMany(CrmCompany::className(), ['fk_address_country' => 'pk_country']);
    }
    
    /*
     * список всех стран
     * $id - получить по ид
     */
    public function getAll($id = null) {
        $all = AddressCountry::find()->asArray()->all();
        $b = ArrayHelper::map($all, 'pk_country', 'name_country');
        if($id !== NULL) return $b[$id];
        return ArrayHelper::merge(['' => \Yii::t('app', 'Not selected')], $b);
        return $b;
    }
}