<?php
namespace app\modules\crm\models;

use Yii;

class CrmRights extends \yii\base\Model
{
    public //1 шаг
        $manager_crm,//црм(базовый менеджер)
        $head_dep_crm//руководитель отдела crm
        ;
    
    public function rules()
    {
        return [
            [['manager_crm', 'head_dep_crm'], 'integer'],//2шаг 
        ];
    }

    public function attributeLabels()
    {
        return [//3 шаг
            'manager_crm' => Yii::t('app/models', 'Role manager crm'),
            'head_dep_crm' => Yii::t('app/models', 'Role head_dep_crm'),
        ];
    }
    
    /*
     * убрать пустые значения перед сохранением в json ролей
     */
    public function deleteEmptysToJson() {
        $a = [];
        foreach ($this->attributes as $name => $val) {
            if($val){//если есть значение
                $a[$name] = $val;
            }
        }
        return json_encode($a);
    }
}