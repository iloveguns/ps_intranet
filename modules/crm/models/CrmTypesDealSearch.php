<?php

namespace app\modules\crm\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\crm\models\CrmTypesDeal;

class CrmTypesDealSearch extends CrmTypesDeal{
    public function rules()
    {
        return [
            [['pk_types_deal', 'fk_department'], 'integer'],
            [['name_types_deal'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = CrmTypesDeal::findWDep();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'pk_types_deal' => $this->pk_types_deal,
            'fk_department' => $this->fk_department,
        ]);

        $query->andFilterWhere(['like', 'name_types_deal', $this->name_types_deal]);

        return $dataProvider;
    }
}