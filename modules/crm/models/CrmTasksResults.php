<?php

namespace app\modules\crm\models;

use Yii;
use app\modules\user\models\Department;

/*
 * Класс для результатов выполнения задач црм
 */
class CrmTasksResults extends \app\modules\crm\components\CrmModelAll
{
    const DENIED = 1;
    const ACCEPTED = 0;
    
    public static function tableName()
    {
        return 'crm_tasks_results';
    }

    public function rules()
    {
        return [
            [['name_task_result'], 'required'],
            [['name_task_result'], 'string'],
            [['fk_department', 'denial'], 'integer'],
            [['fk_department'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['fk_department' => 'pk_department']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_task_result' => Yii::t('app/models', 'Pk Task Result'),
            'name_task_result' => Yii::t('app/models', 'Name Task Result'),
            'fk_department' => Yii::t('app/models', 'Fk Department'),
            'denial' => Yii::t('app/models', 'denial task result')
        ];
    }

    public function getFkDepartment()
    {
        return $this->hasOne(Department::className(), ['pk_department' => 'fk_department']);
    }
    
    //просто вернуть строковое значение
    public function getOneString($id = NULL) {
        $a = CrmTasksResults::findWDep()->andWhere(['pk_task_result' => $id])->one();
        if(!$a) return '';
        $ct = ($a['denial'] == 1) ? 'red' : 'green';        
        return '<span class="text-' . $ct . '">' . $a['name_task_result'] . '</span>';
    }


    public function getAll() {
        $all = CrmTasksResults::findWDep()->asArray()->all();
        
        $bad = $good = $b =[];
        foreach ($all as $tt) {
            if($tt['denial'] == 1) {
                $bad[$tt['pk_task_result']] = $tt['name_task_result'];
            } else {
                $good[$tt['pk_task_result']] = $tt['name_task_result'];
            }
        }
        
        $b['good'] = $good;
        $b['bad'] = $bad;
        
        return $b;
    }
    
    public function getDenial($id = NULL) {
        $b = [1 => Yii::t('app', 'Yes'), 0 => Yii::t('app', 'No')];
        
        if($id !== NULL) return $b[$id];
        return $b;
    }
}