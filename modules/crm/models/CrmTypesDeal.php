<?php

namespace app\modules\crm\models;

use Yii;

/*
 * Класс для типов сделок
 */
class CrmTypesDeal extends \app\modules\crm\components\CrmModelAll
{
    public static function tableName()
    {
        return 'crm_types_deal';
    }

    public function rules()
    {
        return [
            [['name_types_deal'], 'required'],
            [['fk_department'], 'integer'],
            [['name_types_deal'], 'string', 'max' => 80],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_types_deal' => Yii::t('app/models', 'Pk Types Deal'),
            'name_types_deal' => Yii::t('app/models', 'Crm Name Types Deal'),
            'fk_department' => Yii::t('app/models', 'Fk Department'),
        ];
    }
    
    public function getFkDepartment() {
        return $this->hasOne(Department::className(), ['pk_department' => 'fk_department']);
    }
    
    /*
     * все типы сделок 
     * TODO кеш
     */
    public function getAll($id = NULL) {
        $all = self::findWDep()->asArray()->all();

        $b = [];
        foreach ($all as $tt) {
            $b[$tt['pk_types_deal']] = $tt['name_types_deal'];
        }
        
        if($id) return $b[$id];
        return $b;
    }
}