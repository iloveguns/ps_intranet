<?php

namespace app\modules\crm\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\crm\models\CrmContact;
use app\modules\crm\CrmModule;

class CrmContactSearch extends CrmContact {
    
    public function rules() {
        return [
            [['pk_contact', 'fk_crm_company', 'fk_department', 'responsible_user', 'stock', 'tags'], 'integer'],
            [['name_contact', 'contactsPhones', 'contactsEmails', 'post_contact'], 'string'],
            [['address_other', 'birthdate_contact'], 'safe'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = CrmContact::findWDep()->with(['tagsModel', 'fkCrmCompany', 'contacts']);
        
        $pageSize = isset($_GET['per-page']) ? intval($_GET['per-page']) : Yii::$app->params['default_Pagination_PageSize'];

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['name_contact'=>SORT_ASC]],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        $this->load($params);
        
        //собрать конструкцию ['or', ['like', '', ''], ...]
        if($this->contactsPhones) {//поиск по телефонам
            $query->joinWith('contacts');
            $arrcss = [];
            $arrcss[0] = 'OR';
            $contacts_info = explode(',', $this->contactsPhones);
            foreach ($contacts_info as $contact_info) {
                if(strpos($contact_info, '-') !== FALSE || mb_substr($contact_info, 0, 1) == '+') {//если указан '-', то искать в исходном номере
                    $arrcss[] = ['like', 'contact_value', trim($contact_info)];
                } else {
                    $arrcss[] = ['like', 'contact_value_search', trim($contact_info)];
                }
            }
            $query->andWhere($arrcss);
        }
        
        if($this->contactsEmails) {//поиск по email
            $query->joinWith('contacts');
            $query->andWhere(['and', ['like', 'contact_field', 'email'], ['like', 'contact_value', $this->contactsEmails]]);
        }
        
        if($this->tags) {//поиск по тегу
            $query->joinWith('tagsModel')->andWhere(['pk_tag' => $this->tags]);
        }
        
        if($this->stock === NULL) $this->stock = \app\modules\crm\CrmModule::$storeOut;

        if (!$this->validate()) {
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'stock' => $this->stock,
            'pk_contact' => $this->pk_contact,
            'birthdate_contact' => $this->birthdate_contact,
            'fk_crm_company' => $this->fk_crm_company,
            'responsible_user' => $this->responsible_user,
        ]);

        if($this->name_contact) {//попытка поиска в разных раскладках
            $multiLangNameSearch = \app\models\FunctionModel::tryChangeCharsetWord($this->name_contact);
            $query = $this->prepareMultiLangSearchFilter($query, 'name_contact', $multiLangNameSearch);
        }
        
        $query->andWhere(['is_deleted' => 0]); // временно удаленные не выводить
        
        $query->andFilterWhere(['like', 'post_contact', $this->post_contact]);

        return $dataProvider;
    }
}