<?php

namespace app\modules\crm\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\crm\models\CrmDealsProjects;

class CrmDealsProjectsSearch extends CrmDealsProjects{
    
    public function rules()
    {
        return [
            [['pk_dp', 'fk_department'], 'integer'],
            [['name_dp'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = CrmDealsProjects::findWDep();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'pk_dp' => $this->pk_dp,
            'fk_department' => $this->fk_department,
        ]);

        $query->andFilterWhere(['like', 'name_dp', $this->name_dp]);

        return $dataProvider;
    }
}