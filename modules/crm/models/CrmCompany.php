<?php

namespace app\modules\crm\models;

use Yii;
use app\modules\crm\models\AddressStreet;
use app\modules\crm\models\AddressCity;
use app\modules\crm\models\AddressCountry;
use app\modules\crm\models\CrmContact;
use yii\helpers\ArrayHelper;
use app\modules\user\models\User;
use app\modules\crm\models\CrmTasks;
use yii\helpers\Html;
use kartik\select2\Select2;

class CrmCompany extends \app\modules\crm\components\CrmModelDep {    
    const fk = 'fk_crmcompany';
    public $fk = self::fk;
    public $pk = 'pk_company';
    public $tags;//теги
    public $stockContact;
    public $contactsPhones;//телефоны контакт информация
    public $contactsEmails;//емайлы контакт информация
    public $lastTaskOrComment; // последняя задача или коментарий(действие с клиентом)

    public function __construct() {
        if(isset(Yii::$app->user) && !Yii::$app->user->identity->isAdmin()) $this->responsible_user = Yii::$app->user->id;//сразу выбран сотрудник
    }

    public function getFkKey() {
        return $this->fk_key;
    }
    
    public static function tableName() {
        return 'crm_company';
    }

    public function rules() {
        return [
            [['name_company', 'responsible_user'], 'required'],
            [['fk_address_country', 'fk_address_city', 'fk_address_street', 'fk_department', 'stock', 'stockContact'], 'integer'],
            [['birthdate_company', 'create_date', 'update_date', 'tags', 'stock_date', 'contactsPhones', 'lastTaskOrComment', 'is_deleted'], 'safe'],
            [['created_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_user' => 'id_user']],
            [['responsible_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['responsible_user' => 'id_user']],
            [['name_company', 'address_other'], 'string', 'max' => 128],
            [['comment_company'], 'string'],
            [['fk_address_street'], 'exist', 'skipOnError' => true, 'targetClass' => AddressStreet::className(), 'targetAttribute' => ['fk_address_street' => 'pk_street']],
            [['fk_address_city'], 'exist', 'skipOnError' => true, 'targetClass' => AddressCity::className(), 'targetAttribute' => ['fk_address_city' => 'pk_city']],
            [['fk_address_country'], 'exist', 'skipOnError' => true, 'targetClass' => AddressCountry::className(), 'targetAttribute' => ['fk_address_country' => 'pk_country']],
            [['fk_department'], 'exist', 'skipOnError' => true, 'targetClass' => \app\modules\user\models\Department::className(), 'targetAttribute' => ['fk_department' => 'pk_department']],
        ];
    }
    
    public function attributes() {
        return array_merge(
            parent::attributes(),
            ['tags', 'contactsPhones', 'lastTaskOrComment'] // не видит виртуальные атрибуты
        );
    }

    public function attributeLabels() {
        return [
            'pk_company' => Yii::t('app/models', 'Pk crm Company'),
            'name_company' => Yii::t('app/models', 'Name crm Company'),
            'fk_address_country' => Yii::t('app/models', 'Fk Address Country'),
            'fk_address_city' => Yii::t('app/models', 'Fk Address City'),
            'fk_address_street' => Yii::t('app/models', 'Fk Address Street'),
            'address_other' => Yii::t('app/models', 'Address crm Other'),
            'birthdate_company' => Yii::t('app/models', 'Birthdate crm Company'),
            'created_user' => Yii::t('app/models', 'Created crm User'),
            'responsible_user' => Yii::t('app/models', 'Responsible crm User'),
            'comment_company' => Yii::t('app/models', 'Comment crm Company'),
            'stock' => Yii::t('app/models', 'Crm Stock'),
            'tags' => Yii::t('app/models', 'Crm Tags'),
            'stockContact' => Yii::t('app/models', 'Crm stockContact company'),
            'contactsPhones' => Yii::t('app/models', 'Crm Phones Object'),
            'lastTaskOrComment' => Yii::t('app/models', 'Crm lastTaskOrComment')
        ];
    }

    public function getFullAddress() {
        $address = [];
        if($this->fkAddressCity) $address[] = $this->fkAddressCity->name_city;
        if($this->fkAddressStreet) $address[] = $this->fkAddressStreet->name_street;
        if($this->address_other) $address[] = $this->address_other;
        return implode(', ', $address);
    }
    
    public function getFkAddressStreet() {
        return $this->hasOne(AddressStreet::className(), ['pk_street' => 'fk_address_street']);
    }

    public function getFkAddressCity() {
        return $this->hasOne(AddressCity::className(), ['pk_city' => 'fk_address_city']);
    }

    public function getFkAddressCountry() {
        return $this->hasOne(AddressCountry::className(), ['pk_country' => 'fk_address_country']);
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord) {
                $this->created_user = Yii::$app->user->id;
            }
            
            if($this->stockContact == 1){//все контакты в архив поместить
                foreach ($this->crmContacts as $contact){
                    if($contact->stock == 1) continue;
                    $contact->stock = 1;
                    $contact->update();
                }
            }
            
            //некоторые параметры для сравнения изменений
            $old_attrs = (new \yii\db\Query())
                ->select(['responsible_user', 'stock'])
                ->from($this->tableName())
                ->where(['pk_company' => $this->getPrimaryKey()])
                ->one();
            
            //изменение архива
            if($this->stock != $old_attrs['stock']) {
                \app\modules\crm\helpers\CrmHelper::changeArchive([$this->getPrimaryKey()], $this, $this->stock);//не очень корректно
            }
            
            //проверка на смену ответственного
            if($old_attrs['responsible_user'] !== $this->responsible_user){
                foreach ($this->crmContacts as $contact) {//контакты отдать
                    if($contact->responsible_user == $old_attrs['responsible_user']) {//только свои менять
                        $contact->responsible_user = $this->responsible_user;//пересохранить контакты
                        $contact->update();
                    }
                }
                
                foreach ($this->workTasks as $wtask) {//активные задачи отдать
                    if($wtask->fk_user_task == $old_attrs['responsible_user']) {//только свои менять
                        $wtask->fk_user_task = $this->responsible_user;//пересохранить задачи
                        $wtask->update();
                    }
                }
            }
            //end проверка на смену ответственного
            
            return true;
        }
        return false;
    }
    
    public function getCrmTasks() {
        return $this->hasMany(CrmTasks::className(), ['fk_crmcompany' => 'pk_company']);
    }
    
    /**
     * получить одну последнюю задачу к компании
     */
    public function getCrmLastTask() {
        return $this->hasOne(CrmTasks::className(), ['fk_crmcompany' => 'pk_company'])
            ->orderBy('pk_task DESC')
            ->andWhere(['or',
                ['not', ['text_task' => '']],
                ['not', ['status_text' => '']]
            ]);
    }
    
    /**
     * получить контакты, которые принадлежат компании
     */
    public function getCrmContacts() {
        return $this->hasMany(CrmContact::className(), ['fk_crm_company' => 'pk_company']);
    }
    
    public static function linkTo($name, $pk, $options = []) {
        return \yii\helpers\Html::a($name, ['/crm/crm-company/view', 'id' => $pk], $options);
    }
    
    /*
     * вывод формы создания параметров поиска
     */
    public function getSearchFormAttributes() {
        $model = new CrmCompany();
        $cache = Yii::$app->cache;
        
        if($model->load(Yii::$app->request->post())){//если данные получены - используем и заодно в кеш
            $cache->set('crmcompany-search', $model->attributes,60*60*24*30);
        } else if($cache->get('crmcompany-search') === false){//если в кеше ничего нет, то данные по умолчанию
            $model = self::defaultSearchAttrs($model);
        } else{//иначе берем из кеша
            $model->attributes = $cache->get('crmcompany-search');
        }
        
        $model = self::necessarySearchAttrs($model);//внести обязательные поля
        
        return $model;
    }
    
    /*
     * обязательные поля формы поиска
     */
    private function necessarySearchAttrs($model) {
        $model->responsible_user = 1;
        $model->name_company = 1;
        
        return $model;
    }
    
    /*
     * стандартные поля на вывод формы поиска
     */
    private function defaultSearchAttrs($model) {
        //$model->tags = 1;
        
        return $model;
    }
    
    /*
     * выводит список атрибутов поиска из attributeLabels() с отсеиванием
     */
    public function getAttrs() {
        $hiddenAttrsArr = [
            'pk_company', 'stockContact',  'fk_address_city',
            'fk_address_country', 'fk_address_street', 'address_other',
            'birthdate_company', 'comment_company'
        ]; // атрибуты, которые не надо выводить
        $labels = self::attributeLabels();
        
        foreach ($hiddenAttrsArr as $attr) {
            unset($labels[$attr]);
        }        
        
        return $labels;
    }
    
    /*
     * выбрать из параметров включенные и вывести их в виджет gridview
     * кастомизировать некоторые
     */
    public function getAttrsToGridView($attributes, $searchModel){
        $arr = [];
        
        foreach ($attributes as $name => $value) {
            if($value){
                switch ($name){
                    case 'name_company' :
                        $arr[] = [
                            'attribute' => 'name_company',
                            'value' => function ($model, $key, $index, $column) {
                                return Html::a($model->name_company, ['view', 'id' => $model->pk_company], ['data-pjax' => '0']);
                            },
                            'format' => 'raw',
                        ];
                    break;
                
                    case 'created_user' :
                        $arr[] = [
                            'attribute' => 'created_user',
                            'value' => function ($model, $key, $index, $column) {
                                return Html::a(User::getAll($model->created_user, '{lastname} {name}'), ['/user/user/profile', 'id' => $model->created_user], ['target' => '_blank', 'data-pjax' => '0']);
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'created_user',
                                'data' => \app\modules\crm\CrmModule::getAllUsersCrm(NULL, true, '{lastname} {name}'),
                                'options' => ['placeholder' => '', 'id' => Yii::$app->getSecurity()->generateRandomString(8)],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]),
                            'format' => 'raw',
                        ];
                    break;
                
                    case 'responsible_user' :
                        $arr[] = [
                            'attribute' => 'responsible_user',
                            'value' => function ($model, $key, $index, $column) {
                                return Html::a(User::getAll($model->responsible_user, '{lastname} {name}'), ['/user/user/profile', 'id' => $model->responsible_user], ['target' => '_blank', 'data-pjax' => '0']);
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'responsible_user',
                                'data' => \app\modules\crm\CrmModule::getAllUsersCrm(NULL, true, '{lastname} {name}'),
                                'options' => ['placeholder' => '', 'id' => Yii::$app->getSecurity()->generateRandomString(8)],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]),
                            'format' => 'raw',
                        ];
                    break;
                
                    case 'contactsPhones' :
                        $arr[] = [
                            'attribute' => 'contactsPhones',
                            'header' => Yii::t('app/models', 'Crm Phones Object').'&nbsp;'.Html::a('(Инфо)', ['/crm/crm-faq/view', 'id' => 8]),//ид заметки про поиск номеров
                            'value' => function ($model, $key, $index, $column) {
                                return $model->allPhones;
                            },
                            'filterInputOptions' => [
                                'placeholder' => 'номера слитно, через запятую',
                                'class' => 'form-control',
                            ],
                            'format' => 'raw'
                        ];
                    break;
                
                    case 'stock' :
                        $arr[] = [
                            'attribute' => 'stock',
                            'label' => Yii::t('app/models', 'In archive'),
                            'value' => function ($model, $key, $index, $column) {
                                return \app\modules\crm\CrmModule::getStock($model->stock);
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'stock',
                                'data' => \app\modules\crm\CrmModule::getStock(),
                                'options' => ['placeholder' => Yii::t('app', 'Select'), 'id' => Yii::$app->getSecurity()->generateRandomString(8)],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]),
                            'headerOptions' => ['data-toggle' => "tooltip", 'data-placement' => "top", 'title' => '"'.Yii::t('app', 'Yes').'" - поиск по объектам в архиве, "'.Yii::t('app', 'No').'" - поиск по активным объектам'],
                            'format' => 'raw'
                        ];
                    break;
                
                    case 'tags' :
                        $arr[] = [
                            'attribute' => 'tags',
                            'value' => function ($model, $key, $index, $column) {
                                return $model->allTagsLink;
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'tags',
                                'data' => CrmTags::getAll(),
                                'options' => ['placeholder' => Yii::t('app', 'Select'), 'multiple' => true],
                                'pluginOptions' => [
                                    'tags' => true,
                                    'allowClear' => true
                                ],
                            ]),
                            'format' => 'raw'
                        ];
                    break;
                
                    case 'lastTaskOrComment' :
                        $arr[] = [
                            'attribute' => 'lastTaskOrComment',
                            'value' => function ($model, $key, $index, $column) {
                                return $model->lastTaskOrComment();
                            },
                            'enableSorting' => false,
                            'format' => 'raw'
                        ];
                    break;
                }
            }
        }
        
        $gridviewarr = array_merge(
            [[
                'class' => 'yii\grid\CheckboxColumn',
                'checkboxOptions' => ['onclick' => 'js:checkShowBtns(this.checked);']
            ]],
            $arr,
            [[
                'class' => 'yii\grid\ActionColumn',
                'buttons'=>[
                    'view' => function ($url, $model) {

                    },
                    'update'=>function ($url, $model) {
                        if($model->canAdmin()) return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [$url]);
                    },
                    'delete'=>function ($url, $model) {
                        if($model->canAdmin()) return Html::a('<span class="glyphicon glyphicon-trash"></span>', [$url], ['data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],]);
                    },
                ],
            ]]
        );
        return $gridviewarr;
    }
    
    public function lastTaskOrComment() {
        if($this->crmLastTask) {
            $rtext = '';
            
            // выполненная задача
            if($this->crmLastTask->status_task === CrmTasks::STATUS_DONE) {
                $rtext .= '<span class="text-green">';
            }
            
            //$rtext .= $this->crmLastTask->pk_task . ' - ';
            $rtext .= Html::a(User::getAll($this->crmLastTask->fk_user_task, '{lastname} {name}'), ['/user/user/profile', 'id' => $this->crmLastTask->fk_user_task], ['target' => '_blank', 'data-pjax' => '0']). ' : ';
            
            if($this->crmLastTask->status_text) {
                $rtext .= $this->crmLastTask->status_text;
            } else {
                $rtext .= $this->crmLastTask->text_task;
            }
            
            if($this->crmLastTask->status_task === CrmTasks::STATUS_DONE) {
                $rtext .= '</span>';
            }
            
            // + дата действия
            if($this->crmLastTask->update_date) {
                $rtext .= ' (' . Yii::$app->formatter->asDateTime($this->crmLastTask->update_date) . ')';
            } else {
                $rtext .= ' (' . Yii::$app->formatter->asDateTime($this->crmLastTask->create_date) . ')';
            }
            
            return $rtext;
        }
        
        return '';
    }
    
    /*
     * список всех компаний
     * $id - получить по ид
     */
    public function getAll($id = null) {
        $all = CrmCompany::getDb()->cache(function ($db) {
            return CrmCompany::findWDep()->asArray()->all();
        });
        $b = ArrayHelper::map($all, 'pk_company', 'name_company');
        if($id !== NULL) return $b[$id];
        return ArrayHelper::merge(['' => \Yii::t('app', 'Not selected')], $b);
    }
}