<?php

namespace app\modules\crm\models;

use Yii;
use app\modules\user\models\Department;

/*
 * вообще то статусы сделок это
 */
class CrmTypedeals extends \app\modules\crm\components\CrmModelDep
{
    public static function tableName()
    {
        return 'crm_typedeals';
    }

    public function rules()
    {
        return [
            [['name_typedeal'], 'required'],
            [['fk_department'], 'integer'],
            [['name_typedeal'], 'string', 'max' => 128],
            [['fk_department'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['fk_department' => 'pk_department']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_typedeal' => Yii::t('app/models', 'Pk Typedeal'),
            'name_typedeal' => Yii::t('app/models', 'Name Typedeal'),
            'fk_department' => Yii::t('app/models', 'Fk Department'),
        ];
    }

    public function getFkDepartment() {
        return $this->hasOne(Department::className(), ['pk_department' => 'fk_department']);
    }
    
    /*
     * все типы задач 
     * TODO кеш
     */
    public function getAll($id = NULL) {
        $all = CrmTypedeals::find()->asArray()->all();

        $b = [];
        foreach ($all as $tt) {
            $b[$tt['pk_typedeal']] = $tt['name_typedeal'];
        }
        
        if($id) return $b[$id];
        return $b;
    }
}