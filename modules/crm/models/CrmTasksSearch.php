<?php

namespace app\modules\crm\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\crm\models\CrmTasks;
use app\models\FunctionModel;
use app\modules\crm\CrmModule;

class CrmTasksSearch extends CrmTasks {
    public $stock,
        $searchbycompanies, // метка поиска по компаниям
        $searchbycontacts; // метка поиска по контактам
    
    public function rules() {
        return [
            [['pk_task', 'fk_user_task', 'fk_typetask', 'fk_crmcontact', 'fk_crmcompany', 'fk_department', 'created_user', 'stock', 'searchbycontacts'], 'integer'],
            [['search_text', 'search_range_from', 'search_range_to'], 'string'],
            [['date_task', 'time_task', 'text_task', 'create_date', 'status_task', 'update_date', 'searchbycompanies'], 'safe'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function searchCalendar($start = NULL, $end = NULL) {
        $query = CrmTasks::findWDep()
            ->select('date_task')//только дата
            ->joinWith([
            'fkCrmcontact', 'fkCrmcompany'
        ])->andFilterWhere([//не в хранилище
            'or',
            [CrmContact::tableName().'.stock' => CrmModule::$storeOut],
            [CrmCompany::tableName().'.stock' => CrmModule::$storeOut]
        ]);
        
        $query->andFilterWhere([
            'not in', 'status_task', [CrmTasks::STATUS_COMMENT, CrmTasks::STATUS_SYSCOMMENT, CrmTasks::STATUS_DONE],
        ])->andFilterWhere([
            'and',
            ['>=', 'date_task', $start],
            ['<=', 'date_task', $end],
        ])->andFilterWhere([
            'fk_user_task' => Yii::$app->user->id,
        ]);
        
        return $query->orderBy(['date_task' => SORT_ASC])->all();
    }
    
    /*
     * стандартный поиск
     */
    public function searchList($params) {
        $query = CrmTasks::findWDep();

        $pageSize = isset($_GET['per-page']) ? intval($_GET['per-page']) : Yii::$app->params['default_Pagination_PageSize'];

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['date_task'=>SORT_DESC, 'time_task' => SORT_DESC]],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        
        if($this->stock == 0) {//выбран поиск +архив
            $query->joinWith([
                'fkCrmcontact', 'fkCrmcompany'
            ])->andFilterWhere([//не в хранилище\\нельзя трогать
                'or',
                [CrmContact::tableName().'.stock' => CrmModule::$storeOut],
                [CrmCompany::tableName().'.stock' => CrmModule::$storeOut]
            ]);
        }
        
        if($this->update_date) {
            if(stripos($this->update_date, '|')) {//промежуток дат
                $udate = explode('|', $this->update_date);
                $query->andWhere(['>=', CrmTasks::tableName() . '.update_date', $udate[0]])
                        ->andWhere(['<=', CrmTasks::tableName() . '.update_date', $udate[1]]);
            } else {
                $query->andFilterWhere(['like', CrmTasks::tableName() . '.update_date', $this->update_date]);
            }
        }
        
        if($this->create_date) {
            if(stripos($this->create_date, '|')) {//промежуток дат
                $udate = explode('|', $this->create_date);
                $query->andWhere(['>=', CrmTasks::tableName() . '.create_date', $udate[0]])
                        ->andWhere(['<=', CrmTasks::tableName() . '.create_date', $udate[1]]);
            } else {
                $query->andFilterWhere(['like', CrmTasks::tableName() . '.create_date', $this->create_date]);
            }
        }
        
        //текстовый поиск
        if(!empty($this->search_text)) {//если есть поиск строковый, то неважен статус задач
            //текст искать в 2 местах
            $query->andFilterWhere([
                'or',
                ['like', 'status_text', $this->search_text],
                ['like', 'text_task', $this->search_text],
            ]);            
        }
        
        //если статус не выбран - такие условия
        if($this->status_task === NULL) {
            $this->status_task = CrmTasks::STATUS_WORK;//в процессе
            
            $query->andFilterWhere([
                'not in', 'status_task', [CrmTasks::STATUS_COMMENT, CrmTasks::STATUS_SYSCOMMENT, CrmTasks::STATUS_DONE],
            ]);
        } else if($this->status_task == 'nocomments') { // все, без комментариев
            $this->status_task = NULL;
                    
            $query->andFilterWhere([
                'not in', 'status_task', [CrmTasks::STATUS_COMMENT, CrmTasks::STATUS_SYSCOMMENT],
            ]);
        }
        $query->andFilterWhere(['status_task' => $this->status_task]);
        
        if($this->date_task) {//конкретная дата
            $query->andFilterWhere([
                'date_task' => $this->date_task,
            ]);
        } else if($this->search_range_from) {//диапазон дат
            $query->andFilterWhere([
                'and',
                ['>=', 'date_task', $this->search_range_from],
                ['<=', 'date_task', $this->search_range_to],
            ]);
        }
        
        if(empty($this->fk_user_task)){//0 - не подставлят значение текущего сотрудника
            $query->andFilterWhere(['!=', 'fk_user_task', $this->created_user]);
        } elseif($this->fk_user_task == -1) {//-1 - не учитывать поиск по сотруднику
            
        } else {
            $query->andFilterWhere(['fk_user_task' => $this->fk_user_task]);
            if($this->fk_user_task === NULL) $this->fk_user_task = Yii::$app->user->id;//если не выбран - показывать свои
        }
        
        //поиск только по задачам контактов
        if($this->searchbycontacts) {
            $query->andWhere(['not', ['fk_crmcontact' => NULL]]);
        } else if($this->searchbycompanies) {//поиск только по задачам компаний
            $query->andWhere(['not', ['fk_crmcompany' => NULL]]);
        }
        
        $query->andFilterWhere([
            CrmTasks::tableName().'.created_user' => $this->created_user,            
            'fk_typetask' => $this->fk_typetask
        ]);

        $query->andFilterWhere(['like', 'time_task', $this->time_task])
            ->andFilterWhere(['like', 'text_task', $this->text_task]);
        
        return $dataProvider;
    }
    
    /*
     * новый поиск для тудулайн по 4 критериям
     * каждый критерий отдельно находит
     */
    public function searchLinen($params) {
        $arrModels = [];
        $arrModels['today'] = $arrModels['late'] = $arrModels['tomorrow'] = $arrModels['done'] = [];
        $arrModels['countToday'] = $arrModels['countLate'] = $arrModels['countTomorrow'] = $arrModels['countDone'] = 0;
        
        if(!isset($params['CrmTasksSearch']['status_task']) || $params['CrmTasksSearch']['status_task'] != CrmTasks::STATUS_DONE){//для завершенных отдельный подсчет - только их
            $arrToday = $this->searchLine($params);
            $arrLate = $this->searchLine($params);
            $arrTomorrow = $this->searchLine($params);
            
            $arrToday->query->andWhere(['date_task' => FunctionModel::getDateWParam('Y-m-d')])->andWhere(['!=', 'status_task', CrmTasks::STATUS_LATE]);
            $arrLate->query->andWhere(['status_task' => CrmTasks::STATUS_LATE]);
            $arrTomorrow->query->andWhere(['date_task' => FunctionModel::dateModify(FunctionModel::getDateWParam(Yii::$app->params['saveModelDateTime']), '+1 day', 'Y-m-d')]);
                        
            foreach ($arrToday->getModels() as $dataToday) {
                $arrModels['today'][] = $dataToday;
            }
            
            foreach ($arrLate->getModels() as $dataLate) {
                $arrModels['late'][] = $dataLate;
            }
            
            foreach ($arrTomorrow->getModels() as $dataTom) {                
                $arrModels['tomorrow'][] = $dataTom;
            }
            
            $arrModels['countLate'] = $arrLate->getTotalCount();
            $arrModels['countToday'] = $arrToday->getTotalCount();
            $arrModels['countTomorrow'] = $arrTomorrow->getTotalCount();
        } else {
            $arrDones = $this->searchLine($params);
            
            foreach ($arrDones->getModels() as $dataTom) {                
                $arrModels['done'][] = $dataTom;
            }
            
            $arrModels['countDone'] = $arrDones->getTotalCount();
        }
        
        return $arrModels;
    }
    
    /*
     * поиск для сегодня/завтра
     */
    public function searchLine($params) {
        
        $query = CrmTasks::findWDep()->joinWith([
            'fkUserTask'
        ]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['date_task'=>SORT_ASC, 'time_task' => SORT_ASC]],
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);
        
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        
        if($this->update_date) {
            if(stripos($this->update_date, '|')) {//промежуток дат
                $udate = explode('|', $this->update_date);
                $query->andWhere(['>=', CrmTasks::tableName() . '.update_date', $udate[0]])
                        ->andWhere(['<=', CrmTasks::tableName() . '.update_date', $udate[1]]);
            } else {
                $query->andFilterWhere(['like', CrmTasks::tableName() . '.update_date', $this->update_date]);
            }
        }
        
        if($this->stock == 0) {//выбран поиск +архив
            $query->joinWith([
                'fkCrmcontact', 'fkCrmcompany'
            ])->andFilterWhere([//не в хранилище\\нельзя трогать
                'or',
                [CrmContact::tableName().'.stock' => CrmModule::$storeOut],
                [CrmCompany::tableName().'.stock' => CrmModule::$storeOut]
            ]);
        }
        
        //текстовый поиск
        if(!empty($this->search_text)) {//если есть поиск строковый, то неважен статус задач
            //текст искать в 2 местах
            $query->andFilterWhere([
                'or',
                ['like', 'status_text', $this->search_text],
                ['like', 'text_task', $this->search_text],
            ]);
        }
        
        //если статус не выбран - такие условия
        if($this->status_task === NULL) {            
            $query->andFilterWhere([
                'not in', 'status_task', [CrmTasks::STATUS_COMMENT, CrmTasks::STATUS_SYSCOMMENT, CrmTasks::STATUS_DONE],
            ]);
        }
        $query->andFilterWhere(['status_task' => $this->status_task]);
        
        if($this->date_task) {//конкретная дата
            $query->andFilterWhere([
                'date_task' => $this->date_task,
            ]);
        } else if($this->search_range_from) {//диапазон дат
            $query->andFilterWhere([
                'and',
                ['>=', 'date_task', $this->search_range_from],
                ['<=', 'date_task', $this->search_range_to],
            ]);
        }
        
        if($this->fk_user_task === NULL) $this->fk_user_task = Yii::$app->user->id;//если не выбран - показывать свои        
        if($this->fk_user_task != -1) {//-1 - не учитывать поиск по сотруднику
            $query->andFilterWhere([
                'fk_user_task' => $this->fk_user_task,
            ]);
        } 
        
        $query->andFilterWhere([
            'fk_typetask' => $this->fk_typetask,
        ]);
        
        return $dataProvider;
    }
}