<?php

namespace app\modules\crm\models;

use Yii;

class CrmTags extends \app\modules\crm\components\CrmModelAll
{
    public static function tableName()
    {
        return 'crm_tags';
    }

    public function rules()
    {
        return [
            [['name_tag'], 'required'],
            [['name_tag'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_tag' => 'Pk Tag',
            'name_tag' => 'Name Tag',
        ];
    }
    
    public function getAll($id = NULL) {        
        $all = CrmTags::findWDep()->asArray()->all();

        $b = [];
        foreach ($all as $tt) {
            $b[$tt['pk_tag']] = $tt['name_tag'];
        }
        
        if($id) return $b[$id];
        return $b;
    }
}