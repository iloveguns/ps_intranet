<?php

namespace app\modules\crm\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\crm\models\CrmDeals;

class CrmDealsSearch extends CrmDeals {
    
    public function rules() {
        return [
            [['pk_deal', 'price_deal', 'status_deal', 'fk_crmcontact', 'fk_crmcompany', 'responsible_user', 'created_user', 'updated_user', 'fk_deals_project'], 'integer'],
            [['title_task'], 'string'],
            [['name_deal', 'create_date', 'update_date', 'status_date'], 'safe'],
        ];
    }
    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = CrmDeals::findWDep();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['create_date' => SORT_DESC]],
        ]);

        $this->load($params);

        if($this->responsible_user === NULL && Yii::$app->user->identity->fkDepartment->pk_department == \app\modules\crm\CrmModule::$idCrmDep) {
            $this->responsible_user = Yii::$app->user->id;
        }
        
        if (!$this->validate()) {
            return $dataProvider;
        }
        
        //поиск по компании/контакту  654665|contact
        if(stripos($this->title_task, '|')) {
            $arr = explode('|', $this->title_task);
            $id = (int)$arr[0];
            if($arr[1] == 'company') {
                $this->fk_crmcompany = $id;
                
                $m = (new \yii\db\Query())
                ->select(['name_company'])
                ->from(CrmCompany::tableName())
                ->where(['pk_company' => $id])
                ->one();
                $this->title_task = $m['name_company'].' ('.Yii::t('app/views', 'Crm Company').')';
            } else if($arr[1] == 'contact') {
                $this->fk_crmcontact = $id;
                
                $m = (new \yii\db\Query())
                ->select(['name_contact'])
                ->from(CrmContact::tableName())
                ->where(['pk_contact' => $id])
                ->one();
                $this->title_task = $m['name_contact'].' ('.Yii::t('app/views', 'Crm Contact').')';
            }
        }
        
        if($this->create_date) {
            $query->andFilterWhere([
                'like', 'create_date', $this->create_date,
            ]);
        }

        $query->andFilterWhere([
            'pk_deal' => $this->pk_deal,
            'price_deal' => $this->price_deal,
            'status_deal' => $this->status_deal,
            'fk_crmcontact' => $this->fk_crmcontact,
            'fk_crmcompany' => $this->fk_crmcompany,
            'fk_deals_project' => $this->fk_deals_project,
            'responsible_user' => $this->responsible_user,
            'created_user' => $this->created_user,
            'update_date' => $this->update_date,
            'updated_user' => $this->updated_user,
            'status_date' => $this->status_date,
        ]);

        $query->andFilterWhere(['like', 'name_deal', $this->name_deal]);

        return $dataProvider;
    }
}