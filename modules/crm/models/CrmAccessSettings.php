<?php

namespace app\modules\crm\models;

use Yii;
use app\modules\user\models\User;
use app\modules\crm\models\CrmRoles;
use app\modules\user\models\Department;

/*
 * Класс для управления ролями внутри црм и доступ к отделам
 */
class CrmAccessSettings extends \app\modules\crm\components\CrmModelAll
{
    public static function tableName() {
        return 'crm_access_settings';
    }

    public function rules() {
        return [
            [['fk_user', 'fk_role'], 'required'],
            [['fk_user', 'fk_department', 'fk_role'], 'integer'],
            [['fk_role'], 'exist', 'skipOnError' => true, 'targetClass' => CrmRoles::className(), 'targetAttribute' => ['fk_role' => 'pk_role']],
            [['fk_department'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['fk_department' => 'pk_department']],
            [['fk_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_user' => 'id_user']],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'fk_user' => Yii::t('app/models', 'Id User'),
            'fk_department' => Yii::t('app/models', 'Fk Department'),
            'fk_role' => Yii::t('app/models', 'Crm Role'),
        ];
    }
    
    public function getFkRole() {
        return $this->hasOne(CrmRoles::className(), ['pk_role' => 'fk_role']);
    }

    public function getFkDepartment() {
        return $this->hasOne(Department::className(), ['pk_department' => 'fk_department']);
    }

    public function getFkUser() {
        return $this->hasOne(User::className(), ['id_user' => 'fk_user']);
    }
}