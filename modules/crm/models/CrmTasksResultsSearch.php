<?php

namespace app\modules\crm\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\crm\models\CrmTasksResults;

class CrmTasksResultsSearch extends CrmTasksResults{
    public function rules()
    {
        return [
            [['pk_task_result', 'denial', 'fk_department'], 'integer'],
            [['name_task_result'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = CrmTasksResults::findWDep();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'pk_task_result' => $this->pk_task_result,
            'denial' => $this->denial,
            'fk_department' => $this->fk_department,
        ]);

        $query->andFilterWhere(['like', 'name_task_result', $this->name_task_result]);

        return $dataProvider;
    }
}