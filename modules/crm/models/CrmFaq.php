<?php

namespace app\modules\crm\models;

use Yii;
use yii\db\ActiveRecord;

/*
 * Класс для вопросов/ответов в црм
 */
class CrmFaq extends ActiveRecord
{
    public static function tableName()
    {
        return 'crm_faq';
    }
    
    public function behaviors() {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date'],
                ],
                'value' => function() { return \app\models\FunctionModel::getDateTimestamp();},
            ],
        ];
    }

    public function rules()
    {
        return [
            [['question_faq', 'answer_faq'], 'required'],
            [['question_faq', 'answer_faq'], 'string'],
            [['create_date'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_faq' => Yii::t('app/models', 'Pk Faq'),
            'question_faq' => Yii::t('app/models', 'Question Faq'),
            'answer_faq' => Yii::t('app/models', 'Answer Faq'),
            'create_date' => Yii::t('app/models', 'Create Date'),
        ];
    }
}