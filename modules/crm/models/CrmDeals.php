<?php

namespace app\modules\crm\models;

use Yii;
use app\modules\crm\models\CrmCompany;
use app\modules\crm\models\CrmContact;
use app\modules\user\models\User;

class CrmDeals extends \app\modules\crm\components\CrmModelAll {
    public $url;
    public $title_task;//составная инфа для определения к кому относится задача
    
    public static function tableName() {
        return 'crm_deals';
    }

    public function rules() {
        return [
            [['type_deal', 'status_deal'], 'required'],//'name_deal', 
            [['name_deal'], 'string'],
            [['price_deal', 'status_deal', 'fk_crmcontact', 'fk_crmcompany', 'responsible_user', 'created_user', 'updated_user', 'fk_department', 'type_deal', 'fk_deals_project'], 'integer'],
            [['create_date', 'update_date', 'status_date'], 'safe'],
            [['fk_crmcompany'], 'exist', 'skipOnError' => true, 'targetClass' => CrmCompany::className(), 'targetAttribute' => ['fk_crmcompany' => 'pk_company']],
            [['fk_crmcontact'], 'exist', 'skipOnError' => true, 'targetClass' => CrmContact::className(), 'targetAttribute' => ['fk_crmcontact' => 'pk_contact']],
            [['created_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_user' => 'id_user']],
            [['responsible_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['responsible_user' => 'id_user']],
            [['updated_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_user' => 'id_user']],
            [['fk_department'], 'exist', 'skipOnError' => true, 'targetClass' => \app\modules\user\models\Department::className(), 'targetAttribute' => ['fk_department' => 'pk_department']],
        ];
    }

    public function attributeLabels() {
        return [
            'pk_deal' => Yii::t('app/models', 'Pk Deal'),
            'title_task' => Yii::t('app/models', 'Title crm Task'),
            'fk_deals_project' => Yii::t('app/models', 'Crm Deals Project'),
            'name_deal' => Yii::t('app/models', 'Name Deal'),
            'price_deal' => Yii::t('app/models', 'Price Deal'),
            'status_deal' => Yii::t('app/models', 'Status Deal'),
            'type_deal' => Yii::t('app/models', 'Type Deal'),
            'fk_crmcontact' => 'Fk Crmcontact',
            'fk_crmcompany' => 'Fk Crmcompany',
            'create_date' => Yii::t('app/models', 'Create Date'),
            'responsible_user' => Yii::t('app/models', 'Responsible crm User'),
            'created_user' => 'Created User',
            'update_date' => 'Update Date',
            'updated_user' => 'Updated User',
            'status_date' => 'Status Date',
        ];
    }
    
    public function afterFind() {
        $name = $link = '';
        
        //формирование ссылки на клиента(контакт, компания)
        if($this->fkCrmcontact) {
            $name = $this->fkCrmcontact->name_contact;
            $link = ['/crm/crm-contact/view', 'id' => $this->fkCrmcontact->pk_contact];
        } else if($this->fkCrmcompany){
            $name = $this->fkCrmcompany->name_company;
            $link = ['/crm/crm-company/view', 'id' => $this->fkCrmcompany->pk_company];
        }
        $this->url = \yii\helpers\Url::to($link);
        $this->title_task = \yii\helpers\Html::a($name, $link, ['data-pjax' => '0']);
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord){
                $this->responsible_user = $this->created_user = Yii::$app->user->id;
                
                //с названием замуты
                if($this->fk_deals_project) {//название сделки = название проекта сделок
                    $this->name_deal = $this->fkDealsProject->name_dp;
                } else if(empty($this->name_deal)) {
                    $this->name_deal = 'noname';
                }
            }
            return true;
        }
        return false;
    }

    public function getFkDealsProject()
    {
        return $this->hasOne(CrmDealsProjects::className(), ['pk_dp' => 'fk_deals_project']);
    }
    
    public function getFkCrmcontact()
    {
        return $this->hasOne(CrmContact::className(), ['pk_contact' => 'fk_crmcontact']);
    }

    public function getFkCrmcompany()
    {
        return $this->hasOne(CrmCompany::className(), ['pk_company' => 'fk_crmcompany']);
    }
    
    
}