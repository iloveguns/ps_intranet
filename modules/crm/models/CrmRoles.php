<?php

namespace app\modules\crm\models;

use Yii;
use app\modules\user\models\Organization;

/*
 * Класс для ролей внутри црм
 */
class CrmRoles extends \yii\db\ActiveRecord
{    
    public static function tableName()
    {
        return 'crm_roles';
    }

    public function rules()
    {
        return [
            [['fk_organization'], 'integer'],
            [['name', 'json_data', 'fk_organization'], 'required'],
            [['json_data'], 'string'],
            [['name'], 'string', 'max' => 128],
            [['fk_organization'], 'exist', 'skipOnError' => true, 'targetClass' => Organization::className(), 'targetAttribute' => ['fk_organization' => 'pk_organization']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_role' => Yii::t('app/models', 'Pk Role'),
            'fk_organization' => Yii::t('app/models', 'Fk Organization'),
            'name' => Yii::t('app/models', 'Name Crm Role'),
            'json_data' => Yii::t('app/models', 'Json Data'),
        ];
    }

    public function getFkOrganization()
    {
        return $this->hasOne(Organization::className(), ['pk_organization' => 'fk_organization']);
    }
    
    /*
     * список всех ролей
     * $id - получить по ид
     */
    public function getAll($id = null) {
        $all = CrmRoles::find()->asArray()->all();
        $b = \yii\helpers\ArrayHelper::map($all, 'pk_role', 'name');
        if($id) return $b[$id];
        return $b;
    }
}