<?php

namespace app\modules\crm\models;

use Yii;
use yii\helpers\ArrayHelper;

class AddressStreet extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'address_street';
    }

    public function rules()
    {
        return [
            [['name_street', 'fk_city'], 'required'],
            [['name_street'], 'string'],
            [['fk_city'], 'integer'],
            [['fk_city'], 'exist', 'skipOnError' => true, 'targetClass' => AddressCity::className(), 'targetAttribute' => ['fk_city' => 'pk_city']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_street' => Yii::t('app/models', 'Pk Street'),
            'name_street' => Yii::t('app/models', 'Name Street'),
            'fk_city' => Yii::t('app/models', 'Fk City'),
        ];
    }

    public function getFkCity()
    {
        return $this->hasOne(AddressCity::className(), ['pk_city' => 'fk_city']);
    }

    public function getCrmCompanies()
    {
        return $this->hasMany(CrmCompany::className(), ['fk_address_street' => 'pk_street']);
    }
    
    /*
     * список всех улиц
     * $id - получить по ид
     */
    public function getAll($id = null) {
        $all = AddressStreet::find()->asArray()->all();
        $b = ArrayHelper::map($all, 'pk_street', 'name_street');
        if($id !== NULL) return $b[$id];
        return ArrayHelper::merge(['' => \Yii::t('app', 'Not selected')], $b);
        return $b;
    }
}