<?php

namespace app\modules\crm\models;

use Yii;

class CrmContactsInfo extends \yii\db\ActiveRecord {
    public static function tableName()
    {
        return 'crm_contacts_info';
    }
    public function rules()
    {
        return [
            [['contact_field', 'contact_value'], 'required'],
            [['contact_field', 'contact_value', 'contact_comment'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_contacts_info' => Yii::t('app/models', 'Pk Contacts Info'),
            'contact_field' => Yii::t('app/models', 'Crm Contact_inf Field'),
            'contact_value' => Yii::t('app/models', 'Crm Contact_inf Value'),
            'contact_comment' => Yii::t('app/models', 'Crm Contact_info Comment'),
        ];
    }
    
    public function PossibleValues($id = null) {
        $b = [
            'email' => 'email',
            'workEmail' => 'Рабочий email',
            'otherEmail' => 'Другой email',
            'workPhone' => 'Рабочий телефон',
            'workddPhone' => 'Раб. прямой',
            'homePhone' => 'Домашний телефон',
            'mobilePhone' => 'Мобильный телефон',
            'web' => 'web',
            'skype' => 'Skype',
            'icq' => 'ICQ',
            'fax' => 'Факс',
            ];
        if($id !== NULL) return $b[$id];
        return $b;
    }
}