<?php

namespace app\modules\crm\models;

use Yii;
use app\modules\user\models\Department;

/*
 * Класс для проектов сделок по периоду(на месяц, на квартал и тд)
 */
class CrmDealsProjects extends \app\modules\crm\components\CrmModelAll {
    
    public static function tableName()
    {
        return 'crm_deals_projects';
    }

    public function rules()
    {
        return [
            [['name_dp'], 'required'],
            [['fk_department'], 'integer'],
            [['name_dp'], 'string', 'max' => 256],
            [['fk_department'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['fk_department' => 'pk_department']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_dp' => Yii::t('app/models', 'Pk Dp'),
            'name_dp' => Yii::t('app/models', 'Name Crm Deals Project'),
            'fk_department' => Yii::t('app/models', 'Fk Department'),
        ];
    }

    public function getFkDepartment()
    {
        return $this->hasOne(Department::className(), ['pk_department' => 'fk_department']);
    }
    
     public function getAll($id = NULL) {        
        $all = self::findWDep()->asArray()->all();

        $b = [];
        foreach ($all as $tt) {
            $b[$tt['pk_dp']] = $tt['name_dp'];
        }
        
        if($id) return $b[$id];
        return $b;
    }
}