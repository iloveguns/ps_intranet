<?php

namespace app\modules\crm\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\crm\models\CrmAccessSettings;

class CrmAccessSettingsSearch extends CrmAccessSettings{
    public function rules()
    {
        return [
            [['id', 'fk_user', 'fk_department', 'fk_role'], 'integer'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = CrmAccessSettings::findWDep();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'fk_user' => $this->fk_user,
            'fk_department' => $this->fk_department,
            'fk_role' => $this->fk_role,
        ]);

        return $dataProvider;
    }
}