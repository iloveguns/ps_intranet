<?php

namespace app\modules\crm\models;

use Yii;

class CrmRelationTags extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'crm_relation_tags';
    }

    public function rules()
    {
        return [
            [['fk_tag'], 'required'],
            [['fk_tag', 'fk_crmcompany', 'fk_crmcontact'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_rt' => 'Pk Rt',
            'fk_tag' => 'Fk Tag',
            'fk_crmcompany' => 'Fk Company',
            'fk_crmcontact' => 'Fk Contact',
        ];
    }
}