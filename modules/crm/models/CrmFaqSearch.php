<?php

namespace app\modules\crm\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\crm\models\CrmFaq;

class CrmFaqSearch extends CrmFaq{
    public function rules()
    {
        return [
            [['pk_faq'], 'integer'],
            [['question_faq', 'answer_faq', 'create_date'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = CrmFaq::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'pk_faq' => $this->pk_faq,
            'create_date' => $this->create_date,
        ]);

        $query->andFilterWhere(['like', 'question_faq', $this->question_faq])
            ->andFilterWhere(['like', 'answer_faq', $this->answer_faq]);

        return $dataProvider;
    }
}