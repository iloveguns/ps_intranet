<?php

namespace app\modules\crm\models;

use Yii;
use app\modules\user\models\User;
use app\modules\crm\models\CrmTypetask;
use app\modules\crm\models\CrmCompany;
use app\modules\crm\models\CrmContact;
use app\modules\crm\CrmModule;
use app\modules\crm\models\CrmTasksResults;
use yii\db\ActiveRecord;

class CrmTasks extends \app\modules\crm\components\CrmModelAll implements \app\modules\intranet\interfaces\AllInterface, \app\interfaces\ClassnameInterface {
    const STATUS_WORK = 1;
    const STATUS_DELETE = 2;
    const STATUS_LATE = 3;
    const STATUS_COMMENT = 11;//комментарии тоже сюда
    const STATUS_SYSCOMMENT = 12;//системный комментарий
    const STATUS_DONE = 10;
    
    const TEXT_CHANGE_RESPONSIBLE = '::CHANGE_RESPONSIBLE::';
    const TEXT_TO_ARCHIVE = '::TOARCHIVE::';
    const TEXT_FROM_ARCHIVE = '::FROMARCHIVE::';
    
    public $url;
    public $title_task;//составная инфа для определения к кому относится задача
    public $search_text;//произвольный поиск по задачам 
    public $search_range_from, $search_range_to;//произвольный поиск по задачам дата
    
    public function __construct() {
        if(Yii::$app->id != 'basic-console' && isset(Yii::$app->user)) {//консольное приложение(cron)
            $this->fk_user_task = Yii::$app->user->id;//сразу выбран сотрудник, перетирается
        }
    }

    public function behaviors() {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_date'],
                ],
                'value' => function() { return \app\models\FunctionModel::getDateTimestamp();},
            ],
        ];
    }
    
    public static function tableName() {
        return 'crm_tasks';
    }

    public function rules() {
        return [            
            [['create_date', 'update_date'], 'safe'],
            [['date_task', 'fk_user_task'], 'required'],
            [['fk_typetask'], 'required', 'on' => 'create'],//создании задач, а не комментариев
            [['date_task'], 'app\components\DateValidator', 'min' => '-1 day', 'on' => 'create'],//только при создании, чтобы старые можно было закрывать
            [['date_task'],'date', 'format' => 'yyyy-MM-dd'],
            [['fk_user_task', 'fk_typetask', 'fk_crmcontact', 'fk_crmcompany', 'status_task', 'fk_department', 'fk_task_result', 'result_task', 'created_user'], 'integer'],
            [['text_task', 'time_task', 'date_task', 'status_text', 'title_task'], 'string'],
            [['fk_crmcompany'], 'exist', 'skipOnError' => true, 'targetClass' => CrmCompany::className(), 'targetAttribute' => ['fk_crmcompany' => 'pk_company']],
            [['fk_typetask'], 'exist', 'skipOnError' => true, 'targetClass' => CrmTypetask::className(), 'targetAttribute' => ['fk_typetask' => 'pk_typetask']],
            [['fk_user_task'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_user_task' => 'id_user']],
            [['created_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_user' => 'id_user']],
            [['fk_crmcontact'], 'exist', 'skipOnError' => true, 'targetClass' => CrmContact::className(), 'targetAttribute' => ['fk_crmcontact' => 'pk_contact']],
            [['fk_department'], 'exist', 'skipOnError' => true, 'targetClass' => \app\modules\user\models\Department::className(), 'targetAttribute' => ['fk_department' => 'pk_department']],
            [['fk_task_result'], 'exist', 'skipOnError' => true, 'targetClass' => CrmTasksResults::className(), 'targetAttribute' => ['fk_task_result' => 'pk_task_result']],
        ];
    }
    
    public function attributeLabels() {
        return [
            'pk_task' => Yii::t('app/models', 'Pk crm Task'),
            'title_task' => Yii::t('app/models', 'Title crm Task'),
            'date_task' => Yii::t('app/models', 'Date crm Task'),
            'time_task' => Yii::t('app/models', 'Time crm Task'),
            'created_user' => Yii::t('app/models', 'Fk created user'),
            'fk_user_task' => Yii::t('app/models', 'Fk crm User Task'),
            'fk_typetask' => Yii::t('app/models', 'Fk crm Typetask'),
            'fk_crmcontact' => Yii::t('app/models', 'Fk Crmcontact'),
            'fk_crmcompany' => Yii::t('app/models', 'Fk Crmcompany'),
            'text_task' => Yii::t('app/models', 'Text crm Task'),
            'create_date' => Yii::t('app/models', 'Create Date'),
            'update_date' => Yii::t('app/models', 'CRm Update Date Task'),
            'status_task' => Yii::t('app/models', 'Status crm Task'),
            'status_text' => Yii::t('app/models', 'Crm Status Text'),
            'search_text' => Yii::t('app/models', 'Search text crm tasks'),
            'result_task' => Yii::t('app/models', 'Crm task result'),
            'fk_task_result' => Yii::t('app/models', 'Crm results of task'),
            'stock' => Yii::t('app/models', 'Crm Search tasks in stock'),
        ];
    }
    
    public function getTitleText() {
        return $this->linkToEntity['name'];
    }
    
    public function getUrl($absolute = false) {
        $le = $this->linkToEntity;
        return \yii\helpers\Url::to($le['link'], $absolute);
    }

    public function getHtmlLink($icon = '') {
        $le = $this->linkToEntity;
        return \yii\helpers\Html::a($icon.' ('.$le['name'].')', $le['link'], ['data-pjax' => '']);
    }
    
    public function getFkCrmcompany()
    {
        return $this->hasOne(CrmCompany::className(), ['pk_company' => 'fk_crmcompany']);
    }
    
    public function getFkCrmcontact()
    {
        return $this->hasOne(CrmContact::className(), ['pk_contact' => 'fk_crmcontact']);
    }
    
    public function getFkUserTask() {
        return $this->hasOne(User::className(), ['id_user' => 'fk_user_task']);
    }
    
    public function getFkCreatedUserTask() {
        return $this->hasOne(User::className(), ['id_user' => 'created_user']);
    }

    public function getLinkToEntity() {
        $name = $link = '';
        //формирование ссылки на клиента(контакт, компания)
        if($this->fkCrmcontact) {
            $name = $this->fkCrmcontact->name_contact;
            $link = ['/crm/crm-contact/view', 'id' => $this->fkCrmcontact->pk_contact];
        } else if($this->fkCrmcompany){
            $name = $this->fkCrmcompany->name_company;
            $link = ['/crm/crm-company/view', 'id' => $this->fkCrmcompany->pk_company];
        }
        
        return ['name' => $name, 'link' => $link];
    }

    public function afterFind() {
        $name = $link = '';
        
        $le = $this->linkToEntity;
        //$this->url = \yii\helpers\Url::to($link);
        $this->url = $link; // не понял, пустая строка
        $this->title_task = \yii\helpers\Html::a($le['name'], $le['link'], ['data-pjax' => '']);
        
        //проверка и замена спецстрок
        if(strripos($this->status_text, '::SELFLINK::') !== FALSE){//ссылка на себя
            $this->status_text = str_replace('::SELFLINK::', $this->title_task, $this->status_text);
        } else if(strripos($this->status_text, self::TEXT_CHANGE_RESPONSIBLE) !== FALSE) {//символ смены ответственного_с кого|на кого
            $a = explode(' ', $this->status_text);
            $b = explode('|', $a[1]);
            $old_res = User::findOne($b[0]);//найти данные ответственных
            $new_res = User::findOne($b[1]);
            $this->status_text = Yii::t('app/models', 'Changed responsible from {old_resp} to {new_resp}', ['old_resp' => $old_res->linkToProfile, 'new_resp' => $new_res->linkToProfile]);
        } else if(strripos($this->status_text, self::TEXT_TO_ARCHIVE) !== FALSE) {//помещен в архив_кем
            $a = explode(' ', $this->status_text);
            $user = User::findOne($a[1]);
            $this->status_text = Yii::t('app/models', 'Put into archive by {user}', ['user' => $user->linkToProfile]);
        } else if(strripos($this->status_text, self::TEXT_FROM_ARCHIVE) !== FALSE) {//помещен в архив_кем
            $a = explode(' ', $this->status_text);
            $user = User::findOne($a[1]);
            $this->status_text = Yii::t('app/models', 'Restored from archive by {user}', ['user' => $user->linkToProfile]);
        }
    }
    
    /*
     * статусы задач
     */
    public function getAllStatus($id = NULL) {
        $arr = [
            self::STATUS_WORK => Yii::t('app/models', 'Crm status work'),
            self::STATUS_LATE => Yii::t('app/models', 'Crm status late'),
            //self::STATUS_DELETE => Yii::t('app/models', 'Crm status delete'),
            self::STATUS_DONE => Yii::t('app/models', 'Crm status done'),
        ];
        
        if($id) return $arr[$id];
        return $arr;
    }
    
    public function getFkTypetask()
    {
        return $this->hasOne(CrmTypetask::className(), ['pk_typetask' => 'fk_typetask']);
    }
    
    /*
     * определить из составных данных к чему относится
     */
    public function getModels() {
        if(stristr($this->title_task, '|')){
            $expl = explode('|', $this->title_task);
            switch ($expl[1]) {
                case 'contact':
                    $this->fk_crmcontact = $expl[0];
                    $this->fk_crmcompany = NULL;
                    break;
                case 'company':
                    $this->fk_crmcontact = NULL;
                    $this->fk_crmcompany = $expl[0];
                    break;            
                default:
                    break;
            }
        }
        return $this;
    }
    
    /*
     * проверка, может ли сотрудник админить задачу
     */
    public function canAdmin() {
        if(CrmModule::canAdminDep(Yii::$app->user->id)) return true;
        //совместимость со старьем ИЛИ создано собой и для себя И ответств задачи = текущему сотруднику
        if(($this->created_user === NULL || $this->created_user === $this->fk_user_task) && $this->fk_user_task == Yii::$app->user->id) return true;
        
        return false;
    }
    
    /*
     * иконка по статусу
     */
    public function getIconStatus() {
        $icon = '';
        switch ($this->status_task){
            case self::STATUS_COMMENT :
                $icon = '<i class="fa fa-commenting-o" aria-hidden="true"></i>';
                break;
            
            case self::STATUS_SYSCOMMENT :
                $icon = '<i class="fa fa-exclamation" aria-hidden="true"></i>';
                break;
                
            default : 
                $icon = '<i class="fa fa-tasks" aria-hidden="true"></i>';
        }
        return $icon;
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord){
                if(!$this->status_task)
                    $this->status_task = $this::STATUS_WORK;
                $this->created_user = Yii::$app->user->id;
            } else {
                $this->updated_user = Yii::$app->user->id;
            }
            $this->getModels();

            return true;
        }
        return false;
    }
    
    /*
     * создание системного комментария(истории) к объекту
     * @var $text {string}      - текст комментария
     * @var $arrIds {array}     - ид объектов, у которых нужно создать коммент
     * @var $model {Object}     - модель в которой происходят изменения
     * 
     * $addInfo['addtext'] - добавление к каждому сохранению доп текста
     *         ['addtextall'] - добавление ко всем 
     */
    public function createSysComment($text, $arrIds, $model, $addInfo = NULL) {
        $fk = $model->fk;
        $date = date('Y-m-d');
        $id_user = Yii::$app->user->id;        
        $fk_dep = CrmModule::$idCrmDep;
        
        $arrToInsert = [];
        foreach ($arrIds as $id) {
            $wtext = $text;
            if(isset($addInfo['addtext'])) $wtext = $wtext . ' ' . $addInfo['addtext'][$id];
            if(isset($addInfo['addtextall'])) $wtext = $wtext . ' ' . $addInfo['addtextall'];
            $arrToInsert[] = [$date, $id_user, CrmTasks::STATUS_SYSCOMMENT, $wtext, $id, $fk_dep];
        }
        
        $saved = Yii::$app->db->createCommand()->batchInsert(CrmTasks::tableName(), ['date_task', 'fk_user_task', 'status_task', 'status_text', $model->fk, 'fk_department'], $arrToInsert)->execute();
        if($saved){
            return true;
        } else {
            Yii::warning('проблема создания системного комментария');//посмотреть url в котором ошибка без лишнего
        }
    }

    public function getSelfLink() {
        
    }

    public function getName() {
        return 'crm_tasks';
    }

}