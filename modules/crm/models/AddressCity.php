<?php

namespace app\modules\crm\models;

use Yii;
use yii\helpers\ArrayHelper;

class AddressCity extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'address_city';
    }

    public function rules()
    {
        return [
            [['name_city'], 'required'],
            [['name_city'], 'string'],
            [['fk_country'], 'int']
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_city' => Yii::t('app/models', 'Pk City'),
            'name_city' => Yii::t('app/models', 'Name City'),
            'fk_country' => Yii::t('app/models', 'Fk Country'),
        ];
    }

    public function getAddressStreets()
    {
        return $this->hasMany(AddressStreet::className(), ['fk_city' => 'pk_city']);
    }

    public function getFkCountry()
    {
        return $this->hasOne(AddressCountry::className(), ['pk_country' => 'fk_country']);
    }
    
    public function getCrmCompanies()
    {
        return $this->hasMany(CrmCompany::className(), ['fk_address_city' => 'pk_city']);
    }
    
    /*
     * список всех городов
     * $id - получить по ид
     */
    public function getAll($id = null) {
        $all = AddressCity::getDb()->cache(function ($db) {
            return AddressCity::find()->asArray()->all();
        });
        
        $b = ArrayHelper::map($all, 'pk_city', 'name_city');
        if($id !== NULL) return $b[$id];
        return ArrayHelper::merge(['' => \Yii::t('app', 'Not selected')], $b);
        return $b;
    }
}