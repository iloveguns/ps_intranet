<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Create Polling');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Pollings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-polling-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>