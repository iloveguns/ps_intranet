<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Update Polling') . ' : ' . $model->name_polling;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Pollings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_polling];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="api-polling-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>