<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="api-polling-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_polling')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_active')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>