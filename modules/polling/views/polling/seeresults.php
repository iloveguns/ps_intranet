<?php
use app\modules\polling\models\ApiPollingStages;
//yii\helpers\VarDumper::dump($data, 10 ,true);//каша ебаная
$this->title = 'Результаты голосования';
?>
<h1 class="text-center"><?= $this->title ?></h1>

<?php $this->registerJsFile('@web/js/build/chart.min.js');//графики ?>
<?php $this->registerjs('
    var backgroundColor = [
        "#2ecc71",
        "#3498db",
        "#95a5a6",
        "#9b59b6",
        "#f1c40f",
        "#e74c3c",
        "#34495e"
    ];
    ', yii\web\View::POS_END) ?>

<?php foreach ($data as $i => $datum) : ?>
    <h2 class="text-center"><?= $datum['question'] ?></h2>
    
    <?php if($datum['data']['type'] === ApiPollingStages::TYPE_RADIO) : //графики ?>
        <canvas id="chart-<?= $i ?>" height=50></canvas>
        <?php $this->registerjs('
            var data = '. json_encode($datum['data']['count_answers']) .';
            var ctx = document.getElementById("chart-' . $i . '");
            var labels = '. json_encode($datum['data']['preanswers']) .';
            new Chart(ctx, { 
                type: "pie",
                data: {
                    labels: labels,
                    datasets: [{
                        backgroundColor: backgroundColor,
                        data: data
                    }]
                }
            });
            ', yii\web\View::POS_END) ?>
        <?php if(isset($datum['data']['add_answers'])) : ?>
            <h3>Дополнительные ответы:</h3>
            <?php foreach ($datum['data']['add_answers'] as $add_answer) : ?>
                <div class="well"><?= $add_answer ?></div>
            <?php endforeach ?>
        <?php endif ?>
                
    <?php elseif($datum['data']['type'] === ApiPollingStages::TYPE_CHECKBOX) : ?>
        <canvas id="chart-<?= $i ?>" height=50></canvas>
        <?php $this->registerjs('
            var data = '. json_encode($datum['data']['count_answers']) .';
            var ctx = document.getElementById("chart-' . $i . '");
            var labels = '. json_encode($datum['data']['preanswers']) .';
            new Chart(ctx, {
                type: "bar",
                data: {
                    labels: labels,
                    datasets: [{
                        label: "График",
                        data: data,
                        backgroundColor: backgroundColor[0]
                    }]
                }
            });
            ', yii\web\View::POS_END) ?>
        <?php if(isset($datum['data']['add_answers'])) : ?>
            <h3>Дополнительные ответы:</h3>
            <?php foreach ($datum['data']['add_answers'] as $add_answer) : ?>
                <div class="well"><?= $add_answer ?></div>
            <?php endforeach ?>
        <?php endif ?>
                
    <?php elseif($datum['data']['type'] === ApiPollingStages::TYPE_TEXT) : ?>
        не сделано
    <?php elseif($datum['data']['type'] === ApiPollingStages::TYPE_MULTIPLE_TEXT) : ?>
        не сделано
    <?php endif ?>
                
<?php endforeach ?>
