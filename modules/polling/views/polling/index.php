<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::t('app/views', 'Pollings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-polling-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app/views', 'Create Polling'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'name_polling',
                'is_active',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a('Создать этапы', ['/polling/polling-stages/index', 'id' => $model->pk_polling], ['data-pjax' => '']);
                        },
                    ],
                    'template' => '{view} {update} {delete}'
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>