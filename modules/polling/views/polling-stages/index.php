<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
$this->title = Yii::t('app/views', 'Polling Stages') . ' : ' . $model->name_polling;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-polling-stages-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app/views', 'Create Polling Stage'), ['create', 'id' => $model->pk_polling], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'question_stage:ntext',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>