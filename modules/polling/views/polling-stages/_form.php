<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

?>

<div class="api-polling-stages-form">

    <?php $form = ActiveForm::begin(['id' => 'polling-stage-form']); ?>

    <?= $form->field($model, 'question_stage')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fk_option')->widget(Select2::classname(), [
        'data' => $model->getOptionsList(),
        'options' => ['placeholder' => Yii::t('app', 'Select')],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]);?>
    
    <div id="preanswers">
        <?= app\components\WidgetMultipleValuesn::widget([
            'relation' => $model->preanswers,
            'pk_value' => 'pk_preanswer',
            'attrs' => [
                'title_preanswer' => ['type' => 'text'],
            ],
            'id_widget' => 'preanswer_values',
            'model' => $model,
        ]) ?>
    </div>
    
    <div id="preanswers-add-data">
        <?= $form->field($model, 'min_checked', ['options' => ['class' => 'col-md-2 col-lg-2']])->input('number', ['min' => 1, 'step' => 1]) ?>
        <?= $form->field($model, 'max_checked', ['options' => ['class' => 'col-md-2 col-lg-2']])->input('number', ['min' => 1, 'step' => 1]) ?>
    </div>
    <div class="clearfix"></div>
    
    <?= $form->field($model, 'use_textarea')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    
    <?php $this->registerJs("
        $('#apipollingstages-fk_option').change(function(){
            switch(parseInt($(this).val())) {
                case 1://radiobutton
                    $('#preanswers').removeClass('hidden');
                    $('#preanswers-add-data').addClass('hidden');
                    break;
                case 2://checkbox
                    $('#preanswers').removeClass('hidden');
                    $('#preanswers-add-data').removeClass('hidden');
                    break;
                case 3://множ текст поле
                    $('#preanswers').addClass('hidden');
                    $('#preanswers-add-data').addClass('hidden');
                    break;
                case 4://textarea
                    $('#preanswers').addClass('hidden');
                    $('#preanswers-add-data').addClass('hidden');
                    break;
            }
        });
        $('#apipollingstages-fk_option').change();
        
        $('#polling-stage-form').on('beforeSubmit', function (e) {
            if($('#preanswers').hasClass('hidden')) {
                $('#preanswer_values').html('');//любые введенные данные удалить
            }
            if($('#preanswers-add-data').hasClass('hidden')) {
                $('#preanswers-add-data').html('');//дополнительные данные удалить
            }
            return true;
        });
        ", yii\web\View::POS_END) ?>

</div>