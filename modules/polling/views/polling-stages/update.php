<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Update Polling Stage') . ' : ' . $model->pk_stage;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Polling Stages'), 'url' => ['index', 'id' => $model->fk_polling]];
$this->params['breadcrumbs'][] = ['label' => $model->pk_stage];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="api-polling-stages-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>