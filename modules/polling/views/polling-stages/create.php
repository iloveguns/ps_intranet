<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Create Polling Stage');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Polling Stages'), 'url' => ['index', 'id' => $pk_polling]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-polling-stages-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>