<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\polling\models\ApiPollingStages;

$this->title = Yii::t('app/views', 'See Polling Stage');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Polling Stages'), 'url' => ['index', 'id' => $model->fk_polling]];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$moreJs = "";
?>
<div class="api-polling-stages-create" id="polling_root">

    <?php if($admin) : ?>
        <h1><?= Html::encode($this->title) ?></h1>
        <p>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->pk_stage], ['class' => 'btn btn-primary']) ?>
        </p>
        <?php if($previousBtn) echo Html::a(Yii::t('app/views', 'previousBtn'), ['view', 'id' => $previousBtn], ['class' => 'btn btn-success'])//пред этап ?>
        <?php if($nextBtn) echo Html::a(Yii::t('app/views', 'nextBtn'), ['view', 'id' => $nextBtn], ['class' => 'btn btn-success'])//след этап ?>
    <?php endif ?>

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">
                <?= $model->question_stage ?>
            </h3>
        </div>
        <div class="box-body">
            <!-- вывод для просмотра/голосования -->
                <?php
                $modelAnswer = new \app\modules\polling\models\ApiPollingAnswers();
                $modelAnswer->fk_stage = $model->pk_stage;
                $form = ActiveForm::begin(['id' => 'answer-form']); ?>

                    <?php switch ($model->fk_option) {
                        case ApiPollingStages::TYPE_RADIO:
                            $answerHtml = $form->field($modelAnswer, 'answer')->radioList($model->preanswersArr, ['separator' => '<br>']);
                            break;

                        case 2:
                            $answerHtml = $form->field($modelAnswer, 'answer')->checkboxList($model->preanswersArr, ['separator' => '<br>']);
                            break;

                        case 3:
                            $answerHtml = app\components\WidgetMultipleValuesn::widget([
                                'attrs' => [
                                    'answer' => ['type' => 'text'],
                                ],
                                'id' => 'preanswer_values',
                                'model' => $modelAnswer,
                            ]);
                            break;

                        case 4:
                            $answerHtml = $form->field($modelAnswer, 'answer')->textarea();
                            break;
                    }?>

                    <?= Html::label('', '', ['class' => 'hidden', 'id' => 'label-add-data'])//поле для доп информации ?>

                    <?= $answerHtml ?>

                    <?php if($model->use_textarea) ://использовать свободное текстовое поле ?>
                        <?= $form->field($modelAddAnswers, 'text')->textarea() ?>
                    <?php endif ?>

                    <?= $form->field($modelAnswer, 'fk_stage')->hiddenInput()->label(false) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Далее', ['class' => 'btn btn-success']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
            <!-- конец вывод для просмотра/голосования -->
        </div>
    </div>
        
    <?php
    if($model->add_data && $add_data = json_decode($model->add_data, true)) {
        if(empty($add_data['min_checked'])) $add_data['min_checked'] = 1;
        if(empty($add_data['max_checked'])) $add_data['max_checked'] = 0;

        //необходимо опять же передавать каждый раз, а сейчас только в начале есть эта инфа
        $moreJs = "
        var min_checked = " . $add_data['min_checked'] . ",
            max_checked = " . $add_data['max_checked'] . ",
            polling_model_answer = '" . Html::getInputId($modelAnswer, 'answer'). "';
        ";
    }
    //необходимые скрипты
    $this->registerjs('
        var nextPollingStage = "' . $nextBtn . '",
            polling_form_id = "' . $form->id . '",
            polling_html_url = "' . \yii\helpers\Url::to(['/polling/polling-stages/html']) . '",
            api_url = "' . \yii\helpers\Url::to(['/polling/polling/api-info-stage']) . '",//const//url для получения информации
            stage = "' . $model->pk_stage . '",//const//ид этапа
            polling_url = "' . yii\helpers\Url::to(['/polling/polling/save-answers']) . '";
        ' . ApiPollingStages::getOptionsListJs() . $moreJs, yii\web\View::POS_HEAD);
    $this->registerJsFile('@web/js/build/pollings.js', ['depends' => 'yii\jui\JuiAsset']);    
    ?>
    
    <?php if($admin) : ?>
        <!-- вывод данных голосования -->
        <?= Html::button('Показать результаты', ['class' => 'btn btn-primary', 'id' => 'show-results']) ?>
        <div>
            <canvas id="chart" class="hidden"></canvas>

            <div id="text_answer" class="hidden">
                <h3>Ответы:</h3>
                <div id="for_text_answer"></div>
            </div>

            <div id="add_text" class="hidden">
                <h3>Дополнительные ответы:</h3>
                <div id="for_add_text"></div>
            </div>
        </div>
        
        <?php $this->registerJsFile('@web/js/build/chart.min.js');//графики ?>
        <!-- конец вывод данных голосования -->        
    <?php endif ?>

</div>