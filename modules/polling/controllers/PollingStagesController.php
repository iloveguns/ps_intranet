<?php

namespace app\modules\polling\controllers;

use Yii;
use app\modules\polling\models\ApiPollingStages;
use app\modules\polling\models\ApiPollingStagesSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\polling\models\ApiPolling;
use app\modules\polling\models\ApiPollingAddanswers;

class PollingStagesController extends \app\components\Controller
 {
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex($id) {
        $searchModel = new ApiPollingStagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['fk_polling' => $id]);
        
        $model = ApiPolling::findOne($id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }
    
    public function actionView($id) {
        $model = $this->findModel($id);
        
        $nextBtn = ApiPolling::hasNext($model->fk_polling, $id);
        $previousBtn = ApiPolling::hasPrevious($model->fk_polling, $id);
        
        return $this->render('view', [
            'admin' => true,
            'model' => $model,
            'nextBtn' => $nextBtn,
            'previousBtn' => $previousBtn,
            'modelAddAnswers' => new ApiPollingAddanswers()
        ]);
    }
    
    public function actionHtml() {
        $this->jsonResponse();
        
        $post = Yii::$app->request->post();
        if($id = $post['stage']) {
            $model = $this->findModel($id);

            $nextBtn = ApiPolling::hasNext($model->fk_polling, $id);
            $previousBtn = ApiPolling::hasPrevious($model->fk_polling, $id);

            return [
                'html' => $this->renderPartial('view', [
                    'admin' => false,
                    'model' => $model,
                    'nextBtn' => $nextBtn,
                    'previousBtn' => $previousBtn,
                    'modelAddAnswers' => new ApiPollingAddanswers()
                ]),
                'next' => $nextBtn,
            ];
        }
    }

    public function actionCreate($id) {
        $model = new ApiPollingStages();
        $model->fk_polling = $id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'pk_polling' => $id,
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->fk_polling]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);
        $id = $model->fk_polling;//вытащить инфу перед удалением
        $model->delete();

        return $this->redirect(['index', 'id' => $id]);
    }

    protected function findModel($id) {
        if (($model = ApiPollingStages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}