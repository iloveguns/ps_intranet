<?php

namespace app\modules\polling\controllers;

use Yii;
use app\modules\polling\models\ApiPolling;
use app\modules\polling\models\ApiPollingSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class PollingController extends \app\components\Controller
 {
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'save-answers' => ['POST'],
                    'api-info-stage' => ['POST'],
                ],
            ],
        ];
    }
    
    /*
     * просмотр результатов голосования полностью
     */
    public function actionResults($id) {
        $model = $this->findModel($id);
        $data = [];
        
        foreach ($model->stages as $stage) {//получение информации по этапу
            $data[$stage->pk_stage]['question'] = $stage->question_stage;
            $data[$stage->pk_stage]['data'] = ApiPolling::ApiInfoStage($stage->pk_stage);
        }
        
        return $this->render('seeresults', ['data' => $data]);
    }

    public function actionIndex() {
        $searchModel = new ApiPollingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $model = new ApiPolling();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id) {
        if (($model = ApiPolling::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /*
     * сохранение ответов, конечная точка опроса
     */
    public function actionSaveAnswers() {
        $post = Yii::$app->request->post();
        
        $model = new \app\modules\polling\models\ApiPollingAnswers();
        
        $this->jsonResponse();
        
        if($model->load($post)) {
            $model->deleteAll(['fk_user' => Yii::$app->user->id, 'fk_stage' => $model->fk_stage]);//удалить ранее сделанные ответы на этот вопрос
            
            $next = ApiPolling::hasNext(NULL, $model->fk_stage);//есть ли еще этапы
            
            if($model->save()) {                
                return ['success' => true, 'next' => $next];
            } else {
                return ['success' => false, 'errors' => $model->getErrors()];
            }
        }
    }
    
    public function actionApiInfoStage() {
        $post = Yii::$app->request->post();
        
        if(!isset($post['stage'])) throw new NotFoundHttpException();
        
        $this->jsonResponse();

        return ApiPolling::ApiInfoStage($post['stage']);
    }
}