<?php

namespace app\modules\polling;

class PollingModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\polling\controllers';
}
