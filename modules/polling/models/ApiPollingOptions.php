<?php

namespace app\modules\polling\models;

use Yii;
use app\modules\polling\models\ApiPollingStages;

/*
 * Класс для хранения 
 */
class ApiPollingOptions extends \yii\db\ActiveRecord
{
    public static function tableName() {
        return 'api_polling_options';
    }

    public function rules() {
        return [
            [['name_option'], 'required'],
            [['name_option'], 'string', 'max' => 20],
        ];
    }

    public function attributeLabels() {
        return [
            'pk_option' => Yii::t('app/models', 'Pk Option'),
            'name_option' => Yii::t('app/models', 'Name Option'),
        ];
    }

    public function getApiPollingStages() {
        return $this->hasMany(ApiPollingStages::className(), ['fk_option' => 'pk_option']);
    }
}