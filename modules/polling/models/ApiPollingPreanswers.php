<?php

namespace app\modules\polling\models;

use Yii;
use app\modules\polling\models\ApiPollingStages;

/*
 * Класс для возможных ответов на вопрос
 */
class ApiPollingPreanswers extends \yii\db\ActiveRecord
{
    public static function tableName() {
        return 'api_polling_preanswers';
    }

    public function rules() {
        return [
            [['fk_stage'], 'required'],
            [['fk_stage'], 'integer'],
            [['title_preanswer'], 'string'],
            [['fk_stage'], 'exist', 'skipOnError' => true, 'targetClass' => ApiPollingStages::className(), 'targetAttribute' => ['fk_stage' => 'pk_stage']],
        ];
    }

    public function attributeLabels() {
        return [
            'pk_preanswer' => Yii::t('app/models', 'Pk Preanswer'),
            'fk_stage' => Yii::t('app/models', 'Fk Stage'),
            'title_preanswer' => Yii::t('app/models', 'Title Preanswer'),
        ];
    }

    public function getFkStage() {
        return $this->hasOne(ApiPollingStages::className(), ['pk_stage' => 'fk_stage']);
    }
}