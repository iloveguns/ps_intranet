<?php

namespace app\modules\polling\models;

use Yii;
use app\modules\polling\models\ApiPollingStages;
use app\modules\user\models\User;

/*
 * Класс для хранения доп ответов к опросам
 */
class ApiPollingAddanswers extends \yii\db\ActiveRecord
{
    public static function tableName() {
        return 'api_polling_addanswers';
    }

    public function rules() {
        return [
            [['fk_stage', 'fk_user'], 'required'],
            [['fk_stage', 'fk_user'], 'integer'],
            [['text'], 'string'],
            [['fk_stage'], 'exist', 'skipOnError' => true, 'targetClass' => ApiPollingStages::className(), 'targetAttribute' => ['fk_stage' => 'pk_stage']],
            [['fk_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_user' => 'id_user']],
        ];
    }

    public function attributeLabels() {
        return [
            'pk_addanswer' => Yii::t('app/models', 'Pk Addanswer'),
            'fk_stage' => Yii::t('app/models', 'Fk Stage'),
            'fk_user' => Yii::t('app/models', 'Fk User'),
            'text' => Yii::t('app/models', 'Poling Additional Answer'),
        ];
    }

    public function getFkStage() {
        return $this->hasOne(ApiPollingStages::className(), ['pk_stage' => 'fk_stage']);
    }

    public function getFkUser() {
        return $this->hasOne(User::className(), ['id_user' => 'fk_user']);
    }
}