<?php

namespace app\modules\polling\models;

use Yii;
use app\modules\user\models\User;
use app\modules\polling\models\ApiPollingStages;

/*
 * Класс для опросов(самого верхнего уровня)
 */
class ApiPolling extends \yii\db\ActiveRecord
{
    public static function tableName() {
        return 'api_polling';
    }

    public function rules() {
        return [
            [['name_polling'], 'required'],
            [['created_user', 'is_active'], 'integer'],
            [['name_polling'], 'string', 'max' => 50],
            [['created_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_user' => 'id_user']],
        ];
    }

    public function attributeLabels() {
        return [
            'pk_polling' => Yii::t('app/models', 'Pk Polling'),
            'name_polling' => Yii::t('app/models', 'Name Polling'),
            'created_user' => Yii::t('app/models', 'Created User'),
            'is_active' => Yii::t('app/models', 'Is Active'),
        ];
    }

    public function getCreatedUser() {
        return $this->hasOne(User::className(), ['id_user' => 'created_user']);
    }

    public function getStages() {
        return $this->hasMany(ApiPollingStages::className(), ['fk_polling' => 'pk_polling']);
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->created_user = Yii::$app->user->id;
            return true;
        }
        return false;
    }
    
    /*
     * пройдено ли голосование конкретным сотрудником
     */
    public function userHadPolling($id_user = NULL) {
        if(!$id_user) $id_user = Yii::$app->user->id;
        
        $stages = $this->getListPkStages($this->pk_polling);
        
        //определить, на какие из этапов есть ответы
        $answeredStages = ApiPollingAnswers::find()->select('fk_stage')->asArray()->where(['fk_user' => $id_user])->andWhere(['in', 'fk_stage', $stages])->all();
        //отсеивать пройденные, если все пройдены - должен остаться пустой массив $stages
        foreach ($answeredStages as $answeredStage) {
            unset($stages[$answeredStage['fk_stage']]);
        }
        
        if(empty($answeredStages) || !empty($stages)) return false;
        return true;
    }
    
    /*
     * список ид этапов голосования
     * @param {int} $fk_polling - ид голосования
     */
    protected function getListPkStages($fk_polling) {
        $returnArr = [];
        
        $m = ApiPollingStages::find()->select('pk_stage')->asArray()->where(['fk_polling' => $fk_polling])->all();
        
        foreach ($m as $stage) {
            $returnArr[$stage['pk_stage']] = $stage['pk_stage'];
        }
        return $returnArr;
    }
    
    /*
     * список голосований
     */
    public function getPollings($id = NULL) {
        $a = self::find()->where(['created_user' => \Yii::$app->user->id])->all();
        $b = \yii\helpers\ArrayHelper::map($a, 'pk_polling', 'name_polling');
        
        if($id) return $b[$id];
        return $b;
    }
    
    /*
     * получить ид первого этапа голосования
     */
    public function getFirstStage($returnModel = false) {
        if($returnModel) {
            return ApiPollingStages::find()->where(['fk_polling' => $this->pk_polling])->one();
        } else {
            $model = (new \yii\db\Query())->select('pk_stage')->from(ApiPollingStages::tableName())->where(['fk_polling' => $this->pk_polling])->one();
            return ($model) ? $model['pk_stage'] : false;
        }
    }
    
    /*
     * имеет ли текущий этап следующий(для выдачи ссылки на него)
     * @param {int} $fk_polling - ид голосования
     * @param {int} $pk_stage - ид этапа
     */
    public function hasNext($fk_polling = NULL, $pk_stage) {
        if($fk_polling === NULL) {//известен только этап - найти от него голосование
            $m = (new \yii\db\Query())->select('fk_polling')->from(ApiPollingStages::tableName())->where(['pk_stage' => $pk_stage])->one();
            $fk_polling = $m['fk_polling'];
        }
        $model = (new \yii\db\Query())->select('pk_stage')->from(ApiPollingStages::tableName())->where(['fk_polling' => $fk_polling])->andWhere(['>', 'pk_stage', $pk_stage])->one();
        return ($model) ? $model['pk_stage'] : false;
    }
    
    /*
     * имеет ли текущий этап предыдущий(для выдачи ссылки на него)
     * @param {int} $fk_polling - ид голосования
     * @param {int} $pk_stage - ид этапа
     */
    public function hasPrevious($fk_polling, $pk_stage) {
        $model = (new \yii\db\Query())->select('pk_stage')->from(ApiPollingStages::tableName())->where(['fk_polling' => $fk_polling])->andWhere(['<', 'pk_stage', $pk_stage])->orderBy('pk_stage DESC')->one();
        return ($model) ? $model['pk_stage'] : false;
    }
    
    /*
     * получить собранную информацию об этапе опроса
     * @param {int} $pk_stage - ид этапа
     * @return array
     */
    public function ApiInfoStage($pk_stage) {
        $returnArr = [];
        
        if($model = ApiPollingStages::findOne($pk_stage)) {
            $returnArr['type'] = $model->fk_option;
            
            $allCount = ApiPollingAnswers::find()->where(['fk_stage' => $pk_stage])->count();//общее количество
            $returnArr['all_count'] = $allCount;
            
            switch ($model->fk_option) {
                case ApiPollingStages::TYPE_RADIO :
                case ApiPollingStages::TYPE_CHECKBOX :
                    
                    //получить варианты ответов
                    foreach ($model->preanswers as $preanswer) {
                        $returnArr['preanswers'][] = $preanswer->title_preanswer;
                        
                        $count = ApiPollingAnswers::find()->where(['fk_stage' => $pk_stage, 'answer' => $preanswer->pk_preanswer])->count();
                        $returnArr['count_answers'][] = $count;//количественное значение
                    }

                    break;
                
                case ApiPollingStages::TYPE_MULTIPLE_TEXT :
                case ApiPollingStages::TYPE_TEXT :
                    
                    //по каждому конкретному голосовавшему - набор ответов                    
                    foreach (ApiPollingAnswers::find()->select(['answer', 'fk_user'])->asArray()->where(['fk_stage' => $pk_stage])->each() as $answer) {
                        $returnArr['answers'][$answer['fk_user']][] = $answer['answer'];
                    }
                    
                    break;
                    
                default:
                    break;
            }
            
            //дополнительные свободные ответы
            $add_answers = ApiPollingAddanswers::find()->select(['text'])->asArray()->where(['fk_stage' => $pk_stage])->all();

            foreach ($add_answers as $add_answer) {
                $returnArr['add_answers'][] = $add_answer['text'];
            }
        } else {
            return false;
        }
        
        return $returnArr;
    }
}