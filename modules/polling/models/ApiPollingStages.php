<?php

namespace app\modules\polling\models;

use Yii;
use app\modules\polling\models\ApiPolling;
use app\modules\polling\models\ApiPollingOptions;
use app\modules\polling\models\ApiPollingAnswers;

/*
 * Класс для этапов голосования(шагов)
 */
class ApiPollingStages extends \yii\db\ActiveRecord
{
    public $title_preanswer;//возможные ответы
    public $min_checked, $max_checked;
    
    const TYPE_RADIO = 1;//один из вариантов
    const TYPE_CHECKBOX = 2;//множественный из вариантов
    const TYPE_MULTIPLE_TEXT = 3;//множественный текстовый
    const TYPE_TEXT = 4;//одиночный текстовый(textarea)

    public static function tableName() {
        return 'api_polling_stages';
    }

    public function rules() {
        return [
            [['fk_option', 'question_stage'], 'required'],
            [['fk_polling', 'fk_option', 'use_textarea'], 'integer'],
            [['question_stage', 'add_data'], 'string'],
            [['title_preanswer', 'min_checked', 'max_checked'], 'safe'],
            //[['fk_option'], 'exist', 'skipOnError' => true, 'targetClass' => ApiPollingOptions::className(), 'targetAttribute' => ['fk_option' => 'pk_option']],
            [['fk_polling'], 'exist', 'skipOnError' => true, 'targetClass' => ApiPolling::className(), 'targetAttribute' => ['fk_polling' => 'pk_polling']],
        ];
    }

    public function attributeLabels() {
        return [
            'pk_stage' => Yii::t('app/models', 'Pk Stage'),
            'fk_polling' => Yii::t('app/models', 'Fk Polling'),
            'question_stage' => Yii::t('app/models', 'Question Stage'),
            'fk_option' => Yii::t('app/models', 'preanswers type'),
            'min_checked' => 'Минимум выбранных',
            'max_checked' => 'Максимум выбранных',
            'use_textarea' => Yii::t('app/models', 'use additional textarea'),
            'title_preanswer' => Yii::t('app/models', 'preanswers polling stage'),
        ];
    }

    public function getPreanswers() {
        return $this->hasMany(ApiPollingPreanswers::className(), ['fk_stage' => 'pk_stage']);
    }
    
    /*
     * массив для select подготовленных ответов
     */
    public function getPreanswersArr() {
        $a = [];
        
        foreach ($this->preanswers as $preanswer) {
            $a[$preanswer->pk_preanswer] = $preanswer->title_preanswer;
        }
        
        return $a;
    }
    
    public function getApiPollingAnswers() {
        return $this->hasMany(ApiPollingAnswers::className(), ['fk_stage' => 'pk_stage']);
    }

    public function getFkOption() {
        return $this->hasOne(ApiPollingOptions::className(), ['pk_option' => 'fk_option']);
    }

    public function getFkPolling() {
        return $this->hasOne(ApiPolling::className(), ['pk_polling' => 'fk_polling']);
    }
    
    public function getOptionsList($id = NULL) {
        $a = [
            self::TYPE_RADIO => 'Один из',
            self::TYPE_CHECKBOX => 'Checkbox',
            self::TYPE_MULTIPLE_TEXT => 'Множ. свободный ответ',
            self::TYPE_TEXT => 'Свободный ответ',
        ];
        if($id) return $a[$id];
        return $a;
    }
    
    /*
     * типы для использования в js(переменные для определения типа)
     */
    public function getOptionsListJs($id = NULL) {
        $a = [
            self::TYPE_RADIO => 'radio',
            self::TYPE_CHECKBOX => 'checkbox',
            self::TYPE_MULTIPLE_TEXT => 'multiple_text',
            self::TYPE_TEXT => 'textarea',
        ];
        
        $script = '';
        foreach ($a as $id => $value) {
            $script .= 'var stagetype_' . $value . ' = ' . $id . ';';
        }
        return $script;
    }
    
    public function afterFind() {
        if($this->add_data) {
            $e = json_decode($this->add_data, true);
            $this->min_checked = $e['min_checked'];
            $this->max_checked = $e['max_checked'];
        }
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        
        //есть варианты ответов - пройтись по ним
        foreach ($this->title_preanswer as $pk => $preanswer) {
            if(empty($preanswer)) continue;

            //поискать, есть ли такая запись уже
            $findModel = ApiPollingPreanswers::find()->where(['pk_preanswer' => $pk, 'fk_stage' => $this->pk_stage])->one();

            if($findModel) {//есть - обновить,чтобы внешние ключи были постоянными
                $findModel->title_preanswer = $preanswer;
                $findModel->update();
            } else {//нет - создать новую
                $m = new ApiPollingPreanswers();
                $m->fk_stage = $this->pk_stage;
                $m->title_preanswer = $preanswer;
                $m->save();
            }
        }
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {                        
            if($this->min_checked || $this->max_checked) {//доп информация по мин и макс выбору для чекбокса
                $this->add_data = json_encode(['min_checked' => $this->min_checked, 'max_checked' => $this->max_checked]);
            } else {
                $this->add_data = NULL;
            }
            
            return true;
        }
        return false;
    }
}