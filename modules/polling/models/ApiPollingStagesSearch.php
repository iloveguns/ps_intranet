<?php

namespace app\modules\polling\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\polling\models\ApiPollingStages;

class ApiPollingStagesSearch extends ApiPollingStages{
    public function rules()
    {
        return [
            [['pk_stage', 'fk_polling', 'fk_option'], 'integer'],
            [['question_stage'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ApiPollingStages::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'pk_stage' => $this->pk_stage,
            'fk_polling' => $this->fk_polling,
            'fk_option' => $this->fk_option,
        ]);

        $query->andFilterWhere(['like', 'question_stage', $this->question_stage]);

        return $dataProvider;
    }
}