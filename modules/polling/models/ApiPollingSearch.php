<?php

namespace app\modules\polling\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\polling\models\ApiPolling;

class ApiPollingSearch extends ApiPolling{
    public function rules()
    {
        return [
            [['pk_polling', 'created_user', 'is_active'], 'integer'],
            [['name_polling'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ApiPolling::find()->andWhere(['created_user' => Yii::$app->user->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'pk_polling' => $this->pk_polling,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'name_polling', $this->name_polling]);

        return $dataProvider;
    }
}