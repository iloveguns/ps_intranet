<?php

namespace app\modules\polling\models;

use Yii;
use app\modules\user\models\User;
use app\modules\polling\models\ApiPollingStages;

/*
 * Класс для хранения ответов на опросы
 */
class ApiPollingAnswers extends \yii\db\ActiveRecord
{
    public static function tableName() {
        return 'api_polling_answers';
    }

    public function rules() {
        return [
            [['fk_stage', 'answer'], 'required'],
            [['fk_stage', 'fk_user'], 'integer'],
            [['datetime'], 'safe'],
            [['ip_user'], 'string', 'max' => 20],
            [['fk_stage'], 'exist', 'skipOnError' => true, 'targetClass' => ApiPollingStages::className(), 'targetAttribute' => ['fk_stage' => 'pk_stage']],
            [['fk_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_user' => 'id_user']],
        ];
    }

    public function attributeLabels() {
        return [
            'pk_answer' => Yii::t('app/models', 'Pk Answer'),
            'fk_stage' => Yii::t('app/models', 'Fk Stage'),
            'fk_user' => Yii::t('app/models', 'Fk User'),
            'answer' => Yii::t('app/models', 'Poling Answer'),
            'datetime' => Yii::t('app/models', 'Datetime'),
            'ip_user' => Yii::t('app/models', 'Ip User'),
        ];
    }

    public function getFkStage() {
        return $this->hasOne(ApiPollingStages::className(), ['pk_stage' => 'fk_stage']);
    }

    public function getFkUser() {
        return $this->hasOne(User::className(), ['id_user' => 'fk_user']);
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            
            $this->ip_user = Yii::$app->request->userIP;
            $this->fk_user = Yii::$app->user->id;
            
            if(is_array($this->answer)) {//если множественные значения - специфичная обработка
                $insertArray = [];
                
                $count = count(array_count_values($this->answer));
                $i = 0;
                
                foreach ($this->answer as $answer) {
                    ++$i;
                    if(empty($answer)) continue;
                    if($count === $i) {
                        $this->answer = $answer;//последний - сделать значением текущей модели
                    } else {
                        $insertArray[] = [$this->fk_stage, $this->fk_user, $answer, $this->ip_user];
                    }
                }
                
                if($insertArray) {//все остальное вставить пачкой
                    Yii::$app->db->createCommand()->batchInsert($this->tableName(), ['fk_stage', 'fk_user', 'answer', 'ip_user'], $insertArray)->execute();
                }
            }
            
            return true;
        }
        return false;
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        
        //сохранение свободного ответа
        $post = Yii::$app->request->post();
        $model = new ApiPollingAddanswers();
        if($model->load($post)) {
            if(!empty($model->text)) {
                $model->fk_stage = $this->fk_stage;
                $model->fk_user = $this->fk_user;
                if(!$model->save()) {
                    Yii::error('Свободный ответ опросов не сохранился '.implode(',', $model->getErrors()));
                }
            }
        }
    }
}