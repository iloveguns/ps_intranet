<?php
namespace app\modules\intranet\interfaces; 

/*
 * интерфейс, чтоб соблюдать порядок
 * необходимы эти функции тем моделям, кто хочет хранить данные в relation_to_*
 */
interface RelationsUnits {
    /*
     * связь с сотрудниками
     * пример
     * return $this->hasMany(\app\modules\user\models\Organization::className(), ['pk_organization' => 'id_organization'])
             ->viaTable('relation_to_organization', ['item_id' => 'pk_task'], function($query){
                $query->andWhere(['class' => Tasks::className()]);//фильтр для связанной таблицы
            });
     */
    public function getUsers();
    /*
     * связь с отделами
     */
    public function getDepartments();
    /*
     * связь с дивизионами
     */
    public function getDivizions();
    /*
     * связь с организациями
     */
    public function getOrganizations();
}