<?php
namespace app\modules\intranet\interfaces; 

/*
 * интерфейс, чтоб соблюдать порядок
 * необходимы эти функции тем моделям, кто хочет хранить данные в relation_to_*
 */
interface AllInterface {
    /*
     * ссылка на себя
     */
    public function getSelfLink();
    
    /*
     * html ссылка на просмотр сущности
     */
    public function getHtmlLink($icon);
    
    /*
     * чистый url на сущность
     */
    public function getUrl($absolute = false);
    
    /*
     * информация о сущности(для уведомлений например)
     */
    public function getTitleText();
}