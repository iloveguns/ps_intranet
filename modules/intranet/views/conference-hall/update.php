<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use yii\bootstrap\Modal;
use app\modules\intranet\IntranetModule;

?>

<div class="events-company-form">

    <h2><?= IntranetModule::t('all', 'Change Event Company')?> "<?= $model->title?>"</h2>
    
    <?php $form = ActiveForm::begin(['id' => 'events-update']); ?>

    <?= $form->field($model, 'title')->textInput() ?>
    <div class="row">
        <div class="col-md-4">
            <label><?= $model->getAttributeLabel('start_date')?></label>
            <?= DatePicker::widget([
                'id' => 'update_picker_date',
                'name' => 'start_date',
                'type' => DatePicker::TYPE_INPUT,
                'value' => date('Y-m-d',  strtotime($model->start_date)),
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);?>
        </div>
        <div class="col-md-4">
            <label><?= $model->getAttributeLabel('start_date')?></label>
            <?= TimePicker::widget([
                'id' => 'update_picker_start',
                'name' => 'start_time', 
                'value' => date('H:s',  strtotime($model->start_date)),
                'pluginOptions' => [
                    'showMeridian' => false,
                ]
            ]);?>
        </div>
        <div class="col-md-4">
            <label><?= $model->getAttributeLabel('end_date')?></label>
            <?= TimePicker::widget([
                'id' => 'update_picker_end',
                'name' => 'end_time', 
                'value' => date('H:s',  strtotime($model->end_date)),
                'pluginOptions' => [
                    'showMeridian' => false,
                ]
            ]);?>
        </div>
    </div>

    <?= $form->field($model, 'text_event')->textarea(['rows' => 6]) ?>
    
    <?php if(Yii::$app->user->identity->can('change_kz') || Yii::$app->user->identity->isAdmin()) : ?>
        <div class="form-group">
            <?php
            echo Html::a(Yii::t('app', 'Update'), ['conference-hall/update','id' => $model->pk_event], [
                'id' => 'update-event',
                'data-on-done' => 'updateResponse',//js функция после выполнения
                'data-form-id' => 'events-update',//ид формы, к которой привязано submit
                'class' => 'btn btn-primary',
            ]
            );
            $this->registerJs("$('#update-event').click(handleAjaxLink);
                ajaxCallbacks.updateResponse =  function (response) {
                    if(response.success){
                        bootstrapAlert('success','".Yii::t('app/models', 'Success Save')."');
                        $('.modal').modal('hide');
                    }
                    else{
                        bootstrapAlert('danger','".Yii::t('app/models', 'Error Save')."');
                        $('.modal').modal('hide');
                    }
                },", \yii\web\View::POS_END);

            echo Html::a(Yii::t('app', 'Delete'), ['conference-hall/delete','id' => $model->pk_event], [
                'id' => 'delete-event',
                'data-on-done' => 'deleteResponse',//js функция после выполнения
                'data-form-id' => 'events-update',//ид формы, к которой привязано submit
                'class' => 'btn btn-danger',
            ]
            );
            $this->registerJs("$('#delete-event').click(handleAjaxLink);
                ajaxCallbacks.deleteResponse =  function (response) {
                console.log(response);
                    if(response.success){
                        bootstrapAlert('success','".Yii::t('app/models', 'Success Deleted')."');
                        $('.modal').modal('hide');
                    }
                    else{
                        bootstrapAlert('danger','".Yii::t('app/models', 'Error Deleted')."');
                        $('.modal').modal('hide');
                    }
                },", \yii\web\View::POS_END);
            ?>
        </div>
    <?php endif ?>
    
    <?php ActiveForm::end(); ?>
</div>