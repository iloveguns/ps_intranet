<?php
use yii\jui\Draggable;
use yii\bootstrap\Html;
use yii\helpers\Url;
$jsdata = '';
$pk = 0;
if($model != false) $pk = $model->pk_gr;

$this->title = Yii::$app->name;
?>

<div class="panel panel-default">
    <div class="panel-body">
        <?php //echo Html::button(Yii::t('app', 'Save'), ['class' => 'btn btn-primary', 'id' => 'savemap']); ?>
        <?php echo Html::button(Yii::t('app', 'Save'), ['class' => 'btn btn-primary', 'id' => 'savemapdb']); ?>
        <?php //echo Html::button(Yii::t('app', 'Load'), ['class' => 'btn btn-primary', 'id' => 'loadmap']); ?>
    </div>
</div>

<div class="draggable-map" id="draggable-map">
    <div class="jtk-demo-canvas canvas-wide source-target-demo jtk-surface" id="canvas">
        <?php if(!$model) : ?>
            <?php for($i = 1; $i < 100; $i++) : ?>
            <div class="window smallWindow" id="targetWindow<?=$i?>" data-id="<?=$i?>">
                <div class="box box-success">
                    <textarea class="form-control" rows="3" placeholder="текст ..."></textarea>
                   <div id="targetWindow<?=$i?>0" class="batching"></div>
                </div>            
            </div>
            <?php endfor ?>
        <?php endif ?>
    </div>
</div>


<?php 
if($model) $this->registerJs("loadDataToMap('".$model->html_gr."', ".  $model->json_connect_gr.");", yii\web\View::POS_END);

$this->registerJs("
$('#savemapdb').click(function(){
    $.ajax({
        url: '". Url::toRoute('save') ."',
        type: 'post',
        data: {
            json: prepareJson(),
            html: prepareHtml(),
            id: ".$pk.",
        },
        beforeSend : function(){
            gifLoader();
        },
        success: function (data) {
            gifLoader();
            if(data.success == true){
                bootstrapAlert('success', data.message);
            } else {
                bootstrapAlert('danger', data.message);
            }
        }
    })
});
", yii\web\View::POS_END);




$this->registerJsFile('@web/js/draggable-map.js',['depends' => 'yii\jui\JuiAsset','position' => yii\web\View::POS_END]);//тут скрипты

$this->registerCssFile('@web/jsplumb/css/jsPlumbToolkit-demo.css');

$this->registerJsFile('@web/jsplumb/lib/jsBezier-0.8.js',['position' => yii\web\View::POS_END]);
$this->registerJsFile('@web/jsplumb/lib/mottle-0.7.3.js',['position' => yii\web\View::POS_END]);
$this->registerJsFile('@web/jsplumb/lib/biltong-0.3.js',['position' => yii\web\View::POS_END]);
$this->registerJsFile('@web/jsplumb/lib/katavorio-0.17.0.js',['position' => yii\web\View::POS_END]);
$this->registerJsFile('@web/jsplumb/src/util.js',['position' => yii\web\View::POS_END]);
$this->registerJsFile('@web/jsplumb/src/browser-util.js',['position' => yii\web\View::POS_END]);
$this->registerJsFile('@web/jsplumb/src/jsPlumb.js',['position' => yii\web\View::POS_END]);
$this->registerJsFile('@web/jsplumb/src/dom-adapter.js',['position' => yii\web\View::POS_END]);
$this->registerJsFile('@web/jsplumb/src/overlay-component.js',['position' => yii\web\View::POS_END]);
$this->registerJsFile('@web/jsplumb/src/endpoint.js',['position' => yii\web\View::POS_END]);
$this->registerJsFile('@web/jsplumb/src/connection.js',['position' => yii\web\View::POS_END]);
$this->registerJsFile('@web/jsplumb/src/anchors.js',['position' => yii\web\View::POS_END]);
$this->registerJsFile('@web/jsplumb/src/defaults.js',['position' => yii\web\View::POS_END]);
$this->registerJsFile('@web/jsplumb/src/connectors-bezier.js',['position' => yii\web\View::POS_END]);
$this->registerJsFile('@web/jsplumb/src/connectors-statemachine.js',['position' => yii\web\View::POS_END]);
$this->registerJsFile('@web/jsplumb/src/connectors-flowchart.js',['position' => yii\web\View::POS_END]);
$this->registerJsFile('@web/jsplumb/src/renderers-svg.js',['position' => yii\web\View::POS_END]);
$this->registerJsFile('@web/jsplumb/src/base-library-adapter.js',['position' => yii\web\View::POS_END]);
$this->registerJsFile('@web/jsplumb/src/dom.jsPlumb.js',['position' => yii\web\View::POS_END]);
?>