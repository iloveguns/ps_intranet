<?php
use yii\widgets\ListView;
use yii\helpers\Html;

$this->title = 'Графические карты';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="projects-index">
    <h1><?= $this->title ?></h1>
    <p>
        <?= Html::a('Создать', ['view'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_listmap',
    ]);
    ?>
</div>