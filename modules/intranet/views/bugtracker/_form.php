<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

?>

<div class="callboard-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'text')->textarea(['tinymce' => 'tinymce']) ?>
    
    <?= $form->field($model, 'urgency')->widget(Select2::classname(), [
        'data' => $model->getUrgency(),
        'options' => ['placeholder' => Yii::t('app', 'Select')],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]);?>
        
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>