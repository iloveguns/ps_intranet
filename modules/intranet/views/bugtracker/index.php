<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
use app\modules\user\models\User;
use kartik\date\DatePicker;
use app\modules\intranet\models\Bugtracker;
use yii\widgets\Pjax;

$this->title = $text['title'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="callboard-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <p>
        <?= Html::a(Yii::t('app/models', 'I want to report a bug'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php if (isset($text['text'])) : ?>
        <p><?= $text['text'] ?></p>
    <?php endif ?>
    
    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'text',
                    'value' => function ($model, $key, $index, $column) {
                        return Html::a(strip_tags($model->text), ['view', 'id' => $model->pk]);
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'admin_response',
                    'value' => function ($model, $key, $index, $column) {
                        return ($model->admin_response) ? '<span class="text-green">'.Yii::t('app', 'Yes').'</span>' : '<span class="text-red">'.Yii::t('app', 'No').'</span>'; ;
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'admin_response',
                        'data' => ['yes' => Yii::t('app', 'Yes'), 'no' => Yii::t('app', 'No')],
                        'options' => ['placeholder' => ''],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]),
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'urgency',
                    'value' => function ($model, $key, $index, $column) {
                        if($model->urgency)
                        return Bugtracker::getUrgency($model->urgency);
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'urgency',
                        'data' => Bugtracker::getUrgency(),
                        'options' => ['placeholder' => ''],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]),
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'pk_author',
                    'value' => function ($model, $key, $index, $column) {
                        if($model->pk_author){
                            return Html::a(User::getAll($model->pk_author), ['/user/user/profile', 'id' => $model->pk_author], ['target' => '_blank']);
                        } else {
                            return User::getNullUser();
                        }
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'pk_author',
                        'data' => User::getAll(),
                        'options' => ['placeholder' => ''],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]),
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'create_date',
                    'headerOptions' => ['width' => '220px'],
                    'value' => function ($model, $key, $index, $column) {
                        return Yii::$app->formatter->asDateTime($model->create_date);
                    },
                    'filter' => DatePicker::widget([
                        'model' => $searchModel, 
                        'attribute' => 'create_date',
                            'removeButton' => [
                                'icon'=>'remove',
                            ],
                        'options' => ['placeholder' => ''],
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]),
                ],
                [
                    'attribute' => 'done_date',
                    'format' => 'raw',
                    'headerOptions' => ['width' => '220px'],
                    'value' => function ($model, $key, $index, $column) {
                        return Yii::$app->formatter->asDateTime($model->done_date);
                    },
                    'filter' => DatePicker::widget([
                        'model' => $searchModel, 
                        'attribute' => 'done_date',
                            'removeButton' => [
                                'icon'=>'remove',
                            ],
                        'options' => ['placeholder' => ''],
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]),
                ],          
            ],
            'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
            'pager' => [
                'class' => \liyunfang\pager\LinkPager::className(),
                'pageSizeList' => Yii::$app->params['pageSizeList'],
                'pageSizeOptions' => ['class' => 'form-control pagesizeselect'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>  