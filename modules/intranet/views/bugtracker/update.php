<?php

use yii\helpers\Html;

$this->title = $text['update'];
$this->params['breadcrumbs'][] = ['label' => $text['title'], 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="callboard-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>