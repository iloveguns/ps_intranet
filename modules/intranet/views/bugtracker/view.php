<?php

use yii\helpers\Html;
use app\modules\user\models\User;
use kartik\switchinput\SwitchInput;

$this->title = strip_tags($model->pk);
$this->params['breadcrumbs'][] = ['label' => $text['title'], 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row row-cols callboard-view">

    <h1><?= $this->title ?></h1>
    
    <div class="col-md-9 row">
        <?php if(Yii::$app->user->identity->isAdmin() || Yii::$app->user->id == $model->pk_author || in_array(Yii::$app->user->id, $model->check($model->type))) : ?>
        <p>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->pk], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->pk], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <?php endif ?>
        
        <?php if(Yii::$app->user->identity->isAdmin() || in_array(Yii::$app->user->id, $model->check($model->type))) : ?>
            <?= $model->getAttributeLabel('admin_response') ?>
            <?= SwitchInput::widget([
                'name' => $model->admin_response || 0,
                'value' => $model->admin_response, 
                'pluginOptions' => [
                    'onText' => Yii::t('app', 'Yes'),
                    'offText' => Yii::t('app', 'No'),
                ],
                'pluginEvents' => [
                    "switchChange.bootstrapSwitch" => "function(event, state) { changeDone(state); }",
                ]
            ]);
            $this->registerJs("
                function changeDone(value){
                    $.ajax({
                        url: '".  \yii\helpers\Url::toRoute('/bugtracker/change-watch')."',
                        type: 'post',
                        data: {
                            item_id: $model->pk,
                            value: value,
                        },
                        success: function (data) {
                            if(data.success){
                                jsNotify('', 'Успешно');
                            } else {
                                jsNotify('','Проблемы', '', 'danger');
                            }
                        }
                    });
                }
            ", yii\web\View::POS_END);
        ?>
        <?php endif ?>
        
        <?php if($model->urgency) : ?>
            <strong>
                <?= $model->getAttributeLabel('urgency') ?> : <?= $model->getUrgency($model->urgency) ?>
            </strong>
        <?php endif ?>

        <div>
            <div class="well">
                <div>
                    <i class="fa fa-calendar"></i> <?=Yii::$app->formatter->asDatetime($model->create_date)?>
                </div>
                <div class="text-from-redactor">
                    <?= $model->text?>
                </div>
                <div>
                    <p class="pull-right">
                        <strong><?=$model->getAttributeLabel('pk_author')?>: </strong><?= ($model->pkAuthor === NULL) ? User::getNullUser() : $model->pkAuthor->getLinkToProfile($model->pkAuthor->fullName)?>
                    </p>
                </div>
            </div>
        </div>
        <?= app\components\WidgetComments::widget(['modelEntity' => $model])?>
    </div>
    <div class="col-md-3">
        <?= $this->render(Yii::$app->params['themeFolder'].'/_rsidebar', ['model' => $model]) ?>
    </div
</div>