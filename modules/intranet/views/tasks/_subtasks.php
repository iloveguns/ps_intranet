<?php
use yii\helpers\Html;
use app\models\EntityUserDone;
use yii\widgets\ActiveForm;

$extraJSDone = $extraJSDelete = '';//дополнительный js

/*
 * вывод таблицы исполнителей задач + выставление выполнения и коммент
 */
?>
<table class="table table-striped">
    <th>Подзадача</th>
    <th><?= Yii::t('app/models', 'Status')?></th>
    <th>Выполнено</th>
    <?php foreach ($model->subTasks as $subtask) : ?>
        <?php $key = Yii::$app->security->generateRandomString(10);//брать форму по ключу, а не по ид сотрудника(может быть одинаков сотрудник) ?>
            <?php $doneInfo = $model->idHead->getSubTaskUserInfo($subtask); //есть ли записи о выполнении задачи?>
            <tr>
                <td id="td-text-<?= $key ?>" style="<?= ($doneInfo['complete']) ? 'text-decoration:line-through' : '' ?>">
                    <?= $subtask->text ?>
                </td>
                <td id="td-a-<?= $key ?>">
                    <?php if($model->isHead() || $model->isCheck() || $model->isAuthor()) : //только чел из троицы может завершать подзадачи ?>
                        <?php $modelEntity = new EntityUserDone(); $form = ActiveForm::begin(['id' => 'task-comment-'.$key]) //форма открывается?>
                        <?= $form->field($modelEntity, "complete", ['template'=>'{input}'])->hiddenInput(['value' => $doneInfo['complete']]) ?>
                        <?= $form->field($modelEntity, "item_id", ['template'=>'{input}'])->hiddenInput(['value' => $subtask->pk_subtask]) ?>
                        <?= $form->field($modelEntity, "id_done", ['template'=>'{input}'])->hiddenInput(['value' => $doneInfo['id_done']]) ?>
                        <?= $form->field($modelEntity, "id_user", ['template'=>'{input}'])->hiddenInput(['value' => Yii::$app->user->id]) ?>
                        <?= $form->field($modelEntity, "class", ['template'=>'{input}'])->hiddenInput(['value' => $subtask::getName()]) ?>                
                        <?php ActiveForm::end(); //форма закрывется?>
                        <?php $aclass = ($doneInfo['complete']) ? 'btn-danger' : 'btn-success' ?>
                        <?= Html::a(($doneInfo['complete']) ? 'X' : '&#10003;', ['/user-done/executer-complete'], [//кнопка
                            'data-on-done' => 'executerComplete',//js функция после выполнения
                            'data-form-id' => 'task-comment-'.$key,//ид формы, к которой привязано submit
                            'user-id' => Yii::$app->user->id,
                            'key' => $key,
                            'class' => 'btn executer-complete '. $aclass,
                        ]); ?>
                    <?php endif ?>
                </td>
                <td id="td-date-<?= $key ?>">
                    <?php if($doneInfo['complete']) {
                        echo Yii::$app->formatter->asDatetime($doneInfo['date']) . ' (' . app\modules\user\models\User::getAll($doneInfo['id_user'], '{lastname} {name}') . ')';
                    }?>
                </td>
            </tr>
    <?php endforeach ?>
</table>

<?php            
$this->registerJs("
window.idform = undefined;//обнулить
$('.executer-complete').click(handleAjaxLink);//ajax функции

$('.executer-complete').click(function(){
    window.idform = $(this).attr('key');
});//временно сохранить id

ajaxCallbacks.executerComplete = function (response) {
    if(response.success){
        if(response.mode == 'save'){//менять функционал(данные) кнопки
            $('#task-comment-'+window.idform+' #entityuserdone-id_done').val(response.id);
            $('#task-comment-'+window.idform+' #entityuserdone-complete').val(1);
            $('#td-a-'+window.idform+' .executer-complete').html('X');
            $('#td-text-'+window.idform).css({ 'text-decoration' : 'line-through' });
            $('#td-a-'+window.idform+' .executer-complete').removeClass('btn-success').addClass('btn-danger');
            $('#td-form-'+window.idform+' .task-text').html(response.text);
            $('#td-form-'+window.idform+' .field-entityuserdone-text').addClass('hidden');
            $('#td-date-'+window.idform).html(response.date + ' (' + response.user + ')');
        }
        else if(response.mode == 'delete'){
            $('#task-comment-'+window.idform+' #entityuserdone-id_done').val('');
            $('#task-comment-'+window.idform+' #entityuserdone-complete').val('');            
            $('#td-a-'+window.idform+' .executer-complete').html('&#10003;');
            $('#td-text-'+window.idform).css({ 'text-decoration' : 'inherit' });
            $('#td-a-'+window.idform+' .executer-complete').removeClass('btn-danger').addClass('btn-success');
            $('#td-form-'+window.idform+' .task-text').html('');
            $('#td-form-'+window.idform+' .field-entityuserdone-text').removeClass('hidden');
            $('#td-date-'+window.idform).html('');
        }
    } else {
        var text = '';
        $.each( response.data, function( index, value ){
            text += value+'<br>';
        });
        bootstrapAlert('danger',text);
    }
};", \yii\web\View::POS_END);
?>