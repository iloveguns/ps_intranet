<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\user\models\User;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use app\components\WidgetStructure;
use kartik\datetime\DateTimePicker;
use unclead\widgets\MultipleInput;
use yii\helpers\Url;

if($model->isNewRecord) $model->start_date = \app\models\FunctionModel::getDateTimestamp();//новая задача - время текущее
?>

<div class="tasks-form">

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <?= $form->field($model, 'quest')->checkbox() ?>
    
    <?= $form->field($model, 'title')->textInput() ?>
    
    <?= $form->field($model, 'id_head')->widget(Select2::classname(), [
        'data' => User::getAllActive(),
        'options' => ['placeholder' => Yii::t('app', 'Select')],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]);?>
    
    <?= $form->field($model, 'id_check')->widget(Select2::classname(), [
        'data' => User::getAllActive(),
        'options' => ['placeholder' => Yii::t('app', 'Select')],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]);?>

    <?= $form->field($model, 'content')->textarea(['tinymce' => 'tinymce']) ?>
    
    <div>
        <?php
        if(!$model->isNewRecord){
            if($model->subTasks){
                $jsCode = "$('#subtasks-text').find($('.multiple-input-list__item')).first().remove();";//удалить первую строку
                
                $modelSubTask = $modelSubTask->findOne(['fk_task' => $model->pk_task]);//1 элемент ставится нормально
                
                foreach ($model->subTasks as $subtask) {//добавить строки вручную
                    $jsCode .= "$('#subtasks-text table tbody').append('<tr class=\"multiple-input-list__item\"><td class=\"list-cell__text\"><div class=\"form-group field-subtasks-text-1 has-success\"><input type=\"text\" id=\"subtasks-text-$subtask->pk_subtask\" class=\"form-control\" name=\"Subtasks[text][$subtask->pk_subtask]\" value=\"".$subtask->text."\"></div></td><td class=\"list-cell__button\"><div class=\"btn multiple-input-list__btn js-input-remove btn btn-danger\"><i class=\"glyphicon glyphicon-remove\"></i></div></td></tr>');";
                }
                $this->registerJs($jsCode, \yii\web\View::POS_END);
            }
        }
        
        echo $form->field($modelSubTask, 'text')->widget(MultipleInput::className(), [
            'limit'             => 100,
            'allowEmptyList'    => true,
            'enableGuessTitle'  => true,
            'min'               => ($model->isNewRecord) ? 1 : 0,
            'addButtonPosition' => MultipleInput::POS_HEADER
        ])->label(false);
        ?>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <?= $form->field($model, 'start_date', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => Yii::t('app','Enter date')],
                'removeButton' => false,
                'pluginOptions' => [
                    'weekStart' => 1,
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd hh:ii:ss'
                ]
            ]);    
            ?>
            
            <?= $form->field($model, 'end_date', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => Yii::t('app','Enter date')],
                'removeButton' => false,
                'pluginOptions' => [
                    'weekStart' => 1,
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd hh:ii:ss'
                ]
            ]);   
            ?>
        </div>
    </div>

    <div class="panel panel-default">
        <?= WidgetStructure::widget(['model' => $model]) ?>
    </div>
    
    <div>
        <?= $form->field($model, 'direction')->widget(Select2::classname(), [
            'data' => app\modules\intranet\models\ProjectDirections::getDirections(),
            'options' => ['placeholder' => Yii::t('app', 'Select')],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]) ?>
    </div>
    
    <div>
        <?= $form->field($model, 'project_id')->widget(DepDrop::classname(), [
            'data' => \app\modules\user\models\User::getAllDropListEntityUser(app\modules\intranet\models\Projects::getName()),
            'options' => ['placeholder' => Yii::t('app', 'Select')],
            'pluginOptions' => [
                'depends'=>['tasks-direction'],
                'placeholder'=>'Выберите',
                'url' => Url::to(['projects/getbydirection'])
            ],
        ]) ?>
    </div>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php $this->registerJs("
        $('#".$model->formName()."').on('beforeSubmit', function (e) {
            gifLoader();
            return true;
        });
    ", yii\web\View::POS_READY); ?>

</div>