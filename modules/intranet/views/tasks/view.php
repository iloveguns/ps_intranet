<?php

use yii\helpers\Html;
use app\modules\intranet\IntranetModule;
use app\modules\user\models\User;
use yii\bootstrap\Tabs;
use kartik\select2\Select2;
use app\modules\intranet\models\Tasks;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use yii\bootstrap\Modal;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Tasks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row row-cols tasks-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-md-9">
        <?php if(Yii::$app->user->identity->isAdmin() || $model->canAdmin(Yii::$app->user->id)) : ?>
        <p>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->pk_task], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->pk_task], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <?php endif ?>
        
        <div class="alert alert-info col-md-6">
            <ul>
                <li><?=$model->getAttributeLabel('create_date')?>: <?=Yii::$app->formatter->asDatetime($model->create_date)?></li>
                <li><?=$model->getAttributeLabel('start_date')?>: <?=Yii::$app->formatter->asDatetime($model->start_date)?></li>
                <li><?=$model->getAttributeLabel('end_date')?>: <?=Yii::$app->formatter->asDatetime($model->end_date)?></li>
                <li><?=IntranetModule::t('all', 'Duration')?>: 
                    <?php
                    $datetimeFrom = date_create($model->end_date);
                    $datetimeTo = date_create($model->start_date);
                    echo $datetimeFrom->diff( $datetimeTo )->days;
                    ?>дн.
                </li>
                <?php if($model->update_date) : ?><li><?=$model->getAttributeLabel('update_date')?>: <?=Yii::$app->formatter->asDatetime($model->update_date)?></li><?php endif ?>
            </ul>    
        </div>

        <div class="col-md-6">        
            <?php if($model->end_date !== NULL && time() > strtotime($model->end_date)) : ?>
                <div class="alert alert-warning"><?= Yii::t('app/models', 'Task time out')?></div>
            <?php endif ?>
            <div class="alert <?= ($model->status === $model::STATUS_DONE) ? 'alert-success' : 'alert-info'?>"><?= $model->getStatus($model->status)?></div>

            <?php if($model->project_id) : ?>
                <div class="alert alert-success">            
                    <em><?= IntranetModule::t('all', 'The task is part of a project')?>: <?= Html::a($model->project->title, ['projects/view', 'id' => $model->project_id])?></em>
                </div>
            <?php endif ?>
        </div>
        <div class="clearfix"></div>
        
        <div>
            <p><strong><?=$model->getAttributeLabel('id_author')?>: </strong><?= $model->idAuthor->getLinkToProfile($model->idAuthor->fullName)?></p>
            <p><strong><?=$model->getAttributeLabel('id_head')?>: </strong><?= $model->idHead->getLinkToProfile($model->idHead->fullName)?></p>
            <p><strong><?=$model->getAttributeLabel('id_check')?>: </strong><?= $model->idCheck->getLinkToProfile($model->idCheck->fullName)?></p>
        </div>
        
        <div class="mb-1em">
            <div class="mb-1em">
                <h3 class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <?= IntranetModule::t('all', 'Report')?>
                </h3>
            </div>
            <div class="collapse" id="collapseExample">
                <div class="well">
                    <?php if($reports = $model->reports) :
                        $fe = $reports[0];
                        foreach ($reports as $report) : ?>
                            <div class="entity-report-oneview">
                                <div>
                                    <?=Yii::$app->formatter->asDatetime($report->date)?>
                                    <?php if($model->ishead(Yii::$app->user->id) && $model->status != Tasks::STATUS_DONE && $model->status != Tasks::STATUS_DONEBYADMIN && $report->id_report === $fe->id_report) 
                                        echo Html::button('<i class="fa fa-pencil"></i> Редактировать отчет', ['id' => 'modalReportUpdateText', 'class' => 'btn btn-primary showModalReport', 'data-id' => $report->id_report]);
                                    ?>
                                </div>
                                <div>
                                    <div id="r-<?= $report->id_report ?>">
                                        <?=$report->text_report?>
                                    </div>
                                    <?php if($report->text_deniedreport) : ?>
                                        <blockquote>
                                            <?=$report->text_deniedreport?>
                                        </blockquote>
                                    <?php endif ?>
                                </div>
                            </div>
                        <?php endforeach;
                    endif;

                    //вполне можно делать виджет
                    echo Html::beginTag('div', ['class' => 'hidden']);

                    $modelEntityReport = new \app\models\EntityReport();
                    $form = ActiveForm::begin(['id' => 'entityReportForm']);

                    echo $form->field($modelEntityReport, 'text_report')->widget(\yii\redactor\widgets\Redactor::className());
                    echo $form->field($modelEntityReport, 'text_deniedreport')->widget(\yii\redactor\widgets\Redactor::className());//скрывать по необходимости

                    echo $form->field($modelEntityReport, 'item_id')->hiddenInput(['value' => $model->pk_task])->label(false);
                    echo $form->field($modelEntityReport, 'class')->hiddenInput(['value' => $model::getName()])->label(false);

                    echo Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-primary']);
                    ActiveForm::end();

                    echo Html::endTag('div');
                                            
                    if(($model->isCheck(Yii::$app->user->id) || $model->ishead(Yii::$app->user->id) || $model->canAdmin(Yii::$app->user->id)) && $model->status != Tasks::STATUS_DONEBYADMIN && $model->status != Tasks::STATUS_DONE){//автор или ответственный
                        $js = "
                            //открыть модальное окно
                            $('.showModalReport').click(function(){
                                $('#entityReportForm').appendTo('#myModal .modal-body');
                                $('#myModal').modal('toggle');
                            });
                            
                            $('#modalReportDeniedText').click(function(){
                                $('.field-entityreport-text_report').remove();
                                $('#entityReportForm').attr('scenario','deniedtext');
                            });

                            //убрать лишнее поле ввода и форме повесить атрибут
                            $('#modalReportUpdateText').click(function(){
                                $('.field-entityreport-text_report .redactor-editor').html($('#r-' + $(this).attr('data-id')).html());
                                $('.field-entityreport-text_deniedreport').remove();
                                $('#entityReportForm').attr('scenario','updatetext');
                            });
                            $('#modalReportText').click(function(){
                                $('.field-entityreport-text_deniedreport').remove();
                                $('#entityReportForm').attr('scenario','text');
                            });

                            $('#entityReportForm').on('beforeSubmit', function (event, messages) {
                                //маленько поджопно сделано
                                if($('#entityReportForm').attr('scenario') == 'text' && $.trim($('#entityreport-text_report').val()) == ''){
                                    $('#entityreport-text_report').parent().find($('.help-block')).html('Необходимо заполнить «".$modelEntityReport->getAttributeLabel('text_report')."».');
                                    $('#entityreport-text_report').parent().addClass('has-error');
                                    return false;
                                }
                                if($('#entityReportForm').attr('scenario') == 'deniedtext' && $.trim($('#entityreport-text_deniedreport').val()) == ''){
                                    $('#entityreport-text_deniedreport').parent().find($('.help-block')).html('Необходимо заполнить «".$modelEntityReport->getAttributeLabel('text_deniedreport')."».');
                                    $('#entityreport-text_deniedreport').parent().addClass('has-error');
                                    return false;
                                }

                                let datar = $('#entityReportForm').serializeArray();
                                datar.push({name: 'scenario', value: $('#entityReportForm').attr('scenario')});
                                $.ajax({
                                    url: '".\yii\helpers\Url::toRoute('/report/save-report')."',
                                    type: 'post',
                                    dataType: 'json',
                                    data:  datar,
                                    success: function (response) {
                                        if(response.success == true){
                                            location.reload();
                                        }
                                        else{
                                            var text = '';
                                            $.each( response.data, function( index, value ){
                                                text+=value+'<br>';
                                            });
                                            $('#myModal').modal('toggle');
                                            bootstrapAlert('danger',text);
                                        }
                                    }
                                });
                                return false;
                            });
                        ";
                        $this->registerJs($js, yii\web\View::POS_READY);
                    }
                    ?>                
                </div>
            </div>
            <?php if($model->ishead(Yii::$app->user->id) && $model->status != Tasks::STATUS_REPORT && $model->status != Tasks::STATUS_DONE && $model->status != Tasks::STATUS_DONEBYADMIN){
                    //написать отчет
                    echo Html::button('<i class="fa fa-pencil"></i> '.Yii::t('app/models', 'Write report'), ['id' => 'modalReportText', 'class' => 'btn btn-primary showModalReport']);
                }

                if(($model->isCheck(Yii::$app->user->id) && $model->status == Tasks::STATUS_DONE) || $model->status == Tasks::STATUS_DONEBYADMIN){//если завершена - кнопка отменить завершение(статус в работе)
                    echo Html::a('<i class="fa fa-times"></i> '.Yii::t('app/models', 'Task status cancel to work'), ['tasks/change-status', 'idTask' => $model->pk_task, 'idStatus' => Tasks::STATUS_WORK], [
                        'data-on-done' => 'changeStatus',
                        'class' => 'btn btn-primary ml-1em change-status',
                    ]);
                }

                if(Yii::$app->user->identity->isAdmin() && $model->status != Tasks::STATUS_DONEBYADMIN && $model->status != Tasks::STATUS_DONE){//если админ - кнопка завершить принудительно
                    echo Html::a('<i class="fa fa-hand-paper-o"></i> '.Yii::t('app/models', 'Task cancelability'), ['tasks/change-status', 'idTask' => $model->pk_task, 'idStatus' => Tasks::STATUS_DONEBYADMIN], [
                        'data-on-done' => 'changeStatus',
                        'class' => 'btn btn-danger ml-1em change-status',
                    ]);
                }

                if($model->isCheck(Yii::$app->user->id)){
                    if($model->status == Tasks::STATUS_REPORT){//есть отчет - кнопка отклонить отчет и завершить задачу
                        //продлить
                        echo Html::button('<i class="fa fa-repeat"></i> '.Yii::t('app/models', 'Prolong task'), ['class' => 'btn btn-primary ml-1em showModalDateTask']);

                        //отклонить отчет 
                        echo Html::button('<i class="fa fa-exclamation-triangle"></i> '.Yii::t('app/models', 'Cancel report'), ['id' => 'modalReportDeniedText', 'class' => 'btn btn-warning ml-1em showModalReport']);

                        //выполнено
                        echo Html::a('<i class="fa fa-check"></i> '.Yii::t('app/models', 'Task done'), ['tasks/change-status', 'idTask' => $model->pk_task, 'idStatus' => Tasks::STATUS_DONE], [
                            'data-on-done' => 'changeStatus',
                            'class' => 'btn btn-success ml-1em change-status',
                        ]);

                        //провалено
                        echo Html::a('<i class="fa fa-times"></i> '.Yii::t('app/models', 'Task fail'), ['tasks/change-status', 'idTask' => $model->pk_task, 'idStatus' => Tasks::STATUS_FAIL], [
                            'data-on-done' => 'changeStatus',
                            'class' => 'btn btn-danger ml-1em change-status',
                        ]);

                        Modal::begin([
                            'id' => 'newDateTaskModal',
                            'size' => 'modal-lg',
                        ]);

                        $form = ActiveForm::begin(['id' => 'newDateTaskForm']);

                        echo $form->field($model, 'end_date')->widget(DateTimePicker::classname(), [
                            'options' => ['placeholder' => Yii::t('app','Enter date')],
                            'pluginOptions' => [
                                'weekStart' => 1,
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd hh:ii:ss'
                            ]
                        ]);
                        echo Html::hiddenInput('prolongDate', true);
                        echo Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-primary']);

                        ActiveForm::end();

                        Modal::end();
                        $this->registerJs("
                            //открыть модальное окно
                            $('.showModalDateTask').click(function(){
                                $('#newDateTaskModal').modal('toggle');
                            });
                        ", \yii\web\View::POS_END);
                    }
                }
                
                //кнопка передать задачу(авторство) только автору или админ
                if($model->isAuthor() || Yii::$app->user->identity->isAdmin()) {
                    echo Html::button('<i class="fa fa-heartbeat"></i> '.Yii::t('app/models', 'Change author task'), ['class' => 'btn btn-default ml-1em showChangeAuthorTask']);
                    
                    Modal::begin([
                        'id' => 'showChangeAuthorTask',
                        'size' => 'modal-sm',
                        'header' => '<h4>' . Yii::t('app/models', 'Change author task') . '</h4>',
                        'options' => [
                            'tabindex' => false
                        ],
                    ]);
                    
                    $form = ActiveForm::begin(['id' => 'showChangeAuthorTaskForm']);
                    
                    echo $form->field($model, 'id_author')->widget(Select2::classname(), [
                        'data' => User::getAllActive(),
                        'options' => ['placeholder' => Yii::t('app', 'Select')],
                        'pluginOptions' => [
                            'allowClear' => false,
                        ],
                    ]);
                    
                    echo Html::submitButton('Передать', ['class' => 'btn btn-primary', 'id' => 'change_author']);
                    
                    ActiveForm::end();
                    
                    Modal::end();
                    
                    $this->registerJs("
                        //открыть модальное окно
                        $('.showChangeAuthorTask').click(function(){
                            $('#showChangeAuthorTask').modal('toggle');
                        });
                        
                        //смена
                        $('#change_author').click(function(){                            
                            $.ajax({
                                url: '".\yii\helpers\Url::toRoute('/report/task-change-author')."',
                                type: 'post',
                                dataType: 'json',
                                data:  {
                                    pk_task: " . $model->pk_task . ",
                                    id_author: $('#tasks-id_author').val()
                                },
                                success: function (response) {
                                    if(response.success === true) {
                                        alert('Автор сменен');
                                        location.reload();
                                    } else {
                                        alert('Проблемы смены автора');
                                    }
                                }
                            });
                            return false;
                        });
                    ", \yii\web\View::POS_END);
                }
                
                $this->registerJs("
                $('.change-status').click(handleAjaxLink);
                ajaxCallbacks.changeStatus = function (response) {
                    if(response.success){
                        location.reload();
                    } else{
                        bootstrapAlert('danger',response.data);
                    }
                };", \yii\web\View::POS_END);
            ?>
        </div>
        
        <div>
            <?= Tabs::widget([
                'items' => [
                    [
                        'label' => $model->getAttributeLabel('content'),
                        'content' => '<div class="well">'.$model->content.'</div>',
                        'active' => true
                    ],
                    [
                        'label' => Yii::t('app/models', 'Executors'),
                        'content' => $this->render('_executors', ['model' => $model]),                        
                    ]
                ],
            ]) ?>
        </div>
        
        <?php if($model->subTasks) : ?>
            <hr>
            <h4>Подзадачи</h4>
            <?= $this->render('_subtasks', ['model' => $model]) ?>
        <?php endif ?>
        <hr>
        <?= app\components\WidgetComments::widget(['modelEntity' => $model])?>
    </div>
    <div class="col-md-3">
        <?= $this->render(Yii::$app->params['themeFolder'].'/_rsidebar', ['model' => $model]) ?>
    </div>

</div>