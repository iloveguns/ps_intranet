<?php

use yii\helpers\Html;

$this->title = Yii::t('app/models', 'Update Task') .' '. $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Tasks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->pk_task]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tasks-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelSubTask' => $modelSubTask,
    ]) ?>

</div>