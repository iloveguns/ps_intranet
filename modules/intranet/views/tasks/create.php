<?php

use yii\helpers\Html;

$this->title = Yii::t('app/models', 'Create Tasks');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Tasks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tasks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelSubTask' => $modelSubTask,
    ]) ?>

</div>