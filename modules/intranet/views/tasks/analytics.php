<?php
use kartik\select2\Select2;
use yii\helpers\Html;
?>
<div class="row">
    <div class="col-md-12 col-lg-6 row">
        <div class="col-md-12">
            <div class="box box-widget" id="widget-searchone">
                <div class="box-header with-border">
                    <div class="user-block">
                        Статистика по сотруднику
                    </div>
                </div>
                <div class="box-body">
                    <?php 
                    $month = (isset($post['searchone']['month'])) ? $post['searchone']['month'] : date('n');
                    $year = (isset($post['searchone']['year'])) ? $post['searchone']['year'] : date('Y');
                    $user = (isset($post['searchone']['user'])) ? $post['searchone']['user'] : Yii::$app->user->id;
                    ?>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Месяц</label>
                            <?= Select2::widget([
                                'name' => 'searchone[month]',
                                'data' => $months,
                                'value' => $month,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Select'),
                                ],
                            ]); ?>
                        </div>
                        <div class="col-md-2">
                            <label>Год</label>
                            <?= Select2::widget([
                                'name' => 'searchone[year]',
                                'data' => $years,
                                'value' => $year,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Select'),
                                ],
                            ]); ?>
                        </div>
                        <div class="col-md-4">
                            <label>Сотрудник</label>
                            <?= Select2::widget([
                                'name' => 'searchone[user]',
                                'data' => \app\modules\user\models\User::getAllActive(),
                                'value' => $user,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Select'),
                                ],
                            ]); ?>
                        </div>
                        <div class="col-md-3">
                            <?= Html::label('&nbsp;') ?><br>
                            <?= Html::submitButton('Обновить', ['class' => 'btn btn-primary', 'id' => 'searchone']) ?>
                        </div>
                    </div>
                    <div class="box-footer box-comments mt-1em">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
$this->registerJs("
    $('#searchone').click(function(){
        var month = document.getElementsByName('searchone[month]')[0].value,
            year = document.getElementsByName('searchone[year]')[0].value,
            user = document.getElementsByName('searchone[user]')[0].value;
        

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '".\yii\helpers\Url::to(['/intranet/tasks/analytics'])."',
            data: { month: month, year: year, user: user },
            success: function(data){
                $('#widget-searchone .box-footer').html('');
                $('#widget-searchone .box-footer').append('<div class=\"box-comment comment-wrote\"><div class=\"comment-text ml-0\"><strong>Поставлено:</strong><span class=\"pull-right\">' + data.created + '</span></div></div>');
                $('#widget-searchone .box-footer').append('<div class=\"box-comment comment-wrote\"><div class=\"comment-text ml-0\"><strong>Выполнено:</strong><span class=\"pull-right\">' + data.done + '</span></div></div>');
                $('#widget-searchone .box-footer').append('<div class=\"box-comment comment-wrote\"><div class=\"comment-text ml-0\"><strong>Провалено:</strong><span class=\"pull-right\">' + data.failed + '</span></div></div>');
                $('#widget-searchone .box-footer').append('<div class=\"box-comment comment-wrote\"><div class=\"comment-text ml-0\"><strong>В процессе:</strong><span class=\"pull-right\">' + data.work + '</span></div></div>');
                $('#widget-searchone .box-footer').append('<div class=\"box-comment comment-wrote\"><div class=\"comment-text ml-0\"><strong>Подтвержденные отчеты:</strong><span class=\"pull-right\">' + data.done_reports + '</span></div></div>');
            }
        });
    });
", yii\web\View::POS_END);
?>