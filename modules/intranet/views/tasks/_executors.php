<?php
use app\modules\user\models\Units;
use yii\helpers\Html;
use kartik\switchinput\SwitchInput;

$canadmin = Yii::$app->user->identity->isAdmin() || $model->canAdmin(Yii::$app->user->id) || $model->ishead(Yii::$app->user->id);
/*
 * вывод таблицы исполнителей задач + выставление выполнения и коммент + удаление сотрудника из исполнителей
 */
?>
<table class="table table-striped">
    <th><?= Yii::t('app/models', 'User initials')?></th>
    <?php if($canadmin) : ?>
    <th><?= Yii::t('app/models', 'Remove From Task User')?></th>
    <?php endif ?>
    <th><?= Yii::t('app/models', 'Watchers Entity')?></th>
    <?php foreach (Units::onlyIdUsers($model) as $user) : ?>
        <?php //$taskDoneInfo = $user->getTaskUserInfo($model); //есть ли записи о выполнении задачи?>
        <?php $watcher = $user->isWatcher($model->pk_task, $model->getName()) ?>
        <tr>
            <td>
                <?= $user->miniInfo()?>
            </td>
            <?php if($canadmin) : ?>
            <td>
                <?= Html::a(Yii::t('app/models', 'Delete'), ['tasks/remove-user-fromtask', 'idTask' => $model->pk_task, 'iduser' => $user->id_user], [
                    'data-on-done' => 'removeuserfromtask',
                    'data-id_remove' => $user->id_user,
                    'class' => 'btn btn-danger remove-user-fromtask',
                ]);
                $this->registerJs("
                    $('.remove-user-fromtask').click(handleAjaxLink);
                    ajaxCallbacks.removeuserfromtask = function (response) {
                        if(response.success){
                            bootstrapAlert('success',response.data);
                            $('a[data-id_remove='+response.id+']').parent().parent().remove();//убрать поле
                        } else {
                            bootstrapAlert('danger',response.data);
                        }
                    };", \yii\web\View::POS_END);
                ?>
            </td>
            <?php endif ?>
            <?php if($canadmin) : ?>
                <td><?=  SwitchInput::widget([//можно в виджет организовать
                    'name' => $user->id_user,
                    'value' => $watcher, 
                    'pluginOptions' => [
                        'onText' => Yii::t('app/models', 'WaEn'),
                        'offText' => Yii::t('app/models', 'ClEn'),
                    ],
                    'pluginEvents' => [
                        "switchChange.bootstrapSwitch" => "function(event, state) { changeWatch(state, $(this).attr('name')); }",
                    ]
                ])?></td>
                <?php
                $this->registerJs("
                    function changeWatch(value, id_user){
                        if(value) value = 1; else value = 0;
                        $.ajax({
                            url: '".  \yii\helpers\Url::toRoute('/change-watch/change-watch')."',
                            type: 'post',
                            data: {
                                item_id: $model->pk_task,
                                classname: '".addslashes($model->getName())."',
                                value: value,
                                id_user:  id_user,
                                _csrf : '".Yii::$app->request->getCsrfToken()."'
                            },
                            success: function (data) {
                                if(data.success){
                                    jsNotify('', 'Успешно');
                                } else {
                                    jsNotify('','Проблемы', '', 'danger');
                                }
                            }
                        });
                    }
                ", \yii\web\View::POS_END);
                ?>
            <?php else : ?>
                <th><?php if($watcher) echo Yii::t('app/models', 'Watcher Entity')?></th>
            <?php endif ?>
        </tr>
    <?php endforeach ?>
</table>