<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\intranet\models\Tasks;
use app\modules\user\models\User;
use kartik\select2\Select2;
use kartik\date\DatePicker;

$this->title = Yii::t('app/models', 'Tasks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tasks-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app/models', 'Create Tasks'), ['create'], ['class' => 'btn btn-success']) ?>
        
        <?php
        if(Yii::$app->user->identity->isAdmin() || Yii::$app->user->id === 11) {
        echo Html::a('Аналитика задач', ['analytics'], ['class' => 'btn btn-primary']);
        } ?>
        
        <?php if(Yii::$app->user->identity->isAdmin()) echo Html::a(Yii::t('app/models', 'All tasks'), ['all-tasks'], ['class' => 'btn btn-success admin-btn']) ?>
        
        <?php if(Yii::$app->cache->get('task_mode_search'+Yii::$app->user->id) == 'task') {
            $show = Yii::t('app/models', 'Quest Tasks');
            $mode = 'quest';
        } else {
            $show = Yii::t('app/models', 'Tasks');
            $mode = 'task';
        }?>
        <?= Html::a(Yii::t('app/models', Yii::t('app', 'Сonclude').' '.$show), ['index','mode' => $mode], ['class' => 'btn btn-warning']) ?>
    </p>
    <p>
        <?php
        $nameModelGet = 'TasksSearch';
        $status = Yii::$app->request->get($nameModelGet)['status'];
        ?>
        
        <?= Html::a(Yii::t('app/models', 'Tasks status default'), [''], ['class' => ($status === NULL) ? 'btn btn-primary' :'btn btn-default']) ?>
        <?= Html::a(Yii::t('app/models', 'Tasks status work'), ['', $nameModelGet.'[status]' => Tasks::STATUS_WORK], ['class' => ($status == Tasks::STATUS_WORK) ? 'btn btn-primary' :'btn btn-default']) ?>
        <?= Html::a(Yii::t('app/models', 'Tasks status done'), ['', $nameModelGet.'[status]' => Tasks::STATUS_DONE], ['class' => ($status == Tasks::STATUS_DONE) ? 'btn btn-primary' :'btn btn-default']) ?>
        <?= Html::a(Yii::t('app/models', 'Tasks status fail'), ['', $nameModelGet.'[status]' => Tasks::STATUS_FAIL], ['class' => ($status == Tasks::STATUS_FAIL) ? 'btn btn-primary' :'btn btn-default']) ?>
        <?= Html::a(Yii::t('app/models', 'Tasks status report'), ['', $nameModelGet.'[status]' => Tasks::STATUS_REPORT], ['class' => ($status == Tasks::STATUS_REPORT) ? 'btn btn-primary' :'btn btn-default']) ?>
        <?= Html::a(Yii::t('app/models', 'Task status latebytime'), ['', $nameModelGet.'[status]' => Tasks::STATUS_LATEBYTIME], ['class' => ($status == Tasks::STATUS_LATEBYTIME) ? 'btn btn-primary' :'btn btn-default']) ?>
        <?= Html::a(Yii::t('app/models', 'Task status donelatebytime'), ['', $nameModelGet.'[status]' => Tasks::STATUS_DONELATEBYTIME], ['class' => ($status == Tasks::STATUS_DONELATEBYTIME) ? 'btn btn-primary' :'btn btn-default']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'title',
                'value' => function ($model, $key, $index, $column) {
                    return $model->htmlIcon();
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'create_date',
                'headerOptions' => ['width' => '220px'],
                'value' => function ($model, $key, $index, $column) {
                    return Yii::$app->formatter->asDate($model->create_date);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel, 
                    'attribute' => 'create_date',
                    'removeButton' => false,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]),
            ],
            [
                'attribute' => 'start_date',
                'headerOptions' => ['width' => '220px'],
                'value' => function ($model, $key, $index, $column) {
                    return Yii::$app->formatter->asDate($model->start_date);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel, 
                    'attribute' => 'start_date',
                    'removeButton' => false,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]),
            ],
            [
                'attribute' => 'end_date',
                'headerOptions' => ['width' => '220px'],
                'value' => function ($model, $key, $index, $column) {
                    return Yii::$app->formatter->asDate($model->end_date);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel, 
                    'attribute' => 'end_date',
                    'removeButton' => false,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]),
            ],
            [
                'attribute' => 'id_author',
                'value' => function ($model, $key, $index, $column) {
                    return Html::a(User::getAll($model->id_author), ['/user/user/profile', 'id' => $model->id_author], ['target' => '_blank']);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'id_author',
                    'data' => User::getAllActive(),
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'format' => 'raw',
            ],
            [
                'attribute' => 'status',
                'value' => function ($model, $key, $index, $column) {
                    return Tasks::getStatus($model->status, true);
                },
                'filter' => Tasks::getStatus(),
                'format' => 'raw',
            ],
            // 'status',
            // 'update_date',
            // 'start_date',
            // 'end_date',
            // 'create_date',
            // 'project_id',
            //['class' => 'yii\grid\ActionColumn'],
        ],
        'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
        'pager' => [
            'class' => \liyunfang\pager\LinkPager::className(),
            'pageSizeList' => Yii::$app->params['pageSizeList'],
            'pageSizeOptions' => ['class' => 'form-control pagesizeselect'],
        ],
    ]); ?>

</div>