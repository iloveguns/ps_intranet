<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Create Project Directions');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Project Directions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-directions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>