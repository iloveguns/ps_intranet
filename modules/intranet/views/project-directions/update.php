<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Create Project Directions') . ' : ' . $model->pk_direction;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Project Directions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pk_direction, 'url' => ['view', 'id' => $model->pk_direction]];
$this->params['breadcrumbs'][] = Yii::t('app/views', 'Update');
?>
<div class="project-directions-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>