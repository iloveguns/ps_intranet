<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = Yii::t('app/views', 'Project Directions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-directions-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app/views', 'Create Project Directions'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [                        
            'pk_direction',
            'name_direction',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update} {delete}'
            ],
        ],
    ]); ?>
</div>