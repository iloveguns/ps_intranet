<?php

use yii\helpers\Html;

$this->title = Yii::t('app/models', 'Create Blog');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Blogs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blogs-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>