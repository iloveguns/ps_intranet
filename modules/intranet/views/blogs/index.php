<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\user\models\User;
use kartik\select2\Select2;
use kartik\date\DatePicker;

$this->title = Yii::t('app/models', 'Blogs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blogs-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app/models', 'Create Blog'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app/models', 'My blogs'), ['my'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'title',
                'value' => function ($model, $key, $index, $column) {
                    return Html::a(strip_tags($model->title), ['view', 'id' => $model->pk_blog]);
                },
                'format' => 'raw',
            ],
            /*[
                'attribute' => 'text',
                'value' => function ($model, $key, $index, $column) {
                    return substr(strip_tags($model->text), 0, 250);
                },
                'format' => 'raw',
            ],*/
            [
                'attribute' => 'create_date',
                'headerOptions' => ['width' => '220px'],
                'value' => function ($model, $key, $index, $column) {
                    return Yii::$app->formatter->asDate($model->create_date);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel, 
                    'attribute' => 'create_date',
                    'removeButton' => false,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]),
            ],
            [
                'attribute' => 'id_author',
                'value' => function ($model, $key, $index, $column) {
                    if($model->id_author){
                        return Html::a(User::getAll($model->id_author), ['/user/user/profile', 'id' => $model->id_author], ['target' => '_blank']);
                    } else {
                        return User::getNullUser();
                    }
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'id_author',
                    'data' => User::getAll(),
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'format' => 'raw',
            ],
        ],
        'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
        'pager' => [
            'class' => \liyunfang\pager\LinkPager::className(),
            'pageSizeList' => Yii::$app->params['pageSizeList'],
            'pageSizeOptions' => ['class' => 'form-control pagesizeselect'],
        ],
    ]); ?>
</div>