<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
?>

<div class="blogs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'text')->textarea(['tinymce' => 'tinymce']) ?>

    <div>
        <?= $form->field($model, 'status_blog')->widget(Select2::classname(), [
            'data' => $model->getStatus(),
            'options' => ['placeholder' => Yii::t('app', 'Select')],
            'pluginOptions' => [
                'allowClear' => false,
            ],
        ]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>