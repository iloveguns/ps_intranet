<?php

use yii\helpers\Html;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Blogs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blogs-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-md-9 row">
        <?php if(Yii::$app->user->identity->isAdmin() || $model->canAdmin(Yii::$app->user->id)) : ?>
        <p>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->pk_blog], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->pk_blog], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <?php endif ?>
        
        <div>
            <div class="well">
                <div>
                    <i class="fa fa-calendar"></i> <?=Yii::$app->formatter->asDatetime($model->create_date)?>
                </div>
                <div class="text-from-redactor">
                    <?= $model->text?>
                </div>
                <div>
                    <p class="pull-right">
                        <strong><?=$model->getAttributeLabel('id_author')?>: </strong><?= ($model->idAuthor === NULL) ? User::getNullUser() : $model->idAuthor->getLinkToProfile($model->idAuthor->fullName)?>
                    </p>
                </div>
            </div>
        </div>
        <hr>
        <?= app\components\WidgetComments::widget(['modelEntity' => $model])?>
    </div>

    <div class="col-md-3">
        <?= $this->render(Yii::$app->params['themeFolder'].'/_rsidebar', ['model' => $model]) ?>
    </div>
</div>