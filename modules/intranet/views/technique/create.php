<?php

use yii\helpers\Html;

$this->title = Yii::t('app/models', 'Technique_create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Techniques'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="technique-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>