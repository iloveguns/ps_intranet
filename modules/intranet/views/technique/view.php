<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->pk_technique;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Techniques'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="technique-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->pk_technique], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->pk_technique], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name_technique',
            //'img_tecnique',
            'office_num',
        ],
    ]) ?>

</div>