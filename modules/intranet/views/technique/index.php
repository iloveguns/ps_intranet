<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = Yii::t('app/models', 'Techniques');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="technique-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app/models', 'Technique_create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name_technique',
            'office_num',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>