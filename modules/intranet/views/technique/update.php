<?php

use yii\helpers\Html;

$this->title = Yii::t('app/models', 'Technique_update');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Techniques'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pk_technique, 'url' => ['view', 'id' => $model->pk_technique]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="technique-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>