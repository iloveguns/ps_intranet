<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

?>

<div class="technique-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'name_technique')->textInput(['maxlength' => true]) ?>

    <?php 
    echo app\models\UploadFiles::showImg($model, 'img_tecnique');

    echo app\models\UploadFiles::showUploadFormField($form, $model, 'img_tecnique');
    ?>
    
    <div>
        <?= $form->field($model, 'type_technique')->widget(Select2::classname(), [
            'data' => $model->getTypes(),
            'options' => ['placeholder' => Yii::t('app', 'Select')],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]) ?>
    </div>
    
    <?= $form->field($model, 'office_num')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>