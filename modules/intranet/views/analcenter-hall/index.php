<?php 
use yii\helpers\Url;
use app\modules\intranet\models\AnalcenterHall;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use yii\bootstrap\Modal;
use app\modules\intranet\IntranetModule;
use app\models\FunctionModel;

$this->title = Yii::t('app/views', 'Analytics Center');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row row-cols events-company-index">
    
    <h1><?= $this->title ?></h1>
    
    <div class="col-md-8">
        <div class="box">
            <?= \yii2fullcalendar\yii2fullcalendar::widget([
                'options' => [
                    'lang' => 'ru',
                ],
                'header' => [
                    'center'=>'title',
                    'left'=>'prev,next',        
                    'right'=>'today month,agendaWeek,agendaDay',
                ],
                'ajaxEvents' => Url::to(['jsoncalendar']),
                'eventAfterAllRender' => 'function(event, element) {
                    $(".fc-button").click(function(){
                        regEventClick();
                    });
                    regEventClick(); 
                }', // события после рендера
            ]);?>
        </div>
    </div>
    
    <div class="col-md-4">
       <?php
        //if(Yii::$app->user->identity->can('anal_center') || Yii::$app->user->identity->isAdmin()) : // c 15.11.2017 могут все
        $model = new AnalcenterHall();
        $form = ActiveForm::begin(['id' => 'event-create']); ?>
            <?php
            Modal::begin([
                'header' => '<h2>'.IntranetModule::t('all', 'Create Event Company').'</h2>',
                'toggleButton' => ['label' => IntranetModule::t('all', 'Create Event Company'), 'class' => 'btn btn-primary'],
            ]) ?>

            <?= $form->field($model, 'title')->textInput() ?>

            <?= $form->field($model, 'text_event')->textarea(['rows' => 6]) ?>

            <div class="row">
                <div class="col-md-4">
                    <label><?= $model->getAttributeLabel('start_date')?></label>
                    <?= DatePicker::widget([
                        'name' => 'start_date',
                        'type' => DatePicker::TYPE_INPUT,
                        'value' => FunctionModel::getDateWParam('Y-m-d'),
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]);?>
                </div>
                <div class="col-md-4">
                    <label><?= $model->getAttributeLabel('start_date')?></label>
                    <?= TimePicker::widget([
                        'name' => 'start_time', 
                        'value' => date('H:i'),
                        'pluginOptions' => [
                            'showMeridian' => false,
                        ]
                    ]);?>
                </div>
                <div class="col-md-4">
                    <label><?= $model->getAttributeLabel('end_date')?></label>
                    <?= TimePicker::widget([
                        'name' => 'end_time', 
                        'value' => date('H:i'),
                        'pluginOptions' => [
                            'showMeridian' => false,
                        ]
                    ]);?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success', 'id' => 'create-event']) ?>
            </div>
        
            <?php 
            $this->registerJs("
            $('#create-event').click(function(){
                if($('#eventscompany-title').val() == ''){
                    $('.field-eventscompany-title').addClass('has-error');
                    return false;
                }
                else{
                    $('.field-eventscompany-title').removeClass('has-error');
                    $.ajax({
                        url: '".Url::toRoute(['/intranet/analcenter-hall/create'])."',
                        dataType : 'JSON',
                        type : 'POST',
                        data : $('#event-create').serializeArray(),
                        success : function(response){
                            if(response.success == true){
                                $('.modal').modal('hide');
                                bootstrapAlert('success','".Yii::t('app/models', 'Success Save')."');
                                setTimeout(function(){ location.reload(); }, 1000);
                            }
                        }
                    });
                    
                    return false;
                }
            });
            ", \yii\web\View::POS_END);?>

            <?php Modal::end() ?>
        <?php ActiveForm::end();//endif; ?> 
    </div>
</div>
<?php $this->registerJs("
function regEventClick(){
    $('.fc-event').click(function(){
        $.ajax({
            url: '".Url::toRoute(['/intranet/analcenter-hall/get-event-by-id'])."',
            dataType : 'HTML',
            type : 'POST',
            data : {id_event:$(this).attr('href')},
            success : function(response){
                $('#myModal').modal();
                $('#myModal .modal-body').html(response);
            }
        });

        return false;
    });
}
", yii\web\View::POS_END);