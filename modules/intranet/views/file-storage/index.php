<?php 
use app\components\WidgetMyFiles;

$this->title = Yii::t('app/models', 'My Files');
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= $this->title?></h1>

<?= WidgetMyFiles::widget(['updateUrl' => '/intranet/file-storage/index']) ?>

