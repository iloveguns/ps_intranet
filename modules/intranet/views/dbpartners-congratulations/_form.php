<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\intranet\models\DbpartnersCongratulations */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dbpartners-congratulations-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_congratulation')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'date_congratulation')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>