<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Create Congratulations dbp');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Congratulations dbp'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dbpartners-congratulations-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>