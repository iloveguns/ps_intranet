<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Update Congratulations dbp') .' '. $model->pk_congratulation;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Congratulations dbp'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pk_congratulation, 'url' => ['view', 'id' => $model->pk_congratulation]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dbpartners-congratulations-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>