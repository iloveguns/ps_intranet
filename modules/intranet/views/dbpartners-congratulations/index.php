<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = Yii::t('app/views', 'Congratulations dbp');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Dbpartners'), 'url' => ['dbpartners/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dbpartners-congratulations-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app/views', 'Create Congratulations dbp'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'name_congratulation:ntext',
            'date_congratulation',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>