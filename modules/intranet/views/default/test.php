<?php
Yii::$app->view->registerJsFile('@web/js/build/nestable.js',['depends' => 'app\assets\AppAsset', 'position' => yii\web\View::POS_END]);
Yii::$app->view->registerCssFile('@web/css/build/nestable.css');
?>
<div class="dd" id="nestable-json"></div>
/*
var json = '[{"id":1},{"id":2},{"id":3,"children":[{"id":4},{"id":5,"content":"test"}]}]';
var options = {'json': json, 'maxDepth': 2}
$('#nestable-json').nestable(options);

$('.dd').nestable('add', {"id":6,"content":"новый"});
*/