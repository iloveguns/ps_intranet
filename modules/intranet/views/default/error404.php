<?php
use yii\helpers\Html;
?>
<div class="error-page">
    <h2 class="headline text-yellow"> 404</h2>

    <div class="error-content">
    <h3><i class="fa fa-warning text-yellow"></i> <?= Yii::t('app/errors', 'Oops! Page not found')?></h3>
    <p>
        <?= Yii::t('app/errors', 'We could not find the page you were looking for.')?>
    </p>
    <p>
        <?= Yii::t('app/errors', 'Meanwhile, you may try using the search form or')?>
        <?=  Html::a('На главную', '/intranet')?>
    </p>
    <form action="<?= yii\helpers\Url::toRoute(['/intranet/default/search'])?>" method="get" class="search-form">
        <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="<?= Yii::t('app', 'Search...')?>"/>
            <span class="input-group-btn">
            <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </form>
    </div>
</div>