<?php
use app\components\WidgetOnlineUsers;
use app\components\WidgetCalendar;
use app\components\WidgetCallboardLast;
use app\modules\intranet\models\Callboard;
use app\components\WidgetRegisterUsers;
use app\components\WidgetTasks;
use app\components\WidgetBlogs;

$this->title = Yii::$app->name;
?>
<div class="row row-cols intranet-default-index">
    <div class="col-md-7 col-sm-7 col-lg-8">
        <?= WidgetCalendar::widget(['header' => Yii::t('app/views', 'Index calendar')])?>
        <?= WidgetTasks::widget()?>
    </div>
    <div class="hidden-md hidden-sm hidden-lg" id="center-index-box">
        
    </div>
    <div class="col-md-5 col-sm-5 col-lg-4">
        <?= WidgetCallboardLast::widget(['where' => ['!=', 'fk_callboard_category', Callboard::BIRTHDAY], 'title' => Yii::t('app', 'Callboard Events'), 'uniqname' => 'widgetcall'])//все без поздравлений?>
        <?= WidgetCallboardLast::widget(['where' => ['=', 'fk_callboard_category', Callboard::BIRTHDAY], 'title' => Yii::t('app', 'Сongratulations Events') , 'uniqname' => 'widgetcallbirth', 'limit' => 5])//поздравления?>
        <?= WidgetRegisterUsers::widget(['limit' => 2])?>
        <?= WidgetOnlineUsers::widget()?>
        <?= WidgetBlogs::widget(['title' => Yii::t('app/models', 'Blogs') , 'uniqname' => 'widgetblogs', 'limit' => 5])//блоги?>
    </div>
</div>


