<?php

use yii\helpers\Html;
use app\modules\intranet\IntranetModule;

$this->title = IntranetModule::t('all', 'Update Callboard');
$this->params['breadcrumbs'][] = ['label' => IntranetModule::t('all', 'Callboards'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="callboard-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>