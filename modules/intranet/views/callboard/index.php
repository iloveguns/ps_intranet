<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\intranet\IntranetModule;
use kartik\select2\Select2;
use app\modules\user\models\User;
use kartik\date\DatePicker;
use app\modules\intranet\models\CallboardCategory;

$this->title = IntranetModule::t('all', 'Callboards');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="callboard-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(IntranetModule::t('all', 'Create Callboard'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'title',
                'value' => function ($model, $key, $index, $column) {
                    return Html::a(strip_tags($model->title), ['view', 'id' => $model->pk_callboard]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'id_author',
                'value' => function ($model, $key, $index, $column) {
                    if($model->id_author){
                        return Html::a(User::getAll($model->id_author), ['/user/user/profile', 'id' => $model->id_author], ['target' => '_blank']);
                    } else {
                        return User::getNullUser();
                    }
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'id_author',
                    'data' => User::getAll(),
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'format' => 'raw',
            ],
            [
                'attribute' => 'create_date',
                'headerOptions' => ['width' => '220px'],
                'value' => function ($model, $key, $index, $column) {
                    return Yii::$app->formatter->asDate($model->create_date);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel, 
                    'attribute' => 'create_date',
                    'removeButton' => false,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]),
            ],
            [
                'attribute' => 'fk_callboard_category',
                'value' => function ($model, $key, $index, $column) {
                    return CallboardCategory::getCategory($model->fk_callboard_category);
                },
                'filter' => CallboardCategory::getCategory(),
            ],
        ],
        'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
        'pager' => [
            'class' => \liyunfang\pager\LinkPager::className(),
            'pageSizeList' => Yii::$app->params['pageSizeList'],
            'pageSizeOptions' => ['class' => 'form-control pagesizeselect'],
        ],
    ]); ?>

</div>  