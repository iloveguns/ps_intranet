<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\intranet\models\CallboardCategory;
use app\components\WidgetStructure;
use app\components\WidgetGlobalUsed;

?>

<div class="callboard-form">

    <?php $form = ActiveForm::begin(['id' => 'widget-struct-form']); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'text')->textarea(['tinymce' => 'tinymce']) ?>

    <?= $form->field($model, 'fk_callboard_category')->dropDownList(CallboardCategory::getCategory()) ?>
    
    <div class="panel panel-default">
        <?= WidgetStructure::widget(['model' => $model]) ?>
    </div>
    
    <?= WidgetGlobalUsed::widget(['model' => $model]) ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJs("
    $('#widget-struct-form').on('beforeSubmit', function (e) {
        if ($('#selectedStructuresData').val() === window.emptystruct) {
            bootstrapAlert('error', 'Необходимо выбрать сотрудников');
            return false;
        }
        gifLoader();
        return true;
    });
", yii\web\View::POS_READY); ?>