<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\intranet\IntranetModule;
use app\modules\user\models\Units;
use app\modules\user\models\User;

$this->title = strip_tags($model->title);
$this->params['breadcrumbs'][] = ['label' => IntranetModule::t('all', 'Callboards'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fkCategory->name, 'url' => ['index', 'CallboardSearch[fk_callboard_category]' => $model->fkCategory->pk_category]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row row-cols callboard-view">

    <h1><?= $model->title ?></h1>
    
    <div class="col-md-9 row">
        <?php if(Yii::$app->user->identity->isAdmin() || $model->canAdmin(Yii::$app->user->id)) : ?>
        <p>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->pk_callboard], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->pk_callboard], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <?php endif ?>
        
        <?php if($model->fkCategory->pk_category == app\modules\intranet\models\Callboard::BIRTHDAY && isset($model->link_to_entity)) : ?>
        <div class="row">
            <div class="center-block" style="width:300px">
                <?php 
                $user = User::findOne($model->link_to_entity);
                echo $user->getImgAvatar();
                ?>
            </div>
        </div>
        <?php endif ?>
        <div>
            <div class="well">
                <div>
                    <i class="fa fa-calendar"></i> <?=Yii::$app->formatter->asDatetime($model->create_date)?>
                </div>
                <div class="text-from-redactor">
                    <?= $model->text?>
                </div>
                <div>
                    <p class="pull-right">
                        <strong><?=$model->getAttributeLabel('id_author')?>: </strong><?= ($model->idAuthor === NULL) ? User::getNullUser() : $model->idAuthor->getLinkToProfile($model->idAuthor->fullName)?>
                    </p>
                </div>
            </div>
        </div>

        <?= \app\components\WidgetGlobalUsed::renderIt($model) ?>
        
        <div>
            <h3><?= IntranetModule::t('all', 'Callboard for')?>:</h3>
            <div id="selectedStructuresListForm">
                <?= Units::StructInfo($model)//полная инфа Nestable ?>
            </div>
        </div>
        <hr>
        <?= app\components\WidgetComments::widget(['modelEntity' => $model])?>
    </div>
    <div class="col-md-3">
        <?= $this->render(Yii::$app->params['themeFolder'].'/_rsidebar', ['model' => $model]) ?>
    </div>
</div>