<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Update Clients dbp') .' '. $model->name_client;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Dbpartners'), 'url' => ['dbpartners/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Clients dbp'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dbpartners-client-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>