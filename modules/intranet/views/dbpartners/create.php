<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Create Clients dbp');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Dbpartners'), 'url' => ['dbpartners/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Types clients dbp'), 'url' => ['clients']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dbpartners-client-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>