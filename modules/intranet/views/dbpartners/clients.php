<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\user\models\User;
use app\modules\intranet\models\DbpartnersTypeClient;

$this->title = Yii::t('app/views', 'Clients dbp');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Dbpartners'), 'url' => ['dbpartners/index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['dbpartners/clients']];
?>
<div class="dbpartners-client-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app/views', 'Create Clients dbp'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-12">
        <?php echo kartik\select2\Select2::widget([
            'name' => 'congratulation',
            'data' => app\modules\intranet\models\DbpartnersCongratulations::getAll('', true),
            'value' => (isset($_GET['congratulation'])) ? (int)$_GET['congratulation']: 0,
            'options' => [
                'placeholder' => Yii::t('app/views', 'Congratulations dbp'),
                'id' => 'congratulation',
            ],
        ]);
        
        $this->registerJsFile('@web/js/build/queryurl.min.js',['depends' => 'yii\jui\JuiAsset','position' => yii\web\View::POS_END]);
        
        $this->registerJs('
            $("#congratulation").change(function(){
                window.location.href = jQuery.query.set("congratulation", $(this).val());
            });
        ', \yii\web\View::POS_END);        
        ?>
        </div>
    </div>
    
    <div class="clearfix"></div>
    
    <?= GridView::widget([
        //'summary' => '{totalCount}',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'pk_client',
            'name_client:ntext',
            'appointment_client:ntext',
            [
                'attribute' => 'type_client',
                'value' => function ($model, $key, $index, $column) {
                    if($model->type_client)
                    return DbpartnersTypeClient::getAll($model->type_client);
                },
                'filter' => DbpartnersTypeClient::getAll(),
            ],
            [
                'attribute' => 'birthdate_client',
                'value' => function ($model, $key, $index, $column) {
                    if($model->birthdate_client)
                    return Yii::$app->formatter->asDate($model->birthdate_client);
                },
            ],
            [
                'attribute' => 'sex',
                'value' => function ($model, $key, $index, $column) {
                    if($model->sex)
                    return User::getSex($model->sex);
                },
                'filter' => User::getSex(),
            ],
            // 'responsible:ntext',
            // 'fk_user',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>