<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\datecontrol\DateControl;
?>

<div class="dbpartners-client-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_client')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'appointment_client')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'sex')->dropDownList(app\modules\user\models\User::getSex()) ?>
    
    <?= $form->field($model, 'type_client')->widget(Select2::classname(), [
        'data' => \app\modules\intranet\models\DbpartnersTypeClient::getAll(),
        'options' => ['placeholder' => Yii::t('app', 'Select')],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]);?>
    
    <?= $form->field($model, 'birthdate_client')->widget(DateControl::classname(), [
        'type'=>DateControl::FORMAT_DATE,
        'pluginOptions' => [
            'autoclose'=>true,
        ]
    ]);?>
    
    <h4><?= Yii::t('app/views', 'Congratulations dbp')?></h4>
    <?= Html::checkboxList('events', \yii\helpers\ArrayHelper::getColumn($model->congratulations, 'pk_congratulation'), \app\modules\intranet\models\DbpartnersCongratulations::getAll(), ['class' => 'block-box-label']) ?>

    <?= $form->field($model, 'responsible')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>