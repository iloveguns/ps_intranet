<?php
use yii\helpers\Html;
$this->title = Yii::t('app/views', 'Dbpartners');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row dbpartners-index">
    <div class="col-md-4">
        <div class="box box-success">
            <div class="box-body">
                <?= yii2fullcalendar\yii2fullcalendar::widget([
                    'options' => [
                      'lang' => 'ru',
                    ],
                    'header' => [
                        'center'=>'title',
                        'left'=>'prev,next today',        
                        'right'=>'month,agendaWeek',//,,agendaDay
                    ],
                    'events'=> $events,
                  ]);
                ?>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="col-md-4">
            <?= Html::a(Yii::t('app/views','Clients dbp'), ['clients'], ['class' => 'btn btn-primary'])?>
        </div>
        <div class="col-md-4">
            <?= Html::a(Yii::t('app/views','Types clients dbp'), ['dbpartners-type-client/index'], ['class' => 'btn btn-primary'])?>
        </div>
        <div class="col-md-4">
            <?= Html::a(Yii::t('app/views','Congratulations dbp'), ['dbpartners-congratulations/index'], ['class' => 'btn btn-primary'])?>
        </div>
    </div>
</div>