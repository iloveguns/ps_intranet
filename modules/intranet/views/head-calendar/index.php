<?php
$this->title = \app\modules\intranet\IntranetModule::t('all', 'Calendar Director');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row row-cols events-company-index">
    <h1><?= $this->title ?></h1>
    <div class="col-md-8">
        <div class="box">
            <?= \app\components\WidgetCalendar::widget(['iduser' => Yii::$app->params['id_head_smg'], 'header' => \app\modules\intranet\IntranetModule::t('all', 'Calendar Director')])?>
        </div>
    </div>
</div>