<?php

use yii\helpers\Html;
use app\modules\intranet\IntranetModule;

$this->title = IntranetModule::t('all', 'Update Event') .' '. $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->pk_event]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="events-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>