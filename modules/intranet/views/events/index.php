<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\intranet\IntranetModule;
use app\modules\intranet\models\Tasks;
use app\modules\user\models\User;
use kartik\select2\Select2;
use kartik\date\DatePicker;

$this->title = Yii::t('app/models', 'Events');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tasks-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(IntranetModule::t('all', 'Create Event'), ['create'], ['class' => 'btn btn-success']) ?>
        <?php if(Yii::$app->user->identity->isAdmin()) echo Html::a(IntranetModule::t('all', 'All Events'), ['all-events'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'title',
                'value' => function ($model, $key, $index, $column) {
                    $html = Html::a($model->title, ['view', 'id' => $model->pk_event]);
                    return $html;
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'start_date',
                'headerOptions' => ['width' => '220px'],
                'value' => function ($model, $key, $index, $column) {
                    return Yii::$app->formatter->asDate($model->start_date);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel, 
                    'attribute' => 'start_date',
                    'removeButton' => false,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]),
            ],
            [
                'attribute' => 'end_date',
                'headerOptions' => ['width' => '220px'],
                'value' => function ($model, $key, $index, $column) {
                    return Yii::$app->formatter->asDate($model->end_date);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel, 
                    'attribute' => 'end_date',
                    'removeButton' => false,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]),
            ],
            [
                'attribute' => 'id_author',
                'value' => function ($model, $key, $index, $column) {
                    return Html::a(User::getAll($model->id_author), ['/user/user/profile', 'id' => $model->id_author], ['target' => '_blank']);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'id_author',
                    'data' => User::getAllActive(),
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'format' => 'raw',
            ],
        ],
        'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
        'pager' => [
            'class' => \liyunfang\pager\LinkPager::className(),
            'pageSizeList' => Yii::$app->params['pageSizeList'],
            'pageSizeOptions' => ['class' => 'form-control pagesizeselect'],
        ],
    ]); ?>

</div>