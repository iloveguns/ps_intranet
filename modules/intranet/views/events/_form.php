<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\user\models\User;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use app\modules\intranet\models\Tasks;
use app\components\WidgetStructure;
use kartik\datetime\DateTimePicker;
use app\modules\intranet\IntranetModule;
use kartik\datecontrol\DateControl;

if($model->isNewRecord) $model->start_date = \app\models\FunctionModel::getDateTimestamp();//новая задача - время текущее

//подготовка данных
$repeat_attrs = json_decode($model->json_repeat, true);
if(!isset($repeat_attrs['date'])) $repeat_attrs['date'] = '';
$forever_repeat = isset($repeat_attrs['forever']);
//конец подготовка данныхZ
?>

<div class="events-form">

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>
    
    <?= $form->field($model, 'title')->textInput() ?>
    
    <?= $form->field($model, 'text')->textarea(['tinymce' => 'tinymce']) ?>

    <div class="panel panel-default">
        <div class="panel-body">
            <?= $form->field($model, 'start_date', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->widget(DateControl::classname(), [
                'type'=>DateControl::FORMAT_DATETIME,
                'options' => [
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]
            ]);?>
            <?= $form->field($model, 'end_date', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->widget(DateControl::classname(), [
                'type'=>DateControl::FORMAT_DATETIME,
                'options' => [
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]
            ]);?>
        </div>
    </div>
    
    <?= $form->field($model, 'is_public')->checkbox() ?>
    
    <div class="panel panel-default">
        <div class="panel-body row">        
            <div class="form-group col-md-4">
                <label class="control-label"><i class="fa fa-repeat"></i> Повторять</label>
                <?= Html::dropDownList('repeat_select', '', \app\modules\intranet\models\Events::getRepeatOption(), ['class' => 'form-control', 'id' => 'repeat_select'])?>
            </div>
            <div class="form-group col-md-4 repeat_date">
                <label class="control-label">Повторить в дату:</label>
                <?= DateControl::widget([
                    'name'=>'repeat_date', 
                    'id' => 'repeat_date',
                    'type'=>DateControl::FORMAT_DATE,
                    'autoWidget'=>true,
                    'value' => $repeat_attrs['date'],
                    'displayFormat' => 'php:d-F-Y',
                    'saveFormat' => 'php:Y-m-d',
                ]);
                ?>
            </div>
            <div class="form-group col-md-4 repeat_count">
                <label class="control-label">Кол-во раз:</label>
                <?= Html::input('number', 'repeat_count', 5, ['class' => 'form-control', 'id' => 'repeat_count'])?>
                <?= Html::checkbox('repeat_forever', $forever_repeat, ['label' => 'Всегда', 'id' => 'repeat_forever'])?>
            </div>
        </div>
        <?= $form->field($model, 'json_repeat',['options' => ['class' => 'hidden']])->textInput()->label(false) ?>
        <?php
        $this->registerJs("
            function checkRepeatSelect(){
                if($('#repeat_select').val()=='date'){
                    $('.repeat_date').show();
                    $('.repeat_count').hide();
                } else {
                    $('.repeat_date').hide();
                    if($('#repeat_select').val()!=''){
                        $('.repeat_count').show();
                    } else {
                        $('.repeat_count').hide();
                    }
                }
            }
            function checkRepeatCount(){
                if($('#repeat_forever').is(':checked')){
                    $('#repeat_count').attr('disabled',true);
                } else {
                    $('#repeat_count').attr('disabled',false);
                }
            }
            function checkRepeatDate(){
                var repdate = $('#repeat_date');
                if(repdate.val() == ''){
                    repdate.parent().removeClass('has-success');
                    repdate.parent().addClass('has-error');
                    return false;
                } else {
                    repdate.parent().removeClass('has-error');
                    repdate.parent().addClass('has-success');
                }
            }
            function setAttrsEventRepeat(select,date,count,forever){//установить значения из json
                if(select){
                    $('#repeat_select').val(select);
                    if(date){
                        $('.repeat_date').show();
                        $('#repeat_date').val(date);
                        checkRepeatSelect();
                    }
                    if(count){
                        $('#repeat_count').val(count);
                        checkRepeatSelect();
                    }
                    if(forever){
                        $('#repeat_forever').attr('checked',true);
                        checkRepeatCount();
                    }
                }
            }
            function toJsonRepeat(){//сохранять в json
                $('#events-json_repeat').val(JSON.stringify({select:$('#repeat_select').val(), date:$('#repeat_date').val(), count:$('#repeat_count').val(), forever:$('#repeat_forever').is(':checked')}));
            }
            $('form#{$model->formName()}').on('submit', function(e) {
                if($('#repeat_select').val() == 'date'){
                    checkRepeatDate();
                    if($('#repeat_date').val() == ''){
                        return false;
                    }
                }
            });
        ", \yii\web\View::POS_END);
        
        $this->registerJs("
            setAttrsEventRepeat('".(isset($repeat_attrs['select']) ? $repeat_attrs['select'] : '')."','".(isset($repeat_attrs['date']) ? $repeat_attrs['date'] : '')."','".(isset($repeat_attrs['count']) ? $repeat_attrs['count'] : '')."','".(isset($repeat_attrs->forever) ? $repeat_attrs->forever : '')."');
            $('#repeat_select').change(function(){checkRepeatSelect()});
            $('#repeat_forever').change(function(){checkRepeatCount()});
            
            $('#repeat_date').change(function(){checkRepeatDate()});
            
            $('#repeat_forever').change(function(){toJsonRepeat()});
            $('#repeat_count').change(function(){toJsonRepeat()});
            $('#repeat_select').change(function(){toJsonRepeat()});
            $('#repeat_date').change(function(){toJsonRepeat()});

            checkRepeatSelect();
            checkRepeatCount();
            
            toJsonRepeat();
        ", \yii\web\View::POS_LOAD);
        ?>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-body row">
            <div class="form-group col-md-4 notify_timeat">
                <label class="control-label"><i class="fa fa-bell-o"></i> <?= IntranetModule::t('all', 'Notify Time At')?></label>
                <?= Html::dropDownList('notify_timeat', '', \app\modules\intranet\models\Events::getNotifyOptionTimeat(), ['class' => 'form-control', 'id' => 'notify_timeat'])?>
                <?= Html::checkbox('notify_notnotify', '', ['label' => IntranetModule::t('all', 'Not Notify'), 'id' => 'notify_notnotify'])?>
            </div>
            <?php //'уведомлять с' вызывает проблемы, решил закомментить + выше скрипты не тронуты ?>
            <!--<div class="form-group col-md-4">
                <label class="control-label"><i class="fa fa-bell-o"></i> <?= IntranetModule::t('all', 'Or Notify From')?></label>
                <?= DateTimePicker::widget([
                    'name' => 'notify_date',
                    'id' => 'notify_date',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd hh:ii:ss'
                    ]
                ]);
                ?>
            </div>
            <div class="form-group col-md-4 notify_period">
                <label class="control-label"><?= IntranetModule::t('all', 'Notify Every')?></label>
                <?= Html::dropDownList('notify_period', '', \app\modules\intranet\models\Events::getNotifyOption(), ['class' => 'form-control', 'id' => 'notify_period'])?>
            </div>-->
        </div>
        <?= $form->field($model, 'json_notify',['options' => ['class' => 'hidden']])->textInput()->label(false) ?>
        <?php 
        $this->registerJs("
        function toJsonNotify(){//сохранять в json
            $('#events-json_notify').val(JSON.stringify({date:$('#notify_date').val(), period:$('#notify_period').val(), notnotify:$('#notify_notnotify').is(':checked'), timeat: $('#notify_timeat').val()}));
        }
        function checkDateNotify(){
            if($('#notify_date').val() == ''){
                $('.notify_period').hide();
            } else {
                $('.notify_period').show();
            }
        }
        function checkNotNotify(){
            if($('#notify_notnotify').is(':checked')){
                $('#notify_period').attr('disabled',true);
                $('#notify_date').attr('disabled',true);
                $('#notify_timeat').attr('disabled',true);
            } else {
                $('#notify_period').attr('disabled',false);
                $('#notify_date').attr('disabled',false);
                $('#notify_timeat').attr('disabled',false);
            }
        }
        function setAttrsEventNotify(date,period,notnotify,timeat){//установить значения из json
            if(!notnotify){
                $('#notify_notnotify').attr('checked',true);
                checkNotNotify();
            }
            if(date){
                $('#notify_date').val(date);
                checkDateNotify();
            }
            if(period){
                $('#notify_period').val(period);
            }
            if(timeat){
                $('#notify_timeat').val(timeat);
            }
        }
        ", \yii\web\View::POS_END);
        //подготовка данных
        $notify_attrs = json_decode($model->json_notify);
        //конец подготовка данных
        $this->registerJs("
            setAttrsEventNotify('".(isset($notify_attrs->date) ? $notify_attrs->date : '')."','".(isset($notify_attrs->period) ? $notify_attrs->period : '')."','".(isset($notify_attrs) ? true : false)."','".(isset($notify_attrs->timeat) ? $notify_attrs->timeat : '')."');
            $('#notify_date').change(function(){toJsonNotify()});
            $('#notify_period').change(function(){toJsonNotify()});
            $('#notify_notnotify').change(function(){toJsonNotify()});
            $('#notify_timeat').change(function(){toJsonNotify()});
            
            $('#notify_notnotify').change(function(){checkNotNotify()});
            
            $('#notify_date').change(function(){checkDateNotify()});
            
            toJsonNotify();
            checkDateNotify();
        ", \yii\web\View::POS_LOAD);?>
    </div>

    <div class="panel panel-default">
        <?= WidgetStructure::widget(['model' => $model]) ?>
    </div>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php $this->registerJs("
        $('#".$model->formName()."').on('beforeSubmit', function (e) {
            gifLoader();
            return true;
        });
    ", yii\web\View::POS_READY); ?>
</div>