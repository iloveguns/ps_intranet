<?php

use yii\helpers\Html;
use app\modules\intranet\IntranetModule;

$this->title = IntranetModule::t('all', 'Create Event');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>