<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\intranet\IntranetModule;
use app\modules\user\models\User;
use app\modules\user\models\Units;
use yii\bootstrap\Tabs;
use kartik\helpers\Enum;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use yii\bootstrap\Modal;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/models', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row row-cols tasks-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-md-9">
        <?php if(Yii::$app->user->identity->isAdmin() || $model->canAdmin(Yii::$app->user->id)) : ?>
        <p>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->pk_event], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->pk_event], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <?php endif ?>
        
        <div class="alert alert-info">
            <ul>
                <li><?=$model->getAttributeLabel('start_date')?>: <?=Yii::$app->formatter->asDatetime($model->start_date)?></li>
                <li><?=$model->getAttributeLabel('end_date')?>: <?=Yii::$app->formatter->asDatetime($model->end_date)?></li>
                <li><?=$model->getAttributeLabel('create_date')?>: <?=Yii::$app->formatter->asDatetime($model->create_date)?></li>
                <li><?=IntranetModule::t('all', 'Duration')?>: 
                    <?php
                    $datetimeFrom = date_create($model->end_date);
                    $datetimeTo = date_create($model->start_date);
                    echo $datetimeFrom->diff( $datetimeTo )->days;
                    ?>дн.
                </li>
                <?php if($model->update_date) : ?><li><?=$model->getAttributeLabel('update_date')?>: <?=Yii::$app->formatter->asDatetime($model->update_date)?></li><?php endif ?>
            </ul>
        </div>
        
        <div>
            <p><strong><?=$model->getAttributeLabel('id_author')?>: </strong><?= User::getAll($model->id_author)?></p>
            <h3><?=$model->getAttributeLabel('text')?>:</h3>
            <div class="well"><?=$model->text?></div>
        </div>
        
         <div>
            <h3><?= IntranetModule::t('all', 'Event For')?>:</h3>
            <div id="selectedStructuresListForm">
                <?= Units::StructInfo($model)//полная инфа Nestable ?>
            </div>
        </div>
        
        <hr>
        <?= app\components\WidgetComments::widget(['modelEntity' => $model])?>
    </div>
    <div class="col-md-3">
        <?= $this->render(Yii::$app->params['themeFolder'].'/_rsidebar', ['model' => $model]) ?>
    </div>

</div>