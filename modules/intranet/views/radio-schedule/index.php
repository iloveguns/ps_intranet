<?php
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use app\models\FunctionModel;
yii\jui\JuiAsset::register($this);
$mlabel = new app\modules\intranet\models\ScheduleRadio();

$this->title = Yii::t('app/models', 'Radio schedule');
?>
<h1><?= $this->title ?></h1>

<div class="mb-1em">
    <a href="<?= Url::to(['', 'date' => $daylastweek]) ?>" class="btn btn-primary btn-sm">Неделю назад</a>
    <a href="<?= Url::to(['']) ?>" class="btn btn-primary btn-sm ml-1em">Текущ. неделя</a>
    <a href="<?= Url::to(['', 'date' => $daynextweek]) ?>" class="btn btn-primary btn-sm ml-1em">Неделю вперед</a>
    <button class="btn btn-success btn-sm ml-1em" data-toggle="modal" data-target="#myModal" id="radio-create">Создать</button>
    <button class="btn btn-info btn-sm ml-1em" data-toggle="modal" data-target="#reportModal" id="radio-create-report">Формирование отчета</button>
</div>

<div class="col-lg-3 col-md-3 panel panel-default full-width">    
    <table class="table table-bordered text-center">
        <tr>
            <th>Время/Дата</th>
            <?php for($i = 0; $i < 7; $i ++) {                
                $d = FunctionModel::dateModify($date, '+' . $i . ' days', 'd.m.Y');
                $t = '';
                if($today === $d) {
                    $t = 'style="color:#3c8dbc;"';//подсветить текущий день
                }
                echo '<th ' . $t . '>' . $d . '(' . FunctionModel::getRuDayOfWeek($i + 1) . ')</th>';
            }?>
        </tr>
        <?php foreach($times as $time) : ?>
            <tr>
                <?= '<td>' . $time . '</td>'; ?>
                <?php for($i = 0; $i < 7; $i ++) {
                    $d = FunctionModel::dateModify($date, '+' . $i . ' days', 'd.m.Y');
                    $dymd = FunctionModel::dateModify($date, '+' . $i . ' days', 'Y-m-d');
                    $alltime = 0;
                    
                    echo '<td>';
                    echo '<div class="sortable" data-date="' . $dymd . '" data-time="' . $time . '">';
                    
                    if(isset($model[$dymd][$time])) {
                        foreach ($model[$dymd][$time] as $event) {
                            $alltime += $event['duration'];
                            $username = $infousers[$event['fk_user']]['lastname'];
                            
                            echo '<div class="radio-event-wrap open-modal" id="' . $event['id'] . '" data-id="' . $event['id'] . '" data-date="' . $dymd . '" data-time="' . $time . '" data-duration="' . $event['duration'] . '" data-comment="' . $event['comment'] . '" data-fkclient="' . $event['fkClients']['pk_client'] . '">'
                                . $event['fkClients']['name_client'] . '(' . $event['duration'] . ') - ' . $username
                                . '</div>';
                        }
                    }
                    
                    echo '</div>';
                    if($alltime > 0) {
                        $cl = ($alltime >= $maxtime) ? ' text-red' : '';
                        echo '<div class="radio-alltime' . $cl . '">Всего: ' . $alltime . ' сек.</div>';
                    }
                    
                    echo '<button class="btn btn-primary btn-xs open-modal" data-date="' . $dymd . '" data-time="' . $time . '">+</button>';
                    echo '</td>';
                }?>
            </tr>
        <?php endforeach ?>
    </table>
</div>

<div class="modal fade" id="reportModal" role="dialog" aria-labelledby="reportModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="reportModalLabel">Формирование отчета</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <label>С (Дата)</label>
                        <?= DatePicker::widget([
                            'name' => 'start_date_report',
                            'name2' => 'end_date_report',
                            'type' => DatePicker::TYPE_RANGE,
                            'value' => FunctionModel::getDateWParam('Y-m-d'),
                            'value2' => FunctionModel::getDateWParam('Y-m-d'),
                            'separator' => 'по',
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]);?>
                    </div>
                    <div class="col-md-6">
                        <label><?= $mlabel->getAttributeLabel('fk_schedule_radio_client') ?></label>
                        <?= kartik\select2\Select2::widget([
                            'name' => 'fk_client',
                            'value' => '',
                            'data' => $clients,
                            'options' => [
                                'placeholder' => Yii::t('app', 'Select'),
                                'id' => 'report-fk_client'
                            ],
                            'pluginOptions' => [
                                'tokenSeparators' => [',']
                            ],
                        ]);?>
                    </div>
                </div>
                <div class="row mt-1em">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Дата</th>
                                    <th>Время</th>
                                    <th>Программа</th>
                                    <th><?= $mlabel->getAttributeLabel('duration') ?></th>
                                </tr>
                            </thead>
                            <tbody id="radio-report"></tbody>
                        </table>
                        <p>Итого: <span id="report-count"></span> прокатов</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-success hidden" href="" target="_blank" id="report-pathdocument">Скачать</a>
                <button class="btn btn-primary" id="radio-report-create">Сформировать</button>
                <button class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Создать/обновить время</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-5">
                    <h5><strong>Будущие ролики</strong></h5>
                    <div id="radio-events" style="height: 330px;overflow: auto;"></div>
                </div>
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Дата</label>                        
                            <?= DatePicker::widget([
                                'name' => 'start_date',
                                'type' => DatePicker::TYPE_INPUT,
                                'value' => FunctionModel::getDateWParam('Y-m-d'),
                                'options' => [
                                    'id' => 'radio-date'
                                ],
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'yyyy-mm-dd'
                                ]
                            ]);?>
                        </div>
                        <div class="col-md-6">
                            <label>Время</label>
                            <?= TimePicker::widget([
                                'name' => 'start_time', 
                                'value' => date('H:i'),
                                'options' => [
                                    'id' => 'radio-time'
                                ],
                                'pluginOptions' => [
                                    'showMeridian' => false,
                                    'minuteStep' => 5,
                                ]
                            ]);?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label><?= $mlabel->getAttributeLabel('duration') ?></label>
                            <div>
                                <input type="number" class="form-control" id="radio-duration">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label><?= $mlabel->getAttributeLabel('fk_schedule_radio_client') ?></label>
                            <?= kartik\select2\Select2::widget([
                                'name' => 'fk_client',
                                'value' => '',
                                'data' => $clients,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Select'),
                                    'id' => 'radio-fk_client'
                                ],
                                'pluginOptions' => [
                                    'tags' => true,
                                    'tokenSeparators' => [',']
                                ],
                            ]);?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Комментарий</label>
                            <div>
                                <input type="text" class="form-control" id="radio-comment">
                                <input type="hidden" id="radio-id">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="is-repeat-radio"> Повторить
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>С (Дата)</label>
                            <?= DatePicker::widget([
                                'name' => 'start_date_repeat',
                                'name2' => 'end_date_repeat',
                                'type' => DatePicker::TYPE_RANGE,
                                'value' => FunctionModel::getDateWParam('Y-m-d'),
                                'value2' => FunctionModel::getDateWParam('Y-m-d'),
                                'separator' => 'по',
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'yyyy-mm-dd'
                                ]
                            ]);?>
                        </div>
                        <div class="col-md-6">
                            <label>Время</label>
                            <?= TimePicker::widget([
                                'name' => 'repeat_start_time', 
                                'value' => date('H:i'),
                                'options' => [
                                    'id' => 'repeat-radio-time'
                                ],
                                'pluginOptions' => [
                                    'showMeridian' => false,
                                    'minuteStep' => 5,
                                ]
                            ]);?>
                        </div>
                        <div class="col-md-12">
                            <fieldset>
                                <label>Повторять по дням недели:</label>
                                <div>
                                    <label>
                                        <input type="checkbox" name="repeat_days" value="1" />Понедельник
                                    </label><br>
                                    <label>
                                        <input type="checkbox" name="repeat_days" value="2" />Вторник
                                    </label><br>
                                    <label>
                                        <input type="checkbox" name="repeat_days" value="3" />Среда
                                    </label><br>
                                    <label>
                                        <input type="checkbox" name="repeat_days" value="4" />Четверг
                                    </label><br>
                                    <label>
                                        <input type="checkbox" name="repeat_days" value="5" />Пятница
                                    <label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="row">
                <div class="col-md-6 text-left">
                    <button class="btn btn-danger" id="radio-delete-selected">Удалить выбранные</button>
                    <button class="btn btn-danger" id="radio-delete-all-time">Удалить клиента в этом времени</button>
                </div>
                <div class="col-md-6 text-right">
                    <button class="btn btn-danger" id="radio-delete">Удалить</button>
                    <button class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button class="btn btn-primary" id="radio-save">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

<?php
$this->registerJs("
    var event_fk_user = 0;
    var me_fk_user = " . Yii::$app->user->id . ";

    //клик кнопки создания
    $('#radio-create').click(function(){
        $('#radio-duration').val('');
        $('#radio-comment').val('');
        $('#radio-id').val('');
    });
    
    //открытие модального окна и взятие параметров от кнопки
    $('.open-modal').click(function(){
        var d = $(this).attr('data-date').split('-');
        var t = $(this).attr('data-time').split(':');
        $('#radio-date').kvDatepicker('update', new Date(d[0], parseInt(d[1], 10) - 1, d[2]));
        $('#radio-time').timepicker('setTime', t[0] + ':' + t[1]);
        $('#repeat-radio-time').timepicker('setTime', t[0] + ':' + t[1]);
        $('#radio-duration').val($(this).attr('data-duration'));
        $('#radio-comment').val($(this).attr('data-comment'));
        $('#radio-id').val($(this).attr('data-id'));
        $('#radio-fk_client').val($(this).attr('data-fkclient')).trigger('change');
        
        $('#myModal').modal();
        
        //получить данные событий по клиенту
        $.ajax({
            url: '". Url::toRoute('radio-schedule/infoevent') ."',
            type: 'get',
            data: {
                id: $(this).attr('data-id')
            },
            success: function(data) {
                $('#radio-events').html('');
                
                //скрыть кнопку у не своих роликов
                if(parseInt(data.model.fk_user, 10) === me_fk_user) {
                    $('#radio-delete').removeClass('hidden');
                } else {
                    $('#radio-delete').addClass('hidden');
                }
                
                //вывести в список
                for(var i = 0; i < data.events.length; i++) {
                    var inntext = '';
                    
                    if(parseInt(data.events[i].fk_user, 10) === me_fk_user) {
                        inntext = '<input type=\'checkbox\' data-id=\'' + data.events[i].id + '\'>';
                    }
                    
                    var username = data.infousers[data.events[i].fk_user].lastname;
                    $('#radio-events').append('<div class=\'checkbox\'><label> ' + inntext + data.events[i].date + ' ' + data.events[i].time + ' - ' + username + '(' + data.events[i].duration + ')</label></div>');
                }
            }
        });
    });
    
    $('#radio-report-create').click(function(){
        $.ajax({
            url: '". Url::toRoute('radio-schedule/report') ."',
            type: 'post',
            data: {
                start_date_report: $('input[name=\'start_date_report\']').val(),
                end_date_report: $('input[name=\'end_date_report\']').val(),
                pk_client: $('#report-fk_client').val(),
            },
            success: function (data) {
                if(data.success === true) {
                    $('#radio-report').html('');
                    
                    for(var i = 0; i < data.events.length; i ++) {
                        $('#radio-report').append('<tr><td>' + data.events[i].date + '</td><td>' + data.events[i].data.times + '</td><td>Ролики</td><td>' + data.events[i].data.duration + ' сек</td></tr>');
                    }
                    
                    //путь к сгенерированному файлу
                    if(data.pathdocument) {
                        $('#report-pathdocument').removeClass('hidden');
                        $('#report-pathdocument').attr('href', data.pathdocument);
                    } else {
                        $('#report-pathdocument').attr('href', '')
                        $('#report-pathdocument').addClass('hidden');
                    }
                    
                    $('#report-count').html(data.count);
                } else {
                    modelErrorShow(data.errors);
                }
            },
            beforeSend: function() {
                if($('#report-fk_client').val()) {
                    $('#report-fk_client').parent().removeClass('has-error');
                } else {
                    $('#report-fk_client').parent().addClass('has-error');
                    return false;
                }
            }
        });
    });
    
    $('#radio-save').click(function(){
        //подготовка дней для повтора
        var repeat_days = $('input[name=repeat_days]:checked');
        var arrRepeatDays = [];

        for(var i = 0; i < repeat_days.length; i++) {
            arrRepeatDays.push(parseInt(repeat_days[i].value, 10));
        }

        $.ajax({
            url: '". Url::toRoute('radio-schedule/update') ."',
            type: 'post',
            data: {
                date : $('#radio-date').val(),
                time: $('#radio-time').val(),
                duration: $('#radio-duration').val(),
                comment: $('#radio-comment').val(),
                id: $('#radio-id').val(),
                fk_schedule_radio_client: $('#radio-fk_client').val(),
                start_date_repeat: $('input[name=\'start_date_repeat\']').val(),
                end_date_repeat: $('input[name=\'end_date_repeat\']').val(),
                time_repeat: $('#repeat-radio-time').val(),
                arrRepeatDays: arrRepeatDays,
                is_repeat_radio: ($('#is-repeat-radio').prop('checked') === true) ? 1 : 0
            },
            success: function (data) {
                $('#myModal').modal('hide');
                if(data.success === true) {                    
                    jsNotify('Обновлено');
                    setTimeout(function() { location.reload() }, 1000);
                } else {
                    modelErrorShow(data.errors);
                }
            },
        });
    });
    
    //удалить все в этом времени. по ид определять всех
    $('#radio-delete-all-time').click(function(){
        $.ajax({
            url: '". Url::toRoute('radio-schedule/delete') ."',
            type: 'post',
            data: {
                findall: $('#radio-id').val()
            },
            success: function (data) {
                if(confirm('Действительно удалить ' + data.found + ' роликов?')) {
                    $.ajax({
                        url: '". Url::toRoute('radio-schedule/delete') ."',
                        type: 'post',
                        data: {
                            deleteall: $('#radio-id').val()
                        },
                        success: function (data) {
                            $('#myModal').modal('hide');
                            if(data.success === true) {                    
                                jsNotify('Удалено');
                                setTimeout(function() { location.reload() }, 1000);
                            } else {
                                modelErrorShow(data.errors);
                            }
                        }
                    });
                }
            },
        });
    });

    //удалить выбранные
    $('#radio-delete-selected').click(function() {
        var checkedfordel = document.querySelectorAll('#radio-events input:checked');
        var idstodel = [];
        
        for(var i = 0; i < checkedfordel.length; i++) {
            idstodel.push(parseInt(checkedfordel[i].getAttribute('data-id'), 10));
        }
        
        if(confirm('Действительно удалить?')) {
            $.ajax({
                url: '". Url::toRoute('radio-schedule/delete') ."',
                type: 'post',
                data: {
                    ids: idstodel
                },
                success: function (data) {
                    $('#myModal').modal('hide');
                    if(data.success === true) {
                        jsNotify('Удалено');
                        setTimeout(function() { location.reload() }, 1000);
                    } else {
                        jsNotify('Ошибка удаления', '', '', 'danger');
                    }
                }
            });
        }
    });

    $('#radio-delete').click(function(){
        if(confirm('Действительно удалить?')) {
            $.ajax({
                url: '". Url::toRoute('radio-schedule/delete') ."',
                type: 'post',
                data: {
                    id: $('#radio-id').val()
                },
                success: function (data) {
                    $('#myModal').modal('hide');
                    if(data.success === true) {
                        jsNotify('Удалено');
                        setTimeout(function() { location.reload() }, 1000);
                    } else {
                        jsNotify('Ошибка удаления', '', '', 'danger');
                    }
                }
            });
        }
    });
    
    //из докушки
    $('.sortable').sortable({
        connectWith: '.sortable',
        update: function(event, ui) {
            var order = $(this).sortable('toArray');
            var positions = order.join(';');

            $.ajax({
                url: '". Url::toRoute('radio-schedule/setsort') ."',
                type: 'post',
                data: {
                    date: $(this).attr('data-date'),
                    time: $(this).attr('data-time'),
                    positions: positions
                }
            });
        }
    });
    $('#draggable').draggable({
        helper: 'clone',
        revert: 'invalid'
    });
", \yii\web\View::POS_READY);
?>