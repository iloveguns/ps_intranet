<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Update Types clients dbp') .' '. $model->name_type_client;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Types clients dbp'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pk_type_client, 'url' => ['view', 'id' => $model->pk_type_client]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dbpartners-type-client-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>