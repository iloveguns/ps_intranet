<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = Yii::t('app/views', 'Types clients dbp');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Dbpartners'), 'url' => ['dbpartners/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dbpartners-type-client-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app/views', 'Create Types clients dbp'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'pk_type_client',
            'name_type_client:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>