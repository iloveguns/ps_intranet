<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Create Types clients dbp');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Types clients dbp'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dbpartners-type-client-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>