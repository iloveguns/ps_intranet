<?php
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use kartik\time\TimePicker;

$this->title = Yii::t('app/models', 'Drivers schedule');
?>
<h1><?= $this->title ?></h1>

<div class="col-lg-3 col-md-3">
    
<?php if(Yii::$app->user->id == 463 || Yii::$app->user->id == 75 || Yii::$app->user->id == 11 || Yii::$app->user->identity->isAdmin()) : //2 отдела ?>

    <?php 
        if(isset($_GET['date'])) $date = $_GET['date'];
        else $date = NULL;
        if(isset($_GET['dep'])) $dep = $_GET['dep'];
        else $dep = NULL;
    ?>
    
    <h3>
        <a style="margin-right: 20px" class="btn <?= ($dep == 'tv') ? 'btn-primary' : 'btn-default' ?>" href="<?= Url::to(['/intranet/driver-schedule/index', 'date' => $date, 'dep' => 'tv']) ?>">Телевидение</a>
        <a class="btn <?= ($dep == 'pr' || $dep === NULL) ? 'btn-primary' : 'btn-default' ?>" href="<?= Url::to(['/intranet/driver-schedule/index', 'date' => $date, 'dep' => 'pr']) ?>">Прожектор</a>
    </h3>

    <div>
        <hr>
        <h4>Заблокировать водителя</h4>
        <label class="control-label">Водитель</label>
        <?= kartik\select2\Select2::widget([
            'name' => 'inactive-driver',
            'language' => 'ru',
            'data' => $drivers,
            'options' => ['placeholder' => 'Выберите водителя ...', 'id' => 'inactive-driver'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <br>
        <button id="set-driver-inactive" class="btn btn-primary">Заблокировать</button>
    </div>
    <br>
    <div>
        <?php foreach ($inactives as $iac) : ?>
        <?php if(isset($drivers[$iac])) : //убрать после объединения ?>
            <div>
                <?= $drivers[$iac] ?> <button class="btn btn-success set-driver-active" data-id="<?= $iac ?>">Разблокировать</button>
            </div>
        <?php endif ?>
        <?php endforeach ?>
    </div>
    
<?php endif ?>
    
    <div>
        <hr>
        <label class="control-label">Дата</label>
        <?= DatePicker::widget([
            'name' => 'date-driver',
            'id' => 'date-driver',
            'type' => DatePicker::TYPE_INLINE,
            'value' => $date,
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]
        ]); ?>
    </div>
    
    <div>
        <hr>
        <h4>Добавить себя</h4>

        <label class="control-label">Водитель</label>
        <?= kartik\select2\Select2::widget([
            'name' => 'select-driver',
            'language' => 'ru',
            'data' => $actives,
            'options' => ['placeholder' => 'Выберите водителя ...', 'id' => 'select-driver'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>

        <label class="control-label">Дата/время с</label>
        <?php /* TimePicker::widget([
            'name' => 'date_from', 
            'pluginOptions' => [
                'showMeridian' => false,
            ],
            'options' => [
                'placeholder' => Yii::t('app','Enter date'), 'id' => 'date_from'
            ]
        ])*/ ?>
        <?php echo DateTimePicker::widget([
            'name' => 'date_from',
            'options' => ['placeholder' => Yii::t('app','Enter date'), 'id' => 'date_from'],
            'pluginOptions' => [
                'autoclose'=>true,
                'weekStart' => 1,
                'format' => 'yyyy-mm-dd hh:ii:ss'
            ]
        ]); ?>

        <label class="control-label">Дата/время до</label>
        <?php /* TimePicker::widget([
            'name' => 'date_to', 
            'pluginOptions' => [
                'showMeridian' => false,
            ],
            'options' => [
                'placeholder' => Yii::t('app','Enter date'), 'id' => 'date_to'
            ]
        ])*/ ?>
        <?php echo DateTimePicker::widget([
            'name' => 'date_to',
            'options' => ['placeholder' => Yii::t('app','Enter date'), 'id' => 'date_to'],
            'pluginOptions' => [
                'autoclose'=>true,
                'weekStart' => 1,
                'format' => 'yyyy-mm-dd hh:ii:ss'
            ]
        ]); ?>
    </div>

    <div class="mb-1em">
        <label class="control-label">Адрес(район)</label>
        <textarea id="address-driver" class="form-control"></textarea>
    </div>

    <button id="set-driver-schedule" class="btn btn-primary">Сохранить</button>
</div>

<div class="col-lg-hidden col-md-1"></div>
<div class="col-lg-9 col-md-8 panel panel-default">
    <?php if(empty($models)) : ?>
        <h3>В расписании водителя нет записей</h3>
    <?php endif ?>
        
    <?php foreach ($models as $id => $m) : ?>
    <div>
        <h3 class="text-center <?= (array_key_exists($id, $inactives)) ? 'text-stroke' : '' ?>"><?= $drivers[$id] ?></h3>
        <h4 class="text-center"><?= $dataDrivers[$id]['info'] ?></h4>
        <table class="table table-bordered text-center">
            <tr>
                <th width="15%">Время с</th>
                <th width="15%">Время до</th>
                <th>Ф.И.О</th>
                <th>Адрес(район)</th>
                <th></th>
                <th></th>
            </tr>
            <?php foreach ($m as $model) : ?>
                <tr>
                    <td><?= (isset($model['date_from'])) ? Yii::$app->formatter->asTime($model['date_from']) : '-' ?></td>
                    <td><?= (isset($model['date_to'])) ? Yii::$app->formatter->asTime($model['date_to']) : '-' ?></td>
                    <td><?= (isset($model['who'])) ? $model['who'] : '' ?></td>
                    <td><?= (isset($model['address'])) ? $model['address'] : '' ?></td>
                    <td>
                        <?php if(isset($model['who_id']) && (161 === Yii::$app->user->id || 75 === Yii::$app->user->id || Yii::$app->user->identity->isAdmin())) : ?>
                        <i class="fa fa-pencil text-blue change-driver-me" aria-hidden="true"
                           data-id="<?= $model['id'] ?>"
                           data-from="<?= substr($model['date_from'], 11,5) ?>"
                           data-to="<?= substr($model['date_to'], 11,5) ?>"></i>
                        <?php endif ?>
                    </td>
                    <td>
                        <?php if(isset($model['who_id']) && ($model['who_id'] === Yii::$app->user->id || 75 === Yii::$app->user->id || Yii::$app->user->identity->isAdmin())) : ?>
                            <i class="fa fa-times text-red del-driver-me" data-id="<?= $model['id'] ?>" aria-hidden="true"></i>
                        <?php endif ?>
                    </td>
                </tr>
            <?php endforeach ?>
        </table>
    </div>
    <?php endforeach ?>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Изменить время</h4>
      </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label>Дата/время с</label>
                    <?= TimePicker::widget([
                        'name' => 'start_time', 
                        'value' => '19:00',
                        'pluginOptions' => [
                            'showMeridian' => false,
                        ],
                        'options' => [
                            'id' => 'cdate_from',
                        ]
                    ]) ?>
                </div>
                <div class="col-md-6">
                    <label>Дата/время до</label>
                    <?= TimePicker::widget([
                        'name' => 'start_time', 
                        'value' => '11:24 AM',
                        'pluginOptions' => [
                            'showMeridian' => false,
                        ],
                        'options' => [
                            'id' => 'cdate_to',
                        ]
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            <button type="button" class="btn btn-primary" id="change_time">Сохранить</button>
        </div>
    </div>
  </div>
</div>

<?php
$add = 'var sendNotify = false;';
if(Yii::$app->params['sendNotify']) {
    $add = 'var sendNotify = true;';
}

$this->registerJs($add . "
    $('#set-driver-inactive').click(function(){
        if(parseInt($('#inactive-driver').val(), 10) > 0) {        
            $.ajax({
                url: '". Url::toRoute('driver-schedule/inactive') ."',
                type: 'post',
                data: {
                    val: 1,
                    id : parseInt($('#inactive-driver').val(), 10)
                },
                success: function (data) {
                    if(data.success === true) {
                        location.reload();
                    }
                }
            });
        }
    });
    
    $('.set-driver-active').click(function(){
        $.ajax({
            url: '". Url::toRoute('driver-schedule/inactive') ."',
            type: 'post',
            data: {
                val: 0,
                id : parseInt($(this).attr('data-id'), 10)
            },
            success: function (data) {
                if(data.success === true) {
                    location.reload();
                }
            }
        });
    });

    $('.change-driver-me').click(function(){
        $('#cdate_from').timepicker('setTime', $(this).attr('data-from'));
        $('#cdate_to').timepicker('setTime', $(this).attr('data-to'));
        window.update = $(this).attr('data-id');
        
        $('#myModal').modal();
    });
    
    $('#change_time').click(function(){
        console.log($('#cdate_from').val(), $('#cdate_to').val(), window.update);
        
        $.ajax({
            url: '". Url::toRoute('driver-schedule/update') ."',
            type: 'post',
            data: {
                date_from : $('#cdate_from').val(),
                date_to : $('#cdate_to').val(),
                id : window.update,
            },
            success: function (data) {
                if(data.success === true) {
                    $('#myModal').modal('hide');
                    jsNotify('Обновлено');
                    setTimeout(function() { location.reload() }, 1000);
                } else {
                    jsNotify('Ошибка обновления', '', '', 'danger');
                }
            },
            beforeSend: function() {
                if(sendNotify) { // если отправлять уведомление
                    jsNotify('Подождите, отправляется уведомление водителю');
                }
            }
        });
    });

    $('.del-driver-me').click(function(){
        $.ajax({
            url: '". Url::toRoute('driver-schedule/delete') ."',
            type: 'post',
            data: {
                id : $(this).attr('data-id'),
            },
            success: function (data) {
                if(data.success === true) {
                    jsNotify('Удалено');
                    setTimeout(function() { location.reload() }, 1000);
                } else {
                    jsNotify('Ошибка удаления', '', '', 'danger');
                }
            },
            beforeSend: function() {
                if(!confirm('Удалить время из списка?')) return false;
                
                if(sendNotify) { // если отправлять уведомление
                    jsNotify('Подождите, отправляется уведомление водителю');
                }
            }
        });
    });

    $('#set-driver-schedule').click(function(){
        var date_from = formatDateFull($('#date_from-datetime').data('datetimepicker').getDate());
        var date_to = formatDateFull($('#date_to-datetime').data('datetimepicker').getDate());
        var address = $('#address-driver').val();
        
        $.ajax({
            url: '". Url::toRoute('driver-schedule/create') ."',
            type: 'post',
            data: {
                date_from : date_from,
                date_to : date_to,
                address : address,
                id : $('#select-driver').val(),
            },
            success: function (data) {
                if(data.success === true) {
                    $('#date_from').val('');
                    $('#date_to').val('');
                    $('#address-driver').val('');
                    
                    jsNotify('Добавлено');
                    setTimeout(function() { location.reload() }, 1000);
                } else {
                    jsNotify(data.errors, '', '', 'danger');
                }
            },
            beforeSend: function() {
                if($('#date_from').val()) {
                    $('#date_from-datetime').removeClass('has-error');
                } else {
                    $('#date_from-datetime').addClass('has-error');
                    return false;
                }

                if($('#date_to').val()) {
                    $('#date_to-datetime').removeClass('has-error');
                } else {
                    $('#date_to-datetime').addClass('has-error');
                    return false;
                }

                if($('#address-driver').val()) {
                    $('#address-driver').parent().removeClass('has-error');
                } else {
                    $('#address-driver').parent().addClass('has-error');
                    return false;
                }
                
                if(sendNotify) { // если отправлять уведомление
                    jsNotify('Подождите, отправляется уведомление водителю');
                }
            }
        });
    });

    $('#date-driver').css({'display' : 'none'});
    
    $('#date-driver').change(function(){
        window.location = window.location.pathname + '?date=' + formatDate($('#date-driver-kvdate').kvDatepicker('getUTCDate'));
    });
", \yii\web\View::POS_READY);
?>