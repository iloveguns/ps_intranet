<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\intranet\models\Projects;
use app\modules\user\models\User;
use kartik\select2\Select2;
use app\components\WidgetStructure;
use kartik\datetime\DateTimePicker;
?>
<div class="projects-form">        
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput() ?>
    
    <?php
    echo $form->field($model, 'id_head')->widget(Select2::classname(), [
        'data' => User::getAllActive(),
        'options' => ['placeholder' => Yii::t('app', 'Select')],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]);    
    ?>
    
    <?= $form->field($model, 'target')->textarea(['rows' => 3]) ?>    
    
    <?= $form->field($model, 'content')->textarea(['tinymce' => 'tinymce']) ?>

    <?= $form->field($model, 'status')->dropDownList(Projects::getStatus()) ?>

    <div class="panel panel-default">
        <div class="panel-body">
            <?= $form->field($model, 'start_date', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => Yii::t('app','Enter date')],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd hh:ii:ss'
                ]
            ]);    
            ?>

            <?= $form->field($model, 'end_date', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => Yii::t('app','Enter date')],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd hh:ii:ss'
                ]
            ]);    
            ?>
        </div>
    </div>
    
    <?php $model->direction = $model->idDirections; // присвоить для удобства ?>
    
    <?= $form->field($model, 'direction')->widget(Select2::classname(), [
        'data' => app\modules\intranet\models\ProjectDirections::getDirections(),
        'options' => ['placeholder' => Yii::t('app', 'Select'), 'multiple' => true],
        'pluginOptions' => [
            'tags' => true,
            'allowClear' => true,
        ],
    ]) ?>
        
    <div class="panel panel-default">
        <?= WidgetStructure::widget(['model' => $model]) ?>
    </div>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
