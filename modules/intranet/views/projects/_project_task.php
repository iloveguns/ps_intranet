<?php
use yii\helpers\Html;
use app\modules\user\models\User;
use app\modules\user\models\Units;
?>
<div class="panel panel-default project_task">
    <div class="panel-heading">
        <?= Html::a($task->title, '#task'.$task->pk_task, ['data-toggle' => 'collapse']) ?>
        <span class="pull-right task-date-range"><?=Yii::$app->formatter->asDate($task->start_date)?> - <?=Yii::$app->formatter->asDate($task->end_date)?></span>
    </div>
    <div id="task<?=$task->pk_task?>" class="panel-collapse collapse in">
        <div class="panel-body">
            <div class="col-md-8">
                <div class="well">
                    <?= $task->content?>
                </div>
                <h4>Место для отчета</h4>
            </div>
            <div class="col-md-4">
                <div class="well">
                    <ul>
                        <li><?= Yii::t('app/models', 'Start Date') ?> : <?=Yii::$app->formatter->asDate($task->start_date)?></li>
                        <li><?= Yii::t('app/models', 'End Date') ?> : <?=Yii::$app->formatter->asDate($task->end_date)?></li>
                        <li><?= Yii::t('app/models', 'Id Author') ?> : <?= User::getAll($task->id_author)?></li>
                        <li><?= Yii::t('app/models', 'Id Head') ?> : <?= User::getAll($task->id_head)?></li>
                    </ul>
                </div>
                <?= Html::a(Yii::t('app/models', 'Go to task'), ['tasks/view', 'id'=>$task->pk_task], ['class' => 'btn btn-success col-md-12', 'target' => '_blank']) ?>
            </div>
            <h4><?= Yii::t('app/models', 'Executors')?></h4>
            <div class="col-md-12 well">
                <?php foreach (Units::onlyIdUsers($task) as $user){//поиск по сотрудникам из всей структуры ?>
                <?php $taskDoneInfo = $user->getTaskUserInfo($task); //есть ли записи о выполнении задачи ?>
                <div class="col-md-2">
                    <div class="col-md-6 user-margin">
                        <?= $user->getLinkAvatar(true) ?>
                    </div>
                    <div class="col-md-6">
                        <?= \app\models\EntityUserDone::userDoneLabel($taskDoneInfo['complete']) ?>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
