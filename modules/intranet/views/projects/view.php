<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\user\models\Units;
use app\modules\intranet\IntranetModule;
use app\modules\user\models\User;
use app\modules\intranet\models\Projects;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => IntranetModule::t('all', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row row-cols projects-view">
    
    <h1><?= Html::encode($this->title) ?></h1>
    
    <div class="col-md-9">
        <?php if(Yii::$app->user->identity->isAdmin() || $model->canAdmin(Yii::$app->user->id)) : ?>
        <p>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->pk_project], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->pk_project], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <?php endif ?>

        <div class="alert alert-info">
            <ul>
                <li><?=$model->getAttributeLabel('create_date')?>: <?=Yii::$app->formatter->asDatetime($model->create_date)?></li>
                <li><?=$model->getAttributeLabel('update_date')?>: <?=Yii::$app->formatter->asDatetime($model->update_date)?></li>
                <li><?=$model->getAttributeLabel('start_date')?>: <?=Yii::$app->formatter->asDatetime($model->start_date)?></li>
                <li><?=$model->getAttributeLabel('end_date')?>: <?=Yii::$app->formatter->asDatetime($model->end_date)?></li>
                <li><?=IntranetModule::t('all', 'Duration')?>: 
                    <?php
                    $datetimeFrom = date_create($model->end_date);
                    $datetimeTo = date_create($model->start_date);
                    echo $datetimeFrom->diff( $datetimeTo )->days;
                    ?>дн.
                </li>
            </ul>
        </div>
        
        <div class="alert <?= ($model->status === $model::STATUS_DONE) ? 'alert-success' : 'alert-info'?>"><?= (Yii::$app->formatter->asDate(time()) > Yii::$app->formatter->asDate($model->end_date)) ? IntranetModule::t('all', 'Project time out') : $model->getStatus($model->status)?></div>        

        <div>
            <p><strong><?=$model->getAttributeLabel('id_author')?>: </strong><?= User::getAll($model->id_author)?></p>
            <p><strong><?=$model->getAttributeLabel('id_head')?>: </strong><?= User::getAll($model->id_head)?></p>        

            <h3><?=$model->getAttributeLabel('target')?>:</h3>
            <div class="well"><?=$model->target?></div>
            
            <?php if($model->directions) : ?>
                <h3><?=$model->getAttributeLabel('direction')?>:</h3>
                <div class="well"><?= $model->directions ?></div>
            <?php endif ?>

            <h3><?=$model->getAttributeLabel('content')?>:</h3>
            <div class="well"><?=$model->content?></div>
        </div>

        <div>
            <h3><?= IntranetModule::t('all', 'Project for')?>:</h3>
            
            <div id="selectedStructuresListForm">
                <?= Units::StructInfo($model)//полная инфа Nestable ?>
            </div>
        </div>
        
        <div>
            <?php if($model->tasks) :?>
                <h3><?= IntranetModule::t('all', 'Tasks in this project')?>:</h3>
                <?php foreach ($model->tasks as $task) {
                    echo $this->render('_project_task', ['task' => $task]);
                }?>
            <?php endif ?>
        </div>
        <hr>
        <?= app\components\WidgetComments::widget(['modelEntity' => $model])?>
    </div>
    
    <div class="col-md-3">
        <?= $this->render(Yii::$app->params['themeFolder'].'/_rsidebar', ['model' => $model]) ?>
    </div>
</div>
