<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\intranet\models\Projects;
use app\modules\intranet\IntranetModule;
use app\modules\user\models\User;
use kartik\select2\Select2;
use kartik\date\DatePicker;

$this->title = IntranetModule::t('all', 'Projects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-index">
    
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(IntranetModule::t('all', 'Create Project'), ['create'], ['class' => 'btn btn-success']) ?>
        <?php if(Yii::$app->user->identity->isAdmin()) echo Html::a(IntranetModule::t('all', 'All projects'), ['all-projects'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'status',
                'value' => function ($model, $key, $index, $column) {
                    return Projects::getStatus($model->status);
                },
                'filter' => Projects::getStatus(),
            ],
            [
                'attribute' => 'title',
                'value' => function ($model, $key, $index, $column) {
                    return Html::a($model->title, ['view', 'id' => $model->pk_project]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'start_date',
                'headerOptions' => ['width' => '220px'],
                'value' => function ($model, $key, $index, $column) {
                    return Yii::$app->formatter->asDate($model->start_date);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel, 
                    'attribute' => 'start_date',
                    'removeButton' => false,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]),
            ],
            [
                'attribute' => 'end_date',
                'headerOptions' => ['width' => '220px'],
                'value' => function ($model, $key, $index, $column) {
                    return Yii::$app->formatter->asDate($model->end_date);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel, 
                    'attribute' => 'end_date',
                    'removeButton' => false,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]),
            ],
            [
                'attribute' => 'id_author',
                'value' => function ($model, $key, $index, $column) {
                    return Html::a(User::getAll($model->id_author), ['/user/user/profile', 'id' => $model->id_author], ['target' => '_blank']);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'id_author',
                    'data' => User::getAll(),
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'format' => 'raw',
            ],
            [
                'attribute' => 'direction',
                'value' => function ($model, $key, $index, $column) {
                    return $model->directions;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'direction',
                    'data' => app\modules\intranet\models\ProjectDirections::getDirections(),
                    'options' => ['placeholder' => '', 'multiple' => true],
                    'pluginOptions' => [
                        'tags' => true,
                        'allowClear' => true
                    ],
                ]),
                'format' => 'raw',
            ],
        ],
        'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
        'pager' => [
            'class' => \liyunfang\pager\LinkPager::className(),
            'pageSizeList' => Yii::$app->params['pageSizeList'],
            'pageSizeOptions' => ['class' => 'form-control pagesizeselect'],
        ],
    ]); ?>

</div>
