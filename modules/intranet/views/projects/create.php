<?php

use yii\helpers\Html;
use app\modules\intranet\IntranetModule;

$this->title = IntranetModule::t('all', 'Create Project');
$this->params['breadcrumbs'][] = ['label' => IntranetModule::t('all', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
