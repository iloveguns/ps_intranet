<?php
return[
    'Projects' => 'Проекты',
    'All projects' => 'Все проекты',
    'Create Project' => 'Создать проект',
    'Project for' => 'Проект предназначен для',
    'The task is part of a project' => 'Задача входит в проект',
    'Task for' => 'Задача предназначена для',
    'Tasks in this project' => 'Задачи в проекте',
    'Report' => 'Отчет',
    'Text Event' => 'Текст',
    'Calendar Director' => 'Календарь руководителя СМГ',
    
    'Pk Project' => 'ИД проекта',
    'Withous status' => 'Без статуса',
    'Duration' => 'Продолжительность',
    'Project time out' => 'Время проекта истекло',
    
    
    'Subtask text' => 'Подзадачи',
    
    'Callboards' => 'Доска объявлений',
    'Create Callboard' => 'Создать объявление',
    'Update Callboard' => 'Редактировать объявление',
    'Pk Callboard' => 'Ид объявления',
    'Title Callboard' => 'Заголовок',
    'Text Callboard' => 'Содержимое',
    'Fk Callboard Category' => 'Категория',
    'Id Author Callboard' => 'Автор объявления',
    'Callboard for' => 'Объявление предназначено для',
    
    'Create Event Company' => 'Создать запись',
    'Change Event Company' => 'Изменить запись',
    
    'Create Event' => 'Создать событие',
    'All Events' => 'Все события',
    'Update Event' => 'Изменить событие',
    'Or Notify From' => 'Или уведомлять с',
    'Not Notify' => 'Не уведомлять',
    'Notify Every' => 'Каждые',
    'Notify Time At' => 'Уведомить за',
    'Event For' => 'Событие предназначено для',
    'Is Public Event' => 'Публичное событие - делает запись в вашем календаре видимой всем пользователям Интранета',
];