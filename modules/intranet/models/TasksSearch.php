<?php

namespace app\modules\intranet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\intranet\models\Tasks;

class TasksSearch extends Tasks {
    public $onlyIds; // поиск только по ид
    
    public function rules() {
        return [
            [['pk_task', 'id_author', 'id_head', 'status', 'project_id'], 'integer'],
            [['title', 'content', 'update_date', 'start_date', 'end_date', 'create_date'], 'safe'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params, $isAdmin = false) {
        $query = Tasks::find();
        
        $pageSize = isset($_GET['per-page']) ? intval($_GET['per-page']) : Yii::$app->params['default_Pagination_PageSize'];

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['pk_task'=>SORT_DESC]],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);
        
        //ид задач
        $this->onlyIds = isset($_GET['ids']) ? $_GET['ids'] : FALSE;

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        
        //только по ид, остальное не должно влиять
        if($this->onlyIds) {
            $query->where(['in','pk_task', explode(',', $this->onlyIds)]);
        } else {
            if(!$isAdmin){//сотруднику - только свои задачи -=(Yii::$app->user->identity->isAdmin())
                $query->where(['in','pk_task', \app\modules\user\models\User::getAllIdEntityUser(Tasks::getName())]);
            }

            if($this->status == Tasks::STATUS_LATEBYTIME){
                $query->andFilterWhere(['<=', 'end_date', \app\models\FunctionModel::getDateTimestamp()])->andFilterWhere(['!=', 'status', Tasks::STATUS_DONE]);
            } else if($this->status == Tasks::STATUS_DONELATEBYTIME){
                $query->andFilterWhere(['<=', 'end_date', \app\models\FunctionModel::getDateTimestamp()])->andFilterWhere(['=', 'status', Tasks::STATUS_DONE]);
            } else if($this->status == Tasks::STATUS_ALLNOTDONE){
                $query->andFilterWhere(['!=', 'status', Tasks::STATUS_DONE])->andFilterWhere(['!=', 'status', Tasks::STATUS_FAIL])->andFilterWhere(['!=', 'status', Tasks::STATUS_DONEBYADMIN]);
            } else {//просроченные - поиск по времени(а если просроченная и выполненная???)
                $query->andFilterWhere([
                    'status' => $this->status,
                ]);            
            }

            if(Yii::$app->cache->get('task_mode_search'+Yii::$app->user->id) == 'quest'){
                $query->andFilterWhere([
                    'quest' => 1,
                ]);
            } else {
                $query->andFilterWhere([
                    'quest' => 0,
                ]);
            }

            $query->andFilterWhere([
                'pk_task' => $this->pk_task,
                'id_author' => $this->id_author,
                'id_head' => $this->id_head,
                'project_id' => $this->project_id,
            ]);

            $query->andFilterWhere(['like', 'title', $this->title])
                ->andFilterWhere(['like', 'content', $this->content])
                ->andFilterWhere(['like', 'start_date', $this->start_date])
                ->andFilterWhere(['like', 'update_date', $this->update_date])
                ->andFilterWhere(['like', 'create_date', $this->create_date])
                ->andFilterWhere(['like', 'end_date', $this->end_date])
                ;

            if($this->status == Tasks::STATUS_DONELATEBYTIME){
                $dat = $dataProvider;

                $i=0;
                $a = [];
                foreach ($dat->models as $model) {
                    if($model->update_date > $model->end_date){
                        $a[] = $model;
                    }
                    ++$i;
                }
                $dat->models = $a;

                return $dat;
            }
        }
        
        return $dataProvider;
    }
}
