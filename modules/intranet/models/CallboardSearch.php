<?php

namespace app\modules\intranet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\intranet\models\Callboard;

class CallboardSearch extends Callboard
{
    public function rules()
    {
        return [
            [['pk_callboard', 'fk_callboard_category', 'id_author'], 'integer'],
            [['title', 'text', 'create_date'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    /*
     * основной запрос для поиска по критерию с разными дивизионами и тд
     */
    public function querySearch() {
        $query = Callboard::find()->distinct();//с таким запросом легко можно словить повторы
        $query->leftJoin('relation_to_organization', self::tableName().'.pk_callboard = relation_to_organization.item_id');//организация
        $query->leftJoin('relation_to_divizion', self::tableName().'.pk_callboard = relation_to_divizion.item_id');//дивизион
        $query->leftJoin('relation_to_department', self::tableName().'.pk_callboard = relation_to_department.item_id');//отдел
        $query->leftJoin('relation_to_user', self::tableName().'.pk_callboard = relation_to_user.item_id');//персонально
        //условие для поиска + автор NULL(автообъявления интранета)
        $query->where('('.self::tableName().'.id_author IS NULL) OR (relation_to_department.class = :classname AND relation_to_department.id_department = :id_dep) OR (relation_to_organization.class = :classname AND relation_to_organization.id_organization = :id_org) OR (relation_to_divizion.class = :classname AND relation_to_divizion.id_divizion = :id_div) OR (relation_to_user.class = :classname AND relation_to_user.id_user = :user_id)', [':id_div' => Yii::$app->user->identity->fk_divizion, ':id_org' => Yii::$app->user->identity->fk_organization, ':user_id' => Yii::$app->user->id, ':id_dep' => Yii::$app->user->identity->fk_department, ':classname' => Callboard::getName()]);
        
        return $query;
    }
    
    /**
     * чистый запрос быстрее и легче, чем ar
     */
    public function search($params)
    {
        $query = $this->querySearch();
        
        $pageSize = isset($_GET['per-page']) ? intval($_GET['per-page']) : Yii::$app->params['default_Pagination_PageSize'];
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['pk_callboard'=>SORT_DESC]],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'pk_callboard' => $this->pk_callboard,
            //'create_date' => $this->create_date,
            'fk_callboard_category' => $this->fk_callboard_category,
            'id_author' => $this->id_author,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'create_date', $this->create_date])
            ->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }
}