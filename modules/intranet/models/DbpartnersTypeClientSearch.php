<?php

namespace app\modules\intranet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\intranet\models\DbpartnersTypeClient;

/**
 * DbpartnersTypeClientSearch represents the model behind the search form about `app\modules\intranet\models\DbpartnersTypeClient`.
 */
class DbpartnersTypeClientSearch extends DbpartnersTypeClient
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pk_type_client'], 'integer'],
            [['name_type_client'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DbpartnersTypeClient::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pk_type_client' => $this->pk_type_client,
        ]);

        $query->andFilterWhere(['like', 'name_type_client', $this->name_type_client]);

        return $dataProvider;
    }
}