<?php

namespace app\modules\intranet\models;

use Yii;

/*
 * Класс для связи проект-направление
 */
class ProjectToDirection extends \yii\db\ActiveRecord {
    public static function tableName() {
        return 'project_to_direction';
    }

    public function rules() {
        return [
            [['fk_project', 'fk_project_direction'], 'required'],
            [['fk_project', 'fk_project_direction'], 'integer'],
            [['fk_project_direction'], 'exist', 'skipOnError' => true, 'targetClass' => ProjectDirections::className(), 'targetAttribute' => ['fk_project_direction' => 'pk_direction']],
            [['fk_project'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['fk_project' => 'pk_project']],
        ];
    }

    public function attributeLabels() {
        return [
            'fk_project' => Yii::t('app/models', 'Fk Project'),
            'fk_project_direction' => Yii::t('app/models', 'Fk Project Direction'),
        ];
    }

    public function getFkProjectDirection() {
        return $this->hasOne(ProjectDirections::className(), ['pk_direction' => 'fk_project_direction']);
    }

    public function getFkProject() {
        return $this->hasOne(Projects::className(), ['pk_project' => 'fk_project']);
    }
}