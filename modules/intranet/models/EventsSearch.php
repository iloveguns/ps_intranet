<?php

namespace app\modules\intranet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\intranet\models\Events;

/**
 * EventsSearch represents the model behind the search form about `app\modules\intranet\models\Events`.
 */
class EventsSearch extends Events
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pk_event', 'id_author'], 'integer'],
            [['title', 'text', 'start_date', 'end_date', 'create_date', 'update_date', 'json_repeat', 'json_notify'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $isAdmin = false)
    {
        $query = Events::find();

        $pageSize = isset($_GET['per-page']) ? intval($_GET['per-page']) : Yii::$app->params['default_Pagination_PageSize'];

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['pk_event'=>SORT_DESC]],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if(!$isAdmin){//сотруднику - только свои задачи -=(Yii::$app->user->identity->isAdmin())
            $query->where(['in','pk_event', \app\modules\user\models\User::getAllIdEntityUser(Events::getName())]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pk_event' => $this->pk_event,
            'id_author' => $this->id_author,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'json_repeat', $this->json_repeat])
            ->andFilterWhere(['like', 'json_notify', $this->json_notify]);

        return $dataProvider;
    }
}