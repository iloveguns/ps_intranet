<?php

namespace app\modules\intranet\models;

use Yii;
use app\modules\user\models\User;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use app\modules\intranet\IntranetModule;
use app\models\UploadFiles;
use app\modules\intranet\models\CallboardCategory;
use app\modules\user\models\Units;
use app\models\UnreadNotice;
use yii\helpers\Html;
use yii\helpers\Url;

class Callboard extends ActiveRecord implements \app\modules\intranet\interfaces\RelationsUnits, \app\modules\intranet\interfaces\AllInterface, \app\interfaces\ClassnameInterface
{
    const BIRTHDAY = 3;//дни рождения
    const HOLIDAY = 15;//праздники
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date'],
                ],
                'value' => function() { return \app\models\FunctionModel::getDateTimestamp();},
            ],
            [
                'class' => \app\behaviors\DeleteRelations::className(),
                'className' => $this->getName(),
                'pk' => $this->primaryKey(),
            ],
        ];
    }
    
    public static function tableName()
    {
        return 'callboard';
    }

    public function rules()
    {
        return [
            [['title', 'text'], 'string'],
            [['text', 'title'], 'required'],
            [['create_date', 'link_to_entity'], 'safe'],
            [['fk_callboard_category', 'id_author'], 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_callboard' => IntranetModule::t('all', 'Pk Callboard'),
            'title' => IntranetModule::t('all', 'Title Callboard'),
            'text' => IntranetModule::t('all', 'Text Callboard'),
            'create_date' => IntranetModule::t('all', 'Create Date'),
            'fk_callboard_category' => IntranetModule::t('all', 'Fk Callboard Category'),
            'id_author' => IntranetModule::t('all', 'Id Author Callboard'),
        ];
    }

    public function getTitleText() {
        return $this->title;
    }
    
    public function getHtmlLink($icon = '', $mail = false, $type_event = NULL) {        
        $link = Html::a($icon.' '.strip_tags($this->title), $this->getUrl($mail, $type_event),['title' => strip_tags($this->title)]);
        return $link;
    }
    
    public function getUrl($absolute = false) {
        return Url::to(['/intranet/callboard/view', 'id' => $this->pk_callboard], $absolute);
    }
    
    /*
     * подсчет комментариев
     * @return int
     */
    public function countComments() {
        $model = \app\models\Comments::find()->where(['class' => $this->getName(), 'item_id' => $this->getPrimaryKey()])->count();
        return $model;
    }
    /**
     * получить категорию объявления
     */
    public function getFkCategory()
    {
        return $this->hasOne(CallboardCategory::className(), ['pk_category' => 'fk_callboard_category']);
    }

    /**
     * получить ид автора
     */
    public function getIdAuthor()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_author']);
    }
    
    /*
     * получить файлы
     */
    public function getFiles() {
        return UploadFiles::findAll(['class' => self::getName(), 'item_id' => $this->pk_callboard]);
    }
    
    /*abastract
     * связь с сотрудниками
     * $m = Projects::findOne(1);
     * var_dump($m->users[0]->id);
     */
    public function getUsers()
    {
        return $this->hasMany(\app\modules\user\models\User::className(), ['id_user' => 'id_user'])
            ->viaTable('relation_to_user', ['item_id' => 'pk_callboard'], function($query){
                $query->andWhere(['class' => Callboard::getName()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * связь с отделами
     * $m = Projects::findOne(1);
     * $m->departments[0]->users[0]->id
     */
    public function getDepartments()
    {
        return $this->hasMany(\app\modules\user\models\Department::className(), ['pk_department' => 'id_department'])
            ->viaTable('relation_to_department', ['item_id' => 'pk_callboard'], function($query){
                $query->andWhere(['class' => Callboard::getName()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * связь с дивизионами
     * $m = Projects::findOne(1);
     * $m->divizions[0]->departments[0]->users[0]->id
     */
    public function getDivizions()
    {
        return $this->hasMany(\app\modules\user\models\Divizion::className(), ['pk_divizion' => 'id_divizion'])
            ->viaTable('relation_to_divizion', ['item_id' => 'pk_callboard'], function($query){
                $query->andWhere(['class' => Callboard::getName()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * связь с организациями
     * $m = Projects::findOne(1);
     * $m->organizations[0]->divizions[0]->departments[0]->users[0]->id
     */
    public function getOrganizations()
    {
        return $this->hasMany(\app\modules\user\models\Organization::className(), ['pk_organization' => 'id_organization'])
            ->viaTable('relation_to_organization', ['item_id' => 'pk_callboard'], function($query){
                $query->andWhere(['class' => Callboard::getName()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * ссылка на себя
     * $id_comment - можно передать ид коммента и ссылка будет с ним
     */
    public function getSelfLink($id_comment = NULL) {
        $p = ['/intranet/callboard/view', 'id' => $this->pk_callboard];
        if($id_comment) $p['#'] = 'comment-'.$id_comment;
        return \yii\helpers\Url::to($p);
    }
    
    /*
     * имеет ли сотрудник доступ к администрированию задачи
     * только автор может редактировать
     */
    public function canAdmin($id) {
        return $this->id_author === $id;
    }
    
    public function beforeDelete(){
        UnreadNotice::deleteAll(['item_id' => $this->primaryKey(), 'class' => self::getName()]);//уведомления
        
        return parent::beforeDelete();
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord && isset(Yii::$app->user)){//для консольного приложения
                $this->id_author = Yii::$app->user->id;//автор
            }
            return true;
        }
        return false;
    }
    
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
        
        if(Yii::$app->id != 'basic-console'){//консольное приложение(cron)
            \app\components\WidgetGlobalUsed::save($this);
            
            if(isset(Yii::$app->request->post()["selectedStructuresData"])){//не только при создании срабатывает, а и при update
                //сохранение структуры
                $selectedStructure = Yii::$app->request->post()["selectedStructuresData"];//json выбранных структур                
                Structures::saveStructure($selectedStructure, $this->pk_callboard, $this::getName());//сохранение
            }

            if($insert){//новая запись
                //записать в непрочитанные всех, кого выбрали
                UnreadNotice::saveByIds($this, UnreadNotice::NEW_CALLBOARD);
            }
        }
    }

    public function getName() {
        return 'callboard';
    }

}
