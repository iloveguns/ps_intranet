<?php

namespace app\modules\intranet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\intranet\models\ProjectDirections;

class ProjectDirectionsSearch extends ProjectDirections{
    public function rules()
    {
        return [
            //[['pk_direction'], 'integer'],
            [['name_direction'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ProjectDirections::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'pk_direction' => $this->pk_direction,
        ]);

        $query->andFilterWhere(['like', 'name_direction', $this->name_direction]);

        return $dataProvider;
    }
}