<?php

namespace app\modules\intranet\models;

use Yii;
use app\modules\user\models\User;
use app\models\UploadFiles;
use yii\db\ActiveRecord;
use app\modules\intranet\models\Structures;
use app\models\UnreadNotice;
use yii\helpers\Url;
use yii\helpers\Html;

class Bugtracker extends \yii\db\ActiveRecord implements \app\modules\intranet\interfaces\AllInterface, \app\interfaces\ClassnameInterface
{
    public $url = 'intranet';
    public $unread;//тип контроллера
    public $sendWhom;//ид сотрудников, кому рассылать (массив)

    public function __construct($type = NULL) {
        parent::__construct();
        $this->type = $type;
        $this->check($type);
    }
    
    public function check($type = NULL) {
        switch ($type) {
            case 'bugtracker' :
                $this->sendWhom = User::getAdmins();
                $this->unread = UnreadNotice::NEW_BUGTRACKER;
                break;
            
            case 'crm-bugtracker' :
                $this->url = 'crm';
                $this->sendWhom = [1];
                $this->unread = UnreadNotice::NEW_BUGTRACKER;
                break;
            
            case 'sysadmquery' :
                $this->sendWhom = [2];
                $this->unread = UnreadNotice::NEW_SYSADMINQUERY;
                break;
            
            case 'stewardquery' :
                $this->sendWhom = [75];
                $this->unread = UnreadNotice::NEW_STEWARDQUERY;
                break;
        }
        return $this->sendWhom;
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_date'],
                ],
                'value' => function() { return \app\models\FunctionModel::getDateTimestamp();},
            ],
            [
                'class' => \app\behaviors\DeleteRelations::className(),
                'className' => $this->getName(),
                'pk' => $this->primaryKey(),
            ],
        ];
    }
    
    public static function tableName()
    {
        return 'bugtracker';
    }

    public function rules()
    {
        return [
            [['text', 'type'], 'string'],
            [['text'], 'required'],
            [['create_date', 'update_date', 'done_date'], 'safe'],
            [['urgency', 'pk_author', 'admin_response'], 'integer'],
            [['pk_author'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['pk_author' => 'id_user']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk' => 'Pk',
            'text' => Yii::t('app/models', 'Text bug'),
            'create_date' => Yii::t('app/models', 'Create Date'),
            'update_date' => Yii::t('app/models', 'Update Date'),
            'done_date' => Yii::t('app/models', 'Done Date'),
            'urgency' => Yii::t('app/models', 'Bug Urgency'),
            'pk_author' => Yii::t('app/models', 'Id Author'),
            'admin_response' => Yii::t('app/models', 'Bug Done'),
        ];
    }


    public function getPkAuthor()
    {
        return $this->hasOne(User::className(), ['id_user' => 'pk_author']);
    }
    
    /*
     * чистый урл
     * $mail(true) - абсолютный урл
     */
    public function getUrl($absolute = false) {
        $this->check($this->type);//определиться
        return Url::to(['/'.$this->url.'/'.$this->type.'/view', 'id' => $this->primaryKey], $absolute);
    }
    
    public function getTitleText() {
        return false;
    }
    
    /*
     * ссылка на просмотр сущности
     * Callboard::findOne(1)->htmlLink;
     * 
     * $icon - font-awesome icon
     */
    public function getHtmlLink($icon = '', $mail = false) {
        $linktext = $icon;
        $params = ['title' => strip_tags($linktext)];
                
        if(!$mail){//добавить кол-во комментариев
            $linktext .= '<span class="label label-info pull-right">'.$this->countComments().'</span>';
        }
        
        $link = Html::a($linktext, $this->getUrl($mail), $params);
        return $link;
    }
    
    public function getSelfLink() {
        
    }
    
    /*
     * подсчет комментариев
     * @return int
     */
    public function countComments() {
        $model = \app\models\Comments::find()->where(['class' => $this->getName(), 'item_id' => $this->getPrimaryKey()])->count();
        return $model;
    }
    
    /*
     * получить файлы
     */
    public function getFiles() {
        return UploadFiles::findAll(['class' => self::getName(), 'item_id' => $this->pk]);
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord){                
                $this->pk_author = Yii::$app->user->id;//автор
            }
        return true;
        }
        return false;
    }
    
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
        $people = User::getAdmins();
        $people[Yii::$app->user->id] = Yii::$app->user->id;
        
        Structures::saveRelationStructure(['user' => $people], $this->getPrimaryKey(), $this::getName());//сохранение связей? можно и без них, только уведомления кинуть

        if($insert){//новая запись
            //записать в непрочитанные всех, кого выбрали
            UnreadNotice::saveByIds($this, $this->unread, $this->sendWhom);
        }
    }
    
    /*
     * срочност исполнения
     */
    public function getUrgency($id = NULL) {
        $arr = [
            1 => 'В течение дня',
            2 => 'В течении недели',
            ];
        if($id) return $arr[$id];
        return $arr;
    }

    public function getName() {
        return 'bugtracker';
    }

}