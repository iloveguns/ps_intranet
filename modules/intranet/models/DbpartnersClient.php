<?php

namespace app\modules\intranet\models;

use Yii;
use app\modules\user\models\User;
use app\modules\intranet\models\DbpartnersTypeClient;
use app\modules\intranet\models\DbpartnersCongratulations;
use app\modules\intranet\models\DbpartnersRelations;
use app\modules\intranet\IntranetModule;

class DbpartnersClient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dbpartners_client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_client', 'appointment_client', 'type_client'], 'required'],
            [['sex', 'name_client', 'appointment_client', 'responsible'], 'string'],
            [['type_client', 'fk_user'], 'integer'],
            [['birthdate_client'], 'safe'],
            [['fk_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_user' => 'id_user']],
            [['type_client'], 'exist', 'skipOnError' => true, 'targetClass' => DbpartnersTypeClient::className(), 'targetAttribute' => ['type_client' => 'pk_type_client']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_client' => Yii::t('app/models', 'Pk Client'),
            'name_client' => Yii::t('app/models', 'Name Client'),
            'appointment_client' => Yii::t('app/models', 'Appointment Client'),
            'type_client' => Yii::t('app/models', 'Type Client'),
            'birthdate_client' => Yii::t('app/models', 'Birthdate Client'),
            'sex' => Yii::t('app/models', 'Sex Client'),
            'responsible' => Yii::t('app/models', 'Responsible'),
            'fk_user' => Yii::t('app/models', 'Fk User'),
        ];
    }
    
    public function beforeSave($insert) {        
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord){
                $this->fk_user = Yii::$app->user->id;//автор
            }
        return true;
        }
        return false;
    }
    
    public function beforeDelete() {
        if (parent::beforeDelete()) {
            DbpartnersRelations::deleteAll(['fk_client' => $this->pk_client]);//удалить связи
            return true;
        } else {
            return false;
        }
    }
    
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        $congatulations = Yii::$app->request->post('events');//поздравления
        
        DbpartnersRelations::deleteAll(['fk_client' => $this->pk_client]);
        
        foreach ($congatulations as $id => $fk) {
            $m = new DbpartnersRelations();
            $m->fk_client = $this->pk_client;
            $m->fk_congratulation = $fk;
            $m->save();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'fk_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeClient()
    {
        return $this->hasOne(DbpartnersTypeClient::className(), ['pk_type_client' => 'type_client']);
    }
    
    /*
     * найти все праздники конкретного клиента
     */
    public function getCongratulations() {
        return $this->hasMany(DbpartnersCongratulations::className(), ['pk_congratulation' => 'fk_congratulation'])
            ->viaTable('dbpartners_relations', ['fk_client' => 'pk_client']);
    }
    
    /*
     * фильтр по позздравлениям
     */
    public function getOneCongratulation() {
        return $this->hasMany(DbpartnersCongratulations::className(), ['pk_congratulation' => 'fk_congratulation'])
            ->viaTable('dbpartners_relations', ['fk_client' => 'pk_client'], function($query){//use ($type)
                if(isset($_GET['congratulation']) && (int)$_GET['congratulation'] > 0){
                    $query->andWhere(['fk_congratulation' => (int)$_GET['congratulation']]);//фильтр для связанной таблицы
                }
            });
    }
    
    /*
     * найти, используя фильтр по позздравлениям
     */
    public static function findCongratulation()
    {
        return parent::find()->joinWith('oneCongratulation');//хуй знает как параметр в функцию передать так
    }
}