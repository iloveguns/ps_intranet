<?php

namespace app\modules\intranet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\intranet\models\Projects;

class ProjectsSearch extends Projects {    
    public function rules() {
        return [
            [['pk_project', 'id_author', 'id_head', 'status'], 'integer'],
            [['title', 'target', 'content', 'update_date', 'start_date', 'end_date', 'create_date', 'direction'], 'safe'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params, $isAdmin = false) {
        $query = Projects::find();

        $pageSize = isset($_GET['per-page']) ? intval($_GET['per-page']) : Yii::$app->params['default_Pagination_PageSize'];
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['pk_project'=>SORT_DESC]],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        
        // сделано быстро, переделать
        if(!empty($this->direction)) { // найти проекты с этим направлением через таблицу связей
            $fk_projects_find = ProjectToDirection::find()->select('fk_project')->asArray()->where(['IN', 'fk_project_direction', $this->direction])->all();
            
            $fk_projects = [];
            
            foreach ($fk_projects_find as $fk_proj) {
                $fk_projects[] = $fk_proj['fk_project'];
            }
            
            $query->andWhere(['in','pk_project', $fk_projects]);
        }
        
        if(!$isAdmin){//сотруднику - только свои проекты
            $query->andWhere(['in','pk_project', \app\modules\user\models\User::getAllIdEntityUser(Projects::getName())]);
        }

        $query->andFilterWhere([
            'pk_project' => $this->pk_project,
            'id_author' => $this->id_author,
            'id_head' => $this->id_head,
            'status' => $this->status,
            'update_date' => $this->update_date,
            'create_date' => $this->create_date
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'start_date', $this->start_date])
            ->andFilterWhere(['like', 'end_date', $this->end_date])
            ->andFilterWhere(['like', 'target', $this->target])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
