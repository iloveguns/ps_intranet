<?php

namespace app\modules\intranet\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use app\modules\intranet\models\Tasks;
use app\models\UploadFiles;
use app\models\EntityReport;
use app\modules\intranet\models\Subtasks;
use app\modules\intranet\models\TaskUserDone;
use app\modules\user\models\User;
use app\models\EntityUserDone;
use yii\helpers\Html;
use app\models\UnreadNotice;
use yii\helpers\Url;
use app\nodejs\socketio\php\sendSocketIo;

class Tasks extends ActiveRecord implements \app\modules\intranet\interfaces\RelationsUnits, \app\modules\intranet\interfaces\AllInterface, \app\interfaces\ClassnameInterface
{
    const STATUS_WITHOUT = 0;//не должно быть такого статуса
    const STATUS_WORK = 1;//в работе
    const STATUS_DONE = 2;//выполнено
    const STATUS_FAIL = 3;//не выполнена
    const STATUS_REPORT = 4;//отчет написан
    const STATUS_DENIEDREPORT = 5;//отчет отменен
    const STATUS_DONEBYADMIN = 9;//принудительное завершение
    const STATUS_LATEBYTIME = 90;//просроченные
    const STATUS_DONELATEBYTIME = 91;//выполненные но просроченные
    const STATUS_ALLNOTDONE = 100;//все кроме выполненных, проваленных и завершенных админом
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date', 'update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_date'],
                ],
                'value' => function() { return \app\models\FunctionModel::getDateTimestamp();},
            ],
            [
                'class' => \app\behaviors\DeleteRelations::className(),
                'className' => $this->getName(),
                'pk' => $this->primaryKey(),
            ],
        ];
    }
    
    public static function tableName()
    {
        return 'tasks';
    }

    public function rules()
    {
        return [
            [['id_head', 'title', 'id_check'], 'required'],
            [['quest', 'id_author', 'id_head', 'status', 'project_id', 'direction', 'id_check'], 'integer'],
            [['title', 'content'], 'string'],
            [['update_date', 'start_date', 'end_date', 'create_date'], 'safe']
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'pk_task' => Yii::t('app/models', 'Pk Task'),
            'id_author' => Yii::t('app/models', 'Id Author'),
            'id_head' => Yii::t('app/models', 'Id Head'),
            'title' => Yii::t('app/models', 'Title'),
            'content' => Yii::t('app/models', 'Content'),
            'status' => Yii::t('app/models', 'Status'),
            'update_date' => Yii::t('app/models', 'Update Date'),
            'start_date' => Yii::t('app/models', 'Start Date'),
            'end_date' => Yii::t('app/models', 'End Date'),
            'create_date' => Yii::t('app/models', 'Create Date'),
            'project_id' => Yii::t('app/models', 'Project ID'),
            'quest' => Yii::t('app/models', 'Quest Task'),
            'direction' => Yii::t('app/models', 'Direction Task'),
            'id_check' => Yii::t('app/models', 'Id Check Task'),
        ];
    }
    
    public function getTitleText() {
        return $this->title;
    }
    
    /*
     * ссылка на просмотр сущности
     * Callboard::findOne(1)->htmlLink;
     * 
     * $icon - font-awesome icon
     * $mail - убрать лишнее(кол-во комментов например)
     */
    public function getHtmlLink($icon = '', $mail = false, $params = []) {
        $linktext = $icon.' '.$this->title;
        $params['title'] = strip_tags($linktext);
        $unlimited = ($this->end_date === NULL) ? true : false;//задача без конца(безвременная, на всегда)
        
        if($this->end_date < \app\models\FunctionModel::getDateTimestamp() && !$unlimited){//просроченные задачи
            $params['class'] = 'text-red';
        }
        
        if(!$mail){//добавить кол-во комментариев
            $linktext .= '<span class="label label-info pull-right">'.count($this->comments).'</span>';
        }
        
        $link = Html::a($linktext, $this->getUrl($mail), $params);
        return $link;
    }
    
    /*
     * OLD
     * html иконки для задач(ответст, автор ит д)
     * $showdate - показывать дату(в скобках)
     * шота много хмурости добавлено, переделать бы получше
     */
    public function htmlIcon($showdate = false) {
        $html = $preHtml = $afterHtml = '';
        $watcher = Yii::$app->user->identity->isWatcher($this->pk_task, $this->classname());
        $secondstToEnd = strtotime($this->end_date) - strtotime(\app\models\FunctionModel::getDateTimestamp());//время до конца в секундах
        $unlimited = ($this->end_date === NULL) ? true : false;//задача без конца(безвременная, на всегда)
        $soonEnd = $secondstToEnd > 0 && $secondstToEnd < 60*60*24*3;//скоро закончится(3 дня)
        
        if($this->isAuthor()) {
            $preHtml .= '<i class="fa fa-hand-paper-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Вы автор"></i>';
        }
        if($this->isHead()) {
            $preHtml .= '<i class="fa fa-star" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Вы ответственный"></i>';
        }
        if($watcher) {
            $preHtml .= '<i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Вы наблюдатель"></i>';
        } else {
            $preHtml .= '<i class="fa fa-male" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Вы участник"></i>';
        }
        
        if($soonEnd) {
            $preHtml .= '<i class="fa fa-exclamation-triangle text-yellow" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Скоро закончится"></i>';
        }
        
        if($soonEnd) {
            $html = $this->getHtmlLink('', false, ['class' => 'text-yellow']);
        } else if($unlimited) {
            $html = $this->getHtmlLink('', false, ['class' => 'text-green']);
        } else {
            $html = $this->getHtmlLink();//основная ссылка
        }
        
        if($secondstToEnd < 0 && $this->status != Tasks::STATUS_DONE && !$unlimited) {//просрочена
            $afterHtml .= ' <span class="label label-danger">'.Yii::t('app/models', 'Task status latebytime') . ' c ' . Yii::$app->formatter->asDatetime($this->end_date).'</span>';
        }
        
        if($showdate && !$unlimited) {
            $afterHtml .= ' ('.Yii::$app->formatter->asDate($this->start_date).' - '.Yii::$app->formatter->asDate($this->end_date).')';
        }
        
        return $preHtml . $html . $afterHtml;
    }
    
    /*
     * чистый урл
     * $mail(true) - абсолютный урл
     */
    public function getUrl($absolute = false) {
        return Url::to(['/intranet/tasks/view', 'id' => $this->pk_task], $absolute);
    }
    
    /*
     * AR count comments
     */
    public function getComments() {
        return $this->hasMany(\app\models\Comments::className(), ['item_id' => 'pk_task'])->andOnCondition(['class' => $this->getName()]);
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord){
                $this->id_author = Yii::$app->user->id;//автор
                $this->update_date = NULL;//новая задача не редактирована
            } else {
                Yii::$app->session->set('task_id_head', $this->id_head);//он всегда изменяется пишет
            }
            
            return true;
        }
        return false;
    }
    
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
        
        if(isset(Yii::$app->request->post()["selectedStructuresData"])){//не только при создании срабатывает, а и при update
            //поиск добавленных людей и отправка уведомлений
            $oldarr = \yii\helpers\ArrayHelper::toArray(json_decode(\app\modules\user\models\Units::structHiddenFieldData($this)));
            $newarr = \yii\helpers\ArrayHelper::toArray(json_decode(Yii::$app->request->post()["selectedStructuresData"]));
            $diffarray = [];
            foreach (['user'] as $check) {//['organization', 'divizion', 'department', 'user']
                if($oldarr[$check] == $newarr[$check]) continue;
                else{
                    $diffarray = array_diff(explode(',', $newarr[$check]), explode(',', $oldarr[$check]));//сначала собрать разницу, а потом уже слать. хотя можно и тут слать
                }
            }
            
            $usersToIO = [];
            foreach ($diffarray as $difuser) {//собрать нормальный массив
                array_push($usersToIO, $difuser);
            }
            if(!empty($usersToIO)){
                UnreadNotice::saveByIds($this, UnreadNotice::ADD_NEW_USERTO, $usersToIO);
            }            
            //end поиск добавленных людей и отправка уведомлений
            
            //сохранение структуры
            $selectedStructure = Yii::$app->request->post()["selectedStructuresData"];//json выбранных структур
            Structures::saveStructure($selectedStructure, $this->pk_task, $this::getName());//сохранение
        }
        
        //сохранение подзадач
        if(isset($_POST['Subtasks']['text'])) {
            //все подзадачи(для проверки)
            $idSubtasks = yii\helpers\ArrayHelper::map($this->subTasks, 'pk_subtask', 'pk_subtask');
            
            // удаление всех
            Subtasks::deleteAll(['pk_subtask' => $idSubtasks]);
            
            $toins = [];

            foreach ($_POST['Subtasks']['text'] as $pk => $subtask) {
                // точная проверка при создании
                if(Subtasks::find()->asArray()->where(['pk_subtask' => $pk])->one()) {
                    $pk = NULL;
                }
                
                if(empty($subtask)) continue;//пустая строка - нет подзадачи

                // подготовка, вставка существующих с их ид, чтобы не терять записи о выполнении
                $toins[] = [$pk, $subtask, $this->pk_task];
            }

            //массовая вставка
            Yii::$app->db->createCommand()->batchInsert(Subtasks::tableName(), ['pk_subtask', 'text', 'fk_task'], $toins)->execute();
        }
        
        if(isset($idSubtasks) && !empty($idSubtasks)) {
            EntityUserDone::deleteAll(['item_id' => $idSubtasks, 'class' => Subtasks::className()]);//записи о выполнении
        }
        
        /*обнаружить изменения*/
        $changed = [];
        
        if(isset($changedAttributes['id_head'])){
            if(Yii::$app->session->get('task_id_head') != $changedAttributes['id_head']){//изменен ответственный
                $changed['id_head'] = $changedAttributes['id_head'];//по ид сотрудника
            }
        }
        if(isset($changedAttributes['end_date'])){//измен дата окончания
            $changed['end_date'] = $changedAttributes['end_date'];
        }
        if(isset($changedAttributes['title'])){//измен заголовок
            $changed['title'] = $changedAttributes['title'];
        }
        
        //загенерить комментарий в котором указаны изменения
        if($changed){
            $text = Yii::t('app/models', 'Changed Attributes');
            $changes = [];
            foreach ($changed as $key => $change) {
                $changes[] = $this->getAttributeLabel($key);//только название того, что изменилось
            }
            $text .= implode(',', $changes);
            
            //создать коммент(отдать socket.io)
            $ch = curl_init("http://".Yii::$app->params['ipServerWebSocket'].":".Yii::$app->params['ipServerApiPort']."/api/syscomment");
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode([
                    'text' => $text,
                    'item_id' => $this->pk_task,
                    'item_class' => $this->getName()
                ])
            ));            
            curl_exec($ch);
        }
        /*конец обнаружить изменения*/
        
        /*if($insert){//новая запись            //ну выше же же сохранилось
            //записать в непрочитанные всех, кого выбрали
            //UnreadNotice::saveByIds($this, UnreadNotice::NEW_TASK);
        }*/
    }
    
    public function beforeDelete(){        
        Subtasks::deleteAll(['fk_task' => $this->pk_task]);//подзадачи
        EntityUserDone::deleteAll(['item_id' => $this->pk_task, 'class' => Subtasks::className()]);//записи о выполнении    
        
        return parent::beforeDelete();
    }
    
    /**
     * получить подзадачи
     */
    public function getSubTasks()
    {
        return $this->hasMany(Subtasks::className(), ['fk_task' => 'pk_task']);
    }
    
    /*
     * получить файлы
     */
    public function getFiles() {
        return UploadFiles::findAll(['class' => self::getName(), 'item_id' => $this->pk_task]);
    }
    
    /*
     * получить файлы
     */
    public function getReports() {
        return EntityReport::find()->where(['class' => self::getName(), 'item_id' => $this->pk_task])->orderBy('date DESC')->all();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['pk_project' => 'project_id']);
    }

    public function getIdAuthor() {
        return $this->hasOne(User::className(), ['id_user' => 'id_author']);
    }

    public function getIdHead() {
        return $this->hasOne(User::className(), ['id_user' => 'id_head']);
    }
    
    public function getIdCheck() {
        return $this->hasOne(User::className(), ['id_user' => 'id_check']);
    }
    
    /*
     * связь с организациями
     * $m = Projects::findOne(1);
     * $m->organizations[0]->divizions[0]->departments[0]->users[0]->id
     */
    public function getOrganizations()
    {
        return $this->hasMany(\app\modules\user\models\Organization::className(), ['pk_organization' => 'id_organization'])
            ->viaTable('relation_to_organization', ['item_id' => 'pk_task'], function($query){
                $query->andWhere(['class' => Tasks::getName()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * связь с сотрудниками
     * $m = Projects::findOne(1);
     * var_dump($m->users[0]->id);
     */
    public function getUsers()
    {
        return $this->hasMany(\app\modules\user\models\User::className(), ['id_user' => 'id_user'])
            ->viaTable('relation_to_user', ['item_id' => 'pk_task'], function($query){
                $query->andWhere(['class' => Tasks::getName()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * связь с отделами
     * $m = Projects::findOne(1);
     * $m->departments[0]->users[0]->id
     */
    public function getDepartments()
    {
        return $this->hasMany(\app\modules\user\models\Department::className(), ['pk_department' => 'id_department'])
            ->viaTable('relation_to_department', ['item_id' => 'pk_task'], function($query){
                $query->andWhere(['class' => Tasks::getName()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * связь с дивизионами
     * $m = Projects::findOne(1);
     * $m->divizions[0]->departments[0]->users[0]->id
     */
    public function getDivizions()
    {
        return $this->hasMany(\app\modules\user\models\Divizion::className(), ['pk_divizion' => 'id_divizion'])
            ->viaTable('relation_to_divizion', ['item_id' => 'pk_task'], function($query){
                $query->andWhere(['class' => Tasks::getName()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * статус задач
     * $id - ид задачи
     * $label - bootstrap метка статуса
     */
    public function getStatus($id = NULL, $label = NULL) { 
        $b = [self::STATUS_WORK => Yii::t('app/models', 'Work status'), self::STATUS_DONE => Yii::t('app/models', 'Done status'), self::STATUS_FAIL => Yii::t('app/models', 'Fail status'), self::STATUS_REPORT => Yii::t('app/models', 'Report status'), self::STATUS_DENIEDREPORT => Yii::t('app/models', 'Deniedreport status'), self::STATUS_DONEBYADMIN => Yii::t('app/models', 'Donebyadmin status')];
        if($id !== NULL) {
            if($label !== NULL){
                $label = '';//название метки http://getbootstrap.com/css/#buttons-options
                switch ($id){
                    case self::STATUS_WORK :
                    case self::STATUS_REPORT :
                        $label = 'info';
                        break;

                    case self::STATUS_DENIEDREPORT :
                        $label = 'warning';
                        break;
                    
                    case self::STATUS_DONE :
                        $label = 'success';
                        break;
                    
                    case self::STATUS_FAIL :
                        $label = 'danger';
                        break;
                }
                return '<span class="label label-'.$label.'">'.$b[$id].'</span>';
            }
            return $b[$id];
        }

        return $b;
    }
    
    /*
     * ссылка на себя
     */
    public function getSelfLink() {
        return \yii\helpers\Url::to(['/intranet/tasks/view', 'id' => $this->pk_task]);
    }
    
    /*
     * имеет ли сотрудник доступ к администрированию задачи
     * только автор может редактировать
     */
    public function canAdmin($id) {
        //if(Yii::$app->user->identity->isAdmin()) return true;//админ
        return $this->id_author === $id;
    }
    
    /*
     * является ли ответственным задачи
     */
    public function isHead($id = NULL) {
        if($id === NULL) $id = Yii::$app->user->id;
        return $this->id_head === $id;
    }
    
    /*
     * является ли проверяющим задачи
     */
    public function isCheck($id = NULL) {
        if($id === NULL) $id = Yii::$app->user->id;
        return $this->id_check === $id;
    }
    
    /*
     * является ли создателем задачи
     */
    public function isAuthor($id = NULL) {
        if($id === NULL) $id = Yii::$app->user->id;
        return $this->id_head === $id;
    }
    
    /*
     * направление
     */
    public function getDirections($id = NULL) {
        $b = [1 => 'направление 1'];
        if($id !== NULL) return $b[$id];
        return $b;
    }

    public function getName() {
        return 'tasks';
    }
    
    /**
     * первая обычная аналитика для задач по ответственному
     * статистика за месяц и не дает адекватных данных
     * @param {int} $year
     * @param {int} $month
     * @param {int} $id_user
     * @return array
     */
    public function ApiAnalyticsAll($year, $month, $id_user) {
        if(strlen($month) == 1) $month = '0' . $month;
        
        //создано
        $created = Tasks::find()->asArray()->select('pk_task')->where(['id_head' => $id_user])->andWhere(['like', 'create_date', $year . '-' . $month])->all();
        //завершено
        $done = Tasks::find()->asArray()->select('pk_task')->where(['id_head' => $id_user])->andWhere(['like', 'end_date', $year . '-' . $month])->all();
        //проваленные
        $failed = Tasks::find()->asArray()->select('pk_task')->where(['id_head' => $id_user, 'status' => self::STATUS_FAIL])->andWhere(['like', 'update_date', $year . '-' . $month])->all();
        //актуальные сейчас
        $work = Tasks::find()->asArray()->select('pk_task')->where(['id_head' => $id_user, 'status' => self::STATUS_WORK])->all();
        //подтвержденные отчеты по дате отчета
        $done_reports = Yii::$app->db->createCommand("SELECT DISTINCT(`pk_task`) FROM `". Tasks::tableName() ."` LEFT JOIN `". EntityReport::tableName() ."` ON `pk_task` = `item_id` WHERE `status` = ". Tasks::STATUS_WORK ." AND `class` = '". Tasks::getName() ."' AND `text_deniedreport` IS NULL AND `id_head` = " . $id_user . " AND `date` LIKE '%". $year . "-" . $month ."%'")->queryAll();
                
        return [
            'created' => self::getIdsAnalAll($created),
            'done' => self::getIdsAnalAll($done),
            'failed' => self::getIdsAnalAll($failed),
            'work' => self::getIdsAnalAll($work),
            'done_reports' => self::getIdsAnalAll($done_reports)
        ];
    }
    
    protected function getIdsAnalAll($t) {
        if($t === false) return 0;
        if(count($t) < 1) return count($t);
        
        $tmp = [];
        
        foreach ($t as $v) {
            $tmp[] = $v['pk_task'];
        }
        
        return Html::a(count($tmp), ['/intranet/tasks/all-tasks', 'ids' => implode(',', $tmp)], ['target' => '_blank']);
    }
}