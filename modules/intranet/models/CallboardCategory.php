<?php

namespace app\modules\intranet\models;

use Yii;
use app\helpers\Data;
use app\modules\intranet\models\Callboard;
use app\behaviors\CacheFlush;

class CallboardCategory extends \yii\db\ActiveRecord implements \app\interfaces\ClassnameInterface
{
    const CACHE_KEY = 'callboard_category';
    
    static $_data;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'callboard_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['position'], 'integer'],
            [['name'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_category' => Yii::t('app\modules\intranet\models', 'Pk Category'),
            'name' => Yii::t('app\modules\intranet\models', 'Name'),
            'position' => Yii::t('app\modules\intranet\models', 'Position'),
        ];
    }
    
    public function behaviors()
    {
        return [
            CacheFlush::className()
        ];
    }

    /**
     * получить объявления по категории
     */
    public function getCallboards()
    {
        return $this->hasMany(Callboard::className(), ['fk_callboard_category' => 'pk_category']);
    }
    
    /*
     * получить категории
     */
    public function getCategory($id = NULL) {
        if(!self::$_data){
            self::$_data =  Data::cache(self::CACHE_KEY, Yii::$app->params['cacheTime'], function(){
                $result = [];
                try {
                    $model = CallboardCategory::find()->asArray()->all();
                    $result = \yii\helpers\ArrayHelper::map($model, 'pk_category', 'name');
                }catch(\yii\db\Exception $e){}
                return $result;
            });
        }
        if($id !== NULL) return self::$_data[$id];
        return isset(self::$_data) ? self::$_data : null;
    }

    public function getName() {
        return 'callboard_category';
    }

}
