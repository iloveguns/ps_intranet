<?php

namespace app\modules\intranet\models;

use Yii;

/*
 * Класс для направлений проектов
 */
class ProjectDirections extends \yii\db\ActiveRecord {
    public static function tableName() {
        return 'project_directions';
    }

    public function rules() {
        return [
            [['name_direction'], 'required'],
            [['name_direction'], 'string', 'max' => 100],
        ];
    }

    public function attributeLabels() {
        return [
            'pk_direction' => Yii::t('app/models', 'Pk Project Direction'),
            'name_direction' => Yii::t('app/models', 'Name Project Direction'),
        ];
    }
    
    public function getDirections($id = NULL) {
        $c = self::find()->all();
        
        $b = [];
        
        foreach ($c as $pd) {
            $b[$pd['pk_direction']] = $pd['name_direction'];
        }
        
        if($id !== NULL) return $b[$id];
        return $b;
    }
}