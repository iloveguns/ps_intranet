<?php

namespace app\modules\intranet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\intranet\models\Bugtracker;

class BugtrackerSearch extends Bugtracker
{
    public function __construct($type = NULL) {
        parent::__construct($type);
    }
    
    public function rules()
    {
        return [
            [['pk', 'urgency', 'pk_author'], 'integer'],
            [['title', 'text', 'create_date', 'update_date', 'admin_response', 'type', 'done_date'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params, $type = NULL)
    {
        $query = Bugtracker::find();

        $pageSize = isset($_GET['per-page']) ? intval($_GET['per-page']) : Yii::$app->params['default_Pagination_PageSize'];
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['pk'=>SORT_DESC]],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        
        if($type !== NULL) {
            $query->andWhere(['type' => $type]);
        }
        
        //открытые/закрытые
        if($this->admin_response == 'yes'){
            $query->andWhere(['not', ['admin_response' => NULL]]);
        } else if($this->admin_response == 'no') {
            $query->andWhere(['admin_response' => NULL]);
        }
        
        //доступ ко всем
        if(!Yii::$app->user->identity->isAdmin()){
            $query->andFilterWhere(['pk_author' => $this->pk_author]);
            //$query->andFilterWhere(['pk_author' => Yii::$app->user->id]);
        } else {
            $query->andFilterWhere(['pk_author' => $this->pk_author]);
        }
        
        $query->andFilterWhere([
            'pk' => $this->pk,
            'urgency' => $this->urgency,            
        ]);

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'done_date', $this->done_date])
            ->andFilterWhere(['like', 'create_date', $this->create_date])
            ->andFilterWhere(['like', 'update_date', $this->update_date]);

        return $dataProvider;
    }
}