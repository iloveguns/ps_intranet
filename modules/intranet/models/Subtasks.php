<?php

namespace app\modules\intranet\models;

use app\modules\intranet\IntranetModule;

class Subtasks extends \yii\db\ActiveRecord implements \app\interfaces\ClassnameInterface {
    public static function tableName() {
        return 'subtasks';
    }

    public function rules() {
        return [
            [['fk_task'], 'required'],
            [['fk_task', 'done'], 'integer'],
            [['text'], 'string']
        ];
    }

    public function attributeLabels() {
        return [
            'pk_subtask' => IntranetModule::t('all', 'Pk Subtask'),
            'fk_task' => IntranetModule::t('all', 'Fk Task'),
            'text' => IntranetModule::t('all', 'Subtask text'),
        ];
    }

    public function getFkTask()    {
        return $this->hasOne(Tasks::className(), ['pk_task' => 'fk_task']);
    }

    public function getName() {
        return 'subtasks';
    }
}