<?php

namespace app\modules\intranet\models;

use Yii;
use app\modules\intranet\models\DbpartnersClient;

class DbpartnersTypeClient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dbpartners_type_client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_type_client'], 'required'],
            [['name_type_client'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_type_client' => Yii::t('app/models', 'Pk Type Client'),
            'name_type_client' => Yii::t('app/models', 'Type Client'),
        ];
    }
    
    public function getAll($id = NULL) {
        $all = DbpartnersTypeClient::getDb()->cache(function ($db) {
            return DbpartnersTypeClient::find()->asArray()->all();
        });
        
        $b = [];
        foreach ($all as $user) {
            $b[$user['pk_type_client']] = $user['name_type_client'];
        }
        if($id) return $b[$id];
        return $b;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDbpartnersClients()
    {
        return $this->hasMany(DbpartnersClient::className(), ['type_client' => 'pk_type_client']);
    }
}