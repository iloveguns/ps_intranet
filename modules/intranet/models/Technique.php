<?php

namespace app\modules\intranet\models;

use Yii;

class Technique extends \yii\db\ActiveRecord
{
    public $img_tecnique;
    
    public static function tableName()
    {
        return 'technique';
    }

    public function rules()
    {
        return [
            [['name_technique', 'office_num'], 'required'],
            [['office_num', 'type_technique'], 'integer'],
            [['name_technique'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_technique' => Yii::t('app/models', 'Technique_pk'),
            'name_technique' => Yii::t('app/models', 'Technique_name'),
            'img_tecnique' => Yii::t('app/models', 'Technique_img'),
            'office_num' => Yii::t('app/models', 'Technique_office_num'),
            'type_technique' => Yii::t('app/models', 'Technique_type'),
        ];
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        \app\models\UploadFiles::uploadFiles($this, 'img_tecnique', true);//загрузка файлов
    }
    
    /*
     * типы техники
     */
    public function getTypes($id = false) {
        $b = [1 => 'Принтер'];
        if($id === NULL) return 'Тип не определен';
        if($id !== false) return $b[$id];
        return $b;
    }
}