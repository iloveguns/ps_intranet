<?php

namespace app\modules\intranet\models;

use Yii;
use app\modules\user\models\User;

/*
 * Класс для расписания водителей
 */
class ScheduleDriver extends \yii\db\ActiveRecord {
    public static function tableName() {
        return 'schedule_driver';
    }

    public function rules() {
        return [
            [['id_driver', 'date_from', 'date_to', 'fk_user', 'address'], 'required'],
            [['id_driver', 'fk_user'], 'integer'],
            [['date_from', 'date_to'], 'safe'],
            [['address'], 'string'],
            [['fk_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_user' => 'id_user']],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'id_driver' => Yii::t('app/models', 'Id Driver'),
            'date_from' => Yii::t('app/models', 'Date From'),
            'date_to' => Yii::t('app/models', 'Date To'),
            'fk_user' => Yii::t('app/models', 'Fk User'),
        ];
    }

    public function getFkUser() {
        return $this->hasOne(User::className(), ['id_user' => 'fk_user']);
    }
}