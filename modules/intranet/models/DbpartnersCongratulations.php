<?php

namespace app\modules\intranet\models;
use app\modules\intranet\models\DbpartnersClient;
use app\modules\intranet\models\DbpartnersRelations;

use Yii;

class DbpartnersCongratulations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dbpartners_congratulations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_congratulation'], 'required'],
            [['name_congratulation'], 'string'],
            [['date_congratulation'], 'string', 'max' => 5],
        ];
    }
    
    public function getAll($id = NULL, $empty = false) {
        $all = DbpartnersCongratulations::getDb()->cache(function ($db) {
            return DbpartnersCongratulations::find()->asArray()->all();
        });

        $b = [];
        foreach ($all as $user) {
            $b[$user['pk_congratulation']] = $user['name_congratulation'];
        }
        if($id) return $b[$id];
        if($empty) $b = \yii\helpers\ArrayHelper::merge([0 => \Yii::t('app', 'Not selected')], $b);
        return $b;
    }
    
    public function attributeLabels()
    {
        return [
            'pk_congratulation' => Yii::t('app/models', 'Pk Congratulation'),
            'name_congratulation' => Yii::t('app/models', 'Name Congratulation'),
            'date_congratulation' => Yii::t('app/models', 'Date Congratulation'),
        ];
    }
    
    public function getDbpartnersRelations()
    {
        return $this->hasMany(DbpartnersRelations::className(), ['fk_congratulation' => 'pk_congratulation']);
    }
    
    public function getClients() {
        return $this->hasMany(DbpartnersClient::className(), ['pk_client' => 'fk_client'])
            ->viaTable('dbpartners_relations', ['fk_congratulation' => 'pk_congratulation']);
    }
}