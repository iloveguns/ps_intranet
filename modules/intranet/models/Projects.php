<?php

namespace app\modules\intranet\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use app\modules\intranet\models\Structures;
use app\models\UploadFiles;
use app\modules\intranet\models\Projects;
use yii\helpers\Html;
use yii\helpers\Url;

class Projects extends ActiveRecord implements \app\modules\intranet\interfaces\RelationsUnits, \app\modules\intranet\interfaces\AllInterface, \app\interfaces\ClassnameInterface
{
    const STATUS_WITHOUT = 0;
    const STATUS_WORK = 1;
    const STATUS_DONE = 2;
    
    public $direction;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date', 'update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_date'],
                ],
                'value' => function() { return \app\models\FunctionModel::getDateTimestamp();},
            ],
            [
                'class' => \app\behaviors\DeleteRelations::className(),
                'className' => $this->getName(),
                'pk' => $this->primaryKey(),
            ],
        ];
    }
    
    public static function tableName() {
        return 'projects';
    }

    public function rules() {
        return [
            [['id_head', 'title', 'target', 'status'], 'required'],
            [['id_author', 'id_head', 'status'], 'integer'],
            [['title', 'target', 'content'], 'string'],
            [['end_date'], 'app\components\DateIntervalValidator', 'first_date' => $this->tableName().'-start_date'],
            [['update_date', 'start_date', 'end_date', 'create_date', 'direction'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'pk_project' => Yii::t('app/models', 'Pk Project'),
            'id_author' => Yii::t('app/models', 'Id Author'),
            'id_head' => Yii::t('app/models', 'Id Head'),
            'title' => Yii::t('app/models', 'Title'),
            'target' => Yii::t('app/models', 'Target'),
            'content' => Yii::t('app/models', 'Content'),
            'status' => Yii::t('app/models', 'Status'),
            'update_date' => Yii::t('app/models', 'Update Date'),
            'start_date' => Yii::t('app/models', 'Start Date'),
            'end_date' => Yii::t('app/models', 'End Date'),
            'create_date' => Yii::t('app/models', 'Create Date'),
            'direction' => Yii::t('app/models', 'Direction'),
        ];
    }
    
    public function getTitleText() {
        return $this->title;
    }
    
    public function getHtmlLink($icon = '', $mail = false) {
        $link = Html::a($icon.' '.$this->title, $this->getUrl($mail));
        return $link;
    }
    
    public function getUrl($absolute = false) {
        return Url::to(['/intranet/projects/view', 'id' => $this->pk_project], $absolute);
    }
    /*
     * ссылка на себя
     */
    public function getSelfLink() {
        return \yii\helpers\Url::to(['/intranet/projects/view', 'id' => $this->pk_project]);
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            
            if($this->isNewRecord){
                $this->id_author = Yii::$app->user->id;//автор
            }
            return true;
        }
        return false;
    }
    
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
        
        // удалить и вставить направления проектов
        ProjectToDirection::deleteAll(['fk_project' => $this->pk_project]);

        if(!empty($this->direction)) {
            foreach ($this->direction as $direc) {
                $m = new ProjectToDirection();
                $m->fk_project = $this->pk_project;
                $m->fk_project_direction = (int)$direc;
                $m->save();
            }
        }

        // end удалить и вставить направления проектов
        
        if(isset(Yii::$app->request->post()["selectedStructuresData"])){//не только при создании срабатывает, а и при update
            //сохранение структуры
            $selectedStructure = Yii::$app->request->post()["selectedStructuresData"];//json выбранных структур
            Structures::saveStructure($selectedStructure, $this->pk_project, $this::getName());//сохранение
        }
        
        //уведомления?
    }
    
    /*
     * получить файлы проекта
     */
    public function getFiles() {
        return UploadFiles::findAll(['class' => self::getName(), 'item_id' => $this->pk_project]);
    }

    public function getIdHead() {
        return $this->hasOne(User::className(), ['id_user' => 'id_head']);
    }

    public function getIdAuthor() {
        return $this->hasOne(\app\modules\user\models\User::className(), ['id_user' => 'id_author']);
    }
    
    /**
     * привязанные задачи
     */
    public function getTasks() {
        return $this->hasMany(Tasks::className(), ['project_id' => 'pk_project']);
    }
    
    /*
     * связь с сотрудниками
     * $m = Projects::findOne(1);
     * var_dump($m->users[0]->id);
     */
    public function getUsers() {
        return $this->hasMany(\app\modules\user\models\User::className(), ['id_user' => 'id_user'])
            ->viaTable('relation_to_user', ['item_id' => 'pk_project'], function($query){
                $query->andWhere(['class' => Projects::getName()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * связь с отделами
     * $m = Projects::findOne(1);
     * $m->departments[0]->users[0]->id
     */
    public function getDepartments()
    {
        return $this->hasMany(\app\modules\user\models\Department::className(), ['pk_department' => 'id_department'])
            ->viaTable('relation_to_department', ['item_id' => 'pk_project'], function($query){
                $query->andWhere(['class' => Projects::getName()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * связь с дивизионами
     * $m = Projects::findOne(1);
     * $m->divizions[0]->departments[0]->users[0]->id
     */
    public function getDivizions()
    {
        return $this->hasMany(\app\modules\user\models\Divizion::className(), ['pk_divizion' => 'id_divizion'])
            ->viaTable('relation_to_divizion', ['item_id' => 'pk_project'], function($query){
                $query->andWhere(['class' => Projects::getName()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * связь с организациями
     * $m = Projects::findOne(1);
     * $m->organizations[0]->divizions[0]->departments[0]->users[0]->id
     */
    public function getOrganizations()
    {
        return $this->hasMany(\app\modules\user\models\Organization::className(), ['pk_organization' => 'id_organization'])
            ->viaTable('relation_to_organization', ['item_id' => 'pk_project'], function($query){
                $query->andWhere(['class' => Projects::getName()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * статус проекта
     */
    public function getStatus($id = NULL) {
        $b = [self::STATUS_WORK => Yii::t('app/models', 'Work status'), self::STATUS_DONE => Yii::t('app/models', 'Done status')];
        if($id !== NULL) return $b[$id];
        return $b;
    }
    
    public function getProjectToDirections() {
        return $this->hasMany(ProjectToDirection::className(), ['fk_project' => 'pk_project']);
    }
    
    /**
     * все направления проекта
     * @returns {String|NULL}
     */
    public function getDirections() {
        if($isptd = $this->projectToDirections) { 
            $a = [];

            foreach ($isptd as $ptd) {
                $a[] = ProjectDirections::getDirections($ptd->fk_project_direction);
            }

            return implode(', ', $a);
        }
        return NULL;
    }
    
    public function getIdDirections() {
        if($isptd = $this->projectToDirections) { 
            $a = [];

            foreach ($isptd as $ptd) {
                $a[] = $ptd->fk_project_direction;
            }

            return $a;
        }
        return NULL;
    }
    
    /*
     * имеет ли сотрудник доступ к администрированию проекта
     * только автор может редактировать
     */
    public function canAdmin($id) {
        return $this->id_author === $id;
    }

    public function getName() {
        return 'projects';
    }

}
