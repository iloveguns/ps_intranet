<?php

namespace app\modules\intranet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\intranet\models\DbpartnersClient;

/**
 * DbpartnersClientSearch represents the model behind the search form about `app\modules\intranet\models\DbpartnersClient`.
 */
class DbpartnersClientSearch extends DbpartnersClient
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pk_client', 'type_client', 'fk_user'], 'integer'],
            [['sex', 'name_client', 'appointment_client', 'birthdate_client', 'responsible'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if(isset($_GET['congratulation']) && $_GET['congratulation'] > 0){
            $query = DbpartnersClient::findCongratulation();//ломает summary на выходе(считает лишнее). выход нормальный, но числа странные
        } else {
            $query = DbpartnersClient::find();
        }

        $pageSize = isset($_GET['per-page']) ? intval($_GET['per-page']) : Yii::$app->params['default_Pagination_PageSize'];
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'sex' => $this->sex,
            'pk_client' => $this->pk_client,
            'type_client' => $this->type_client,
            'birthdate_client' => $this->birthdate_client,
            'fk_user' => $this->fk_user,
        ]);

        $query->andFilterWhere(['like', 'name_client', $this->name_client])
            ->andFilterWhere(['like', 'appointment_client', $this->appointment_client])
            ->andFilterWhere(['like', 'responsible', $this->responsible]);

        return $dataProvider;
    }
}