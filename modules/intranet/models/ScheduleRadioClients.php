<?php

namespace app\modules\intranet\models;

use Yii;

/*
 * Класс для клиентов в расписании радио
 */
class ScheduleRadioClients extends \yii\db\ActiveRecord
 {
    public static function tableName() {
        return 'schedule_radio_clients';
    }

    public function rules() {
        return [
            [['name_client'], 'required'],
            [['name_client'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'pk_client' => Yii::t('app/models', 'Pk Client'),
            'name_client' => Yii::t('app/models', 'Name Client'),
        ];
    }
}