<?php

namespace app\modules\intranet\models;

use Yii;
use app\modules\user\models\User;
use app\modules\intranet\models\ScheduleRadioClients;

/*
 * Класс для 
 */
class ScheduleRadio extends \yii\db\ActiveRecord {
    public static function tableName() {
        return 'schedule_radio';
    }

    public function rules() {
        return [
            [['date'], 'safe'],
            [['time', 'duration', 'fk_user', 'fk_schedule_radio_client'], 'required'],
            [['duration', 'fk_user', 'sort', 'fk_schedule_radio_client'], 'integer'],
            [['time'], 'string', 'max' => 5],
            [['comment'], 'string', 'max' => 255],
            [['fk_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_user' => 'id_user']],
            [['fk_schedule_radio_client'], 'exist', 'skipOnError' => true, 'targetClass' => ScheduleRadioClients::className(), 'targetAttribute' => ['fk_schedule_radio_client' => 'pk_client']],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'date' => Yii::t('app/models', 'Date'),
            'time' => Yii::t('app/models', 'Time'),
            'duration' => Yii::t('app/models', 'Хронометраж (сек)'),
            'comment' => Yii::t('app/models', 'Comment'),
            'fk_user' => Yii::t('app/models', 'Fk User'),
            'sort' => Yii::t('app/models', 'Sort'),
            'fk_schedule_radio_client' => Yii::t('app/models', 'Клиент')
        ];
    }

    public function getFkUser() {
        return $this->hasOne(User::className(), ['id_user' => 'fk_user']);
    }
    
    public function getFkClients() {
        return $this->hasOne(ScheduleRadioClients::className(), ['pk_client' => 'fk_schedule_radio_client']);
    }
}