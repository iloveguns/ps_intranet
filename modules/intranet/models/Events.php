<?php

namespace app\modules\intranet\models;

use Yii;
use app\modules\user\models\User;
use app\modules\intranet\IntranetModule;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\UnreadNotice;

class Events extends ActiveRecord implements \app\modules\intranet\interfaces\RelationsUnits, \app\modules\intranet\interfaces\AllInterface, \app\interfaces\ClassnameInterface
{
    const REPEAT_DATE = 'date';//в конкретную дату 1 раз
    const REPEAT_DAILY = 'daily';//ежедневно
    const REPEAT_WEEKLY = 'weekly';//еженедельно
    const REPEAT_MONTHLY = 'monthly';//ежемесячно
    const REPEAT_YEARLY = 'yearly';//ежегодно

    public $get_users;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date', 'update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_date'],
                ],
                'value' => function() { return \app\models\FunctionModel::getDateTimestamp();},
            ],
            [
                'class' => \app\behaviors\DeleteRelations::className(),
                'className' => $this->getName(),
                'pk' => $this->primaryKey(),
            ],
        ];
    }
    
    public static function tableName()
    {
        return 'events';
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['id_author', 'is_public'], 'integer'],
            [['title', 'text', 'json_repeat', 'json_notify'], 'string'],
            [['start_date', 'end_date', 'create_date', 'update_date', 'class', 'item_id'], 'safe'],
            [['id_author'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_author' => 'id_user']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_event' => IntranetModule::t('all', 'Pk Event'),
            'id_author' => Yii::t('app/models', 'Id Author'),
            'title' => Yii::t('app/models', 'Title'),
            'text' => Yii::t('app/models', 'Content'),
            'start_date' => Yii::t('app/models', 'Start Date'),
            'end_date' => Yii::t('app/models', 'End Date'),
            'create_date' => Yii::t('app/models', 'Create Date'),
            'update_date' => Yii::t('app/models', 'Update Date'),
            'is_public' => IntranetModule::t('all', 'Is Public Event'),
            'json_repeat' => 'Json Repeat Event',
            'json_notify' => 'Json Notify Event',
        ];
    }

    public function getIdAuthor()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_author']);
    }

    public function getDepartments() {
        return $this->hasMany(\app\modules\user\models\Department::className(), ['pk_department' => 'id_department'])
            ->viaTable('relation_to_department', ['item_id' => 'pk_event'], function($query){
                $query->andWhere(['class' => Events::getName()]);//фильтр для связанной таблицы
            });
    }

    public function getDivizions() {
        return $this->hasMany(\app\modules\user\models\Divizion::className(), ['pk_divizion' => 'id_divizion'])
            ->viaTable('relation_to_divizion', ['item_id' => 'pk_event'], function($query){
                $query->andWhere(['class' => Events::getName()]);//фильтр для связанной таблицы
            });
    }
    
    public function getTitleText() {
        return false;
    }

    public function getHtmlLink($icon = '', $mail = false) {
        $linktext = $icon.' '.$this->title;
        $params = ['title' => strip_tags($linktext)];
        
        if($this->end_date < \app\models\FunctionModel::getDateTimestamp()){//просроченные задачи
            $params['class'] = 'text-red';
        }
        
        if(!$mail){//добавить кол-во комментариев
            $linktext .= '<span class="label label-info pull-right">'.$this->countComments().'</span>';
        }
        
        $link = Html::a($linktext, $this->getUrl($mail), $params);
        return $link;
    }
    
    public function countComments() {
        $model = \app\models\Comments::find()->where(['class' => $this->getName(), 'item_id' => $this->getPrimaryKey()])->count();
        return $model;
    }
    
    /*
     * чистый урл
     * $mail(true) - абсолютный урл
     */
    public function getUrl($absolute = false) {
        return Url::to(['/intranet/events/view', 'id' => $this->pk_event], $absolute);
    }
    
    public function getOrganizations() {
        return $this->hasMany(\app\modules\user\models\Organization::className(), ['pk_organization' => 'id_organization'])
            ->viaTable('relation_to_organization', ['item_id' => 'pk_event'], function($query){
                $query->andWhere(['class' => Events::getName()]);//фильтр для связанной таблицы
            });
    }

    public function getSelfLink() {
        
    }
    
    /*
     * получить файлы
     */
    public function getFiles() {
        return \app\models\UploadFiles::findAll(['class' => self::getName(), 'item_id' => $this->pk_event]);
    }

    public function getUsers() {
        return $this->hasMany(\app\modules\user\models\User::className(), ['id_user' => 'id_user'])
            ->viaTable('relation_to_user', ['item_id' => 'pk_event'], function($query){
                $query->andWhere(['class' => Events::getName()]);//фильтр для связанной таблицы
            });
    }
    
    public function getRepeatOption($id = null) {
        $a = ['' => Yii::t('app/models', 'one event'), self::REPEAT_DATE => Yii::t('app/models', 'date event'), self::REPEAT_DAILY => Yii::t('app/models', 'daily event'), self::REPEAT_WEEKLY => Yii::t('app/models', 'weekly event')];
        //лишние убраны, self::REPEAT_MONTHLY => Yii::t('app/models', 'monthly event'), self::REPEAT_YEARLY => Yii::t('app/models', 'yearly event')
        if($id) return $a[$id];
        return $a;
    }
    
    public function getNotifyOption($id = null) {
        $a = [10 => '10 '.Yii::t('app', 'minutes'), 15 => '15 '.Yii::t('app', 'minutes'), 20 => '20 '.Yii::t('app', 'minutes'), 30 => '30 '.Yii::t('app', 'minutes'), 60 => '1 '.Yii::t('app', 'hour'),];
        
        if($id) return $a[$id];
        return $a;
    }
    
    public function getNotifyOptionTimeat($id = null) {
        $a = [10 => '10 '.Yii::t('app', 'minutes'), 15 => '15 '.Yii::t('app', 'minutes'), 20 => '20 '.Yii::t('app', 'minutes'), 30 => '30 '.Yii::t('app', 'minutes'), 60 => '1 '.Yii::t('app', 'hour'), 720 => '12 '.Yii::t('app', 'hours'), 1440 => '24 '.Yii::t('app', 'houra')];
        
        if($id) return $a[$id];
        return $a;
    }
    
    /*
     * имеет ли сотрудник доступ к администрированию задачи
     * только автор может редактировать
     */
    public function canAdmin($id) {
        return $this->id_author === $id;
    }
    
    public function beforeSave($insert) {
        /*определ повтор события*/
        $js_rep = NULL;
        $repeat = json_decode($this->json_repeat);
        
        if(empty($repeat->select)){//событие 1 раз
            
        } else if($repeat->select == 'date'){//повторить в дату
            $js_rep = ['select' => 'date', 'date' => $repeat->date];
        } else {
            $js_rep = ['select' => $repeat->select];
            if($repeat->forever){//повторять всегда
                $js_rep['forever'] = true;
            } else {
                $js_rep['count'] = $repeat->count;
            }
        }
            
        if($js_rep !== NULL) $this->json_repeat = json_encode($js_rep);
        else $this->json_repeat = $js_rep;
        /*определ повтор события*/
        
        /*опред уведомл события*/
        $notify = json_decode($this->json_notify);
        
        $this->json_notify = NULL;
                
        if($notify->notnotify){
            //ничего
        } else {
            if(isset($notify->date) && isset($notify->period)){//если есть дата
                $notify->date = substr($notify->date, 0, -2).'00';//секунды попадают иногда всякие
                $this->json_notify = json_encode(['date' => $notify->date, 'period' => $notify->period]);
            } else {//значит за какое то время
                $this->json_notify = json_encode(['notifyat' =>  \app\models\FunctionModel::dateModify($this->start_date, '-'.(int)$notify->timeat.' minutes'), 'timeat' => $notify->timeat]);
            }
        }
        /*конец опред уведомл события*/
        
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord){
                $this->id_author = Yii::$app->user->id;//автор
                $this->update_date = NULL;
            }
            return true;
        }
        return false;
    }
    
    public function afterSave($insert, $changedAttributes){        
        parent::afterSave($insert, $changedAttributes);
        
        if(Yii::$app->id != 'basic-console'){
            if(isset(Yii::$app->request->post()["selectedStructuresData"])){//не только при создании срабатывает, а и при update
                //сохранение структуры
                $selectedStructure = Yii::$app->request->post()["selectedStructuresData"];//json выбранных структур
                Structures::saveStructure($selectedStructure, $this->pk_event, $this::getName(), true);//сохранение
            } else if($this->get_users) {
                //сохранение структуры ОПЯТЬ ЖЕ
                Structures::saveStructure($this->get_users, $this->pk_event, $this::getName(), true);//сохранение
            }

            if($insert){//новая запись            
                //записать в непрочитанные всех, кого выбрали
                UnreadNotice::saveByIds($this, UnreadNotice::NEW_EVENT);
            }
        }
    }

    public function getName() {
        return 'events';
    }

}