<?php

namespace app\modules\intranet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\intranet\models\Technique;

class TechniqueSearch extends Technique
{
    public function rules()
    {
        return [
            [['pk_technique', 'office_num'], 'integer'],
            [['name_technique', 'img_tecnique'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }
    
    public function search($params)
    {
        $query = Technique::find();
        
        $pageSize = isset($_GET['per-page']) ? intval($_GET['per-page']) : Yii::$app->params['default_Pagination_PageSize'];

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['pk_technique'=>SORT_DESC]],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'pk_technique' => $this->pk_technique,
            'office_num' => $this->office_num,
        ]);

        $query->andFilterWhere(['like', 'name_technique', $this->name_technique])
            ->andFilterWhere(['like', 'img_tecnique', $this->img_tecnique]);

        return $dataProvider;
    }
}