<?php

namespace app\modules\intranet\models;

use Yii;

class DbpartnersRelations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dbpartners_relations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_client', 'fk_congratulation'], 'required'],
            [['fk_client', 'fk_congratulation'], 'integer'],
            [['fk_client'], 'exist', 'skipOnError' => true, 'targetClass' => DbpartnersClient::className(), 'targetAttribute' => ['fk_client' => 'pk_client']],
            [['fk_congratulation'], 'exist', 'skipOnError' => true, 'targetClass' => DbpartnersCongratulations::className(), 'targetAttribute' => ['fk_congratulation' => 'pk_congratulation']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_relation' => Yii::t('all', 'Pk Relation'),
            'fk_client' => Yii::t('all', 'Fk Client'),
            'fk_congratulation' => Yii::t('all', 'Fk Congratulation'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkClient()
    {
        return $this->hasOne(DbpartnersClient::className(), ['pk_client' => 'fk_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkCongratulation()
    {
        return $this->hasOne(DbpartnersCongratulations::className(), ['pk_congratulation' => 'fk_congratulation']);
    }
}