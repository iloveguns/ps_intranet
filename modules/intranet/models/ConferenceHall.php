<?php

namespace app\modules\intranet\models;

use Yii;
use app\modules\user\models\User;
use app\modules\intranet\IntranetModule;

/*
 * расписание конференц зала
 */
class ConferenceHall extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'conference_hall';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'id_user'], 'required'],
            [['title', 'text_event'], 'string'],
            [['start_date', 'end_date', 'create_date'], 'safe'],
            [['id_user'], 'integer'],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_event' => IntranetModule::t('all', 'Pk Event'),
            'title' => Yii::t('app/models', 'Title'),
            'start_date' => Yii::t('app/models', 'Start Date'),
            'end_date' => Yii::t('app/models', 'End Date'),
            'create_date' => IntranetModule::t('all', 'Create Date'),
            'text_event' => IntranetModule::t('all', 'Text Event'),
            'id_user' => IntranetModule::t('all', 'Id User'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }
}