<?php

namespace app\modules\intranet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\intranet\models\Blogs;

class BlogsSearch extends Blogs
{
    public function rules()
    {
        return [
            [['pk_blog', 'id_author', 'status_blog'], 'integer'],
            [['title', 'text', 'create_date'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params, $my = false)
    {
        $query = Blogs::find();
        if($my === false) {
            $query->where(['status_blog' => Blogs::STATUS_PUBLISHED]);
        } else {
            $this->id_author = Yii::$app->user->id;
        }
        
        $pageSize = isset($_GET['per-page']) ? intval($_GET['per-page']) : Yii::$app->params['default_Pagination_PageSize'];
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['pk_blog'=>SORT_DESC]],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'pk_blog' => $this->pk_blog,
            'id_author' => $this->id_author,
            'status_blog' => $this->status_blog,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'create_date', $this->create_date])
            ->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }
}