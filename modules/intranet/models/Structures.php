<?php

namespace app\modules\intranet\models;

use Yii;
use app\modules\intranet\IntranetModule;
use app\modules\user\models\Department;
use app\modules\user\models\Divizion;
use app\modules\user\models\Organization;

/*
 * все, что связано с сохранением информации, полученной от \app\modules\user\models\Units 
 */
class Structures extends \yii\base\Model
{
    /*
     * функция принимает json и выдает массив 
     * проверяет, принадлежат ли группы сотрудников какой-то вышестоящей структуре и если да - объединяет их в нее
     * в итоге можно сохранять по отделам и тд если все сотрудники выбраны
     * очень толстая функция, но иначе заебался думать. решил на сервере все крутить
     */
    public function prepareSaveStructuresJson($json,$onlyusers) {
        $linkUsers = [];//оставшиеся сотрудники
        $linkDepartments = [];//собирать полные отделы
        $linkDivizions = [];//собирать полные дивизионы
        $linkOrganizations = [];//собирать полные дивизионы
        
        $struct = json_decode($json, true);//в массив
        
        if($onlyusers !== true){
            //прогнать отделы //не надо отделы, только сотрудников        
            $deps = explode(',', $struct['department']);
            foreach ($deps as $dep) {            
                $usersdep = Department::getUsersByDepartment($dep);
                if(stripos($struct['user'], $usersdep) !== FALSE){//если подходит - полный отдел
                    $linkDepartments[$dep] = $dep;
                    $struct['user'] = \app\models\FunctionModel::str_replace_once($usersdep, "", $struct['user']);//убрать пройденных сотрудников из строки
                }
            }
        }
        
        //все, что осталось в user - отдельные сотрудники
        $users = explode(',', $struct['user']);
        foreach ($users as $user) {
            if(empty($user)) continue;
            $linkUsers[] = $user;
        }
        
        //прогнать дивизионы
        $divs = explode(',', $struct['divizion']);
        foreach ($divs as $div) {
            $depdiv = Divizion::getDepartmentsByDivizion($div);
            if(strripos($struct['department'], $depdiv) !== FALSE){//если подходит - полный дивизион
                $linkDivizions[$div] = $div;
                foreach (explode(',', $depdiv) as $del) {//удалить из нижней структуры все, что вошло сюда
                    unset($linkDepartments[$del]);
                }
            }
        }
        
        //организации тоже
        $orgs = explode(',', $struct['organization']);
        foreach ($orgs as $org) {
            $divorg = Organization::getDivizionsByOrganization($org);
            if(strripos($struct['divizion'], $divorg) !== FALSE){//если подходит - полный дивизион
                $linkOrganizations[$org] = $org;
                foreach (explode(',', $divorg) as $del) {//удалить из нижней структуры все, что вошло сюда
                    unset($linkDivizions[$del]);
                }
            }
        }
        
        //выводимый ключ должен быть второй частью имени таблицы
        return ['user' => $linkUsers, 'department' => $linkDepartments, 'divizion' => $linkDivizions, 'organization' => $linkOrganizations];
    }
 
    /*
     * сохранение моделей 
     * удаляет и записывает данные каждый раз
     * $arr - из метода выше
     * $id  - ид сохраняемой модели
     * $class - class модели
     */
    public function saveRelationStructure($arr, $id, $class) {
        foreach ($arr as $structName => $data) {
            if($structName == 'user'){
                $values = '(NULL, :id, :value, :class, NULL)';
            } else {
                $values = '(NULL, :id, :value, :class)';
            }
            Yii::$app->db->createCommand('DELETE FROM `relation_to_'.$structName.'` WHERE `item_id` = :id AND `class` = :class ', [':id' => $id, ':class' => $class])->execute();//удалить сначала все
            if(empty($data)) continue;//отсеить пустые
            
            foreach ($data as $value) {
                Yii::$app->db->createCommand('INSERT INTO `relation_to_'.$structName.'` VALUES '.$values, [':id' => $id, ':value' => $value, ':class' => $class])->execute();//затем снова записать
            }
        }
    }
    
    /*
     * преобразовать и сохранить голый материал
     * $json - сырые данные из виджета структур
     * $id_project - ид проекта, к которому прикреплять
     * @var $pushme {bool}      - явное указание, добавлять ли себя в случае когда себя не выбрал
     */
    public function saveStructure($json, $id_project, $name, $onlyusers = false, $pushme = true) {        
        $arrToSave = Structures::prepareSaveStructuresJson($json,$onlyusers);//подготовить
        
        //всегда добавлять себя, если не выбран изначально
        if(!in_array(Yii::$app->user->id, $arrToSave['user']) && $pushme){
            array_push($arrToSave['user'], Yii::$app->user->id);
        }
        
        Structures::saveRelationStructure($arrToSave, $id_project, $name);//сохранить
        return 1;
    }
}
