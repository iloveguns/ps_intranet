<?php

namespace app\modules\intranet\models;

use Yii;
use app\modules\user\models\User;
use app\models\UploadFiles;

class Blogs extends \yii\db\ActiveRecord implements \app\modules\intranet\interfaces\AllInterface, \app\interfaces\ClassnameInterface
{
    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;
    
    public function behaviors()
    {
        return [
            [
                'class' => \app\behaviors\DeleteRelations::className(),
                'className' => $this->getName(),
                'pk' => $this->primaryKey(),
            ],
        ];
    }
    
    public static function tableName()
    {
        return 'blogs';
    }

    public function rules()
    {
        return [
            [['title', 'text'], 'string'],
            [['text', 'status_blog'], 'required'],
            [['create_date'], 'safe'],
            [['id_author', 'status_blog'], 'integer'],
            [['id_author'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_author' => 'id_user']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_blog' => Yii::t('app/models', 'Pk Blog'),
            'title' => Yii::t('app/models', 'Title blog'),
            'text' => Yii::t('app/models', 'Text blog'),
            'create_date' => Yii::t('app/models', 'Create Date'),
            'id_author' => Yii::t('app/models', 'Id Author'),
            'status_blog' => Yii::t('app/models', 'Status Blog'),
        ];
    }
    
    /*
     * имеет ли сотрудник доступ к администрированию задачи
     * только автор может редактировать
     */
    public function canAdmin($id) {
        return $this->id_author === $id;
    }
    
    /*
     * ссылка на просмотр сущности
     * blogs::findOne(1)->htmlLink;
     */
    public function getHtmlLink($icon = '', $mail = false, $type_event = NULL) {        
        $link = \yii\helpers\Html::a($icon.' '.strip_tags($this->title), $this->getUrl($mail, $type_event),['title' => strip_tags($this->title)]);
        return $link;
    }
    
    public function getUrl($absolute = false) {
        $url = ['/intranet/blogs/view', 'id' => $this->pk_blog];
        return \yii\helpers\Url::to($url, $absolute);
    }
    
    /*
     * подсчет комментариев
     * @return int
     */
    public function countComments() {
        $model = \app\models\Comments::find()->where(['class' => $this->getName(), 'item_id' => $this->getPrimaryKey()])->count();
        return $model;
    }
    
    /*
     * получить файлы
     */
    public function getFiles() {
        return UploadFiles::findAll(['class' => self::className(), 'item_id' => $this->pk_blog]);
    }

    public function getIdAuthor()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_author']);
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord && isset(Yii::$app->user)){//для консольного приложения
                $this->id_author = Yii::$app->user->id;//автор
            }
            
            $this->text = \app\helpers\Data::strip_bad_multibyte_chars($this->text);
            
            return true;
        }
        return false;
    }
    
    public function getStatus($id = NULL) {
        //$this->status_blog = Blogs::STATUS_DRAFT;//предустановка значения по умолчанию
        $s = [Blogs::STATUS_DRAFT => Yii::t('app/models', 'draft'), Blogs::STATUS_PUBLISHED => Yii::t('app/models', 'published')];
        if($id !== NULL) return $s[$id];
        return $s;
    }

    public function getSelfLink() {
        
    }

    public function getTitleText() {
        
    }

    public function getName() {
        return 'blogs';
    }

}