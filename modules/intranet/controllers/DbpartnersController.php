<?php

namespace app\modules\intranet\controllers;

use Yii;
use app\modules\intranet\models\DbpartnersClient;
use app\modules\intranet\models\DbpartnersClientSearch;
use app\modules\intranet\models\DbpartnersCongratulations;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii2fullcalendar\models\Event;

/**
 * DbpartnersController implements the CRUD actions for DbpartnersClient model.
 */
class DbpartnersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionIndex() {
        $models = DbpartnersClient::find()->all();
        
        $events = [];
        
        foreach ($models as $model) {
            foreach ($model->congratulations as $c){
                $Event = new Event();
                $Event->id = $model->name_client;
                $Event->title = $model->name_client;
                $Event->url = \yii\helpers\Url::to(['/intranet/dbpartners/view', 'id' => $model->pk_client]);
                
                if($c->pk_congratulation == 4) {//чисто условно тут находится личный др
                    $Event->start = date('Y-m-d\TH:i:s\Z', strtotime(date('Y').'-'.substr($model->birthdate_client,5)));
                } else {
                    $Event->start = date('Y-m-d\TH:i:s\Z', strtotime(date('Y').'-'.$c->date_congratulation));
                }
                
                $events[] = $Event;
            }
        }
        
        return $this->render('index', ['events' => $events]);
    }

    /**
     * Lists all DbpartnersClient models.
     * @return mixed
     */
    public function actionClients()
    {
        $searchModel = new DbpartnersClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('clients', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DbpartnersClient model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DbpartnersClient model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DbpartnersClient();        

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['clients']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DbpartnersClient model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['clients']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DbpartnersClient model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['clients']);
    }

    /**
     * Finds the DbpartnersClient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DbpartnersClient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DbpartnersClient::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}