<?php

namespace app\modules\intranet\controllers;

use yii\web\Controller;
use Yii;
use app\models\UnreadNotice;
use app\modules\user\models\User;

class DefaultController extends Controller
{    
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionTest() {        
        return $this->render('test');
    }
    
    /*
     * поиск отовсюду
     */
    public function actionSearch($q) {
        //$m = User::
        var_dump($q);
    }
    
    /**
     * новые сообщения
     */
    public function actionDialog() {        
        return $this->render('dialog');
    }
    
    public function actionError() {
        $exception = Yii::$app->errorHandler->exception;
        $render = 'error';
        
        if ($exception !== null) {
            $statusCode = $exception->statusCode;
            $name = $exception->getName();
            $message = $exception->getMessage();
            
            switch ($statusCode){
                case 404 : 
                    Yii::warning(\yii\helpers\Url::to(), 404);//посмотреть url в котором ошибка без лишнего
                    $render = 'error404';
                    break;
            }
            
            return $this->render($render, [
                'exception' => $exception,
                'statusCode' => $statusCode,
                'name' => $name,
                'message' => $message
            ]);
        }
    }    
    //переносы
    
    
    public function actionProjectsp(){//перенос проектов
        $p = Yii::$app->db->createCommand(
            'SELECT * FROM project'
            )->queryAll();
       
        foreach ($p as $project) {
            $a = Yii::$app->db->createCommand("SELECT id_user FROM `user` WHERE `old_id` = '".$project['author_id']."';")->queryOne();
            
            $projectt = new \app\modules\intranet\models\Projects();
            $projectt->pk_project = $project["id_project"];
            $projectt->id_author = $a['id_user'];//beforeSave() убери
            $head = explode(' ', $project['head']);            
            if($head[0]){
                $a = Yii::$app->db->createCommand("SELECT id_user FROM `user` WHERE `lastname` LIKE  '%".$head[0]."%';")->queryOne();
                if($a['id_user']){
                    $projectt->id_head = $a['id_user'];
                } else {
                    $projectt->id_head = 1;
                }
            }
            else{
                $projectt->id_head = 1;
            }
            
            if($project['completed'] == 1){
                $projectt->status = 2;
            } else {
                $projectt->status = 1;
            }
            
            $projectt->title = $project['title'];
            $projectt->target = $project['target'];
            $projectt->content = $project['content'];
            $projectt->update_date = $project['update_date'];
            $projectt->start_date = $project['start_date'];
            $projectt->end_date = $project['end_date'];
            $projectt->create_date = $project['create_date'];
            //var_dump($projectt->save());
        }
    }
    
    public function actionTaskp() {//перелив задач 08,06,2016
        $p = Yii::$app->db->createCommand(
            'SELECT * FROM task LIMIT 100'
            )->queryAll();
        
        foreach ($p as $tsk) {
            $task = new \app\modules\intranet\models\Tasks();
            $task->pk_task = $tsk['id_task'];
            $a = Yii::$app->db->createCommand("SELECT id_user FROM `user` WHERE `old_id` = '".$tsk['author_id']."';")->queryOne();
            $task->id_author = $a['id_user'];
            $a = Yii::$app->db->createCommand("SELECT id_user FROM `user` WHERE `old_id` = '".$tsk['head']."';")->queryOne();
            $task->id_head = $a['id_user'];
            $task->title = $tsk['title'];
            $task->content = $tsk['content'];
            
            switch ($tsk['status']) {
                case 7:
                    $s = 3;
                    break;
                case 3:
                    $s = 4;
                    break;
                case 5:
                    $s = 2;
                    break;
                case 9:
                    $s = 1;
                    break;
                case 6:
                    $s = 5;
                    break;

                default:
                    $s = 1;
                    break;
            }
            
            $task->status = $s;
            $task->update_date = $tsk['update_date'];
            $task->start_date = $tsk['begin_date'];
            $task->end_date = $tsk['end_date'];
            $task->create_date = $tsk['create_date'];
            if($task->project_id)
            $task->project_id = $tsk['project_id'];
        }
    }
}
