<?php

namespace app\modules\intranet\controllers;

use Yii;
use app\modules\intranet\models\Projects;
use app\modules\intranet\models\ProjectsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

class ProjectsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * вывод проектов для людей
     */
    public function actionIndex()
    {
        $searchModel = new ProjectsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * вывод проектов для админа(всех)
     */
    public function actionAllProjects()
    {
        $searchModel = new ProjectsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionCreate()
    {
        $model = new Projects();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pk_project]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if(!Yii::$app->user->identity->isAdmin() && !$model->canAdmin(Yii::$app->user->id)) throw new \yii\web\ForbiddenHttpException(Yii::t ('app', 'You are not allowed to access this page'));

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pk_project]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        
        if(!Yii::$app->user->identity->isAdmin() && !$model->canAdmin(Yii::$app->user->id)) throw new \yii\web\ForbiddenHttpException(Yii::t ('app', 'You are not allowed to access this page'));
        
        $model->delete();
        
        return $this->redirect(['index']);
    }
    
    protected function findModel($id)
    {
        if (($model = Projects::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionGetbydirection() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $dir_id = $parents[0];
                
                $outs = Projects::find()->joinWith('projectToDirections')->select(['pk_project', 'title'])->asArray()->where(['fk_project_direction' => $dir_id])->limit(10)->all();
                
                foreach ($outs as $pr) {
                    $out[] = ['id' => $pr['pk_project'], 'name' => $pr['title']];
                }
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
}