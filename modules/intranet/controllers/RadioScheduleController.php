<?php

namespace app\modules\intranet\controllers;

use Yii;
use app\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\FunctionModel;
use app\modules\intranet\models\ScheduleRadio;
use app\modules\intranet\models\ScheduleRadioClients;

class RadioScheduleController extends Controller {
    const MAX_TIME = 105;//кол-во в секундах для вставки
    
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'update' => ['POST'],
                    'delete' => ['POST'],
                    'setsort' => ['POST'],
                    'report' => ['POST']
                ],
            ],
        ];
    }
    
    /**
     * формирование отчета
     */
    public function actionReport() {    
        $post = Yii::$app->request->post();
        
        $this->jsonResponse();
        
        if($pk_client = (int)$post['pk_client']) {
            $rarr = [];
            $aarr = [];
            $count = 0;
            
            $data_client = ScheduleRadioClients::find()->select(['name_client'])->asArray()->where(['pk_client' => $pk_client])->one();
            
            $models = ScheduleRadio::find()->select('date, time, duration')->asArray()
                    ->where(['fk_schedule_radio_client' => $pk_client])
                    ->andWhere(['>=', 'date', $post['start_date_report']])
                    ->andWhere(['<=', 'date', $post['end_date_report']])
                    ->orderBy('date ASC, time ASC')
                    ->all();
            
            //собрать в удобный вид по дням
            foreach ($models as $model) {
                $d = date('d.m.Y', strtotime($model['date']));
                unset($model['date']);
                
                $rarr[$d][] = $model;
            }
            
            //ворд
            $phpWord = new \PhpOffice\PhpWord\PhpWord();
            $section = $phpWord->addSection();

            $section->addTextBreak(1);
            $fancyTableStyleName = 'Fancy Table';
            $fancyTableStyle = array('borderSize' => 6, 'cellMargin' => 80, 'alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER);
            $fancyTableFirstRowStyle = array('borderBottomSize' => 18);
            $fancyTableCellStyle = array('valign' => 'top');
            $fancyTableCellBtlrStyle = array('valign' => 'center', 'textDirection' => \PhpOffice\PhpWord\Style\Cell::TEXT_DIR_BTLR);
            $fancyTableFontStyle = array('bold' => true, 'name' => 'Times New Roman', 'size' => 12);
            $datacellFontStyle = array('name' => 'Times New Roman', 'size' => 12);
            $paragraphStyle = ['alignment' => 'center'];
            $headerStyle = ['alignment' => 'right'];
            
            $section->addImage(
                \Yii::getAlias('@webroot') . '/images/radiokp.jpg',
                [
                    'width' => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(3.68),
                    'height' => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(4.44),
                    'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::RIGHT
                ]);
            
            $section->addText('656002, г. Барнаул,', $datacellFontStyle, $headerStyle);
            $section->addText('Короленко,51,', $datacellFontStyle, $headerStyle);
            $section->addText('тел. (83852) 722-090', $datacellFontStyle, $headerStyle);
            $section->addTextBreak(1);
            
            $section->addText('Эфирная справка', $fancyTableFontStyle, $paragraphStyle);
            $section->addTextBreak(1);
            
            $textrun = $section->addTextRun();
            $textrun->addText('Заказчик: ', $fancyTableFontStyle);
            $textrun->addText($data_client['name_client'], $datacellFontStyle);
            
            $textrun = $section->addTextRun();
            $textrun->addText('Канал радиовещания: ', $fancyTableFontStyle);
            $textrun->addText('«Комсомольская правда – Барнаул» 106.8 FM', $datacellFontStyle);
            
            $textrun = $section->addTextRun();
            $textrun->addText('Период: ', $fancyTableFontStyle);
            $textrun->addText(date('d.m.Y', strtotime($post['start_date_report'])) . ' - ' . date('d.m.Y', strtotime($post['end_date_report'])), $datacellFontStyle);
            $section->addTextBreak(1);

            $phpWord->addTableStyle($fancyTableStyleName, $fancyTableStyle, $fancyTableFirstRowStyle);
            $table = $section->addTable($fancyTableStyleName);
            $table->addRow(700);
            $table->addCell(2500, $fancyTableCellStyle)->addText('Дата', $fancyTableFontStyle, $paragraphStyle);
            $table->addCell(2500, $fancyTableCellStyle)->addText('Время', $fancyTableFontStyle, $paragraphStyle);
            $table->addCell(3000, $fancyTableCellStyle)->addText('Программа', $fancyTableFontStyle, $paragraphStyle);
            $table->addCell(2000, $fancyTableCellStyle)->addText('Хронометраж', $fancyTableFontStyle, $paragraphStyle);
            
            //собрать во время и длительность
            foreach ($rarr as $k => $event) {
                $times = [];
                $duration = 0;
                
                foreach ($event as $t) {
                    $times[] = $t['time'];
                    $duration = $t['duration'];
                    ++$count;
                }
                
                $ts = implode(', ', $times);
                
                $aarr[] = [
                    'date' => $k,
                    'data' => [
                        'times' => $ts,
                        'duration' => $duration // если все длительности одинаковые*/
                        ]
                    ];

                //еще ворд
                $table->addRow();
                $table->addCell(2500)->addText($k, $datacellFontStyle);
                $table->addCell(2500)->addText($ts, $datacellFontStyle);
                $table->addCell(3000)->addText('Ролики', $datacellFontStyle);
                $table->addCell(2000)->addText($duration . ' сек', $datacellFontStyle);
            }
            
            $pd = 'office/radio_reports/'. $data_client['name_client'] . '.docx';
            
            $section->addTextBreak(1);
            $textrun = $section->addTextRun();
            $textrun->addText('Итого: ', $fancyTableFontStyle);
            $textrun->addText($count . ' прокатов', $datacellFontStyle);
            $section->addText('Общая стоимость: ', $fancyTableFontStyle);
            
            $section->addTextBreak(1);
            $section->addText('Главный редактор «Комсомольской правды на Алтае»', $datacellFontStyle);
            $section->addText('Ведерникова О.С.', $datacellFontStyle);
            
            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
            $objWriter->save($pd);
            //end ворд
            
            return ['success' => true, 'events' => $aarr, 'count' => $count, 'pathdocument' => "/$pd"];
        }
    }
    
    /*
     * сортировка
     */
    public function actionSetsort() {
        $post = Yii::$app->request->post();
        
        $this->jsonResponse();
        
        $ids = explode(';', $post['positions']);
                
        foreach ($ids as $k => $id) {
            Yii::$app->db->createCommand("UPDATE `schedule_radio` SET `sort` = $k, `date` = '" . $post['date'] . "', `time` = '" . $post['time'] . "' WHERE `schedule_radio`.`id` = $id;")->execute();
        }
    }
    
    /**
     * получить инфу о записи
     */
    public function actionInfoevent(int $id) {
        $this->jsonResponse();
        
        if($model = ScheduleRadio::find()->asArray()->with('fkClients')->where(['id' => $id])->one()) {
            $events = ScheduleRadio::find()->select('id, comment, date, time, duration, fk_user')->asArray()
                    ->where(['fk_schedule_radio_client' => $model['fk_schedule_radio_client']])
                    ->andWhere(['>=', 'date', $model['date']])
                    ->orderBy('date ASC, time ASC')
                    ->all();
            
            $userids = [];
            $arr = [];
            
            //проверить, собрать
            foreach ($events as $k => $event) {
                //если в одном дне - проверять время, раннее не нужно
                if($model['date'] == $event['date'] && $model['time'] >= $event['time']) {
                    unset($events[$k]);
                } else {
                    $e = $event;
                    $e['name_client'] = $model['fkClients']['name_client'];
                    $arr[] = $event;
                    
                    $userids[$event['fk_user']] = $event['fk_user'];
                    $events[$k]['name_client'] = $model['fkClients']['name_client'];
                }
            }
            
            unset($events);
            
            //найти сотрудников
            $usersinfo = \app\modules\user\models\User::find()->select('id_user, lastname, name')->asArray()->where(['in', 'id_user', $userids])->all();
            $infousers = [];
            foreach ($usersinfo as $userinfo) {
                $id = $userinfo['id_user'];
                unset($userinfo['id_user']);
                $infousers[$id] = $userinfo;
            }
        }
        
        return ['events' => $arr, 'infousers' => $infousers, 'model' => $model];
    }
    
    public function actionDelete() {
        $post = Yii::$app->request->post();
        
        $this->jsonResponse();
        
        if(isset($post['id'])) {
            $model = ScheduleRadio::findOne((int)$post['id']);
            if($model->fk_user === Yii::$app->user->id) {
                return ['success' => (bool)$model->delete(), 'errors' => $model->getErrors()];
            } else {
                return ['success' => false, 'errors' => [0 => 'Не ваш клиент']];
            }            
        }
        
        if(isset($post['ids'])) {
            $model = ScheduleRadio::deleteAll('id IN(' . implode(',', $post['ids']) . ')');
            return ['success' => (bool)$model];
        }
        
        //подсчет для удаления
        if(isset($post['findall'])) {
            $model = ScheduleRadio::findOne((int)$post['findall']);
            $c = ScheduleRadio::find()->where(['time' => $model->time, 'fk_schedule_radio_client' => $model->fk_schedule_radio_client, 'fk_user' => Yii::$app->user->id])->count();
            return ['found' => $c];
        }
        
        //удаление всех во времени
        if(isset($post['deleteall'])) {
            $model = ScheduleRadio::findOne((int)$post['deleteall']);
            $c = ScheduleRadio::deleteAll(['time' => $model->time, 'fk_schedule_radio_client' => $model->fk_schedule_radio_client, 'fk_user' => Yii::$app->user->id]);
            return ['success' => true, 'deleted' => $c];
        }
        
        return ['success' => false, 'errors' => [0 => 'нет ид']];
    }
    
    /**
     * создание/редактирование записей
     * 
     * arrRepeatDays - 1 - понедельник
     */
    public function actionUpdate() {
        $post = Yii::$app->request->post();
        
        $this->jsonResponse();
        
        //определить создание или редактирование модели
        if((int)$post['id'] > 0) {
            $model = ScheduleRadio::findOne((int)$post['id']);
            
            $limitsec = Yii::$app->db->createCommand('SELECT SUM(`duration`) AS sum FROM `schedule_radio` WHERE `time` = "' . $post['time'] . '" AND `date` = "' . $post['date'] . '" AND `id` != ' . (int)$post['id'])->queryOne();
        } else {            
            $limitsec = Yii::$app->db->createCommand('SELECT SUM(`duration`) AS sum FROM `schedule_radio` WHERE `time` = "' . $post['time'] . '" AND `date` = "' . $post['date'] . '"')->queryOne();
                        
            $model = new ScheduleRadio();
            $model->fk_user = Yii::$app->user->id;
        }
        
        //определять кол-во секунд в день и время, не больше лимита
        if((int)$limitsec['sum'] + (int)$post['duration'] > self::MAX_TIME) {
            return ['success' => FALSE, 'errors' => [0 => 'Длительность превышена']];
        }
                
        // создать нового клиента
        if((int)$post['fk_schedule_radio_client'] === 0 || (mb_strlen((int)$post['fk_schedule_radio_client']) != mb_strlen($post['fk_schedule_radio_client']))) {
            $fkc = new ScheduleRadioClients();
            $fkc->name_client = $post['fk_schedule_radio_client'];
            
            if($fkc->save()) {
                $model->fk_schedule_radio_client = $fkc->pk_client;
            }
        } else {
            $model->fk_schedule_radio_client = (int)$post['fk_schedule_radio_client'];
        }
        
        $model->date = $post['date'];
        $model->time = $post['time'];
        $model->duration = $post['duration'];
        $model->comment = ($post['comment']) ? $post['comment'] : NULL;
                
        //добавлять после существующих, иначе первым
        if($model->isNewRecord) {
            if($modelf = ScheduleRadio::find()->asArray()->where(['date' => $post['date']])->andWhere(['time' => $post['time']])->orderBy('sort desc')->one()) {
                $model->sort = (int)$modelf['sort'] + 1;
            } else {
                $model->sort = 1;
            }
        }
                
        if($saved = $model->save()) {
            //определить повторение
            if(isset($post['is_repeat_radio']) && (int)$post['is_repeat_radio'] > 0) {
                foreach (FunctionModel::getDatesBetweenDates($post['start_date_repeat'], $post['end_date_repeat'], 'Y-m-d', true) as $datet) {
                    
                    //для избежания двойного сохранения
                    if($datet == $model->date) {
                        continue;
                    }
                    
                    //если указаны дни - проверять каждый день из периода и пропускать не подходящие
                    if(isset($post['arrRepeatDays'])) {
                        $checkdate = (int)date('w', strtotime($datet));

                        if(!in_array($checkdate, $post['arrRepeatDays'])) {
                            continue;
                        }
                    }
                    
                    $modelt = new ScheduleRadio();
                    $modelt->fk_user = Yii::$app->user->id;
                    $modelt->date = $datet;
                    $modelt->time = $post['time_repeat'];
                    $modelt->fk_schedule_radio_client = $model->fk_schedule_radio_client;
                    $modelt->duration = $post['duration'];
                    $modelt->comment = $post['comment'];
                    if($modelt->isNewRecord) { // немного нелогично
                        if($modelf = ScheduleRadio::find()->asArray()->where(['date' => $datet])->andWhere(['time' => $post['time']])->orderBy('sort desc')->one()) {
                            $modelt->sort = (int)$modelf['sort'] + 1;
                        } else {
                            $modelt->sort = 1;
                        }
                    }
                    $modelt->save();
                }
            }
        }
        
        return ['success' => (bool)$saved, 'errors' => $model->getErrors()];
    }

    public function actionIndex($date = NULL) {
        $daystart = date(Yii::$app->params['saveModelDateTime'], strtotime('Mon this week')); // начальная дата если нет иной

        if($date === NULL) {
            $date = $daystart;
        }
        $dateend = FunctionModel::dateModify($date, '+6 days');//конец недели

        $daylastweek = date(Yii::$app->params['saveModelDateTime'], strtotime('last monday', strtotime($date)));
        $daynextweek = date(Yii::$app->params['saveModelDateTime'], strtotime('next monday', strtotime($date)));
        $today = date('d.m.Y');
        
        $nmodel = [];
        $times = [];
        $userids = [];
        $models = ScheduleRadio::find()->asArray()->with('fkClients')->where(['>=', 'date', $date])->andWhere(['<=', 'date', $dateend])->orderBy('time ASC, date ASC')->all();
        
        //переварить по дням
        foreach ($models as $model) {
            $d = $model['date'];
            $t = $model['time'];
            $s = $model['sort'];
            unset($model['date']);
            unset($model['time']);
            unset($model['sort']);
            $nmodel[$d][$t][$s] = $model;
            
            //также нужно узнать все время для выстраивания сетки
            $times[$t] = $t;
            
            //ид пользователей для получения инфы
            $userids[] = $model['fk_user'];
        }
        
        $usersinfo = \app\modules\user\models\User::find()->select('id_user, lastname, name')->asArray()->where(['in', 'id_user', $userids])->all();
        $infousers = [];
        foreach ($usersinfo as $userinfo) {
            $id = $userinfo['id_user'];
            unset($userinfo['id_user']);
            $infousers[$id] = $userinfo;
        }
        
        //теперь отсортировать по сортировке
        foreach ($nmodel as $k=> $model) {
            foreach ($model as $kt => $time) {
                ksort($time);
                $nmodel[$k][$kt] = $time;
            }
        }
        
        $clients = ScheduleRadioClients::find()->asArray()->all();
        $cclients = [];
        foreach ($clients as $client) {
            $cclients[$client['pk_client']] = $client['name_client'];
        }
        
        return $this->render('index', [
            'model' => $nmodel,
            'daylastweek' => $daylastweek,
            'daynextweek' => $daynextweek,
            'today' => $today,
            'date' => $date,
            'times' => $times,
            'infousers' => $infousers,
            'clients' => $cclients,
            'maxtime' => self::MAX_TIME
        ]);
    }
}