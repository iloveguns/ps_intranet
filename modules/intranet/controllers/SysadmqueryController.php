<?php

namespace app\modules\intranet\controllers;

use Yii;
use app\modules\intranet\models\Bugtracker;
use app\modules\intranet\models\BugtrackerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class SysadmqueryController extends Controller
{
    const TYPE = 'sysadmquery';
    public $text;
    
    /*
     * условие входа по дивизиону
     */
    public function enterCondition() {
        return in_array(Yii::$app->user->identity->fkDivizion->pk_divizion, [1,2,3,6]);
    }

    public function __construct($id, $module) {
        parent::__construct($id, $module);
        $this->text = [
            'title' => Yii::t('app/models', 'System Admin Query'),
            'create' => Yii::t('app/models', 'Create Sysadmin query'),
            'update' => Yii::t('app/models', 'Update Sysadmin query'),
            'type' => self::TYPE,
            'notice' => \app\models\UnreadNotice::NEW_SYSADMINQUERY,
        ];
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new BugtrackerSearch(self::TYPE);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, self::TYPE);

        return $this->render('/bugtracker/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'text' => $this->text,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        
        return $this->render('/bugtracker/view', [
            'model' => $model,
            'text' => $this->text,
        ]);
    }

    public function actionCreate()
    {
        $model = new Bugtracker(self::TYPE);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pk]);
        } else {
            return $this->render('/bugtracker/create', [
                'model' => $model,
                'text' => $this->text
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pk]);
        } else {
            return $this->render('/bugtracker/update', [
                'model' => $model,
                'text' => $this->text
            ]);
        }
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
                
        if(!Yii::$app->user->identity->isAdmin() && !$model->pk_author == Yii::$app->user->id) throw new \yii\web\ForbiddenHttpException(Yii::t ('app', 'You are not allowed to access this page'));
        
        $model->delete();
        
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Bugtracker::find()->where(['type' => self::TYPE])->andWhere(['pk' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
