<?php

namespace app\modules\intranet\controllers;

use Yii;
use app\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\intranet\models\ScheduleDriver;
use app\models\FunctionModel;

class DriverScheduleController extends Controller {
    const dataDrivers = [
        1 => ['info' => 'Автомобиль: <u>X 146 ТK</u>, Телефон: <u>8-903-911-10-07</u>', 'fk_user' => ''],
        2 => ['info' => 'Автомобиль: <u>К 754 РК</u>, Телефон: <u>8-903-912-44-33</u>', 'fk_user' => ''],
        3 => ['info' => 'Автомобиль: <u>Е 730 УО</u>, Телефон: <u>8-913-365-28-49</u>', 'fk_user' => 537],
        4 => ['info' => 'Автомобиль: <u>Т 648 УМ</u>, Телефон: <u>8-903-949-23-27</u>', 'fk_user' => ''],
        5 => ['info' => 'Автомобиль: <u>Х 220 УР</u>, Телефон: <u>8-923-561-31-56</u>', 'fk_user' => ''],
        6 => ['info' => 'Автомобиль: <u>Х 315 УТ</u>, Телефон: <u>8-913-211-27-43</u>', 'fk_user' => 50],
        7 => ['info' => 'Автомобиль: <u>Р 810 РК</u>, Телефон: <u>8-909-507-33-48</u>', 'fk_user' => 51],
        8 => ['info' => 'Автомобиль: <u>С 715 РС</u>, Телефон: <u>8-913-085-20-94</u>', 'fk_user' => 502],
        9 => ['info' => 'Автомобиль: <u>Н 407 СМ</u>, Телефон: <u>8-983-176-28-61</u>', 'fk_user' => 503],
        10 => ['info' => 'Автомобиль: <u>C 098 TT</u>, Телефон: <u>8-929-391-17-20</u>', 'fk_user' => '']
    ];
    
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex($date = NULL, $dep = NULL) {
        $models = $arr = $drivers = $dataDrivers = [];
        
        //для телевизоров одни водители
        if($dep == 'tv') { // in_array(Yii::$app->user->identity->fk_department, [10, 12]) || 
            $drivers = [
                1 => 'Козлов Сергей Михайлович',
                2 => 'Корчагин Евгений Станиславович',
                3 => 'Печеркин Андрей Михайлович',
                4 => 'Трубников Сергей Григорьевич',
                5 => 'Фризен Алексей Валерьевич'
            ];
        } else {//для остальных - другие
            $drivers = [
                6 => 'Боровинский Игорь Робертович',
                7 => 'Андреев Дмитрий Владимирович',
                8 => 'Амеличкин Евгений Евгеньевич',
                9 => 'Шулепов Александр Викторович',
                10 => 'Капустин Андрей Александрович'
            ];
        }
        
        if($date === NULL) {
            $date = FunctionModel::getDateWParam('Y-m-d');
        }

        $models = ScheduleDriver::find()
            ->where(['like', 'date_from', $date])
            ->andWhere(['in', 'id_driver', array_keys($drivers)])
            ->orderBy('date_from ASC')
            ->all();
        
        foreach ($models as $model) {
            $day = substr($model->date_from, 0, 10);

            $arr[$model->id_driver][] = [
                'id' => $model->id,
                'date_from' => $model->date_from,
                'date_to' => $model->date_to,
                'who' => $model->fkUser->fullName,
                'who_id' => $model->fk_user,
                'address' => $model->address
            ];
        }
        
        ksort($arr);//сортировка по водителю
        
        //дополнить пустыми строками незаполненные строки
        foreach ($drivers as $c => $m) {
            if(!isset($arr[$c])) {
                $arr[$c] = [];
            }
            
            $count = count($arr[$c]);
            if($count < 8) {
                for($i=0;$i< 8 - $count;$i++) {
                    $arr[$c][] = [];
                }
            }
        }
        
        if(!$date) $date = FunctionModel::getDateWParam('Y-m-d');
        
        //заблокированные водители
        $inactiveq = \Yii::$app->db->createCommand("SELECT * FROM `schedule_driver_inactive`")->queryAll();
        $inactives = [];
        $actives = $drivers;
        
        foreach ($inactiveq as $iq) {
            $inactives[$iq['id_driver']] = $iq['id_driver'];
            
            if(array_key_exists($iq['id_driver'], $actives)) {
                unset($actives[$iq['id_driver']]);
            }
        }
        
        return $this->render('index',[
            'date' => $date,
            'models' => $arr,
            'drivers' => $drivers,
            'dataDrivers' => self::dataDrivers,
            'inactives' => $inactives,
            'actives' => $actives
        ]);
    }
    
    public function actionInactive() {
        $post = Yii::$app->request->post();
        
        $this->jsonResponse();
        
        if((int)$post['val'] === 1) {
            $q = \Yii::$app->db->createCommand("INSERT INTO `schedule_driver_inactive` VALUES (".$post['id'].")")->execute();
        } else {
            $q = \Yii::$app->db->createCommand("DELETE FROM `schedule_driver_inactive` WHERE `id_driver` = ".$post['id'])->execute();
        }
        
        return ['success' => (bool)$q];
    }
    
    public function actionUpdate() {
        $post = Yii::$app->request->post();
        
        $this->jsonResponse();
        
        $model = ScheduleDriver::findOne($post['id']);
        $oldatefrom = $model->date_from;
        $oldateto = $model->date_to;
        
        $model->date_from = substr($model->date_from, 0, 11) . $post['date_from'] . ':00';
        $model->date_to = substr($model->date_to, 0, 11) . $post['date_to'] . ':00';
        
        if(!empty(self::dataDrivers[$model->id_driver]['fk_user'])) {
            $this->sendNotice(self::dataDrivers[$model->id_driver]['fk_user'], $model->fk_user, $model->date_from, $model->date_to, $model->address, 'Расписание водителей. Поездка ' . Yii::$app->formatter->asDate($oldatefrom) . ' c ' . Yii::$app->formatter->asTime($oldatefrom) . ' до ' . Yii::$app->formatter->asTime($oldateto) . ' изменена.');
        }
        
        return ['success' => (bool)$model->update()];
    }
    
    public function actionDelete() {
        $post = Yii::$app->request->post();
        
        $this->jsonResponse();
        
        $model = ScheduleDriver::findOne($post['id']);
        
        if(isset($post['id'])) {
            $has = Yii::$app->db->createCommand('DELETE FROM `schedule_driver` WHERE `id` = '.$post['id'])->execute();
            
            if($has && !empty(self::dataDrivers[$model->id_driver]['fk_user'])) {
                $this->sendNotice(self::dataDrivers[$model->id_driver]['fk_user'], $model->fk_user, $model->date_from, $model->date_to, $model->address, 'Расписание водителей. Поездка отменена.');//$model->email
            }
            
            return ['success' => (bool)$has];
        }
    }
    
    public function actionCreate() {
        $post = Yii::$app->request->post();
        
        $save = false;
        
        $this->jsonResponse();
        
        $date_from = substr($post['date_from'], 0, -2) . '00';
        $date_fromymd = substr($post['date_from'], 0, 10);
        $date_toymd = substr($post['date_to'], 0, 10);
        $date_to = substr($post['date_to'], 0, -2) . '00';
        
        //пусть создает задним временем
        if(($date_from < FunctionModel::getDateTimestamp() || $date_to < FunctionModel::getDateTimestamp()) && Yii::$app->user->id != 75 && Yii::$app->user->id != 463) {
            return ['success' => $save, 'errors' => 'Дата не может быть прошедшей'];
        } else {
            //проверить дату/время
            
            /* 12:00 - 13:00
             * условие для отмены попадания внутрь 12:05 - 12:35
             *      12:05 > 12:00 && 12:35 <= 13:00  ||| date_from < $date_from && date_to >= $date_to
             * полностью больше 11:00 - 14:00
             *      11:00 < 12:00 && 14:00 >= 13:00  ||| date_from > $date_from && date_to <= $date_to
             * частичное совпадение 11:00 - 12:30, 12:30 - 13:30
             *      (11:00 <= 12:00 && 12:30 <= 13:00) || (12:00 >= 12:30 && 13:00 >= 13:30)
             *      (date_from >= $date_from && date_to >= $date_to) || (date_from <= $date_from && date_to <= $date_to)
             */
            
            /*$has = Yii::$app->db->createCommand('
                SELECT * FROM `schedule_driver` WHERE (`id_driver`='.$post['id'].') AND (
                ((`date_from` > "'.$date_from.'") AND (`date_from` <= "'.$date_to.'"))
                OR ((`date_to` > "'.$date_from.'") AND (`date_to` <= "'.$date_to.'"))
                OR ((`date_to` >= "'.$date_to.'") AND (`date_to` <= "'.$date_fromymd.' 23:59:59"))
                )')->queryOne();*/
            
            /*
             * кажется нормаль
             */
            $sql = '
                SELECT `date_from`, `date_to` FROM `schedule_driver` WHERE (`id_driver`='.$post['id'].') 
                AND (
                    (`date_from` < "'.$date_from.'" && `date_to` >= "'.$date_to.'") OR
                    (`date_from` > "'.$date_from.'" && `date_to` <= "'.$date_to.'") OR
                    (
                        (`date_from` > "'.$date_from.'" && `date_to` >= "'.$date_to.'" && `date_to` <= "'.$date_from.'") 
                            OR 
                        (`date_from` < "'.$date_from.'" && `date_to` <= "'.$date_to.'" && `date_to` >= "'.$date_from.'")
                    )
                )
                AND `date_from` LIKE "%'. $date_fromymd .'%"
                AND `date_to` LIKE "%'. $date_toymd .'%"
                ';
            
            $has = Yii::$app->db->createCommand($sql)->queryAll();
            
            if($has) {
                return ['success' => $save, 'errors' => 'Это время уже занято. Выберите другое'];
            } else {
                $model = new ScheduleDriver();
                $model->id_driver = $post['id'];
                $model->date_from = $date_from;
                $model->date_to = $date_to;
                $model->fk_user = Yii::$app->user->id;
                $model->address = $post['address'];
                                
                if(!empty(self::dataDrivers[(int)$post['id']]['fk_user'])) {
                    $this->sendNotice(self::dataDrivers[(int)$post['id']]['fk_user'], Yii::$app->user->id, $date_from, $date_to, $post['address'], 'Расписание водителей. На вас записались.');
                }

                $save = $model->save();
            }
        }
        
        return ['success' => $save];
    }
    
    protected function sendNotice($id_user_receiver, $id_user, $date_from, $date_to, $address, $title) {
        if(!$id_user_receiver) return false;
        
        $email = Yii::$app->db->createCommand('SELECT `email` FROM ' . \app\modules\user\models\User::tableName() . ' WHERE `id_user` = ' . $id_user_receiver)->queryOne();
        if(!isset($email['email']) || !$email['email']) return false;
        $email = $email['email'];
        
        // отправка почты водителю
        if(Yii::$app->params['sendNotify']) {
            @\app\helpers\Mail::send($email,
                $title, 
                [
                    'title' => $title,
                    'text' => \app\modules\user\models\User::getAll($id_user) . ', <strong>' . Yii::$app->formatter->asDate($date_from) . '</strong> с <strong>' . Yii::$app->formatter->asTime($date_from) . '</strong> до <strong>' . Yii::$app->formatter->asTime($date_to) . '</strong>, ' . $address
                ],
                '@app/mail/driverschedule/index');
        }
    }
}