<?php

namespace app\modules\intranet\controllers;

use yii\web\Controller;
use Yii;
use yii\filters\VerbFilter;
use app\modules\intranet\models\ConferenceHall;

class ConferenceHallController extends Controller
{    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'get-event-by-id' => ['post'],
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {   
        $events = array();
        $model = ConferenceHall::find()->all();

        foreach ($model as $value) {
            $Event = new \yii2fullcalendar\models\Event();
            $Event->id = $value->pk_event;
            $Event->url = $Event->id;//можно
            $Event->title = substr(Yii::$app->formatter->asTime($value->start_date),0,-3).' - '.substr(Yii::$app->formatter->asTime($value->end_date),0,-3).' '.$value->title;
            $Event->start = $value->start_date;
            $Event->end = $value->end_date;
            $events[] = $Event;
        }
        
        return $this->render('index', ['events' => $events]);
    }
    
    public function actionCreate() {
        $model = new ConferenceHall();
        $post = Yii::$app->request->post();
        $model->load($post);
        $model->id_user = Yii::$app->user->id;
        $model->start_date = $post["start_date"].' '.$post["start_time"];
        $model->end_date = $post["start_date"].' '.$post["end_time"];
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        if($model->save()){
            $res = [
                'success' => true,
            ];
        }
        return $res;
    }
    
    /*
     * 
     */
    public function actionUpdate($id) {
        if(!Yii::$app->user->identity->can('change_kz') && !Yii::$app->user->identity->isAdmin()) throw new \yii\web\ForbiddenHttpException(Yii::t('app', 'You are not allowed to access this page'));
        
        $model = ConferenceHall::findOne($id);
        $post = Yii::$app->request->post();
        $model->load($post);
        $model->id_user = Yii::$app->user->id;
        $model->start_date = $post["start_date"].' '.$post["start_time"];
        $model->end_date = $post["start_date"].' '.$post["end_time"];
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        if($model->save()){
            $res = [
                'success' => true,
            ];
        }
        return $res;
    }
    
    /*
     * удалить
     */
    public function actionDelete($id) {
        if(!Yii::$app->user->identity->can('change_kz') && !Yii::$app->user->identity->isAdmin()) throw new \yii\web\ForbiddenHttpException(Yii::t('app', 'You are not allowed to access this page'));
        
        $success = false;
        
        if(ConferenceHall::findOne($id)->delete()){
            $success = true;
        }
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $res = [
            'success' => $success,
        ];
        
        return $res;
    }
    
    /*
     * выдать просмотр и форму редактирования мероприятия
     */
    public function actionGetEventById(){
        $post = Yii::$app->request->post();
        $model = ConferenceHall::findOne($post['id_event']);
        
        return $this->renderAjax('update', [//Ajax
            'model' => $model,
        ]);
    }
}
