<?php

namespace app\modules\intranet\controllers;

use Yii;
use app\modules\intranet\models\Tasks;
use app\modules\intranet\models\TasksSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\intranet\models\Structures;
use app\modules\intranet\models\Subtasks;
use app\modules\user\models\Units;
use app\models\UnreadNotice;

class TasksController extends \app\components\Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'executer-complete' => ['post'],
                    'remove-user-fromtask' => ['post'],
                    'api-list-data' => ['get']
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        if(Yii::$app->request->get('mode')){//выбор между поручениями и задачами
            Yii::$app->cache->set('task_mode_search'+Yii::$app->user->id, Yii::$app->request->get('mode'));
        } else {
            if(Yii::$app->cache->get('task_mode_search'+Yii::$app->user->id) === false)
            Yii::$app->cache->set('task_mode_search'+Yii::$app->user->id, 'task');//по умолчанию задачи
        }
        
        $searchModel = new TasksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * вывод задач для админа(всех)
     */
    public function actionAllTasks()
    {
        $searchModel = new TasksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * аналитика задач
     */
    public function actionAnalytics() {
        if(Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            
            $this->jsonResponse();
            
            return Tasks::ApiAnalyticsAll($post['year'], $post['month'], $post['user']);
        } else {
            $months = ["1"=>"Январь","2"=>"Февраль","3"=>"Март","4"=>"Апрель","5"=>"Май", "6"=>"Июнь", "7"=>"Июль","8"=>"Август","9"=>"Сентябрь","10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь"];

            $fy = Tasks::find()->select('create_date')->asArray()->orderBy('create_date ASC')->one();
            $firstYear = substr($fy['create_date'], 0, 4);//начальный год
            $years = [$firstYear => $firstYear];

            //собрать года
            for($i = 1; $i <= date('Y') - $firstYear + 1; $i++) {
                $a = $firstYear+$i;
                $years[$a] = $a;
            }        

            return $this->render('analytics', [
                'months' => $months,
                'years'  => $years
            ]);
        }
    }
    
    public function actionView($id)
    {
        $model = $this->findModel($id);
        
        if(!in_array(Yii::$app->user->id, Units::onlyIdUsers($model,true, true)) && !Yii::$app->user->identity->isAdmin()) throw new \yii\web\ForbiddenHttpException(Yii::t('app', 'You are not allowed to access this page'));//лишним нельзя входить
        
        if(isset(Yii::$app->request->post()['prolongDate'])){
            $post = Yii::$app->request->post()['Tasks'];
            $model->end_date = $post['end_date'];
            $model->status = Tasks::STATUS_WORK;

            if($model->update()){
                Yii::$app->getSession()->setFlash('success', Yii::t('app/views', 'New end date set'));
                return $this->redirect(['view', 'id' => $id]);
            }
        }
        
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionCreate() {
        $model = new Tasks();
        $modelSubTask = new Subtasks();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pk_task]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelSubTask' => $modelSubTask,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelSubTask = new Subtasks();
     
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pk_task]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelSubTask' => $modelSubTask,
            ]);
        }
    }

    public function actionDelete($id)
    {   
        $model = $this->findModel($id);
        if(!Yii::$app->user->identity->isAdmin() && !$model->canAdmin(Yii::$app->user->id)) throw new \yii\web\ForbiddenHttpException(Yii::t ('app', 'You are not allowed to access this page'));
        
        $model->delete();
        
        return $this->redirect(['index']);
    }
    
    /*
     * смена статуса у задач
     * $idTask - id задачи
     * $idStatus - id статуса
     * @return arr json
     */
    public function actionChangeStatus($idTask, $idStatus) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $model = Tasks::findOne($idTask);
        $model->status = (int)$idStatus;
        
        if($model->update()){
            $res = [
                'success' => true,
            ];
            
            UnreadNotice::saveByIds($model, UnreadNotice::CHANGE_STATUS, false, true);
        }
        else{
            $res = [
                'success' => false,
                'data' => $model->getErrors(),
            ];
        }
        return $res;
    }
    
    /*
     * удаление сотрудника из задачи
     */
    public function actionRemoveUserFromtask($idTask, $iduser) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $model = $this->findModel($idTask);//а если не найдено?
        $idsTask = Units::onlyIdUsers($model,true);
        unset($idsTask[$iduser]);//убрать убираемого сотрудника
        
        if(count($idsTask) == 0){//только один сотрудник остался
            $res = [
                'success' => false,
                'data' => Yii::t('app/models', 'One More User Left Executors'),
            ];
            return $res;
        }
        
        if(!in_array(Yii::$app->user->id, $idsTask) && !Yii::$app->user->identity->isAdmin() && !$model->ishead(Yii::$app->user->id)) throw new \yii\web\ForbiddenHttpException(Yii::t('app', 'You are not allowed to access this page'));//лишним нельзя входить
        
        //создать json для имитации сохранения задачи через интерфейс
        $a['user'] = implode(',', $idsTask);//только сотрудники. можно прогнать поиск отдела по сотрудникам(мало ли)
        $a['department'] = $a['divizion'] = $a['organization'] = '';
        $selectedStructure = json_encode($a);//имитация json
        
        \app\models\UnreadNotice::deleteAll(['class' => $model->getName(), 'item_id' => $idTask, 'id_user' => $iduser]);//уведомление удалить(физически) у сотрудника
        
        \app\models\EntityUserDone::deleteAll(['class' => $model->getName(), 'item_id' => $idTask, 'id_user' => $iduser]);//метка выполнении удалить(физически) у сотрудника
                
        Structures::saveStructure($selectedStructure, $model->pk_task, $model::getName(), false, false);//пересохранение
        
        $res = [
            'success' => true,
            'data' => Yii::t('app/models', 'User Removed From Executors'),
            'id' => $iduser,
        ];
        
        return $res;
    }
    
    /*
     * сортировка задач по условию
     * выдача ид
     */
    public function actionGetSortDataTasks() {
        $this->jsonResponse();
        
        $sort = [];//параметры сортировки
        $return = [];//возвращаемое значение
        
        $mode = Yii::$app->request->post('mode');
        
        switch ($mode) {
            case 'start_date_desc' : 
                $sort = ['start_date' => SORT_DESC];
                $s = "start_date DESC";
                break;
            
            case 'start_date_asc' : 
                $sort = ['start_date' => SORT_ASC];
                $s = "start_date ASC";
                break;
            
            case 'end_date_asc' : 
                $sort = ['end_date' => SORT_ASC];
                $s = "end_date ASC";
                break;
            
            case 'end_date_desc' : 
                $sort = ['end_date' => SORT_DESC];
                $s = "end_date DESC";
                break;
        }
        
        $tasks = Yii::$app->db->createCommand(
            "SELECT `pk_task` FROM `tasks` WHERE `pk_task` IN (
                SELECT `pk_task` FROM `tasks` LEFT JOIN `relation_to_divizion` ON `relation_to_divizion`.item_id = `tasks`.pk_task WHERE `id_divizion` = ".Yii::$app->user->identity->fk_divizion." AND `class` = 'tasks'
                UNION SELECT `pk_task` FROM `tasks` LEFT JOIN `relation_to_department` ON `relation_to_department`.item_id = `tasks`.pk_task WHERE `id_department` = ".Yii::$app->user->identity->fk_department." AND `class` = 'tasks'
                UNION SELECT `pk_task` FROM `tasks` LEFT JOIN `relation_to_user` ON `relation_to_user`.item_id = `tasks`.pk_task WHERE `id_user` = ".Yii::$app->user->id." AND `class` = 'tasks'
            ) AND `status` != ".Tasks::STATUS_FAIL." AND `status` != ".Tasks::STATUS_DONEBYADMIN." AND `status` != ".Tasks::STATUS_DONE." ORDER BY ".$s
        )->queryAll();  
        
        foreach ($tasks as $task) {
            $return[] = $task['pk_task'];
        }
        $return = array_reverse($return);//потому что на выходе prepend стоит
        
        return $return;
    }
    
    /*
     * API задач. при загрузке главной делается запрос сюда. отдать список задач json, там он уже сформируется
     */
    public function actionApiListData() {
        $this->jsonResponse();
        
        $returnData = [];
        $sort = [];        
        
        $cookie = isset($_GET['sort']) ? $_GET['sort'] : NULL;
        
        switch ($cookie) {
            case 'start_date_desc' : 
                $sort = '`start_date` DESC';
                break;

            case 'start_date_asc' : 
                $sort = '`start_date` ASC';
                break;

            case 'end_date_asc' : 
                $sort = '`end_date` ASC';
                break;

            case 'end_date_desc' :
                $sort = '`end_date` DESC';
                break;                
            default:
                $sort = '`end_date` ASC';
        }
        
        //самый точный поиск по задачам сотрудника(правда без организаций)
        $models = Yii::$app->db->createCommand(
            "SELECT * FROM `tasks` WHERE `pk_task` IN (
                SELECT `pk_task` FROM `tasks` LEFT JOIN `relation_to_divizion` ON `relation_to_divizion`.item_id = `tasks`.pk_task WHERE `id_divizion` = ".Yii::$app->user->identity->fk_divizion." AND `class` = 'tasks'
                UNION SELECT `pk_task` FROM `tasks` LEFT JOIN `relation_to_department` ON `relation_to_department`.item_id = `tasks`.pk_task WHERE `id_department` = ".Yii::$app->user->identity->fk_department." AND `class` = 'tasks'
                UNION SELECT `pk_task` FROM `tasks` LEFT JOIN `relation_to_user` ON `relation_to_user`.item_id = `tasks`.pk_task WHERE `id_user` = ".Yii::$app->user->id." AND `class` = 'tasks'
            ) AND `status` != ".Tasks::STATUS_FAIL." AND `status` != ".Tasks::STATUS_DONEBYADMIN." AND `status` != ".Tasks::STATUS_DONE." ORDER BY ".$sort
        )->queryAll();                
        
        if($models) {
            foreach ($models as $model) {
                $unlimited = ($model['end_date'] === NULL) ? true : false;//задача без конца(безвременная, на всегда)
                $secondstToEnd = strtotime($model['end_date']) - strtotime(\app\models\FunctionModel::getDateTimestamp());//время до конца в секундах
                
                $cc = Yii::$app->db->createCommand("SELECT count(`id_comment`) AS C FROM `comments` WHERE `item_id` = ".$model['pk_task']." AND `class` = '".Tasks::getName()."'")->queryOne();

                //конечно, api должно возвращать норм, но пока так
                $taskInfo = [
                    'pk'            => $model['pk_task'],//int
                    'title'         => $model['title'],//string
                    'is_author'     => $model['id_head'] === Yii::$app->user->id,
                    'is_head'       => $model['id_head'] === Yii::$app->user->id,
                    'is_watcher'    => Yii::$app->user->identity->isWatcher($model['pk_task'], Tasks::getName()),
                    'is_ends_soon'  => $secondstToEnd > 0 && $secondstToEnd < 60*60*24*3,//скоро закончится(3 дня)
                    'is_late'       => $secondstToEnd < 0 && $model['status'] != Tasks::STATUS_DONE && !$unlimited,
                    'is_unlimited'  => $unlimited,
                    'date_start'    => Yii::$app->formatter->asDate($model['start_date']),
                    'date_end'      => Yii::$app->formatter->asDate($model['end_date']),
                    'url'           => \yii\helpers\Url::to(['/intranet/tasks/view', 'id' => $model['pk_task']]),
                    'count_comments'=> $cc['C']
                ];
                $returnData['tasks'][] = $taskInfo;
            }
            
            $returnData['success'] = TRUE;
        } else {
            $returnData = ['success' => false, 'errors' => ['' => 'Нет задач']];
        }
            
        return $returnData;
    }

    protected function findModel($id)
    {
        if (($model = Tasks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}