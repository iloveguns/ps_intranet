<?php

namespace app\modules\intranet\controllers;

use Yii;
use app\modules\intranet\models\Events;
use app\modules\intranet\models\EventsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\user\models\Units;

/**
 * контроллер событий
 */
class EventsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {                    
        $searchModel = new EventsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * вывод задач для админа(всех)
     */
    public function actionAllEvents()
    {
        $searchModel = new EventsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionView($id) {
        $model = $this->findModel($id);
        
        if(!in_array(Yii::$app->user->id, Units::onlyIdUsers($model,true, true)) && !Yii::$app->user->identity->can('see_all_events')) throw new \yii\web\ForbiddenHttpException(Yii::t('app', 'You are not allowed to access this page'));//лишним нельзя входить
        
        return $this->render('view', [
            'model' => $model,
        ]);
    }
    
    /*
     * напрямую сохранить событие
     */
    public function actionCreateget() {
        $model = new Events();
        
        $get = Yii::$app->request->get();
        
        //приходит извне для предзаполнения данных
        if(isset($get['users'])) {
            
            $model->class = $get['class'];
            $model->item_id = $get['item_id'];
            $model->title = $get['title'];
            //эмуляция ui
            $model->json_notify = json_encode(['notnotify' => true]);
            $model->get_users = json_encode(['organization' => '', 'department' => '', 'divizion' => '', 'user' => $get['users']]);
            
            if($model->save()) {                
                //сохранение сотрудников
                $insertArray = [];
                foreach (explode(',', $get['users']) as $user) {
                    $insertArray[] = [$model->pk_event, (int)$user, $model->getName()];
                }
                Yii::$app->db->createCommand()->batchInsert('relation_to_user', ['item_id', 'id_user', 'class'], $insertArray)->execute();
                
                //создать коммент(отдать socket.io)
                $io = new \app\nodejs\socketio\php\sendSocketIo();
                $io->send('comment', ['text' => 'Создано событие ' . \yii\helpers\Html::a('"'. $model->title .'"', ['/intranet/events/view', 'id' => $model->pk_event]), 'item_id' => $model->item_id, 'class' => $model->class, 'id_user' => NULL, 'reply_to' => 0, 'changed' => 0]);
            
                //отправить в редактирование
                $this->redirect(['update', 'id' => $model->pk_event]);
            }
        } else {
            $this->redirect('create');
        }
    }

    public function actionCreate()
    {        
        $model = new Events();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pk_event]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pk_event]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        
        if(!Yii::$app->user->identity->isAdmin() && !$model->canAdmin(Yii::$app->user->id)) throw new \yii\web\ForbiddenHttpException(Yii::t ('app', 'You are not allowed to access this page'));
        
        $model->delete();
        
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
