<?php

namespace app\modules\intranet\controllers;

use yii\web\Controller;
use Yii;
use app\models\GraphicRelate;
use yii\data\ActiveDataProvider;

class GraphrelateController extends Controller
{    
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => GraphicRelate::find()->where(['fk_user_gr' => Yii::$app->user->id])->orderBy('pk_gr DESC'),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }
    
    /*
     * сохранение в бд всего
     */
    public function actionSave() {
        $post = Yii::$app->request->post();
        
        if($post['id'] > 0){
            if(!$model = GraphicRelate::findOne($post['id'])) return ['success' => false, 'message' => 'Не найдено',];
            $model->title_gr = 'test';
            $model->html_gr = trim(preg_replace('/\s\s+/', ' ',$post['html']));
            $model->json_connect_gr = $post['json'];
            $model->fk_user_gr = Yii::$app->user->id;
        } else {
            $model = new GraphicRelate();
            $model->title_gr = 'test';
            $model->html_gr = trim(preg_replace('/\s\s+/', ' ',$post['html']));
            $model->json_connect_gr = $post['json'];
            $model->fk_user_gr = Yii::$app->user->id;
        }
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        if($model->save()){
            $res = [
                'success' => true,
                'message' => 'Сохранено',
            ];
        } else {
            $res = [
                'success' => false,
                'message' => 'Ошибка сохранения',
            ];
        }
        
        return $res;
    }
    
    public function actionView($id = null) {
        $model = false;
        
        if($id){
            $model = GraphicRelate::findOne($id);
        }
        
        return $this->render('map', ['model' => $model]);
    }
}
