<?php

namespace app\modules\intranet\controllers;

use yii\web\Controller;
use Yii;

class FileStorageController extends Controller
{    
    public function actionIndex()
    {
        return $this->render('index');
    }
}
