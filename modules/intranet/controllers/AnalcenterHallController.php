<?php

namespace app\modules\intranet\controllers;

use Yii;
use yii\filters\VerbFilter;
use app\modules\intranet\models\AnalcenterHall;

class AnalcenterHallController extends \app\components\Controller
{    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'get-event-by-id' => ['post'],
                ],
            ],
        ];
    }
    
    public function actionIndex() {
        return $this->render('index');
    }
    
    /**
     * для разгрузки, получать по частям
     */
    public function actionJsoncalendar($start=NULL, $end=NULL, $_=NULL) {
        $events = [];
        
        $allentitys = AnalcenterHall::find()
            ->where(['>=', 'start_date', $start . '00:00:00'])
            ->andWhere(['<=', 'end_date', $end . '23:59:59'])
            ->all();
        
        foreach ($allentitys as $value) {
            $Event = new \yii2fullcalendar\models\Event();
            $Event->id = $value->pk_event;
            $Event->url = $Event->id;//можно
            $Event->title = substr(Yii::$app->formatter->asTime($value->start_date),0,-3).' - '.substr(Yii::$app->formatter->asTime($value->end_date),0,-3).' '.$value->title.' ('. \app\modules\user\models\User::getAll($value->id_user, '{lastname}') .')';
            $Event->start = $value->start_date;
            $Event->end = $value->end_date;
            $events[] = $Event;
        }
        
        $this->jsonResponse();
        
        return $events;
    }
    
    public function actionCreate() {
        $model = new AnalcenterHall();
        $post = Yii::$app->request->post();
        $model->load($post);
        $model->id_user = Yii::$app->user->id;
        $model->start_date = $post["start_date"].' '.$post["start_time"];
        $model->end_date = $post["start_date"].' '.$post["end_time"];
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        if($model->save()){
            $res = [
                'success' => true,
            ];
        }
        return $res;
    }
    
    /*
     * 
     */
    public function actionUpdate($id) {
        //if(!Yii::$app->user->identity->can('anal_center') && !Yii::$app->user->identity->isAdmin()) throw new \yii\web\ForbiddenHttpException(Yii::t('app', 'You are not allowed to access this page'));
        
        $model = AnalcenterHall::findOne($id);
        $post = Yii::$app->request->post();
        $model->load($post);
        $model->id_user = Yii::$app->user->id;
        $model->start_date = $post["start_date"].' '.$post["start_time"];
        $model->end_date = $post["start_date"].' '.$post["end_time"];
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        if($model->save()){
            $res = [
                'success' => true,
            ];
        }
        return $res;
    }
    
    /*
     * удалить
     */
    public function actionDelete($id) {
        //if(!Yii::$app->user->identity->can('anal_center') && !Yii::$app->user->identity->isAdmin()) throw new \yii\web\ForbiddenHttpException(Yii::t('app', 'You are not allowed to access this page'));
        
        $success = false;
        
        if(AnalcenterHall::findOne($id)->delete()){
            $success = true;
        }
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $res = [
            'success' => $success,
        ];
        
        return $res;
    }
    
    /*
     * выдать просмотр и форму редактирования мероприятия
     */
    public function actionGetEventById(){
        $post = Yii::$app->request->post();
        $model = AnalcenterHall::findOne($post['id_event']);
        
        return $this->renderAjax('update', [//Ajax
            'model' => $model,
        ]);
    }
}
