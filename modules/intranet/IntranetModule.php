<?php

namespace app\modules\intranet;

use Yii;

class IntranetModule extends \yii\base\Module {
    public $controllerNamespace = 'app\modules\intranet\controllers';

    public function init() {
        parent::init();
        
        Yii::$app->view->registerCssFile('@web/css/build/jquery.nestable.css');
        $this->registerTranslations();
    }
    
    public function registerTranslations() {
        Yii::$app->i18n->translations['modules/intranet/*'] = [
            'class'          => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath'       => '@app/modules/intranet/messages',
            'fileMap'        => [
                'modules/intranet/all' => 'all.php',
            ],
        ];
    }
    
    public static function t($category, $message, $params = [], $language = null) {
        return Yii::t('modules/intranet/' . $category, $message, $params, $language);
    }
}
