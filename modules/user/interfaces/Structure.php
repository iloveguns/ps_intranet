<?php
namespace app\modules\user\interfaces; 

/*
 * интерфейс, чтоб соблюдать порядок
 */
interface Structure {
    /*
     * краткая информация об объекте
     */
    public function miniInfo();
    
    /*
     * получить все объекты
     */
    public function getAll($id = null);
    
    /*
     * фото в теге img
     */
    public function getImgAvatar();
}