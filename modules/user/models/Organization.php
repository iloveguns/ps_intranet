<?php

namespace app\modules\user\models;

use Yii;
use app\modules\user\UserModule;
use \yii\helpers\ArrayHelper;
/**
 * This is the model class for table "organization".
 *
 * @property integer $pk_organization
 * @property string $name
 *
 * @property User $user
 */
class Organization extends \yii\db\ActiveRecord implements \app\modules\user\interfaces\Structure
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organization';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['photo'], 'string'],
            [['name'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_organiztion' => UserModule::t('all', 'Pk Organiztion'),
            'photo' => UserModule::t('all', 'Photo'),
            'name' => UserModule::t('all', 'Name'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDivizions()
    {
        return $this->hasMany(Divizion::className(), ['fk_organization' => 'pk_organization'])->orderBy(['sort' => SORT_ASC]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['fk_organization' => 'pk_organization']);
    }
    
    /*
     * связь с проектами
     * $m = User::findOne(1);
     * var_dump($m->fkDivizion->projects[0]->pk_project);
     */
    public function getProjects()
    {
        return $this->hasMany(\app\modules\intranet\models\Projects::className(), ['pk_project' => 'item_id'])
             ->viaTable('relation_to_organization', ['id_organization' => 'pk_organization'], function($query){
                $query->andWhere(['class' => \app\modules\intranet\models\Projects::className()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * связь с задачами
     */
    public function getTasks()
    {
        return $this->hasMany(\app\modules\intranet\models\Tasks::className(), ['pk_task' => 'item_id'])
             ->viaTable('relation_to_organization', ['id_organization' => 'pk_organization'], function($query){
                $query->andWhere(['class' => \app\modules\intranet\models\Tasks::className()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * список всех организаций
     * $id - получить по ид
     */
    public function getAll($id = null) {
        $all = Organization::find()->asArray()->all();
        $b = ArrayHelper::map($all, 'pk_organization', 'name');
        if($id !== NULL) return $b[$id];
        return ArrayHelper::merge(['' => \Yii::t('app', 'Not selected')], $b);
        return $b;
    }
    
    /*
     * поиск отсортированный
     */
    public static function find()
    {
        return parent::find()->orderBy('sort ASC');
    }
    
    /*
     * список дивизионов конкретной организации
     * return array
     */
    public function getDivizionsById($id) {
        $outArr = [];
        $org = Organization::findOne($id);
        if($org) {
            $org = $org->divizions;
            $div = ArrayHelper::toArray($org,['app\modules\user\models\Organization' => ['divizions']]);
            $div = array_map(function($div) {
                return array(
                    'id' => $div['pk_divizion'],
                    'name' => $div['name']
                );
            }, $div);
            return $div;
        }
    }
    
    /*
     * получить список сотрудников по ид отдела
     */
    public function getDivizionsByOrganization($id){
        $org = Organization::findOne($id);
        $divs = '';
        if(isset($org->divizions)){
            foreach ($org->divizions as $div) {
                $divs .= $div->pk_divizion.',';
            }
        }
        return substr($divs, 0, -1);
    }
    
    /*
     * фото в теге img
     */
    public function getImgAvatar()
    {
        return \yii\helpers\Html::img($this->photo, ['width' => '100%']);
    }
    
    /*
     * краткая информация об организации
     */
    public function miniInfo($removeBtn = false){
        $info = '<div class="dd3-content">';
        if($removeBtn) $info .= Units::removeButton();
        $info .= '<div class="structure-mini-photo">'.$this->imgAvatar.'</div><span class="struct-name">'.$this->name.'</span></div>';
        return $info;
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if(!$this->photo){
                $this->photo = NULL;
            }
            return true;
        }
        return false;
    }
}
