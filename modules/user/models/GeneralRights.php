<?php
namespace app\modules\user\models;

use app\modules\user\UserModule;
use Yii;

class GeneralRights extends \yii\base\Model
{
    public //1 шаг
        $tousers,//к сотрудникам доступ
        $change_kz,//конференц-зал
        $dbpartners,//база партнеров
        $crud_holidays,//редактирование/создание праздников
        $anal_center,//аналитический центр(переговорка)
        $see_all_events//просмотр всех событий
        ;
    
    public function rules() {
        return [
            [['tousers', 'change_kz', 'dbpartners', 'crud_holidays', 'anal_center', 'see_all_events'], 'integer'],//2шаг 
        ];
    }

    public function attributeLabels() {
        return [//3 шаг
            'tousers' => UserModule::t('rights', 'tousers'),
            'change_kz' => UserModule::t('rights', 'change confzal'),
            'dbpartners' => UserModule::t('rights', 'dbpartners'),
            'crud_holidays' => UserModule::t('rights', 'crud_holidays'),
            'anal_center' => UserModule::t('rights', 'anal_center'),
            'see_all_events' => UserModule::t('rights', 'see_all_events'),
        ];
    }
    
    /*
     * убрать пустые значения перед сохранением в json ролей
     */
    public function deleteEmptysToJson() {
        $a = [];
        foreach ($this->attributes as $name => $val) {
            if($val){//если есть значение
                $a[$name] = $val;
            }
        }
        return json_encode($a);
    }
}