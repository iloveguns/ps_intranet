<?php
namespace app\modules\user\models;

/*
 * системный сотрудник(интранет) и его параметры как у обычного сотрудника
 */
class NullUser
{
    public $idUser = '';
    public $name = 'Великий';
    public $secondname = '';

    public function __construct() {
        $this->secondname = \Yii::t('app/models', 'Null User');
    }
    
    //аватар
    public function getLinkAvatar() {
        return \yii\helpers\Html::img('/images/nulluser.png', ['width' => '100%']);
    }
    
    //строка профиля
    public function getLinkToProfile() {
        return $this->name.' '.$this->secondname;
    }
}
