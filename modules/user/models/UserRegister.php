<?php
namespace app\modules\user\models;

use Yii;
use app\modules\user\models\User;
use yii\helpers\Html;

/*
 * регистрация пользователей
 */
class UserRegister extends User implements \app\interfaces\ClassnameInterface
{
    public function getHtmlLink($icon) {
        $link = Html::a($icon.' '.$this->fullName, ['/user/user/registers-workers']);
        return $link;
    }

    public function getSelfLink() {
        
    }
    
    public function getUrl($absolute = false) {
        return \yii\helpers\Url::to(['/user/user/registers-workers'], $absolute);
    }

    public function getName() {
        return 'user_register';
    }

}
