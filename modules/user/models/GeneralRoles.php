<?php

namespace app\modules\user\models;

use Yii;
use \yii\helpers\ArrayHelper;
use app\modules\user\UserModule;

class GeneralRoles extends \yii\db\ActiveRecord
{
    const ROLE_ADMIN = 1;//админ
    const ROLE_AFTER_APPROVE_REG = 2;//после регистрации роль
    
    public static function tableName()
    {
        return 'general_roles';
    }

    public function rules()
    {
        return [
            [['fk_organization'], 'integer'],
            [['name'], 'required'],
            [['json_data'], 'string'],
            [['name'], 'string', 'max' => 128]
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_role' => UserModule::t('all', 'Pk Role'),
            'fk_organization' => UserModule::t('all', 'fk_organization'),
            'name' => UserModule::t('all', 'Name Role'),
            'json_data' => UserModule::t('all', 'Json Data Role'),
        ];
    }

    public function getFkOrganization()
    {
        return $this->hasOne(Organization::className(), ['pk_organization' => 'fk_organization']);
    }

    public function getUsers()
    {
        return $this->hasMany(User::className(), ['fk_role' => 'pk_role']);
    }
    
    /*
     * список всех ролей
     * $id - получить по ид
     */
    public function getAll($id = null) {
        $all = GeneralRoles::find()->asArray()->all();
        $b = ArrayHelper::map($all, 'pk_role', 'name');
        if($id) return $b[$id];
        return $b;
    }
}