<?php

namespace app\modules\user\models;

use Yii;
use app\modules\user\UserModule;
use \yii\helpers\ArrayHelper;

class Department extends \yii\db\ActiveRecord implements \app\modules\user\interfaces\Structure
{
    public static function tableName()
    {
        return 'department';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['photo'], 'string'],
            [['fk_divizion'], 'integer'],
            [['name'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_department' => UserModule::t('all', 'Pk Department'),
            'name' => UserModule::t('all', 'Name'),
            'photo' => UserModule::t('all', 'Photo'),
            'fk_divizion' => UserModule::t('all', 'Fk Divizion'),
        ];
    }

    public function getFkDivizion()
    {
        return $this->hasOne(Divizion::className(), ['pk_divizion' => 'fk_divizion']);
    }

    public function getUsers() {
        return $this->hasMany(User::className(), ['fk_department' => 'pk_department'])->with('fkStatus', 'fkAppointment');//->andOnCondition(['user_status' => 9])
    }
    
    /**
     * кол-во активных сотрудников в отделе
     */
    public function getCountWorkusers() {
        $models = Yii::$app->db->createCommand('SELECT COUNT(`id_user`) AS count FROM `user` LEFT JOIN `user_status` ON `pk_status` = `fk_status` WHERE `fk_department` = ' . $this->pk_department . ' AND `status` != ' . UserStatus::FIRED)->queryOne();
        return $models['count'];
    }
    
    /*
     * связь с проектами
     * $m = User::findOne(1);
     * var_dump($m->fkDepartment->projects[0]->pk_project);
     */
    public function getProjects()
    {
        return $this->hasMany(\app\modules\intranet\models\Projects::className(), ['pk_project' => 'item_id'])
             ->viaTable('relation_to_department', ['id_department' => 'pk_department'], function($query){
                $query->andWhere(['class' => \app\modules\intranet\models\Projects::className()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * связь с проектами
     */
    public function getTasks()
    {
        return $this->hasMany(\app\modules\intranet\models\Tasks::className(), ['pk_task' => 'item_id'])
             ->viaTable('relation_to_department', ['id_department' => 'pk_department'], function($query){
                $query->andWhere(['class' => \app\modules\intranet\models\Tasks::className()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * список всех отделов
     * $id - получить по ид
     */
    public function getAll($id = null) {
        $all = Department::find()->asArray()->all();
        $b = ArrayHelper::map($all, 'pk_department', 'name');
        //return ArrayHelper::merge(['' => \Yii::t('app', 'Not selected')], $b);
        return $b;
    }
    
    /*
     * получить список сотрудников по ид отдела
     */
    public function getUsersByDepartment($id){
        $dep = Department::findOne($id);
        $users = [];
        if(isset($dep->users)){
            foreach ($dep->users as $user) {
                if(isset($user->fkStatus)) {//может и не быть
                    if($user->fkStatus->status == \app\modules\user\models\UserStatus::FIRED) continue;//отсеить уволенных
                    array_push($users, $user->id_user);
                }
            }
        }
        return implode(',', $users);
    }
    
    /*
     * фото в теге img
     */
    public function getImgAvatar()
    {
        return \yii\helpers\Html::img($this->photo, ['width' => '100%']);
    }
    
    /*
     * краткая информация об отделе
     */
    public function miniInfo($removeBtn = false){
        $info = '<div class="dd3-content">';
        if($removeBtn) $info .= Units::removeButton();
        $info .= '<div class="structure-mini-photo">'.$this->imgAvatar.'</div><span class="struct-name">'.$this->name.'</span></div>';
        return $info;
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if(!$this->photo){
                $this->photo = NULL;
            }
            return true;
        }
        return false;
    }
}
