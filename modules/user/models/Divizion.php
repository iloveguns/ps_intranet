<?php

namespace app\modules\user\models;

use Yii;
use app\modules\user\UserModule;
use \yii\helpers\ArrayHelper;

class Divizion extends \yii\db\ActiveRecord implements \app\modules\user\interfaces\Structure {
    public static function tableName()
    {
        return 'divizion';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['photo'], 'string'],
            [['name'], 'string', 'max' => 128],
            [['fk_organization'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_divizion' => UserModule::t('all', 'Pk Divizion'),
            'name' => UserModule::t('all', 'Name'),
            'photo' => UserModule::t('all', 'Photo'),
            'fk_organization' => UserModule::t('all', 'Fk Organization'),
        ];
    }
    
    public function getDepartments()
    {
        return $this->hasMany(Department::className(), ['fk_divizion' => 'pk_divizion'])->orderBy(['sort' => SORT_ASC]);
    }

    public function getFkOrganization()
    {
        return $this->hasOne(Organization::className(), ['pk_organization' => 'fk_organization']);
    }

    public function getUsers() {
        return $this->hasMany(User::className(), ['fk_divizion' => 'pk_divizion']);
    }
    
    /**
     * кол-во активных сотрудников в дивизионе
     */
    public function getCountWorkusers() {
        $models = Yii::$app->db->createCommand('SELECT COUNT(`id_user`) AS count FROM `user` LEFT JOIN `user_status` ON `pk_status` = `fk_status` WHERE `fk_divizion` = ' . $this->pk_divizion . ' AND `status` != ' . UserStatus::FIRED)->queryOne();
        return $models['count'];
    }
    
    /*
     * связь с проектами
     * $m = User::findOne(1);
     * var_dump($m->fkDivizion->projects[0]->pk_project);
     */
    public function getProjects()
    {
        return $this->hasMany(\app\modules\intranet\models\Projects::className(), ['pk_project' => 'item_id'])
             ->viaTable('relation_to_divizion', ['id_divizion' => 'pk_divizion'], function($query){
                $query->andWhere(['class' => \app\modules\intranet\models\Projects::className()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * связь с задачами
     */
    public function getTasks()
    {
        return $this->hasMany(\app\modules\intranet\models\Tasks::className(), ['pk_task' => 'item_id'])
             ->viaTable('relation_to_divizion', ['id_divizion' => 'pk_divizion'], function($query){
                $query->andWhere(['class' => \app\modules\intranet\models\Tasks::className()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * список всех дивизионов
     * $id - получить по ид
     */
    public function getAll($id = null) {
        $all = Divizion::find()->asArray()->all();
        $b = ArrayHelper::map($all, 'pk_divizion', 'name');
        if($id !== NULL) return $b[$id];
        //return ArrayHelper::merge(['' => \Yii::t('app', 'Not selected')], $b);
        return $b;
    }
    
    /*
     * список дивизионов конкретной организации
     * return array
     */
    public function getDepartmentsById($id) {
        $org = Divizion::findOne($id);
        if($org){
            $org = $org->departments;
            $div = ArrayHelper::toArray($org,['app\modules\user\models\Divizion' => ['departments']]);
            $div = array_map(function($div) {
                return array(
                    'id' => $div['pk_department'],
                    'name' => $div['name']
                );
            }, $div);
            return $div;
        }
    }
    
    /*
     * получить список отделов по ид дивизиона
     */
    public function getDepartmentsByDivizion($id){
        $div = Divizion::findOne($id);
        $deps = '';
        if(isset($div->departments)){
            foreach ($div->departments as $dep) {
                $deps .= $dep->pk_department.',';
            }
        }
        return substr($deps, 0, -1);
    }
    
    /*
     * фото в теге img
     */
    public function getImgAvatar()
    {
        return \yii\helpers\Html::img($this->photo, ['width' => '100%']);
    }
    
    /*
     * краткая информация о дивизионе
     */
    public function miniInfo($removeBtn = false){
        $info = '<div class="dd3-content">';
        if($removeBtn) $info .= Units::removeButton();
        $info .= '<div class="structure-mini-photo">'.$this->imgAvatar.'</div><span class="struct-name">'.$this->name.'</span></div>';
        return $info;
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if(!$this->photo){
                $this->photo = NULL;
            }
            return true;
        }
        return false;
    }
}
