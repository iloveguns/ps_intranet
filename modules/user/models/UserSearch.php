<?php

namespace app\modules\user\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\user\models\User;
use app\modules\user\models\UserStatus;

class UserSearch extends User {
    public function rules() {
        return [
            [['id_user', 'fk_organization', 'fk_divizion', 'fk_department', 'fk_status', 'fk_appointment', 'office_num', 'where_from'], 'integer'],
            [['lastname', 'name', 'secondname', 'birthday', 'sex', 'marital', 'photo', 'phone', 'email', 'password', 'icq', 'skype', 'registration_date', 'update_date', 'fk_role'], 'safe'],
        ];
    }

    public function search($params) {
        $query = User::findActive();//без сортировки по умолчанию

        $pageSize = isset($_GET['per-page']) ? intval($_GET['per-page']) : Yii::$app->params['default_Pagination_PageSize'];
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort'=> ['defaultOrder' => ['id_user'=>SORT_DESC],
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);
        
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_user' => $this->id_user,
            'birthday' => $this->birthday,
            'registration_date' => $this->registration_date,
            'update_date' => $this->update_date,
            'fk_organization' => $this->fk_organization,
            'fk_divizion' => $this->fk_divizion,
            'fk_department' => $this->fk_department,
            'fk_appointment' => $this->fk_appointment,
            'fk_role' => $this->fk_role,
        ])
        ->andWhere(['not' , ['fk_status' => null]]);//отсеять не утвержденных зарегавшихся

        $query->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'secondname', $this->secondname])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'marital', $this->marital])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'icq', $this->icq])
            ->andFilterWhere(['like', 'skype', $this->skype]);
        
        if($this->fk_status){//поиск статуса в связанной таблице
            $statuses = UserStatus::findByCondition(['status' => $this->fk_status])->asArray()->all();
            if($statuses){//если найдено что-нибудь
                $searchStatusArr = [];
                foreach ($statuses as $status) {
                    $searchStatusArr[] = $status['pk_status'];
                }
                $query->andFilterWhere(['in','fk_status',$searchStatusArr]);
            }
            else{//иначе ничего не выдавать
                $query->where('0=1');
            }
        }
        
        return $dataProvider;
    }
}
