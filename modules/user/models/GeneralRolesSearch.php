<?php

namespace app\modules\user\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\user\models\GeneralRoles;

/**
 * GeneralRolesSearch represents the model behind the search form about `app\modules\user\models\GeneralRoles`.
 */
class GeneralRolesSearch extends GeneralRoles
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pk_role', 'fk_organization'], 'integer'],
            [['name', 'json_data'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GeneralRoles::find()->where('pk_role != '.GeneralRoles::ROLE_ADMIN.' AND pk_role != '.GeneralRoles::ROLE_AFTER_APPROVE_REG);//админа и новоприбывшего не надо редактировать

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'pk_role' => $this->pk_role,
            'fk_organization' => $this->fk_organization,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'json_data', $this->json_data]);

        return $dataProvider;
    }
}
