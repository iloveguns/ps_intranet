<?php

namespace app\modules\user\models;

use Yii;

class PersonalNotes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'personal_notes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_pnote', 'text_pnote'], 'required'],
            [['text_pnote'], 'string'],
            [['fk_user'], 'integer'],
            [['title_pnote'], 'string', 'max' => 128],
            [['fk_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_user' => 'id_user']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_pnote' => Yii::t('app/models', 'Pk Pnote'),
            'title_pnote' => Yii::t('app/models', 'Title Pnote'),
            'text_pnote' => Yii::t('app/models', 'Text Pnote'),
            'fk_user' => Yii::t('app/models', 'Fk User'),
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->fk_user = Yii::$app->user->id;
            return true;
        }
        return false;
    }
    
    /*
     * вывод списка заметок
     */
    public function listHtml() {
        return \Yii::$app->view->render('//personal/list-note', ['model' => $this]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'fk_user']);
    }
}