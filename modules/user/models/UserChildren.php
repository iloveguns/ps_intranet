<?php

namespace app\modules\user\models;

use Yii;
use app\modules\user\models\User;

/*
 * Класс для детей сотрудников
 */
class UserChildren extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'user_children';
    }

    public function rules()
    {
        return [
            [['fk_user', 'name_child', 'age_child', 'sex_child'], 'required'],
            [['fk_user', 'age_child'], 'integer'],
            [['name_child', 'sex_child'], 'string'],
            [['fk_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_user' => 'id_user']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_child' => 'Pk Child',
            'fk_user' => 'Fk User',
            'name_child' => Yii::t('app/models', 'Name Child'),
            'age_child' => Yii::t('app/models', 'Age Child'),
            'sex_child' => Yii::t('app/models', 'Sex Child'),
        ];
    }
    
    public function getFkUser() {
        return $this->hasOne(User::className(), ['id_user' => 'fk_user']);
    }
    
    /*
     * сохранение связи детей с user
     * $fk_user (int) - ид сотрудника
     */
    public function saveRelationUser($fk_user) {
        UserChildren::deleteAll(['fk_user' => $fk_user]);
        
        for($i = 1; $i <= count($this->name_child); $i++){
            $savemodel = new UserChildren();
            $savemodel->fk_user = $fk_user;
            $savemodel->name_child = $this->name_child[$i];
            $savemodel->age_child = $this->age_child[$i];
            $savemodel->sex_child = $this->sex_child[$i];
            
            if($savemodel->validate() && !$savemodel->save()){
                Yii::error('Не сохранились данные модели '.$this->className());                
            }
        }
    }
}