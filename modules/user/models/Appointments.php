<?php

namespace app\modules\user\models;

use Yii;
use app\modules\user\UserModule;
use \yii\helpers\ArrayHelper;
/**
 * This is the model class for table "appointments".
 *
 * @property integer $pk_appointment
 * @property string $name
 *
 * @property User[] $users
 */
class Appointments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'appointments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_appointment' => UserModule::t('all', 'Pk appointment'),
            'name' => UserModule::t('all', 'Appointment name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['fk_appointment' => 'pk_appointment']);
    }
    
    /*
     * список всех дивизионов
     * $id - получить по ид
     */
    public function getAll($id = null) {
        $all = Appointments::find()->orderBy(['name'=>SORT_ASC])->asArray()->all();
        $b = ArrayHelper::map($all, 'pk_appointment', 'name');
        if($id) return $b[$id];
        return $b;
    }
    
    /*
     * получить название должности по ид
     */
    public function getOneName($id) {
        $appointment = Appointments::findOne($id);
        return $appointment->name;
    }
}