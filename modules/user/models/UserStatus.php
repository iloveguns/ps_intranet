<?php

namespace app\modules\user\models;

use Yii;
use \yii\helpers\ArrayHelper;
use app\modules\user\UserModule;
use app\modules\user\models\User;

class UserStatus extends \yii\db\ActiveRecord {
    const WORKING = 1;//нормальный
    const ONHOLIDAY = 2;//отпуск
    const SICK = 3;//больничный
    const DECREE = 4;//декрет
    const BUSINESSTRIP = 5;//в командировке
    const FIRED = 9;//уволен

    public static function tableName() {
        return 'user_status';
    }

    public function rules()    {
        return [
            [['status'], 'required'],
            [['status'], 'integer'],
            [['from', 'to'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'pk_status' => Yii::t('app', 'Pk Status'),
            'status' => UserModule::t('all', 'Status'),
            'from' => Yii::t('app', 'From'),
            'to' => Yii::t('app', 'To'),
        ];
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['fk_status' => 'pk_status']);
    }
    
    public function getUsers() {
        return $this->hasMany(User::className(), ['fk_status' => 'pk_status']);
    }
    
    public function getStatus($id = NULL) {
        $b = [self::WORKING => 'Работает', self::ONHOLIDAY => 'В отпуске', self::SICK => 'На больничном', self::DECREE => 'В декрете', self::BUSINESSTRIP => 'В командировке', self::FIRED => 'Уволен'];
        if($id) return $b[$id];
        return $b;
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if($this->status == self::FIRED) {//действия при увольнении сотрудника
                \app\modules\crm\helpers\CrmHelper::deleteUserFromCrm($this->user->id_user);//удалить из црм
            }
            
            // если уволен или декрет - поставить дату смены статуса
            if(($this->status == self::FIRED || $this->status == self::DECREE) && $this->oldAttributes['status'] != $this->status) {
                $this->from = \app\models\FunctionModel::getDateTimestamp();
            }
            return true;
        }
        return false;
    }
}
