<?php
//auth_key необходим для залогинивания по кукам
namespace app\modules\user\models;

use Yii;
use app\modules\user\UserModule;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use app\modules\user\models\UserStatus;
use kartik\date\DatePicker;
use app\modules\user\models\Appointments;
use app\modules\intranet\models\Tasks;
use app\modules\intranet\models\Projects;
use app\models\EntityUserDone;
use app\helpers\Mail;
use app\modules\intranet\models\Events;
use app\helpers\Image;

class User extends ActiveRecord implements \yii\web\IdentityInterface, \app\modules\user\interfaces\Structure, \app\modules\intranet\interfaces\AllInterface, \app\interfaces\ClassnameInterface
{
    CONST ALL_ACCESS = 'all_access';
    
    protected $worktasks;//getWorkTasks()

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['registration_date', 'update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_date'],
                ],
                'value' => function() { return \app\models\FunctionModel::getDateTimestamp();},
            ],
        ];
    }

    public static function tableName()
    {
        return 'user';
    }

    public function rules()
    {
        return [
            [['auth_key','birthday', 'registration_date', 'update_date', 'office_num'], 'safe'],
            [['lat', 'lng'], 'double'],
            [['sex', 'marital', 'photo', 'phone', 'email', 'icq', 'skype'], 'string'],
            [['email','password','fk_department', 'fk_department', 'fk_organization', 'lastname', 'name', 'secondname'], 'required'],
            [['fk_organization', 'fk_divizion', 'fk_department', 'sort', 'fk_status', 'fk_appointment', 'fk_role', 'setting_notify_email', 'where_from', 'telegram_chatid'], 'integer'],
            ['email', 'email'],
            [['lastname', 'name', 'secondname'], 'string', 'max' => 45],
            [['password'], 'string', 'max' => 50]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_user' => UserModule::t('all', 'Id User'),
            'lastname' => UserModule::t('all', 'Lastname'),
            'name' => UserModule::t('all', 'Name'),
            'secondname' => UserModule::t('all', 'Secondname'),
            'where_from' => UserModule::t('all', 'User Where From'),
            'birthday' => UserModule::t('all', 'Birthday'),
            'sex' => UserModule::t('all', 'Sex'),
            'marital' => UserModule::t('all', 'Marital'),
            'photo' => UserModule::t('all', 'Photo'),
            'phone' => UserModule::t('all', 'Phone'),
            'email' => UserModule::t('all', 'Email'),
            'password' => UserModule::t('all', 'Password'),
            'icq' => UserModule::t('all', 'Icq'),
            'skype' => UserModule::t('all', 'Skype'),
            'registration_date' => UserModule::t('all', 'Registration Date'),
            'fk_status' => UserModule::t('all', 'Status'),
            'fk_appointment' => UserModule::t('all', 'Appointment'),
            'update_date' => UserModule::t('all', 'Update Date'),
            'fk_organization' => UserModule::t('all', 'fk_organization'),
            'fk_divizion' => UserModule::t('all', 'fk_divizion'),
            'fk_department' => UserModule::t('all', 'fk_department'),
            'fk_role' => UserModule::t('all', 'Fk Role'),
            'sort' => UserModule::t('all', 'sort'),
            'setting_notify_email' => UserModule::t('all', 'setting_notify_email'),
            'office_num' => UserModule::t('all', 'office_num'),
        ];
    }
    
    /*
     * бот, создающий объявления и тд. имеет NULL в поле автор_ид
     * создан класс NullUser
     */
    public function getNullUser() {
        return Yii::t('app/models', 'Null User');
    }
    
    /*
     * связь с детьми
     */
    public function getChildren() {   
        return $this->hasMany(UserChildren::className(), ['fk_user' => 'id_user']);
    }
    
    /*
     * выполнение действий после подтверждения регистрации сотрудника
     * $approve - успешно ли подтверждение
     */
    public function afterRegistration($approve = true) {
        //удалить уведомление у всех админов
        \app\models\UnreadNotice::deleteAll('item_id = :item_id AND class = :class',[':item_id' => $this->id_user, ':class' => $this->getName()]);
        
        //письмо об успешной регистрации
        if($approve){
            Mail::send($this->email, Yii::t('app/mails', 'Success Registration'),[], '@app/mail/register/index');
        }
    }
    
    
    /*
     * выводит список атрибутов поиска из attributeLabels() с отсеиванием
     */
    public function getAttrs() {
        $hiddenAttrsArr = ['id_user','sort','photo', 'setting_notify_email', 'password'];//атрибуты, которые не надо выводить
        $labels = self::attributeLabels();
        
        foreach ($hiddenAttrsArr as $attr) {
            unset($labels[$attr]);
        }        
        
        return $labels;
    }
    /*
     * вывод формы создания параметров поиска
     */
    public function getSearchFormAttributes() {
        $model = new UserSearch();
        $cache = Yii::$app->cache;
        
        if($model->load(Yii::$app->request->post())){//если данные получены - используем и заодно в кеш
            $cache->set('user-search', $model->attributes,60*60*24);
        }
        else if($cache->get('user-search') === false){//если в кеше ничего нет, то данные по умолчанию
            $model = self::defaultSearchAttrs($model);
        }
        else{//иначе берем из кеша
            $model->attributes = $cache->get('user-search');
        }
        $model = self::necessarySearchAttrs($model);//внести обязательные поля
        return $model;
    }
    
    /*
     * обязательные поля формы поиска
     */
    private function necessarySearchAttrs($model) {
        $model->id_user = 1;
        return $model;
    }
    
    /*
     * стандартные поля на вывод формы поиска
     */
    private function defaultSearchAttrs($model) {        
        $model->name = 1;
        $model->lastname = 1;
        return $model;
    }
    
    
    /*
     * выбрать из параметров включенные и вывести их в виджет gridview
     * кастомизировать некоторые
     */
    public function getAttrsToGridView($attributes,$searchModel){
        $arr = [];
        foreach ($attributes as $name => $value) {
            if($value){
                switch ($name){
                    case 'id_user' : //не выводить
                        break;
                    case 'fk_status' : //статус
                        $arr[] = [
                            'attribute' => 'fk_status',
                            'value' => function ($model, $key, $index, $column) {
                                return UserStatus::getStatus($model->fkStatus->status);
                            },
                            'filter' => UserStatus::getStatus(),
                        ];
                        break;
                    
                    case 'birthday' : //день рождения
                        $arr[] = [
                            'attribute' => 'birthday',
                            'headerOptions' => ['width' => '220px'],
                            'filter' => DatePicker::widget([
                                'model' => $searchModel, 
                                'attribute' => 'birthday',
                                'removeButton' => false,
                                'options' => ['placeholder' => UserModule::t('all','Enter date')],
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'yyyy-mm-dd'
                                ]
                            ]),
                        ];
                        break;
                    
                    case 'fk_organization' : //организация
                        $arr[] = [
                            'attribute' => 'fk_organization',
                            'value' => function ($model, $key, $index, $column) {
                                return $model->fkOrganization->name;
                            },
                            'filter' => Organization::getAll(),
                        ];
                        break;
                    
                    case 'fk_divizion' : //дивизион
                        $arr[] = [
                            'attribute' => 'fk_divizion',
                            'value' => function ($model, $key, $index, $column) {
                                return $model->fkDivizion->name;
                            },
                            'filter' => Divizion::getAll(),
                        ];
                        break;
                    
                    case 'fk_department' : //отдел
                        $arr[] = [
                            'attribute' => 'fk_department',
                            'value' => function ($model, $key, $index, $column) {
                                return $model->fkDepartment->name;
                            },
                            'filter' => Department::getAll(),
                        ];
                        break;
                    
                    case 'sex' : //пол
                        $arr[] = [
                            'attribute' => 'sex',
                            'value' => function ($model, $key, $index, $column) {
                                if($model->sex)
                                return User::getSex($model->sex);
                            },
                            'filter' => User::getSex(),
                        ];
                        break;
                        
                    case 'fk_appointment' : //должность
                        $arr[] = [
                            'attribute' => 'fk_appointment',
                            'value' => function ($model, $key, $index, $column) {
                                if($model->fk_appointment)
                                return Appointments::getAll($model->fk_appointment);
                            },
                            'filter' => Appointments::getAll(),
                        ];
                        break;
                        
                    case 'fk_role' : //роль
                        $arr[] = [
                            'attribute' => 'fk_role',
                            'value' => function ($model, $key, $index, $column) {
                                if($model->fk_role)
                                return GeneralRoles::getAll($model->fk_role);
                            },
                            'filter' => GeneralRoles::getAll(),
                        ];
                        break;
                    
                    case 'marital' : //семейное положение
                        $arr[] = [
                            'attribute' => 'marital',
                            'value' => function ($model, $key, $index, $column) {
                                if($model->marital)
                                return User::getMarital($model->marital);
                            },
                            'filter' => User::getMarital(),
                        ];
                        break;
                        
                    case 'lastname' : //фамилия ссылка
                        $arr[] = [
                            'attribute' => 'lastname',
                            'value' => function ($model, $key, $index, $column) {
                                return $model->getLinkToProfile($model->lastname, false, false);
                            },
                            'format' => 'raw',
                        ];
                        break;
                    
                    case 'where_from' : //город
                        $arr[] = [
                            'attribute' => 'where_from',
                            'value' => function ($model, $key, $index, $column) {
                                if($model->where_from)
                                return \app\modules\crm\models\AddressCity::getAll($model->where_from);
                            },
                            'filter' => \app\modules\crm\models\AddressCity::getAll(),
                        ];
                        break;
                    
                    case 'office_num' : //номер кабинета
                        $arr[] = [
                            'attribute' => 'office_num',
                            'value' => function ($model, $key, $index, $column) {
                                return $model->office_num;
                            },
                            'format' => 'raw',
                        ];
                        break;
                    
                    case 'name' : //имя ссылка
                        $arr[] = [
                            'attribute' => 'name',
                            'value' => function ($model, $key, $index, $column) {
                                return $model->getLinkToProfile($model->name, false, false);
                            },
                            'format' => 'raw',
                        ];
                        break;
                    
                    default :
                        $arr[] = $name;
                        break;
                }
            }
        }
        
        $gridviewarr = array_merge(
            [['class' => 'yii\grid\SerialColumn']],
            $arr,
            [[
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Действия', 
                'headerOptions' => ['width' => '20'],
                'template' => '{update}',
                'visible' => Yii::$app->user->identity->can('tousers') ? true : false,
            ]]
        );
        return $gridviewarr;
    }
    
    /*
     * поиск сотрудника по строке(по имени, отчеству, фамилии)
     * $string - строка поиска
     * $limit - limit sql
     */
    public function searchUserByString($string, $limit = 5) {
        $find = User::findActive()
                ->andFilterWhere(['like', 'lastname', $string])
                ->orFilterWhere(['like', 'name', $string])
                ->orFilterWhere(['like', 'secondname', $string])
                ->limit($limit)
                ->all();
        return $find;
    }

    /*
     * получение данных о сотрудниках любых
     */
    public function getAll($id = NULL, $format = '{lastname} {name} {secondname}') {
        $all = User::getDb()->cache(function ($db) use($format) {
            $all = User::find()->asArray()->all();
            $b = [];        
            foreach ($all as $user) {
                $s = preg_replace('#\{lastname}#s', $user['lastname'], $format);
                $s = preg_replace('#\{name}#s', $user['name'], $s);
                $s = preg_replace('#\{secondname}#s', $user['secondname'], $s);

                $b[$user['id_user']] = $s;
            }
            return $b;
        });
        
        
        if($id) return $all[$id];
        return $all;
    }
    
    /*
     * получение данных о сотрудниках только активных (копия сверху)
     */
    public function getAllActive($id = NULL, $onlyids = false, $format = '{lastname} {name} {secondname}') {
        $all = User::getDb()->cache(function ($db) {
            return User::findActive()->asArray()->orderBy('lastname ASC')->all();
        });
        
        $b = [];
        foreach ($all as $user) {
            $s = preg_replace('#\{lastname}#s', $user['lastname'], $format);
            $s = preg_replace('#\{name}#s', $user['name'], $s);
            $s = preg_replace('#\{secondname}#s', $user['secondname'], $s);
            
            $b[$user['id_user']] = ($onlyids) ? $user['id_user'] : $s;
        }
        if($id) return $b[$id];
        return $b;
    }
    
    public function getOnlineData()
    {
        return $this->hasOne(\app\models\UsersOnline::className(), ['pk_user' => 'id_user']);
    }
    
    /**
     * загруженные файлы
     */
    public function getUploadFiles()
    {
        return $this->hasMany(UploadFiles::className(), ['id_user' => 'id_user']);
    }
    
    /**
     * руководитель проекта
     */
    public function getProjectsHead()
    {
        return $this->hasMany(\app\modules\intranet\models\Projects::className(), ['id_head' => 'id_user']);
    }

    /**
     * автор проекта
     */
    public function getProjectsAuthor()
    {
        return $this->hasMany(\app\modules\intranet\models\Projects::className(), ['id_author' => 'id_user']);
    }
    
    /**
     * @return непрочитанные уведомления(все)
     */
    public function getUnreadNotices($limit = 10)
    {
        return $this->hasMany(\app\models\UnreadNotice::className(), ['id_user' => 'id_user'])->andOnCondition(['readed' => \app\models\UnreadNotice::READED_NOTREAD])->orderBy(['id_notice' => SORT_DESC])->limit($limit);
    }
    
    /*
     * просто число непрочитанных уведомлений
     */
    public function getCountUnreadNotices()
    {
        return $this->hasMany(\app\models\UnreadNotice::className(), ['id_user' => 'id_user'])->andOnCondition(['readed' => \app\models\UnreadNotice::READED_NOTREAD])->orderBy(['id_notice' => SORT_DESC])->count();
    }
    
    /*
     * получить последние уведомления(типа архив)
     * $limit - кол-во записей
     */
    public function getLastReadNotices($limit = 10) {
        return $this->hasMany(\app\models\UnreadNotice::className(), ['id_user' => 'id_user'])->andOnCondition(['readed' => \app\models\UnreadNotice::READED_READ])->orderBy(['id_notice' => SORT_DESC])->limit($limit);
    }
    
    /*
     * заметки
     */
    public function getPersonalNotes()
    {
        return $this->hasMany(PersonalNotes::className(), ['fk_user' => 'id_user']);
    }
    
    /**
     * автор задачи
     */
    public function getTasksAuthor()
    {
        return $this->hasMany(Tasks::className(), ['id_author' => 'id_user']);
    }
    
    public function getEventsAuthor()
    {
        return $this->hasMany(Events::className(), ['id_author' => 'id_user']);
    }
    
    /**
     * руководитель задачи
     */
    public function getTasksHead()
    {
        return $this->hasMany(Tasks::className(), ['id_head' => 'id_user']);
    }
    
    public function getEventsHead()
    {
        return $this->hasMany(Events::className(), ['id_author' => 'id_user']);
    }
    
    /*
     * связь с задачами активными
     * $m = User::findOne(1);
     * var_dump($m->tasks[0]->pk_task);
     * 
     * $date - date (2016-06-06) - найти задачи в текущую дату
     * $end(2 параметр вместе c $date) - найти задачи в промежутке $date - $end
     * $quest - поручение (1) или задача(0)
     * $sort - array сортировка
     */
    public function getTasks($date = NULL, $end = NULL, $quest = 0, array $sort = ['end_date' => SORT_ASC]) {
        $query = $this->hasMany(\app\modules\intranet\models\Tasks::className(), ['pk_task' => 'item_id'])->andOnCondition(['status' => Tasks::STATUS_WORK, 'quest' => $quest]);
        
        if($date && $end === NULL) {//только 1 параметр
            $dateend = $date.' 23:59:59';
            
            $query->andWhere(['<=', 'start_date', $dateend])->andWhere(['>=', 'end_date', $date]);//дата начала и конца
        } else if($end !== NULL && $date !== NULL){//2 параметра
            $datestart = $date.' 00:00:00';
            $dateend = $end.' 23:59:59';
            
            $query->orFilterWhere(['and', ['>=', 'start_date', $datestart], ['<=', 'end_date', $dateend]])//задачи в пределах дат
                ->orFilterWhere(['and', ['<=', 'start_date', $datestart], ['>=', 'end_date', $dateend]])//задачи которые идут в это время
                ->orFilterWhere(['and', ['<=', 'start_date', $datestart], ['>=', 'end_date', $datestart]])
                ->orFilterWhere(['and', ['<=', 'start_date', $dateend], ['>=', 'end_date', $dateend]]);
        }
        
        
        $query->viaTable('relation_to_user', ['id_user' => 'id_user'], function($query){
                $query->andWhere(['class' => \app\modules\intranet\models\Tasks::getName()]);//фильтр для связанной таблицы
            })->orderBy($sort);
        
        if($date) return $query->all();//вот так
        return $query;
    }
    
    /**
     * задачи, заканчивающиеся в период
     * гораздо проще условие поиска, чем getTasks()
     * 
     * @param {Date} $start - поиск от даты
     * @param {Date} $end   - поиск до даты
     */
    public function getTasksEndInPeriod($start, $end) {
        $query = $this->hasMany(\app\modules\intranet\models\Tasks::className(), ['pk_task' => 'item_id'])
                ->andOnCondition(['status' => Tasks::STATUS_WORK]);
        
        $query->andWhere(['between', 'end_date', $start, $end]);        
        
        $query->viaTable('relation_to_user', ['id_user' => 'id_user'], function($query){
                $query->andWhere(['class' => \app\modules\intranet\models\Tasks::getName()]);//фильтр для связанной таблицы
            });
        
        return $query->all();
    }
    
    /*
     * $public = 'all'(все),true(только публичные),false(только не публичные)
     * +подбор повторяющихся
     */
    public function getEvents($public = 'all', $date = NULL, $end = NULL) {
        $query = $this->hasMany(Events::className(), ['pk_event' => 'item_id']);
        $query->andWhere(['like', 'json_repeat', '"forever":true']);//вечные повторы сразу добавить
        
        /*
         * найти ненулевые повторы, просчитать их и посмотреть, попадают ли они в этот диапазон.
         * достаточно неуклюжий подбор, зато 100% корректный
         */
        $dep = new \yii\caching\DbDependency();
        $dep->sql = 'SELECT count(*) FROM '.Events::tableName().' WHERE id_author = '.Yii::$app->user->id;
        $arrIds = Yii::$app->db->cache(function ($db) use($date, $end) {
            $eventsNotNull = $this->hasMany(Events::className(), ['pk_event' => 'item_id']);
            $eventsNotNull->andWhere(['not', ['json_repeat' => NULL]]);
            $eventsNotNull->viaTable('relation_to_user', ['id_user' => 'id_user'], function($query){
                    $query->andWhere(['class' => Events::getName()]);//фильтр для связанной таблицы
                })->orderBy(['pk_event' => SORT_DESC]);
            $events = $eventsNotNull->all();

            $arrIds = [];

            //каждое событие просчитать
            foreach ($events as $event) {
                if($repeat = json_decode($event->json_repeat, TRUE)) {
                    $start_date = $event->start_date;
                    $end_date = $event->end_date;
                    $modif = '';
                    $count = 0;

                    if(isset($repeat['count'])) {//указано регулярное повторение
                        $count = $repeat['count'];
                    }

                    switch ($repeat['select']) {
                        case Events::REPEAT_WEEKLY:
                            $modif = 'week';
                            break;

                        case Events::REPEAT_DAILY:
                            $modif = 'day';
                            break;

                        case Events::REPEAT_MONTHLY:
                            $modif = 'month';
                            break;

                        case Events::REPEAT_YEARLY:
                            $modif = 'year';
                            break;

                        case Events::REPEAT_DATE:
                            if($repeat['date'] >= $date && $repeat['date'] <= $end){//дата соотв диапазону
                                $arrIds[$event->pk_event] = $event->pk_event;
                            }
                            continue;
                    }

                    //просчитать и собрать ид подходящих
                    for($i = 1; $i <= $count; $i ++) {
                        $event->start_date = \app\models\FunctionModel::dateModify($start_date, '+'.$i.' '.$modif);
                        $event->end_date = \app\models\FunctionModel::dateModify($end_date, '+'.$i.' '.$modif);
                        if($event->start_date >= $date && $event->end_date <= $end){//точное совпадение с промежутком
                            $arrIds[$event->pk_event] = $event->pk_event;
                            break;
                        }
                    }
                }
            }
            return $arrIds;
        }, 5000, $dep);
        //добавочное условие к основному поиску
        if(!empty($arrIds)) $query->orWhere(['in', 'pk_event', $arrIds]);
        //end найти ненулевые
        
        if(is_bool($public)){
            ($public === true) ? $public = 1 : $public = 0 ;
            
            $query->andOnCondition(['is_public' => $public]);
        }
        
        if($date && $end === NULL) {//только 1 параметр
            $dateend = $date.' 23:59:59';
            
            $query->andWhere(['<=', 'start_date', $dateend])->andWhere(['>=', 'end_date', $date]);//дата начала и конца
        } else if($end !== NULL && $date !== NULL){//2 параметра
            $datestart = $date.' 00:00:00';
            $dateend = $end.' 23:59:59';
            
            $query->orFilterWhere(['and', ['>=', 'start_date', $datestart], ['<=', 'end_date', $dateend]])//задачи в пределах дат
                ->orFilterWhere(['and', ['<=', 'start_date', $datestart], ['>=', 'end_date', $dateend]])//задачи которые идут в это время
                ->orFilterWhere(['and', ['<=', 'start_date', $datestart], ['>=', 'end_date', $datestart]])
                ->orFilterWhere(['and', ['<=', 'start_date', $dateend], ['>=', 'end_date', $dateend]]);
        }
        
        
        $query->viaTable('relation_to_user', ['id_user' => 'id_user'], function($query){
                $query->andWhere(['class' => \app\modules\intranet\models\Events::getName()]);//фильтр для связанной таблицы
            })->orderBy(['pk_event' => SORT_DESC]);
        
        if($date) return $query->all();//вот так
        return $query;
    }
    
    //связь с поручениями
    public function getQuests() {
        return $this->getTasks(NULL, NULL, 1);
    }
    
    /**
     * ЗДЕСЬ ОШИБКА, НЕ НАХОДИТ ЗАДАЧУ 2817 у 422, искать юзер+отдел+дивизион+организация
     * возвращает все задачи сотрудник
     */
    public function getAllTasks()
    {   
        return $this->hasMany(\app\modules\intranet\models\Tasks::className(), ['pk_task' => 'item_id'])
            ->viaTable('relation_to_user', ['id_user' => 'id_user'], function($query){
                $query->andWhere(['class' => \app\modules\intranet\models\Tasks::getName()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * найти все задачи, даже если сотрудник не выбран в списке, но является автором или ответственным
     * то что нужно. просто список всех задач
     */
    public function getAllTasksUserAll($find = false, array $sort = ['end_date' => SORT_ASC]) {
        $searchModel = new \app\modules\intranet\models\TasksSearch();
        $query = $searchModel->search(['TasksSearch' => ['status' => Tasks::STATUS_ALLNOTDONE]])->query;
        $query->with('comments');//жадно, но сокращает запросы
        $query->orderBy($sort);
        if($find) return $query->all();
        return $query;
    }
    
    /*
     * связь с проектами
     * $m = User::findOne(1);
     * var_dump($m->projects[0]->pk_project);
     */
    public function getProjects()
    {
        return $this->hasMany(\app\modules\intranet\models\Projects::className(), ['pk_project' => 'item_id'])
            ->viaTable('relation_to_user', ['id_user' => 'id_user'], function($query){
                $query->andWhere(['class' => \app\modules\intranet\models\Projects::getName()]);//фильтр для связанной таблицы
            });
    }
    
    /*
     * связь с проектами но только проекты в процессе
     */
    public function getWorkProjects()
    {
        $models = Projects::findAll(['status' => Projects::STATUS_WORK]);
        $arr = [];
        
        foreach ($models as $model) {//найти те, в которых есть сотрудник
            if(array_key_exists($this->id_user,  Units::onlyIdUsers($model,true,true))){
                $arr[] = $model;
            }
        }
        return $arr;
        /*return $this->hasMany(\app\modules\intranet\models\Projects::className(), ['pk_project' => 'item_id'])->andOnCondition(['status' => Projects::STATUS_WORK])
            ->viaTable('relation_to_user', ['id_user' => 'id_user'], function($query){
                $query->andWhere(['class' => \app\modules\intranet\models\Projects::className()]);//фильтр для связанной таблицы
            });*/
    }

    /**
     * получить отдел
     */
    public function getFkDepartment()
    {
        return $this->hasOne(Department::className(), ['pk_department' => 'fk_department']);
    }

    /**
     * получить дивизион
     */
    public function getFkDivizion()
    {
        return $this->hasOne(Divizion::className(), ['pk_divizion' => 'fk_divizion']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAppointment()
    {
        return $this->hasOne(Appointments::className(), ['pk_appointment' => 'fk_appointment']);
    }
    
    /**
     * получить организацию
     */
    public function getFkOrganization()
    {
        return $this->hasOne(Organization::className(), ['pk_organization' => 'fk_organization']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkStatus()
    {
        return $this->hasOne(UserStatus::className(), ['pk_status' => 'fk_status']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkRole()
    {
        return $this->hasOne(GeneralRoles::className(), ['pk_role' => 'fk_role']);
    }
    
    /**
     * комментарии сотрудника
     * пока не активны
     */
    /*public function getComments()
    {
        return $this->hasMany(\app\models\Comments::className(), ['id_user' => 'id_user']);
    }*/
    
    /**
     * просмотр выполнения задач(не получается искать по сотруднику и задаче)
     */
    public function getTaskUserDone()
    {
        return $this->hasOne(EntityUserDone::className(), ['id_user' => 'id_user', 'class' =>self::getName()]);
    }
    
    /*
     * удобный поиск по сотруднику и задаче
     * должна быть только одна запись каждого сотрудника по задаче
     */
    public function getTaskUserInfo($model) {
        return EntityUserDone::findOne(['id_user' => $this->id_user, 'item_id' => $model->pk_task, 'class' =>$model::getName()]);
    }
    
    /*
     * определить является ли сотрудник наблюдателем в сущности по ид и классу
     */
    public function isWatcher($id, $class) {
        $a = Yii::$app->db->createCommand('SELECT `watcher` FROM `relation_to_user` WHERE `id_user` = '.$this->id_user.' AND `class` = "'.addslashes($class).'" AND `item_id` = '.$id)->queryOne();
        return ($a['watcher']) ? true : false;
    }
    
    /*
     * удобный поиск по сотруднику и подзадачи(только ответственный)
     * сотрудник не учитывается, а только класс и ид подзадачи
     */
    public function getSubTaskUserInfo($model) {
        return EntityUserDone::findOne(['item_id' => $model->pk_subtask, 'class' =>$model::getName()]);
    }
    
    /*
     * графические связи/отношения
     */
    public function getGraphicRelates()
    {
        return $this->hasMany(\app\models\GraphicRelate::className(), ['fk_user_gr' => 'id_user']);
    }
    
    /*
     * поиск по емайлу для аутентификации
     */
    public static function findByUseremail($useremail)
    {
        return static::findOne(['email' => $useremail]);
    }
    
    public function findById($id){
        return User::findOne($id);
    }
    
    /*
     * последние активные сотрудники
     */
    public function getLastActive($limit) {
        return User::findActive()->limit($limit)->orderBy(['id_user' => SORT_DESC])->all();
    }
    
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }
    
    /*
     * через этот метод можно обращаться к свойствам модели Yii::$app->user->identity->username - (getUsername())
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }
    
    public static function findByUsername($username)
    {
        
    }
    
    /*
     * ссылка на сотрудника
     * $user->getLinkToProfile('text', true)
     */
    public function getLinkToProfile($content = false, $tooltip = false, $cluetip = true) {
        $params = ['target' => '_blank'];
        
        if(!$content) $content = $this->fullName;//по умолчанию - ФИО
        
        if($tooltip){
            $params['data-toggle'] = 'tooltip';
            $params['data-placement'] = 'auto';
            $params['title'] = $this->fullName;
        }
        
        if($cluetip){
            $params['data-cluetip'] = 'true';
        }
        
        return \yii\helpers\Html::a($content, ['/user/user/profile', 'id' => $this->id_user], $params);
    }
    
    /*
     * ссылка на написание сообщения
     * OLD
     */
    public function getLinkToSendMessage($content = false, $params = []) {
        $param = ['target' => '_blank'];
        $param = array_merge($param, $params);
        
        if(!$content) $content = $this->fullName;//по умолчанию - ФИО
        
        return \yii\helpers\Html::a($content, '/dialog#' . $this->id_user, $param);
    }
    
    /*
     * возвращает ссылку на фото пользователя
     */
    public function getAvatar()
    {
        if(!$this->photo) $this->photo = '/images/nophoto.png';
        return $this->photo;
    }
    
    /*
     * фото сотрудника в теге img
     * @param $options {array}  - параметры тега изображения
     * @param $crop {array}     - параметры изменения размеров фото[ширина, высота]
     */
    public function getImgAvatar($options = [], $crop = []) {
        $params = ['width' => '100%'];
        $params = array_merge($params,$options);
        
        $img = self::getAvatar();
        
        if($crop) {
            if(!isset($crop[0]) || !isset($crop[1])) throw new \yii\base\InvalidParamException('both width and height are required');
            $img = Image::thumb($img, $crop[0], $crop[1]);
        }
        
        // ленивая загрузка для куч изображений
        if(isset($params['lazy']) && $params['lazy'] === true) {
            $params['data-src'] = $img;
            return \yii\helpers\Html::img('', $params);
        }
        
        return \yii\helpers\Html::img($img, $params);
    }
    
    /*
     * фото сотрудника в теге img ссылкой
     * $user->getLinkAvatar(true)
     * $user->linkAvatar
     */
    public function getLinkAvatar($tooltip = false, $options = [], $crop = []) {
        return self::getLinkToProfile(self::getImgAvatar($options, $crop), $tooltip, true);
        
    }
    
    /*
     * возвращает ссылку на фото пользователя
     */
    public function getAppointment()
    {
        return Appointments::getOneName($this->fk_appointment);
    }
    
    /*
     * возвращает id пользователя
     */
    public function getId()
    {
        return $this->id_user;
    }
    /*
     * возвращает фамилию имя пользователя
     */
    public function getUsername()
    {
        return $this->lastname.' '.$this->name;
    }
    
    /*
     * полное фио
     */
    public function getFullName($format = '{lastname} {name} {secondname}')
    {
        $s = preg_replace('#\{lastname}#s', $this->lastname, $format);
        $s = preg_replace('#\{name}#s', $this->name, $s);
        $s = preg_replace('#\{secondname}#s', $this->secondname, $s);
        return $s;
    }
    
    /*
     * отправлять ли уведомления на email
     */
    public function getSendNotifyToEmail() {
        return ($this->setting_notify_email == 1) ? true : false;
    }
    
    /*cookie*/
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    
    //--найти сущности
    /*
     * получить все проекты, в которых участвует сотрудник(ИД сотрудника или ИД отдела или ИД дивизиона или ИД организации или автор пустого проекта, в которых он состоит)
     * $nameEntity -className() сущности(projects, tasks)
     * $names = false // включить ли название сущности
     * @return array of object{project}
     */
    public function getAllEntityUser($nameEntity, $names = false) {
        $allIds = [];
        $pk = '';//название пк столбца модели
        $entity = '';//название сущности
        $thisClass = Yii::$app->user->identity;
        $full = true;
        
        switch ($nameEntity){//специфичные для каждой модели
            case Projects::getName() :
                $pk = 'pk_project';
                $entity = 'projects';
                break;
            
            case Tasks::getName() :
                $pk = 'pk_task';
                $entity = 'tasks';
                foreach ($thisClass->allTasks as $p) {//организации, в которой сотрудник
                    $allIds[$p[$pk]] = ($names) ? $p->title : $p[$pk];
                }
                break;
                
            case \app\modules\intranet\models\Events::getName() :
                $pk = 'pk_event';
                $entity = 'events';
                $full = false;
                break;
            
            default :
                throw new \yii\base\InvalidParamException('передай нормальный параметр' . $nameEntity);
        }
        
        //предварительно надо добавить во все модели связи
        
        //руководителя добавить даже если никого не назначено
        foreach ($thisClass[$entity.'Head'] as $p) {
            $allIds[$p[$pk]] = ($names) ? $p->title : $p[$pk];
        }

        //автор пусть тоже видит
        foreach ($thisClass[$entity.'Author'] as $p) {
            $allIds[$p[$pk]] = ($names) ? $p->title : $p[$pk];
        }

        foreach ($thisClass[$entity] as $p) {//сотрудника
            $allIds[$p[$pk]] = ($names) ? $p->title : $p[$pk];
        }

        if($full){
            foreach ($thisClass->fkDepartment[$entity] as $p) {//отдела, в котором сотрудник
                $allIds[$p[$pk]] = ($names) ? $p->title : $p[$pk];
            }

            foreach ($thisClass->fkDivizion[$entity] as $p) {//дивизиона, в котором сотрудник
                $allIds[$p[$pk]] = ($names) ? $p->title : $p[$pk];
            }

            foreach ($thisClass->fkOrganization[$entity] as $p) {//организации, в которой сотрудник
                $allIds[$p[$pk]] = ($names) ? $p->title : $p[$pk];
            }
        }
        
        // по ид проектов выбрать их в порядке создания. новые - выше
        if($entity === 'projects' && $names) {
            $id_arr = [];
            
            foreach ($allIds as $id => $name) {
                $id_arr[] = $id;                
            }
            
            $allIds = [];
            
            $pr = Projects::find()->select(['pk_project', 'title'])->asArray()->where(['IN', 'pk_project', $id_arr])->orderBy('create_date DESC')->all();
            
            foreach ($pr as $proj) {
                $allIds[$proj['pk_project']] = $proj['title']; 
            }            
        }
        
        return $allIds;
    }
    
    /*
     * @return array ид проектов, в которых состоит сотрудник
     * передавать имя модели
     */
    public function getAllIdEntityUser($modelName) {
        return self::getAllEntityUser($modelName);
    }
    
    /*
     * @return array ид=>название проектов, в которых состоит сотрудник
     * Yii::$app->user->identity->allDropListProjectUser
     */
    public function getAllDropListEntityUser($modelName) {
        //$a = ['' => ''];//ни к чему не привязан
        return self::getAllEntityUser($modelName, true);
    }
    //--найти сущности
    
    /*
     * получить пол
     */
    public function getSex($id = NULL) {
        $b = ['m' => Yii::t('app/models', 'Male'), 'f' => Yii::t('app/models', 'Female')];
        if($id) return $b[$id];
        return $b;
    }
    
    /*
     * получить семейное положение
     */
    public function getMarital($id = FALSE) {
        if($id === NULL) return Yii::t('app/models', 'Not Set');
        $b = ['single' => UserModule::t('all', 'Single'), 'married' => UserModule::t('all', 'Married')];
        if($id) return $b[$id];
        return $b;
    }
    
    /*
     * сохранение структуры(сотрудников)
     */
    public function saveUsers($arr) {
        foreach ($arr as $fk_department => $user) {
            foreach ($user as $sort => $id) {
                $divizion = Department::findOne($fk_department);
                //обновление всей структуры
                $q = Yii::$app->db->createCommand()->update(User::tableName(), ['sort' => $sort, 'fk_department' => $fk_department, 'fk_divizion' => $divizion->fkDivizion->pk_divizion, 'fk_organization' => $divizion->fkDivizion->fkOrganization->pk_organization], '`id_user` = '.$id)->execute();
            }    
        }
        
        return TRUE;
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord){
                //статус
                $status = new UserStatus();
                $status->status = $this->fk_status;
                $status->save();                
                $this->fk_status = $status->pk_status;
            }else{
                
            }
            return true;
        }
        return false;
    }
    
    /*
     * ::find() только активных(не уволенных)
     */
    public static function findActive()
    {
        return parent::find()->joinWith('fkStatus')->where(['not', ['status' => UserStatus::FIRED]]);
    }
    
    /*
     * стандартный ::find()
     */
    public static function findWIthoutSort()
    {
        return parent::find();
    }
    
    /*
     * поиск отсортированный
     */
    public static function find()
    {
        return parent::find()->orderBy('sort ASC');
    }
    
    /*
     * функция определения прав пользователя
     * @return 1 - если есть право, 0 - если нет
     * Yii::$app->user->identity->can('название права')
     */
    public function can($param) {
        if(!$this->fkRole) die('Ошибка. Нет роли у сотрудника №'.$this->id_user.'/*User.php*/');
        $rights = json_decode($this->fkRole->json_data,true);//получить права в массиве
        if(isset($rights[self::ALL_ACCESS])) return TRUE;//полный доступ ко всему(админ)
        return isset($rights[$param]);
    }
    
    /*
     * аналог, но просто админ или нет
     */
    public function isAdmin() {
        return self::can(self::ALL_ACCESS);
    }
    
    /*
     * список админов(например для уведомления всем)
     * $onlyIds(true) - только ид админов
     */
    public function getAdmins($onlyIds = true) {
        $admins = User::findAll(['fk_role' => GeneralRoles::ROLE_ADMIN]);
        if($onlyIds){
            $arr = [];
            foreach ($admins as $ar) {
                $arr[$ar['id_user']] = $ar['id_user'];
            }
            $admins = $arr;
        }
        return $admins;
    }
    
    /*
     * краткая информация о сотруднике
     */
    public function miniInfo($removeBtn = false){
        $info = '<div class="dd3-content">';
        if($removeBtn) $info .= Units::removeButton();
        $info .= '<div class="structure-mini-photo">'.$this->getImgAvatar([], [100, 100]).'</div><div class="col-md-12"><a href="'.\yii\helpers\Url::to(['/user/user/profile','id' => $this->id]).'" class="structure-user-link" target="_blank"><span class="struct-name">'.$this->username.'</span><span class="label label-primary pull-right">'.UserStatus::getStatus($this->fkStatus->status).'</span></a><br><span>'.$this->fkAppointment->name.'</span></div></div>';
        return $info;
    }

    public function getHtmlLink($icon) {
        
    }
    
    public function getUrl($absolute = false) {
        
    }

    public function getSelfLink() {
        
    }

    public function getTitleText() {
        return false;
    }

    public function getName() {
        return 'user';
    }

}
