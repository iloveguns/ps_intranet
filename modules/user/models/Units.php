<?php

namespace app\modules\user\models;

use Yii;
use \yii\helpers\ArrayHelper;
use app\modules\user\models\Department;
use app\modules\user\models\Organization;
use yii\helpers\Html;
use app\modules\user\UserModule;
use app\modules\user\models\User;

/*
 * получение всего, связанного со структурой организации
 */
class Units extends \yii\base\Model{    
    /*
     * жуткая дичь
     * иерархический список структуры
     * выводятся в порядке орг-див-отдел, остальные игнорируются
     * генерирует в разметке для https://github.com/dbushell/Nestable
     * $changeBtn(true) - признак вывода для администраторов(редактирование элементов)
     * $selectStruct(true) - возможность выделять структуру и получать список ид выделенных
     */
    public function hierarchy($changeBtn = false, $selectStruct = false, $showfired = false) {
        echo '<label for="struct-search-pole">'.Yii::t('app', 'Search Users').'</label>
        <input type="text" id="struct-search-pole" class="form-control">';//поле для поиска
        
        $dependency = new \yii\caching\DbDependency();//зависимость
        $dependency->sql = 'SELECT count(*) FROM '.User::tableName();
        
        $key = 'struct-hierarchy';//имя кеша
        $key .= ($changeBtn) ? 'change' : '';
        $key .= ($showfired) ? 'wfired' : 'nofired';
        
        $cache = Yii::$app->cache;
        
        $wrap = $cache->get($key);
            
        if ($wrap === false) {

            $wrap = '';
            $wrap .=  '<menu id="nestable-menu">
                    <button type="button" class="btn btn-default" data-action="expand-all">Развернуть все</button>
                    <button type="button" class="btn btn-default" data-action="collapse-all">Свернуть все</button>
                    <button type="button" class="btn btn-default" id="toggle-department">Свернуть/развернуть отделы</button>
                </menu>';
            $wrap .= '<div class="dd structure-list-main"><ol class="dd-list">';
            //может какой то альтернативный подход сделать вместо этой каши? проект который содержит всех        
            $org = new Organization();
            $org = $org->find()->all();

            foreach ($org as $organization) {
                $wrap .= '<li class="dd-item dd3-item dd-organization" data-id="'.$organization->pk_organization.'" data-struct="organization">';
                if($changeBtn) $wrap .= '<a href="'.\yii\helpers\Url::toRoute(['update','unit' => 'organization','id' => $organization->pk_organization]).'" class="pull-right"><i class="fa fa-pencil"></i></a>';
                if($changeBtn) $wrap .= '<div class="dd-handle dd3-handle"></div>';
                $wrap .= $organization->miniInfo(true);
                if(!empty($organization->divizions)){//есть привязанные дивизионы
                    $wrap .= '<ol class="dd-list">';
                    foreach ($organization->divizions as $divizion) {
                        if($showfired) {

                        } else if($divizion->countWorkusers < 1) {
                            continue;
                        }
                        
                        $wrap .= '<li class="dd-item dd3-item dd-divizion" data-id="'.$divizion->pk_divizion.'" data-struct="divizion">';
                        if($changeBtn) $wrap .= '<a href="'.\yii\helpers\Url::toRoute(['update','unit' => 'divizion','id' => $divizion->pk_divizion]).'" class="pull-right"><i class="fa fa-pencil"></i></a>';
                        if($changeBtn) $wrap .= '<div class="dd-handle dd3-handle"></div>';
                        $wrap .= $divizion->miniInfo(true);
                        if(!empty($divizion->departments)){//есть привязанные отделы
                            $wrap .= '<ol class="dd-list">';
                            foreach ($divizion->departments as $department) {
                                if($showfired) {
                                    
                                } else if($department->countWorkusers < 1) {
                                    continue;
                                }
                                
                                $wrap .= '<li class="dd-item dd3-item dd-department" data-id="'.$department->pk_department.'" data-struct="department">';
                                if($changeBtn) $wrap .= '<a href="'.\yii\helpers\Url::toRoute(['update','unit' => 'department','id' => $department->pk_department]).'" class="pull-right"><i class="fa fa-pencil"></i></a>';
                                if($changeBtn) $wrap .= '<div class="dd-handle dd3-handle"></div>';
                                $wrap .= $department->miniInfo(true);
                                if(!empty($department->users)){//вывод сотрудников
                                    $wrap .= '<ol class="dd-list">';
                                    foreach ($department->users as $user) {
                                        if($showfired){//показывать всех сотрудников, даже если уволены
                                            
                                        } else {
                                            if($user->fkStatus === null || $user->fkStatus->status == UserStatus::FIRED) continue;//отсеить недавно зареганных и уволенных
                                        }
                                        $wrap .= '<li class="dd-item dd3-item dd-user" data-id="'.$user->id_user.'" data-struct="user">';
                                        if($changeBtn) $wrap .= '<div class="dd-handle dd3-handle"></div>';
                                        if($changeBtn) $wrap .= '<a href="#" class="pull-right ajax-link-user"><i class="fa fa-pencil"></i></a>';
                                        $wrap .= $user->miniInfo(true);
                                    }
                                    $wrap .= '</ol>';
                                }
                            }
                            $wrap .= '</ol>';
                        }
                        $wrap .= '</li>';
                    }
                    $wrap .= '</ol>';
                }
                $wrap .= '</li>';
            }        
            $wrap .= '</ol></div>';
        
            $cache->set($key, $wrap, \Yii::$app->params['cacheTime'], $dependency);
        }
        
        /*if($selectStruct){//выбирать структуры            
            $wrap .= '<div class="form-group">';
            $wrap .= Html::submitButton(Yii::t('app', 'Choose selected'), ['class' => 'btn btn-success', 'id' => 'getSelectedUnits']);
            $wrap .= '</div>';
            $wrap .= Html::hiddenInput('nestable-selected-structure');
        }*/
        
Yii::$app->view->registerJsFile('@web/js/build/jquery.nestable.js',['depends' => 'app\assets\AppAsset', 'position' => yii\web\View::POS_END]);
Yii::$app->view->registerJs("
$('#toggle-department').click(function(){//свернуть/развернуть отделы
    if($('.dd-department').hasClass('dd-collapsed')){
        $('.dd').nestable('expandDepartment');
    }else{
        $('.dd').nestable('collapseDepartment');
    }
});

$('a[href=saveunits]').click(function(){unitSaveAjax();return false});//сохранение структуры

$('.ajax-link-user').click(function(){profileUserAjax($(this).parent().attr('data-id'))});//модальное окно редакт сотрудника
$('.dd').nestable({});

$('.dd').nestable('collapseDepartment');
$('#nestable-menu').on('click', function(e)
{
    var target = $(e.target),
        action = target.data('action');
    if (action === 'expand-all') {
        $('.dd').nestable('expandAll');
    }
    if (action === 'collapse-all') {
        $('.dd').nestable('collapseAll');
    }
});

$('#struct-search-pole').on('keyup',(function() {
    if($(this).val().length >= 2){//1 символ нечего гонять
        searchStruct($(this).val());
    } else if($(this).val().length == 0) {
        $('.dd-item').show()
    }
}));
", \yii\web\View::POS_READY, 'nestable');
        
        return $wrap;
    }
    
    /*
     * выдает просто кнопку удалить(font-awesome)
     */
    public function removeButton() {
        return '<a class="structure-remove" target="_blank"><i class="fa fa-times"></i></a>';
    }
    
    /*
     * определение порядка всего ко всему иерархически
     * $arr - массив значений(serialize из nestable)
     * (порядок) - (ид)
     */
    public function defineSort($arr) {        
        $org = [];//организации
        $div = [];//дивизионы
        $dep = [];//отделы
        $users = [];//сотрудники
        
        foreach ($arr as $organization) {
            $org[] = $organization['id'];
            if(isset($organization['children'])) {
                foreach ($organization['children'] as $divizion) {
                    $div[$organization['id']][] = $divizion['id'];
                    if(isset($divizion['children'])) {
                        foreach ($divizion['children'] as $department) {
                            $dep[$divizion['id']][] = $department['id'];
                            if(isset($department['children'])) {
                                foreach ($department['children'] as $user) {
                                    $users[$department['id']][] = $user['id'];
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return [
            'organizations' => $org,
            'divizions' => $div,
            'departments' => $dep,
            'users' => $users,
        ];
    }
    
    /*
     * сохранение структуры полное, после сортировки defineSort()
     * $arr - сортированный массив
     */
    public function saveSortAll($arr) {
        self::saveOrganizations($arr['organizations']);
        self::saveDivizions($arr['divizions']);
        self::saveDepartments($arr['departments']);
        User::saveUsers($arr['users']);
        return TRUE;
    }
    
    /*
     * сохранение структуры(организаций)
     */
    public function saveOrganizations($arr) {
        foreach ($arr as $sort => $id) {
            $q = Yii::$app->db->createCommand()->update(Organization::tableName(), ['sort' => $sort], 'pk_organization = '.$id)->execute();
        }
        
        return TRUE;
    }
    
    /*
     * сохранение структуры(дивизионов)
     */
    public function saveDivizions($arr) {
        foreach ($arr as $fk_organization => $divizion) {
            foreach ($divizion as $sort => $id) {
                $q = Yii::$app->db->createCommand()->update(Divizion::tableName(), ['sort' => $sort, 'fk_organization' => $fk_organization], '`pk_divizion` = '.$id)->execute();
            }    
        }
        
        return TRUE;
    }
    
    /*
     * сохранение структуры(отделов)
     */
    public function saveDepartments($arr) {
        foreach ($arr as $fk_divizion => $department) {
            foreach ($department as $sort => $id) {
                $q = Yii::$app->db->createCommand()->update(Department::tableName(), ['sort' => $sort, 'fk_divizion' => $fk_divizion], '`pk_department` = '.$id)->execute();
            }    
        }
        
        return TRUE;
    }
    
    /*
     * вывод инфы о всей структуре поочередно
     */
    public function miniInfo($model) {
        $returnText = '';
        foreach ($model->organizations as $organization) {
            $returnText .= $organization->miniInfo();
        }
        foreach ($model->divizions as $divizion) {
            $returnText .= $divizion->miniInfo();
        }
        foreach ($model->departments as $department) {
            $returnText .= $department->miniInfo();
        }
        foreach ($model->users as $user) {
            $returnText .= $user->miniInfo();
        }
        
        return $returnText;
    }
    
    /*
     * вывод только ид сотрудников из всей структуры
     * $model - модель
     * $onlyids(true) - только массив ид=>ид, иначе полный модель user
     * $withAuthor(true) - добавление в список автора(он не выводится по умолчанию, только выбранные сотрудники)
     */
    public function onlyIdUsers($model, $onlyids = false, $withAuthor = false) {
        //инициализация
        $organizations = $divizions = $departments = $users = $allusers = [];

        //найти ид организаций
        foreach ($model->organizations as $org) {
            $organizations[$org->pk_organization] = $org->pk_organization;
        }

        //найти ид дивизионов
        foreach ($model->divizions as $div) {
            $divizions[$div->pk_divizion] = $div->pk_divizion;
        }

        //найти ид отделов
        foreach ($model->departments as $dep) {
            $departments[$dep->pk_department] = $dep->pk_department;
        }

        //найти ид сотрудников
        foreach ($model->users as $user) {
            $users[$user->id_user] = $user->id_user;
        }
        
        $query = User::findActive();
        
        if($onlyids){//только ид сотрудников
            $query->select('id_user');
        }
        
        $query->andFilterWhere(['or', ['in', 'fk_organization', $organizations], ['in', 'fk_divizion', $divizions], ['in', 'fk_department', $departments], ['in', 'id_user', $users]]);
        $allusersfind = $query->all();

        foreach ($allusersfind as $user) {
            $allusers[$user->id_user] = ($onlyids) ? $user->id_user : $user;
        }
        
        if($withAuthor && isset($model->idAuthor)){
            $author = $model->idAuthor;//автора тоже надо включать?
            $allusers[$author["id_user"]] = ($onlyids) ? $author["id_user"] : $author;
        }
        
        if(isset($model->idHead)){//ответственный тоже обязан быть в списке, даже если его не выбрали
            $head = $model->idHead;
            $allusers[$head["id_user"]] = ($onlyids) ? $head["id_user"] : $head;
        }
        
        return $allusers;
    }
    
    //---сбор и вывод структуры NESTABLE
    
    /*
     * вывод инфы в nestable виде ОБЩИЙ вызов
     * СЛАБОЕ МЕСТО, надо получше думать
     */
    public function structInfo($model, $changeBtn = false) {
        $wrap = '';
        $wrap .= self::structOrganizationFull($model, $changeBtn);//организации
        $wrap .= self::structDivizionFull($model, $changeBtn);//дивизионы
        $wrap .= self::structDepartmentFull($model, $changeBtn);//дивизионы
        $wrap .= self::structUserFull($model, $changeBtn);//дивизионы
        
        Yii::$app->view->registerCssFile('@web/css/build/jquery.nestable.css');
        Yii::$app->view->registerJsFile('@web/js/build/jquery.nestable.js',['depends' => 'app\assets\AppAsset', 'position' => yii\web\View::POS_END]);
        
        Yii::$app->view->registerJs("$('#selectedStructuresListForm').nestable({});$('#selectedStructuresListForm').nestable('collapseAll');" , \yii\web\View::POS_READY, 'nestableformproject');
        return $wrap;
    }
    
    /*
     * только организации. со всей вложенностью
     * Nestable
     */
    public function structOrganizationFull($model, $changeBtn) {
        $wrap = '';
        
        foreach ($model->organizations as $organization) {//если организация тут - то она полная
            $wrap .= '<li class="dd-item dd3-item dd-organization" data-id="'.$organization->pk_organization.'" data-struct="organization">';
            if($changeBtn) $wrap .= '<a href="'.\yii\helpers\Url::toRoute(['update','unit' => 'organization','id' => $organization->pk_organization]).'" class="pull-right"><i class="fa fa-pencil"></i></a>';
            if($changeBtn) $wrap .= '<div class="dd-handle dd3-handle"></div>';
            $wrap .= $organization->miniInfo(true);
                $wrap .= self::structDivizionFull($organization, $changeBtn);
        }
        
        return $wrap;
    }
    
    /*
     * только дивизионы. со всей вложенностью
     */
    public function structDivizionFull($model, $changeBtn) {
        $wrap = '';
        
        if(!empty($model->divizions)){//есть привязанные дивизионы
            $wrap .= '<ol class="dd-list">';

            foreach ($model->divizions as $divizion) {//если организация тут - то она полная
                $wrap .= '<li class="dd-item dd3-item dd-divizion" data-id="'.$divizion->pk_divizion.'" data-struct="divizion">';
                if($changeBtn) $wrap .= '<a href="'.\yii\helpers\Url::toRoute(['update','unit' => 'divizion','id' => $divizion->pk_divizion]).'" class="pull-right"><i class="fa fa-pencil"></i></a>';
                if($changeBtn) $wrap .= '<div class="dd-handle dd3-handle"></div>';
                $wrap .= $divizion->miniInfo(true);
                    $wrap .= self::structDepartmentFull($divizion, $changeBtn);
            }
            $wrap .= '</ol>';
        }
        
        return $wrap;
    }
    
    /*
     * только отделы. со всей вложенностью
     */
    public function structDepartmentFull($model, $changeBtn) {
        $wrap = '';
        
        if(!empty($model->departments)){//есть привязанные отделы
            $wrap .= '<ol class="dd-list">';
            foreach ($model->departments as $department) {
                $wrap .= '<li class="dd-item dd3-item dd-department" data-id="'.$department->pk_department.'" data-struct="department">';
                if($changeBtn) $wrap .= '<a href="'.\yii\helpers\Url::toRoute(['update','unit' => 'department','id' => $department->pk_department]).'" class="pull-right"><i class="fa fa-pencil"></i></a>';
                if($changeBtn) $wrap .= '<div class="dd-handle dd3-handle"></div>';
                $wrap .= $department->miniInfo(true);
                    $wrap .= self::structUserFull($department, $changeBtn);
            }
            $wrap .= '</ol>';
        }
        
        return $wrap;
    }
    
    /*
     * только сотрудники
     */
    public function structUserFull($model, $changeBtn) {
        $wrap = '';
        
        if(!empty($model->users)){//вывод сотрудников
            $wrap .= '<ol class="dd-list">';
            foreach ($model->users as $user) {
                if($user->fkStatus === null || $user->fkStatus->status == UserStatus::FIRED) continue;//отсеить уволенных
                $wrap .= '<li class="dd-item dd3-item dd-user" data-id="'.$user->id_user.'" data-struct="user">';
                if($changeBtn) $wrap .= '<div class="dd-handle dd3-handle"></div>';
                if($changeBtn) $wrap .= '<a href="#" class="pull-right ajax-link-user"><i class="fa fa-pencil"></i></a>';
                $wrap .= $user->miniInfo(true);
            }
            $wrap .= '</ol>';
        }
        
        return $wrap;
    }
    
    //---конец сбор и вывод структуры NESTABLE
    
    //---сбор данных в json скрытое поле
    public function structHiddenFieldData($model) {
        //в свои переменные
        $organizations = $model->organizations;
        $divizions = $model->divizions;
        $departments = $model->departments;
        $users = $model->users;
        
        $arr = ['organization' => [], 'divizion' => [], 'department' => [], 'user' => []];
        
        //организации
        foreach ($organizations as $organization) {
            foreach ($organization->divizions as $divs) {
                $divizions[] = $divs->pk_divizion;//для поиска ниже
            }
            $arr['organization'][] = $organization->pk_organization;
        }
        $arr['organization'] = implode(',', $arr['organization']);
        
        //дивизионы
        foreach ($divizions as $divizion) {            
            if(!isset($divizion->pk_divizion)){//если есть - брать, если нет - искать
                $divizion = Divizion::findOne($divizion);
            }
            foreach ($divizion->departments as $deps) {
                $departments[] = $deps->pk_department;
            }
            $arr['divizion'][] = $divizion->pk_divizion;            
        }
        $arr['divizion'] = implode(',', $arr['divizion']);
        
        //отделы
        foreach ($departments as $department) {
            if(!isset($department->pk_department)){
                $department = Department::findOne($department);
            }
            foreach ($department->users as $usrs) {
                if(isset($usrs->fkStatus)) {//может не быть
                    if($usrs->fkStatus->status == UserStatus::FIRED) continue;//уволенные
                    $users[] = $usrs->id_user;
                }
            }
            $arr['department'][] = $department->pk_department;
        }
        $arr['department'] = implode(',', $arr['department']);
        
        //сотрудники
        foreach ($users as $user) {            
            if(!isset($user->id_user)){
                $user = User::findOne($user);
            }
            if($user->fkStatus->status == UserStatus::FIRED) continue;//уволенные
            $arr['user'][] = $user->id_user;
        }
        $arr['user'] = implode(',', $arr['user']);
                
        return json_encode($arr);
    }    
    //---конец сбор данных в json скрытое поле
    
    
    /*
     * получить модель по имени и ид
     * @return object $model
     */
    public function getModelByNameid($name, $id) {
        $model = '';
        
        switch ($name) {
            case \app\modules\crm\models\CrmTasks::getName() :
                $model = \app\modules\crm\models\CrmTasks::findOne($id);
                break;
            
            case \app\modules\intranet\models\Tasks::getName() :
                $model = \app\modules\intranet\models\Tasks::findOne($id);
                break;
            
            case \app\modules\intranet\models\Projects::getName() :
                $model = \app\modules\intranet\models\Projects::findOne($id);
                break;
            
            case \app\modules\intranet\models\Callboard::getName() :
                $model = \app\modules\intranet\models\Callboard::findOne($id);
                break;
            
            case UserRegister::getName() :
                $model = UserRegister::findOne($id);
                break;
            
            case User::getName() :
                $model = User::findOne($id);
                break;
            
            case \app\modules\intranet\models\Events::getName() :
                $model = \app\modules\intranet\models\Events::findOne($id);
                break;
            
            case \app\modules\intranet\models\Bugtracker::getName() :
                $model = \app\modules\intranet\models\Bugtracker::findOne($id);
                break;
            
            case \app\modules\intranet\models\Blogs::getName() :
                $model = \app\modules\intranet\models\Blogs::findOne($id);
                break;
            
            case \app\models\ApiSendmailSending::getName() :
                $model = \app\models\ApiSendmailSending::findOne($id);
                break;
            
            default :
                return false;
        }
        
        return $model;
    }
}