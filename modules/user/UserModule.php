<?php

namespace app\modules\user;
use Yii;

class UserModule extends \yii\base\Module {
    public $controllerNamespace = 'app\modules\user\controllers';

    public function init() {
        parent::init();
        Yii::setAlias('@modelsuser','app\modules\user\models');//alias
        Yii::$app->view->registerCssFile('@web/css/build/jquery.nestable.css');
        $this->registerTranslations();
    }
    
    public function registerTranslations() {
        Yii::$app->i18n->translations['modules/user/*'] = [
            'class'          => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath'       => '@app/modules/user/messages',
            'fileMap'        => [
                'modules/user/all' => 'all.php',
                'modules/user/rights' => 'rights.php',
            ],
        ];
    }
    
    public static function t($category, $message, $params = [], $language = null) {
        return Yii::t('modules/user/' . $category, $message, $params, $language);
    }
}
