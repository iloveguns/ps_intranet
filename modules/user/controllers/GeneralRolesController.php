<?php

namespace app\modules\user\controllers;

use Yii;
use app\modules\user\models\GeneralRoles;
use app\modules\user\models\GeneralRolesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\user\UserModule;
use app\modules\user\models\GeneralRights;

class GeneralRolesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new GeneralRolesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new GeneralRoles();
        $modelRights = new GeneralRights();

        if($modelRights->load(Yii::$app->request->post())){
            $model->json_data = $modelRights->deleteEmptysToJson();//вот так сохраняются права в роли
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', Yii::t('app/views', 'Role created'));
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelRights' => $modelRights,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelRights = new GeneralRights();
        
        //падает
        foreach (json_decode($model->attributes['json_data'],true) as $key => $value) {//ну просто вставить значения в свои места
            $modelRights->$key = $value;
        }

        if($modelRights->load(Yii::$app->request->post())){
            $model->json_data = $modelRights->deleteEmptysToJson();//вот так сохраняются права в роли
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', Yii::t('app/views', 'Role updated'));
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelRights' => $modelRights,
            ]);
        }
    }

    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
        } catch (\yii\db\IntegrityException $e) {
            Yii::$app->session->addFlash('danger', Yii::t('app', 'Model doesn\'t delete').'<br>'.$e->getMessage());
        }

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = GeneralRoles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
