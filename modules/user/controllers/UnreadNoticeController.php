<?php

namespace app\modules\user\controllers;

use Yii;
use app\models\UnreadNotice;
use yii\filters\VerbFilter;

class UnreadNoticeController extends \app\components\Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {        
        $pages = new \yii\data\Pagination(['totalCount' => UnreadNotice::ApiNoticesUserCount(Yii::$app->user->id), 'pageSize' => Yii::$app->params['default_Pagination_PageSize']]);
        
        $data = UnreadNotice::ApiNoticesUser(Yii::$app->user->id, false, $pages->pageSize, $pages->offset);
        
        $dataProvider = new \yii\data\ArrayDataProvider([
            'models' => $data['notices'],
            'pagination' => $pages,
            'totalCount' => $pages->totalCount
        ]);

        return $this->render('index', [
            'data' => $data,
            'pages' => $pages,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionDelete()
    {
        $this->jsonResponse();
        
        $dellistids = Yii::$app->request->post('dellistids');
        
        if(UnreadNotice::deleteAll(['in', 'id_notice', $dellistids])){            
            return [
                'success' => true,
            ];
        }
        return [
            'success' => false,
        ];
    }
}