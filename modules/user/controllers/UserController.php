<?php

namespace app\modules\user\controllers;

use Yii;
use app\modules\user\models\User;
use app\modules\user\models\UserRegister;
use app\modules\user\models\Organization;
use app\modules\user\models\Divizion;
use app\modules\user\models\UserSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\user\models\UserStatus;

class UserController extends \app\components\Controller {
    public function actions() {
        return [
            'uploadPhoto' => [//фотки сотрудников
                'class' => 'budyaga\cropper\actions\UploadAction',
                'url' => '/uploads/users',
                'path' => '@webroot/uploads/users/',
                'width' => 800,
                'height' => 800,
            ]
        ];
    }
    
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'dependency-divizion' => ['post'],
                    'dependency-department' => ['post'],
                    'view-ajax' => ['post'],
                    'approve-register' => ['post'],
                    'deny-register' => ['post'],
                ],
            ],
        ];
    }

    /**
     * список сотрудников
     */
    public function actionIndex() {        
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /*
     * публичный профиль сотрудника
     */
    public function actionProfile($id) {
        $model = $this->findModel($id);
        
        if (Yii::$app->request->isAjax) {//всплывающие таблички с краткой инфой
            return $this->renderAjax('ajaxProfile', [
                'model' => $model,
            ]);
        } else {
            return $this->render('profile', [
                'model' => $model,
            ]);
        }
        
        
    }
    
    /*
     * редактировать публичный профиль сотрудника
     */
    public function actionUpdateProfile($id) {
        if(!Yii::$app->user->identity->can('tousers') && Yii::$app->user->id != $id) throw new \yii\web\ForbiddenHttpException(Yii::t('app','403 exception'));
        $model = $this->findModel($id);
        $modelstatus = UserStatus::findOne($model->fk_status);
        return $this->render('update', [
            'model' => $model,
            'ajax' => 'true',
            'modelstatus' => $modelstatus,
        ]);
    }
    
    /*
     * ajax вывод 
     */
    public function actionViewAjax() {
        $post = Yii::$app->request->post();
        $model = $this->findModel($post['id']);
        $modelStatus = UserStatus::findOne($model->fk_status);
        
        return $this->renderAjax('update', [
            'model' => $model,
            'ajax' => 'true',
            'modelstatus' => $modelStatus,
        ]);
    }

    public function actionCreate()    {
        $model = new User();
        $modelstatus = new UserStatus();        
        
        //сохранение статуса
        $modelstatus->load(Yii::$app->request->post());
        $modelstatus->save();
        //
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->link('fkStatus', $modelstatus);//связь статус
            
            //--сохранение детей
            $modelChildren = new \app\modules\user\models\UserChildren();
            $modelChildren->load(Yii::$app->request->post());
            $modelChildren->saveRelationUser($model->id_user);
            //--end сохранение детей
            
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelstatus' => $modelstatus,
            ]);
        }
    }

    public function actionUpdate($id) {
        if(!Yii::$app->user->identity->can('tousers') && Yii::$app->user->id != $id) throw new \yii\web\ForbiddenHttpException(Yii::t('app','403 exception'));
        
        $model = $this->findModel($id);
        $modelstatus = UserStatus::findOne($model->fk_status);
                
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            //сохранение статуса
            $modelstatus->load(Yii::$app->request->post());
            $model->link('fkStatus', $modelstatus);            
            $modelstatus->save();
            //
            
            //--сохранение детей
            $modelChildren = new \app\modules\user\models\UserChildren();
            $modelChildren->load(Yii::$app->request->post());
            $modelChildren->saveRelationUser($model->id_user);
            //--end сохранение детей
            
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $res = [
                    'success' => true,
                ];

                return $res;
            } else{
                $this->flash('success', \app\modules\user\UserModule::t('all', 'User data saved'));
                return $this->back();
            }
        } else {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $res = [
                    'success' => false,
                    'data' => $model->getErrors(),
                ];

                return $res;
            }
            else{
                return $this->render('update', [
                    'model' => $model,
                    'modelstatus' => $modelstatus,
                ]);
            }
        }
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id) {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /*
     * список людей и их онлайн последний
     * перевод на mysql
     */
    public function actionLastOnline() {        
        $model = \app\models\UsersOnline::find()->with('pkUser')->orderBy('now_online DESC, last_online DESC')->all();
        
        return $this->render('lastonline', ['model' => $model]);
    }
    
    public function actionShowViewersEntity($id) {
        $m = \app\models\EntityView::findOne($id);
        $model = \app\models\EntityView::find()->where(['class' => $m->class, 'item_id' => $m->item_id])->orderBy('id_entity_view DESC')->all();
        
        return $this->render('viewers_entity', ['model' => $model, 'count' => count($model)]);
    }
    
    /*
     * выдает дивизионы конкретной организации
     */
    public function actionDependencyDivizion() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $org_id = $parents[0];//ид организации
                $org = Organization::getDivizionsById($org_id);
                echo \yii\helpers\Json::encode(['output'=>$org, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
    
    /*
     * выдает отделы конкретной организации
     */
    public function actionDependencyDepartment() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $div_id = $parents[0];//ид организации
                $div = Divizion::getDepartmentsById($div_id);
                echo \yii\helpers\Json::encode(['output'=>$div, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
    
    /*
     * просмотр зарегавшихся сотрудников
     * просто fk_status = null, значит зарегавшийся
     */
    public function actionRegistersWorkers() {
        $model = User::findAll(['fk_status' => NULL]);
                
        return $this->render('registerWorkers',[
            'model' => $model,
        ]);
    }
    
    /*
     * подтвердить регистрацию сотрудника
     * $id - ид сотрудника
     */
    public function actionApproveRegister($id) {
        $approve = false;        
        $model = UserRegister::findOne($id);
        $modelstatus = new UserStatus();
        $modelstatus->status = UserStatus::WORKING;
        $modelstatus->from = \app\models\FunctionModel::getDateTimestamp();
        $modelstatus->save();
        $model->fk_role = \app\modules\user\models\GeneralRoles::ROLE_AFTER_APPROVE_REG;//роль после утверждения
        
        if($model->save()){
            $model->link('fkStatus', $modelstatus);//связь статус
            $approve = true;
            $model->afterRegistration();
        }
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;//ответ 
        return [
            'approve' => $approve,
            'id_user' => $id,
            'message' => Yii::t('app', 'User Registration Approved'),
        ];
    }
    
    /*
     * отменить регистрацию сотрудника
     * $id - ид сотрудника
     */
    public function actionDenyRegister($id) {
        $deny = false;
        $model = UserRegister::findOne($id);
        
        if($model->delete()){
            $deny = true;
            $model->afterRegistration(false);
        }
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;//ответ 
        return [
            'deny' => $deny,
            'id_user' => $id,
            'message' => Yii::t('app', 'User Registration Denied'),
        ];
    }
}
