<?php

namespace app\modules\user\controllers;

use Yii;
use app\modules\user\models\User;
use app\modules\user\models\Department;
use app\modules\user\models\Divizion;
use app\modules\user\models\Organization;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\user\UserModule;
use app\modules\user\models\Units;
use app\modules\intranet\models\Technique;

class UnitsController extends Controller {
    public function actions() {
        return [
            'uploadPhoto' => [//фотки организаций, отделов и дивизионов
                'class' => 'budyaga\cropper\actions\UploadAction',
                'url' => '/uploads/structures',
                'path' => '@webroot/uploads/structures/',
                'width' => 800,
                'height' => 800,
            ]
        ];
    }
    
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'saveunits' => ['post'],
                    'structure-widget' => ['post'],
                    'getoffice' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex() { 
        return $this->render('index');
    }
    
    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * создание структур в одном месте
     */
    public function actionCreate() {        
        $modelOrganization = new Organization();//Организация
        $modelDivizion = new Divizion();//дивизион
        $modelDepartment = new Department();//отдел        
        
        if( ($modelOrganization->load(Yii::$app->request->post()) && $modelOrganization->save()) || ($modelDivizion->load(Yii::$app->request->post()) && $modelDivizion->save()) || ($modelDepartment->load(Yii::$app->request->post()) && $modelDepartment->save()) ){
            
            //узнать кто же сохранен
            if($modelOrganization->name){
                $name = Organization::tableName();
            } else if($modelDivizion->name){
                $name = Divizion::tableName();
            } else{
                $name = Department::tableName();
            }
            
            Yii::$app->cache->flush();
            Yii::$app->session->addFlash('success', UserModule::t('all', $name.' saved'));
            return $this->redirect(['index']);
        }
        else{
            return $this->render('create', [
                'modelOrganization' => $modelOrganization,
                'modelDivizion' => $modelDivizion,
                'modelDepartment' => $modelDepartment,
            ]);
        }        
    }
    
    /*
     * Редактирование организаций
     * $unit - название того, что редактируется
     * $id - ид
     */
    public function actionUpdate($id, $unit) {
        switch ($unit){
            case 'organization' :
                $model = Organization::findOne($id);
                break;
            case 'divizion' :
                $model = Divizion::findOne($id);
                break;
            case 'department' :
                $model = Department::findOne($id);
                break;
            default :
                throw new NotFoundHttpException();
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render($unit, [
                'model' => $model,
            ]);
        }        
    }
    
    /*
     * вывод структуры для выбора сотрудников и прочего
     */
    public function actionStructureWidget() {
        return $this->renderAjax('structure-widget', [
            
        ]);
    }
    
    /*
     * вывод людей в отделах
     * html, быстрый говнокод
     */
    public function actionGetoffice() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $office_num = (int)Yii::$app->request->post()['office_num'];
        if(!$office_num) return ['success' => false];
        
        //сотрудники
        $users = User::findActive()->andWhere(['office_num' => $office_num])->all();
        
        $a = '<div class="col-md-8"><h4>Сотрудники</h4><ul class="products-list product-list-in-box">';
        foreach ($users as $user) {
            $a .= '<li class="item">
                <div class="product-img">'.$user->getLinkAvatar(true).'</div>
                <div class="product-info">
                    '.$user->getLinkToProfile(false, false, false).'
                    <span class="product-description">'.$user->fkDepartment->name.' - '.$user->fkAppointment->name.'</span>
                </div>
            </li>';
        }
        $a .= '</ul></div><div class="col-md-4"><h4>Техника</h4><ul class="products-list product-list-in-box">';
        
        //техника
        $techniques = Technique::findAll(['office_num' => $office_num]);
        foreach ($techniques as $technique) {
            $a .= '<li class="item"><div>'.$technique->name_technique.' ('.Technique::getTypes($technique->type_technique).')</div></li>';
        }
        
        $a .= '</ul></div>';
        
        return ['success' => true, 'html' => $a];
    }
    
    /*
     * сохранение порядка структур
     */
    public function actionSaveunits(){
        if(Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $arrStructure = \yii\helpers\BaseJson::decode($data['data']);
            
            $sortArr = Units::defineSort($arrStructure);//сортировка
            
            $save = Units::saveSortAll($sortArr);
            if($save){
                Yii::$app->cache->flush();
                Yii::$app->session->addFlash('success', UserModule::t('all','Units saved'));
                return $this->redirect(['index']);
            }
        }
    }
    
    /*
     * удалить структуру
     * $id - ид удаляемого
     * $unit - название удаляемого
     */
    public function actionDeleteUnit($id, $unit) {
        switch ($unit) {
            case 'organization':
                $model = Organization::findOne($id);
                break;

            case 'divizion':
                $model = Divizion::findOne($id);
                break;
                
            case 'department':
                $model = Department::findOne($id);
                break;
        }
        
        try {
            $model->delete();
            Yii::$app->cache->flush();
            Yii::$app->session->addFlash('success', UserModule::t('all', $model->tableName().' deleted'));
        } catch (\yii\db\IntegrityException $e) {
            Yii::$app->session->addFlash('danger', Yii::t('app', 'Model doesn\'t delete').'<br>'.$e->getMessage());
        }
        
        return $this->redirect(['index']);
    }
    
    /*
     * карта-план поэтажная здания смг
     */
    public function actionPlan() {
        return $this->render('plan');
    }
    
    /*
     * интерактивная карта адресов
     */
    public function actionMap() {
        $model = User::findActive()->andWhere(['not', ['lat' => NULL]])->andWhere(['not', ['lng' => NULL]])->all();
        
        $arrUsers = $points = [];
        foreach ($model as $user) {
            $arrUsers[] = '"'.$user->fullName.'"';
            $points[] = '['.implode(',', [$user->lat, $user->lng]).']';
        }
        
        $points = implode(',', $points);//в js вид надо привести
        
        $arrUsers = '['.implode(',', $arrUsers).']';
        
        return $this->render('imap', ['points' => $points, 'arrUsers' => $arrUsers]);
    }
}
