<?php
//выводит список структур
use app\modules\user\models\Units;
use yii\helpers\Html;
?>
<div class="row">
    <div class="col-md-8">
        <?= Units::hierarchy(FALSE, true)//список собственно ?>
    </div>
    
    <div class="col-md-4" id="selectedStructuresList">
        <h4>Выбранные:</h4>
        <div class="form-group">
            <button type="button" class="btn btn-default" id="struct-delete-all">Убрать всех</button>
            <?= Html::submitButton(Yii::t('app', 'Choose selected'), ['class' => 'btn btn-success', 'id' => 'getSelectedUnits'])?>
        </div>
        <?= Html::hiddenInput('nestable-selected-structure')?>
    </div>
</div>