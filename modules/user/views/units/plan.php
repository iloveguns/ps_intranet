<?php 
use yii\helpers\Url;

$this->title = 'Поэтажный план здания';

$this->registerJsFile('@web/js/build/highlight.min.js',['position' => yii\web\View::POS_END, 'depends' => 'yii\web\YiiAsset']);

$dataMaphighlight = '{"fillColor":"00ff00","shadow":true,"shadowBackground":"ffffff"}';//http://maschek.hu/imagemap/imgmap/

$this->registerJs("
$('.navs-floor a').click(function(){
    let fid = parseInt($(this).attr('data-id'));
    setTimeout(function(){ $('#ImageMap' + fid).maphilight() }, 100);    
});

$('#ImageMap0').maphilight();

$('area').click(function(e) {
    e.preventDefault();
    let idoff = $(this).attr('data-office-num');
    let name_office = $(this).attr('title');
    
    $.ajax({
        url: '".Url::toRoute('getoffice')."',
        type: 'POST',
        data: {
            office_num:  idoff,
        },
        success: function (data) {
            if(data.success === true && data.html){
                $('#myModal .modal-body').html('<div class=\"row\">' + data.html + '</div>');
                $('#myModal .modal-header h3').html(name_office);
                $('#myModal').modal('show');
            }
        }                
    });
});
", yii\web\View::POS_READY);
?>
<div>
    <ul class="nav nav-pills navs-floor">
        <li class="active">
            <a href="#0floor" data-id="0" data-toggle="tab">Цоколь</a>
        </li>
        <li>
            <a href="#1floor" data-id="1" data-toggle="tab">1 этаж</a>
        </li>
        <li>
            <a href="#2floor" data-id="2" data-toggle="tab">2 этаж</a>
        </li>
        <li>
            <a href="#3floor" data-id="3" data-toggle="tab">3 этаж</a>
        </li>
    </ul>
    <div class="tab-content clearfix">
        <div class="tab-pane active" id="0floor">
            <img id="ImageMap0" src="/images/units/0floor.png" usemap="#mapfloor0">
            
            <map id="mapfloor0" name="mapfloor0">
                <area data-office-num="6" shape="rect" alt="" title="Распространение" coords="622,393,837,479" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="4" shape="rect" alt="" title="Аппаратная" coords="787,527,855,645" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="99" shape="rect" alt="" title="Водительская" coords="423,105,517,143" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
            </map>
        </div>
        <div class="tab-pane" id="1floor">
            <img id="ImageMap1" src="/images/units/1floor.png" usemap="#mapfloor1">

            <map id="mapfloor1" name="mapfloor1">
                <area data-office-num="122" shape="poly" alt="" title="Руководитель СМГ" coords="231,99,231,126,245,126,245,143,322,142,322,23,251,22,250,99,237,99" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="121" shape="poly" alt="" title="Отдел по работе с административными структурами" coords="158,98,227,98,227,147,241,148,240,182,158,182" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="123" shape="rect" alt="" title="Помощник руководителя СМГ" coords="339,98,407,148" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="100" shape="rect" alt="" title="Пакетные продажи" coords="424,216,519,366" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="120" shape="poly" alt="" title="Юристы" coords="241,204,227,204,227,223,240,223,240,282,227,281,227,302,159,301,159,184,242,183,242,189" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="119" shape="poly" alt="" title="Переговорная" coords="320,329,320,283,335,283,335,225,323,225,323,205,244,205,244,329,275,330" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="118" shape="poly" alt="" title="Медиа менеджер" coords="160,387,241,387,241,378,228,378,227,366,241,366,241,307,160,306" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="116" shape="rect" alt="" title="Радио КП" coords="158,425,320,480" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="102" shape="rect" alt="" title="Производство" coords="524,393,759,479" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="103" shape="rect" alt="" title="Редакция Политсибру" coords="763,392,841,478" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="104" shape="rect" alt="" title="Шишкина" coords="917,392,975,448" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="106" shape="rect" alt="" title="Продажи ТВ" coords="917,451,975,610" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="107" shape="rect" alt="" title="Редакция Автограф" coords="844,540,900,646" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="107" shape="rect" alt="" title="Реклама Автограф" coords="720,540,840,645" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="108" shape="rect" alt="" title="Редакция АИФ" coords="599,540,717,645" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="109" shape="rect" alt="" title="Сайт АИФ" coords="535,539,596,645" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="110" shape="rect" alt="" title="Алтайский вестник" coords="461,539,518,645" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="111" shape="rect" alt="" title="ИТ-отдел и техническая служба" coords="338,539,458,645" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="112" shape="rect" alt="" title="Отдел продаж Политсибру" coords="276,539,335,645" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="113" shape="poly" alt="" title="Гундарин" coords="273,646,273,540,233,539,216,556,216,646" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="114" shape="poly" alt="" title="Негреев" coords="159,646,213,646,213,554,195,535,160,534" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
            </map>
        </div>
        <div class="tab-pane" id="2floor">
            <img id="ImageMap2" src="/images/units/2floor.png" usemap="#mapfloor2">
            
            <map id="mapfloor2" name="mapfloor2">
                <area data-office-num="217" shape="poly" alt="" title="Комсомольская правда" coords="158,16,157,224,240,224,241,122,336,122,336,16" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="214" shape="rect" alt="" title="Питахина" coords="159,227,239,305" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="215" shape="rect" alt="" title="Спринт" coords="244,214,320,305" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="201" shape="rect" alt="" title="Отдел персонала" coords="424,133,520,194" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="202" shape="poly" alt="" title="Директор барнаульского дивизиона" coords="520,197,520,284,424,284,425,254,411,254,411,197" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="203" shape="rect" alt="" title="Служба учета" coords="558,309,840,364" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="204" shape="rect" alt="" title="Куценко" coords="917,309,973,397" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="206" shape="poly" alt="" title="PR-департамент" coords="835,416,919,417,919,403,975,403,974,527,918,527,917,435,901,436,900,563,835,564" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="207" shape="rect" alt="" title="РГ Сокол" coords="652,415,831,563" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="208" shape="rect" alt="" title="Отдел видеомонтажа" coords="536,415,648,563" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="209" shape="rect" alt="" title="Павильон" coords="234,414,519,563" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="212" shape="rect" alt="" title="Операторская" coords="159,415,230,562" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="213" shape="rect" alt="" title="Редакция ТВ" coords="17,310,141,559" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="211" shape="rect" alt="" title="Водители" coords="205,357,280,397" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="210" shape="rect" alt="" title="Гримерная" coords="284,356,352,396" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
                <area data-office-num="299" shape="poly" alt="" title="Ресепшн" coords="339,17,339,62,355,59,368,52,379,42,385,29,388,17" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
            </map>
        </div>
        <div class="tab-pane" id="3floor">
            <img id="ImageMap3" src="/images/units/3floor.png" usemap="#mapfloor3">
            
            <map id="mapfloor3" name="mapfloor3">
                <area data-office-num="220" shape="rect" alt="" title="Менеджер КЗ" coords="144,336,226,398" href="" target="" data-maphilight='<?= $dataMaphighlight ?>'/>
            </map>
        </div>
    </div>
</div>