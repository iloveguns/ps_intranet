<?php
use yii\helpers\Url;
use budyaga\cropper\Widget;
use app\modules\user\UserModule;
use app\modules\user\models\Organization;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div class="divizion-form">
    <h1><?=$model->isNewRecord ? UserModule::t('all', 'Create divizion') : UserModule::t('all', 'Update divizion')?></h1>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'fk_organization')->dropDownList(Organization::getAll()) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>      
    
    <?php if(!$model->isNewRecord) : ?>
    <div class="col-lg-4 col-md-6 col-sm-12">
        <?= $form->field($model, 'photo')->widget(Widget::className(), [
        'uploadUrl' => Url::toRoute('/user/units/uploadPhoto'),
        'cropAreaHeight' => 100,
        'cropAreaWidth' => 260,
    ]) ?>
    </div>
    <?php endif ?>
    
    <div class="clearfix"></div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php if(!$model->isNewRecord) : ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete-unit', 'id' => $model->pk_divizion, 'unit' => 'divizion'], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?php endif ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>