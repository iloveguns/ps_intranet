<?php
use app\modules\user\UserModule;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\sidenav\SideNav;
use app\modules\user\models\Units;
use app\components\WidgetFormerUsers;

$this->title = Yii::t('app/models', 'Units list');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row row-cols units-index">
    <h1><?= Html::encode($this->title) ?></h1>
    
    <div class="col-md-9">
        <?= Units::hierarchy(Yii::$app->user->identity->can('tousers'), false, (Yii::$app->request->get('show') == 'all') ? true : false)?>
    </div>
    <?php if(Yii::$app->user->identity->can('tousers')) : ?>
        <div class="col-md-3">
            <?= SideNav::widget([
                'type' => SideNav::TYPE_DEFAULT,
                'heading' => 'Options',
                'containerOptions' => [
                    'class' => 'affix'
                ],
                'items' => [
                    [
                        'url' => 'create',
                        'label' => UserModule::t('all', 'Create units'),
                        'icon' => 'home'
                    ],
                    [
                        'url' => 'saveunits',
                        'label' => UserModule::t('all', 'Save units'),
                        'icon' => 'home',
                    ],
                    [
                        'label' => 'Вывод',
                        'icon' => 'question-sign',
                        'items' => [
                            ['label' => 'С уволенными', 'url'=>  Url::to(['', 'show' => 'all']), 'active' => (Yii::$app->request->get('show') == 'all')],
                            ['label' => 'Обычный', 'url'=> Url::to(['', 'show' => 'active']), 'active' => (Yii::$app->request->get('show') == 'active')],
                        ],
                    ],
                ],
            ]) ?>
        </div>
    <?php endif ?>
</div>
<?php $this->registerJs("//скрипты для этого
    function profileUserAjax(id){//вызов формы редактирования сотрудника 
        $.ajax({
            url: '".Url::toRoute('user/view-ajax')."',
            type: 'post',
            data: {
                id:  id,
            },
            success: function (data) {
                $('.modal-body').html(data);
                $('#myModal').modal('toggle');
            }
        });
        return false;
    }
    function unitSaveAjax(){//сохранение структуры
        $.ajax({
            url: '".Url::toRoute('saveunits')."',
            type: 'post',
            data: {
                data:  JSON.stringify($('.dd').nestable('serialize')),
            },
            beforeSend : function(){
                gifLoader();
            },
            success: function (data) {
                console.log(data);
            }                
        });
    }
", \yii\web\View::POS_END);
$this->registerJs("
    var step = '';//bugfix
", \yii\web\View::POS_END);
?>
<?= WidgetFormerUsers::widget()//бывшие ?>
