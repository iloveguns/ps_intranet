<?php 
$this->title = Yii::t('app/models', 'Inter map');
?>
<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script>
ymaps.ready(function () {
    let arrUsers = <?= $arrUsers ?>;
    
    let myMap = new ymaps.Map('map', {
            center: [53.340699, 83.755328],
            zoom: 12
        }, {
            searchControlProvider: 'yandex#search'
        }),
        clusterer = new ymaps.Clusterer(),
        getPointData = function (index) {
            return {
                balloonContentBody: arrUsers[index],
                clusterCaption: 'метка <strong>' + index + '</strong>'
            };
        },
        points = [
            <?= $points ?>
        ],
        geoObjects = [];

    for (let i = 0, len = points.length; i < len; i++) {
        geoObjects[i] = new ymaps.Placemark(points[i], getPointData(i));
    }

    clusterer.add(geoObjects);
    myMap.geoObjects.add(clusterer);
});
</script>
<div class="row">
    <div class="col-md-12">
        <div id="map" style="height:500px"></div>
        <?= yii\helpers\Html::a('Добавить себя', ['/user/user/update-profile','id'=>  Yii::$app->user->id], ['class' => 'btn btn-success'])?>
    </div>
</div>