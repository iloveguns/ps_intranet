<?php
use yii\helpers\Html;
use app\modules\user\UserModule;

$this->title = UserModule::t('all', 'Create appointment');
$this->params['breadcrumbs'][] = ['label' => UserModule::t('all', 'Appointments list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appointments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>