<?php
use yii\helpers\Html;
use app\modules\user\UserModule;

$this->title = UserModule::t('all', 'Update appointment', [
    'modelClass' => 'Appointments',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => UserModule::t('all', 'Appointments list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->pk_appointment]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appointments-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>