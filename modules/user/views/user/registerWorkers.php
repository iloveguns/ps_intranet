<?php
use app\modules\user\UserModule;
use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\user\models\User;
use app\modules\user\models\Appointments;

$this->title = UserModule::t('all', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if(empty($model)) :?>
    <h1><?= UserModule::t('all', 'Empty Register Users')?></h1>
<?php else : ?>
    <h1><?= Yii::t('app/models', 'Register Workers')?></h1>
<table class="table table-hover">
    <thead>
        <tr>
            <th><?= $model[0]->getAttributeLabel('name')?></th>
            <th><?= $model[0]->getAttributeLabel('fk_appointment')?></th>
            <th><?= Yii::t('app', 'Approve Register')?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($model as $user) : ?>
            <tr id="user-<?=$user->id?>">
                <td><?= $user->fullName?></td>
                <td>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseUser<?= $user->id_user?>" aria-expanded="false" aria-controls="collapseUser<?= $user->id_user?>">
                        <?= Yii::t('app', 'Show data register user')?>
                    </button>
                </td>
                <td>
                    <?=Html::a('+', ['user/approve-register','id' => $user->id], [
                        'id' => 'approve-register',
                        'data-on-done' => 'simpleResponse',//js функция после выполнения
                        'class' => 'btn btn-success regist',
                    ])?>
                    <?=Html::a('-', ['user/deny-register','id' => $user->id], [
                        'id' => 'deny-register',
                        'data-on-done' => 'simpleResponse',//js функция после выполнения
                        'class' => 'btn btn-danger regist',
                    ])?>
                </td>
            </tr>
            <div class="collapse" id="collapseUser<?= $user->id_user?>">
                <div class="well row row-cols">
                    <h4><?= $user->fullName?></h4>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?=UserModule::t('all', 'Birthday')?> : <?=Yii::$app->formatter->asDate($user->birthday)?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?=UserModule::t('all', 'Sex')?> : <?=User::getSex($user->sex)?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?=UserModule::t('all', 'Email')?> : <?=$user->email?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?=UserModule::t('all', 'fk_organization')?> : <?= app\modules\user\models\Organization::getAll($user->fk_organization)?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?=UserModule::t('all', 'fk_divizion')?> : <?= \app\modules\user\models\Divizion::getAll($user->fk_divizion)?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?=UserModule::t('all', 'Appointment')?> : <?= Appointments::getAll($user->fk_appointment)?>
                    </div>
                </div>
            </div>
        <?php endforeach?>
    </tbody>
</table>


<?php 
$this->registerJs("$('.regist').click(handleAjaxLink);
ajaxCallbacks.simpleResponse = function (response) {
    if(response.approve == true || response.deny == true){
        $('#user-'+response.id_user).remove();
        bootstrapAlert('success', response.message);
    }
    else{
        bootstrapAlert('danger', response.message);
    }
}", \yii\web\View::POS_END);?>
<?php endif ?>