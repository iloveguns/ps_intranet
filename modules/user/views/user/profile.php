<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\user\UserModule;
use app\modules\user\models\User;
use app\modules\user\models\UserStatus;

$this->title = $model->fullName.' - '.UserModule::t('all', 'User profile');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row user-profile">
    
    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
        <?=$model->imgAvatar?>
        <div class="panel panel-default">
            <div class="panel-body">
                <?= Html::a(Yii::t('app', 'Send Message User'), ['/dialog#' . $model->id], ['class' => 'btn btn-info'])?>
            </div>
        </div>
    </div>
    
    <div class="well col-xs-12 col-sm-6 col-md-8 col-lg-8">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th><?=  UserModule::t('all','Personal card profile')?></th>
                    <th class="pull-right">
                        <?= Html::a(Yii::t('app', 'Change'), ['update-profile','id' => $model->id], ['class' => 'btn btn-danger'])?>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2"><i><?=$model->fullName?></i></td>
                </tr>
                <tr>
                    <td><?=UserModule::t('all', 'fk_organization')?>:</td>
                    <td><?=$model->fkOrganization->name?></td>
                </tr>
                <tr>
                    <td><?=UserModule::t('all', 'fk_divizion')?>:</td>
                    <td><?=$model->fkDivizion->name?></td>
                </tr>
                <tr>
                    <td><?=UserModule::t('all', 'fk_department')?>:</td>
                    <td><?=$model->fkDepartment->name?></td>
                </tr>
                <tr>
                    <td><?=UserModule::t('all', 'Registration Date')?>:</td>
                    <td><?=Yii::$app->formatter->asDatetime($model->registration_date)?></td>
                </tr>
                <tr>
                    <td><?=UserModule::t('all', 'Appointment')?>:</td>
                    <td><?=$model->fkAppointment->name?></td>
                </tr>
                <tr>
                    <td colspan="2"><i><?=  UserModule::t('all','Contacts data')?>:</i></td>
                </tr>
                <tr>
                    <td><?=UserModule::t('all', 'Phone')?>:</td>
                    <td><?=$model->phone?></td>
                </tr>
                <tr>
                    <td><?=UserModule::t('all', 'Email')?>:</td>
                    <td><a href="mailto:<?=$model->email?>"><?=$model->email?></a></td>
                </tr>
                <tr>
                    <td><?=UserModule::t('all', 'Status')?>:</td>
                    <td>
                        <?= UserStatus::getStatus($model->fkStatus->status)?>
                        <?php $ustatus = $model->fkStatus; if($ustatus->status != UserStatus::WORKING) {
                            echo Yii::t('app', 'From').' '.Yii::$app->formatter->asDate($ustatus->from);
                                    
                            if(!empty($ustatus->to)) {
                                echo Yii::t('app', 'To').' '.Yii::$app->formatter->asDate($ustatus->to);
                            }
                        }?>
                    </td>
                </tr>
                <tr>
                    <td>ICQ:</td>
                    <td><?=$model->icq?></td>
                </tr>
                <tr>
                    <td colspan="2"><i><?=  UserModule::t('all','Personal data')?>:</i></td>
                </tr>
                <tr>
                    <td><?=UserModule::t('all', 'Birthday')?>:</td>
                    <td><?=Yii::$app->formatter->asDate($model->birthday)?></td>
                </tr>
                <tr>
                    <td><?=UserModule::t('all', 'Sex')?>:</td>
                    <td><?=User::getSex($model->sex)?></td>
                </tr>
                <tr>
                    <td><?=UserModule::t('all', 'Marital')?>:</td>
                    <td><?=User::getMarital($model->marital)?></td>
                </tr>
                <tr>
                    <td><?= UserModule::t('all', 'office_num')?> :</td>
                    <td><?= $model->office_num ?></td>
                </tr>
                <?php if($model->where_from) : ?>
                    <tr>
                        <td><?= UserModule::t('all', 'User Where From') ?> :</td>
                        <td><?= \app\modules\crm\models\AddressCity::getAll($model->where_from) ?></td>
                    </tr>
                <?php endif ?>
                <tr>
                    <td><?=UserModule::t('all', 'Last Online')?>:</td>
                    <td>
                        <?php if(!isset($model->onlineData->now_online)) : ?>
                            <?=UserModule::t('all', 'Empty Data About Online')?>
                        <?php else : ?>                        
                            <?php if($model->onlineData->now_online) : ?>
                                <?=UserModule::t('all', 'Online')?>
                            <?php else : ?>
                                <?= Yii::$app->formatter->asDatetime($model->onlineData->last_online)?>
                            <?php endif ?>
                        <?php endif ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <?= \app\components\WidgetCalendar::widget(['iduser' => $model->id_user, 'header' => Yii::t('app/views', 'Profile calendar')])?>
    </div>
</div>