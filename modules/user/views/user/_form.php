<?php
use kartik\depdrop\DepDrop;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\user\models\Organization;
use app\modules\user\models\Department;
use app\modules\user\models\UserStatus;
use app\modules\user\UserModule;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\modules\user\models\User;
use app\modules\user\models\Appointments;
use budyaga\cropper\Widget;
use app\modules\user\models\GeneralRoles;
use kartik\select2\Select2;
?>
<div class="user-form">
    <?php $formarr = ['id' => 'user-change']; if(isset($ajax)) $formarr['action'] = ['user/update','id' => $model->id]?>
    <?php $form = ActiveForm::begin($formarr); ?>
    
    <div class="col-lg-4 col-md-6 col-sm-12">
        <?= $form->field($model, 'photo')->widget(Widget::className(), [
            'uploadUrl' => Url::toRoute('/user/user/uploadPhoto'),
            'cropAreaHeight' => 300,
            'cropAreaWidth' => 300,
            'width' => 800,
            'height' => 800,
            'maxSize' => 1024*10240
        ]) ?>
    </div>
    
    <?= $form->field($model, 'lastname', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'secondname', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'birthday', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->widget(DatePicker::classname(), [
        'options' => ['placeholder' => Yii::t('app','Enter date')],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);    
    ?>

    <?= $form->field($model, 'sex', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->dropDownList(User::getSex()) ?>

    <?= $form->field($model, 'marital', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->dropDownList(User::getMarital()) ?>
    
    <?= $form->field($model, 'phone', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->textInput() ?>

    <?= $form->field($model, 'email', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->textInput() ?>

    <?= $form->field($model, 'password', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->textInput() ?>

    <?= $form->field($model, 'icq', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->textInput() ?>

    <?= $form->field($model, 'skype', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->textInput() ?>
    
    <div class='col-lg-4 col-md-6 col-sm-12'>
        <?= $form->field($model, 'fk_appointment')->widget(Select2::classname(), [
            'data' => Appointments::getAll(),
            'options' => ['placeholder' => Yii::t('app', 'Select')],
            'pluginOptions' => [
                'allowClear' => false,
            ],
        ]);?>
    </div>
    
    <?php if(Yii::$app->user->identity->isAdmin()) : ?>
    
    <?= $form->field($model, 'fk_role', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->dropDownList(GeneralRoles::getAll()) ?>
    
    <?php endif ?>
    
    <?= $form->field($model, 'where_from', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->widget(Select2::classname(), [
        'data' => \app\modules\crm\models\AddressCity::getAll(),
        'options' => ['placeholder' => Yii::t('app', 'Select')],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]);?>
    
    <?= $form->field($model, 'office_num', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->textInput() ?>
    
    <?= $form->field($model, 'fk_department', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->widget(Select2::classname(), [
        'data' => Department::getAll(),
        'options' => ['placeholder' => Yii::t('app', 'Select')],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]);?>
    
    <?= $form->field($model, 'lat')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'lng')->hiddenInput()->label(false) ?>
    
    <div>
        <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script type="text/javascript">
            function initmap() {//ymaps.ready(initmap)
                let lat = <?= ($model->lat) ? $model->lat : '53.351729' ?>;
                let lng = <?= ($model->lng) ? $model->lng : '83.759316' ?>;
                
                let coords = [lat,lng];

                let myPlacemark,
                    myMap = new ymaps.Map('map', {
                        center: coords,
                        zoom: 16
                    }, {
                        searchControlProvider: 'yandex#search'
                    });

                    myPlacemark = createPlacemark(coords);
                    myMap.geoObjects.add(myPlacemark);
                    myPlacemark.events.add('dragend', function () {
                        getAddress(myPlacemark.geometry.getCoordinates());
                    });
                    getAddress(coords);

                myMap.events.add('click', function (e) {
                    let coords = e.get('coords');

                    $('#user-lat').val(coords[0].toFixed(6));
                    $('#user-lng').val(coords[1].toFixed(6));

                    myPlacemark.geometry.setCoordinates(coords);
                    getAddress(coords);
                });
                
                myPlacemark.events.add("dragend", function (e) {			
                    coords = this.geometry.getCoordinates();
                    $('#user-lat').val(coords[0].toFixed(6));
                    $('#user-lng').val(coords[1].toFixed(6));
                }, myPlacemark);

                function createPlacemark(coords) {
                    return new ymaps.Placemark(coords, {
                        iconCaption: 'поиск...'
                    }, {
                        preset: 'islands#violetDotIconWithCaption',
                        draggable: true
                    });
                }

                function getAddress(coords) {
                    myPlacemark.properties.set('iconCaption', 'поиск...');
                    ymaps.geocode(coords).then(function (res) {
                        let firstGeoObject = res.geoObjects.get(0);

                        myPlacemark.properties
                            .set({
                                iconCaption: firstGeoObject.properties.get('name'),
                                balloonContent: firstGeoObject.properties.get('text')
                            });
                    });
                }
            }
        </script>
        <div class="col-lg-8 col-md-12 col-sm-12 mb-1em">
            <label class="control-label" for="user-fk_role"><?= Yii::t('app/models', 'Coordinates lat lng')?></label><br>
            <button class="btn btn-primary" onclick="$('#map').css({'height':'300px', 'width':'100%'});initmap();$(this).remove();return false;">Показать карту</button>
            <div id="map"></div>
        </div>
    </div>
    
    
    <div class="clearfix"></div>
    <?= app\components\WidgetMultipleValuesn::widget([
        'title' => 'Дети',
        'relation' => $model->children,
        'attrs' => [
            'name_child' => ['type' => 'text'],
            'age_child' => ['type' => 'number'],
            'sex_child' => ['type' => 'dropdown', 'items' => $model->getSex()],
        ],
        'model' => new app\modules\user\models\UserChildren(),
    ]) ?>
    
    <div class="clearfix"></div>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $form->field($modelstatus, 'status', ['options' => ['class' => 'col-md-12']])->dropDownList(UserStatus::getStatus()) ?>

            <?= $form->field($modelstatus, 'from', ['options' => ['class' => 'col-md-6']])->widget(DatePicker::classname(), [
                'options' => ['placeholder' => UserModule::t('all','Enter date')],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);    
            ?>

            <?= $form->field($modelstatus, 'to',['options' => ['class' => 'col-md-6']])->widget(DatePicker::classname(), [
                'options' => ['placeholder' => UserModule::t('all','Enter date')],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);    
            ?>
        </div>
    </div>
    
<?php if(!$model->fk_department) : //то есть только при создании показывать ?>    
<?= $form->field($model, 'fk_organization')->dropDownList(Organization::getAll(),['id' => 'fk_organization']) ?>     
<?= $form->field($model, 'fk_divizion')->widget(DepDrop::classname(), [
    'options'=>['id'=>'fk_divizion'],
    'pluginOptions'=>[
        'depends'=>['fk_organization'],
        'placeholder'=>Yii::t('app', 'Select'),
        'url'=>Url::to(['dependency-divizion'])
    ]
]);
?>    
<?= $form->field($model, 'fk_department')->widget(DepDrop::classname(), [
    'options'=>['id'=>'fk_department'],
    'pluginOptions'=>[
        'depends'=>['fk_divizion'],
        'placeholder'=>Yii::t('app', 'Select'),
        'url'=>Url::to(['dependency-department'])
    ]
]);
?>
<?php 
$this->registerJs(<<<JS
$('#user-change').on('beforeSubmit', function(){
    if($("#fk_department").val() === null){
        $(".field-user-fk_department").addClass('has-error');
        return false;
    }
    else{
        $(".field-user-fk_department").removeClass('has-error');
        return true;
    }
});
JS
);?>
<?php endif ?>
    
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $form->field($model, 'setting_notify_email', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->checkbox() ?>
        </div>
    </div>
    
    <div class="form-group">
        <?php 
        if(isset($ajax) && $ajax == true){
            echo Html::a(Yii::t('app', 'Save'), ['user/update','id' => $model->id], [
                'id' => 'update-user',
                'data-on-done' => 'userResponse',//js функция после выполнения
                'data-form-id' => 'user-change',//ид формы, к которой привязано submit
                'class' => 'btn btn-primary',
            ]
            );
            $this->registerJs("$('#update-user').click(handleAjaxLink);
            ajaxCallbacks.userResponse = function (response) {
                if(response.success){
                    bootstrapAlert('success','".UserModule::t('all', 'User data saved')."');
                    $('.modal').modal('hide');
                }
                else{
                    bootstrapAlert('danger','".UserModule::t('all', 'User data not saved')."');
                    $('.modal').modal('hide');
                }
            }", \yii\web\View::POS_END);
        }
        else{
            echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
        }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
    <?php $this->registerJs('$("#user-phone").mask("+79999999999");', yii\web\View::POS_READY); ?>
</div>