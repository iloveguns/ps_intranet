<?php
$this->title = Yii::t('app', 'Users Online');
$i = 0;
?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $this->title?></h3>
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <th>#</th>
                    <th><?= Yii::t('app', 'User')?></th>
                    <th><?= Yii::t('app/views', 'Was online')?></th>
                </tr>
                <?php foreach ($model as $user) : ?>
                    <tr>
                        <td><?= ++$i?></td>
                        <td><div style="width: 100px"><?= $user->pkUser->getLinkAvatar(true)?></div></td>
                        <td>
                            <?php if($user->now_online) : ?>
                                <span><i class="fa fa-circle text-success"></i><span>Online</span></span>
                            <?php else : ?>
                                <?= Yii::$app->formatter->asDatetime($user->last_online)?>
                            <?php endif ?>
                        </td>
                    </tr>
                <?php endforeach; ?>                
            </tbody>
        </table>
    </div>
</div>