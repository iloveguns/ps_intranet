<?php
$this->title = Yii::t('app', 'Viewers');
$i = 0;
?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $count ?></h3>
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <th>#</th>
                    <th><?= Yii::t('app', 'User')?></th>
                    <th>Дата/время просмотра</th>
                </tr>
                <?php foreach ($model as $user) : ?>
                    <tr>
                        <td><?= ++$i?></td>
                        <td><div style="width: 100px"><?= $user->idUser->getLinkAvatar(true, [], [100, 100])?></div></td>
                        <td>
                            <?= Yii::$app->formatter->asDatetime($user->date) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>                
            </tbody>
        </table>
    </div>
</div>