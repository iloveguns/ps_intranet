<?php
//упрощенная версия профиля сотрудника
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\user\UserModule;
use app\modules\user\models\User;
use app\modules\user\models\UserStatus;
?>
<div class="well col-lg-12">
    <table class="table">
        <tbody>
            <tr>
                <?=$model->imgAvatar?>
                <?= $model->getLinkToSendMessage(Yii::t('app', 'Send Message User'), ['class' => 'btn btn-info'])?>
            </tr>
            <tr>
                <td colspan="2"><i><?= $model->getLinkToProfile()?></i></td>
            </tr>
            <tr>
                <td><?=UserModule::t('all', 'fk_department')?>:</td>
                <td><?=$model->fkDepartment->name?></td>
            </tr>
            <tr>
                <td><?=UserModule::t('all', 'Appointment')?>:</td>
                <td><?=$model->fkAppointment->name?></td>
            </tr>
            <tr>
                <td><?=UserModule::t('all', 'Phone')?>:</td>
                <td><?=$model->phone?></td>
            </tr>
            <tr>
                <td><?=UserModule::t('all', 'Email')?>:</td>
                <td><a href="mailto:<?=$model->email?>"><?=$model->email?></a></td>
            </tr>
            <tr>
                <td>ICQ:</td>
                <td><?=$model->icq?></td>
            </tr>
            <tr>
                <td><?=UserModule::t('all', 'Birthday')?>:</td>
                <td><?=Yii::$app->formatter->asDate($model->birthday)?></td>
            </tr>
            <tr>
                <td><?=UserModule::t('all', 'Status')?>:</td>
                <td><?= UserStatus::getStatus($model->fkStatus->status)?> <?php if($model->fkStatus->status != UserStatus::WORKING) echo Yii::t('app', 'From').' '.Yii::$app->formatter->asDate($model->fkStatus->from).Yii::t('app', 'To').' '.Yii::$app->formatter->asDate($model->fkStatus->to)?></td>
            </tr>
            <?php if($model->where_from) : ?>
                <tr>
                    <td><?= UserModule::t('all', 'User Where From') ?> :</td>
                    <td><?= \app\modules\crm\models\AddressCity::getAll($model->where_from) ?></td>
                </tr>
            <?php endif ?>
            <tr>
                <td><?=UserModule::t('all', 'Last Online')?>:</td>
                <td>
                    <?php 
                    $onlineData = $model->onlineData;
                    if(!isset($onlineData->now_online)) : ?>
                        <?=UserModule::t('all', 'Empty Data About Online')?>
                    <?php else : ?>                        
                        <?php if($onlineData->now_online) : ?>
                            <?=UserModule::t('all', 'Online')?>
                        <?php else : ?>
                            <?= Yii::$app->formatter->asDatetime($onlineData->last_online)?>
                        <?php endif ?>
                    <?php endif ?>
                </td>
            </tr>
        </tbody>
    </table>
</div>