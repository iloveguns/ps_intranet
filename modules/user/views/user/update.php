<?php
use yii\helpers\Html;
use app\modules\user\UserModule;

(isset($ajax)) ? : $ajax = false;

$this->title = UserModule::t('all', 'Update user: ', [
    'modelClass' => 'User',
]) . ' ' . $model->fullname;
$this->params['breadcrumbs'][] = ['label' => UserModule::t('all', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php if(isset($_GET['required'])) : ?>
        <div class="alert alert-danger" role="alert">Необходимо заполнить фото</div>
    <?php endif ?>

    <?= $this->render('_form', [
        'model' => $model,
        'ajax' => $ajax,
        'modelstatus' => $modelstatus,
    ]) ?>

</div>