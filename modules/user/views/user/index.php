<?php
use app\modules\user\UserModule;
use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\user\models\User;

$this->title = UserModule::t('all', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <div>
        <?php
        $model = User::getSearchFormAttributes();
        $form = \yii\widgets\ActiveForm::begin(['id' => 'search-attrs-user']);
        foreach ($model->getAttrs() as $name => $attr) {
            echo '<div class="col-lg-2 col-md-4 col-sm-6">';
            echo $form->field($model, $name)->checkbox();
            echo '</div>';
        }
        echo '<div class="clearfix"></div>';
        echo \yii\helpers\Html::submitButton(Yii::t('app/models', 'Change search attributes'), ['class' =>  'btn btn-primary']);
        \yii\widgets\ActiveForm::end();

        $attrs = $model->getAttrsToGridView($model->attributes,$searchModel);
        ?>
    </div>
    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php if(Yii::$app->user->identity->can('tousers')) : ?>
    <p>
        <?= Html::a(UserModule::t('all', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $attrs,
        'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
        'pager' => [
            'class' => \liyunfang\pager\LinkPager::className(),
            'pageSizeList' => Yii::$app->params['pageSizeList'],
            'pageSizeOptions' => ['class' => 'form-control pagesizeselect'],
        ],
    ]); ?>

</div>