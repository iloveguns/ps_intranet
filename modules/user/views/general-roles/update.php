<?php
use yii\helpers\Html;
use app\modules\user\UserModule;

$this->title = UserModule::t('all', 'Update General Roles').': '. $model->name;
$this->params['breadcrumbs'][] = ['label' => UserModule::t('all', 'General Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->pk_role]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="general-roles-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelRights' => $modelRights,
    ]) ?>

</div>