<?php
use app\modules\user\UserModule;
use yii\helpers\Html;

$this->title = UserModule::t('all', 'Create General Roles');
$this->params['breadcrumbs'][] = ['label' => UserModule::t('all', 'General Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="general-roles-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelRights' => $modelRights,
    ]) ?>

</div>