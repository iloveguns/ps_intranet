<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\user\UserModule;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => UserModule::t('all', 'General Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="general-roles-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->pk_role], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->pk_role], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'pk_role',
            [
                'attribute' => 'fk_organization',
                'value' => $model->fkOrganization->name,
            ],
            'name',
            [
                'attribute' => 'json_data',
                'value' => $model->json_data,
            ],
        ],
    ]) ?>

</div>