<?php
use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\user\models\Organization;
use app\modules\user\UserModule;
use app\modules\user\models\GeneralRoles;

$this->title = UserModule::t('all', 'General Roles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="general-roles-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(UserModule::t('all', 'Create General Roles'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'fk_organization',
                'value' => function ($model, $key, $index, $column) {
                    return $model->fkOrganization->name;
                },
                'filter' => Organization::getAll(),
            ],
            [
                'attribute' => 'name',
                'value' => function ($model, $key, $index, $column) {
                    return GeneralRoles::getAll($model->pk_role);
                },
                'filter' => GeneralRoles::getAll(),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>