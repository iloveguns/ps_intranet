<?php
use app\modules\user\UserModule;
use yii\helpers\Html;
use app\modules\user\models\Organization;
use yii\widgets\ActiveForm;
?>

<div class="general-roles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fk_organization')->dropDownList(Organization::getAll()) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <p><?=UserModule::t('rights', 'List rights:')?></p>
    
    <?php
    foreach ($modelRights->attributeLabels() as $name => $attr) {
        echo $form->field($modelRights, $name)->checkBox();
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>