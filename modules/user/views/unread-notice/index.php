<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modelsView\ViewUnreadNotice;

$this->title = Yii::t('app', 'Notifications Archive');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unread-notice-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= Html::button(Yii::t('app', 'Delete Selected'), ['id' => 'btn-check', 'class' => 'btn btn-danger']); ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => Yii::t('app', 'Text notify'),
                'value' => function ($model, $key, $index, $column) {
                    return ViewUnreadNotice::newHtmlNotify($model);
                },
                'format' => 'raw',                
            ],
            ['class' => 'yii\grid\CheckboxColumn'],
        ],
        'pager' => [
           'class' => \kop\y2sp\ScrollPager::className(),
           'container' => '.grid-view tbody',
           'item' => 'tr',
           'paginationSelector' => '.grid-view .pagination',
           'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
        ],
    ]); ?>
    
    <?php //переделать
    $script = '
    jQuery(document).ready(function() {
      $("#btn-check").click(function() {
        var keys = $("#w0").yiiGridView("getSelectedRows");
        $.ajax({
            type: "POST",
            url: "'.\yii\helpers\Url::to(['unread-notice/delete']).'", 
            dataType: "json",
            data: {dellistids: keys},
            success: function(response){
                if(response.success){
                    $.each( keys, function( index, value ) {
                        $("tr[data-key="+value+"]").remove();
                    });
                } else {
                    alert("Ошибка удаления или сущность уже удалена");
                }
            },
            beforeSend: function(){
                if(keys == "") return false;
                if(!confirm("Точно удалить?")) return false;                
            }
        });
      });
    });';
    $this->registerJs($script, \yii\web\View::POS_END);
    ?>
</div>