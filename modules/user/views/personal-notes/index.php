<?php

use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = Yii::t('app/views', 'My Notes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personal-notes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app/views', 'Create Personal Notes'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'options' => ['class' => 'row'],
        'itemView' => '_list',
        'summaryOptions' => [
            'class' => 'col-md-12',
        ]
    ]) ?>
</div>