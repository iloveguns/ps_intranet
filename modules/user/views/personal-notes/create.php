<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Create Personal Notes');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'My Notes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personal-notes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>