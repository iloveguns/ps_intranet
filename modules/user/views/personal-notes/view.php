<?php

use yii\helpers\Html;

$this->title = $model->title_pnote;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'My Notes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personal-notes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->pk_pnote], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->pk_pnote], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app/views', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="well">
        <div class="text-from-redactor">
            <?= $model->text_pnote?>
        <div>
    </div>
</div>