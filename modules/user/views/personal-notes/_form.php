<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="personal-notes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title_pnote')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text_pnote')->textarea(['tinymce' => 'tinymce']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>