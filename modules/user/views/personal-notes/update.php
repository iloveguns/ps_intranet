<?php

use yii\helpers\Html;

$this->title = Yii::t('app/views', 'Update Personal Notes') . '"'.$model->title_pnote.'"';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'My Notes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title_pnote, 'url' => ['view', 'id' => $model->pk_pnote]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personal-notes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>