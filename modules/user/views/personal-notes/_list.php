<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>

<div class="col-md-6">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::a(Html::encode($model->title_pnote), ['view', 'id' => $model->pk_pnote]) ?></h3>
        </div>
    </div>
</div>