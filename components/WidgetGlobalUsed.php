<?php
namespace app\components;

use yii\base\Widget;
use Yii;
use app\modules\polling\models\ApiPolling;

/*
 * виджет для вставки общедоступного доп. контента
 */
class WidgetGlobalUsed extends Widget{
    public $model;
    private $pollings;

    public function init(){
        //получить голосования
        $pollings = ApiPolling::getPollings();
        
        //найти существующую привязку к голосованиям
        $selectPollings = WidgetGlobalUsed::findOneByAttr($this->model, 'polling');
        
        $this->pollings = ['polling' => $pollings, 'selected' => $selectPollings];
    }

    public function run(){
        return $this->render('widgetGlobalUsed', ['model' => $this->model, 'pollings' => $this->pollings]);
    }
    
    /*
     * сохранение
     * @param {string} $model - модель
     */
    public function save($model) {
        $post = Yii::$app->request->post();
        
        foreach ($post['GlobalUsed'] as $attr => $value) {//только голосования пока есть
            if(empty($value)) continue;
            if($found = WidgetGlobalUsed::findOneByAttr($model, $attr)) {//если есть - обновить
                Yii::$app->db->createCommand()->update('global_used', ['global_value' => $value], 'pk_global_used = ' . $found['pk_global_used'])->execute();
            } else {//если нет - вставить
                Yii::$app->db->createCommand()->insert('global_used', [
                    'className' => $model->getName(),
                    'item_id' => $model->primaryKey,
                    'global_attribute' => $attr,
                    'global_value' => $value
                ])->execute();
            }
        }
    }
    
    /*
     * найти по атрибуту
     * @param {string} $model - модель
     * @param {string} $attr - атрибут для поиска
     */
    private function findOneByAttr($model, $attr) {
        return (new \yii\db\Query())
            ->select(['global_value', 'pk_global_used'])
            ->from('global_used')
            ->where(['className' => $model->getName(), 'item_id' => $model->primaryKey, 'global_attribute' => $attr])
            ->one();
    }
    
    private function findAll($model) {
        return (new \yii\db\Query())
            ->select(['*'])
            ->from('global_used')
            ->where(['className' => $model->getName(), 'item_id' => $model->primaryKey])
            ->one();
    }
    
    /*
     * вывод того, что было сохранено
     */
    public function renderIt($model) {
        if($found = self::findAll($model)) {
            if($found['global_attribute'] === 'polling') {//голосование.дать указания, где взять дальнейшую инфу
                
                if($polling = ApiPolling::findOne($found['global_value'])) {
                    $userHadPolling = $polling->userHadPolling();
                    
                    if($userHadPolling) {
                        return '<div class="alert alert-success" role="alert">Голосование завершено</div>' . \yii\helpers\Html::a('Посмотреть результаты', ['/polling/polling/results', 'id' => $polling->pk_polling], ['class' => 'btn btn-primary']);
                    } else {
                        $model = $polling->getFirstStage(true);//первый этап
                    
                        $nextBtn = ApiPolling::hasNext($model->fk_polling, $model->pk_stage);
                        $previousBtn = ApiPolling::hasPrevious($model->fk_polling, $model->pk_stage);
                    
                        return \Yii::$app->controller->renderPartial('@app/modules/polling/views/polling-stages/view', [
                            'model' => $model,
                            'admin' => false,
                            'nextBtn' => $nextBtn,
                            'previousBtn' => $previousBtn,
                            'modelAddAnswers' => new \app\modules\polling\models\ApiPollingAddanswers()
                        ]);
                    }
                } else {
                    return '<div class="alert alert-danger" role="alert">Не удается найти голосование</div>';
                }
                
            }
        }
        return '';
    }
}