<?php
/*
 * виджет отображения последних объявлений (всех)
 * $last - количество последних объявлений на вывод(5)
 */
namespace app\components;

use yii\base\Widget;
use app\modules\intranet\models\Callboard;
use yii\helpers\Url;

class WidgetCallboardLast extends Widget{
    public $limit;//количество последних объявлений
    public $where = '';//условие вывода http://www.yiiframework.com/doc-2.0/yii-db-query.html#where()-detail
    public $title;//заголовок виджета
    public $uniqname;//уникальное имя(для кук и урл)
    protected 
        $model, 
        $dependency,
        $url;//ссылка на посмотреть все
    
    /*
     * не могу понять, почему он 10 раз за каждый вызов делает запрос count
     */
    public function init(){
        if(!$this->limit) $this->limit = 10;
        
        $this->dependency = new \yii\caching\DbDependency();
        $this->dependency->sql = 'SELECT count(*) as c FROM '.Callboard::tableName();
        $this->model = Callboard::getDb()->cache(function () {
            return \app\modules\intranet\models\CallboardSearch::querySearch()->andWhere($this->where)->limit($this->limit)->orderBy('pk_callboard DESC')->all();
        }, \Yii::$app->params['cacheTime'], $this->dependency);
        
        switch ($this->uniqname){
            case 'widgetcall' : //просто объявления
                $this->url = Url::toRoute(['/intranet/callboard/index']);
                break;
            
            case 'widgetcallbirth' : 
                $this->url = Url::toRoute(['/intranet/callboard/index?CallboardSearch[fk_callboard_category]='.Callboard::BIRTHDAY]);
                break;
        }
    }

    public function run(){
        return $this->render('widgetCallboardLast' ,['model' => $this->model, 'title' => $this->title, 'cookieName' => $this->uniqname, 'url' => $this->url, 'dependency' => $this->dependency]);
    }
}