<?php
/*
 * виджет вывода выбора структур
 */
namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;
use app\modules\user\models\User;

class WidgetStructure extends Widget{
    public $model;

    public function init(){
        
    }

    public function run(){
        return $this->render('widgetStructure', ['model' => $this->model]);
    }
}
?>