<?php
/*
 * виджет написания комментариев
 */
namespace app\components;

use Yii;
use yii\base\Widget;

class WidgetComments extends Widget {
    public $modelEntity;//модель сущности, которой необходимы комменты | $this->modelEntity->getName() | $this->modelEntity->primaryKey

    public function run(){
        Yii::$app->view->registerJs("//константы для комментариев
            var react_item_id = " . $this->modelEntity->getPrimaryKey() . ",
                react_item_class = '" . $this->modelEntity->getName() . "',
                react_null_user = 'Великий интранет';
        ", \yii\web\View::POS_BEGIN);

        Yii::$app->view->registerJsFile('@web/react/vendor.js',['depends' => 'app\assets\AppAsset', 'position' => yii\web\View::POS_END]);
        Yii::$app->view->registerJsFile('@web/react/comments.js',['depends' => 'app\assets\AppAsset', 'position' => yii\web\View::POS_END, 'async' => true]);
        Yii::$app->view->registerJsFile('@web/js/build/tippy.min.js',['depends' => 'app\assets\AppAsset', 'position' => yii\web\View::POS_END, 'async' => true]);
        Yii::$app->view->registerCssFile('@web/css/build/tippy.min.css');

        return '<div id="comments"></div>'
        . '<input id="tinymce-file-upload" type="file" name="tinymce-file-upload" style="display: none;" accept="image/*" />' // для изображений
        . '<input id="tinymce-media-upload" type="file" name="tinymce-media-upload" style="display: none;" accept="video/*" />'; // для видеофайлов
    }
}