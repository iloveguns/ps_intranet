<?php
namespace app\components;

use Yii;
use yii\helpers\FileHelper;
use yii\base\Event;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\View;
use app\helpers\Packer;
use app\helpers\HtmlCompressor;

/*
    "natxet/cssmin": "*",
    "mrclay/minify": "*",
    "tedivm/jshrink": "*",
 * при разных настройках - разный результат
 */
class CodeMinifier extends \yii\base\Component implements \yii\base\BootstrapInterface{
    
    public $useInAjaxPjax = false;//использовать сжатие js в ajax/pjax//в ajax слишком много всего подключает, лучше держать выключенным
    
    public $enabled = true;
    
        public $jsCompress = true;

            public $jsFileCompile = false;//объединять ли js файлы(pjax на повторное обновление выдает ошибку "cannot read property submit of undefined" - на кнопке)

                public $jsFileRemouteCompile = false;//скачивание удаленных файлов(подключенных)

            public $jsFileCompress = true;

            public $jsPack = false;//обфускация js на выходе

            public $jsCompressFlaggedComments = false;//сохранять ли комменты -true(авторские права)/ false - удаление всех комментов

    public $cssFileCompress = false;
    
    public $cssFileCompile = false;
        
        public $cssCompress = true;
        
        public $cssFileBottom = false;//тег css сместить вниз, особого смысла нет

    public $htmlCompress = true;
    
        public $htmlCompressOptions = [
            'extra'         => true,
            'no-comments'   => true
        ];
        

    public function bootstrap($app) {
        if ($app instanceof \yii\web\Application && !YII_ENV_DEV) {//для приложения(не консольного) + в продакшн режиме только//
            
            //все по порядку
            $app->view->on(View::EVENT_END_PAGE, function(Event $e) use ($app) {
                $view = $e->sender;
                
                if($app->request->isAjax || $app->request->isPjax) {
                    //а может не стоит прерывать а всего лишь с пустой настройкой прогонять?
                    if(!$this->useInAjaxPjax) return;//запрещено настройками
                }
                
                if ($this->enabled && $view instanceof View && $app->response->format == Response::FORMAT_HTML) {
                    $this->_processing($view);
                }
            });
            
            //html минифицировать
            $app->response->on(\yii\web\Response::EVENT_BEFORE_SEND, function (\yii\base\Event $event) use ($app) {
                $response = $event->sender;

                if ($this->enabled && $this->htmlCompress && $response->format == \yii\web\Response::FORMAT_HTML) {
                    if (!empty($response->data)) {
                        $response->data = $this->_processingHtml($response->data);
                    }

                    if (!empty($response->content)) {
                        $response->content = $this->_processingHtml($response->content);
                    }
                }
            });
        }
    }

    /*
     * основная функция
     */
    protected function _processing(View $view) {
        //минификация js кода
        if ($view->js && $this->jsCompress) {
            foreach ($view->js as $pos => $parts) {
                if ($parts) {
                    $view->js[$pos] = $this->_processingJs($parts, $pos);
                }
            }
        }
        
        //минификация подключаемых css
        if ($view->css && $this->cssCompress) {
            $view->css = $this->_processingCss($view->css);
        }
        
        //минификация подключаемых css файлов
        if ($view->cssFiles && $this->cssFileCompile) {
            $view->cssFiles = $this->_processingCssFiles($view->cssFiles);
        }
        
        //Перенос файлов css вниз страницы, где файлы js View::POS_END
        if ($view->cssFiles && $this->cssFileBottom) {
            if (\yii\helpers\ArrayHelper::getValue($view->jsFiles, View::POS_END)) {
                $view->jsFiles[View::POS_END] = \yii\helpers\ArrayHelper::merge($view->cssFiles, $view->jsFiles[View::POS_END]);//соединить с js
            } else {
                $view->jsFiles[View::POS_END] = $view->cssFiles;
            }
            $view->cssFiles = [];
        }
        
        //минификация файлов js в один
        if ($view->jsFiles && $this->jsFileCompile) {
            foreach ($view->jsFiles as $pos => $files) {
                if ($files) {
                    $view->jsFiles[$pos] = $this->_processingJsFiles($files);
                }
            }
        }
    }
    
    /*
     * минификация js кода
     * $this->registerJs()
     * @param $parts {array} - массив js кода
     * @param $pos {string} - позиция, в которой код должен быть зарегистрирован
     */
    protected function _processingJs($parts, $pos) {
        if (empty($parts)) return;
        
        $result = [];
        $firstkey = NULL;
        $allJsKey = '';//собираемый js код
        
        foreach ($parts as $key => $value) {
            if($firstkey === NULL) {
                $firstkey = $key;//определить первый ключ и в него все залить
            }
            
            if($this->jsPack) {
                if($pos == View::POS_BEGIN) {//скрипты в начале вызывают некоторые проблемы(возможно не успевают распаковываться)
                    $allJsKey .= \JShrink\Minifier::minify($value, ['flaggedComments' => $this->jsCompressFlaggedComments]);
                    $result[$key] = '';
                } else {
                    $packer = new Packer($value);//каждый кусок кода запаковать, можно также в конце все собрать и запаковать
                    $result[$key] = '';
                    $allJsKey .= $packer->pack();
                }
            } else if($this->jsCompress) {
                $allJsKey .= \JShrink\Minifier::minify($value, ['flaggedComments' => $this->jsCompressFlaggedComments]);
                $result[$key] = '';
                
            }
        }
        $result[$firstkey] = $allJsKey;//все в 1 код собрать, а остальное будет пустым(для уменьшения еще большего, т.к. перенос строки разделяет эти куски)

        return $result;
    }
    
    /*
     * минификация css кода
     * $this->registerСss()
     */
    protected function _processingCss($css = []) {
        $newCss = [];

        foreach ($css as $code => $value) {
            $newCss[] = preg_replace_callback('/<style\b[^>]*>(.*)<\/style>/is', function($match) {
                return $match[1];
            }, $value);
        }

        $css = implode($newCss, "\n");
        $css = \CssMin::minify($css);
        return [md5($css) => Html::style($css)];
    }
    
    /*
     * минификация html
     */
    protected function _processingHtml($html) {
        $options = $this->htmlCompressOptions;
        return HtmlCompressor::compress($html, $options);
    }
    
    /*
     * минификация js в один файл
     * решил не использовать, а проводить операции с js препроцессорами
     */
    protected function _processingJsFiles($files = []) {
        $fileName   =  md5( implode(array_keys($files)) . $this->getSettingsHash()) . '.js';
        $publicUrl  = \Yii::getAlias('@web/assets/js-compress/' . $fileName);

        $rootDir    = \Yii::getAlias('@webroot/assets/js-compress');
        $rootUrl    = $rootDir . '/' . $fileName;

        //если есть - вернуть ссылку на файл
        if (file_exists($rootUrl)) {
            $resultFiles        = [];

            foreach ($files as $fileCode => $fileTag) {
                if (!Url::isRelative($fileCode)) {
                    $resultFiles[$fileCode] = $fileTag;
                } else {
                    if ($this->jsFileRemouteCompile) {
                        $resultFiles[$fileCode] = $fileTag;
                    }
                }
            }

            $publicUrl                  = $publicUrl . "?v=" . filemtime($rootUrl);
            $resultFiles[$publicUrl]    = Html::jsFile($publicUrl);            
            return $resultFiles;
        }

        try {
            $resultContent  = [];
            $resultFiles    = [];
            foreach ($files as $fileCode => $fileTag) {                
                if (Url::isRelative($fileCode)) {
                    $contentFile = \app\models\FunctionModel::fileGetContents( Url::to(\Yii::getAlias($fileCode), true) );
                    $resultContent[] = trim($contentFile) . "\n;";
                } else {
                    if ($this->jsFileRemouteCompile) {
                        $contentFile = \app\models\FunctionModel::fileGetContents( $fileCode );
                        $resultContent[] = trim($contentFile);
                    } else {
                        $resultFiles[$fileCode] = $fileTag;
                    }
                }
            }
        } catch (\Exception $e) {
            \Yii::error($e->getMessage(), static::className());
            return $files;
        }
        
        if ($resultContent) {
            $content = implode($resultContent, ";\n");
            if (!is_dir($rootDir)) {
                if (!FileHelper::createDirectory($rootDir, 0777)) {
                    return $files;
                }
            }
            
            if ($this->jsFileCompress) {
                $content = \JShrink\Minifier::minify($content, ['flaggedComments' => $this->jsCompressFlaggedComments]);
            }

            $page = \Yii::$app->request->absoluteUrl;
            $useFunction = function_exists('curl_init') ? 'curl extension' : 'php file_get_contents';
            $filesString = implode(', ', array_keys($files));

            $file = fopen($rootUrl, "w");
            fwrite($file, $content);
            fclose($file);
        }


        if (file_exists($rootUrl)) {
            $publicUrl                  = $publicUrl . "?v=" . filemtime($rootUrl);
            $resultFiles[$publicUrl]    = Html::jsFile($publicUrl);
            return $resultFiles;
        } else {
            return $files;
        }
    }
    
    /*
     * минификация css файлов. сбор всех в 1 + скачивание удаленных
     */
    protected function _processingCssFiles($files = []) {
        $fileName   =  md5( implode(array_keys($files)) . $this->getSettingsHash() ) . '.css';
        $fileNamePrint   =  md5( implode(array_keys($files)) . $this->getSettingsHash() ) . 'print.css';
        $publicUrl  = \Yii::getAlias('@web/assets/css-compress/' . $fileName);
        $publicUrlPrint  = \Yii::getAlias('@web/assets/css-compress/' . $fileNamePrint);
        
        $rootDir    = \Yii::getAlias('@webroot/assets/css-compress');
        $rootUrl    = $rootDir . '/' . $fileName;
        $rootUrlPrint    = $rootDir . '/' . $fileNamePrint;
        
        if (file_exists($rootUrl)) {//существует файл - просто отдать его
            $return = true;
            $resultFiles = [];

            foreach ($files as $fileCode => $fileTag) {
                if (Url::isRelative($fileCode)) {
                    
                } else {
                    if (!$this->cssFileRemouteCompile) {
                        $resultFiles[$fileCode] = $fileTag;
                    }
                }
            }

            $publicUrl = $publicUrl . "?v=" . filemtime($rootUrl);
            $resultFiles[$publicUrl] = Html::cssFile($publicUrl);   
            
            if (file_exists($rootUrlPrint)) {//существует файл печати - просто отдать его
                $publicUrl = $publicUrlPrint . "?v=" . filemtime($rootUrlPrint);
                $resultFiles[$publicUrl] = Html::cssFile($publicUrl, ['media' => 'print']);
            }
            
            return $resultFiles;
        }
        
        try {
            $resultContent  = [];
            $resultContentPrint  = [];
            $resultFiles    = [];
            foreach ($files as $fileCode => $fileTag) {
                if (Url::isRelative($fileCode)) {
                    $contentTmp         = trim(\app\models\FunctionModel::fileGetContents( Url::to(\Yii::getAlias($fileCode), true) ));

                    $fileCodeTmp = explode("/", $fileCode);
                    unset($fileCodeTmp[count($fileCodeTmp) - 1]);
                    $prependRelativePath = implode("/", $fileCodeTmp) . "/";

                    $contentTmp    = \Minify_CSS::minify($contentTmp, [
                        "prependRelativePath" => $prependRelativePath,
                        'compress'          => true,
                        'removeCharsets'    => true,
                        'preserveComments'  => true,
                    ]);
                    
                    if(strstr($fileTag, 'print')) {//css для печати надо отдельно.часто там взаимоисключающие правила
                        $resultContentPrint[] = $contentTmp;
                    } else {
                        $resultContent[] = $contentTmp;
                    }
                } else {
                    if ($this->cssFileRemouteCompile) {
                        //Пытаемся скачать удаленный файл
                        $contentTmp = trim(\app\models\FunctionModel::fileGetContents( $fileCode ));
                        if(strstr($fileTag, 'print')) {//css для печати надо отдельно.часто там взаимоисключающие правила
                            $resultContentPrint[] = $contentTmp;
                        } else {
                            $resultContent[] = $contentTmp;
                        }
                    } else {
                        $resultFiles[$fileCode] = $fileTag;
                    }
                }
            }
        } catch (\Exception $e) {
            \Yii::error($e->getMessage(), static::className());
            return $files;
        }
        
        if ($resultContent) {
            $content = implode($resultContent, "\n");
            if (!is_dir($rootDir)) {
                if (!FileHelper::createDirectory($rootDir, 0777)) {
                    return $files;
                }
            }

            if ($this->cssFileCompress) {
                $content = \CssMin::minify($content);
            }

            $page = \Yii::$app->request->absoluteUrl;
            $useFunction = function_exists('curl_init') ? 'curl extension' : 'php file_get_contents';
            $filesString = implode(', ', array_keys($files));

            \Yii::info("Create css file: {$publicUrl} from files: {$filesString} to use {$useFunction} on page '{$page}'", static::className());


            $file = fopen($rootUrl, "w");
            fwrite($file, $content);
            fclose($file);
        }
        
        if($resultContentPrint) {
            $content = implode($resultContentPrint, "\n");
            
            if ($this->cssFileCompress) {
                $content = \CssMin::minify($content);
            }
            
            $file = fopen($rootUrlPrint, "w");
            fwrite($file, $content);
            fclose($file);
        }
        
        return $this->_processingCssFiles($files);
    }
    
    
    public function getSettingsHash() {
        return serialize((array) $this);
    }
}
