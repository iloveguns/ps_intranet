<?php
use yii\helpers\Html;
use app\modules\user\models\User;
$cookieName = 'widgetonlineusers';
$collapse = isset($_COOKIE[$cookieName]);
?>
<div class="box box-success <?php if($collapse) echo 'collapsed-box'?>">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('app', 'Users Online')?></h3>
        <div class="box-tools pull-right">
            <span class="label label-info"><?= count($model) ?></span>
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-id-widg="<?=$cookieName?>"><i class="fa fa-<?= ($collapse) ? 'chevron-up' : 'chevron-down'?>"></i></button>
        </div>
    </div>
    <div class="box-body">
        <ul class="users-list clearfix">
        <?php foreach ($model as $user) : ?>
            <li>
                <?= $user->pkUser->getLinkAvatar(true, [], [100,100])?>
            </li>
        <?php endforeach ?>
        </ul>
    </div>
    <div class="box-footer text-center">
        <?= Html::a(Yii::t('app', 'View All Users'), ['/user/user/last-online'], ['class' => 'uppercase'])?>
    </div>
</div>