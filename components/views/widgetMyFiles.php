<?php
use yii\widgets\Pjax;
use yii\helpers\Html;
use kartik\file\FileInput;
use yii\helpers\Url;
$params = [];//дополнительные параметры для загрузчика
?>
<?php Pjax::begin(['id' => 'pjax_files' ,'linkSelector'=>'#pjaxbtn', 'timeout' => false]); ?>

<?php
if($updateUrl) { //если указан путь для обновления
    echo Html::a("Обновить", [$updateUrl, 'id' => $id, 't'=>time()], ['class' => 'btn hidden', 'id'=>'pjaxbtn']);//не хочет обновляться по хорошему, только через кнопку с меткой времени
    $this->registerJs('
        jQuery("#uploader-files").on("fileuploaded", function(event, data, id, index) { loadfile("'.addslashes($modelClass).'", "'.$id.'"); setTimeout(function() { $("#pjaxbtn").click() }, 100)});//обновление после загрузки
    ', yii\web\View::POS_READY);
} else {//если не указан - просто показать сообщение загрузки
    $this->registerJs("
        jQuery('#uploader-files').on('filebatchpreupload', function(event, data, id, index) {
            $('#kv-success-1').html('<h4>".Yii::t('app', 'Upload Status')."</h4><ul></ul>').hide();
        }).on('fileuploaded', function(event, data, id, index) {
            var fname = data.files[index].name,
                out = '<li>' + '".Yii::t('app', 'Uploaded file')." # ' + (index + 1) + ' - '  +  
                    fname + '</li>';
            $('#kv-success-1 ul').append(out);
            $('#kv-success-1').fadeIn('slow');
        });
    ", yii\web\View::POS_READY);
}
?>
Максимальный размер для загрузки: <?= ini_get('upload_max_filesize') ?>
<div class="panel panel-default widget-files-upload">
    <div class="panel-heading"><?= Yii::t('app', 'Project files')?></div>
    <div class="panel-body text-left">
        <div id="kv-error" style="display:none"></div>
        <div id="kv-success-1" class="alert alert-success fade in" style="margin-top:10px;display:none"></div>
        <?php
        foreach ($model as $file) {
            echo '<div id="df-'.$file->id_file.'">';
            echo Html::a($file->name, ['/file/upload', 'id_file' => $file->id_file], ['class' => 'btn btn-default', 'target' => '_blank']);

            //ajax удаление файла
            echo Html::a(Yii::t('app', 'Delete'), ['/file/delete-by-id', 'id_file' => $file->id_file], [
                'data-on-done' => 'onAfterDeleteFileAjax',//js функция после выполнения
                'class' => 'btn btn-danger delete-file',
            ]);
            
            echo '<button class="btn" data-clipboard-text=\''.Url::to(['/file/upload', 'id_file' => $file->id_file], true).'\'>'.Yii::t('app', 'Copy Link').'</button>';//\''.Html::a($file->name, $file->path).'\'
            echo '<button class="btn hidden" data-clipboard-text=\''.Url::to($file->path, true).'\'>Прямая ссылка</button>';//\''.Html::a($file->name, $file->path).'\'

            $this->registerJs("
                var clipboard = new Clipboard('.btn');
                clipboard.on('success', function(e) {
                    e.clearSelection();
                    alert('".Yii::t('app', 'Link copied to buffer')."');
                });
            ", yii\web\View::POS_READY);
            
            $this->registerJs("$('.delete-file').click(handleAjaxLink);
            ajaxCallbacks.onAfterDeleteFileAjax = function (response) {
                if(response.success){
                    bootstrapAlert('success','".Yii::t('app', 'Deleted file')."');
                    $('#df-'+response.id_file).remove();
                }
                else{
                    bootstrapAlert('danger','".Yii::t('app', 'Not deleted file')."');
                }
            }", \yii\web\View::POS_END);
            echo '</div>';
        }

            $params = [
                'showUpload' => true,
                'showRemove' => false,
                'showPreview' => true,
            ];

            $this->registerJs('
                //jQuery("#uploader-files").on("filebatchselected", function(event, files) { jQuery("#uploader-files").fileinput("upload"); });
            ', yii\web\View::POS_READY);

            echo FileInput::widget([
                'name' => 'user_agent',
                'language' => 'ru',
                'id' => 'uploader-files',
                'options' => ['multiple' => true],
                'pluginOptions' => array_merge([
                    'showCaption' => false,//ошибки показ
                    'previewFileType' => 'any',
                    'maxFileCount' => 8,
                    'uploadUrl' => Url::to(['/file/file-upload', 'id' => $id, 'name' => $name, 'class' => $modelClass]),
                    'elErrorContainer' => '#kv-error',
                ], $params)
            ]);
        ?>
    </div>
</div>


<?php Pjax::end(); ?>