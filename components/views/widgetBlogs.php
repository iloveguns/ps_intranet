<?php
use yii\helpers\Url;
use app\modules\user\models\User;
use app\modules\user\models\Units;

/*
 * персональный для каждого человека(кэш)
 */

$collapse = isset($_COOKIE[$cookieName]);
?>
<div class="box box-success <?php if($collapse) echo 'collapsed-box'?>">
    <?php if ($this->beginCache($cookieName.'-'.Yii::$app->user->id, ['dependency' => $dependency])) { ?>
    <div class="box-header with-border">
        <h3 class="box-title"><?= $title?></h3>
        <div class="box-tools pull-right">
            <a href="<?= Url::toRoute(['/intranet/blogs/create'])?>" class="label label-info" data-toggle="tooltip" data-placement="top" title="<?= Yii::t('app', 'Create Blog')?>"><?= Yii::t('app', 'Create')?></a>
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-id-widg="<?=$cookieName?>"><i class="fa fa-<?= ($collapse) ? 'chevron-up' : 'chevron-down'?>"></i></button>
        </div>
    </div>
    <div class="box-body">
        <ul class="products-list product-list-in-box">
            <?php foreach ($model as $call) : ?>
                <li class="item">
                    <?= \yii\bootstrap\Html::a(strip_tags($call->title).' <span class="label label-info pull-right">'.$call->countComments().'</span>', ['/intranet/blogs/view', 'id' => $call->pk_blog], ['title' => Yii::$app->formatter->asDate($call->create_date)])?>
                </li>
            <?php endforeach ?>
        </ul>
    </div>
    <div class="box-footer text-center">
        <a href="<?= $url?>" class="uppercase"><?= Yii::t('app', 'Show All')?></a>
    </div>
    <?php
    $this->endCache();
    }
    ?>
</div>
