<?php
use yii\helpers\Html;
use app\modules\user\models\Units;
?>
<div class="panel-body">
    <?= Html::hiddenInput('selectedStructuresData', Units::structHiddenFieldData($model), ['id'=>'selectedStructuresData']) ?>
    
    <div class="form-group clearfix">
        <a class='btn btn-primary' onclick="getStructure()">сотрудники</a>
    </div>

    <div id="selectedStructuresListForm">
        <?= Units::StructInfo($model)//краткая инфа о всех, кому назначено ?>
    </div>
    <?php 
    $emptystruct = json_encode(['organization' => '', 'divizion' => '', 'department' => '', 'user' => '']);//строка, обозначающая пустое поле для выбора сотрудников
    $this->registerJs('window.emptystruct = \''.$emptystruct.'\';getStructure(false); setTimeout(function(){copyStruct($("#selectedStructuresListForm").children(), $("#selectedStructuresList").first())}, 800); setTimeout(function(){structStartAll()}, 1200);', yii\web\View::POS_READY); ?>
</div>