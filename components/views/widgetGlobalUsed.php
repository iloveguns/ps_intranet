<?php
use kartik\select2\Select2;
?>
<div class="panel-group" id="global_add_options_accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="global_add_options">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#global_add_options_accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                  Дополнительные параметры
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="global_add_options">
            <div class="panel-body">
                <?php
                echo '<label class="control-label">Добавить голосование</label>';
                echo Select2::widget([
                    'name' => 'GlobalUsed[polling]',
                    'data' => $pollings['polling'],
                    'value' => $pollings['selected'],
                    'options' => [
                        'placeholder' => Yii::t('app', 'Select')
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>