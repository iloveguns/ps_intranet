<?php
use app\modules\user\UserModule;
if ($this->beginCache('widget-former-users', ['dependency' => $dependency])) {
?>
<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
  <?= UserModule::t('all', 'Former Employees') . ' (' . count($model) . ')' ?>
</button>
<div class="collapse" id="collapseExample">
  <div class="well">
    <ul class="products-list product-list-in-box">
        <?php foreach ($model as $user) : ?>
            <li class="col-md-3 item pull-left">
                <div class="product-img">
                    <?= $user->getLinkAvatar(true, ['lazy' => true])?>
                </div>
                <div class="product-info">
                    <?= $user->getLinkToProfile($user->fullName)//фио?>
                    <span class="product-description"><?= $user->fkAppointment->name ?></span>
                </div>
            </li>
        <?php endforeach ?>
    </ul>
  </div>
</div>
<?php 
$this->registerJs("
    // ленивая загрузка изображений уволившихся
    function lazyload() {
        document.querySelectorAll('#collapseExample img[lazy]')
            .forEach(function(cv){
                cv.setAttribute('src', cv.getAttribute('data-src'));
                cv.removeAttribute('data-src');
                cv.removeAttribute('lazy');
            });
            
        $('#collapseExample').off('show.bs.collapse', lazyload);
    }
    
    $('#collapseExample').on('show.bs.collapse', lazyload);
", \yii\web\View::POS_END);
$this->endCache(); } ?>