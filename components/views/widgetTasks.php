<?php
use yii\helpers\Url;

$collapse = isset($_COOKIE[$cookieName]);

$this->registerJs("
    var tasks_cookie_collapse_name = '" . $cookieName . "';        
", yii\web\View::POS_END);

$this->registerJsFile('@web/js/build/tasks.js', ['depends' => 'yii\jui\JuiAsset']);
?>
<div id="tasks-list" class="box box-success <?php if($collapse) echo 'collapsed-box'?>">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('app', 'Tasks Widget')?> (<span class="tasks-all-count"></span>)</h3>
        <div class="box-tools pull-right">
            <?= \yii\helpers\Html::dropDownList('sort_task', $cookie, ['end_date_asc' => Yii::t('app/models', 'sort end_date asc'), 'end_date_desc' => Yii::t('app/models', 'sort end_date desc'), 'start_date_asc' => Yii::t('app/models', 'sort start_date asc'), 'start_date_desc' => Yii::t('app/models', 'sort start_date desc')], ['class' => 'form-control', 'style' => 'display: inline;width: initial', 'id' => 'sort_task']) ?>
            <a href="<?= Url::toRoute(['/intranet/tasks/create'])?>" class="label label-info" data-toggle="tooltip" data-placement="top" title="<?= Yii::t('app', 'Create New Task')?>"><?= Yii::t('app', 'Create')?></a>
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-id-widg="<?=$cookieName?>"><i class="fa fa-<?= ($collapse) ? 'chevron-up' : 'chevron-down'?>"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="row btns-shows">
            <div class="col-md-12">
                <div class="pull-left">
                    <span class="mr-5"><i class="fa fa-battery-full text-green" title="Постоянные" data-class="unlimited" data-toggle="tooltip" data-placement="top"></i> - <span id="task-unlimited"></span></span>
                    <span class="mr-5"><i class="fa fa-battery-three-quarters text-primary" title="Текущие" data-class="current" data-toggle="tooltip" data-placement="top"></i> - <span id="task-current"></span></span>
                    <span class="mr-5"><i class="fa fa-battery-quarter text-yellow" title="Срочные" data-class="endsoon" data-toggle="tooltip" data-placement="top"></i> - <span id="task-endsoon"></span></span>
                    <span class="mr-5"><i class="fa fa-battery-empty text-red" title="Просроченные" data-class="late" data-toggle="tooltip" data-placement="top"></i> - <span id="task-late"></span></span>
                </div>
            </div>
        </div>

        <ul class="products-list product-list-in-box"></ul>
    </div>
    <div class="box-footer text-center">
        <a href="<?= Url::toRoute(['/intranet/tasks/index'])?>" class="uppercase"><?= Yii::t('app', 'Show All')?></a>
    </div>
</div>