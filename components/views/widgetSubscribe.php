<?php
use yii\bootstrap\Html;
use yii\helpers\Url;
    
$id = $model->getPrimaryKey();
$dclasss = '';
$dclassu = '';

if($subscribed){
    $dclasss = 'display:none';
} else {
    $dclassu = 'display:none';
}
echo Html::button(Yii::t('app', 'Subscribe'), ['class' => 'btn btn-primary btn-subscribes', 'style' => $dclasss, 'id' => 'set-subscribe', 'url' => Url::to(['/notify/subscribe']), 'data-class' => $model->className,]);
echo Html::button(Yii::t('app', 'Unsubscribe'), ['class' => 'btn btn-primary btn-subscribes', 'style' => $dclassu, 'id' => 'set-unsubscribe', 'url' => Url::to(['/notify/unsubscribe']), 'data-class' => $model->className,]);

$script = <<< JS
$('.btn-subscribes').on('click', function() {
    $.ajax({
        method: 'POST',
        url: $(this).attr('url'),
        data: {class: $(this).attr('data-class'), id: $id},
        success: function(data) {
            if(data.success == 'subscribed') {
                $('#set-subscribe').hide();
                $('#set-unsubscribe').show();
            } else if(data.success == 'unsubscribed') {
                $('#set-unsubscribe').hide();
                $('#set-subscribe').show();
            } else {
                console.error('что-то случилось');
            }
        }
    });
});
JS;
$this->registerJs($script, yii\web\View::POS_END);