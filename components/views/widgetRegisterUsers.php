<?php
use yii\helpers\Url;
use app\modules\user\models\User;

$collapse = isset($_COOKIE[$cookieName]);
?>
<?php if ($this->beginCache($cookieName, ['dependency' => $dependency, 'duration' => 3600*30])) { ?>
<div class="box box-success <?php if($collapse) echo 'collapsed-box'?>">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('app', 'Last Register Users')?></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-id-widg="<?=$cookieName?>"><i class="fa fa-<?= ($collapse) ? 'chevron-up' : 'chevron-down'?>"></i></button>
        </div>
    </div>
    <div class="box-body">
        <ul class="products-list product-list-in-box">
            <?php foreach ($model as $user) : ?>
                <li class="item">
                    <div class="product-img">
                        <?= $user->getLinkAvatar(true)?>
                    </div>
                    <div class="product-info">
                        <?= $user->getLinkToProfile(false, false, false)?>
                        <span class="product-description"><?= $user->fkDepartment->name?> - <?= $user->fkAppointment->name?></span>
                    </div>
                </li>
            <?php endforeach ?>
        </ul>
    </div>
</div>
<?php $this->endCache(); } ?>