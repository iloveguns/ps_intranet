<div class="panel panel-default widget-recent-viewers">
    <div class="panel-heading"><?= Yii::t('app', 'Recent Viewers')?> 
        <div class="box-tools pull-right">
            <span class="label label-info" data-toggle="tooltip" title="<?= Yii::t('app/views', 'Unique users seen')?>"><?= $count?></span>
        </div>
    </div>
    <div class="panel-body text-center">
        <ul class="nav nav-list text-left">
            <?php foreach ($model as $user) : ?>
            <li class="row">
                <div class="col-lg-3 col-md-6">
                    <?= $user->idUser->getLinkAvatar(true, [], [100, 100])?>
                </div>
                <div class="col-lg-9 col-md-6 padding-left-0">
                    <?= Yii::$app->formatter->asDatetime($user->date) ?>
                </div>
            </li>
            <?php endforeach ?>
            <?php if($model) : ?>
                <?= yii\helpers\Html::a(Yii::t('app/views', 'Show All Viewers'), ['/user/user/show-viewers-entity', 'id' => $user->id_entity_view], ['class' => 'btn btn-primary']) ?>
            <?php endif ?>
            </script>
        </ul>    
    </div>
</div>