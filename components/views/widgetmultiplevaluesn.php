<?php
use kartik\select2\Select2;
use yii\helpers\Html;

$row = 0;//счетчик строк
?>
<?php if($title) : ?>
    <strong><?= $title ?></strong>
<?php endif ?>

<table class="table table-condensed" id="<?= $id_widget ?>">
    <tbody class="multiple-data">
        <tr>
            <?php foreach ($attrs as $key => $params) : ?>
                <td class="ctr"><?php if($showHeadTable) echo Html::label($model->getAttributeLabel($key)) ?></td>
            <?php endforeach ?>
            <td width="50px">
                <div class="btn btn-default" id="<?= $id_widget ?>" name="clone-multiple">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </div>
            </td>
        </tr>
        <?php foreach ($relation as $r) : ++$row; ?>
            <tr data-mid="<?= $row?>">
                <?php $j = 0; foreach ($attrs as $key => $params) : 
                    $j++;
                    $id_val = ($pk_value && isset($r[$pk_value])) ? $r[$pk_value] : $row;//существующее значение или порядковый номер
                ?>
                    <td>
                        <?php 
                        if(!isset($params['type'])) throw new yii\base\InvalidParamException('Widget multivalues requiered params type');//не хватает обязательных параметров
                        
                        if($params['type'] == 'text' || $params['type'] == 'number') {//текстовое поле(атрибут числовое)
                            echo Html::activeTextInput($model, $key.'['.$id_val.']', ['type' => $params['type'], 'class' => 'form-control val'.($j).'class', 'value' => isset($r->$key) ? $r->$key : '']); 
                        } elseif($params['type'] == 'dropdown') {//выпадающий список
                            echo Html::activeDropDownList($model, $key.'['.$id_val.']', $params['items'], ['type' => 'number', 'class' => 'form-control val'.($j).'class', 'value' => isset($r->$key) ? $r->$key : '']); 
                        }?>
                    </td>
                <?php endforeach ?>
                <td>
                    <div class="btn btn-danger remove-clone-multiple">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </div>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>
<?php $this->registerJs("setRemove();" . $addJs, yii\web\View::POS_END) ?>