<?php
use yii\helpers\Url;

$collapse = isset($_COOKIE[$cookieName]);

if($collapse) {
    $this->registerJs("
        $('#".$cookieName."').addClass('collapsed-box');
        $('button[data-id-widg=widgetcalendar]').click(function(){
            if($('#".$cookieName."').hasClass('collapsed-box')) {
                setTimeout(function(){ $('.fc-today-button').click() }, 1000);            
            }
        });
    ", \yii\web\View::POS_READY);
}?>
<div class="box box-success" id="<?=$cookieName?>">
    <div class="box-header with-border">
        <h3 class="box-title"><?php if($header) echo $header; else echo Yii::t('app', 'Calendar Events')?></h3>
        <?php if($mycalendar) : ?>
            <div class="box-tools pull-right">
                <a href="<?= Url::toRoute(['/intranet/events/create'])?>" class="label label-info" data-toggle="tooltip" data-placement="top" title="<?= Yii::t('app/models', 'Create New Event')?>"><?= Yii::t('app', 'Create')?></a>
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-id-widg="<?=$cookieName?>"><i class="fa fa-<?= ($collapse) ? 'chevron-up' : 'chevron-down'?>"></i></button>
            </div>
        <?php endif?>
    </div>
    <div class="box-body">
        <?= yii2fullcalendar\yii2fullcalendar::widget([
            'options' => [
              'lang' => 'ru',
            ],
            'header' => [
                'center'=>'title',
                'left'=>'prev,next today',        
                'right'=>'today month,agendaWeek,agendaDay',//,,agendaDay
            ],
            'ajaxEvents' => Url::to(['/calendar/jsoncalendar', 'id' => $iduser])
          ]);
        ?>
    </div>
</div>