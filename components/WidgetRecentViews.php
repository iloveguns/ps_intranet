<?php
/*
 * виджет вывода тех, кто просматривал
 */
namespace app\components;

use yii\base\Widget;
use app\models\EntityView;

class WidgetRecentViews extends Widget{
    private $model;
    public $limit = 10;//выводить последние n сотрудников
    public $className;//класс сущности
    public $item_id;//ид сущности
    protected $count = 0;

    public function init(){
        //просмотр сущности сотрудником
        if(\Yii::$app->user->id != 1){//меня не надо палить
            \app\models\EntityView::saveWatch($this->className, $this->item_id);
        }
        $this->model = EntityView::find()->where(['class' => $this->className, 'item_id' => $this->item_id])->orderBy('date DESC')->limit($this->limit)->all();
        
        $this->count = EntityView::find()->where(['class' => $this->className, 'item_id' => $this->item_id])->distinct('id_user')->count();
    }

    public function run(){
        return $this->render('widgetRecentViews', ['model' => $this->model, 'count' => $this->count]);
    }
}
?>