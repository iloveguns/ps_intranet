<?php
/*
 * виджет подписки/от писки
 */
namespace app\components;

use yii\base\Widget;
use Yii;

class WidgetSubscribe extends Widget{
    public $model;
    public $subscribed;
    
    public function init(){
        $this->subscribed = Yii::$app->db->createCommand('SELECT `fk_user` FROM `subscribes` WHERE class = :class AND item_id = :item_id AND fk_user = :fk_user')
        ->bindValue(':class', $this->model->getName())
        ->bindValue(':item_id', $this->model->getPrimaryKey())
        ->bindValue(':fk_user', Yii::$app->user->id)
        ->queryOne();
    }
    
    public function run(){
        return $this->render('widgetSubscribe', ['model' => $this->model, 'subscribed' => $this->subscribed]);
    }
}