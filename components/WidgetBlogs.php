<?php
/*
 * виджет отображения последних объявлений (всех)
 * $last - количество последних объявлений на вывод(5)
 */
namespace app\components;

use yii\base\Widget;
use app\modules\intranet\models\Blogs;
use yii\helpers\Url;

class WidgetBlogs extends Widget{
    public $limit;//количество последних объявлений
    public $where = '';//условие вывода http://www.yiiframework.com/doc-2.0/yii-db-query.html#where()-detail
    public $title;//заголовок виджета
    public $uniqname;//уникальное имя(для кук и урл)
    protected 
        $model, 
        $dependency,
        $url;//ссылка на посмотреть все
    
    /*
     * не могу понять, почему он 10 раз за каждый вызов делает запрос count
     */
    public function init(){
        if(!$this->limit) $this->limit = 10;
        
        $this->dependency = new \yii\caching\DbDependency();
        $this->dependency->sql = 'SELECT count(*) as c FROM '.Blogs::tableName();
        $this->model = Blogs::getDb()->cache(function () {
            return Blogs::find()->where(['status_blog' => Blogs::STATUS_PUBLISHED])->limit($this->limit)->orderBy('pk_blog DESC')->all();
        }, \Yii::$app->params['cacheTime'], $this->dependency);
        
        $this->url = Url::to(['/intranet/blogs/index']);
    }

    public function run(){
        return $this->render('widgetBlogs' ,['model' => $this->model, 'title' => $this->title, 'cookieName' => $this->uniqname, 'url' => $this->url, 'dependency' => $this->dependency]);
    }
}