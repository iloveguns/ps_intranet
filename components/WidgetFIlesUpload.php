<?php
/*
 * виджет загрузчика файлов bootstrap
 * http://demos.krajee.com/widget-details/fileinput
 * ajax загрузка с обновлением
 * + контроллер должен быть написан
 */
namespace app\components;

use yii\base\Widget;

class WidgetFIlesUpload extends Widget{
    public $model;//модель с файлами
    public $id;//ид модели
    public $name;//имя модели
    public $uploadAtSelect = true;//загружать сразу при выборе
    public $updateUrl = false;//обновлять ли после загрузки(PJAX)
    public $modelClass;//класс модели
    public $uploadCondition = true;//условие загрузки(вывода кнопки) ($model->canAdmin(Yii::$app->user->id) || Yii::$app->user->identity->isAdmin() автор или админ всея)

    public function init(){
        //if(!$this->uploadCondition) $this->uploadCondition = true;
    }

    public function run(){
        return $this->render('widgetFilesUpload', ['model' => $this->model, 'id' => $this->id, 'uploadAtSelect' => $this->uploadAtSelect, 'name' => $this->name, 'updateUrl' => $this->updateUrl, 'modelClass' => $this->modelClass, 'uploadCondition' => $this->uploadCondition]);
    }
}
?>