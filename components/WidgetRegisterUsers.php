<?php
/*
 * вывод последних зареганных сотрудников
 */
namespace app\components;

use yii\base\Widget;
use Yii;
use app\modules\user\models\User;

class WidgetRegisterUsers extends Widget{
    
    public $limit = 5;
    protected $model;
    private $cookiename = 'widgetregisterusers';
    private $dependency;//внутри виджета кеш
    
    public function init(){
        $this->dependency = [
            'class' => 'yii\caching\DbDependency',
            'sql' => 'SELECT count(id_user) FROM '.User::tableName(),
        ];
        
        $this->model = User::getDb()->cache(function ($db) {
            return User::getLastActive($this->limit);
        });
    }

    public function run(){
        return $this->render('widgetRegisterUsers' ,['model' => $this->model, 'cookieName' => $this->cookiename, 'dependency' => $this->dependency]);
    }
}
?>