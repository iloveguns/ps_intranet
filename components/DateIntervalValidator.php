<?php
namespace app\components;

use yii\validators\Validator;

/*
 * валидатор проверки 2х дат, чтоб 2 дату не выбирали меньше чем указано в настройках. например 2 дата минимум через 1 день после первой
 */
class DateIntervalValidator extends Validator
{
    public $first_date;//получить первую дату js
    protected $duration;


    public function init()
    {
        parent::init();
        $this->duration = \Yii::$app->params['minDurationTask'];
        $days = floor($this->duration/86400);//перевод в дни
        $this->message = 'Минимальная длительность - '.$days.'(дней)';
    }
    
    public function validateAttribute($model, $attribute)
    {
        /*
         * в принципе на клиенте хватит проверки
        $value = $model->$attribute;
        if ($value == 1) {
            $model->addError($attribute, $this->message);
        }*/
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $message = json_encode($this->message);
        //Invalid Date в Firefox, не может сделать дату из строки
        return <<<JS
function check_dateinterval(val) {
    var dateinterval_start_attr = new Date($('#$this->first_date').val().replace("-", " ", "g"));
    var dateinterval_start = (dateinterval_start_attr.getTime()/1000);
    
    var dateinterval_end_attr = new Date(val.replace("-", " ", "g"));
    var dateinterval_end = (dateinterval_end_attr.getTime()/1000);
    
    console.log(dateinterval_start,dateinterval_end);
    //if(dateinterval_end_attr == 'Invalid Date') return true;
    return (dateinterval_end-dateinterval_start > $this->duration);
}
if (!check_dateinterval(value)) { messages.push($message); }
JS;
    }
}