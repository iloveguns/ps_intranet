<?php
/*
 * формирование выдачи для календаря(задачи, проекты и всякое) в массив
 */
namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;
use Yii;

class WidgetCalendar extends Widget{
    
    public $toCalendar = [];//события для календаря
    public $iduser, $header;
    private $mycalendar = false;
    public $cookieName = 'widgetcalendar';

    public function init(){
        if(!$this->iduser) {//свой календарь, не пойму
            $this->mycalendar = true;
            $this->iduser = Yii::$app->user->id;
        } else if((int)$this->iduser === Yii::$app->user->id) { // заходит в свой
            $this->mycalendar = true;
        } else {
            $this->cookieName = false;
        }
    }

    public function run(){
        return $this->render('widgetCalendar' ,['toCalendar' => $this->toCalendar, 'iduser' => $this->iduser, 'mycalendar' => $this->mycalendar, 'header' => $this->header, 'cookieName' => $this->cookieName, 'mycalendar' => $this->mycalendar]);
    }
}
?>