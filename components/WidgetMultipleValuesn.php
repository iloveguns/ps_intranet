<?php
/*
 * много-много для указанных полей
 * сохранять в модели как обычно
 * 
 * параметры атрибутов type - 'text', 'number', 'dropdown'(обязательно $items[])
 */
namespace app\components;

class WidgetMultipleValuesn extends \yii\base\Widget{
    public $relation,//связь с данными
        $attrs,//атрибуты модели
        $title,//заголовок
        $addJs,//дополнительный js, который можно и отдельно зарегать, но ...
        $showHeadTable = true,//выводить ли надписи вверху таблицы
        $id_widget = false,//конкретный ид виджета
        $pk_value = null,//подставлять ид вместо чисел по порядку  в строке значенийы
        $model;//модель и параметры
    
    public function init(){
        if(!$this->relation) {
            $attrs = [];
            foreach ($this->attrs as $attr) {
                $attrs[] = $attr;
            }
            $this->relation = [$attrs];
        }
        
        $this->id_widget = ($this->id_widget) ? $this->id_widget : \Yii::$app->security->generateRandomString(8);//уникальный ид для нескольких виджетов
    }

    public function run(){
        return $this->render('widgetmultiplevaluesn', ['relation' => $this->relation, 'model' => $this->model, 'attrs' => $this->attrs, 'title' => $this->title, 'addJs' => $this->addJs, 'showHeadTable' => $this->showHeadTable, 'id_widget' => $this->id, 'pk_value' => $this->pk_value]);
    }
}
?>