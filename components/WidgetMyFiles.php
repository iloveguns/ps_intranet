<?php
/*
 * виджет загрузчика файлов bootstrap
 * http://demos.krajee.com/widget-details/fileinput
 * ajax загрузка с обновлением
 * + контроллер должен быть написан
 */
namespace app\components;

use yii\base\Widget;

class WidgetMyFiles extends Widget{
    public $model;//модель с файлами
    public $id = 0;//ид модели
    public $name = 'filestorage';//имя модели
    public $updateUrl = false;//обновлять ли после загрузки(PJAX)
    public $modelClass = 'FileStorage';//класс модели

    public function init(){
        $this->model = \app\models\UploadFiles::findAll(['class' => 'FileStorage', 'id_user' => \Yii::$app->user->id]);
    }

    public function run(){
        return $this->render('widgetMyFiles', ['model' => $this->model, 'id' => $this->id, 'name' => $this->name, 'updateUrl' => $this->updateUrl, 'modelClass' => $this->modelClass]);
    }
}
?>