<?php
namespace app\components;

use Yii;
use yii\helpers\Url;

class Controller extends \yii\web\Controller
{
    public function flash($type, $message) {
        Yii::$app->getSession()->setFlash($type=='error'?'danger':$type, $message);
    }

    public function back() {
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    public function jsonResponse() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    }
}
