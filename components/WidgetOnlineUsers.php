<?php
/*
 * виджет отображения людей онлайн(и не онлайн)
 * себя не выдает, если в одной вкладке обновлять, т.к. регистрация онлайна происходит после загрузки страницы
 * 
 * ['onlyOnline' => false] - всех. по умолчанию - онлайн
 */
namespace app\components;

use yii\base\Widget;
use app\models\UsersOnline;

class WidgetOnlineUsers extends Widget{
    
    public $onlyOnline = true;
    private $model;

    public function init(){
        $this->model = UsersOnline::find()->with('pkUser')->where(['now_online' => 1])->all();
    }

    public function run(){
        return $this->render('widgetOnlineUsers' ,['model' => $this->model, 'onlyOnline' => $this->onlyOnline]);
    }
}
?>