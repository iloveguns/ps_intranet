<?php
namespace app\components;

use yii\validators\Validator;
use app\models\FunctionModel;

/*
 * валидатор php даты
 * [['date_task'], 'app\components\DateValidator', 'min' => '-1 day'],
 */
class DateValidator extends Validator
{
    public $min;//уменьшение даты
    
    public function validateAttribute($model, $attribute) {
        $date = FunctionModel::dateModify(FunctionModel::getDateTimestamp(), $this->min);
        
        if($model->$attribute <= $date) {
            $model->addError($attribute, \Yii::t('app/models', 'Date does not required to min.'));
        }
    }
}