<?php
/*
 * виджет отображения задач всех сотрудника
 */
namespace app\components;

use yii\base\Widget;

class WidgetTasks extends Widget{
    
    protected $model;
    private $cookiename = 'widgettasks';

    public function run(){
        return $this->render('widgetTasks', ['cookieName' => $this->cookiename, 'cookie' => isset($_COOKIE['widget-task-sort']) ? $_COOKIE['widget-task-sort'] : NULL]);
    }
}
?>