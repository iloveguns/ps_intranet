<?php
/*
 * виджет отображения бывших сотрудников
 * (на основе онлайн виджета)
 */
namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;
use app\modules\user\models\User;
use app\modules\user\models\UserStatus;

class WidgetFormerUsers extends Widget{
    private $model, $dependency;

    public function init(){
        $this->dependency = [
            'class' => 'yii\caching\DbDependency',
            'sql' => 'SELECT count(*) FROM '. User::tableName(),
        ];
        
        $this->model = User::find()->joinWith('fkStatus')->where(['status' => UserStatus::FIRED])->all();
    }

    public function run(){
        return $this->render('widgetFormerUsers' ,['model' => $this->model, 'dependency' => $this->dependency]);
    }
}
?>