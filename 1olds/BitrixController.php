<?php

namespace app\commands;
/**
 * начат и недоделан, зарисовочки есть
 */
class BitrixController extends \yii\console\Controller {
    /*
     * выгрузка из битрикс24
     */
    public function actionCompany() {
        if (($handle = fopen("companies.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                
                $old_pk = $data[0];
                $name_company = $data[2];
                $phones = explode(', ', $data[4]);//crm_contacts_info
                $emails = explode(', ', $data[10]);//crm_contacts_info
                $responsible_user = $data[13];
                $address_other = $data[14];
                $fk_address_city = $data[15];
                $fk_address_country = 1;
                
                /* проверки */
                if(empty($address_other)) $address_other = NULL;
                
                if($responsible_user == 'Анастасия Лепина') {
                    $responsible_user = 506;
                } else if($responsible_user == 'Юлия Андрусенко') {
                    $responsible_user = 526;
                } else if($responsible_user == 'Наталья Плужникова') {
                    $responsible_user = 252;
                } else if($responsible_user == 'Лидия Баёва') {
                    $responsible_user = 98;
                }
                
                switch($fk_address_city) {
                    case 'Барнаул':
                        $fk_address_city = 1;
                        break;
                    case 'Санкт Петербург':
                    case 'Санкт-Петербург':
                        $fk_address_city = 159;
                        break;
                    case 'Москва':
                    case 'москва':
                    case 'Москва, Симферополь':
                    case 'Уфа,Москва':
                    case 'г. Москва, ул. Льва Толстого, д. 5/1, оф. 721':
                        $fk_address_city = 154;
                        break;
                    case 'Томбов':
                        $fk_address_city = 204;
                        break;
                    case 'Новосибирск':
                    case 'Новосибисрк':
                        $fk_address_city = 153;
                        break;
                    case 'Воронеж':
                        $fk_address_city = 205;
                        break;
                    case 'Хабаровск':
                        $fk_address_city = 206;
                        break;
                    case 'Казань':
                        $fk_address_city = 207;
                        break;
                    case 'Иркутск':
                        $fk_address_city = 208;
                        break;
                    case 'Пермь':
                    case 'пермь':
                    case 'Пермь (-2часа)':
                        $fk_address_city = 196;
                        break;
                    case 'Екатеринбург':
                        $fk_address_city = 162;
                        break;
                    case 'Кемерово':
                        $fk_address_city = 161;
                        break;
                    case 'Омск':
                        $fk_address_city = 179;
                        break;
                    case 'Томск':
                        $fk_address_city = 166;
                        break;
                    case 'Владивосток':
                    case 'владивосток':
                    case 'Владивосток, Хабаровск':
                        $fk_address_city = 155;
                        break;
                    case 'Краснодар':
                    case 'Краснодарский край':
                    case 'г. Краснодар':
                        $fk_address_city = 209;
                        break;
                    case 'Киров':
                        $fk_address_city = 210;
                        break;
                    case 'Нижний Новгород':
                        $fk_address_city = 195;
                        break;
                    case 'Ижевск':
                        $fk_address_city = 211;
                        break;
                    case 'Красноярск':
                        $fk_address_city = 165;
                        break;
                    case 'Бийск':
                        $fk_address_city = 5;
                        break;
                    case 'Крым':
                        $fk_address_city = 199;
                        break;
                    case 'Новокузнецк':
                        $fk_address_city = 187;
                        break;
                    case 'Тюмень':
                        $fk_address_city = 191;
                        break;
                    case 'Вологда':
                        $fk_address_city = 160;
                        break;
                    case 'Сургут':
                        $fk_address_city = 212;
                        break;
                    case 'Челябинск':
                    case 'г. Челябинск':
                        $fk_address_city = 152;
                        break;
                    case 'Новоалтайск':
                        $fk_address_city = 7;
                        break;
                    case 'Чита':
                        $fk_address_city = 213;
                        break;
                    case 'Белокуриха':
                        $fk_address_city = 4;
                        break;
                    case 'Благовещенск':
                        $fk_address_city = 214;
                        break;
                    case 'Абакан':
                        $fk_address_city = 215;
                        break;
                    case 'Мичуринск':
                        $fk_address_city = 216;
                        break;
                    case 'Бердск':
                        $fk_address_city = 217;
                        break;
                    case 'Саратов':
                    case 'г. Саратов, ул. Б.Казачья 98':
                        $fk_address_city = 218;
                        break;
                    case 'Петрозаводск':
                        $fk_address_city = 219;
                        break;
                    case 'курск':
                    case 'Курск':
                        $fk_address_city = 220;
                        break;
                    case 'Волгоград':
                        $fk_address_city = 221;
                        break;
                    case 'Рязань':
                        $fk_address_city = 222;
                        break;
                    case 'Белгород':
                        $fk_address_city = 223;
                        break;
                    case 'Самара':
                        $fk_address_city = 224;
                        break;
                    case 'Калуга':
                        $fk_address_city = 225;
                        break;
                    case 'Улан-Удэ':
                        $fk_address_city = 226;
                        break;
                    case 'Смоленск':
                        $fk_address_city = 227;
                        break;
                    case 'Усурийск':
                        $fk_address_city = 228;
                        break;
                    case 'Крымск':
                        $fk_address_city = 229;
                        break;
                    case 'Витебск':
                        $fk_address_city = 230;
                        break;
                    case 'Комсомольск-на-Амуре':
                        $fk_address_city = 231;
                        break;
                    case 'Ярославль':
                        $fk_address_city = 232;
                        break;
                    case 'Владимир':
                        $fk_address_city = 233;
                        break;
                    case 'Прокопьевск':
                        $fk_address_city = 189;
                        break;
                    case 'Одесса':
                        $fk_address_city = 234;
                        break;
                    case 'Солнечногорск':
                        $fk_address_city = 180;
                        break;
                    case 'Мытищи':
                        $fk_address_city = 235;
                        break;
                    case 'Находка':
                        $fk_address_city = 236;
                        break;
                    case 'Сочи':
                        $fk_address_city = 194;
                        break;
                    case 'Артем, Приморский край':
                    case 'Артем':
                        $fk_address_city = 237;
                        break;
                    case 'Уфа':
                        $fk_address_city = 238;
                        break;
                    case 'Архангельск':
                        $fk_address_city = 239;
                        break;
                    case 'Оренбург':
                        $fk_address_city = 240;
                        break;
                    case 'Тольяти':
                    case 'Тольятти':
                        $fk_address_city = 241;
                        break;
                    case 'Ростов-на-Дону':
                        $fk_address_city = 193;
                        break;
                    case 'Брянск':
                        $fk_address_city = 242;
                        break;
                    default:
                        $fk_address_city = NULL;
                }
                /*if(empty($fk_address_city) || $fk_address_city == 'Московская область' || $fk_address_city == 'Краснодарский край,' || $fk_address_city == 'нижегородская область'
                    || $fk_address_city == 'Брянская область' || $fk_address_city == 'Омская область' || $fk_address_city == 'Владимирская область Меленковский район село Ляхи ул. Кооперативная д.21'
                    || $fk_address_city == 'Костромская область' || $fk_address_city == 'Алтайский край' || $fk_address_city == 'Самарская область'
                ) {
                    $fk_address_city = NULL;
                }*/
                /* end проверки */
                
                //телефоны
                $dataarrp = [];
                foreach ($phones as $phone) {
                    $dataarrp[] = [NULL, 'workPhone', $phone, 1, NULL];
                    
                    /*Yii::$app->db->createCommand()->batchInsert('crm_contacts_info', ['pk_contacts_info','contact_field', 'contact_value','fk_crmcompany', 'fk_crmcontact'], 
                        $dataarr
                    )->execute();*/
                }
                
                //емейлы
                $dataarre = [];
                foreach ($emails as $email) {
                    $dataarre[] = [NULL, 'workEmail', $email, 1, NULL];
                    
                    /*Yii::$app->db->createCommand()->batchInsert('crm_contacts_info', ['pk_contacts_info','contact_field', 'contact_value','fk_crmcompany', 'fk_crmcontact'], 
                        $dataarr
                    )->execute();*/
                }
                
                //if($responsible_user != 506)
                var_dump($fk_address_city);
            }
            fclose($handle);
        }
    }
}
