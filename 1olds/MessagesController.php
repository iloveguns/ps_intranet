<?php

namespace app\commands;

/**
 * перенос старых сообщений в новые
 * ALTER TABLE `messages_dialog` DROP FOREIGN KEY `messages_dialog_fk_last_message`; ALTER TABLE `messages_dialog` ADD CONSTRAINT `messages_dialog_fk_last_message` FOREIGN KEY (`fk_last_message`) REFERENCES `messages_messages`(`pk_message`) ON DELETE SET NULL ON UPDATE RESTRICT;
 */
class MessagesController extends \yii\console\Controller {
    /**
     * сначала check
     * цель - удаление обертки от редактора <p>...</p> => <br>
     */
    public function actionIndex() {
        $counts = 0;
        
        foreach (\app\models\Messages::find()->orderBy('pk_message ASC')->asArray()->each() as $model) {
            $text = $model['text_message'];
            
            $senders = \Yii::$app->db->createCommand("SELECT `fk_dialog` FROM `messages_dialogs_users` WHERE `fk_user` = " . $model['sender'])->queryAll();
            $receivers = \Yii::$app->db->createCommand("SELECT `fk_dialog` FROM `messages_dialogs_users` WHERE `fk_user` = " . $model['receiver'])->queryAll();
            
            //очистка от тегов <p>, </p>, <span>, </span>, <br> - остается
            $string = preg_replace("/<p>/is", '', $text);
            $string = preg_replace("/<\/p>/is", '', $string);
            $string = preg_replace("/<\/span>/is", '', $string);
            $string = preg_replace("/<span>/is", '', $string);
            
            if(empty($senders) || empty($receivers)) {
                $fk_dialog = $this->createDialog($model);
            } else {
                $s = $r = [];
                
                foreach ($senders as $value) {
                    $s[$value['fk_dialog']] = $value['fk_dialog'];
                }
                
                foreach ($receivers as $value) {
                    $r[$value['fk_dialog']] = $value['fk_dialog'];
                }
                
                $fk_dialog = current(array_keys(array_intersect($s, $r)));
                
                if($fk_dialog === false) { // не нашлись общие диалоги
                    $fk_dialog = $this->createDialog($model);
                }
            }

            \Yii::$app->db->createCommand("INSERT INTO `messages_messages` VALUES (NULL, ".$fk_dialog.", ".$model['sender'].", '".$model['date']."', '".\yii\helpers\Html::encode($string)."', 1);")->execute();
            \Yii::$app->db->createCommand("UPDATE `messages_dialog` SET `fk_last_message` = (SELECT LAST_INSERT_ID()) WHERE `messages_dialog`.`pk_dialog` = ".$fk_dialog.";")->execute();
        }
    }
    
    /**
     * создание диалога и всего
     */
    public function createDialog($model) {
        \Yii::$app->db->createCommand("INSERT INTO `messages_dialog` VALUES (NULL, NULL, CURRENT_TIMESTAMP)")->execute();
        $fk_dialog = \Yii::$app->db->createCommand("SELECT `pk_dialog` FROM `messages_dialog` ORDER BY `pk_dialog` DESC LIMIT 1")->queryOne();
        $fk_dialog = $fk_dialog['pk_dialog'];

        \Yii::$app->db->createCommand("INSERT INTO `messages_dialogs_users` VALUES (NULL, ".$fk_dialog.", ".$model['sender'].")")->execute();
        \Yii::$app->db->createCommand("INSERT INTO `messages_dialogs_users` VALUES (NULL, ".$fk_dialog.", ".$model['receiver'].")")->execute();
        
        return $fk_dialog;
    }
    
    public function actionDate() {
        $all = \Yii::$app->db->createCommand("SELECT * FROM `messages_dialog`")->queryAll();
        
        foreach ($all as $m) {
            $date = \Yii::$app->db->createCommand("SELECT `create_date` FROM `messages_messages` WHERE `pk_message` = ".$m['fk_last_message'])->queryOne();
            $date = $date['create_date'];
            
            \Yii::$app->db->createCommand("UPDATE `messages_dialog` SET `create_date` = '".$date."' WHERE `pk_dialog` = ".$m['pk_dialog'])->execute();
        }
    }
}
