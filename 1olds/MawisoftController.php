<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\FunctionModel;
use app\modules\crm\models\CrmTasks;
use app\modules\crm\models\CrmContact;
use app\modules\crm\models\CrmCompany;
use app\modules\crm\models\CrmTags;
use app\modules\crm\models\CrmRelationTags;
use app\modules\crm\models\CrmContactsInfo;
use app\modules\crm\models\CrmDeals;

/**
 * где-то есть ошибка добавления в существующие компании, контакты при добавлении
 */
class MawisoftController extends Controller {    
    public $o_politsib = 4;//политсиб
    public $o_sokol = 14;//(сокол) прожектор
    public $o_avtograf = 20;//автограф
    public $o_rezon = 29;//резонанс

    public $kazcountry = [
        'Казахстан, Алматы','Казахстан','Павлодар','Атбасар','Казахстан, Алмата','Павлодар КЗ','Казахстан г. Усть-Каменогорск',
    ];


    public function __construct($id, $module, $config = []) {
        set_time_limit(0);        
        parent::__construct($id, $module, $config);
    }
    
    public function __destruct() {
        echo 'время выполнения: '.Yii::getLogger()->getElapsedTime().PHP_EOL;
    }
   
    //все клиенты - компании
    public function actionClients() {
        //Yii::$app->db->createCommand('DELETE FROM `crm_company` WHERE 1')->execute();
        //создание запроса на основании ид сотрудников
        $idUsers = [3209];
        $fk_department = $this->o_rezon;//ид отдела в интранете
        $listUsersInDeps = Yii::$app->db->createCommand('SELECT id_user FROM `user` WHERE `fk_department` = '.$fk_department)->queryAll();
        $listUsersInDep = [];//все ид сотрудников этого отдела
        foreach ($listUsersInDeps as $user) {
            $listUsersInDep[$user['id_user']] = $user['id_user'];
        }
        
        $sqlwh = $data = '';
        foreach ($idUsers as $id) {
            $crmuser = $this->switchuser($id);
            if(!$crmuser) continue;
        
            //$allIdsClients = Yii::$app->dbpg->createCommand('SELECT count(client_id) FROM client_user WHERE department_id = '.$otdel.' AND user_id = '.$id)->queryAll();//все клиенты
            $allIdsClients = Yii::$app->dbpg->createCommand('SELECT client_id FROM client_user WHERE user_id = '.$id)->queryAll();//все клиенты
            //var_dump($allIdsClients);die();
            
            foreach ($allIdsClients as $idClient) {
                $client = Yii::$app->dbpg->createCommand('
                    SELECT client.id, city.name AS cityname, client.comment, client.city AS address, client.name AS name, client.last_edit, client.last_report FROM client, city WHERE client.id = '.$idClient['client_id'].' AND city.id = client.city_id
                ')->queryOne();
                
                //может и не быть
                $clientevent = Yii::$app->dbpg->createCommand('
                    SELECT event.date AS create_date, event.user_id AS created_user FROM event WHERE owner_id = '.$idClient['client_id'].' AND event.event_type_id = 601 
                ')->queryOne();
                
                echo '('.$client['id'].')  ';
                                
                if(!$clientevent) {
                    $clientevent['create_date'] = NULL;
                    $clientevent['created_user'] = NULL;
                }
                
                $insert = Yii::$app->db->createCommand()->insert(CrmCompany::tableName(), [
                    'old_pkcompany' => $client['id'],
                    'responsible_user' => $crmuser,
                    'name_company' => $client['name'],
                    'comment_company' => $client['comment'],
                    'fk_address_country' => (in_array($client['cityname'], $this->kazcountry)) ? 2 : 1,
                    'fk_address_city' => $this->switchCity($client['cityname'], $client['id']),
                    //'fk_address_street' => '',
                    'address_other' => $client['address'],
                    //'birthdate_company' => '',
                    'created_user' => $this->switchuser($clientevent['created_user'], true),
                    'create_date' => $clientevent['create_date'],
                    'updated_user' => $crmuser,//неизвестно где искать
                    'update_date' => $client['last_edit'],                    
                    'fk_department' => $fk_department,
                ])->execute();
                
                
                if($insert) {//вставлено
                    $newIdCompany = Yii::$app->db->createCommand('SELECT pk_company FROM '.CrmCompany::tableName().' WHERE old_pkcompany = '.$client['id'])->queryOne();
                    $newIdCompany = $newIdCompany['pk_company'];
                    
                    //контактные лица
                    $clientContacts = Yii::$app->dbpg->createCommand('SELECT id, name, "comment", "position", surname FROM person WHERE client_id = '.$client['id'])->queryAll();//контакты(лица) // department_id - принадлежност к отделу
                    foreach ($clientContacts as $clientContact) {
                        echo $clientContact['id'].' ';
                        
                        Yii::$app->db->createCommand()->insert(CrmContact::tableName(), [
                            'name_contact' => ($clientContact['surname']) ? $clientContact['name'] .' '. $clientContact['surname'] : $clientContact['name'],
                            'old_pkcontact' => $clientContact['id'],
                            'comment_contact' => ($clientContact['comment']) ? $clientContact['comment'] : NULL,
                            'post_contact' => ($clientContact['position']) ? $clientContact['position'] : NULL,
                            'responsible_user' => $crmuser,
                            'fk_crm_company' => $newIdCompany,
                            'fk_department' => $fk_department,
                        ])->execute();
                        
                        $newIdContact = Yii::$app->db->createCommand('SELECT pk_contact FROM '. CrmContact::tableName().' WHERE old_pkcontact = '.$clientContact['id'])->queryOne();
                        $newIdContact = $newIdContact['pk_contact'];
                        
                        //контактная информация
                        $dataarr = [];
                        $contacts = Yii::$app->dbpg->createCommand('SELECT "type", info FROM contact WHERE owner_id = '.$clientContact['id'].' AND owner_name = \'person\';')->queryAll();
                        foreach ($contacts as $contact) {
                            $dataarr[] = [$this->switchContacts($contact['type']), $contact['info'], $newIdContact];
                        }
                        if(!empty($dataarr)){//если есть что вставлять
                            Yii::$app->db->createCommand()->batchInsert(CrmContactsInfo::tableName(), ['contact_field', 'contact_value','fk_crmcontact'], 
                                $dataarr
                            )->execute();
                        }
                        //end контактная информация
                    }
                    echo PHP_EOL;
                    //end контактные лица
                    
                    //контактная информация
                    $dataarr = [];
                    $contacts = Yii::$app->dbpg->createCommand('SELECT "type", info FROM contact WHERE owner_id = '.$client['id'].' AND owner_name = \'client\';')->queryAll();
                    
                    foreach ($contacts as $contact) {
                        $dataarr[] = [$this->switchContacts($contact['type']), $contact['info'], $newIdCompany];
                    }
                    if(!empty($dataarr)){//если есть что вставлять
                        Yii::$app->db->createCommand()->batchInsert(CrmContactsInfo::tableName(), ['contact_field', 'contact_value','fk_crmcompany'], 
                            $dataarr
                        )->execute();
                    }
                    //end контактная информация
                    
                    //задачи+отчеты
                    $dataarr = [];
                    $tasks = Yii::$app->dbpg->createCommand('SELECT id, message, date, user_id, report_id, event_type_id FROM event WHERE owner_id = '.$idClient['client_id'].' AND owner_name = \'client\' AND event_type_id != 641 AND event_type_id != 550 AND event_type_id != 560 AND event_type_id != 562 AND event_type_id != 1000 AND event_type_id != 601 AND event_type_id != 530 AND event_type_id != 531 AND event_type_id != 540 AND event_type_id != 620  ORDER BY id;')->queryAll();
                    
                    foreach ($tasks as $task) {
                        if($task['message'] == 'движение по сделке') continue;//создан контакт то есть
                        
                        $st = CrmTasks::STATUS_WORK;//статус начальный
                        $report = [];
                        
                        echo 'task:'.$task['id'];
                        
                        if($task['report_id']){//есть отчет
                            $report = Yii::$app->dbpg->createCommand('SELECT message, date, success FROM report WHERE id = '.$task['report_id'])->queryOne();
                            if($report['success'] == 't'){
                                $st = CrmTasks::STATUS_DONE;
                            } else {
                                $st = CrmTasks::STATUS_LATE;
                            }
                        } else {
                            $report['date'] = $report['message'] = NULL;
                        }
                        
                        $t = substr($report['date'], 11, 5);
                        if(empty($t)) $t = '18:00';//не указано время

                        echo PHP_EOL;
                        
                        $responsible_task_user = $this->switchuser((int)$task['user_id']);
                        if(in_array($responsible_task_user, $listUsersInDep)){//только если совпадает с ид сотрудников из этого отдела
                            $dataarr[] = [//набить в массив для множественной вставки
                                substr($task['date'], 0, 10),
                                $t,
                                $responsible_task_user,
                                $this->switchEventType($task['event_type_id'], $fk_department),
                                $newIdCompany,
                                $this->remove_emoji($task['message']),
                                $task['date'],
                                $report['date'],
                                $st,
                                $this->remove_emoji($report['message']),
                                $fk_department
                            ];                        
                        }
                    }                    
                    if(!empty($dataarr)){//если есть что вставлять
                        Yii::$app->db->createCommand()->batchInsert(CrmTasks::tableName(), ['date_task', 'time_task','fk_user_task', 'fk_typetask','fk_crmcompany', 'text_task', 'create_date', 'update_date', 'status_task', 'status_text', 'fk_department'], 
                            $dataarr
                        )->execute();
                    }
                    //end задачи+отчеты
                    
                    //сделки+
                    $dataarr = [];
                    $deals = Yii::$app->dbpg->createCommand('SELECT date, closed, description, summ, responsible_user_id FROM proposal WHERE client_id = '.$client['id'])->queryAll();
                    
                    foreach ($deals as $deal) {//UPDATE `crm_deals` SET `responsible_user`=92 WHERE responsible_user IN(291,27)
                        if($deal['closed'] == false) $status_deal = 2;//не закрыта
                        else $status_deal = 6;//закрыто(успех)
                        
                        if(empty($deal['description'])) $deal['description'] = 'Без имени';
                        
                        echo 'Сделка создана ';
                        
                        $dataarr[] = [$deal['description'], $deal['summ'], $status_deal, $newIdCompany, $deal['date'], $this->switchuser((int)$deal['responsible_user_id']), $this->switchuser((int)$deal['responsible_user_id']), $fk_department];
                    }
                    if(!empty($dataarr)){//если есть что вставлять
                        Yii::$app->db->createCommand()->batchInsert(CrmDeals::tableName(), ['name_deal', 'price_deal','status_deal', 'fk_crmcompany','create_date', 'responsible_user', 'created_user', 'fk_department'], 
                            $dataarr
                        )->execute();
                    }
                    //end сделки
                }
                echo $insert.PHP_EOL;
            }
            echo 'Сотрудник '.$crmuser.' завершен'.PHP_EOL;
        }
    }
    
    public function actionClientsother() {
        //создание запроса на основании ид сотрудников
        $idUsers = [4003, 4001, 3351, 3607];////сотрудники отдела //[3212, 4306] - политсиб, [3553, 2156, 3008, 2201, 3703, 2151, 4300] - сокол, [4250, 4350, 3204, 3608, 3902, 4000] - автограф
        $otdel = 400;
        $fk_department = $this->o_sokol;//ид отдела в интранете
        $listUsersInDeps = Yii::$app->db->createCommand('SELECT id_user FROM `user` WHERE `fk_department` = '.$fk_department)->queryAll();
        $listUsersInDep = [];//все ид сотрудников этого отдела
        foreach ($listUsersInDeps as $user) {
            $listUsersInDep[$user['id_user']] = $user['id_user'];
        }
        
        $crmuser = 92;//кому в архив
        
        $sqlwh = $data = '';        
        //$allIdsClients = Yii::$app->dbpg->createCommand('SELECT count(client_id) FROM client_user WHERE department_id = '.$otdel.' AND user_id = '.$id)->queryAll();//все клиенты
        $allIdsClients = Yii::$app->dbpg->createCommand('SELECT client_id FROM client_user WHERE user_id NOT IN ('. implode(', ', $idUsers).') AND department_id = '.$otdel)->queryAll();//все клиенты
        //var_dump('SELECT client_id FROM client_user WHERE user_id NOT IN ('. implode(', ', $idUsers).') AND department_id = '.$otdel);die();

        foreach ($allIdsClients as $idClient) {                
            $client = Yii::$app->dbpg->createCommand('
                SELECT client.id, city.name AS cityname, client.comment, client.city AS address, client.name AS name, client.last_edit, client.last_report FROM client, city WHERE client.id = '.$idClient['client_id'].' AND city.id = client.city_id
            ')->queryOne();

            //может и не быть
            $clientevent = Yii::$app->dbpg->createCommand('
                SELECT event.date AS create_date, event.user_id AS created_user FROM event WHERE owner_id = '.$idClient['client_id'].' AND event.event_type_id = 601 
            ')->queryOne();

            echo '('.$client['id'].')  ';

            if(!$clientevent) {
                $clientevent['create_date'] = NULL;
                $clientevent['created_user'] = NULL;
            }

            $insert = Yii::$app->db->createCommand()->insert(CrmCompany::tableName(), [
                'old_pkcompany' => $client['id'],
                'responsible_user' => $crmuser,
                'name_company' => $client['name'],
                'comment_company' => $client['comment'],
                'fk_address_country' => (in_array($client['cityname'], $this->kazcountry)) ? 2 : 1,
                'fk_address_city' => $this->switchCity($client['cityname'], $client['id']),
                //'fk_address_street' => '',
                'address_other' => $client['address'],
                //'birthdate_company' => '',
                'created_user' => $this->switchuser($clientevent['created_user'], true),
                'create_date' => $clientevent['create_date'],
                'updated_user' => $crmuser,//неизвестно где искать
                'update_date' => $client['last_edit'],                    
                'fk_department' => $fk_department,
                'stock' => 1,
            ])->execute();


            if($insert) {//вставлено
                $newIdCompany = Yii::$app->db->createCommand('SELECT pk_company FROM '.CrmCompany::tableName().' WHERE old_pkcompany = '.$client['id'])->queryOne();
                $newIdCompany = $newIdCompany['pk_company'];

                //контактные лица
                $clientContacts = Yii::$app->dbpg->createCommand('SELECT id, name, "comment", "position", surname FROM person WHERE client_id = '.$client['id'])->queryAll();//контакты(лица) // department_id - принадлежност к отделу
                foreach ($clientContacts as $clientContact) {
                    echo $clientContact['id'].' ';

                    Yii::$app->db->createCommand()->insert(CrmContact::tableName(), [
                        'name_contact' => ($clientContact['surname']) ? $clientContact['name'] .' '. $clientContact['surname'] : $clientContact['name'],
                        'old_pkcontact' => $clientContact['id'],
                        'comment_contact' => ($clientContact['comment']) ? $clientContact['comment'] : NULL,
                        'post_contact' => ($clientContact['position']) ? $clientContact['position'] : NULL,
                        'responsible_user' => $crmuser,
                        'fk_crm_company' => $newIdCompany,
                        'fk_department' => $fk_department,
                        'stock' => 1,
                    ])->execute();

                    $newIdContact = Yii::$app->db->createCommand('SELECT pk_contact FROM '. CrmContact::tableName().' WHERE old_pkcontact = '.$clientContact['id'])->queryOne();
                    $newIdContact = $newIdContact['pk_contact'];

                    //контактная информация
                    $dataarr = [];
                    $contacts = Yii::$app->dbpg->createCommand('SELECT "type", info FROM contact WHERE owner_id = '.$clientContact['id'].' AND owner_name = \'person\';')->queryAll();
                    foreach ($contacts as $contact) {
                        $dataarr[] = [$this->switchContacts($contact['type']), $contact['info'], $newIdContact];
                    }
                    if(!empty($dataarr)){//если есть что вставлять
                        Yii::$app->db->createCommand()->batchInsert(CrmContactsInfo::tableName(), ['contact_field', 'contact_value','fk_crmcontact'], 
                            $dataarr
                        )->execute();
                    }
                    //end контактная информация
                }
                echo PHP_EOL;
                //end контактные лица

                //контактная информация
                $dataarr = [];
                $contacts = Yii::$app->dbpg->createCommand('SELECT "type", info FROM contact WHERE owner_id = '.$client['id'].' AND owner_name = \'client\';')->queryAll();

                foreach ($contacts as $contact) {
                    $dataarr[] = [$this->switchContacts($contact['type']), $contact['info'], $newIdCompany];
                }
                if(!empty($dataarr)){//если есть что вставлять
                    Yii::$app->db->createCommand()->batchInsert(CrmContactsInfo::tableName(), ['contact_field', 'contact_value','fk_crmcompany'], 
                        $dataarr
                    )->execute();
                }
                //end контактная информация

                //задачи+отчеты
                $dataarr = [];
                $tasks = Yii::$app->dbpg->createCommand('SELECT id, message, date, user_id, report_id, event_type_id FROM event WHERE owner_id = '.$idClient['client_id'].' AND owner_name = \'client\' AND event_type_id != 641 AND event_type_id != 550 AND event_type_id != 560 AND event_type_id != 562 AND event_type_id != 1000 AND event_type_id != 601 AND event_type_id != 530 AND event_type_id != 531 AND event_type_id != 540 ORDER BY id;')->queryAll();

                foreach ($tasks as $task) {
                    if($task['message'] == 'движение по сделке') continue;//создан контакт то есть

                    $st = CrmTasks::STATUS_WORK;//статус начальный
                    $report = [];

                    echo 'task:'.$task['id'];

                    if($task['report_id']){//есть отчет
                        $report = Yii::$app->dbpg->createCommand('SELECT message, date, success FROM report WHERE id = '.$task['report_id'])->queryOne();
                        if($report['success'] == 't'){
                            $st = CrmTasks::STATUS_DONE;
                        } else {
                            $st = CrmTasks::STATUS_LATE;
                        }
                    } else {
                        $report['date'] = $report['message'] = NULL;
                    }

                    $t = substr($report['date'], 11, 5);
                    if(empty($t)) $t = '18:00';//не указано время

                    echo PHP_EOL;

                    $responsible_task_user = $this->switchuser((int)$task['user_id']);
                    if(in_array($responsible_task_user, $listUsersInDep)){//только если совпадает с ид сотрудников из этого отдела
                        $dataarr[] = [//набить в массив для множественной вставки
                            substr($task['date'], 0, 10),
                            $t,
                            $responsible_task_user,
                            $this->switchEventType($task['event_type_id'], $fk_department),
                            $newIdCompany,
                            $this->remove_emoji($task['message']),
                            $task['date'],
                            $report['date'],
                            $st,
                            $this->remove_emoji($report['message']),
                            $fk_department
                        ];                        
                    }
                }                    
                if(!empty($dataarr)){//если есть что вставлять
                    Yii::$app->db->createCommand()->batchInsert(CrmTasks::tableName(), ['date_task', 'time_task','fk_user_task', 'fk_typetask','fk_crmcompany', 'text_task', 'create_date', 'update_date', 'status_task', 'status_text', 'fk_department'], 
                        $dataarr
                    )->execute();
                }
                //end задачи+отчеты

                //сделки+
                $dataarr = [];
                $deals = Yii::$app->dbpg->createCommand('SELECT date, closed, description, summ, responsible_user_id FROM proposal WHERE client_id = '.$client['id'])->queryAll();

                foreach ($deals as $deal) {//UPDATE `crm_deals` SET `responsible_user`=92 WHERE responsible_user IN(291,27)
                    if($deal['closed'] == false) $status_deal = 2;//не закрыта
                    else $status_deal = 6;//закрыто(успех)

                    if(empty($deal['description'])) $deal['description'] = 'Без имени';

                    echo 'Сделка создана ';

                    $dataarr[] = [$deal['description'], $deal['summ'], $status_deal, $newIdCompany, $deal['date'], $this->switchuser((int)$deal['responsible_user_id']), $this->switchuser((int)$deal['responsible_user_id']), $fk_department];
                }
                if(!empty($dataarr)){//если есть что вставлять
                    Yii::$app->db->createCommand()->batchInsert(CrmDeals::tableName(), ['name_deal', 'price_deal','status_deal', 'fk_crmcompany','create_date', 'responsible_user', 'created_user', 'fk_department'], 
                        $dataarr
                    )->execute();
                }
                //end сделки
            }
            echo $insert.PHP_EOL;
        }
    }
        
    private function remove_emoji($text){
        return preg_replace('/([0-9|#][\x{20E3}])|[\x{00ae}|\x{00a9}|\x{203C}|\x{2047}|\x{2048}|\x{2049}|\x{3030}|\x{303D}|\x{2139}|\x{2122}|\x{3297}|\x{3299}][\x{FE00}-\x{FEFF}]?|[\x{2190}-\x{21FF}][\x{FE00}-\x{FEFF}]?|[\x{2300}-\x{23FF}][\x{FE00}-\x{FEFF}]?|[\x{2460}-\x{24FF}][\x{FE00}-\x{FEFF}]?|[\x{25A0}-\x{25FF}][\x{FE00}-\x{FEFF}]?|[\x{2600}-\x{27BF}][\x{FE00}-\x{FEFF}]?|[\x{2900}-\x{297F}][\x{FE00}-\x{FEFF}]?|[\x{2B00}-\x{2BF0}][\x{FE00}-\x{FEFF}]?|[\x{1F000}-\x{1F6FF}][\x{FE00}-\x{FEFF}]?/u', '', $text);
    }
    
    /*
     * выгрузка клиентов(контактов компаний) в настоящий интранет
     * old_pk
     */
    public function actionSql() {//DELETE FROM `crm_company` WHERE `fk_department` = 14
        //die();
        $newtcomp = 'crm_company';
        $newtcont = 'crm_contact';
        $otdel = $this->o_sokol;//переносимый отдел
        
        //вброс компаний
        foreach (CrmCompany::find()->asArray()->batch(100) as $datas) {
            $dataarr = [];
            foreach ($datas as $data) {                    
                $data['old_pk'] = $data['pk_company'];
                $data['pk_company'] = NULL;//убрать пк
                $dataarr[] = $data;
            }
            Yii::$app->dbi->createCommand()->batchInsert($newtcomp, ['pk_company', 'old_pk', 'old_pkcompany', 'responsible_user','name_company', 'comment_company', 'fk_address_country', 'fk_address_city', 'fk_address_street', 'address_other', 'birthdate_company', 'created_user', 'create_date', 'update_date', 'updated_user', 'fk_department', 'stock'], 
                $dataarr
            )->execute();
        }
        echo 'Компании прошли'.PHP_EOL;
        
        //вброс контактов
        foreach (CrmContact::find()->asArray()->batch(100) as $datas) {
            $dataarr = [];
            foreach ($datas as $data) {
                $newid = Yii::$app->dbi->createCommand('SELECT `pk_company` FROM '.$newtcomp.' WHERE `old_pk` = '.$data['fk_crm_company'].' AND fk_department = '.$otdel)->queryOne();
                $newidcomp = $newid['pk_company'];
                $data['old_pk'] = $data['pk_contact'];
                $data['pk_contact'] = NULL;//убрать пк
                $data['fk_crm_company'] = $newidcomp;
                $dataarr[] = $data;
            }
            Yii::$app->dbi->createCommand()->batchInsert($newtcont, ['pk_contact', 'old_pk', 'old_pkcontact', 'name_contact','comment_contact', 'post_contact', 'birthdate_contact', 'fk_crm_company', 'created_user', 'create_date', 'update_date', 'updated_user', 'fk_department', 'responsible_user', 'stock'], 
                $dataarr
            )->execute();
        }
        echo 'Контакты прошли'.PHP_EOL;
        
        //вброс контактной инфы
        foreach (\app\modules\crm\models\CrmContactsInfo::find()->asArray()->batch(100) as $datas) {
            $dataarr = [];
            $sql = '';
            foreach ($datas as $data) {
                if($data['fk_crmcompany'] === NULL){
                    $newid = Yii::$app->dbi->createCommand('SELECT `pk_contact` FROM '.$newtcont.' WHERE `old_pk` = '.$data['fk_crmcontact'].' AND fk_department = '.$otdel)->queryOne();
                    $newid = $newid['pk_contact'];
                    $data['fk_crmcontact'] = $newid;
                } else {
                    $newid = Yii::$app->dbi->createCommand('SELECT `pk_company` FROM '.$newtcomp.' WHERE `old_pk` = '.$data['fk_crmcompany'].' AND fk_department = '.$otdel)->queryOne();
                    $newid = $newid['pk_company'];
                    $data['fk_crmcompany'] = $newid;
                }
                $data['pk_contacts_info'] = NULL;
                $dataarr[] = $data;
            }

            Yii::$app->dbi->createCommand()->batchInsert('crm_contacts_info', ['pk_contacts_info','contact_field', 'contact_value','fk_crmcompany', 'fk_crmcontact'], 
                $dataarr
            )->execute();
        }
        echo 'Контактная инфа +'.PHP_EOL;
        
        //вброс задач
        foreach (CrmTasks::find()->asArray()->batch(100) as $datas) {
            $dataarr = [];
            $sql = '';
            foreach ($datas as $data) {
                $newid = '';
                if($data['fk_crmcompany'] === NULL){
                    $newid = Yii::$app->dbi->createCommand('SELECT `pk_contact` FROM '.$newtcont.' WHERE `old_pk` = '.$data['fk_crmcontact'].' AND fk_department = '.$otdel)->queryOne();
                    $newid = $newid['pk_contact'];
                    $data['fk_crmcontact'] = $newid;
                } else {
                    $newid = Yii::$app->dbi->createCommand('SELECT `pk_company` FROM '.$newtcomp.' WHERE `old_pk` = '.$data['fk_crmcompany'].' AND fk_department = '.$otdel)->queryOne();
                    $newid = $newid['pk_company'];
                    $data['fk_crmcompany'] = $newid;
                }
                $data['pk_task'] = NULL;
                $dataarr[] = $data;
            }
            
            Yii::$app->dbi->createCommand()->batchInsert('crm_tasks', ['pk_task','date_task', 'time_task', 'created_user', 'fk_user_task', 'fk_typetask', 'fk_crmcontact', 'fk_crmcompany', 'text_task', 'create_date', 'update_date', 'updated_user', 'status_task', 'status_text', 'result_task', 'fk_task_result', 'fk_department'], 
                $dataarr
            )->execute();
        }
        echo 'Задачи прошли'.PHP_EOL;
        
        //вброс сделок
        foreach (CrmDeals::find()->asArray()->batch(100) as $datas) {
            $dataarr = [];
            $sql = '';
            foreach ($datas as $data) {
                if($data['fk_crmcompany'] === NULL){
                    $newid = Yii::$app->dbi->createCommand('SELECT `pk_contact` FROM '.$newtcont.' WHERE `old_pk` = '.$data['fk_crmcontact'].' AND fk_department = '.$otdel)->queryOne();
                    $newid = $newid['pk_contact'];
                    $data['fk_crmcontact'] = $newid;
                } else {
                    $newid = Yii::$app->dbi->createCommand('SELECT `pk_company` FROM '.$newtcomp.' WHERE `old_pk` = '.$data['fk_crmcompany'].' AND fk_department = '.$otdel)->queryOne();
                    $newid = $newid['pk_company'];
                    $data['fk_crmcompany'] = $newid;
                }
                $data['pk_deal'] = NULL;                
                $dataarr[] = $data;
            }
            Yii::$app->dbi->createCommand()->batchInsert('crm_deals', ['pk_deal','name_deal','price_deal','status_deal','fk_crmcontact','fk_crmcompany','create_date','responsible_user','created_user','update_date','updated_user','status_date','fk_department'], 
                $dataarr
            )->execute();
        }
        echo 'Сделки прошли'.PHP_EOL;
    }
    
    //определить город из этой каши
    public function switchCity($str, $idclient) {
        echo $str . PHP_EOL;
        $city = NULL;
        switch ($str) {
            case 'Барнаул' :
            case 'барнаул' : 
            case 'Офис в Барнауле' : 
            case 'Барнул' :
            case 'г. Барнаул' :
            case 'Баранул' : 
            case 'г.Барнаул' :
            case 'Алтайский край\Барнаул' :
                $city = 1;
                break;
            
            case 'Бийск' :
            case 'г.Бийск' :
            case 'Алтайский край\Бийск' :
                $city = 5;
                break;
            
            case 'Челябинск' :
            case 'г. Челябинск' :
                $city = 152;
                break;
            
            case 'Белокуриха' :
            case 'г. Белокуриха' : 
                $city = 4;
                break;
            
            case 'Новосибирск' :
            case 'Нсб' : 
                $city = 153;
                break;
            
            case 'с. Майма' :
            case 'с. Майма, Республика Алтай' :
            case 'Майма' : 
                $city = 129;
                break;
            
            case 'Москва' :
                $city = 154;
                break;
            
            case 'Владивосток' :
                $city = 155;
                break;
            
            case 'Волчихинский район с.Волчиха' :
            case 'Волчиха' : 
                $city = 156;
                break;
            
            case 'Новоалтайск' :
            case 'Алтайский край\Новоалтайск' : 
                $city = 7;
                break;
            
            case 'Зональный район, с. Соколово' :
                $city = 157;
                break;
            
            case 'Поспелиха' :
                $city = 118;
                break;
            
            case 'Алтайский край\Славгород' :
            case 'Славгород' :
                $city = 9;
                break;
            
            case 'ННР, с. Гальбштадт' :
                $city = 158;
                break;
            
            case 'г. Санкт-Петербург' :
            case 'Санкт - Петербург' :
                $city = 159;
                break;
            
            case 'г. Вологда' :
                $city = 160;
                break;
            
            case 'Кемерово' :
            case 'Другие города\Кемерово' :
                $city = 161;
                break;
            
            case 'Екатеринбург' :
            case 'г. Екатеринбург' :
                $city = 162;
                break;
            
            case 'г.Рубцовск' :
            case 'Руцовск' : 
            case 'Алтайский край,г.Рубцовск' :
            case 'Рубцовск' :
            case 'Алтайский край\Рубцовск' :
                $city = 8;
                break;
            
            case 'Залесовский район, с. Залесово' :
            case 'Залесово' : 
                $city = 163;
                break;
            
            case 'Камень-на-Оби' :
            case 'Камень-на-оби' :
            case 'г.Камень-на-Оби' :
            case 'Камень-на Оби' : 
            case 'Камень - на -Оби' : 
            case 'Алтайский край\Камень-на-Оби' :
                $city = 6;
                break;
            
            case 'с. Кулунда' :
                $city = 164;
                break;
            
            case 'Горно-Алтайск' :
            case 'г.Горно-Алтайск' :
                $city = 2;
                break;
            
            case 'Красноярск' :
                $city = 165;
                break;
            
            case 'Заринск' :
            case 'г.Заринск' :
                $city = 10;
                break;
            
            case 'с. Алтайское' :
            case 'Алтайское' : 
                $city = 68;
                break;
            
            case 'Павловск' :
            case 'с. Павловск' :
            case 2152 :
                $city = 28;
                break;
            
            case 'Краснощековский район, с. Краснощеково' :
                $city = 130;
                break;
            
            case 'Шелаболихинский р-н, с.Шелаболиха' :
                $city = 79;
                break;
            
            case 'Романово' :
                $city = 80;
                break;
            
            case 'Томск' :
                $city = 166;
                break;
            
            case 'Омск' :
                $city = 179;
                break;
            
            case 'село Калманка' :
                $city = 115;
                break;
            
            case 'Славгородский р-н, с. Знаменка' :
                $city = 167;
                break;
            
            case 'с.Родино' :
                $city = 168;
                break;
            
            case 'Яровое' :
            case 'г.Яровое' :
            case 'г. Яровое' :
                $city = 11;
                break;
            
            case 'Новоегорьевское' : 
                $city = 169;
                break;
            
            case 'г. Горняк' : 
            case 'Г. Горняк' :
                $city = 170;
                break;
            
            case 'Змеиногорск' :
            case 'г.Змеиногорск' :
            case 'Алтайский край,г.Змеиногорск' : 
                $city = 171;
                break;
            
            case 'с. Красный Партизан' :
                $city = 172;
                break;
            
            case 'Благовещенка' :
            case 'р.п.Благовещенка' : 
            case 'Благовещенский район, р.п. Благовещенка' :
                $city = 173;
                break;
            
            case 'село Малый Бащелак' :
                $city = 174;
                break;
            
            case 'Павлодар' :
            case 'павлодар' : 
            case 'Павлодар КЗ' :
                $city = 175;
                break;
            
            case 'Атбасар' :
                $city = 176;
                break;
            
            case 'село Устьянка' :
                $city = 177;
                break;
            
            case ', с. Быстрый исток' :
            case 'Быстроистокский район, с. Быстрый исток' : 
                $city = 178;
                break;
            
            case 'г. Солнечногорск' :
                $city = 180;
                break;
            
            case 'Междуреченск' :
                $city = 181;
                break;
            
            case 'Исилькуль' :
                $city = 182;
                break;
            
            case 'Казахстан, Алмата' :
            case 'Казахстан, Алматы' :
                $city = 183;
                break;
            
            case 'Алейск' :
                $city = 3;
                break;
            
            case 'с. Топчиха' :
                $city = 98;
                break;
            
            case 'Бийский район, с. Верх-Катунское' :
                $city = 184;
                break;
            
            case 'Алтайский край,Смоленский район,с.Солоновка' :
                $city = 185;
                break;
            
            case 'с. Хабары' :
                $city = 137;
                break;
            
            case 'Немецкий р-н, с.Шумановка' :
                $city = 186;
                break;
            
            case 'Новокузнецк' :
                $city = 187;
                break;
            
            case 'Ребриха' :
                $city = 51;
                break;
            
            case 'с. Енисейское, Бийский р-н' :
                $city = 188;
                break;
            
            case 'Прокопьевск' :
                $city = 189;
                break;
            
            case 'с. Веселоярск' :
            case 'Рубцовский р-н с.Веселоярск' :
                $city = 190;
                break;
            
            case 'Тюмень' :
                $city = 191;
                break;
            
            case 'село Новороманово' :
                $city = 78;
                break;
            
            case 'Казахстан г. Усть-Каменогорск' :
                $city = 191;
                break;
            
            case 'Ростов на Дону' :
            case 'Ростов-на-Дону' :
                $city = 193;
                break;
            
            case 'Сочи' :
                $city = 194;
                break;
            
            case 'Нижний Новгород' :
                $city = 195;
                break;
            
            case 'Пермь' :
                $city = 196;
                break;
            
            case 'Ключевской район п.Целинный' :
                $city = 197;
                break;
            
            case 'Курьинский район, с. колывань' :
                $city = 198;
                break;
            
            case 'Тальменский район, р.п. Тальменка' :
            case 'Тальменский район, р.п. Тальменка, ул.Вокзальная,14' :
            case 'р.п.Тальменка' :
                $city = 26;
                break;
            
            case 'Крым' :
                $city = 199;
                break;
            
            case 'каменский район, с. столбово' :
                $city = 200;
                break;
            
            case 'Тальменский район, с. Ларичиха' :
            case 'Тальменский р-н с.Ларичиха' :
                $city = 201;
                break;
            
            case 'Баевский район, с. Баево' :
                $city = 45;
                break;
            
            case 'c. Черемное' :
                $city = 42;
                break;
            
            case 'с. Шипуново' :
                $city = 202;
                break;
            
            default : 
                $dontsee = [
                    'Саратовская обл.', 'Алтайский край', 'Чемальский район', 'Павловский район', 'Красногорский район', 'Алтайский край\Бийский район','Саратовская обл.',
                    'Немецкий национальный район', 'Завьяловский район', 'Кулундинский район', 'Республика Алтай', 'Усть-Калманский район', 'Шипуновский р-н', 'Тогульский район',
                    'Калманский район', 'Залесовский район', 'Советский район', 'Алтайский район', 'Солонешенский район', 'Алтайский край\Завьяловский район', 'Алейский район',
                    'Бийский р-н', 'Ребрихинский район', 'Змеиногорский район', 'Ключевский район', 'Новичихинский район', 'Третьяковский район', 'Хабарский район', 'Благовещенский район',
                    'Шипуновский район', 'Немецкий национальный район', 'Томская область', 'Суетский район', 'Бийский район', 'Троицкий р-н', 'Зональный район', 'Мамонтовский р-н',
                    'Смоленский р-н', 'Казахстан', 'Краснощёковский р-н', 'Новичихинский р-н', 'Смоленский район', 'Ельцовский район', 'Первомайский р-н', 'Майминский район',
                    'Быстроистокский район', 'Каменский район', 'Заринский район', 'Приморье', 'Локтевский район', 'целинный район', 'Целинный район', 'Каменский р-н', 'Егорьевский р-н',
                    'Онгудайский район', 'Шелаболихинский район', 'Петропавловский район', 'Кемеровская область', 'Мамонтовский район', 'Омская область', 'Усть-Пристанский район',
                    'Рубцовский район', 'Поспелихинский район', 'Первомайский район', 'Егорьевский район', 'Целинный р-н', 'Чарышский район', 'Алтайский край\Благовещенский район',
                    'Угловский район', 'Топчихинский район', 'Баевский район', 'Чебулинский район', 'Панкрушихинский район', 'Тальменский район', 'Краснощёковский район', 'Суетский  район',
                    'Волчихинский район', 'Тюменцевский район', 'Романовский район', 'Ленинск-Кузнецкий район', 'Славгородский район', 'Родинский район', 'Михайловский район', 'Табунский район',
                    'Гурьевский район', 'Усть-Коксинский район', 'Улаганский район', 'Кош-Агачский район', 'Чехов', 'Тюменцевский р-он', 'Троицкий район', 'Новосибирская область', 'руководитель',
                    'Алтайский край\Косихинский район','Курьинский район','Немецкий район','Бурлинский район','Ребрихинский р-н','п.Плодосовхоз','Крутихинский район','Кытмановский район',
                    'Михайловский р-н','Косихинский район','Косихинский Район','Новочихинский р-н','Волчихинский р-н','Экибастуз','Кузбасс','Немецкий р-н, с.Полевоев','Панкрушихинский',
                    'Усть - Алейка','с. Сентелек','Петропавловский р-н','Чойский','Чарышское','Рубцовский р-н','Крутихинский район,с. Крутиха, ул. Гагарина, 60','Рубцовский р-н с.Новоросиййское',
                    'Смоленское', 'Алтайский край\Калманский район', 'Михайловский р-н,с.Михайловское', 'Змеиногорский р-н,п.Октябрьский,ул.Комсомольская,2а', 'Иркутская область', 
                ];
                
                if(in_array($str, $dontsee) || empty($str)) {
                    
                } else {//запись
                    $file = 'citys.txt';
                    file_put_contents($file, $str . ' - '.$idclient.PHP_EOL, FILE_APPEND | LOCK_EX);
                }
        }
        return $city;
    }
    
    //найти сотрудников
    public function switchuser ($id, $null = false) {
        $created_user = NULL;
        switch ($id) {
            case 4402 : //Коновалова Татьяна
                $created_user = 112;
                break;
            
            case 4450 : //Ильиных Анна
                $created_user = 70;
                break;
            
            case 4500 : //Митюкова Евгения
                $created_user = 520;
                break;
            
            case 3212 : //якубовская
                $created_user = 80;
                break;
            
            case 4306 : //вохмина
            case 4200 :
                $created_user = 270;
                break;
            
            case 4201 : //артемова алиса
                $created_user = 471;
                break;
            
            case 3903 : //касимова
                $created_user = 175;
                break;
            
            case 3202 : //кривилева
                $created_user = 365;
                break;
            
            case 3363 : //милёшина
                $created_user = 398;
                break;
            
            case 2650 : //овиденко
                $created_user = 14;
                break;
            
            case 3001 : //осипова
                $created_user = 318;
                break;
            
            case 3357 : //пак
                $created_user = 277;
                break;
            
            case 3618 : //родина
                $created_user = 432;
                break;
            
            case 3160 : //синдеева
                $created_user = 354;
                break;
            
            case 2050 : //школина
                $created_user = 10;
                break;
            
            case 3204 : //горбунова
                $created_user = 79;
                break;
            
            case 3205 : //бакушкина
                $created_user = 337;
                break;
            
            case 3603 : //бассауэр
                $created_user = 6;
                break;
            
            case 3602 : //зеленова
                $created_user = 85;
                break;
            
            case 2600 : //зяблицева
                $created_user = 31;
                break;
            
            case 3553 : //корчагина
            case 3900 :
                $created_user = 92;
                break;
            
            case 4350 : //кот елена
                $created_user = 508;
                break;
            
            case 3608 : //ланская
                $created_user = 422;
                break;
            
            case 4000 : //навасардян
                $created_user = 176;
                break;
            
            case 1400 : //орищенко
                $created_user = 27;
                break;
            
            case 3902 : //хряпчина
                $created_user = 146;
                break;
            
            case 4307 : //черетун
                $created_user = 61;
                break;
            
            case 4250 : //шартон
                $created_user = 493;
                break;
            
            case 3905 : //шишкина
                $created_user = 87;
                break;
            
            case 3700 : //суворова-Карасёва
                $created_user = 140;
                break;
            
            case 3500 : //колосова
                $created_user = 88;
                break;
            
            case 2104 : //тарасова
                $created_user = 26;
                break;
            
            case 3008 : //корнилова
                $created_user = 63;
                break;
            
            case 3360 : //стахнева
                $created_user = 427;
                break;
            
            case 3610 : //болдарева
                $created_user = 46;
                break;
            
            case 2156 : //ситникова
                $created_user = 34;
                break;
            
            case 112 : //шмакова
                $created_user = 291;
                break;
            
            case 3210 : //камболина
                $created_user = 67;
                break;
            
            case 4100 : //щеголькова
                $created_user = 460;
                break;
            
            case 3800 : //савельева
                $created_user = 442;
                break;
            
            case 3901 : //смолянникова
                $created_user = 12;
                break;
            
            case 3351 : //петракова софиЯ
                $created_user = 389;
                break;
            
            case 112 : //шмакова Татьяна Александровна
                $created_user = 291;
                break;
            
            case 3009 : //дегтярева анна
                $created_user = 341;
                break;
            
            case 3612 : //сентяпова алла
                $created_user = 424;
                break;
            
            case 4300 : //чалик марина
                $created_user = 499;
                break;
            
            case 2151 : //клименко татьяна
                $created_user = 32;
                break;
            
            case 3904 : //мерзликина дарья
                $created_user = 147;
                break;
            
            case 3600 : //глазнева юлия
                $created_user = 417;
                break;
            
            case 3619 : //Цымбалова Ксения
                $created_user = 429;
                break;
            
            case 3703 : //король марина
            case 3702 : 
                $created_user = 107;
                break;
            
            case 2201 : //кагамлык ирина
                $created_user = 30;
                break;
            
            case 3356 : //филиппов евгений
                $created_user = 393;
                break;
            
            case 3607 : //гусельникова светлана
                $created_user = 99;
                break;
            
            case 3617 : //Балдуркин Евгений
                $created_user = 428;
                break;
            
            case 4001 : //Залуцкая Ольга
                $created_user = 456;
                break;
            
            case 3208 : //Плотникова Светлана
                $created_user = 106;
                break;
            
            case 3207 : //Булгакова Евгения
                $created_user = 60;
                break;
            
            case 3359 : //Стрекалова Анастасия
                $created_user = 392;
                break;
            
            case 3213 : //Артеменко Лариса
            case 2152 :
                $created_user = 9;
                break;
            
            case 3352 : //Кузнецова Наталья
                $created_user = 111;
                break;
            
            case 3609 : //Баёва Лидия
                $created_user = 98;
                break;
            
            case 3203 : //гергерт эдуард
                $created_user = 370;
                break;
            
            case 3850 : //Линникова Марина
                $created_user = 448;
                break;
            
            case 3300 : //Николаенко Анна
                $created_user = 378;
                break;
            
            case 3358 : //Сладкевич Александр
                $created_user = 379;
                break;
            
            case 4304 : //Бин Юлия
                $created_user = 93;
                break;
            
            case 3301 : //Тарабукина Маргарита
                $created_user = 380;
                break;
            
            case 3450 : //Солопов Александр
                $created_user = 402;
                break;
            
            case 4400 : //Кудрина Ритта
                $created_user = 114;
                break;
            
            case 4203 : //Быстрова Наталья(уволен, создан временный сотрудник)
                $created_user = 513;
                break;
            
            case 3251 : //Шилов Никита
                $created_user = 78;
                break;
            
            case 3701 : //Купцова Ирина Александровна
                $created_user = 17;
                break;
            
            case 3209 : //бекташева
                $created_user = 76;
                break;
            
            default : //неизвестнй сотрудник(нет в интранете)
                if(in_array($id, [4550,3950, 3206, 3951, 3614, 3616, 3613, 3211, 3152, 3364, 3201, 4202, 3100, 3005 ,3010 ,3200 ,2901,3153,3604 ,3606 ,3354 ,3353, 3601, 4050, 3501, 3551, 3552, 3650, 3611, 4150, 4002, 3307, 3400])) {
                } else {
                    if(!empty($id)){
                        $file = 'names.txt';
                        file_put_contents($file, $id.PHP_EOL, FILE_APPEND | LOCK_EX);
                    }
                }
                $created_user = 98;//руководитель отдела
        }
        
        if($created_user === NULL && $null) return NULL;//если неизвестный сотрудник(которого нет в интранете)
        
        return $created_user;
    }
    
    //типы событий(позвонить, создан)
    public function switchEventType($id, $fk_department) {
        //601 - создание контакта в общем
        $c = NULL;
        switch ($id) {//ПРОВЕРЬ АКТУАЛЬНОСТЬ ОБЯЗАТЕЛЬНО ПЕРЕД ВЫГРУЗКОЙ
            case 1004 ://созвониться
                if($fk_department == $this->o_politsib) $c = 10;
                if($fk_department == $this->o_sokol) $c = 18;
                if($fk_department == $this->o_avtograf) $c = 19;
                if($fk_department == $this->o_rezon) $c = 31;
                break;
            
            case 1002 ://встретиться
                if($fk_department == $this->o_politsib) $c = 9;
                if($fk_department == $this->o_sokol) $c = 14;
                if($fk_department == $this->o_avtograf) $c = 20;
                if($fk_department == $this->o_rezon) $c = 32;
                break;
            
            case 1003 ://"отправить e-mail"
                if($fk_department == $this->o_politsib) $c = 8;
                if($fk_department == $this->o_sokol) $c = 15;
                if($fk_department == $this->o_avtograf) $c = 21;
                if($fk_department == $this->o_rezon) $c = 42;
                break;
            
            case 1001 ://прочее
                if($fk_department == $this->o_politsib) $c = 11;
                if($fk_department == $this->o_sokol) $c = 16;
                if($fk_department == $this->o_avtograf) $c = 22;
                if($fk_department == $this->o_rezon) $c = 43;
                break;
            
            case 1000 :
                $c = 0;//продажа/сделка ИГНОР но можно
                break;
            
            case 562 :
                $c = 0;//закрыть сделку ИГНОР но можно
                break;
            
            /*case 560 ://создать отгрузку(сделку)
                if($fk_department == $this->o_sokol) $c = 17;
                break;*/
            
            case 1010 :
                $c = 12;//входящий звонок ?
                break;
            
            case 550 ://отгрузить товар? игнор видимо, ничего не делает                
                break;
            
            default:
                $c = 'see'.$id;
        }
        
        if($c === NULL) echo 'нет ид ---------------'. $id .PHP_EOL;
        return $c;
    }
    
    //типы контактной информации
    public function switchContacts($id) {
        $c = NULL;
        switch ($id) {
            case 'phone' :
                $c = 'workPhone';
                break;
            
            case 'fax' :
                $c = 'fax';
                break;
            
            case 'mobile' :
                $c = 'mobilePhone';
                break;
            
            case 'email' :
                $c = 'email';
                break;
            
            case 'www' :
                $c = 'web';
                break;
            
            case 'icq' :
                $c = 'icq';
                break;
            
            case 'skype' :
                $c = 'skype';
                break;
        }
        return $c;
    }
    
    /*
     * очистка после переноса. неактально, в процессе переноса лишние не вносятся
     */
    public function actionCleaning() {
        $otdel = $this->o_rezon;
        
        //пройти по всем задачам, определить отдел ответственного и удалить, если отдел не совпадает с указанным(задачи, комменты из других отделов)
        $rows = (new \yii\db\Query())
        ->select(['pk_task', 'user.fk_department'])
        ->join('LEFT JOIN', \app\modules\user\models\User::tableName(), 'crm_tasks.fk_user_task = user.id_user')
        ->from(CrmTasks::tableName())
        ->where(['crm_tasks.fk_department' => $otdel]);
        
        foreach ($rows->batch(100) as $tasks) {
            $arrDelete = [];
            foreach ($tasks as $task) {
                if($task['fk_department'] != $otdel) {//поместить в очеред на удаление
                    $arrDelete[] = $task['pk_task'];
                }
            }
            if($arrDelete){
                $impl = implode(',', $arrDelete);
                $a = Yii::$app->db->createCommand('DELETE FROM '.CrmTasks::tableName().' WHERE pk_task IN ('.$impl.')')->execute();
                echo 'удалено '.$a.' записей';
            }
        }
    }
        
    public function actionCleantask() {
        $delete_count = 0;
        $otdel = $this->o_rezon;
        
        /*$rows = (new \yii\db\Query())
        ->select(['name_company', 'pk_company'])
        ->from(CrmCompany::tableName())
        ->where(['fk_department' => $otdel]);*/
        
        foreach ($rows->each() as $company) {            
            //найти задачи, сравнивать по этим свойствам
            $tasks = (new \yii\db\Query())
            ->select(['create_date', 'created_user', 'text_task', 'pk_task', 'status_text'])
            ->from(CrmTasks::tableName())
            ->where(['fk_crmcompany' => $company['pk_company']])
            ->all();
            
            $create_date = $created_user = $text_task = $status_text =  $alltasks = $uniqtasks = [];
            
            //загнать в массивы
            foreach ($tasks as $task) {
                $create_date[$task['create_date']] = $task['pk_task'];
                $created_user[$task['created_user']] = $task['pk_task'];
                $text_task[$task['text_task']] = $task['pk_task'];
                $status_text[$task['status_text']] = $task['pk_task'];
                $alltasks[] = $task['pk_task'];
            }
            
            //из всех собрать в 1 массив уникальных ид задач            
            $uniqtasks = array_unique(array_merge(
                array_values($status_text), 
                array_values($create_date), 
                array_values($created_user),
                array_values($text_task)
            ));
            
            $deleteTasksIds = array_diff($alltasks, $uniqtasks);//точно можно удалять
            
            if($deleteTasksIds){
                $a = Yii::$app->db->createCommand('DELETE FROM '.CrmTasks::tableName().' WHERE pk_task IN ('. implode(',', $deleteTasksIds).')')->execute();
                $delete_count += $a;
                echo 'Удалено: '.$a.PHP_EOL;
            }
        }
        
        echo 'Всего удалено: '.$delete_count.PHP_EOL;
    }
    
    /**
     * новый алгоритм
     */
    public function actionDublicatesnew() {
        $delete_count = 0;
        
        /*$rows = (new \yii\db\Query())
        ->select(['pk_company', 'old_pkcompany'])
        ->from(CrmCompany::tableName())
        ->where(['responsible_user' => 70])//76,98,9,67,106,111,112,140,520,70//ид в интранете по 1
        ->all();*/
        
        $allcompany = $uniqcompany = [];
            
        //загнать в массивы
        foreach ($rows as $task) {
            $uniqcompany[$task['old_pkcompany']] = $task['pk_company'];
            $allcompany[] = $task['pk_company'];
        }
        
        $deleteTasksIds = array_diff($allcompany, $uniqcompany);//точно можно удалять
        
        if($deleteTasksIds){
            $a = Yii::$app->db->createCommand('DELETE FROM '. CrmCompany::tableName().' WHERE pk_company IN ('. implode(',', $deleteTasksIds).')')->execute();
            $delete_count += $a;
            echo 'Удалено: '.$a.PHP_EOL;
        }
        
        echo 'Всего удалено: '.$delete_count.PHP_EOL;
    }
    
    /*
     * неск раз запускать, до нуля
     * попытка удаления дубликатов компаний+контактов
     * пустых, без всего
     */
    public function actionDublicates() {
        $otdel = $this->o_rezon;        
        
        $arrToDeleteComp = [];
        
        $rows = (new \yii\db\Query())
        ->select(['name_company', 'pk_company'])
        ->from(CrmCompany::tableName())
        ->where(['fk_department' => $otdel]);
        
        foreach ($rows->each() as $company) {
            $simpleCompanys = (new \yii\db\Query())->select(['name_company', 'pk_company', 'COUNT(pk_task) AS count_task', 'COUNT(pk_deal) AS count_deal', 'pk_contact'])->from(CrmCompany::tableName())
                    ->join('LEFT JOIN', CrmTasks::tableName(), 'crm_tasks.fk_crmcompany = crm_company.pk_company')
                    ->join('LEFT JOIN', CrmContact::tableName(), 'crm_contact.fk_crm_company = crm_company.pk_company')
                    ->join('LEFT JOIN', CrmDeals::tableName(), 'crm_deals.fk_crmcompany = crm_company.pk_company')
                    ->where(['name_company' => $company['name_company']])->andWhere(['!=', 'pk_company', $company['pk_company']])->all();
            
            foreach ($simpleCompanys as $simpleCompany) {
                if($simpleCompany['pk_company'] !== NULL){//находят сами себя
                    
                    if($simpleCompany['count_task'] == 0) {//нет задач
                        if($simpleCompany['pk_contact'] !== NULL) {//есть контакты - проверить их
                            $conts = Yii::$app->db->createCommand('SELECT pk_contact FROM crm_contact WHERE fk_crm_company = '.$simpleCompany['pk_company'])->queryAll();//все контакты
                            
                            foreach ($conts as $cont) {
                                $couctas = Yii::$app->db->createCommand('SELECT COUNT(*) AS count_task FROM `crm_tasks` WHERE fk_crmcontact = '.$cont['pk_contact'])->queryOne();
                                if($couctas['count_task'] == 0){//и у контакта нет задач
                                    $arrToDeleteComp[] = $simpleCompany['pk_company'];
                                }
                            }
                        } else {//нет контактов 0 удалить
                            $arrToDeleteComp[] = $simpleCompany['pk_company'];
                        }
                    }
                    
                    
                    
                }
            }
        }
        
        //var_dump($arrToDeleteComp);
        if($arrToDeleteComp){
            $a = Yii::$app->db->createCommand('DELETE FROM '.CrmCompany::tableName().' WHERE pk_company IN ('. implode(',', $arrToDeleteComp).')')->execute();
            echo 'Удалено: '.$a.PHP_EOL;
        } else {
            echo 'нечего удалять: '.PHP_EOL;
        }
    }
}