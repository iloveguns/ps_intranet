<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\FunctionModel;

/*
 * получение информации из 2gis
 */
/*
CREATE TABLE `gis` (
  `pk` int(10) UNSIGNED NOT NULL,
  `name_company` varchar(256) NOT NULL,
  `address_company` varchar(256) NOT NULL,
  `phones_company` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `gis`
  ADD PRIMARY KEY (`pk`);
 */
class GisController extends Controller {
    const PAGINATION = 50;//ограничение 2гиса
    const KEY = 'ruoedw9225';
    const DISTRICT_ID = 563559838777356;//районы: 563559838777359 - ленинский, 563559838777352 - железнодорожный, 563559838777353 - Индустриальный, 563559838777355 - Октябрьский, 563559838777356 - Центральный

    public function actionIndex() {
        //список рубрик - $( ".rubricsList__listItemLinkTitle" ).each(function( index ) { console.log( $( this ).attr('href') ); });
        $arrCats = [
            563070212511019, 563070212548503, 563070212505601, 563070212505602, 563070212525132, 563070212505610, 563070212505603, 563070212505604, 563070212506387, 563070212505605,
            563070212505613, 563070212505615, 563070212505606, 563070212506635, 563070212507221, 563070212505614, 563070212505607, 563070212505608, 563070212548467, 563070212505609,
            563070212548472, 563070212506346, 563070212512147, 563070212505612, 563070212505611, 563070212548463, 563070212506569, 
            ];
        
        foreach ($arrCats as $cat_id) {        
            $cat = FunctionModel::fileGetContents('https://catalog.api.2gis.ru/2.0/catalog/rubric/list?parent_id='.$cat_id.'&region_id=4&sort=popularity&fields=items.rubrics&key='.self::KEY);
            $pcat = json_decode($cat, true);
            
            $page = 1;
            $this->Cat($pcat, $page);
        }
    }
    
    /*
    * подкатегории. получить ид и сформировать ссылку
    * по 50 шт
    */
    private function Cat($pcat, $page) {
        foreach ($pcat['result']['items'] as $subcat) {
            $subcat = FunctionModel::fileGetContents('https://catalog.api.2gis.ru/2.0/catalog/branch/list?page='.$page.'&page_size='.self::PAGINATION.'&district_id='.self::DISTRICT_ID.'&rubric_id='.$subcat['id'].'&hash=101ad4f60a3e39fb&stat%5Bpr%5D=3&region_id=4&fields=items.contact_groups%2Citems.flags%2Citems.address%2Citems.rubrics%2Citems.point%2Citems.external_content%2Citems.schedule%2Citems.org%2Citems.ads.options%2Crequest_type%2Cwidgets%2Cfilters%2Citems.reviews%2Ccontext_rubrics%2Chash%2Csearch_attributes&key='.self::KEY);
            $psubcat = json_decode($subcat, true);

            $insertArray = [];

            if(isset($psubcat['result']) && isset($psubcat['result']['items'])){
                foreach ($psubcat['result']['items'] as $comp) {
                    $street = $comp['address']['components'][0]['street'];//улица(название)
                    $streetNumber = $comp['address']['components'][0]['number'];//номер дома улицы

                    $phones = [];
                    if(isset($comp['contact_groups'][0])) {
                        foreach ($comp['contact_groups'][0]['contacts'] as $cont_value) {
                            if($cont_value['type'] == 'phone') {//только телефоны
                                $phones[] = $cont_value['value'];
                            }
                        }
                    }
                    //echo $comp['name'].' '.$street.' : '.$streetNumber.PHP_EOL;

                    $insertArray[] = [$comp['name'], $street.', '.$streetNumber, implode(', ', $phones)];
                }
                
                Yii::$app->db->createCommand()->batchInsert('gis', ['name_company', 'address_company', 'phones_company'], $insertArray)->execute();
                
                echo $psubcat['result']['total'].PHP_EOL;
                if($psubcat['result']['total'] > self::PAGINATION) {//значит нужно делать еще запрос
                    echo 'Рекурсия'.PHP_EOL;
                    $this->Cat($pcat, $page+1);
                }
            }
            return true;
        }
    }
}
