<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\FunctionModel;
use app\modules\crm\models\CrmTasks;
use app\modules\crm\models\CrmContact;
use app\modules\crm\models\CrmCompany;
use app\modules\crm\models\CrmTags;
use app\modules\crm\models\CrmRelationTags;
use app\modules\crm\models\CrmContactsInfo;

class AmoCrmController extends Controller {
    private $amo;
    
    public function __construct($id, $module, $config = []) {
        set_time_limit(0);
        $this->amo = Yii::$app->amocrm->getClient();
        
        parent::__construct($id, $module, $config);
    }
    
    public function __destruct() {
        echo 'время выполнения: '.Yii::getLogger()->getElapsedTime().PHP_EOL;
    }
   
    //точно все компании находит?
    public function actionCompanys() {//2097
        $run = true;//запуск, пока есть инфа
        $count = 0;
        $countall = 1;
        $offset = 500;
        $arrInsertedIds = [];
        
        while ($run) {
            $companys = $this->amo->company->apiList([//компании
                'limit_rows' => 500,
                'limit_offset' => $count * $offset,
            ]);
            
            if(empty($companys)) {
                $run = false;
                echo 'данные закончились'.PHP_EOL;
                break;
            }
            
            //---выполнение операций с данными
            foreach ($companys as $company) {                
                if(isset($arrInsertedIds[$company['id']])) {
                    echo 'Дубликат: '.$company['id'].' '.PHP_EOL;
                    continue;
                }
                $arrInsertedIds[$company['id']] = $company['id'];
                echo 'начинаю '.$countall.' - '.$company['id'].PHP_EOL;
                
                $date = new \DateTime();
                $date->setTimestamp($company['date_create']);
                $date->modify('+7 hour');

                $insertArr = [];

                Yii::$app->db->createCommand()->insert(CrmCompany::tableName(), [
                    'pk_company' => $company['id'],
                    'responsible_user' => $this->switchuser($company['responsible_user_id']),
                    'name_company' => $company['name'],
                    'created_user' => $this->switchuser($company['created_user_id'], false),
                    'create_date' => $date->format('Y-m-d H:i:s'),
                    'fk_department' => 13,
                ])->execute();

                //теги
                if(!empty($company['tags'])) {
                    foreach ($company['tags'] as $tag) {
                        $tagid = NULL;

                        $countTags = Yii::$app->db->createCommand('SELECT pk_tag FROM '.CrmTags::tableName().' WHERE name_tag = "'.$tag['name'].'"')->queryOne();
                        if(!$countTags) {//нет тега
                            $mt = new CrmTags();
                            $mt->name_tag = $tag['name'];
                            $mt->save();
                            $tagid = $mt->pk_tag;
                        } else {//есть тег - добавить
                            $tagid = $countTags['pk_tag'];
                        }
                        Yii::$app->db->createCommand()->insert(CrmRelationTags::tableName(), [
                            'fk_tag' => $tagid,
                            'fk_crmcompany' => $company['id'],
                        ])->execute();
                    }                
                }

                //контактная информация
                if(!empty($company['custom_fields'])){
                    foreach ($company['custom_fields'] as $field) {
                        if(!isset($field['code'])) continue;//?? День рождения без кода
                        if($field['code'] == 'ADDRESS'){//обновить контакт(там адрес содержится)
                            Yii::$app->db->createCommand()->update(CrmCompany::tableName(), ['address_other' => $field['values'][0]['value']], 'pk_company = '.$company['id'])->execute();
                        } else if($field['code'] == 'WEB') {
                            Yii::$app->db->createCommand()->insert(CrmContactsInfo::tableName(), [
                                'contact_field' => 'web',
                                'contact_value' => $field['values'][0]['value'],
                                'fk_crmcompany' => $company['id'],
                            ])->execute();
                        } else {
                            foreach ($field['values'] as $value) {
                                Yii::$app->db->createCommand()->insert(CrmContactsInfo::tableName(), [
                                    'contact_field' => $this->switchContacts($value['enum']),
                                    'contact_value' => $value['value'],
                                    'fk_crmcompany' => $company['id'],
                                ])->execute();
                            }
                        }
                    }
                }
                ++$countall;
            }
            //---конец выполнение операций с данными
            
            $count++;
            echo 'Засыпаю в '.$count.' раз'.PHP_EOL;            
            sleep(1);
        }
    }
    
    /*
     * контакты амоцрм
     */
    public function actionContacts() {
        $run = true;//запуск, пока есть инфа
        $count = 0;
        $countall = 1;
        $offset = 500;        
        
        while ($run) {
            $contacts = $this->amo->contact->apiList([//контакты
                'limit_rows' => 500,
                'limit_offset' => $count * $offset,
            ]);
            
            if(empty($contacts)) {
                $run = false;
                echo 'данные закончились'.PHP_EOL;
                break;
            }
            
            //---выполнение операций с данными
            foreach ($contacts as $contact) {
                echo 'начинаю '.$countall.' - '.$contact['id'].PHP_EOL;
                
                $date = new \DateTime();
                $dateu = $date;
                $dateu->setTimestamp($contact['last_modified']);
                $dateu->modify('+7 hour');
                $date->setTimestamp($contact['date_create']);
                $date->modify('+7 hour');
                
                $comp = $contact['linked_company_id'];//компании может не быть
                if(empty($comp)) {
                    $comp = NULL;//нет привязи к компании
                } else {
                    $haveComp = Yii::$app->db->createCommand('SELECT responsible_user FROM '. CrmCompany::tableName().' WHERE pk_company = "'.$comp.'"')->queryOne();
                    if(!$haveComp) continue;//если нет ид компании(совсем новые контакты)
                }
                
                Yii::$app->db->createCommand()->insert(CrmContact::tableName(), [
                    'pk_contact' => $contact['id'],                    
                    'name_contact' => $contact['name'],
                    'fk_crm_company' => $comp,                    
                    'created_user' => $this->switchuser($contact['created_user_id'], false),
                    'create_date' => $date->format('Y-m-d H:i:s'),
                    'update_date' => $dateu->format('Y-m-d H:i:s'),
                    'responsible_user' => $this->switchuser($contact['responsible_user_id']),
                    'fk_department' => 13,
                ])->execute();
                
                //теги
                if(!empty($contact['tags'])) {
                    foreach ($contact['tags'] as $tag) {
                        $tagid = NULL;

                        $countTags = Yii::$app->db->createCommand('SELECT pk_tag FROM '.CrmTags::tableName().' WHERE name_tag = "'.$tag['name'].'"')->queryOne();
                        if(!$countTags) {//нет тега
                            $mt = new CrmTags();
                            $mt->name_tag = $tag['name'];
                            $mt->save();
                            $tagid = $mt->pk_tag;
                        } else {//есть тег - добавить
                            $tagid = $countTags['pk_tag'];
                        }
                        Yii::$app->db->createCommand()->insert(CrmRelationTags::tableName(), [
                            'fk_tag' => $tagid,
                            'fk_crmcontact' => $contact['id'],
                        ])->execute();
                    }                
                }
                
                //контактная информация
                if(!empty($contact['custom_fields'])){
                    foreach ($contact['custom_fields'] as $field) {
                        if(!isset($field['code'])) continue;//?? День рождения без кода
                        if($field['code'] == 'ADDRESS'){//обновить контакт(там адрес содержится)
                            Yii::$app->db->createCommand()->update(CrmContact::tableName(), ['address_other' => $field['values'][0]['value']], 'pk_contact = '.$contact['id'])->execute();
                        } else if($field['code'] == 'WEB') {
                            Yii::$app->db->createCommand()->insert(CrmContactsInfo::tableName(), [
                                'contact_field' => 'web',
                                'contact_value' => $field['values'][0]['value'],
                                'fk_crmcontact' => $contact['id'],
                            ])->execute();
                        } else if($field['code'] == 'POSITION') {//должность
                            Yii::$app->db->createCommand()->update(CrmContact::tableName(), ['post_contact' => $field['values'][0]['value']], 'pk_contact = '.$contact['id'])->execute();
                        } else {
                            foreach ($field['values'] as $value) {
                                Yii::$app->db->createCommand()->insert(CrmContactsInfo::tableName(), [
                                    'contact_field' => $this->switchContacts($value['enum']),
                                    'contact_value' => $value['value'],
                                    'fk_crmcontact' => $contact['id'],
                                ])->execute();
                            }
                        }
                    }
                }
                ++$countall;
            }
            //---конец выполнение операций с данными

            $count++;
            echo 'Засыпаю в '.$count.' раз'.PHP_EOL;            
            sleep(1);
        }
    }
    
    //3
    public function actionTaskscontact() {
        $model = Yii::$app->db->createCommand('SELECT pk_contact FROM '. CrmContact::tableName())->queryAll();
        $this->actionTasksnew($model, 'pk_contact');
    }
    
    //3
    public function actionTaskscompany() {
        $model = Yii::$app->db->createCommand('SELECT pk_company FROM '. CrmCompany::tableName())->queryAll();
        $this->actionTasksnew($model, 'pk_company');
    }
    
    public function actionTasksnew($model, $attr) {
        $count = 0;
        $countall = 1;
        $sttext = '';
        
        foreach ($model as $company) {
            $tasks = $this->amo->task->apiList([//задачи
                'element_id' => $company[$attr],
                'limit_rows' => 500,
            ]);
            
            if(empty($tasks)) {
                continue;
            }
            
            //---выполнение операций с данными
            foreach ($tasks as $task) {
                echo 'начинаю '.$countall.' - '.$task['id'].PHP_EOL;
                
                $date = new \DateTime();
                $datec = new \DateTime();
                $dateu = new \DateTime();
                $dateu->setTimestamp($task['last_modified']);//посл изменение
                $dateu->modify('+7 hour');
                $datec->setTimestamp($task['complete_till']);//дата завершения
                $datec->modify('+7 hour');
                $date->setTimestamp($task['date_create']);//дата создания
                $date->modify('+7 hour');
                
                $comp = $cont = NULL;                
                if($task['element_type'] == 1){//контакт
                    $cont = $task['element_id'];
                    if(empty($cont)) {
                        $cont = NULL;//нет привязи к контакту
                    } else {
                        $haveCont = Yii::$app->db->createCommand('SELECT responsible_user FROM '. CrmContact::tableName().' WHERE pk_contact = "'.$cont.'"')->queryOne();
                        if(!$haveCont) continue;//если нет ид компании(совсем новые контакты)
                    }
                } else {//компания
                    $comp = $task['element_id'];
                    if(empty($comp)) {
                        $comp = NULL;//нет привязи к компании
                    } else {
                        $haveComp = Yii::$app->db->createCommand('SELECT responsible_user FROM '. CrmCompany::tableName().' WHERE pk_company = "'.$comp.'"')->queryOne();
                        if(!$haveComp) continue;//если нет ид компании(совсем новые контакты)
                    }
                }
                
                $st = 1;//в процессе
                $status = (int)$task['status'];
                if($status == 1) {
                    $st = 10;//выполнена
                    
                    $dt = $dateu->format('Y-m-d');
                    
                    if($dateu->format('H:i') == '23:59') $tt = 'allday';
                    else $tt = $dateu->format('H:i');
                } else {
                    if($datec->format('H:i') == '23:59') $tt = 'allday';
                    else $tt = $datec->format('H:i');
                    
                    $dt = $datec->format('Y-m-d');
                }
                
                $sttext = $task['result'];//массив
                if(!isset($sttext['text'])) $sttext = NULL;
                else $sttext = $sttext['text'];
                
                Yii::$app->db->createCommand()->insert(CrmTasks::tableName(), [
                    'date_task' => $dt,//$datec было, но не совпадает
                    'time_task' => $tt,
                    'fk_user_task' => $this->switchuser((int)$task['responsible_user_id']),
                    'fk_typetask' => $this->switchTypeTask($task['task_type']),                    
                    'fk_crmcontact' => $cont,
                    'fk_crmcompany' => $comp,
                    'text_task' => $task['text'],
                    'create_date' => $date->format('Y-m-d H:i:s'),
                    'update_date' => $dateu->format('Y-m-d H:i:s'),
                    'status_task' => $st,
                    'status_text' => $sttext,                  
                    'fk_department' => 13,
                ])->execute();
                
                ++$countall;
            }
            //---конец выполнение операций с данными

            $count++;
            echo 'Засыпаю в '.$count.' раз'.PHP_EOL;            
            sleep(1);
        }
    }
    
    /*
     * заметки(приписки к контактам)
     */
    //4
    public function actionNotescontact() {
        $model = Yii::$app->db->createCommand('SELECT pk_contact FROM '. CrmContact::tableName())->queryAll();
        $this->actionNotesc($model, 'pk_contact', 'contact');
    }
    
    //4
    public function actionNotescompany() {
        $model = Yii::$app->db->createCommand('SELECT pk_company FROM '. CrmCompany::tableName())->queryAll();
        $this->actionNotesc($model, 'pk_company', 'company');
    }
    
    public function actionNotesc($model, $attr, $type) {
        $count = 0;
        $countall = 1;
        $sttext = '';
        
        foreach ($model as $company) {
            $tasks = $this->amo->note->apiList([//примечания
                'type' => $type,
                'element_id' => $company[$attr],
                'limit_rows' => 500,
            ]);
            
            if(empty($tasks)) {
                continue;
            }
            
            //---выполнение операций с данными
            foreach ($tasks as $task) {
                echo 'начинаю '.$countall.' - '.$task['id'].PHP_EOL;
                
                $date = new \DateTime();
                $dateu = new \DateTime();
                $dateu->setTimestamp($task['last_modified']);//посл изменение
                $dateu->modify('+7 hour');
                $date->setTimestamp($task['date_create']);//дата создания
                $date->modify('+7 hour');
                
                $comp = $cont = NULL;
                if($task['element_type'] == 1){//контакт
                    $cont = $task['element_id'];
                    if(empty($cont)) {
                        $cont = NULL;//нет привязи к контакту
                    } else {
                        $haveCont = Yii::$app->db->createCommand('SELECT responsible_user FROM '. CrmContact::tableName().' WHERE pk_contact = "'.$cont.'"')->queryOne();
                        if(!$haveCont) continue;//если нет ид компании(совсем новые контакты)
                    }
                } else {//компания
                    $comp = $task['element_id'];
                    if(empty($comp)) {
                        $comp = NULL;//нет привязи к компании
                    } else {
                        $haveComp = Yii::$app->db->createCommand('SELECT responsible_user FROM '. CrmCompany::tableName().' WHERE pk_company = "'.$comp.'"')->queryOne();
                        if(!$haveComp) continue;//если нет ид компании(совсем новые контакты)
                    }
                }
                
                $st = 0;
                $sttext = $task['text'];
                
                switch ((int)$task['note_type']) {
                    case 12 ://	Компания создана
                        $st = 12;//сис коммент
                        $sttext = 'Создана компания ::SELFLINK::';
                        break;
                    
                    case 2 ://Создан контакт
                        $st = 12;//сис коммент
                        $sttext = 'Создан контакт ::SELFLINK::';
                        break;
                    
                    case 3 :
                    case 4 ://обычный коммент
                        $st = 11;//сис коммент
                        break;

                    default:
                        //continue;
                        break;
                }
                
                if($st === 0) continue;
                
                Yii::$app->db->createCommand()->insert(CrmTasks::tableName(), [
                    'date_task' => $date->format('Y-m-d'),
                    'time_task' => $date->format('H:i'),
                    'fk_user_task' => $this->switchuser((int)$task['responsible_user_id']),                  
                    'fk_crmcontact' => $cont,
                    'fk_crmcompany' => $comp,
                    'create_date' => $date->format('Y-m-d H:i:s'),
                    'update_date' => $dateu->format('Y-m-d H:i:s'),
                    'status_task' => $st,
                    'status_text' => $sttext,                  
                    'fk_department' => 13,
                ])->execute();
                
                ++$countall;
            }
            //---конец выполнение операций с данными
            //DELETE FROM `crm_tasks` WHERE (text_task IS NULL AND status_text LIKE '%{"%')

            $count++;
            echo 'Засыпаю в '.$count.' раз'.PHP_EOL;            
            sleep(1);
        }
    }
    
    /*
     * сделки
     */
    //5
    public function actionDeals() {
        $count = 0;
        $countall = 1;
        $sttext = '';
        $offset = 500;
        $run = true;
        
        while ($run) {
            $leads = $this->amo->lead->apiList([//примечания
                'limit_rows' => 500,
                'limit_offset' => $count * $offset,
            ]);
            
            if(empty($leads)) {
                $run = false;
                echo 'данные закончились'.PHP_EOL;
                break;
            }
            
            foreach ($leads as $lead) {
                echo 'начинаю '.$countall.' - '.$lead['id'].PHP_EOL;
                
                $date = new \DateTime();
                $dateu = $date;
                $dateu->setTimestamp($lead['last_modified']);
                $dateu->modify('+7 hour');
                $date->setTimestamp($lead['date_create']);
                $date->modify('+7 hour');
                
                if(!(int)$lead['linked_company_id']) continue;
                
                Yii::$app->db->createCommand()->insert(\app\modules\crm\models\CrmDeals::tableName(), [               
                    'name_deal' => $lead['name'],
                    'price_deal' => $lead['price'],
                    'status_deal' => $this->switchDeal((int)$lead['status_id']),
                    'fk_crmcompany' => (int)$lead['linked_company_id'],
                    'create_date' => $date->format('Y-m-d H:i:s'),
                    'responsible_user' => $this->switchuser((int)$lead['responsible_user_id']),                    
                    'created_user' => $this->switchuser((int)$lead['created_user_id'], false),
                    'update_date' => $dateu->format('Y-m-d H:i:s'),
                ])->execute();
                ++$countall;
            }
            
            $count++;
            echo 'Засыпаю в '.$count.' раз'.PHP_EOL;            
            sleep(1);
        }
    }
    
    /*
     * типы сделок
     */
    public function switchDeal($id) {
        $c = NULL;
        switch ($id) {
            case 7638512 :
                $c = 2;
                break;
            
            case 7638514 :
                $c = 3;
                break;
            
            case 7638516 :
                $c = 4;
                break;
            
            case 7638518 :
                $c = 5;
                break;
            
            case 142 :
                $c = 6;
                break;
            
            case 143 :
                $c = 7;
                break;
        }
        return $c;
    }
    
    /*
     * контакты из ид амоцрм
     */
    public function switchContacts($id) {
        $c = NULL;
        switch ($id) {
            case 2169892 :
                $c = 'workEmail';
                break;

            case 2169894 :
                $c = 'privEmail';
                break;

            case 2169896 :
                $c = 'otherEmail';
                break;

            case 2169898 :
                $c = 'ICQ';
                break;

            case 2169900 :
                $c = 'Jabber';
                break;
            
            case 2169902 :
                $c = 'Skype';
                break;
            
            case 2169904 :
                $c = 'GTALK';
                break;
            
            case 2169906 :
                $c = 'MSN';
                break;
            
            case 2169880 :
                $c = 'workPhone';
                break;
            
            case 2169884 :
                $c = 'mobilePhone';
                break;
            
            case 2169886 :
                $c = 'fax';
                break;
                
            case 2169890 :
                $c = 'otherPhone';
                break;
            
            case 2169882 :
                $c = 'workddPhone';
                break;
        }
        return $c;
    }
    
    /*
     * типы задачи
     * ТОЛЬКО ДЛЯ ЭТОГО ОТДЕЛА
     */
    public function switchTypeTask($id) {
        $type = NULL;
        switch ($id) {
            case 'FOLLOW_UP' : 
                //$type = 4;
                break;
            
            case 'CALL' : 
                $type = 4;
                break;
            
            case 'MEETING' : 
                $type = 6;
                break;
            
            case 'LETTER' : 
                //$type = 6;
                break;
        }
        return $type;
    }
    
    //найти сотрудников
    public function switchuser ($id, $notnull = true) {
        $created_user = NULL;
        switch ($id) {
            case 221522 : //василиса
                $created_user = 87;
                break;

            case 221978 : //Анна Серебрянникова
                $created_user = 97;
                break;

            case 221976 : //юля бин
                $created_user = 93;
                break;

            case 221980 : //Анжелика Скачк
                $created_user = 94;
                break;

            case 232638 : //Коренкевич Лена
                $created_user = 472;
                break;
        }
        if($notnull && $created_user === NULL) $created_user = 87;
        
        return $created_user;
    }
}

/*
 * mysqldump -uroot -proot crm crm_company crm_contact crm_contacts_info crm_deals crm_relation_tags crm_tags crm_tasks crm_typedeals crm_typetask > dump.sql
 */