$(function() {
    /**
     * имитация заполнения
     */
    $('.grid-stack-item-content').each(function(i, f){
        $(this).html('<img class="gr-image-widget" data-pk_content="' + i + '" src="http://politsib.ru/img/2018-03-05/thumbs/720x400/340032cdb4d572f7c507d39b5a533ded.jpg"><div class="gr-text-widget">test</div>')
    });
    
    /**
     * загрузка данных по клику на выбор
     */
    var onSetContentToBlock = function() {
        if($('.grid-stack-item.gr-selected').length) {
            $('.grid-stack-item.gr-selected .grid-stack-item-content')[0].innerHTML = '';
            $('.grid-stack-item.gr-selected .grid-stack-item-content')[0].innerHTML = `<img class="gr-image-widget" data-pk_content="${$(this).attr('data-pk_content')}" src="${$(this).attr('data-img_content')}"><div class="gr-text-widget">${$(this).attr('data-title_content')}</div>`;
        }
    }
    
    $('.gr-set-content-to-block').bind( "click", onSetContentToBlock );
    
    /**
     * после поиска pjax
     */
    $(document).on('pjax:success', function() {
        $('.gr-set-content-to-block').unbind( "click", onSetContentToBlock );
        $('.gr-set-content-to-block').bind( "click", onSetContentToBlock );
    });
    
    
    /**
     * клик на блок и выделение
     */
    var onClickGR = function() {
        $('.grid-stack-item.gr-selected').removeClass("gr-selected");
        $(this).addClass("gr-selected");
    }
    
    $('.grid-stack-item').bind( "click", onClickGR );
    
    /**
     * при добавлении нового блока
     */
    $('.grid-stack').on('added', function() {
        $('.grid-stack-item').unbind( "click", onClickGR );
        $('.grid-stack-item').bind( "click", onClickGR );
    });
});

var options = {
    width: 12,
    float: true,
    removable: true,
    removeTimeout: 100,
    cellHeight: 100,
    disableResize: true,
    verticalMargin: 3
};
$('.grid-stack').gridstack(options);
new function () {
    this.serializedData = [{ "x": 0, "y": 4, "width": 4, "height": 1 }, { "x": 6, "y": 3, "width": 6, "height": 1 }, { "x": 0, "y": 3, "width": 6, "height": 1 }, { "x": 8, "y": 4, "width": 4, "height": 1 }, { "x": 4, "y": 4, "width": 4, "height": 1 }, { "x": 0, "y": 1, "width": 3, "height": 1 }, { "x": 3, "y": 1, "width": 3, "height": 1 }, { "x": 6, "y": 1, "width": 3, "height": 1 }, { "x": 9, "y": 1, "width": 3, "height": 1 }, { "x": 0, "y": 2, "width": 12, "height": 1 }];
    this.grid = $('.grid-stack').data('gridstack');
    this.loadGrid = function () {
        this.grid.removeAll();
        var items = GridStackUI.Utils.sort(this.serializedData);
        _.each(items, function (node) {
            $('.grid-stack').data('gridstack').addWidget($('<div><div class=\"grid-stack-item-content\" /><div/>'),
                node.x, node.y, node.width, node.height);
        }, this);
        return false;
    }.bind(this);
    this.saveGrid = function () {
        this.serializedData = _.map($('.grid-stack > .grid-stack-item:visible'), function (el) {
            el = $(el);
            var node = el.data('_gridstack_node');
            return {
                x: node.x,
                y: node.y,
                width: node.width,
                height: node.height,
                pk_content: parseInt(el.find('.gr-image-widget').attr('data-pk_content'), 10)
            };
        }, this);
        
        // сортировка по порядку, x y, слева направо
        this.serializedData = _.sortBy(_.sortBy(this.serializedData, 'x'), 'y');
        $('#saved-data').val(JSON.stringify(this.serializedData, null, '    '));
        return false;
    }.bind(this);
    
    /**
     * очистить все
     */
    this.clearGrid = function () {
        this.grid.removeAll();
        return false;
    }.bind(this);
    
    /**
     * добавление блоков при нажатии кнопки с атрибутами
     */
    this.addWidget = function(el) {
        var x = parseInt(el.target.getAttribute('data-x'), 10);
        var y = parseInt(el.target.getAttribute('data-y'), 10);
        var node = {
            x: 0,
            y: 0,
            width: x,
            height: y,
            auto_position:true
        };
        var item = $(`<div><div class="grid-stack-item-content" size="${x}x${y}" /><div class="grid-stack-item-inner"></div><div/>`);
        this.grid.addWidget(item, node.x, node.y, node.width, node.height);
    }.bind(this);
    $(".gr-add_widget").click(this.addWidget);

    $('#save-grid').click(this.saveGrid);
    $('#load-grid').click(this.loadGrid);
    $('#clear-grid').click(this.clearGrid);
    this.loadGrid();
};