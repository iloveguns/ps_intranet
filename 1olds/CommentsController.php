<?php

namespace app\commands;

/**
 * переносил комментарии старые в новые через такую процедуру
 */
class CommentsController extends \yii\console\Controller {
    /**
     * сначала check
     * цель - удаление обертки от редактора <p>...</p> => <br>
     */
    public function actionIndex() {
        $counts = 0;
        
        foreach (\app\models\Comments::find()->select(['id_comment', 'text'])->orderBy('id_comment DESC')->asArray()->each() as $model) {//->each()
            if(strpos($model['text'], '<p>') === false) {//уже кодирован
                $text = html_entity_decode($model['text']);
            } else {
                $text = $model['text'];
            }
            
            try {
                $dom = new \DOMDocument;
                $dom->loadHTML($text);
                $countp = $dom->getElementsByTagName('p')->length;

                if($countp === 1) {
                    //если 1 тэг <p>...</p>, то его полностью удалить
                    continue;
                    $string = preg_replace("/<p>(.+?)<\/p>/is", "$1", $text);                        
                } elseif($countp === 0) {
                    continue;
                } else {
                    $string = str_replace("<p>", "", $text);
                    $string = str_replace("</p>", "<br>", $string);
                    if(substr($string, -4) === '<br>') {
                        $string = substr($string, 0, -4);
                    }
                }

                //сохранение
                $counts++;
                \Yii::$app->db->createCommand("UPDATE ". \app\models\Comments::tableName() . " SET `text` = '" . htmlentities($string) . "' WHERE id_comment = " . $model['id_comment'])->execute();
            } catch (\Exception $e) {//дохуя просто некорректных
                continue;
            }
        }
        echo $counts . ' комментариев';
    }
    
    /**
     * проверка валидности комментариев(html)
     */
    public function actionCheck(){
        $wrongs = [];
        
        foreach (\app\models\Comments::find()->select(['id_comment', 'text'])->orderBy('id_comment DESC')->asArray()->each() as $model) {
            if(strpos($model['text'], '<p>') === false) {//уже кодирован
                $text = html_entity_decode($model['text']);
            } else {
                $text = $model['text'];
            }
            
            if(strlen($text) < 1) {//с пустым текстом удалить
                \Yii::$app->db->createCommand("DELETE FROM ". \app\models\Comments::tableName() . " WHERE id_comment = " . $model['id_comment'])->execute();
            } else {                
                try {
                    $dom = new \DOMDocument;
                    $dom->loadHTML($text);
                    $countp = $dom->getElementsByTagName('p')->length;
                } catch (\Exception $e) {//дохуя просто некорректных
                    $wrongs[] = $model['id_comment'];
                    continue;
                }
            }
        }
        
        var_dump($wrongs);
    }
}

/**
    $string = '<p>Text</p><p>Text</p>';
    $string = preg_replace("/<p>(.+?)<\/p>/is", "$1", $string);
    echo($string);
 */
