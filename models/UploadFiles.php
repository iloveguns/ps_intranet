<?php

namespace app\models;

use Yii;

class UploadFiles extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'upload_files';
    }

    public function rules()
    {
        return [
            [['class', 'item_id', 'id_user', 'name', 'path'], 'required'],
            [['item_id', 'id_user'], 'integer'],
            [['name', 'path'], 'string'],
            [['class'], 'string', 'max' => 128],
            [['label', 'attr'], 'string', 'max' => 50]//в метке можно любую инфу указывать, например главное фото или метку множеств фото модели, например аватар, профиль
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_file' => Yii::t('app', 'Id File'),
            'class' => Yii::t('app', 'Class'),
            'item_id' => Yii::t('app', 'Item ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'name' => Yii::t('app', 'Name'),
            'path' => Yii::t('app', 'Path'),
        ];
    }

    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }
    
    /*
     * физическое удаление файлов
     * $class   - class
     * $item_id - pk сущности(проект, задача и тд)
     */
    public function deleteFiles($class, $item_id) {
        foreach (UploadFiles::findAll(['item_id' => $item_id, 'class' => $class]) as $file) {
            $pathfile = \Yii::getAlias('@webroot').$file->path;
            @unlink($pathfile);
        }
    }
    
    /*
     * вывод превью сохраненной картинки
     * echo ::showImg($model, 'labelfile', ['class' => 'zalupa']);
     */
    public function showImg($model, $attr, $del_button = true, $label = '', $options = []){
        if($foundFiles = self::findFiles($model, $attr, $label)) {
            if(!isset($options['class'])) $options['class'] = '';
            $options['style'] = Yii::$app->params['photoAtAdminUploadParams'];
            $options['class'] .= ' img-thumbnail';
            
            $html = '';
            
            foreach ($foundFiles as $file) {
                $html .= \Yii::$app->view->render('//upload_files/img_file', ['file' => $file, 'del_button' => $del_button, 'options' => $options]);//вывести простой вид файла
            }
        } else {
            return false;
        }
        
        return $html;
    }
    
    /*
     * простой вывод файлов(название, ссылка) + кнопка удаления
     */
    public function showFilesSimple($model, $attr, $del_button = true, $label = '') {
        if($foundFiles = self::findFiles($model, $attr, $label)) {
            $html = '';
            
            foreach ($foundFiles as $file) {
                $html .= \Yii::$app->view->render('//upload_files/simple_file', ['file' => $file, 'del_button' => $del_button]);//вывести простой вид файла
            }
            
            return $html;
        } else {
            return false;
        }
    }
    
    /*
     * найти файлы модели по атрибуту и метке
     */
    private function findFiles($model, $attr, $label = '') {
        $files = UploadFiles::find()->where(['class' => $model->className(), 'item_id' => $model->getPrimaryKey()])
                ->andWhere(['or', ['attr' => $attr], ['attr' => NULL]]);// => NULL - обратная совместимость(в новых версиях убрать до ['attr' => $attr])
        if(!empty($label)) $files->andWhere(['label' => $label]);
        
        return $files->all();
    }
    
    /*
     * поле формы для загрузки картинки
     * обычный kartik input file с вариациями параметров
     * 
     * $form - ActiveForm
     * $model - модель, которой нужно загрузить
     * $attr - атрибут модели
     */
    public function showUploadFormField($form, $model, $attr, $options = [], $pluginOptions = []) {
        if(!isset($options['accept'])) $options['accept'] = 'image/*';//'accept' => '*' для загрузки всего, по умолчанию - только изображения
        if(!isset($pluginOptions['showUpload'])) $pluginOptions['showUpload'] = false;
        
        return $form->field($model, $attr)->widget(\kartik\file\FileInput::classname(), [
            'options' => $options,
            'pluginOptions' => $pluginOptions,
        ]);
    }
    
    /*
     * загрузка файлов только для моделей
     * $model - модель, которой нужно загрузить
     * $attr - атрибут модели
     * $saveLabel(string) - метка для сохранения()
     * $removeOld(bool) - удалить старые файлы (все) +физически
     */
    public function uploadFiles($model, $attr, $removeOld = false, $saveLabel = '') {
        $files = \yii\web\UploadedFile::getInstances($model, $attr);
        if($files) {
            if($removeOld) {
                self::removeFiles($model, $attr);                
            }

            foreach ($files as $file) {
                $a = explode(".", $file->name);
                $b = end($a);
                $namem = Yii::$app->params['uploadFilesFolder'] . DIRECTORY_SEPARATOR . Yii::$app->security->generateRandomString().".{$b}";
                $path = isset($namem) ? Yii::getAlias('@webroot') . $namem : null;
                if($file->saveAs($path)){
                    $modelf = new \app\models\UploadFiles();
                    $modelf->item_id = (int) $model->getPrimaryKey();
                    $modelf->id_user = Yii::$app->user->id;
                    $modelf->class = $model->className();
                    $modelf->name = $file->name;
                    $modelf->attr = $attr;//метка атрибута файла
                    if(!empty($saveLabel)) {
                        $modelf->label = $saveLabel;//просто метка, чтоб найти конкретный атрибут
                    }
                    $modelf->path = $namem;
                    $modelf->save();
                } else {
                    Yii::error('Файл не сохранен в папку', UploadFiles::className());
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    /*
     * удаление файлов
     * @param {Object} $model   - модель, у которой удаляются файлы
     * @param {String} $attr    - атрибут удаляемого файла
     * @param {Bool} $all       - удалить ли все файлы модели
     */
    public function removeFiles($model, $attr, $all = false) {
        $query = self::find()
                ->select(['path', 'id_file'])
                ->asArray()
                ->where(['class' => $model->className(), 'item_id' => $model->getPrimaryKey()]);
        if($all !== TRUE) $query = $query->andWhere(['attr' => $attr]);
        $oldfiles = $query->all();
        
        if(empty($oldfiles)) {
            return true;
        } else {
            $deleteArr = [];

            foreach ($oldfiles as $ofile) {
                @unlink(Yii::getAlias('@webroot').$ofile['path']);
                array_push($deleteArr, $ofile['id_file']);
            }

            return UploadFiles::deleteAll('id_file IN ('.implode(',', $deleteArr).')');
        }
    }
}