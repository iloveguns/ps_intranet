<?php

namespace app\models;

use Yii;
use app\modules\user\models\User;

class GraphicRelate extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'graphic_relate';
    }

    public function rules()
    {
        return [
            [['title_gr', 'html_gr', 'json_connect_gr', 'fk_user_gr'], 'required'],
            [['html_gr', 'json_connect_gr'], 'string'],
            [['fk_user_gr'], 'integer'],
            [['title_gr'], 'string', 'max' => 128],
            [['fk_user_gr'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_user_gr' => 'id_user']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'pk_gr' => Yii::t('app/models', 'Pk Gr'),
            'title_gr' => Yii::t('app/models', 'Title Gr'),
            'html_gr' => Yii::t('app/models', 'Html Gr'),
            'json_connect_gr' => Yii::t('app/models', 'Json Connect Gr'),
            'fk_user_gr' => Yii::t('app/models', 'Fk User Gr'),
        ];
    }

    public function getFkUserGr()
    {
        return $this->hasOne(User::className(), ['id_user' => 'fk_user_gr']);
    }
}