<?php

namespace app\models;

use Yii;
use app\modules\user\models\User;

class UsersOnline extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_online';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pk_user'], 'required'],
            [['pk_user', 'now_online'], 'integer'],
            [['last_online'], 'safe'],
            [['pk_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['pk_user' => 'id_user']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_user' => 'Pk User',
            'last_online' => 'Last Online',
            'now_online' => 'Now Online',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPkUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'pk_user']);
    }
}