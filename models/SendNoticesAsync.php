<?php

namespace app\models;

use Yii;

/*
 * Класс для отправки уведомлений(почта, телеграм) не сразу, а вызовом скрипта
 */
class SendNoticesAsync extends \yii\db\ActiveRecord {
    public static function tableName() {
        return 'send_notices_async';
    }

    public function rules() {
        return [
            [['item_class', 'item_id', 'type', 'fk_user'], 'required'],
            [['item_id', 'type', 'fk_user', 'is_sended', 'from_user'], 'integer'],
            [['item_class'], 'string', 'max' => 50],
        ];
    }

    public function attributeLabels() {
        return [
            'pk_sna' => Yii::t('app/models', 'Pk Sna'),
            'item_class' => Yii::t('app/models', 'Item Class'),
            'item_id' => Yii::t('app/models', 'Item ID'),
            'type' => Yii::t('app/models', 'Type'),
            'fk_user' => Yii::t('app/models', 'Fk User'),
            'is_sended' => Yii::t('app/models', 'Is Sended'),
            'from_user' => Yii::t('app/models', 'from_user'),
        ];
    }
}