<?php
namespace app\models;

use Yii;
use GuzzleHttp\Exception\ClientException;
use unreal4u\TelegramAPI\TgLog;
use unreal4u\TelegramAPI\Telegram\Methods\SendMessage;

/*
 * отправка уведомлений в телеграм
 * ALTER TABLE `user` ADD `telegram_chatid` INT UNSIGNED NULL DEFAULT NULL AFTER `lng`;
 */
class sendTelegram {
    
    /*
     * @param $id_chats {array} - список ид, которым нужно отправить телеграм
     */
    function send($id_chats, $data) {
        if(!$data['text'] || empty($id_chats)) return;//отправляемый текст или пустой массив получателей
        
        $sendMessage = new SendMessage();
        $sendMessage->parse_mode = 'HTML';
        $sendMessage->text = $data['text'];
        $tgLog = new TgLog('216887227:AAFsZxLTQxYpU0AeduIeOJu8g7dkMphxbr8');//ид бота, который отправляет
        
        foreach ($id_chats as $id_chat) {
            $sendMessage->chat_id = $id_chat;
            
            try {
                $tgLog->performApiRequest($sendMessage);
            } catch (ClientException $e) {
                Yii::error('Telegram class doesn\'t send a message' . json_encode($e->getMessage()));
                echo '<pre>';
                var_dump($e->getMessage());
                echo '</pre>';
            }
        }
    }
}