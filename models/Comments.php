<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * комментарии сотрудников ко всему. общая
 */
class Comments extends ActiveRecord implements \app\interfaces\ClassnameInterface
{
    const DELETED = 1;//удаленный коммент
    const NOT_DELETED = 0;//активный коммент

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date'],
                ],
                'value' => function() { return \app\models\FunctionModel::getDateTimestamp();},
            ],
        ];
    }
    
    public static function tableName()
    {
        return 'comments';
    }

    public function rules()
    {
        return [
            [['item_id', 'class', 'text'], 'required'],
            [['item_id', 'id_user', 'reply_to', 'deleted', 'changed'], 'integer'],
            [['text'], 'string'],
            [['create_date'], 'safe'],
            [['class'], 'string', 'max' => 128],
            [['text'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],//xss
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_comment' => Yii::t('app', 'Id Comment'),
            'item_id' => Yii::t('app', 'Item ID'),
            'class' => Yii::t('app', 'Class at comment'),
            'id_user' => Yii::t('app', 'Id User at comment'),
            'text' => Yii::t('app', 'Text comment'),
            'create_date' => Yii::t('app', 'Create Date'),
            'reply_to' => Yii::t('app', 'Reply comment')
        ];
    }
    
    /*
     * по умолчанию - сортировка по дате
     */
    public static function find()
    {
        return parent::find()->orderBy('create_date DESC');
    }
    
    /*
     * html представление конкретного коммента
     * используется в виджете для вывода
     * $comment->toHtml()
     */
    public function toHtml() {
        return \Yii::$app->view->render('//comment/one', ['comment' => $this]);
    }
    
    public function beforeDelete() {
        if (parent::beforeDelete()) {
            //удалить рейтинги комментария
            \Yii::$app->db->createCommand("DELETE FROM `ratings` WHERE `item_id` = " . $this->id_comment. " AND `class` = '".self::getName()."'")->execute();
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * получить пользователя по комменту
     */
    public function getIdUser()
    {
        $fu = $this->hasOne(\app\modules\user\models\User::className(), ['id_user' => 'id_user']);
        if(isset($fu->primaryModel->id_user)) return $fu;//если найден - отправить
        
        $fu = new \app\modules\user\models\NullUser();
        return $fu;//если нет - интранет
    }

    public function getName() {
        return 'comments';
    }

}
