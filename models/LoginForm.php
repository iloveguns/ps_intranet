<?php
/*
 * производит вход в систему и запись попыток входа
 */
namespace app\models;

use Yii;
use yii\base\Model;
use app\modules\user\models\User;
use app\modules\user\models\UserStatus;

class LoginForm extends \yii\db\ActiveRecord
{
    const CACHE_KEY = 'SIGNIN_TRIES';
    
    public $email;
    public $password;
    public $rememberMe = true;

    private $_user = false;

    public static function tableName()
    {
        return 'loginform';
    }
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }
    
    public function attributeLabels(){
        return[
            'password' => Yii::t('app/models', 'Password'),
            'time' => Yii::t('app', 'Time'),
            'rememberMe' => Yii::t('app/models', 'rememberMe'),
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            
            if (!$user || !($this->password === $user->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        $cache = Yii::$app->cache;
        $cache->flush();//ОХУЕЛ?
        if(($tries = (int)$cache->get(self::CACHE_KEY.'-'.$_SERVER['HTTP_USER_AGENT'])) > 5){
            $this->addError('password', Yii::t('app', 'You tried to login too often. Please wait 5 minutes.'));
            return false;
        }
        
        $this->ip = $_SERVER['REMOTE_ADDR'];
        $this->user_agent = $_SERVER['HTTP_USER_AGENT'];
        $this->time = FunctionModel::getDateTimestamp();
        $this->input_email = $this->email;
        
        if ($this->validate()) {
            $this->input_password = '******';
            $this->success = 1;
            
        }else {
            $this->success = 0;
            $this->input_password = $this->password;
            $cache->set(self::CACHE_KEY.'-'.$_SERVER['HTTP_USER_AGENT'], ++$tries, 300);
        }
        $this->insert(false);
        
        return $this->success ? Yii::$app->user->login($this->getUser(), 3600*24*365) : false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUseremail($this->email);
        }
        if($this->_user){//если нашлось
            if($this->_user->fkStatus === NULL){
                $this->addError('email', Yii::t('app/models', 'Your registration not confirmed'));
            }
            else if($this->_user->fkStatus->status == UserStatus::FIRED) {
                $this->addError('email', Yii::t('app/models', 'You fired'));
                //return false;//уволен
            }
        }
        return $this->_user;
    }
}
