<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use app\modules\user\models\Units;
use app\modules\intranet\models\Events;
use app\modules\intranet\models\Bugtracker;
use app\modules\intranet\models\Callboard;
use app\modules\intranet\models\Tasks;
use app\modules\intranet\models\Projects;
use app\modules\intranet\models\Blogs;
use app\modules\crm\models\CrmTasks;
use app\nodejs\socketio\php\sendSocketIo;

/**
 * модель для сохранения непрочитанных уведомлений при их создании например. чтоб сотрудник мог посмотреть все, если долго не заходил
 * UPD май 2017
 * расширение класса, добавление новых полей и логики
 * 
 * обязательные поля для минимального уведомления:
 * class {string} - имя класса(бывший className())
 * item_id {int}  - ид модели класса
 * id_user {int}  - ид пользователя, которому уведомление
 * type {int}     - тип уведомления(расшифровка хранится в константах класса)
 * 
 * дополнительные поля (NULL)
 * from_user {int} - от кого, кто создал уведомление 
 * to_id {int}     - ид класса, к которому привязано уведомление
 * to_name {string}- имя класса, к которому привязано уведомление
 */
class UnreadNotice extends \yii\db\ActiveRecord {
    //в принципе можно любые типы сделать на любое действие - уведомление
    const NEW_COMMENT = 1;//новый комментарий
    const REPLY_COMMENT = 2;//ответ на комментарий
    const ADD_NEW_USERTO = 3;//новый сотрудник к существующему(задаче) К ЧЕМУ УГОДНО
    const NEW_REGISTER_USER = 10;//новый зарегавшийся
    const NEW_CONGRAT = 20;//новое поздравление
    const NEW_CALLBOARD = 30;//новое уведомление
    const NEW_TASK = 40;//новая задача
    const NEW_LOAD_FILE = 50;//новый загруженный файл
    const CHANGE_STATUS = 60;//смена статуса
    const NEW_EVENT = 70;//новое событие
    const NOTIFY_EVENT = 71;//уведомление о событии(в настройках его)
    const NEW_BUGTRACKER = 100;//объявление в баг-трекер
    const NEW_SYSADMINQUERY = 101;//системному админу заявку
    const NEW_STEWARDQUERY = 102;//завхозу
    const CRM_DONE_YOUR_TASK = 200;//выполнена порученная задача
    const API_MAIL_SENDING_DONE = 300;//рассылка email завершена
    
    //сообщения вообще отдельно
    const NEW_MESSAGE = 2;//новое сообщение
    
    const READED_NOTREAD = 0;
    const READED_READ = 1;//просмотрено уведомление
    
    public static function tableName() {
        return 'unread_notice';
    }

    public function rules() {
        return [
            [['datetime'], 'safe'],
            [['class', 'item_id', 'id_user', 'type'], 'required'],
            [['item_id', 'id_user', 'from_user'], 'integer'],
            [['class'], 'string', 'max' => 128]
        ];
    }

    public function attributeLabels() {
        return [
            'id_notice' => Yii::t('app/models', 'Id Notice'),
            'datetime' => Yii::t('app/models', 'DateTime'),
            'class' => Yii::t('app', 'Class'),
            'item_id' => Yii::t('app', 'Item ID'),
            'readed' => Yii::t('app/models', 'Readed notify'),
            'id_user' => Yii::t('app', 'Id User'),
        ];
    }

    public function getIdUser() {
        return $this->hasOne(\app\modules\user\models\User::className(), ['id_user' => 'id_user']);
    }
    
    /*
     * новая функция, которая сохраняет в бд и рассылает ПРОСТО УВЕДОМЛЕНИЕ. все однотипно и просто
     * 
     * СЛОЖНОВАТО, ПЕРЕДЕЛАТЬ
     * 
     * берет ид из Units
     * $model - модель
     * $type - тип(новое сообщ, задача или тд тп)
     * @param {array} $users - массив ид сотрудников
     */
    public function saveByIds($model, $type, $users = false, $unsetme = true) {
        $usersToIO = [];
        
        /* доп. переменные и проверка */
        $from_user = NULL;
        
        if($model->getName() == Bugtracker::getName()) {
            $from_user = Yii::$app->user->id;
        }
        /* end доп. переменные и проверка */
        
        // должно быть так, т.к. не передается массив сюда
        if(!$users){
            $users = Units::onlyIdUsers($model, true, true);//найти ид всех с ответственным
        }
        
        //если не выбрано никого - отсылать всем
        if(count($users) < 1 && $type != UnreadNotice::NEW_EVENT){//если нет сотрудников - выбрать всех
            $users = \app\modules\user\models\User::getAllActive(false, true);
        }
        
        //затем удалить отправителя
        if($unsetme) {
            unset($users[Yii::$app->user->id]);//убрать из рассылки отправителя
        }
        
        // подготовка к сохранению
        $toAsync = [];
        
        foreach ($users as $user) {
            if($user === NULL) continue; // случалось и такое
            
            $toAsync[] = [
                $model::getName(),
                $model->getPrimaryKey(),
                $type,
                $from_user,
                $user
            ];
        }        
        
        if(count($toAsync) > 0) {        
            Yii::$app->db->createCommand()->batchInsert(SendNoticesAsync::tableName(), ['item_class', 'item_id', 'type', 'from_user', 'fk_user'], $toAsync)->execute();

            // только контроллер и действие правильно указать
            shell_exec('php ' . Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'yii user/sendnoticesasync > /dev/null 2>/dev/null &');
        }
        return true;
        
        // дальше перенесено в консольное приложение для распараллеливания нагрузки
        
        /*if($users){//если есть они вообще
            $class = $model::getName();
            $pk = $model->getPrimaryKey();
            $arrSaveData = [];
            
            var_dump($class, $pk, $users, $type);
            die();
            
            if(Yii::$app->params['sendNotify']) {//если указана отправка во внешние источники(можно еще раньше проверять)
                // сохранить в таблицу для рассылки параллельно
                
                $arrSaved = [];//для хранения уже сохраненных уведомлений
                
                // найти всех, кому надо отправить в почту
                $sendEmails = \app\modules\user\models\User::find()->asArray()->select(['id_user', 'email'])->where(['setting_notify_email' => 1])->andWhere(['in', 'id_user', $users])->all();
                if($sendEmails) {
                    //Yii::error($sendEmails, 'отправить почту');
                    //отправка почты
                    foreach ($sendEmails as $idemail) {//сохранять по одиночке 
                        $m = new UnreadNotice();
                        $m->class = $class;
                        $m->item_id = $pk;
                        $m->type = $type;
                        $m->id_user = $idemail['id_user'];
                        $m->from_user = $from_user;
                        $m->save();
                        
                        $api = self::ApiNoticesUser($idemail['id_user'], false, 1);
                        $one = array_shift($api['notices']);
                        $link = \app\modelsView\ViewUnreadNotice::newHtmlNotify($one, false);
                        
                        $arrSaved[$idemail['id_user']] = ['id_notice' => $m->id_notice, 'link' => $link];
                        
                        //@\app\helpers\Mail::send($idemail['email'], Yii::t('app', 'New Notification'), ['text' => $link], '@app/mail/notify/notify');
                    }
                }
                // end найти всех, кому надо отправить в почту

                // найти всех, кому надо отправить в телеграм
                $sendTelegram = \app\modules\user\models\User::find()->asArray()->select(['id_user', 'telegram_chatid'])->where(['not', ['telegram_chatid' => NULL]])->andWhere(['in', 'id_user', $users])->all();
                if($sendTelegram) {
                    $sendTelegramIds = [];
                    
                    foreach ($sendTelegram as $sendT) {
                        if(isset($arrSaved[$sendT['id_user']])) {//уже сохранено уведомление
                            $link = $arrSaved[$sendT['id_user']]['link'];
                        } else {//сохранять по одиночке 
                            $m = new UnreadNotice();
                            $m->class = $class;
                            $m->item_id = $pk;
                            $m->type = $type;
                            $m->id_user = $sendT['id_user']; 
                            $m->from_user = $from_user;
                            $m->save();
                            
                            $api = self::ApiNoticesUser($sendT['id_user'], false, 1);
                            $one = array_shift($api['notices']);
                            $link = \app\modelsView\ViewUnreadNotice::newHtmlNotify($one, false);
                            
                            $arrSaved[$sendT['id_user']] = ['id_notice' => $m->id_notice, 'link' => $link];
                        }
                        @sendTelegram::send([$sendT['telegram_chatid']], ['text' => $link]);//по одиночке
                    }
                }
                // end найти всех, кому надо отправить в телеграм
                
                // собрать ид пользователей для отправки в сокет
                foreach ($arrSaved as $id => $saved) {
                    array_push($usersToIO, $id);
                }
            }
            die();
            
            // сохранение всех остальных, кому ни в почту, ни в телеграм не надо просто и быстро
            foreach ($users as $id) {
                if(empty($id) || in_array($id, $usersToIO)) {//какие то ошибки начали падать, что пустой ид + если уже создан заранее = пропустить
                    continue;
                }
                array_push($usersToIO, $id);
                $arrSaveData[] = [$class, $pk, $id, $type, $from_user];
            }
            
            unset($users);
            
            if(!empty($arrSaveData)) {
                Yii::$app->db->createCommand()->batchInsert(UnreadNotice::tableName(), ['class', 'item_id', 'id_user', 'type', 'from_user'], $arrSaveData)->execute();
            }
            // end сохранение всех остальных
            
            //и отправка уведомлений массовая
            $io = new sendSocketIo();
            $io->send('notify', ['uniqid' => $usersToIO, 'updateNotify' => true]);//отправить всем метку о том, что надо обновить кол-во уведомлений у сервера
        }*/
        // end дальше перенесено в консольное приложение для распараллеливания нагрузки
        
        return true;
    }
    
    /*
     * кол-во уведомлений
     * @param {int} $id_user    - ид сотрудника
     * @returns {int}
     */
    public function ApiNoticesUserCount($id_user) {
        return self::find()->asArray()->where(['id_user' => $id_user])->count();
    }
    
    /*
     * возвращать массив, вся логика здесь
     * 
     * UPD 25.05.2018 - собирать похожие уведомления(множественные комментарии). ставить метку 'repeat' => сколько раз повторяется
     * 
     * @param {int} $id_user    - ид сотрудника
     * @param {bool} $onlyNew   - искать только новые(без архивных)
     * @param {int} $limit      - ограничение выборки из базы
     * @param {int} $offset     - смещение выборки из базы
     * @returns {array}
     */
    public function ApiNoticesUser($id_user, $onlyNew = false, $limit = 15, $offset = 0) {
        $r = [];
        
        $count_notices = Yii::$app->db->createCommand(
            'SELECT count(*) AS count FROM ' . self::tableName() . ' WHERE `id_user` = ' . $id_user . ' AND `readed` = ' . self::READED_NOTREAD . ';'
        )->queryOne()['count'];
         
        $r['count_notices'] = $count_notices;
        
        if($onlyNew || $count_notices > 0) {//вернуть новые(непрочитанные)
            $notices = self::findNotices($id_user, $limit, self::READED_NOTREAD, $count_notices);
            $r['type'] = 'new';
        } elseif(!$onlyNew && $notices = self::findNotices($id_user, $limit, self::READED_READ, $count_notices)) {//вернуть старые
            $r['type'] = 'archive';
        } else {
            return $r;
        }
        
        $tmp = [];
        
        //создать ответ в виде массива
        foreach ($notices as $notice) {
            // собирать однотипные события в повторения
            if($notice['type'] == UnreadNotice::NEW_COMMENT) {
                $name = $notice['item_id'] . '-' .$notice['class'];

                if(isset($tmp[$name])) {
                    $r['notices'][$tmp[$name]['key']]['repeat'] += 1;
                    $tmp[$name]['repeat'] += 1;
                    continue; // можно не вычислять похожие
                } else {
                    $tmp[$name] = ['repeat' => 1, 'key' => $notice['id_notice']];
                }
            }
            
            $linked = $to = $from = [];
            
            if($notice['item_id'] && $notice['class']) {
                $linked = self::getSimpleDataByName($notice['class'], $notice['item_id']);
            }
            
            if($notice['to_id'] && $notice['to_name']) {//если есть имя и ид привязанной доп. модели
                $to = self::getSimpleDataByName($notice['to_name'], $notice['to_id']);
            }
            
            if($notice['from_user']) {//если есть ид создавшего сотрудника
                $user = \app\modules\user\models\User::find()->select(['lastname', 'name'])->asArray()->where(['id_user' => $notice['from_user']])->one();
                $from = ['name' => $user['lastname'] . ' ' . $user['name']];
            }
            
            $findModel = Units::getModelByNameid($notice['class'], $notice['item_id']);//найти модель(например проекты)
            if(!$findModel) {
                Yii::error('не нашел модель '.$notice['class'].' '.$notice['item_id'], 'не нашел модель');
                continue;
            }
            $notice_title = strip_tags($findModel->getTitleText());//strip_tags - для старых

            $r['notices'][$notice['id_notice']] = [//для удобства gridview
                'notice_id'         => $notice['id_notice'],
                'notice_class'      => $notice['class'],
                'notice_item_id'    => $notice['item_id'],
                'notice_type'       => $notice['type'],
                'notice_title'      => $notice_title,
                'notice_datetime'   => $notice['datetime'],
                'notice_url'        => \yii\helpers\Url::to(['/notify/middle-notice', 'id' => $notice['id_notice']], true),//абсолютный урл добавлен для внешних ссылок
                'from'              => $from,
                'notice_linked'     => $linked,
                'to'                => $to
            ];
            
            if(($notice['type'] == UnreadNotice::NEW_COMMENT) && (isset($tmp[$name]) && isset($tmp[$name]['repeat']))) {
                $r['notices'][$notice['id_notice']]['repeat'] = $tmp[$name]['repeat'];
            }
        }
        
        return $r;
    }
    
    /**
     * поиск уведомлений пользователя, запрос + кэш
     * @param {int} $id_user
     * @param {int} $limit
     * @param {bool} $read
     * @return Array
     */
    protected static function findNotices($id_user, $limit, $read, $count_notices) {
        // кэш зависит от многого
        $key = 'un_' . $id_user . '_' . $read . '_' . $count_notices;
        
        $notices = Yii::$app->cache->get($key);
        
        if($notices === false) {
            // попадаются повторные объявления и тд, нужно только комментарии игнорировать
            // для выборки точного кол-ва уведомлений, даже с повторениями
            $notices = Yii::$app->db->createCommand('SELECT * FROM `'.self::tableName().'`
                WHERE `id_user` = :id_user AND `readed` = :readed
                AND `item_id` IN(
                SELECT item_id FROM(
                    SELECT DISTINCT item_id, class FROM `'.self::tableName().'` WHERE `id_user` = :id_user AND `readed` = :readed
                    ORDER BY `id_notice`  DESC
                    LIMIT :limit
                ) a
                )
                AND `class` IN(
                SELECT class FROM(
                    SELECT DISTINCT item_id, class FROM `'.self::tableName().'` WHERE `id_user` = :id_user AND `readed` = :readed
                    ORDER BY `id_notice`  DESC
                    LIMIT :limit
                ) a
            ) ORDER BY `id_notice` DESC;')
                ->bindValue(':id_user', $id_user)
                ->bindValue(':limit', $limit)
                ->bindValue(':readed', $read)
                ->queryAll();
            
            Yii::$app->cache->set($key, $notices, \Yii::$app->params['cacheTime']);
        }
        
        return $notices;
    }
    
    /*
     * вернуть модель по имени модели 
     * в массиве
     * @title - заголовок
     */
    public function getSimpleDataByName($name, $id) {
        $model = '';
        
        switch ($name) {
            case Comments::getName():
                $model = Comments::find()->select('text')->asArray()->where(['id_comment' => $id])->one();
                $model = ['title' => html_entity_decode($model['text']), 'name' => $name];
                break;
            
            case Callboard::getName():
                $model = Callboard::find()->select('title')->asArray()->where(['pk_callboard' => $id])->one();
                $model = ['title' => $model['title'], 'name' => $name];
                break;
            
            case Tasks::getName():
                $model = Tasks::find()->select('title')->asArray()->where(['pk_task' => $id])->one();
                $model = ['title' => $model['title'], 'name' => $name];
                break;
            
            case Events::getName():
                $model = Events::find()->select('title')->asArray()->where(['pk_event' => $id])->one();
                $model = ['title' => $model['title'], 'name' => $name];
                break;
            
            case Bugtracker::getName():
                $model = Bugtracker::find()->select('text')->asArray()->where(['pk' => $id])->one();
                $model = ['title' => $model['text'], 'name' => $name];
                break;
            
            case Projects::getName():
                $model = Projects::find()->select('title')->asArray()->where(['pk_project' => $id])->one();
                $model = ['title' => $model['title'], 'name' => $name];
                break;
            
            case Blogs::getName():
                $model = Blogs::find()->select('title')->asArray()->where(['pk_blog' => $id])->one();
                $model = ['title' => $model['title'], 'name' => $name];
                break;
            
            case CrmTasks::getName():
                $model = CrmTasks::find()->select('text_task')->asArray()->where(['pk_task' => $id])->one();
                $model = ['title' => $model['text_task'], 'name' => $name];
                break;

            default:
                $model = ['title' => 'не могу найти', 'name' => 'не могу найти'];
                Yii::error('не могу найти класс в ' . $name .' | '.$id);
                break;
        }
        
        return $model;
    }
    
    /*
     * общедоступный метод для метки о просмотре уведомления
     * удалял раньше а теперь ставит метку для архива уведомлений
     */
    public function setReadedNotice($item_id, $classname) {
        UnreadNotice::updateAll(['readed' => UnreadNotice::READED_READ], ['id_user' => Yii::$app->user->id, 'item_id' => $item_id, 'class' => $classname]);
    }
    
    /*
     * удалить все уведомления у конкретного сотрудника
     * return 0 или 1 при успехе
     */
    public function setReadeAllNoticeByIdUser($id_user) {
        return UnreadNotice::updateAll(['readed' => UnreadNotice::READED_READ], ['id_user' => $id_user]);
    }
    
    public function getReaded($id = NULL) {
        $a = [1 => Yii::t('app', 'Yes'), 0 => Yii::t('app', 'No')];
        if($id === NULL) return $a;
        return $a[$id];
    }
    
    /**
     * кол-во непрочитанных сообщений
     * @return String
     */
    public function getCountMessages() {
        //непрочитанные сообщения 
        $countMessages = \Yii::$app->db->createCommand("SELECT COUNT(*) AS c FROM `messages_messages` WHERE `fk_dialog`IN (SELECT `fk_dialog` FROM `messages_dialogs_users` WHERE `fk_user` = ".Yii::$app->user->id.") AND `readed` = 0 AND `fk_user_sender` != ".Yii::$app->user->id)->queryOne();
        $countMessages = ($countMessages['c'] > 0) ? $countMessages['c'] : '';
        return $countMessages;
    }
}
