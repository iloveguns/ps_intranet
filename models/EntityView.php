<?php

namespace app\models;

use Yii;

class EntityView extends \yii\db\ActiveRecord
{
    public static function tableName() {
        return 'entity_view';
    }

    public function rules() {
        return [
            [['class', 'item_id', 'id_user'], 'required'],
            [['item_id', 'id_user'], 'integer'],
            [['class'], 'string', 'max' => 128],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => \app\modules\user\models\User::className(), 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    public function attributeLabels() {
        return [
            'id_entity_view' => Yii::t('app', 'Id Entity View'),
            'class' => Yii::t('app', 'Class'),
            'item_id' => Yii::t('app', 'Item ID'),
            'id_user' => Yii::t('app', 'Id User'),
        ];
    }

    public function getIdUser() {
        return $this->hasOne(\app\modules\user\models\User::className(), ['id_user' => 'id_user']);
    }
    
    /*
     * сохранение в базу просмотра
     */    
    public function saveWatch($class, $item_id) {
        $has = EntityView::find()->asArray()->where([
            'class' => $class,
            'item_id' => $item_id,
            'id_user' => Yii::$app->user->id
        ])->orderBy('id_entity_view DESC')->one();
        
        if($has === NULL) {
            Yii::$app->db->createCommand()->insert(EntityView::tableName(), [
                'class' => $class,
                'item_id' => $item_id,
                'id_user' => Yii::$app->user->id
            ])->execute();
        } else {
            Yii::$app->db->createCommand()->update(EntityView::tableName(), 
            ['date' => FunctionModel::getDateTimestamp()],
            '`class` = "' . $class. '" AND `item_id` = ' . $item_id . ' AND `id_user` = ' . Yii::$app->user->id)->execute();
        }
    }
}
