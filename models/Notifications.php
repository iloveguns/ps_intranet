<?php

namespace app\models;

use Yii;

/*
 * Класс для уведомлений расширеный  API
 */
class Notifications extends \yii\db\ActiveRecord
{
    const NEW_COMMENT = 1;//новый комментарий
    const REPLY_COMMENT = 2;//ответ на комментарий
    const ADD_NEW_USERTO = 3;//новый сотрудник к существующему(задаче)
    const NEW_CALLBOARD = 30;//новое объявление
    const NEW_TASK = 40;//новая задача(добавлен к задаче)
    
    
    public static function tableName() {
        return 'notifications';
    }

    public function rules() {
        return [
            [['datetime'], 'safe'],
            [['type', 'id_user', 'to_id', 'item_id'], 'required'],
            [['type', 'id_user', 'to_id', 'from_user', 'item_id'], 'integer'],
            [['class'], 'string'],
            [['to_name'], 'string', 'max' => 10],
        ];
    }

    public function attributeLabels() {
        return [
            'pk' => Yii::t('app/models', 'Pk'),
            'datetime' => Yii::t('app/models', 'Date'),
            'type' => Yii::t('app/models', 'Type'),
            'id_user' => Yii::t('app/models', 'To User'),
            'to_id' => Yii::t('app/models', 'To ID'),
            'to_name' => Yii::t('app/models', 'To Name'),
            'from_user' => Yii::t('app/models', 'From User'),
            'item_id' => Yii::t('app/models', 'Link Fk'),
        ];
    }
    
    public function getByUserId($user_id, $limit = 10) {
        $ms = self::find()->asArray()->where(['id_user' => $user_id])->limit($limit)->all();
        $ns = [];
        
        foreach ($ms as $m) {
            $linked = $to = $from = [];
            
            if($m['item_id'] && $m['class']) {//если есть имя и ид привязанной доп. модели
                $linked = self::getSimpleDataByName($m['class'], $m['item_id']);
            }
            
            if($m['to_id'] && $m['to_name']) {//если есть имя и ид привязанной модели
                $to = self::getSimpleDataByName($m['to_name'], $m['to_id']);
            }
            
            if($m['from_user']) {//если есть ид создавшего сотрудника
                $user = \app\modules\user\models\User::find()->asArray()->where(['id_user' => $m['from_user']])->one();
                $from = ['name' => $user['lastname'] . ' ' . $user['name']];
            }
            
            $ns[] = ['pk' => $m['pk'], 'type' => $m['type'], 'item_id' => $m['item_id'], 'linked' => $linked, 'to' => $to, 'from' => $from];
        }
        
        return $ns;
    }
}