<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use app\modules\intranet\IntranetModule;

/**
 * таблица для хранения отчетов к чему нибудь
 * text_report - обязательное поле(коммент)
 * text_deniedreport - если заполнено, то это текст отмены отчета
 */
class EntityReport extends ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['date'],
                ],
                'value' => function() { return \app\models\FunctionModel::getDateTimestamp();},
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'entity_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class', 'item_id'], 'required'],
            [['item_id'], 'integer'],
            [['text_report', 'text_deniedreport', 'date'], 'string'],
            [['class'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_report' => Yii::t('app/models', 'Id Report'),
            'class' => Yii::t('app/models', 'Class'),
            'item_id' => Yii::t('app/models', 'Item ID'),
            'text_report' => Yii::t('app/models', 'Text Report'),
            'text_deniedreport' => Yii::t('app/models', 'Text Deniedreport'),
            'date' => Yii::t('app/models', 'Date'),
        ];
    }
}
