<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\modules\user\models\User;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\app\modules\user\models\User',
                //'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            //'status' => \app\modules\user\models\UserStatus::St,
            'email' => $this->email,
        ]);

        if (!$user) {
            return false;
        }
            
        $password = Yii::$app->security->generateRandomString(8);//. '_' . time()
        
        $update = Yii::$app->db->createCommand()
            ->update(User::tableName(), ['password' => $password], 'email = :email')
            ->bindValue(':email', $this->email)
            ->execute();
        

        return \app\helpers\Mail::send($this->email, Yii::t('app', 'Reset Password'), ['password' => $password], '@app/mail/resetpassword/index');
    }
}
