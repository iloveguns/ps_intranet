<?php
/*
 * функции, полезные для всего
 */
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Comments;
use app\modules\intranet\models\Tasks;
use app\modules\intranet\models\Callboard;
use app\modules\intranet\models\Events;
use app\modules\intranet\models\Projects;
use app\modules\intranet\models\Bugtracker;
use app\modules\intranet\models\Blogs;

class FunctionModel extends Model {
    /*
     * текущее время timestamp php
     */
    public function getDateTimestamp() {
        return FunctionModel::getDateWParam(Yii::$app->params['saveModelDateTime']);
    }
    
    /*
     * вернуть любую дату в текущем времени
     */
    public function getDateWParam($param) {
        date_default_timezone_set(Yii::$app->params['timeZone']);//так норм
        return date($param, time());
    }
    
    /*
     * $date = Yii::$app->params['saveModelDateTime'];
     * $modify(string) '+1 day'
     */
    public function dateModify($date, $modify, $format = NULL) {
        if($format === NULL) $format = Yii::$app->params['saveModelDateTime'];
        $date = \DateTime::createFromFormat(Yii::$app->params['saveModelDateTime'], $date);
        if(!$date) return 'Не получается сделать дату из этой даты';
        $date->modify($modify);
        return $date->format($format);
    }
    
    /*
     * вернуть день недели(навание) из даты
     * например getWeekday('2017-03-08 10:00:00'); // returns Wednesday
     */
    public function getWeekday($date) {
        $dow_numeric = date('w', strtotime($date));
        $dow_text = date('l', strtotime("Sunday +{$dow_numeric} days"));
        return $dow_text;
    }
    
    /*
     * получить название базы данных
     */
    public function getDsnAttribute($name, $dsn) {
        if (preg_match('/' . $name . '=([^;]*)/', $dsn, $match)) {
            return $match[1];
        } else {
            return null;
        }
    }
    
    /*
     * str_replace только один раз и первое вхождение(в структуре были проблемы с заменой всех значений числа 10 : 10, 10*, *10)
     */
    function str_replace_once($search, $replace, $text){ 
        $pos = strpos($text, $search); 
        return $pos!==false ? substr_replace($text, $replace, $pos, strlen($search)) : $text; 
    }
    
    /*
     * список месяцев
     */
    function getMonths($mt = NULL) {
        $months = [];
        for ($m=1; $m<=12; $m++) {
            $month = date('F', mktime(0,0,0,$m, 1, date('Y')));
            $months[$m] = Yii::t('app', $month);
        }
        if($mt) return $months[$mt];        
        return $months;        
    }
    
    function getDays() {//дни 1-31
        $days = [];
        for ($m=1; $m<=31; $m++) {
            $days[$m] = $m;
        }
        
        return $days;        
    }
    
    /*
     * 00:00 - 23:30 список времени
     * $allday (bool)   - включить доп. вариант ВЕСЬ ДЕНЬ
     * $id (int)        - время для выдачи
     * $full (bool)     - каждую минуту или по полчаса
     */
    function getTimes($allday = false, $id = NULL, $full = true){
        $times = [];
        if($full) $mf = 1;
        else $mf = 30;
        
        if($allday)
            $times['allday'] = Yii::t('app', 'allday');
        
        for ($h = 0; $h < 24; $h++){
            for ($m = 0; $m < 60 ; $m += $mf){
                $time = sprintf('%02d:%02d', $h, $m);
                $times["$time"] = "$time";
            }
        }
        
        if($id !== NULL) return $times[$id];
        return $times;
    }
    
    
    /*
     * ближайшее время по интервалу
     * 14:07 - 14:30
     */
    /*function getClosestTime() {
        $time = FunctionModel::getDateWParam('i');
        if($time > 30){
            return (FunctionModel::getDateWParam('H')+1).':00';
        } else {
            return FunctionModel::getDateWParam('H').':30';
        }
    }*/


    /*
     * зашифровать имя модели(чтоб напрямую имя класса не передавать)
     */
    public function encriptModelClass($modelclass) {
        $class = 'model';//возвращаемый класс
        
        switch ($modelclass) {
            case Comments::className() :
                $class .= 'Comments';
                break;
            
            case Tasks::className() :
                $class .= 'Tasks';
                break;
            
            case Projects::className() :
                $class .= 'Projects';
                break;
            
            case Callboard::className() :
                $class .= 'Callboard';
                break;
            
            case Events::className() :
                $class .= 'Events';
                break;
            
            case Bugtracker::className() :
                $class .= 'Bugtracker';
                break;
            
            case Blogs::className() :
                $class .= 'Blogs';
                break;
            
            default :
                Yii::error('Не описана модель для encriptModelClass - '.$modelclass);
        }
        
        return $class;
    }
    
    /*
     * расшифровать класс модели(перед сохранением в базу например)
     */
    public function decriptModelClass($class) {//дополнить
        $modelclass = '';
        
        switch ($class) {
            case 'modelComments' :
                $modelclass = Comments::className();
                break;
            
            case 'modelTasks' :
                $modelclass = Tasks::className();
                break;
            
            case 'modelCallboard' :
                $modelclass = Callboard::className();
                break;
            
            case 'modelEvents' :
                $modelclass = Events::className();
                break;
            
            case 'modelProjects' :
                $modelclass = Projects::className();
                break;
            
            case 'modelBugtracker' :
                $modelclass = Bugtracker::className();
                break;
            
            case 'modelBlogs' :
                $modelclass = Blogs::className();
                break;
            
            default :
                Yii::error('Не описана модель для decriptModelClass - '.$class);
        }
        
        return $modelclass;
    }
    
    /*
     * проверка строки на json
     */
    function isJSON($string) {
        return ((is_string($string) && (is_object(json_decode($string)) || is_array(json_decode($string))))) ? true : false;
    }
    
    /*
     * говорящее название
     * @param $file {string} - url 
     */
    function fileGetContents($file) {
        if (function_exists('curl_init')) {
            $url     =   $file;
            $ch      =   curl_init();
            $timeout =   3;

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

            $result = curl_exec($ch);
            if ($result === false) {
                $errorMessage = curl_error($ch);
                curl_close($ch);

                throw new \Exception($errorMessage);
            }

            $info = curl_getinfo($ch);
            if (\yii\helpers\ArrayHelper::getValue($info, 'http_code') == 404) {
                curl_close($ch);
                throw new \Exception("File not found: {$file}");
            }

            curl_close($ch);

            return $result;
        } else {
            $ctx = stream_context_create(array('http'=>
                array(
                    'timeout' => 3,
                )
            ));

            return file_get_contents($file, false, $ctx);
        }
    }
    
    /**
     * отправка post json(для общения с апи например)
     * @param {string} $url
     * @param {array} $data
     * @return array
     */
    function sendPostJson($url, $data) {
        $data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );                                                                                                                   
        return curl_exec($ch);
    }
    
    /*
     * str_split для мультибайтовых кодировок
     * разбиение на символы
     * @param $string {string} - входящая строка
     * @return array  of chars
     */
    function mbStringToArray ($string) { 
        $strlen = mb_strlen($string); 
        while ($strlen) { 
            $array[] = mb_substr($string,0,1,"UTF-8"); 
            $string = mb_substr($string,1,$strlen,"UTF-8"); 
            $strlen = mb_strlen($string); 
        } 
        return $array; 
    } 
    
    /*
     * попытка поменять раскладку
     * @param $string {string} - входящая строка
     * @return array of string - в 2 языках
     */
    function tryChangeCharsetWord($string) {
        //массивы с точным совпадением
        $ru = [
            "й","ц","у","к","е","н","г","ш","щ","з","х","ъ","ф","ы","в","а","п","р","о","л","д","ж","э","я","ч","с","м","и","т","ь","б","ю","."
        ];
        $en = [
            "q","w","e","r","t","y","u","i","o","p","[","]","a","s","d","f","g","h","j","k","l",";","'","z","x","c","v","b","n","m",",",".","/"
        ];
        //массив замены англ uppercase для русского
        $ensymb = [
            '{' => '[', '}' => ']', ':' => ';', '"' => "'", '<' => ',', '>' => '.', '?' => '/'
        ];
        
        $returnText = [];
        $returnText['ru'] = $returnText['en'] = '';
        
        $text = FunctionModel::mbStringToArray(mb_strtolower($string));//начальное приведение
        
        //англ в русский
        foreach ($text as $char) {
            if($char === ' ') {//пробел, спецсимвол
                $returnText['ru'] .= ' ';
                continue;
            }
            
            if(array_key_exists($char, $ensymb)) {
                $char = $ensymb[$char];
            }
            
            $enchar = array_search($char, $en);
            if($enchar === FALSE) {//если нет символов из языка
                $returnText['ru'] .= $char;
            } else {
                $returnText['ru'] .= $ru[$enchar];
            }
        }
        
        //русский в англ
        foreach ($text as $char) {
            if($char === ' ') {//пробел, спецсимвол
                $returnText['en'] .= ' ';
                continue;
            }
            $ruchar = array_search($char, $ru);
            if($ruchar === FALSE) {//если нет символов из языка
                $returnText['en'] .= $char;
            } else {
                $returnText['en'] .= $en[$ruchar];
            }
        }
        
        return $returnText;
    }
    
    /*
     * получение дат, в формате(например для поиска дней рождений)
     */
    public function getDatesBetweenDates($start, $end, $format = 'm-d', $includeend = false) {        
        $from = new \DateTime($start);
        $to   = new \DateTime($end);

        $period = new \DatePeriod($from, new \DateInterval('P1D'), $to);

        $arrayOfDates = array_map(
            function($item) use($format) {return $item->format($format);},
            iterator_to_array($period)
        );
        
        if($includeend) {
            $arrayOfDates[] = $to->format($format);
        }
            
        return $arrayOfDates;
    }
    
    /**
     * день недели по русски
     * 
     * @param {int} $d - номер дня недели
     */
    public function getRuDayOfWeek($d) {
        $dm = '';
        
        switch ((int)$d) {
            case 1:
                $dm = 'Понедельник';
                break;
            case 2:
                $dm = 'Вторник';
                break;
            case 3:
                $dm = 'Среда';
                break;
            case 4:
                $dm = 'Четверг';
                break;
            case 5:
                $dm = 'Пятница';
                break;
            case 6:
                $dm = 'Суббота';
                break;
            default:
                $dm = 'Воскресенье';
        }
        
        return $dm;
    }
}
