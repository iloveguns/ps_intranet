<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\helpers\Data;
use app\behaviors\CacheFlush;

class Settings extends ActiveRecord
{
    const VISIBLE_NONE = 0;
    const VISIBLE_ROOT = 1;
    const VISIBLE_ALL = 2;

    static $_data;

    public static function tableName()
    {
        return 'system_settings';
    }

    public function rules()
    {
        return [
            [['name', 'title', 'value'], 'required'],
            [['name', 'title', 'value'], 'trim'],
            ['name',  'match', 'pattern' => '/^[a-zA-Z][\w_-]*$/'],
            ['name', 'unique'],
            ['visibility', 'number', 'integerOnly' => true]
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app/models', 'Name settings'),
            'title' => Yii::t('app/models', 'Title settings'),
            'value' => Yii::t('app/models', 'Value'),
            'visibility' => Yii::t('app/models', 'Only for developer')
        ];
    }

    public function behaviors()
    {
        return [
            CacheFlush::className()
        ];
    }

    /*
     * получить значение по имени
     */
    public static function get($name)
    {
        if(!self::$_data){
            self::$_data =  Data::cache(self::CACHE_KEY, Yii::$app->params['cacheTime'], function(){
                $result = [];
                try {
                    foreach (parent::find()->all() as $setting) {
                        $result[$setting->name] = $setting->value;
                    }
                }catch(\yii\db\Exception $e){}
                return $result;
            });
        }
        return isset(self::$_data[$name]) ? self::$_data[$name] : null;
    }

    /*
     * новая запись в настройках
     */
    public static function set($name, $value)
    {
        if(self::get($name)){
            $setting = Settings::find()->where(['name' => $name])->one();
            $setting->value = $value;
        } else {
            $setting = new Settings([
                'name' => $name,
                'value' => $value,
                'title' => $name,
                'visibility' => self::VISIBLE_ROOT
            ]);
        }
        $setting->save();
    }
}