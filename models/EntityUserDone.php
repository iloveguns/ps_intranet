<?php

namespace app\models;

use Yii;

class EntityUserDone extends \yii\db\ActiveRecord {
    const TASK_COMPLETE = 1;//метка о выполнении
    const TASK_UNCOMPLETE = 0;
    
    public static function tableName() {
        return 'entity_user_done';
    }

    public function rules()
    {
        return [
            [['class', 'id_user', 'complete'], 'required'],
            [['item_id', 'id_user', 'complete' ,'id_done'], 'integer'],
            [['text'], 'string'],
            [['date'], 'safe'],
            [['class'], 'string', 'max' => 128]
        ];
    }

    public function attributeLabels() {
        return [
            'id_done' => Yii::t('app/models', 'ID'),
            'class' => Yii::t('app/models', 'Class'),
            'item_id' => Yii::t('app/models', 'Entity done ID'),
            'id_user' => Yii::t('app/models', 'Id User'),
            'complete' => Yii::t('app/models', 'Complete'),
            'text' => Yii::t('app/models', 'Entity User Done Text'),
        ];
    }
    
    /*
     * просто вывод метки о выполнении задачи
     * $complete - 1|0
     * в проектах используется
     */
    public function userDoneLabel($complete) {
        $fa = ($complete) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>';
        $class = ($complete) ? "label-success" : "label-warning";
        $text = Yii::t("app/models", ($complete) ? 'Done' : 'Not Done');
        $label = '<span class="label status-label '.$class.'" style="margin-right:15px;">'.$fa.' '.$text.'</span>';
        return $label;
    }

    public function getIdUser() {
        return $this->hasOne(\app\modules\user\models\User::className(), ['id_user' => 'id_user']);
    }
    
    public function afterSave($insert, $changedAttributes) { 
        parent::afterSave($insert, $changedAttributes);
        
        //раз время в бд ставится, надо его взять
        $t = $this->findOne($this->id_done);
        $this->date = $t->date;
    }
}
