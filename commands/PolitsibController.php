<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\db\Query;

/**
 * truncate необходимо перед вставкой
 * сначала мелочи для связей, новости в конце
 */
class PolitsibController extends Controller {
    public $fk_site = 1;
    
    // ничего, просто погонять
    /*public function actionCheckimg () {
        $news = Yii::$app->dbimper->createCommand('SELECT id_news_old, pk_content FROM `content` WHERE headimgsrc_content IS NULL AND pk_content > 100000;')->queryAll();
        
        foreach ($news as $new) {
            $image = Yii::$app->dbpolitsib->createCommand('SELECT image FROM `news` WHERE id_news = '.$new['id_news_old'].';')->queryOne();
            var_dump($image['image'] . ' - ' . $new['pk_content']);
        }
        
        var_dump(count($news));
    }*/
    
    // комментарии
    /*public function actionComments() {
        $limit = 1000;
        $offset = 0;
        
        do {
            $news = Yii::$app->dbpolitsib->createCommand('SELECT * FROM `news_comments` LIMIT ' . $limit . ' OFFSET ' . $offset . ';')->queryAll();
            
            $ins = [];
            
            foreach ($news as $comment) {
                $fk_content = Yii::$app->dbimper->createCommand('SELECT pk_content FROM `content` WHERE id_news_old = '.$comment['news_id'].';')->queryOne();
                $fk_content = $fk_content['pk_content'];
                
                $ins[] = [$fk_content, $comment['content'], $comment['create_date'], $comment['id_comments'], $comment['active']];
            }
            
            $inserted = Yii::$app->dbimper->createCommand()->batchInsert('content_comments', 
                ['fk_content', 'text_comment', 'create_date', 'old_id_comment', 'is_active']
            , $ins)->execute();
            echo $inserted . ' комментов' . PHP_EOL;
            
            $offset += $limit;
        } while (count($news) >= $limit);
    }*/
    
    public function actionIndex() {
        // вставка тегов
        /*echo 'вставка тегов'.PHP_EOL;
        Yii::$app->dblocalapi->createCommand('SET FOREIGN_KEY_CHECKS = 0;TRUNCATE TABLE `tags`;SET FOREIGN_KEY_CHECKS = 1;')->execute();
        
        $insertTags = [];
        $tags = Yii::$app->dbpolitsib->createCommand('SELECT `name`, `id_tags` FROM `tags`;')->queryAll();
        
        foreach ($tags as $tag) {
            $insertTags[] = [NULL, $tag['name'], $this->fk_site, $tag['id_tags']];            
        }
                
        $transaction = Yii::$app->dbpolitsib->beginTransaction();
        
        try {
            Yii::$app->dblocalapi->createCommand()->batchInsert('tags', ['pk_tag', 'name_tag', 'fk_site', 'old_tag_id'], 
                $insertTags
            )->execute();
            
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        
        unset($tags);
        unset($insertTags);

        echo 'END вставка тегов'.PHP_EOL;*/
        // END вставка тегов
        
        
        // НОВОСТИ
        // хранение старых ид - 
        /*echo 'НОВОСТИ'.PHP_EOL;

        Yii::$app->dblocalapi->createCommand('SET FOREIGN_KEY_CHECKS = 0;TRUNCATE TABLE `content`;SET FOREIGN_KEY_CHECKS = 1;')->execute();
        
        $limit = 1000;
        $offset = 0;
        
        do {
            $news = Yii::$app->dbpolitsib->createCommand('SELECT '
                    . '`id_news`, `title`, `content`, `create_date`, `date`, `author_id`,'
                    . '`status`, `comments`, `image`, `update_date`, `type_code`, `rss_yandex`'
                    . 'FROM `news` ORDER BY `id_news` ASC LIMIT ' . $limit . ' OFFSET ' . $offset . ';')->queryAll();
            
            $ins = [];
            
            foreach ($news as $new) {
                $fk_material_rubric = 1;

                switch ($new['type_code']) {
                    case 2:
                        $fk_material_rubric = 2;
                        break;

                    case 3:
                        $fk_material_rubric = 3;
                        break;
                    case 9:
                        $fk_material_rubric = 4;
                        break;
                }

                $text_content = str_replace("\n",'',htmlentities($new['content'], ENT_QUOTES));
                $title = trim(str_replace("\n",'',htmlentities($new['title'], ENT_QUOTES)));
                
                // определение изображение
                $headimgsrc_content = NULL;

                if($new['image']) {
                    $headimgsrc_content_p = Yii::$app->dblocalapi->createCommand('SELECT `name_file` FROM `upload_files` WHERE `original_name_file` = "'. $new['image'] . '"')->queryOne();

                    if($headimgsrc_content_p['name_file']) {
                        $headimgsrc_content = 'http://109.195.33.205/papi/upload/f/' . $headimgsrc_content_p['name_file'];
                    }
                }
                // END определение изображение
                
                $ins[] = [
                    $new['id_news'], $title, $text_content, $new['create_date'],
                    $new['date'], $this->switchUser($new['author_id']), $fk_material_rubric, (int)$rss_yandex,
                    $new['status'], $new['comments'], $headimgsrc_content, $new['update_date'], $new['id_news'], $this->fk_site
                ];
            }
            
            $transaction = Yii::$app->dblocalapi->beginTransaction();
        
            try {
                $inserted = Yii::$app->dblocalapi->createCommand()->batchInsert('content', 
                    ['id_news_old', 'title_content', 'text_content', 'create_date',
                     'publish_date', 'fk_user_created', 'fk_material_rubric', 'exclude_rss_yandex',
                     'status_content', 'is_enabled_comments', 'headimgsrc_content', 'update_date', 'slug_content', 'fk_site']
                , $ins)->execute();
                echo $inserted . ' контента' . PHP_EOL;
                
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
            
            $offset += $limit;
        } while (count($news) >= $limit);

        Yii::$app->dblocalapi->createCommand('UPDATE `content` SET `publish_date` = "2000-01-01 00:00:00" WHERE `publish_date` = "0000-00-00 00:00:00"')->execute();
        unset($news);

        echo 'END НОВОСТИ'.PHP_EOL;*/
        // END НОВОСТИ


        // НОВОСТИ ГАЛЕРЕИ
        /*echo 'НОВОСТИ ГАЛЕРЕИ'.PHP_EOL;

        $galleryNews = Yii::$app->dbpolitsib->createCommand('SELECT DISTINCT(`news_id`) FROM `news_photo`;')->queryAll();
        $i = 0;
        $c = 0;
        $sql = '';
        $countAll = count($galleryNews);

        foreach ($galleryNews as $gallery_new) {
            $id_news_old = $gallery_new['news_id']; // старый ид новости
            $arr_ids = [];
            $galleries = Yii::$app->dbpolitsib->createCommand('SELECT `image_big` FROM `news_photo` WHERE `news_id` = ' . $id_news_old . ';')->queryAll(); // `title` - описание фоты

            foreach ($galleries as $gallery_img) {
                $file = Yii::$app->dblocalapi->createCommand('SELECT `pk_file`, `name_file` FROM `upload_files` WHERE `original_name_file` = "' . $gallery_img['image_big'] . '";')->queryOne();
                $pk_file = $file['pk_file'];
                $name_file = 'http://109.195.33.205/papi/upload/f/' . $file['name_file'];

                if($pk_file) { // может и не быть
                    $arr_ids[] = $pk_file;
                }
            }


            $contents= Yii::$app->dblocalapi->createCommand('SELECT `headimgsrc_content`, `text_content` FROM `content` WHERE `id_news_old` = "' . $id_news_old . '";')->queryOne();
            $is_head_img = $contents['headimgsrc_content'];
            $text_content = $contents['text_content'];

            
            // если фото 1, может делать его главным просто?
            if(count($arr_ids) === 1) {
                if($is_head_img) { // игнор или вставить <img>?
                    $text_content = htmlentities("<img src='$name_file'>", ENT_QUOTES) . $text_content;

                    $sql .= 'UPDATE content SET `text_content` = "' . $text_content . '" WHERE `id_news_old` = ' . $id_news_old . ';';
                } else { // сделать главным
                    $sql .= 'UPDATE content SET `headimgsrc_content` = "' . $name_file . '" WHERE `id_news_old` = ' . $id_news_old . ';';
                }
            } else if(count($arr_ids) < 1) {

            } else  { // делать галерею и вставлять в text_content
                $text_content = htmlentities('[gallery ids="'. implode(',', $arr_ids) . '"]', ENT_QUOTES) . $text_content;

                $sql .= 'UPDATE content SET `text_content` = "' . $text_content . '" WHERE `id_news_old` = ' . $id_news_old . ';';
            }

            $i++;
            $c++;

            //сброс
            if($i === 500 || $c === $countAll) {
                $is = Yii::$app->dblocalapi->createCommand($sql)->execute();
                echo 'вставка ' . $c .' - '. (bool)$is . PHP_EOL;
                $i = 0;
            }
        }

        
        unset($galleryNews);
        unset($doHeadimgsrc);
        unset($sql);
        unset($countAll);

        echo 'END НОВОСТИ ГАЛЕРЕИ'.PHP_EOL;
        // END НОВОСТИ ГАЛЕРЕИ        
        
        
        // НОВОСТИ_ТЕГ
        echo 'НОВОСТИ_ТЕГ'.PHP_EOL;

        $limit = 1000;
        $offset = 0;
        Yii::$app->dblocalapi->createCommand('TRUNCATE TABLE `r_content_to_tags`;')->execute();
        
        do {
            $news_to_tag = Yii::$app->dbpolitsib->createCommand('SELECT * FROM `news_to_tags` LIMIT ' . $limit . ' OFFSET ' . $offset . ';')->queryAll();
            
            $ins = [];
            
            foreach ($news_to_tag as $new_to_tag) {
                $new_data = Yii::$app->dblocalapi->createCommand('SELECT
                    (SELECT `pk_tag` FROM `tags` WHERE `old_tag_id` = ' . $new_to_tag['tags_id'] . ') AS pk_tag,
                    (SELECT `pk_content` FROM `content` WHERE `id_news_old` = ' . $new_to_tag['news_id'] . ') AS pk_content;')->queryOne();

                $ins[] = [$new_data['pk_content'], $new_data['pk_tag']];
            }
            
            $transaction = Yii::$app->dblocalapi->beginTransaction();
        
            try {
                $inserted = Yii::$app->dblocalapi->createCommand()->batchInsert('r_content_to_tags', 
                    ['fk_content', 'fk_tag']
                , $ins)->execute();
                echo $inserted . ' тегов контента' . PHP_EOL;
                
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
            
            $offset += $limit;
        } while (count($news_to_tag) >= $limit);
        
        unset($news_to_tag);

        echo 'END НОВОСТИ_ТЕГ'.PHP_EOL;*/
        // END НОВОСТИ_ТЕГ


        // переформирование тегов img и путей в новые
        echo 'переформирование тегов img и путей в новые'.PHP_EOL;

        $imgnews = Yii::$app->dblocalapi->createCommand('SELECT `pk_content` FROM `content` WHERE `text_content` LIKE "%img%";')->queryAll();
        $count = 0;

        //$transaction = Yii::$app->dblocalapi->beginTransaction();

        foreach ($imgnews as $imgnew) {
            $content = Yii::$app->dblocalapi->createCommand('SELECT `text_content` FROM `content` WHERE `pk_content` = ' . $imgnew['pk_content'] . ';')->queryOne();
            $text_content = html_entity_decode($content['text_content']);

            preg_match_all('/<img[^>]+>/i', $text_content, $images);

            foreach ($images[0] as $image) {
                preg_match_all('/src="([^"]+)"/i', $image, $srcs);

                foreach ($srcs[1] as $src) {
                    if(strpos($src, '/thumbs/') === false) {
                        // эти уже не спасти, разные папки, разбросаны везде
                        // заменить на пустую строку - нет, надо менять по полной строке - $srcs[0]
                        //$text_content = str_replace($src, '', $text_content);
                    } else {
                        $headimgsrc_content_p = Yii::$app->dblocalapi->createCommand('SELECT `name_file` FROM `upload_files` WHERE `original_name_file` = "'. $src . '"')->queryOne();

                        if($headimgsrc_content_p['name_file']) {
                            $headimgsrc_content = 'http://109.195.33.205/papi/upload/f/' . $headimgsrc_content_p['name_file'];

                            // путь (src) изменен на новый
                            $text_content = str_replace($src, $headimgsrc_content, $text_content);
                            $count++;
                        }
                    }
                }

                // можно убрать лишние свойства в style="", типа margin-left, HEGHT, border:0px  #333333;
                // preg_match_all('/style="([^"]+)"/i', $image, $styles);

                // foreach ($styles[1] as $style) {
                    // $text_content = str_replace($src, $headimgsrc_content, $text_content);
                    // var_dump($imgnew['pk_content'], $style);
                // }
            }

            // обратно закодировать
            $text_content = htmlentities($text_content, ENT_QUOTES);
            // var_dump($imgnew['pk_content'] . ' : ' . $text_content);
            // echo PHP_EOL;

            Yii::$app->dblocalapi->createCommand()
                    ->update('content', ['text_content' => $text_content], '`pk_content` = "' . $imgnew['pk_content'] . '"')
                    ->execute();

            echo $count . PHP_EOL;
        }

        echo 'END переформирование тегов img и путей в новые'.PHP_EOL;
        // END переформирование тегов img и путей в новые
    }
    
    // смена ид политсиба на ид интранета
    public function switchUser($id) {
        // 170 - ид главного редактора сейчас
        
        $arr = [1 => 170, 3 => 19, 4 => 331, 5 => 322, 10 => 279, 11 => 403, 13 => 39, 14 => 415,
            15 => 414, 16 => 104, 17 => 2, 19 => 413, 20 => 430, 21 => 144, 22 => 151,
            23 => 473, 24 => 494, 25 => 507, 26 => 522, 27 => 24, 28 => 555, 29 => 567, 30 => 170];
        
        if($id === NULL) return 170;
        return $arr[$id];
    }
    
    
    // TRUNCATE upload_files
    public function actionImgs() {
        Yii::$app->dblocalapi->createCommand('TRUNCATE upload_files')->execute();

        $rootPath = '1a/uploads';

        if(!file_exists($rootPath)) mkdir($rootPath);

        // 1 шаг - галереи перенести(gallery)
        $ins = [];
        // перенос в папку uploads + запись в таблицу uploads_files 
        foreach (glob("1a/content/**/**/**/gallery/*.*") as $filename) {
            $path = explode('/', $filename);
            $name_file = array_pop($path);

            $name_file = self::newFileName($name_file);

            $yearfolder = getcwd().'/'.$rootPath.'/'.$path[2];
            if(!file_exists($yearfolder)) mkdir($yearfolder);

            $monthfolder = $yearfolder.'/'.$path[3].'_'.$path[4];
            if(!file_exists($monthfolder)) mkdir($monthfolder);

            $oldFileName = getcwd().'/'.$filename;
            $newFileName = $monthfolder.'/'.$name_file;
            $pathForDB = '/media/static/uploads/'.$path[2].'/'.$path[3].'_'.$path[4];            
            
            $oldPath = explode('/', $filename);
            array_shift($oldPath);
            $oldPath = '/'.implode('/', $oldPath);
            
            if(copy(
                $oldFileName,
                $newFileName
            )) {
                $ins[] = [1, $oldPath, $name_file, $pathForDB];
                echo $name_file.PHP_EOL;
            }
        }
        
        // запись в бд
        $inserted = Yii::$app->dblocalapi->createCommand()->batchInsert('upload_files', 
            ['fk_site', 'original_name_file', 'name_file', 'path']
        , $ins)->execute();
        
        echo 'вставлено gallery: '.$inserted.PHP_EOL;
        // END 1 шаг
        
        
        // 2 шаг из папки content остальное, кроме галерей
        $ins = [];
        
        foreach (glob("1a/content/**/**/**/*.*") as $filename) {
            if(is_dir($filename)) continue;
            
            $path = explode('/', $filename);
            $name_file = array_pop($path);

            $name_file = self::newFileName($name_file);

            $yearfolder = getcwd().'/'.$rootPath.'/'.$path[2];
            if(!file_exists($yearfolder)) mkdir($yearfolder);

            $monthfolder = $yearfolder.'/'.$path[3].'_'.$path[4];
            if(!file_exists($monthfolder)) mkdir($monthfolder);

            $oldFileName = getcwd().'/'.$filename;
            $newFileName = $monthfolder.'/'.$name_file;
            $pathForDB = '/media/static/uploads/'.$path[2].'/'.$path[3].'_'.$path[4];
            
            
            $oldPath = explode('/', $filename);
            array_shift($oldPath);
            $oldPath = '/'.implode('/', $oldPath);
            
            if(copy(
                $oldFileName,
                $newFileName
            )) {
                $ins[] = [1, $oldPath, $name_file, $pathForDB];
                echo $name_file.PHP_EOL;
            }
        }
        
        // запись в бд
        $inserted = Yii::$app->dblocalapi->createCommand()->batchInsert('upload_files', 
            ['fk_site', 'original_name_file', 'name_file', 'path']
        , $ins)->execute();
        
        echo 'вставлено: '.$inserted.PHP_EOL;
        // END 2 шаг
    }
    
    /**
     * 2 порция, папка imgs, сливать с имеющимися из actionImgs
     */
    public function actionImgsmore() {
        $rootPath = '1a/uploads';
        
        $ins = [];

        if(!file_exists($rootPath)) mkdir($rootPath);

        // sudo chmod 777 /media/static/uploads    до копирования
        // sudo chmod 755 /media/static/uploads    после копирования

        // 720х400
        foreach (glob("1a/img/**/thumbs/**/*.*") as $filename) {
            $path = explode('/', $filename);
            $name_file = array_pop($path);
            $fold = explode('-', $path[2]);

            $yearfolder = getcwd().'/'.$rootPath.'/'.$fold[0];
            if(!file_exists($yearfolder)) mkdir($yearfolder);

            $monthfolder = $yearfolder.'/'.$fold[1].'_'.$fold[2];
            if(!file_exists($monthfolder)) mkdir($monthfolder);
            
            //-----------
            $pathForDB = '/media/static/uploads/'.$fold[0].'/'.$fold[1].'_'.$fold[2];
            
            $oldPath = explode('/', $filename);
            array_shift($oldPath);
            $oldPath = '/'.implode('/', $oldPath);
            $name_file = self::newFileName($name_file);
            
            if(copy(
                getcwd().'/'.$filename,
                $monthfolder.'/'.$name_file
            )) {
                $ins[] = [1, $oldPath, $name_file, $pathForDB];
                echo $name_file.PHP_EOL;
            }
        }

        // запись в бд
        $inserted = Yii::$app->dblocalapi->createCommand()->batchInsert('upload_files', 
            ['fk_site', 'original_name_file', 'name_file', 'path']
        , $ins)->execute();
        
        $ins = [];
        
        echo 'вставлено thumbs/720x400: '.$inserted.PHP_EOL;

        // просто все внутри
        foreach (glob("1a/img/**/thumbs/*.*") as $filename) {
            $path = explode('/', $filename);
            $name_file = array_pop($path);
            $fold = explode('-', $path[2]);

            //var_dump($filename);

            $yearfolder = getcwd().'/'.$rootPath.'/'.$fold[0];
            if(!file_exists($yearfolder)) mkdir($yearfolder);

            $monthfolder = $yearfolder.'/'.$fold[1].'_'.$fold[2];
            if(!file_exists($monthfolder)) mkdir($monthfolder);

            //-----------
            $pathForDB = '/media/static/uploads/'.$fold[0].'/'.$fold[1].'_'.$fold[2];
            
            $oldPath = explode('/', $filename);
            array_shift($oldPath);
            $oldPath = '/'.implode('/', $oldPath);
            $name_file = self::newFileName($name_file);

            if(copy(
                getcwd().'/'.$filename,
                $monthfolder.'/'.$name_file
            )) {
                $ins[] = [1, $oldPath, $name_file, $pathForDB];
                echo $name_file.PHP_EOL;
            }
        }
        
        // запись в бд
        $inserted = Yii::$app->dblocalapi->createCommand()->batchInsert('upload_files', 
            ['fk_site', 'original_name_file', 'name_file', 'path']
        , $ins)->execute();
        
        $ins = [];
        
        echo 'вставлено thumbs: '.$inserted.PHP_EOL;
    }

    /**
     * это то 1 раз точно, самые древние фотки
     * 3 порция, папка /home/politsib/politsib.ru/htdocs/images/news/news, сливать с имеющимися из actionImgs, actioImgsmore
     */
    public function actionImgsmoren() {
        $rootPath = '1a/uploads';
        
        $ins = [];

        if(!file_exists($rootPath)) mkdir($rootPath);

        foreach (glob("1a/news/*.*") as $filename) {
            if(strpos($filename, '_icon.')) continue; // *_icon.* - просто уменьшенные копии - не нужны

            $path = explode('/', $filename);
            $name_file = array_pop($path);

            $yearfolder = getcwd().'/'.$rootPath.'/2012';
            if(!file_exists($yearfolder)) mkdir($yearfolder);

            $monthfolder = $yearfolder.'/01_01';
            if(!file_exists($monthfolder)) mkdir($monthfolder);
            
            //-----------
            $pathForDB = '/media/static/uploads/2012/01_01';
            
            $oldPath = explode('/', $filename);
            array_shift($oldPath);
            $oldPath = '/images/news/'.implode('/', $oldPath);
            $name_file = self::newFileName($name_file);
            
            if(copy(
                getcwd().'/'.$filename,
                $monthfolder.'/'.$name_file
            )) {
                $ins[] = [1, $oldPath, $name_file, $pathForDB];
                echo $name_file.PHP_EOL;
            }
        }

        // запись в бд
        $inserted = Yii::$app->dblocalapi->createCommand()->batchInsert('upload_files', 
            ['fk_site', 'original_name_file', 'name_file', 'path']
        , $ins)->execute();
        
        $ins = [];
        
        echo 'вставлено /news/: '.$inserted.PHP_EOL;
    }

    public function actionTags() {
        /*$tags = Yii::$app->dbpolitsib->createCommand('SELECT * FROM tags WHERE id_tags > 1247;')->queryAll();

        $a = [];

        foreach ($tags as $tag) {
            Yii::$app->dblocalapi->createCommand('INSERT INTO tags (pk_tag, name_tag, fk_site, old_tag_id) VALUES (NULL, "'. $tag['name'].'", 1, "'.$tag['id_tags'].'")')->execute();
        }*/
    }

    /**
     * новое имя файла для записи в бд, везде использовать
     */
    private function newFileName($name_file)
    {
        $dot = explode('.', $name_file);
        $nf = substr($dot[0], 0, -3);
        $dot = array_pop($dot);

        list($usec, $sec) = explode(" ", microtime());

        $name = ((float)$usec + (float)$sec) . '_' . rand(0, 99999);

        // uuid из node.js
        return UUID::v5('6ba7b810-9dad-11d1-80b4-00c04fd430c8', $name) . '.' . $dot;
    }
}



/**
 * UUID class
 *
 * The following class generates VALID RFC 4122 COMPLIANT
 * Universally Unique IDentifiers (UUID) version 3, 4 and 5.
 *
 * UUIDs generated validates using OSSP UUID Tool, and output
 * for named-based UUIDs are exactly the same. This is a pure
 * PHP implementation.
 *
 * @author Andrew Moore
 * @link http://www.php.net/manual/en/function.uniqid.php#94959
 */
class UUID
{
    /**
     * Generate v3 UUID
     *
     * Version 3 UUIDs are named based. They require a namespace (another 
     * valid UUID) and a value (the name). Given the same namespace and 
     * name, the output is always the same.
     * 
     * @param   uuid    $namespace
     * @param   string  $name
     */
    public static function v3($namespace, $name)
    {
        if(!self::is_valid($namespace)) return false;
        // Get hexadecimal components of namespace
        $nhex = str_replace(array('-','{','}'), '', $namespace);
        // Binary Value
        $nstr = '';
        // Convert Namespace UUID to bits
        for($i = 0; $i < strlen($nhex); $i+=2) 
        {
            $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
        }
        // Calculate hash value
        $hash = md5($nstr . $name);
        return sprintf('%08s-%04s-%04x-%04x-%12s',
        // 32 bits for "time_low"
        substr($hash, 0, 8),
        // 16 bits for "time_mid"
        substr($hash, 8, 4),
        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 3
        (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,
        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,
        // 48 bits for "node"
        substr($hash, 20, 12)
        );
    }
    /**
     * 
     * Generate v4 UUID
     * 
     * Version 4 UUIDs are pseudo-random.
     */
    public static function v4() 
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand(0, 0xffff), mt_rand(0, 0xffff),
        // 16 bits for "time_mid"
        mt_rand(0, 0xffff),
        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand(0, 0x0fff) | 0x4000,
        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand(0, 0x3fff) | 0x8000,
        // 48 bits for "node"
        mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
    /**
     * Generate v5 UUID
     * 
     * Version 5 UUIDs are named based. They require a namespace (another 
     * valid UUID) and a value (the name). Given the same namespace and 
     * name, the output is always the same.
     * 
     * @param   uuid    $namespace
     * @param   string  $name
     */
    public static function v5($namespace, $name) 
    {
        if(!self::is_valid($namespace)) return false;
        // Get hexadecimal components of namespace
        $nhex = str_replace(array('-','{','}'), '', $namespace);
        // Binary Value
        $nstr = '';
        // Convert Namespace UUID to bits
        for($i = 0; $i < strlen($nhex); $i+=2) 
        {
            $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
        }
        // Calculate hash value
        $hash = sha1($nstr . $name);
        return sprintf('%08s-%04s-%04x-%04x-%12s',
        // 32 bits for "time_low"
        substr($hash, 0, 8),
        // 16 bits for "time_mid"
        substr($hash, 8, 4),
        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 5
        (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,
        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,
        // 48 bits for "node"
        substr($hash, 20, 12)
        );
    }
    public static function is_valid($uuid) {
        return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
                      '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
    }
}