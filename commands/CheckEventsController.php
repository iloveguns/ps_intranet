<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\modules\intranet\models\Events;
use app\models\FunctionModel;
use app\models\UnreadNotice;

class CheckEventsController extends Controller
{
    public function actionIndex() {
        //логика абсолютно меняется
        
        $date = FunctionModel::getDateWParam('Y-m-d');
        $time = FunctionModel::getDateWParam('H:i:s');
        $stime = FunctionModel::getDateWParam('H:i');
        
        $model = Events::find()->select(['json_repeat', 'json_notify', 'pk_event'])
                ->andWhere(['not', ['json_notify' => NULL]])
                ->andWhere(['not', ['json_repeat' => NULL]])
                ->orWhere(['and', ['json_repeat' => NULL], ['like', 'json_notify', $date]])//уведомление в конкр дату однократно
                ->all();
        
        foreach ($model as $event) {
            $notify = json_decode($event['json_notify'], true);
            $repeat = json_decode($event['json_repeat'], true);
            $ntime = NULL;//время уведомления
            $delete = true;//метка для удаления(при необходимости)
            
            //для кол-ва повторений проверить попадает ли на сегодня
            if(isset($repeat['count'])) {
                $arrDates = [];
                switch ($repeat['select']){//выбрать тип обновления и просчитать даты
                    case Events::REPEAT_DAILY : //ежедневно. +1 день
                        if(substr($notify['notifyat'], 0, 10) == $date) {//если дата уже попадает, то сразу можно дальше, без просчета
                            $ntime = substr($notify['notifyat'], 11, 8);
                        } else {                        
                            for($i = 1; $i <= $repeat['count']; $i++) {
                                $t = ($i === 1) ? $notify['notifyat'] : $arrDates[$i-1];
                                $arrDates[$i] = FunctionModel::dateModify($t, '+1 day');
                                $fdate = $arrDates[$i];

                                if(substr($fdate, 0, 10) == $date) {//если дата уже попадает, то сразу можно дальше, без просчета
                                    $ntime = substr($fdate, 11, 8);//только время уведомления
                                    break;
                                }
                            }
                        }
                    break;

                    case Events::REPEAT_WEEKLY : //еженедельно. +1 неделя
                        for($i = 1; $i <= $repeat['count']; $i++) {
                            $t = ($i === 1) ? $notify['notifyat'] : $arrDates[$i-1];
                            $arrDates[$i] = FunctionModel::dateModify($t, '+1 week');
                            $fdate = $arrDates[$i];
                            
                            if(substr($fdate, 0, 10) == $date) {//если дата уже попадает, то сразу можно дальше, без просчета
                                $ntime = substr($fdate, 11, 8);//только время уведомления
                                break;
                            } elseif(substr($fdate, 0, 10) > $date) {//уходит на будущее, дальше не считать
                                $delete = false;
                                break;
                            }
                        }
                    break;
                }
            } else {
                $ntime = substr($notify['notifyat'], 11, 8);//только время уведомления
            }
            
            if($ntime === NULL) {//время не нашлось после просчета - можно стереть json_notify
                if($delete) {
                    echo 'хочу удалить ' . $event['pk_event'];
                    Yii::$app->db->createCommand()->update(Events::tableName(), ['json_notify' => NULL], 'pk_event = '.$event['pk_event'])->execute();
                }
                continue;
            }
            
            if($ntime > $time) {
                continue;//еще не время
            } else {
                $ntime = substr($ntime, 0, -3);//только время уведомления без секунд
                if($ntime == $stime) {//время отсылать уведомление
                    echo 'отсылаю на '.$event['pk_event'];//и можно удалить, если последнее
                    UnreadNotice::saveByIds($event, UnreadNotice::NOTIFY_EVENT, false, false);
                } else {//уже опоздал

                }
            }
        }

        /*die();
        //найти которые дата сегодня и надо уведомлять
        $model = Events::find()->where(['like', 'start_date', FunctionModel::getDateWParam('Y-m-d')])->andWhere(['not', ['json_notify' => null]])->all();
        
        foreach ($model as $event) {
            $periodminutes = [];//период в минутах
            $notify = json_decode($event->json_notify, true);
            if(FunctionModel::getDateTimestamp() >= $event->start_date) continue;//еще не время или уже началось
            
            if(isset($notify['notifyat'])){//уведомить за 
                if(substr($notify['notifyat'], 0, -3) == substr(FunctionModel::getDateTimestamp(), 0, -3)){//время отправки без секунд
                    echo 'отправляю';
                    UnreadNotice::saveByIds($event, UnreadNotice::NOTIFY_EVENT, false, false);
                }                
            } else if(isset($notify['period'])) { //уведоммить с
                //найти минуты в которых создавать уведомление
                if($notify['period'] <= 60){//только меньше часа
                    $p = 60/$notify['period'];

                    for($i = 0; $i < $p; $i++){
                        $periodminutes[$notify['period']*$i] = $notify['period']*$i;
                    }
                } else {
                    //пока нет, но можно полную дату формировать от начала +n часов
                }
                
                if($notify['date'] == FunctionModel::getDateTimestamp() || in_array(FunctionModel::getDateWParam('i'), $periodminutes)){//если время совпадает, то отправить полюбому уведомление
                    echo 'отправить';
                    UnreadNotice::saveByIds($event, UnreadNotice::NOTIFY_EVENT, false, false);
                }
            }
        }
        
        //найти которые закончились сегодня и надо продлять
        $model = Events::find()->where(['<=', 'end_date', FunctionModel::getDateTimestamp()])->andWhere(['not', ['json_repeat' => null]])->all();
                
        foreach ($model as $event) {
            $tosave= [];//массив сохранения
            $repeat = json_decode($event->json_repeat, true);

            //а нахуя пересохранять с новой датой?
            /*switch ($repeat['select']){//выбрать тип обновления и вычислить новую дату
                case Events::REPEAT_DATE : //в дату. сотворить дату день от указанного +время от текущго                        
                    $start_date = $repeat['date'].' '.  explode(' ', $event->start_date)[1];
                    $end_date = $repeat['date'].' '.  explode(' ', $event->end_date)[1];
                    $repeat = NULL;
                break;

                case Events::REPEAT_DAILY : //ежедневно. +1 день
                    $start_date = FunctionModel::dateModify($event->start_date, '+1 day');
                    $end_date = FunctionModel::dateModify($event->end_date, '+1 day');
                break;

                case Events::REPEAT_WEEKLY : //еженедельно. +1 неделя
                    $start_date = FunctionModel::dateModify($event->start_date, '+1 week');
                    $end_date = FunctionModel::dateModify($event->end_date, '+1 week');
                break;

                /*case 'monthly' : //ежемесячно. +1 месяц
                    $start_date = FunctionModel::dateModify($event->start_date, '+1 month');
                    $end_date = FunctionModel::dateModify($event->end_date, '+1 month');
                break;

                case 'yearly' : //ежегодно. +1 год
                    $start_date = FunctionModel::dateModify($event->start_date, '+1 year');
                    $end_date = FunctionModel::dateModify($event->end_date, '+1 year');
                break;
            }
            
            
            $tosave['start_date'] = $start_date;
            $tosave['end_date'] = $end_date;*/
                
            //обновить
            /*if(isset($repeat['count'])){//если есть количество повторов
                if($repeat['count'] == 1){//последний повтор
                    $repeat = NULL;//все, больше не надо повторять и поле станет NULL
                } else {
                    $repeat['count'] = --$repeat['count'];//просто уменьшить счетчик
                }
            }
                
            if($repeat){//состояние может измениться до этого момента
                $repeat = json_encode($repeat);
                $tosave['json_repeat'] = $repeat;
            } else {
                $tosave['json_repeat'] = NULL;
            }
            
            //в любом случае надо обновить данные
            Yii::$app->db->createCommand()->update('events', $tosave, 'pk_event = '.$event->pk_event)->execute();
        }*/
    }
}
