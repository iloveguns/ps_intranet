<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\modules\intranet\models\ScheduleRadio;

class RadioController extends Controller {
    //по ошибке на расписании радио
    public function actionIndex() {
        die();
        for($i = 1; $i < 31; $i++) {
            if($i < 10) {
                $i = '0' . $i;
            }
            
            $date = '2017-11-'. $i;
            
            $models = ScheduleRadio::find()->asArray()->where(['date' => $date])->all();
            $nmodel = [];            
            
            foreach ($models as $model) {
                $d = $model['date'];
                $t = $model['time'];
                unset($model['date']);
                $nmodel[$d][$t][] = $model;
            }
            
            foreach ($nmodel as $model) {
                foreach ($model as $m) {
                    if(count($m) === 1) {
                        //пропускать, нет совпадений
                    } else {
                        for($j = 0; $j < count($m); $j++){
                            var_dump(Yii::$app->db->createCommand("UPDATE `schedule_radio` SET `sort` = " . $j . " WHERE `schedule_radio`.`id` = " . $m[$j]['id'] . ";")->execute());
                        }
                    }                    
                }
            }
        }
    }
    
    /**
     * очистка папки с отчетами от расписания радио
     */
    public function actionRemovereports() {
        foreach(glob(Yii::getAlias('@radioreports/*')) as $file){
            if(is_file($file)) {
                unlink($file);                
            }
        }
    }
}
