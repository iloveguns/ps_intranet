<?php

namespace app\commands;

use Yii;
use yii\console\Controller;

require_once(__DIR__. '/../helpers/simple_html_dom.php');

class MycostsController extends Controller {
    const E2E4 = 'e2e4';
    const DNS = 'dns';//днс, технопоинт
    const REGARD = 'regard';
        
    /*
     * инструмент для мониторинга за ценами - берет и сохраняет
     */
    public function actionIndex() {        
        //для тестов
        /*$ch = curl_init("http://www.regard.ru/catalog/tovar215210.htm");

        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:40.0)' . 'Gecko/20100101 Firefox/40.0']);
        
        $html = new \simple_html_dom();
        $html->load(curl_exec($ch));
        curl_close($ch);
        if($f = $html->find('meta[itemprop="price"]')) {
            $price = $f[0]->attr['content'];
            $correct = 1;
        } else {
            $correct = 0;
            $price = 0;
        }
        var_dump($price);
        die();*/
        
        $products = Yii::$app->dbtest->createCommand("SELECT * FROM `mycosts_products`;")->queryAll();
        
        foreach ($products as $product) {
            $shop = $product['shop'];
            $comment = $product['comment'];
            
            $ch = curl_init($product['link']);
            
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:40.0)' . 'Gecko/20100101 Firefox/40.0']);
            
            $html = new \simple_html_dom();
            $html->load(curl_exec($ch));
            curl_close($ch);
            
            if($shop === self::E2E4) {
                if($f = $html->find('input[data-itemid]')) {
                    $correct = 1;
                    $price = $f[0]->attr['data-price'];
                } else {
                    $correct = 0;
                    $price = 0;
                }
            } else if($shop === self::DNS) {
                if($f = $html->find('i[data-product-param="price"]')) {
                    $correct = 1;
                    $price = $f[0]->attr['data-value'];
                } else {
                    $correct = 0;
                    $price = 0;
                }
            } else if($shop === self::REGARD) {
                if($f = $html->find('meta[itemprop="price"]')) {
                    $price = $f[0]->attr['content'];
                    $correct = 1;
                } else {
                    $correct = 0;
                    $price = 0;
                }
            } else {
                continue;
            }
            
            Yii::$app->dbtest->createCommand("INSERT INTO `mycosts` (`id`, `price`, `correct`, `fk_product`) VALUES (NULL, '" . $price . "', " . $correct . ", '" . $product['pk_product'] . "');")->execute();
        }
    }
}
