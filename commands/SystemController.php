<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\FunctionModel;

class SystemController extends Controller {    
    /*
     * бекап
     */
    public function actionBackup() {
        $nameArchive = 'backup_intranet_'.date('d-m-Y').'.zip';//имя архива
        $folderToSave = 'backups';//папка под chmod 777 в корне
        $folderPath = Yii::getAlias('@app');//корневая папка 
        
        $nameSqldump = 'sqldump';//название файла бэкапа базы
        $dbname = FunctionModel::getDsnAttribute('dbname', Yii::$app->getDb()->dsn);//имя базы данных
                
        shell_exec('mysqldump -u'.Yii::$app->db->username.' -p'.Yii::$app->db->password.' '.$dbname.' > '.$folderPath.'/'.$nameSqldump.'.sql;');//бэкап базы в корень, потом удалить. NULL всегда возвращает
        
        if(shell_exec('cd '.$folderPath.'/..;zip -r '.$nameArchive.' html/ -x *node_modules/* *.git/* *vendor/* *runtime/* *web/assets/*;')){//забекапить
            shell_exec('cd '.$folderPath.'/..;mv '.$nameArchive.' '.$folderToSave.'/'.$nameArchive);//переместить
            
            unlink($folderPath.'/'.$nameSqldump.'.sql');//удалить бекап базы из корня 
            echo $nameArchive.' created!'.PHP_EOL;
        }
    }
    
    /*
     * просто для прикола подсчет комментариев по сотрудникам
     */
    public function actionCountcom() {
        $arr = [];
        
        $users = Yii::$app->db->createCommand('SELECT id_user, lastname, name FROM user')->queryAll();
        foreach ($users as $user) {
            $cc = Yii::$app->db->createCommand('SELECT count(id_user) AS count FROM comments WHERE id_user = '.$user['id_user'])->queryOne();
            $arr[$cc['count']][] = ['name' => $user['lastname'] . ' ' .$user['name'], 'count' => $cc['count']];
        }
        
        krsort($arr);
        
        foreach ($arr as $value) {
            foreach ($value as $data) {
                echo $data['name'].' - '. $data['count'] . PHP_EOL;
            }
        }
    }
}