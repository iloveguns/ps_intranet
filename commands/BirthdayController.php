<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\modules\user\models\User;
use app\models\FunctionModel;
use app\modules\intranet\models\Callboard;
use app\models\UnreadNotice;
use app\modules\user\models\UserStatus;

class BirthdayController extends Controller
{
    /*
     * посмотреть у кого др и создать объявление
     */
    public function actionIndex()
    {
        $todaymonthday = FunctionModel::getDateWParam('m-d');
        $todayDate = FunctionModel::getDateWParam('Y-m-d');
        
        $model = User::find()->where(['like', 'birthday', $todaymonthday])->all();
        
        foreach ($model as $user) {
            $b = substr($user->birthday, 5);//найти месяц день и сравнить конкретно, чтоб без ошибок было
            if($b != $todaymonthday) continue;
            
            if($user->fkStatus->status == UserStatus::FIRED) continue;//уволен
            //принадлежит всем, но это не указывается. просто сразу уведомление каждому прислать unread_notice
            $callboard = new Callboard();
            $callboard->fk_callboard_category = Callboard::BIRTHDAY;//поздравления. менять по необходимости
            $callboard->id_author = NULL;//важно
            $callboard->title = Yii::t('app/nulluser', 'Today, {date} {username} celebrates its birthday!', ['date' => Yii::$app->formatter->asDate($todayDate), 'username' => $user->linkToProfile]);
            $callboard->text = Yii::t('app/nulluser', 'Сongratulations');
            $callboard->link_to_entity = $user->id_user;//просто ссылка на сотрудника
            
            $callboard->save();
            
            //всем уведомление кинуть
            foreach (User::findActive()->all() as $userNotice) {
                $notice = new UnreadNotice();
                $notice->class = $callboard->getName();
                $notice->item_id = $callboard->pk_callboard;
                $notice->id_user = $userNotice->id_user;
                $notice->type = UnreadNotice::NEW_CONGRAT;
                $notice->save();
            }
        }
    }
}
