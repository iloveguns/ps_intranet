<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use \app\modules\crm\models\CrmContactsInfo;

class CrmController extends Controller {
    protected static $_counter = [];
    protected static $_sql = '';
    
    /**
     * полное копирование отдела со всеми компаниями, контактми, задачами и тд
     * 
     * crm_company(компании) - отдел
     * crm_contact(контакты) - отдел
     * crm_contacts_info(инфа контактов и компаний) - ид
     * crm_deals(сделки) - отдел
     * crm_deals_projects(проекты сделок) - отдел + ид сделок
     * crm_tags(теги контактов и компаний) - ид, запись в crm_relation_tags
     * crm_tasks(задачи) - отдел, ид
     * crm_tasks_results(результаты задач) - отдел, ид 
     */
    public function actionCopyDepartmentFull() {
        $dep_from = 14; // копируемый отдел
        $dep_to = 59; // отдел, в который копировать
        
        //результаты задач
        /*$crm_tasks_results = Yii::$app->db->createCommand("SELECT * FROM `crm_tasks_results` WHERE `fk_department` = " . $dep_from)->queryAll();
        
        $ins = [];
        foreach ($crm_tasks_results as $c) {
            $ins[] = [$c['name_task_result'], $c['denial'], $dep_to];
        }
        
        $inserted = Yii::$app->db->createCommand()->batchInsert(\app\modules\crm\models\CrmTasksResults::tableName(), ['name_task_result', 'denial', 'fk_department'], $ins)->execute();
        echo $inserted . ' результатов задач' . PHP_EOL;
        //end результаты задач
        
        
        //типы задач
        $crm_typetask = Yii::$app->db->createCommand("SELECT * FROM `crm_typetask` WHERE `fk_department` = " . $dep_from)->queryAll();
        
        $ins = [];
        foreach ($crm_typetask as $c) {
            $ins[] = [$c['name_typetask'], $dep_to, $c['important']];
        }
        
        $inserted = Yii::$app->db->createCommand()->batchInsert(\app\modules\crm\models\CrmTypetask::tableName(), ['name_typetask', 'fk_department', 'important'], $ins)->execute();
        echo $inserted . ' типов задач' . PHP_EOL;
        //end типы задач
        
        
        //теги
        $tags = Yii::$app->db->createCommand("SELECT * FROM `crm_tags` WHERE `fk_department` = " . $dep_from)->queryAll();
        
        $ins = [];
        foreach ($tags as $c) {
            $ins[] = [$c['name_tag'], $dep_to];
        }
        
        $inserted = Yii::$app->db->createCommand()->batchInsert(\app\modules\crm\models\CrmTags::tableName(), ['name_tag', 'fk_department'], $ins)->execute();
        echo $inserted . ' тегов' . PHP_EOL;
        //end теги
     
        
        //Проекты сделок
        $crm_deals_projects = Yii::$app->db->createCommand("SELECT * FROM `crm_deals_projects` WHERE `fk_department` = " . $dep_from)->queryAll();
        
        $ins = [];
        foreach ($crm_deals_projects as $c) {
            $ins[] = [$c['name_dp'], $dep_to];
        }
        
        $inserted = Yii::$app->db->createCommand()->batchInsert(\app\modules\crm\models\CrmDealsProjects::tableName(), ['name_dp', 'fk_department'], $ins)->execute();
        echo $inserted . ' проектов сделок' . PHP_EOL;
        //end Проекты сделок
        
        
        //Продукты
        $crm_types_deal = Yii::$app->db->createCommand("SELECT * FROM `crm_types_deal` WHERE `fk_department` = " . $dep_from)->queryAll();
        
        $ins = [];
        foreach ($crm_types_deal as $c) {
            $ins[] = [$c['name_types_deal'], $dep_to];
        }
        
        $inserted = Yii::$app->db->createCommand()->batchInsert(\app\modules\crm\models\CrmTypesDeal::tableName(), ['name_types_deal', 'fk_department'], $ins)->execute();
        echo $inserted . ' продуктов' . PHP_EOL;
        //end Продукты
        
        
        //компании
        $crm_company = Yii::$app->db->createCommand("SELECT * FROM `crm_company` WHERE `fk_department` = " . $dep_from)->queryAll();
        
        $ins = [];
        foreach ($crm_company as $c) {
            $ins[] = [
                $c['pk_company'],
                $c['responsible_user'],
                $c['name_company'],
                $c['comment_company'],
                $c['fk_address_country'],
                $c['fk_address_city'],
                $c['fk_address_street'],
                $c['address_other'],
                $c['birthdate_company'],
                $c['created_user'],
                $c['create_date'],
                $c['update_date'],
                $c['updated_user'],
                $dep_to,
                $c['stock'],
                $c['stock_date']
            ];
        }
        
        $inserted = Yii::$app->db->createCommand()->batchInsert(\app\modules\crm\models\CrmCompany::tableName(), [
            'old_pkcompany', // ид копируемой компании
            'responsible_user',
            'name_company',
            'comment_company',
            'fk_address_country',
            'fk_address_city',
            'fk_address_street',
            'address_other',
            'birthdate_company',
            'created_user',
            'create_date',
            'update_date',
            'updated_user',
            'fk_department',
            'stock',
            'stock_date'
        ], $ins)->execute();
        
        echo $inserted . ' компаний' . PHP_EOL;
        unset($crm_company);
        //end компании
        
        //контакты
        $crm_contact = Yii::$app->db->createCommand("SELECT * FROM `crm_contact` WHERE `fk_department` = " . $dep_from)->queryAll();
        
        $ins = [];
        foreach ($crm_contact as $c) {
            $fk_crm_company = NULL;
            
            if($c['fk_crm_company']) {
                $fk_crm_company = Yii::$app->db->createCommand("SELECT `pk_company` FROM `crm_company` WHERE `old_pkcompany` = " . $c['fk_crm_company'])->queryOne();
                $fk_crm_company = $fk_crm_company['pk_company'];
            }
            
            $ins[] = [
                $c['pk_contact'],
                $c['name_contact'],
                $c['comment_contact'],
                $c['post_contact'],
                $c['birthdate_contact'],
                $fk_crm_company,
                $c['created_user'],
                $c['create_date'],
                $c['update_date'],
                $c['updated_user'],
                $dep_to,
                $c['responsible_user'],
                $c['stock'],
                $c['stock_date']
            ];
        }
        
        $inserted = Yii::$app->db->createCommand()->batchInsert(\app\modules\crm\models\CrmContact::tableName(), [
            'old_pkcontact',
            'name_contact',
            'comment_contact',
            'post_contact',
            'birthdate_contact',
            'fk_crm_company',
            'created_user',
            'create_date',
            'update_date',
            'updated_user',
            'fk_department',
            'responsible_user',
            'stock',
            'stock_date'
            
        ], $ins)->execute();
        
        echo $inserted . ' контактов' . PHP_EOL;
        unset($crm_contact);
        //end контакты
        
        
        //контактная информация контакты
        $crm_contacts_info_cont = Yii::$app->db->createCommand("SELECT `old_pkcontact`, `pk_contact` FROM `crm_contact` WHERE `fk_department` = " . $dep_to)->queryAll();
                
        foreach ($crm_contacts_info_cont as $c) {
            $crm_contacts_info = Yii::$app->db->createCommand("SELECT * FROM `crm_contacts_info` WHERE `fk_crmcontact` = " . $c['old_pkcontact'])->queryAll();
            
            $ins = [];
            
            foreach ($crm_contacts_info as $cinfo) {
                $ins[] = [
                    $cinfo['contact_field'],
                    $cinfo['contact_value'],
                    $cinfo['contact_value_search'],
                    $cinfo['fk_crmcompany'],
                    $c['pk_contact'],
                    $cinfo['contact_comment']
                ];
            }
            
            $inserted = Yii::$app->db->createCommand()->batchInsert(CrmContactsInfo::tableName(), [
                'contact_field',
                'contact_value',
                'contact_value_search',
                'fk_crmcompany',
                'fk_crmcontact',
                'contact_comment'
            ], $ins)->execute();
        }
        unset($crm_contacts_info_cont);
        //end контактная информация контакты
        
        
        //контактная информация компании
        $crm_contacts_info_comp = Yii::$app->db->createCommand("SELECT `old_pkcompany`, `pk_company` FROM `crm_company` WHERE `fk_department` = " . $dep_to)->queryAll();
                
        foreach ($crm_contacts_info_comp as $c) {
            $crm_contacts_info = Yii::$app->db->createCommand("SELECT * FROM `crm_contacts_info` WHERE `fk_crmcompany` = " . $c['old_pkcompany'])->queryAll();
            
            $ins = [];
            
            foreach ($crm_contacts_info as $cinfo) {
                $ins[] = [
                    $cinfo['contact_field'],
                    $cinfo['contact_value'],
                    $cinfo['contact_value_search'],
                    $c['pk_company'],
                    $cinfo['fk_crmcontact'],
                    $cinfo['contact_comment']
                ];
            }
            
            $inserted = Yii::$app->db->createCommand()->batchInsert(CrmContactsInfo::tableName(), [
                'contact_field',
                'contact_value',
                'contact_value_search',
                'fk_crmcompany',
                'fk_crmcontact',
                'contact_comment'
            ], $ins)->execute();
        }
        unset($crm_contacts_info_comp);*/
        //end контактная информация компании
        
        
        //задачи
        $crm_tasks= Yii::$app->db->createCommand("SELECT * FROM `crm_tasks` WHERE `fk_department` = " . $dep_from)->queryAll();
        
        $ins = [];
        $i = 0;
        foreach ($crm_tasks as $c) {
            //тип задачи найти
            $tt = NULL;
            if($c['fk_typetask']) {
                $t = Yii::$app->db->createCommand("SELECT `name_typetask` FROM `crm_typetask` WHERE `fk_department` = " . $dep_from . " AND `pk_typetask` = " . $c['fk_typetask'])->queryOne();            
            
                if($tt = Yii::$app->db->createCommand("SELECT `pk_typetask` FROM `crm_typetask` WHERE `fk_department` = " . $dep_to . " AND `name_typetask` = '" . $t['name_typetask'] . "'")->queryOne()) {
                    $tt = (int)$tt['pk_typetask'];
                }
            }
            //end тип задачи найти
            
            //ид контакта найти
            $fk_crmcontact = NULL;
            
            if($c['fk_crmcontact']) {
                $fk_crmcontact = Yii::$app->db->createCommand("SELECT `pk_contact` FROM `crm_contact` WHERE `old_pkcontact` = " . $c['fk_crmcontact'])->queryOne();
                $fk_crmcontact = $fk_crmcontact['pk_contact'];
            }
            //end ид контакта найти
            
            //ид компании найти
            $fk_crmcompany = NULL;
            
            if($c['fk_crmcompany']) {
                $fk_crmcompany = Yii::$app->db->createCommand("SELECT `pk_company` FROM `crm_company` WHERE `old_pkcompany` = " . $c['fk_crmcompany'])->queryOne();
                $fk_crmcompany = $fk_crmcompany['pk_company'];
            }
            //end ид компании найти
            
            //результат задачи найти
            $rr = NULL;
            if($c['fk_task_result']) {
                $r = Yii::$app->db->createCommand("SELECT `name_task_result` FROM `crm_tasks_results` WHERE `pk_task_result` = " . $c['fk_task_result'])->queryOne();
                
                $rr = Yii::$app->db->createCommand("SELECT `pk_task_result` FROM `crm_tasks_results` WHERE `fk_department` = " . $dep_to . " AND `name_task_result` = '" . $r['name_task_result'] . "'")->queryOne();
                $rr = (int)$rr['pk_task_result'];
            }
            //end результат задачи найти
            
            $ins[] = [
                $c['date_task'],
                $c['time_task'],
                $c['created_user'],
                $c['fk_user_task'],
                $tt,
                $fk_crmcontact,
                $fk_crmcompany,
                $c['text_task'],
                $c['create_date'],
                $c['update_date'],
                $c['updated_user'],
                $c['status_task'],
                $c['status_text'],
                $c['result_task'],
                $rr,                
                $dep_to
            ];
            
            $i++;
            
            if($i === 5000) { // скинуть, обновить
                $i = 0;
                $inserted = Yii::$app->db->createCommand()->batchInsert(\app\modules\crm\models\CrmTasks::tableName(), [
                    'date_task',
                    'time_task',
                    'created_user',
                    'fk_user_task',
                    'fk_typetask',
                    'fk_crmcontact',
                    'fk_crmcompany',
                    'text_task',
                    'create_date',
                    'update_date',
                    'updated_user',
                    'status_task',
                    'status_text',
                    'result_task',
                    'fk_task_result',            
                    'fk_department'
                ], $ins)->execute();
                $ins = [];
                echo $inserted . ' задач' . PHP_EOL;
            }
        }
        $inserted = Yii::$app->db->createCommand()->batchInsert(\app\modules\crm\models\CrmTasks::tableName(), [
            'date_task',
            'time_task',
            'created_user',
            'fk_user_task',
            'fk_typetask',
            'fk_crmcontact',
            'fk_crmcompany',
            'text_task',
            'create_date',
            'update_date',
            'updated_user',
            'status_task',
            'status_text',
            'result_task',
            'fk_task_result',            
            'fk_department'
        ], $ins)->execute();
        $ins = [];
        echo $inserted . ' задач' . PHP_EOL;
        unset($crm_tasks);        
        //end задачи
        
        
        //сделки
        /*$crm_deals = Yii::$app->db->createCommand("SELECT * FROM `crm_deals` WHERE `fk_department` = " . $dep_from)->queryAll();
        
        $ins = [];
        foreach ($crm_deals as $c) {
            //типы сделок
            $tt = NULL;
            if($c['type_deal']) {
                $t = Yii::$app->db->createCommand("SELECT `name_types_deal` FROM `crm_types_deal` WHERE `fk_department` = " . $dep_from . " AND `pk_types_deal` = " . $c['type_deal'])->queryOne();
            
                if($tt = Yii::$app->db->createCommand("SELECT `pk_types_deal` FROM `crm_types_deal` WHERE `fk_department` = " . $dep_to . " AND `name_types_deal` = '" . $t['name_types_deal'] . "'")->queryOne()) {
                    $tt = (int)$tt['pk_types_deal'];
                }
            }
            //end типы сделок
            
            //проекты сделок
            $dp = NULL;
            if($c['fk_deals_project']) {
                $t = Yii::$app->db->createCommand("SELECT `name_dp` FROM `crm_deals_projects` WHERE `fk_department` = " . $dep_from . " AND `pk_dp` = " . $c['fk_deals_project'])->queryOne();
            
                if($dp = Yii::$app->db->createCommand("SELECT `pk_dp` FROM `crm_deals_projects` WHERE `fk_department` = " . $dep_to . " AND `name_dp` = '" . $t['name_dp'] . "'")->queryOne()) {
                    $dp = (int)$dp['pk_dp'];
                }
            }
            //end проекты сделок
            
            //ид контакта найти
            $fk_crmcontact = NULL;
            
            if($c['fk_crmcontact']) {
                $fk_crmcontact = Yii::$app->db->createCommand("SELECT `pk_contact` FROM `crm_contact` WHERE `old_pkcontact` = " . $c['fk_crmcontact'])->queryOne();
                $fk_crmcontact = $fk_crmcontact['pk_contact'];
            }
            //end ид контакта найти
            
            //ид компании найти
            $fk_crmcompany = NULL;
            
            if($c['fk_crmcompany']) {
                $fk_crmcompany = Yii::$app->db->createCommand("SELECT `pk_company` FROM `crm_company` WHERE `old_pkcompany` = " . $c['fk_crmcompany'])->queryOne();
                $fk_crmcompany = $fk_crmcompany['pk_company'];
            }
            //end ид компании найти
            
            $ins[] = [
                $c['name_deal'],
                $c['price_deal'],
                $c['status_deal'],
                $tt,
                $dp,
                $fk_crmcontact,
                $fk_crmcompany,
                $c['create_date'],
                $c['responsible_user'],
                $c['created_user'],
                $c['update_date'],
                $c['updated_user'],
                $c['status_date'],
                $dep_to
            ];
        }
        
        $inserted = Yii::$app->db->createCommand()->batchInsert(\app\modules\crm\models\CrmDeals::tableName(), [
            'name_deal',
            'price_deal',
            'status_deal',
            'type_deal',
            'fk_deals_project',
            'fk_crmcontact',
            'fk_crmcompany',
            'create_date',
            'responsible_user',
            'created_user',
            'update_date',
            'updated_user',
            'status_date',
            'fk_department'
        ], $ins)->execute();
        echo $inserted . ' сделок' . PHP_EOL;
        unset($crm_deals);
        //end сделки
        
        
        //связи тегов
        $crm_relation_tags = Yii::$app->db->createCommand("SELECT * FROM `crm_relation_tags` LEFT JOIN `crm_company` ON `pk_company` = `fk_crmcompany` WHERE `fk_department` = " . $dep_from)->queryAll();
        
        $ins = [];
        foreach ($crm_relation_tags as $c) {
            //ид контакта найти
            $fk_crmcontact = NULL;
            
            if($c['fk_crmcontact']) {
                $fk_crmcontact = Yii::$app->db->createCommand("SELECT `pk_contact` FROM `crm_contact` WHERE `old_pkcontact` = " . $c['fk_crmcontact'])->queryOne();
                $fk_crmcontact = $fk_crmcontact['pk_contact'];
            }
            //end ид контакта найти
            
            //ид компании найти
            $fk_crmcompany = NULL;
            
            if($c['fk_crmcompany']) {
                $fk_crmcompany = Yii::$app->db->createCommand("SELECT `pk_company` FROM `crm_company` WHERE `old_pkcompany` = " . $c['fk_crmcompany'])->queryOne();
                $fk_crmcompany = $fk_crmcompany['pk_company'];
            }
            //end ид компании найти
            $tag = Yii::$app->db->createCommand("SELECT `name_tag` FROM `crm_tags` WHERE `fk_department` = " . $dep_from . " AND `pk_tag` = " . $c['fk_tag'])->queryOne();

            if($t = Yii::$app->db->createCommand("SELECT `pk_tag` FROM `crm_tags` WHERE `fk_department` = " . $dep_to . " AND `name_tag` = '" . $tag['name_tag'] . "'")->queryOne()) {
                $t = (int)$t['pk_tag'];
            }
            //ид тега
            
            //end ид тега
            
            $ins[] = [
                $t,
                $fk_crmcompany,
                $fk_crmcontact
            ];
        }
        
        $crm_relation_tags = Yii::$app->db->createCommand("SELECT * FROM `crm_relation_tags` LEFT JOIN `crm_contact` ON `pk_contact` = `fk_crmcontact` WHERE `fk_department` = " . $dep_from)->queryAll();
        
        foreach ($crm_relation_tags as $c) {
            //ид контакта найти
            $fk_crmcontact = NULL;
            
            if($c['fk_crmcontact']) {
                $fk_crmcontact = Yii::$app->db->createCommand("SELECT `pk_contact` FROM `crm_contact` WHERE `old_pkcontact` = " . $c['fk_crmcontact'])->queryOne();
                $fk_crmcontact = $fk_crmcontact['pk_contact'];
            }
            //end ид контакта найти
            
            //ид компании найти
            $fk_crmcompany = NULL;
            
            if($c['fk_crmcompany']) {
                $fk_crmcompany = Yii::$app->db->createCommand("SELECT `pk_company` FROM `crm_company` WHERE `old_pkcompany` = " . $c['fk_crmcompany'])->queryOne();
                $fk_crmcompany = $fk_crmcompany['pk_company'];
            }
            //end ид компании найти
            $tag = Yii::$app->db->createCommand("SELECT `name_tag` FROM `crm_tags` WHERE `fk_department` = " . $dep_from . " AND `pk_tag` = " . $c['fk_tag'])->queryOne();

            if($t = Yii::$app->db->createCommand("SELECT `pk_tag` FROM `crm_tags` WHERE `fk_department` = " . $dep_to . " AND `name_tag` = '" . $tag['name_tag'] . "'")->queryOne()) {
                $t = (int)$t['pk_tag'];
            }
            //ид тега
            
            //end ид тега
            
            $ins[] = [
                $t,
                $fk_crmcompany,
                $fk_crmcontact
            ];
        }
        
        $inserted = Yii::$app->db->createCommand()->batchInsert(\app\modules\crm\models\CrmRelationTags::tableName(), [
            'fk_tag',
            'fk_crmcompany',
            'fk_crmcontact'
        ], $ins)->execute();
        echo $inserted . ' связей тегов' . PHP_EOL;
        unset($crm_relation_tags);*/
        //end связи тегов
    }
    
    public function actionCrmreplace() {
        die();
        $arr = [];
        $users = Yii::$app->db->createCommand("SELECT id_user, fk_department FROM user WHERE fk_role = 7")->queryAll();
        foreach ($users as $user) {
            $arr[] = [$user['id_user'], $user['fk_department'], 2];
        }
        
        if($arr) {
            $a = Yii::$app->db->createCommand()->batchInsert(\app\modules\crm\models\CrmAccessSettings::tableName(), ['fk_user', 'fk_department', 'fk_role'], $arr)->execute();
            var_dump($a);
            $tasks = Yii::$app->db->createCommand("UPDATE `user` SET `fk_role`=3 WHERE `fk_role` = 7;")->execute();
            $tasks = Yii::$app->db->createCommand("DELETE FROM `general_roles` WHERE `general_roles`.`pk_role` = 7;")->execute();
            $tasks = Yii::$app->db->createCommand("DELETE FROM `general_roles` WHERE `general_roles`.`pk_role` = 7;")->execute();
        }
        
        $s = \app\modules\user\models\User::find()->select('id_user')->joinWith('fkStatus')->where(['status' => 9])->all();
        $as = [];
        foreach ($s as $ss) {
            array_push($as, $ss->id_user);
        }
        if($as) {
            $f = \app\modules\crm\models\CrmAccessSettings::deleteAll(['in', 'fk_user', $as]);
            var_dump($f);
        }
    }

    /*
    * слияние без проверки на дублирование
    * остальное не переносится
    * + ответственные тоже не меняются
    */
    public function actionImplode() {
        $type_client = 'fk_crmcompany';//тип клиента (компания/контакт) - должны быть одинаковы
        $imploding = 61254907;//какие слить
        $to = 61254796;//в какой слить
        
        //задачи + комменты перенести
        $tasks = Yii::$app->db->createCommand("UPDATE `crm_tasks` SET `" . $type_client . "` = " . $to . " WHERE `" . $type_client . "` = " . $imploding)->execute();
        echo 'задачи ' . $tasks . PHP_EOL;
        
        //слияние контактной инфы
        $infos = Yii::$app->db->createCommand("UPDATE `crm_contacts_info` SET `" . $type_client . "` = " . $to . " WHERE `" . $type_client . "` = " . $imploding)->execute();
        echo 'контактная информация ' . $infos . PHP_EOL;
        
        //слияние сделок
        $infos = Yii::$app->db->createCommand("UPDATE `crm_deals` SET `" . $type_client . "` = " . $to . " WHERE `" . $type_client . "` = " . $imploding)->execute();
        echo 'сделки ' . $infos . PHP_EOL;
        
        if($type_client === 'fk_crmcompany') {//только для компаний - перенос контактов
            $contacts = Yii::$app->db->createCommand("UPDATE `crm_contact` SET `fk_crm_company` = " . $to . " WHERE `fk_crm_company` = " . $imploding)->execute();
            echo 'контакты компании ' . $contacts . PHP_EOL;
        }
    }
    
    /*
     * превращение телефонных номеров
     * запускать 1 раз, т.к происиходит преобразование номеров в более единый формат
     */
    public function actionIndex() {        
        $arrToDel = [];
        
        //номера типа 66 66 66, 66-66-66,666 666, привести в 666-666
        $models = Yii::$app->db->createCommand("SELECT * FROM crm_contacts_info WHERE contact_value REGEXP '^[0-9]{2} [0-9]{2} [0-9]{2}$' OR contact_value REGEXP '^[0-9]{2}-[0-9]{2}-[0-9]{2}$' OR contact_value REGEXP '^[0-9]{3} [0-9]{3}$'")->queryAll();
        
        $arrToDel = $this->recurMobile($models);
    }
    
    /*
     * превращение в сплошное число для поиска
     */
    public function toSearch($models) {
        $arrToDel = [];
        foreach ($models as $phone) {
            $nomer = preg_replace("/\D/","",$phone['contact_value']);            
            
            if(strlen($nomer) < 5) {//некорректный(слишком малой) номер типа(333t2) - удалить
                $arrToDel[] = $phone['pk_contacts_info'];
            } else {//превратить в число и обновить
                echo $nomer.' - '.$phone['contact_value'].' ('.$phone['pk_contacts_info'].')'.PHP_EOL;
                Yii::$app->db->createCommand("UPDATE ".CrmContactsInfo::tableName()." SET contact_value_search = '".$nomer."' WHERE pk_contacts_info = ".$phone['pk_contacts_info'].';')->execute();
            }
        }
        return $arrToDel;
    }
    
    /*
     * преобразование мобильных в разные номера
     */
    protected function recurMobile($models, $rec = false, $continue = false) {
        $delReccomp = $delReccont = $arrToDel = $at = [];
        
        foreach ($models as $phone) {
            //var_dump($phone);
            if($rec) {
                $to = $val = '';
                $del = false;
                if($phone['fk_crmcompany']) {                    
                    $to = 'fk_crmcompany';
                    $val = $phone['fk_crmcompany'];
                    if(!in_array($val, $delReccomp)){
                        $del = true;
                        array_push($delReccomp, $val);
                    }
                } else if($phone['fk_crmcontact']) {
                    $to = 'fk_crmcontact';
                    $val = $phone['fk_crmcontact'];
                    if(!in_array($val, $delReccont)){
                        $del = true;
                        array_push($delReccont, $val);
                    }
                } else continue;
                
                //удалить только рекурсивную и сохранить как новые
                if($del) {
                    //$this->deleteContacts([$phone['pk_contacts_info']]);
                }
            }
            
            $comm = '';
            $nomer = preg_replace("/\D/","",$phone['contact_value']);
            $strn = strlen($nomer);//длина числового номера
            
            if(($strn + 3) < strlen($phone['contact_value'])) {//что то еще содержится - в комментарий пересохранить?
                $comm = preg_replace('/[0-9]+/', '', mb_substr($phone['contact_value'], $strn+1));
                //$comm = preg_replace('/[-]+/', '', $comm);
                if(strlen($comm) < 7) $comm = NULL;
            }
            
            if(stripos($phone['contact_value'], '(3852)')) {
                $p = explode('(3852)', $phone['contact_value']);
                //echo 'город | '.$p[1].' / '.strlen($comm).' ** '.$comm.' id:'.$phone['pk_contacts_info'].PHP_EOL;
            }
            
            if(strlen($nomer) == 10 || strlen($nomer) == 11) {//стандартные номера
                if(strlen($nomer) == 11) {//11 цифр - убрать первую(восьмерка, семерка = ясное дело, она и так добавится)
                    $nomer = substr($nomer, 1);
                }
                $savedFormat = $this->toMobile($nomer);//GG

                //echo 'охуенный мобильный: ' . $savedFormat .' / '.strlen($comm).' ** '.$comm.' id:'.$phone['pk_contacts_info'].PHP_EOL;
                /*if($rec) {
                    $this->newTable($to, $savedFormat, $phone['contact_field'], $val);
                } else {
                    $this->updateTable($phone['pk_contacts_info'], $savedFormat, $comm, 'mobilePhone');//перевести в мобильный
                }*/
            } elseif(strlen($nomer) == 6) {//изменить на городской номер с меткой workPhone
                $savedFormat = $this->toPhone($nomer);
                
                echo 'нормальный городской: ' . $savedFormat . ' | ' .$phone['pk_contacts_info'].PHP_EOL;
                /*if($rec) {
                    $this->newTable($to, $savedFormat, $phone['contact_field'], $val);
                } else {*/
                    $this->updateTable($phone['pk_contacts_info'], $savedFormat, $comm);
                //}
            } elseif(strlen($nomer) > 10 && !$continue) {//возможно номер состоит из нескольких. попытаться разделить
                $expz = explode(',', $phone['contact_value']);//разделить на запятые
                
                $arr = [];
                foreach ($expz as $xnomer) {//подготовить и отправить на рекурсию
                    $arr[] = ['contact_value' => $xnomer, 'contact_field' => $phone['contact_field'], 'pk_contacts_info' => $phone['pk_contacts_info'], 'fk_crmcompany' => $phone['fk_crmcompany'], 'fk_crmcontact' => $phone['fk_crmcontact']];
                }
                
                $this->recurMobile($arr, true, true);
            } elseif($continue) {//не трогать их, проще
                self::$_counter[] = $phone['pk_contacts_info'];
            } elseif(strlen($nomer) < 5) {//некорректный(слишком малой) номер типа(333t2) - удалить
                $arrToDel[] = $phone['pk_contacts_info'];
            } else {//сохранить сплошняком
                //echo $phone['contact_field'];
                /*if($rec) {
                    $this->newTable($to, $nomer, $phone['contact_field'], $val);
                } else {
                    $this->updateTable($phone['pk_contacts_info'], $nomer, $comm);
                }*/
            }
        }
        return $arrToDel;
    }
    
    //удалить по ид МАССИВ
    protected function deleteContacts($arrId) {
        if($arrId) {
            $sid = implode(',', $arrId);
            $c = Yii::$app->db->createCommand("DELETE FROM ".CrmContactsInfo::tableName()." WHERE pk_contacts_info IN (".$sid.")")->execute();
            echo 'Удалено '.$c.' телефонных номеров ('.$sid.')'.PHP_EOL;
        }
        return [];
    }
    
    //вернуть шаблонный номер мобильный
    protected function toMobile($str) {
        return preg_replace("/(\d{3})(\d{3})(\d{2})(\d{2})/", "+7 $1-$2-$3-$4", $str);
    }
    
    //вернуть шаблонный номер городской(6 цифр)
    protected function toPhone($str) {
        return preg_replace("/(\d{3})(\d{3})/", "$1-$2", $str);
    }
    
    //обновить таблицу по ид - новое значение
    protected function updateTable($pk, $val, $coment = NULL) {
        $adds = '';
        if($coment) $adds = ", contact_comment = '".$coment."'";
        Yii::$app->db->createCommand("UPDATE ".CrmContactsInfo::tableName()." SET contact_value = '".$val."' ".$adds." WHERE pk_contacts_info = ".$pk.';')->execute();
    }
    
    protected function newTable($cond, $val, $cf, $condval) {
        Yii::$app->db->createCommand("INSERT INTO ".CrmContactsInfo::tableName()." (`contact_field`, `contact_value`, $cond) VALUES ('$cf', '$val', $condval)")->execute();
    }
}
