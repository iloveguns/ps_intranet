<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\FunctionModel;
use app\modules\crm\models\CrmTasks;

class CrmTaskController extends Controller
{
    /*
     * ставить статус просроченные каждые полчаса
     */
    public function actionIndex()
    {
        $date = FunctionModel::getDateWParam('Y-m-d');
        $time = FunctionModel::getDateWParam('H:i');
        
        //так то не очень большая выборка будет
        //не выбирать все комменты и тд, только актуальные задачи
        $models = Yii::$app->db->createCommand('SELECT pk_task, date_task, time_task FROM '.CrmTasks::tableName().' WHERE status_task != :status_task AND status_task != :status_task_done AND status_task != :status_task_comment AND status_task != :status_task_syscomment AND date_task <= :date_task')
            ->bindValue(':status_task', CrmTasks::STATUS_LATE)
            ->bindValue(':status_task_comment', CrmTasks::STATUS_COMMENT)
            ->bindValue(':status_task_syscomment', CrmTasks::STATUS_SYSCOMMENT)
            ->bindValue(':status_task_done', CrmTasks::STATUS_DONE)
            ->bindValue(':date_task', $date)
            ->queryAll();
        
        foreach ($models as $model) {
            if($model['date_task'] == $date && $model['time_task'] == 'allday') {//задачи на сегодня, весь день. они еще не просрочены
                continue;//пропускать 
            } else if($model['time_task'] <= $time || ($model['date_task'] < $date && $model['time_task'] == 'allday')){//задачи со временем меньше или (датой меньше и allday, то есть весь день вчера и ранее)
                Yii::$app->db->createCommand()->update(CrmTasks::tableName(), ['status_task' => CrmTasks::STATUS_LATE], ['pk_task' => $model['pk_task']])->execute();
            }
        }
    }
    
    /*
     * лол втф хуйня
     * у коренкевич постоянно задачи просрочены даже на 3 месяца вперед
     * исправление-костыль
     */
    public function actionChange() {
        $date = FunctionModel::getDateWParam('Y-m-d');
        $time = FunctionModel::getDateWParam('H:i');
        
        $models = Yii::$app->db->createCommand('SELECT pk_task, date_task, time_task FROM '.CrmTasks::tableName().' WHERE status_task = :status_task AND date_task > :date_task')
            ->bindValue(':status_task', CrmTasks::STATUS_LATE)
            ->bindValue(':date_task', $date)
            ->queryAll();
        
        foreach ($models as $model) {
            Yii::$app->db->createCommand()->update(CrmTasks::tableName(), ['status_task' => CrmTasks::STATUS_WORK], ['pk_task' => $model['pk_task']])->execute();
        }
    }
}
