<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\modules\crm\models\CrmCompany;
use app\modules\crm\models\CrmContactsInfo;

class CopyController extends Controller {
    /*
     * отделение компаний с контактами, которые попали в другой отдел
     * 14 - отдел, к которому прикрепились компании отдела 29
     */
    public function actionIndex() {
        die();
        $prob_otdel = 13;
        $copies = Yii::$app->db->createCommand("SELECT DISTINCT `fk_crm_company` FROM `crm_contact` WHERE `fk_crm_company` IN(SELECT `pk_company` FROM `crm_company` WHERE `fk_department` = " . $prob_otdel . ") AND `fk_department` = 29")->queryAll();
        foreach ($copies as $copy) {
            //поиск существующей компании в отделе по ид
            $oldpk = Yii::$app->db->createCommand("SELECT `old_pkcompany` FROM `crm_company` WHERE `pk_company` = " . $copy['fk_crm_company'])->queryOne();
            $found = Yii::$app->db->createCommand("SELECT `pk_company` FROM `crm_company` WHERE `old_pkcompany` = " . $oldpk['old_pkcompany'] . " AND `fk_department` = 29")->queryOne();
            
            if($found) {
                $newid = $found['pk_company'];
            } else {
                //var_dump($copy['fk_crm_company']);continue;//первый раз до сюда запускал, чтоб создать ненайденные компании
                //копирование компании
                $resp = Yii::$app->db->createCommand("SELECT `responsible_user` FROM `crm_contact` WHERE `fk_crm_company` = " . $copy['fk_crm_company'] . " AND `fk_department` = 29")->queryOne();
                if(!$resp['responsible_user']) $resp['responsible_user'] = 98;

                $model = CrmCompany::findOne($copy['fk_crm_company']);
                $attrs = $model->attributes;
                unset($attrs['pk_company']);
                $attrs['created_user'] = 98;//руководитель отдела
                $attrs['fk_department'] = 29;//ид отдела
                $attrs['responsible_user'] = $attrs['updated_user'] = $attrs['created_user'] = $resp['responsible_user'];//только на этом основании можно судить
                //end копирование компании
                
                if(Yii::$app->db->createCommand()->insert(CrmCompany::tableName(), $attrs)->execute()) {
                    $newid = Yii::$app->db->getLastInsertID();
                }
            }
            //поиск существующей компании в отделе по ид
            
            //var_dump($copy['fk_crm_company'] . ' - ' . $newid);//ид копируемой - куда копировать

            //копирование контактной информации
            $cinfo = Yii::$app->db->createCommand("SELECT * FROM `crm_contacts_info` WHERE `fk_crmcompany` = " . $copy['fk_crm_company'])->queryOne();
            $cmodel = CrmContactsInfo::find()->asArray()->where(['fk_crmcompany' => $copy['fk_crm_company']])->all();
            foreach ($cmodel as $cm) {
                unset($cm['pk_contacts_info']);
                $cm['fk_crmcompany'] = $newid;

                Yii::$app->db->createCommand()->insert(CrmContactsInfo::tableName(), $cm)->execute();
            }
            //end копирование контактной информации

            //обновить ид контактов после создания
            Yii::$app->db->createCommand('UPDATE `crm_contact` SET `fk_crm_company` = ' . $newid . ' WHERE `fk_crm_company` = ' . $copy['fk_crm_company'] . ' AND `fk_department` = 29')->execute();

            //обновить задачи
            Yii::$app->db->createCommand('UPDATE `crm_tasks` SET `fk_crmcompany` = ' . $newid . ' WHERE `fk_crmcompany` = ' . $copy['fk_crm_company'] . ' AND `fk_department` = 29')->execute();

            //обновить сделки
            Yii::$app->db->createCommand('UPDATE `crm_deals` SET `fk_crmcompany` = ' . $newid . ' WHERE `fk_crmcompany` = ' . $copy['fk_crm_company'] . ' AND `fk_department` = 29')->execute();
            
        }
    }
    
    /**
     * очистка, удаление дублирующихся контактных данных компаний, контактов
     */
    public function actionClearcinfo() {
        $alldeleted = 0;
        $companies = Yii::$app->db->createCommand("SELECT `pk_company` FROM `crm_company`")->queryAll();
        
        foreach ($companies as $company) {
            $cdatum = Yii::$app->db->createCommand("SELECT `pk_contacts_info`, `contact_field`, `contact_value` FROM `crm_contacts_info` WHERE `fk_crmcompany` = " . $company['pk_company'])->queryAll();
            $idata = [];
            $delids = [];
            //собрать ид лишних данных
            foreach ($cdatum as $cdata) {
                $c = $cdata['contact_field'] . '_' . $cdata['contact_value'];

                if(in_array($c, $idata)) {
                    $delids[] = $cdata['pk_contacts_info'];
                } else {
                    $idata[] = $c;
                }
            }
            
            if($delids) {
                $deleted = Yii::$app->db->createCommand('DELETE FROM `crm_contacts_info` WHERE `crm_contacts_info`.`pk_contacts_info` IN (' . implode(',', $delids) . ')')->execute();
                echo 'Удалено: ' . $deleted . PHP_EOL;
                $alldeleted += $deleted;
            }
        }
        unset($companies);
        echo 'Всего удалено у компаний: ' . $alldeleted . PHP_EOL;
        
        $contacts = Yii::$app->db->createCommand("SELECT `pk_contact` FROM `crm_contact`")->queryAll();
        
        foreach ($contacts as $contact) {
            $cdatum = Yii::$app->db->createCommand("SELECT `pk_contacts_info`, `contact_field`, `contact_value` FROM `crm_contacts_info` WHERE `fk_crmcontact` = " . $contact['pk_contact'])->queryAll();
            $idata = [];
            $delids = [];
            //собрать ид лишних данных
            foreach ($cdatum as $cdata) {
                $c = $cdata['contact_field'] . '_' . $cdata['contact_value'];

                if(in_array($c, $idata)) {
                    $delids[] = $cdata['pk_contacts_info'];
                } else {
                    $idata[] = $c;
                }
            }
            
            if($delids) {
                $deleted = Yii::$app->db->createCommand('DELETE FROM `crm_contacts_info` WHERE `crm_contacts_info`.`pk_contacts_info` IN (' . implode(',', $delids) . ')')->execute();
                echo 'Удалено: ' . $deleted . PHP_EOL;
                $alldeleted += $deleted;
            }
        }
        unset($contacts);
        echo 'Всего удалено у контактов: ' . $alldeleted . PHP_EOL;
    }
}
