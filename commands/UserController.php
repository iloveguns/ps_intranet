<?php

namespace app\commands;

use Yii;
use app\models\SendNoticesAsync;

use yii\console\Controller;
use app\models\FunctionModel;
use app\modules\user\models\UserStatus;
use app\models\UnreadNotice;
use app\models\UsersOnline;

class UserController extends Controller {
    /*
     * удалить уведомления у удаленных сотрудников
     * чистит мало
     */
    public function actionRemoveNotification() {
        $arrIdusers = [];
        
        $todaymonthday = FunctionModel::getDateWParam('m-d');
        $todayDate = FunctionModel::getDateWParam('Y-m-d');        
        
        $statuses = \Yii::$app->db->createCommand('SELECT id_user FROM `user` LEFT JOIN users_online ON user.id_user = users_online.pk_user LEFT JOIN user_status ON user.fk_status = user_status.pk_status WHERE users_online.last_online IS NULL OR user_status.status = '.UserStatus::FIRED)->queryAll();

        foreach ($statuses as $status) {
            $arrIdusers[$status['id_user']] = $status['id_user'];            
            
            /*if(!isset($status->user->id_user)){//найти испорченые статусы
                var_dump($status->pk_status);
            }*/
        }
        $a = UnreadNotice::deleteAll(['in', 'id_user', $arrIdusers]);
        echo 'Удалено '.$a.' уведомлений'.PHP_EOL;
        
        // ну они не нужны
        $b = SendNoticesAsync::deleteAll(['is_sended' => 1]);
        echo 'Удалено '.$b.' уведомлений об отправке'.PHP_EOL;
    }
    
    /**
     * вызывается из интранета для параллельного запуска, без ожидания
     * при ошибке (Calling unknown method) не прерывается
     */
    public function actionSendnoticesasync() {
        $arrSaveData = [];
        $usersToIO = []; // массив для набивания ид сотрудников, которым нужно обновить уведомления
        $toUpdate = []; // ид пройденных для обновления в таблице, что отправлено
        
        // выбрать всех неотправленных ВООБЩЕ
        $send_notices = Yii::$app->db->createCommand(
            'SELECT `pk_sna`, `item_class`, `item_id`, `type`, `from_user`, `is_sended`, `fk_user`, `setting_notify_email`, `telegram_chatid`, `email` '
            . 'FROM `' . SendNoticesAsync::tableName() . '` '
            . 'LEFT JOIN user ON `' . SendNoticesAsync::tableName() . '`.`fk_user` = `' . \app\modules\user\models\User::tableName() . '`.`id_user` '
            . 'WHERE `is_sended` = 0'
        )->queryAll();
                
        foreach ($send_notices as $send_notice) {
            $class      = $send_notice['item_class'];
            $pk         = $send_notice['item_id'];
            $id_user    = (int)$send_notice['fk_user'];
            $type       = $send_notice['type'];
            $from_user  = $send_notice['from_user'];
            
            if((int)$send_notice['setting_notify_email'] === 1 && Yii::$app->params['sendNotify']) {                
                try {
                    // сначала создание
                    $m = new UnreadNotice();
                    $m->class = $class;
                    $m->item_id = $pk;
                    $m->type = $type;
                    $m->id_user = $id_user;
                    $m->from_user = $from_user;
                    $m->save();
                    
                    // потом это же уведомление получить
                    $api = UnreadNotice::ApiNoticesUser($id_user, false, 1);
                    $one = array_shift($api['notices']);
                    $link = \app\modelsView\ViewUnreadNotice::newHtmlNotify($one, false);

                    $arrSaved[$id_user] = ['id_notice' => $m->id_notice, 'link' => $link];

                    @\app\helpers\Mail::send($send_notice['email'], Yii::t('app', 'New Notification'), ['text' => $link], '@app/mail/notify/notify');
                } catch (\yii\base\UnknownMethodException $ex) {
                    $t = $ex->getMessage() . '<br>MODEL: "' . SendNoticesAsync::tableName() . '"<br>JSON:<br>' . json_encode($send_notice);
                    @\app\helpers\Mail::send(Yii::$app->params['adminEmail'], 'ошибка интранета', ['text' => $t], '@app/mail/notify/notify');
                }
            }
            
            if(!empty($send_notice['telegram_chatid']) && Yii::$app->params['sendNotify']) {
                if(isset($arrSaved[$id_user])) {//уже сохранено уведомление
                    $link = $arrSaved[$id_user]['link'];
                } else {//сохранять по одиночке
                    try {
                        $m = new UnreadNotice();
                        $m->class = $class;
                        $m->item_id = $pk;
                        $m->type = $type;
                        $m->id_user = $id_user; 
                        $m->from_user = $from_user;
                        $m->save();
                        
                        $api = UnreadNotice::ApiNoticesUser($id_user, false, 1);
                        $one = array_shift($api['notices']);
                        $link = \app\modelsView\ViewUnreadNotice::newHtmlNotify($one, false);
                        
                        $arrSaved[$id_user] = ['id_notice' => $m->id_notice, 'link' => $link];
                    } catch (\yii\base\UnknownMethodException $ex) {
                        $t = $ex->getMessage() . '<br>MODEL: "' . SendNoticesAsync::tableName() . '"<br>JSON:<br>' . json_encode($send_notice);
                        @\app\helpers\Mail::send(Yii::$app->params['adminEmail'], 'ошибка интранета', ['text' => $t], '@app/mail/notify/notify');
                    }
                }
                try {
                    // 18 апреля 2018 - невозможно отправить в телеграм
                    // @\app\models\sendTelegram::send([$send_notice['telegram_chatid']], ['text' => $link]);//по одиночке
                } catch (Exception $ex) {

                }
            }
            
            /**
             * сохранение всех остальных, кому ни в почту, ни в телеграм не надо просто и быстро
             * или если не надо отправлять во внешние источники(только в интранет)
             */
            if(
                ((int)$send_notice['setting_notify_email'] === 0 && empty($send_notice['telegram_chatid'])) ||
                !Yii::$app->params['sendNotify']
            ) {
                $arrSaveData[] = [$class, $pk, $id_user, $type, $from_user];
            }
            
            array_push($toUpdate, $send_notice['pk_sna']);
            array_push($usersToIO, $id_user);
        }
        
        unset($send_notices);

        // массово сохранить уведомления
        if(!empty($arrSaveData)) {
            Yii::$app->db->createCommand()->batchInsert(UnreadNotice::tableName(), ['class', 'item_id', 'id_user', 'type', 'from_user'], $arrSaveData)->execute();
        }
        
        // массово обновить
        if(!empty($toUpdate)) {
            SendNoticesAsync::updateAll(['is_sended' => 1], ['IN', 'pk_sna', $toUpdate]);
        }
        
        unset($arrSaveData);
        
        //и отправка уведомлений массовая
        $io = new \app\nodejs\socketio\php\sendSocketIo();
        $io->send('notify', ['uniqid' => $usersToIO, 'updateNotify' => true]);//отправить всем метку о том, что надо обновить кол-во уведомлений у сервера
    }
}
