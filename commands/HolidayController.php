<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\modules\admins\models\Holidays;
use app\models\FunctionModel;
use app\modules\user\models\User;
use app\modules\intranet\models\Callboard;
use app\models\UnreadNotice;

class HolidayController extends Controller {
    /*
     * посмотреть праздники и создать объявление
     */
    public function actionIndex()
    {
        $todaymonthday = FunctionModel::getDateWParam('m-d');
        $todayDate = FunctionModel::getDateWParam('Y-m-d');
        
        $model = Holidays::find()->where(['like', 'date', $todaymonthday])->all();
        
        foreach ($model as $user) {
            $callboard = new Callboard();
            $callboard->fk_callboard_category = Callboard::HOLIDAY;
            $callboard->id_author = NULL;//важно
            $callboard->title = Yii::t('app/nulluser', 'Today, {date} {holiday}', ['date' => Yii::$app->formatter->asDate($todayDate), 'holiday' => $user->title]);
            $callboard->text = Yii::t('app/nulluser', 'Сongratulations');
            
            $callboard->save();
            
            //всем уведомление кинуть
            foreach (User::findActive()->all() as $userNotice) {
                $notice = new UnreadNotice();
                $notice->class = $callboard->getName();
                $notice->item_id = $callboard->pk_callboard;
                $notice->id_user = $userNotice->id_user;
                $notice->type = UnreadNotice::NEW_CONGRAT;
                $notice->save();
            }
        }
    }
}
