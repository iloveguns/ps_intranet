<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>

<div class="col-md-12">
    <div class="box box-success box-notelist" data-attach="note<?=$model->pk_pnote?>" data-title="<?=Html::encode($model->title_pnote)?>">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::a(Html::encode($model->title_pnote), ['/user/personal-notes/view', 'id' => $model->pk_pnote], ['target' => '_blank']) ?></h3>
        </div>
        <div class="box-body">
            <?= mb_substr(HtmlPurifier::process($model->text_pnote), 0, 120, "utf-8") ?>
        </div>
    </div>
</div>