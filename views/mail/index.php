<?php
    use yii\helpers\Html;
?>

<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
    Посмотреть <?= $models['names'] ?>
</button>
<div class="collapse" id="collapseExample">
    <table class="table table-responsive table-striped">
        <tr>
            <th><?= $models['names'] ?></th>
            <th>Емайлы</th>
            <th>Удалить</th>
        </tr>
        <?php foreach ($models['data'] as $pk => $model) : ?>
            <tr>
                <td><?= $model['name'] ?></td>
                <td class="emails" data-emails="<?= implode(',', $model['emails']) ?>">
                    <?php if($model['emails']) : ?>
                        <?= implode(', ', $model['emails']) ?>
                    <?php endif ?>
                </td>
                <td>
                    <i class="fa fa-times text-red del-table" aria-hidden="true"></i>
                </td>
            </tr>
        <?php endforeach ?>
    </table>
</div>

<?= Html::hiddenInput('emails', implode(',', $emails), ['id' => 'emails']) ?>

<div class="row">
    <div class="form-group col-md-3">
        <label>Шаблон письма</label>
        <?= kartik\select2\Select2::widget([
            'name' => 'template',
            'data' => \app\models\ApiSendmailTemplates::getAllMy(),
            'options' => [
                'id' => 'select-template',
                'placeholder' => Yii::t('app', 'Select'),
            ],
        ]); ?>
    </div>
    <div class="col-md-3">
        <label>Почта</label>
        <?= kartik\select2\Select2::widget([
            'name' => 'template',
            'data' => app\models\ApiSendmailUserMails::getAllMy(),
            'options' => [
                'id' => 'select-smail',
                'placeholder' => Yii::t('app', 'Select'),
            ],
        ]); ?>
    </div>
</div>

<div class="form-group">
    <?= Html::button('Отправить', ['class' => 'btn btn-primary', 'id' => 'send-mail']) ?>
</div>

<?php
$this->registerJs("
    const url = '//".Yii::$app->params['ipServerWebSocket'].":".Yii::$app->params['ipServerWebSocketPort']."/api/sendmail';
    const id_user = ".Yii::$app->user->id.";
    let content, subject, files, template,
        emails = $('#emails').val().split(',');//адреса для отправки
    
    //выбор шаблона
    $('#select-template').change(function(){
        template = $(this).val();
        $.ajax({
            type: 'POST',
            url: '".\yii\helpers\Url::to('/crm/mail/get-info-template')."',
            data: { id: $(this).val() },
            success: function (data) {
                content = data.content;
                subject = data.subject;
                files = data.files;
            },
            error: function (e) {
                console.error(e);
            }
        })
    });

    //убрать какую-то строку
    $('.del-table').click(function() {
        let root = $(this).parent().parent();
        let emails_row = root.find('.emails').attr('data-emails');
        
        if(emails_row) {//если вообще есть данные
            $.each( emails_row.split(','), function( key, value ) {//пройти по каждому
                if(emails.indexOf(value) !== -1) {//посмотреть, есть ли это в массиве
                    emails.splice(emails.indexOf(value), 1);//и удалить, раз удаляется
                }
            });
        }
        
        root.remove();//удалить строку
    });
    
    //отправка письма
    $('#send-mail').click(function(event) {
        let btn = $(this),
            semail = $('#select-smail').val();
        
        $.ajax({
            type: 'POST',
            url: url,
            data: { emails_to: JSON.stringify(emails), content: content, subject: subject, files: files, id_user: id_user, template: template, semail: semail },
            beforeSend: function () {//проверять заполненность всех полей, кроме файлов
                let errors = [];
            
                if(emails.length <= 0) {
                    errors.push('нет адресов');
                }
                
                if(template === undefined || template <= 0) {
                    errors.push('Не выбран шаблон');
                }
                
                if(content === undefined || content.length <= 0) {
                    errors.push('нет контента');
                }
                
                if(subject === undefined || subject.length <= 0) {
                    errors.push('нет темы');
                }
                
                if(!id_user) {
                    errors.push('нет пользователя');
                }
                
                if(!semail) {
                    errors.push('Выберите почту');
                }
                
                if(errors.length > 0) {
                    modelErrorShow(errors);
                    return false;
                }
                btn.prop('disabled', true);
            },
            success: function (data) {
                if(data.code >= 200) {
                    jsNotify('Успешно отправлено. Ожидайте уведомления.');
                }
                console.log(data);
            },
            error: function (e) {
                console.error(e);
            }
        }).then(function(){
            btn.prop('disabled', false);
        });
    });
", yii\web\View::POS_END)
?>