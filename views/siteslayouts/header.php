<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\sites\SitesModule;
?>
<header class="main-header">
    <img class="pull-left head-img">
    <?= Html::a('<span class="logo-mini">' . Yii::t('app/views', 'Sites module') . '</span><span class="logo-lg">' . Yii::t('app/views', 'Sites module') . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown messages-menu">
                    <?= Html::dropDownList('selected_site', SitesModule::$idSite, [], ['id' => 'selected_site', 'class' => 'form-control', 'style' => 'margin-top:8px']);
                    $this->registerJs("
                        var sel_site = {id:". SitesModule::$idSite .",url_site:''};
                    ", yii\web\View::POS_END);
                    ?>
                </li>
                
                
                <li class="dropdown messages-menu" id="notificationMessagePlace">
                    <a href="<?= Url::to(['/dialog']) ?>" class="dropdown-toggle">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success"></span>
                    </a>
                </li>
                
                <!-- уведомления -->
                <?php
                    $nn = \app\models\UnreadNotice::ApiNoticesUser(Yii::$app->user->id);
                    $countNotices = '';
                    
                    if(isset($nn['type']) && $nn['type'] == 'new') {
                        $countNotices = $nn['count_notices'];
                        $this->registerJs("
                            Tinycon.setBubble(".$countNotices.");
                        ", \yii\web\View::POS_READY);
                    }
                ?>
                <li class="dropdown notifications-menu" id="notificationPlace">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning"><?= $countNotices ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">
                            <?= \Yii::t('app/views', 'Notices')?>
                        </li>
                        <li>
                            <ul class="menu">                                    
                                <?php
                                if(isset($nn['type'])) :
                                foreach ($nn['notices'] as $notice) : ?>
                                    <li>
                                        <?= \app\modelsView\ViewUnreadNotice::newHtmlNotify($notice) ?>
                                    </li>
                                <?php endforeach;
                                endif ?>
                            </ul>
                        </li>
                        <li class="footer">
                            <?= Html::a(Yii::t('app', 'Delete All Notices'), ['/notify/delete-all-notices'], ['id' => 'notices-delete']) ?>
                        </li>
                        <li class="footer">
                            <a href="<?= Url::toRoute(['/user/unread-notice/index'])?>"><?= Yii::t('app', 'Notifications Archive')?></a>
                        </li>
                    </ul>
                </li>
                <!-- end уведомления -->
                
                <!-- все задачи.сортировка через виджет задается -->
                <li id="header-list-tasks" class="dropdown tasks-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-flag-o"></i>
                        <span class="label label-danger tasks-all-count"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">
                            <?= \Yii::t('app/models', 'Tasks')?>
                        </li>
                        <li>
                            <ul class="menu">
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="<?= Url::toRoute(['/intranet/tasks/index', 'mode' => 'task'])?>"><?= Yii::t('app', 'View all tasks')?></a>
                        </li>
                    </ul>
                </li>
                <!-- end все задачи -->
                
                <?php
                    //невыполненные поручения
                    $tasks = Yii::$app->user->identity->quests;
                    $countTasks = count($tasks);
                ?>
                <?php if($countTasks > 0) : ?>
                <li class="dropdown tasks-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-thumb-tack"></i>
                        <span class="label label-danger"><?= $countTasks?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">
                            <?= \Yii::t('app','У вас {n, plural, =0{нет поручений} =1{# поручение} other{# поручений}}!',['n' => $countTasks])?>
                        </li>
                        <li>
                            <ul class="menu">
                                <?php foreach ($tasks as $task) : //вывод поручений?>
                                <li>
                                    <?= $task->getHtmlLink()?>
                                </li>
                                <?php endforeach ?>
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="<?= Url::toRoute(['/intranet/tasks/index', 'mode' => 'quest'])?>"><?= Yii::t('app', 'View all quests')?></a>
                        </li>
                    </ul>
                </li>
                <?php endif; ?>

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= Yii::$app->user->identity->avatar ?>" class="user-image" alt="<?= Yii::$app->user->identity->username?>"/>
                        <span class="hidden-xs"><?= Yii::$app->user->identity->username?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="<?= Yii::$app->user->identity->avatar ?>" class="img-circle" alt="<?= Yii::$app->user->identity->username?>"/>
                            <p>
                                <?= Yii::$app->user->identity->username?> - <?= Yii::$app->user->identity->appointment?>
                            </p>
                        </li>
                        <li class="user-body">
                            <div class="row">
                                <div class="col-xs-6 text-center">
                                    <?= Html::a(Yii::t('app/views', 'My Notes'), ['/user/personal-notes/index'])?>
                                </div>
                            </div>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?= Url::toRoute(['/user/user/profile','id'=>  Yii::$app->user->id]) ?>" class="btn btn-default btn-flat"><?= Yii::t('app', 'Profile')?></a>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    Yii::t('app','Sign out'),
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>
                
                <li>
                    <a href="<?= Url::toRoute(['/user/user/update-profile','id'=>  Yii::$app->user->id]) ?>"><i class="fa fa-pencil"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>