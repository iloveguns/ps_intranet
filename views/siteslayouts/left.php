<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <ul class="sidebar-menu hidden linkbehindusertop">
                <li>
                    <?= yii\helpers\Html::a('<span class="logo-mini">СМГ</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
                </li>
            </ul>
            
            <div class="pull-left image">
                <?= Yii::$app->user->identity->getLinkAvatar('', ['class' => 'img-circle', 'alt' => Yii::$app->user->identity->username])?>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username?></p>
                <a href="#" id="userStatusWebN"><i class="fa fa-circle text-danger"></i><span>Offline</span></a>
            </div>
        </div>
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    [
                        'label' => Yii::t('app', 'Go to intranet'),
                        'icon' => 'fa fa-home',
                        'url' => ['/intranet/default/index'],
                    ],
                    
                    //['label' => Yii::t('app/views', 'sites Create content'), 'icon' => 'fa fa-sticky-note-o', 'url' => ['createcontent']],
                    ['label' => Yii::t('app/views', 'sites Content'), 'icon' => 'fa fa-tasks', 'url' => ['/sites/sites/viewcontent']],
                    ['label' => Yii::t('app/views', 'sites Menu'), 'icon' => 'fa fa-tasks', 'url' => ['/sites/sites/menu']],
                    ['label' => Yii::t('app/views', 'sites Text blocks'), 'icon' => 'fa fa-text-width', 'url' => ['/sites/sites/textblocks']],
                    ['label' => Yii::t('app/views', 'sites Main page'), 'icon' => 'fa fa-text-width', 'url' => ['/sites/sites/mainpageindex']],
                    ['label' => Yii::t('app/views', 'sites Stories'), 'icon' => 'fa fa-text-width', 'url' => ['/sites/sites/stories']],
                    ['label' => Yii::t('app/views', 'sites Content Authors'), 'icon' => 'fa fa-text-width', 'url' => ['/sites/sites/contentauthors']],
                    ['label' => Yii::t('app/views', 'Sites Roles'), 'icon' => 'fa fa-address-book', 'url' => ['/sites/sites-roles/index'], 'visible' => Yii::$app->user->identity->isAdmin()],
                    ['label' => Yii::t('app/views', 'Sites Access Settings'), 'icon' => 'fa fa-users', 'url' => ['/sites/sites-access-settings/index'], 'visible' => \app\modules\sites\SitesModule::canAccess()],
                    ['label' => Yii::t('app/views', 'sites Banners'), 'icon' => 'fa fa-text-width', 'url' => ['banners']],
                    
                    [
                        'label' => Yii::t('app', 'Scroll to top'),
                        'icon' => 'fa fa-arrow-up',
                        'url' => '#totop',
                    ],
                ],
            ]
        ) ?>
    </section>
</aside>
