<?php
$this->title = Yii::t('app', 'Registration');
use app\modules\user\models\User;
use yii\widgets\ActiveForm;
use app\modules\user\UserModule;
use yii\helpers\Html;
use kartik\date\DatePicker;
use app\modules\user\models\Organization;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use app\modules\user\models\GeneralRoles;
use app\modules\user\models\Appointments;
use kartik\datecontrol\DateControl;

UserModule::registerTranslations();
?>
<div class="user-form">
    <?php if(isset($_GET['success'])) : ?>
        <div class="alert alert-success" role="alert" style="margin-top: 50px;">
            Вы отправили свои данные админу. Ждите подтверждения
        </div>    
    <?php else : ?>
    
    <h1><?=  Yii::t('app', 'Registration')?></h1>
    
    <?php $formarr = ['id' => 'user-register']?>
    <?php $form = ActiveForm::begin($formarr); ?>
    
    <?= $form->field($model, 'lastname', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'name', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'secondname', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->textInput(['maxlength' => true]) ?>

    <?php /* $form->field($model, 'birthday', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->widget(DatePicker::classname(), [
        'options' => ['placeholder' => Yii::t('app','Enter date')],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd'
        ]
    ]); */?>
    
    <?= $form->field($model, 'birthday', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->textInput() ?>

    <?= $form->field($model, 'sex', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->dropDownList(User::getSex()) ?>

    <?= $form->field($model, 'email', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->textInput() ?>

    <?= $form->field($model, 'password', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->textInput() ?>
    
    <?= $form->field($model, 'fk_appointment', ['options' => ['class' => 'col-lg-4 col-md-6 col-sm-12']])->dropDownList(Appointments::getAll()) ?>
    
    <div class="clearfix"></div>
    <?= $form->field($model, 'fk_organization')->dropDownList(Organization::getAll(),['id' => 'fk_organization']) ?>
    
<?php 
echo $form->field($model, 'fk_divizion')->widget(DepDrop::classname(), [
    'options'=>['id'=>'fk_divizion'],
    'pluginOptions'=>[
        'depends'=>['fk_organization'],
        'placeholder'=>Yii::t('app', 'Select'),
        'url'=>Url::to(['/user/user/dependency-divizion'])
    ]
]);
?>    
<?php 
echo $form->field($model, 'fk_department')->widget(DepDrop::classname(), [
    'options'=>['id'=>'fk_department'],
    'pluginOptions'=>[
        'depends'=>['fk_divizion'],
        'placeholder'=>Yii::t('app', 'Select'),
        'url'=>Url::to(['/user/user/dependency-department'])
    ]
]);
?>
<?php 
$this->registerJs(<<<JS
$('#user-register').on('beforeSubmit', function(){
    if($("#fk_department").val() === null){
        $(".field-user-fk_department").addClass('has-error');
        return false;
    }
    else{
        $(".field-user-fk_department").removeClass('has-error');
        return true;
    }
});
$('#userregister-birthday-disp').addClass('form-control');
$('#userregister-birthday').mask('9999-99-99',{placeholder:"ГГГГ-ММ-ДД"});
JS
);?>
    
    <div class="clearfix"></div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Register Me'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>
    <?php ActiveForm::end(); ?>
    <?php endif ?>
</div>