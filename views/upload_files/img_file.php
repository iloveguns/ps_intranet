<?php
use yii\helpers\Html;

$id_file = $file->id_file;
if($del_button) $options['class'] .= ' file-view-' . $file->id_file;
?>

<?php if(exif_imagetype(Yii::getAlias('@webroot') . $file->path)) : ?>
    <span class="file-view-<?= $id_file ?>">    
        <?= Html::img(\app\helpers\Image::thumb($file->path, 150, 150), $options) ?>
        <i class="fa fa-times text-red fdel-btn delete-files" aria-hidden="true" idf="<?= $id_file ?>"></i>
    </span>
<?php endif ?>

<?php
$this->render('script');
$this->registerCss(' .fdel-btn{ position: relative; top: -80px;left: -25px; }');
?>
