<?php
use yii\helpers\Html;

$this->render('script');
$id_file = $file->id_file;
?>
<div class="file-view-<?= $id_file ?>">
    <?php 
    echo Html::a($file->name, ['/file/upload', 'id_file' => $id_file], ['class' => 'btn btn-default', 'target' => '_blank']);
    if($del_button) {
        echo Html::button(Yii::t('app', 'Delete'), [
            'class' => 'btn btn-danger delete-files',
            'idf' => $id_file,
        ]);
    }
    ?>
</div>
