<?php
//удаление файлов
$this->registerJs("
    $('.delete-files').on('click', function() {
        if(!confirm('Точно удалить?')) return;
        let id_file = $(this).attr('idf');
        $.ajax({
            method: 'GET',
            url: '" . \yii\helpers\Url::to(['/file/delete-by-id']) . "?id_file=' + id_file,
            success: function(data) {
                if(data.success == true){
                    $('.file-view-' + id_file).remove();
                    jsNotify('Файл удален');
                } else {
                    modelErrorShow(data.errors);
                }
            }
        });
    });
", yii\web\View::POS_END, 'del-file');
