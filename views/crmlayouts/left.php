<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <ul class="sidebar-menu hidden linkbehindusertop">
                <li>
                    <?= yii\helpers\Html::a('<span class="logo-mini">СМГ</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
                </li>
            </ul>
            
            <div class="pull-left image">
                <?= Yii::$app->user->identity->getLinkAvatar('', ['class' => 'img-circle', 'alt' => Yii::$app->user->identity->username])?>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username?></p>
                <a href="#" id="userStatusWebN"><i class="fa fa-circle text-danger"></i><span>Offline</span></a>
            </div>
        </div>
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    [
                        'label' => Yii::t('app', 'Go to intranet'),
                        'icon' => 'fa fa-home',
                        'url' => ['/intranet/default/index'],
                    ],
                    [
                        'label' => Yii::t('app/models', 'Crm module'),
                        'icon' => 'fa fa-home',
                        'url' => ['/crm/default/index'],
                        'class' => 'active',
                        'items' => [
                            ['label' => Yii::t('app/views', 'Crm Analytics'), 'icon' => 'fa fa-pie-chart', 'url' => ['/crm/crm-analytics/index'],],
                            ['label' => Yii::t('app/views', 'Crm Client Tasks'), 'icon' => 'fa fa-tasks', 'url' => ['/crm/crm-tasks/todoline'],],
                            ['label' => Yii::t('app/views', 'Crm Companies'), 'icon' => 'fa fa-building-o', 'url' => ['/crm/crm-company/index'],],
                            ['label' => Yii::t('app/views', 'Crm Contacts'), 'icon' => 'fa fa-user', 'url' => ['/crm/crm-contact/index']],
                            ['label' => Yii::t('app/models', 'Crm Deals'), 'icon' => 'fa fa-briefcase', 'url' => ['/crm/crm-deals/index'],],
                            ['label' => Yii::t('app/views', 'Crm clients birthdays'), 'icon' => 'fa fa-birthday-cake', 'url' => ['/crm/default/birthdays'],],
                            ['label' => Yii::t('app/views', 'Crm Type Task'), 'icon' => 'fa fa-sticky-note-o', 'url' => ['/crm/crm-typetask/index'], 'visible' => \app\modules\crm\CrmModule::canAdminDep()],
                            ['label' => Yii::t('app/models', 'Crm Types Deal'), 'icon' => 'fa fa-sticky-note-o', 'url' => ['/crm/crm-types-deal/index'],],
                            ['label' => Yii::t('app/models', 'Crm Tasks Results'), 'icon' => 'fa fa-server', 'url' => ['/crm/crm-tasks-results/index'],],
                            ['label' => Yii::t('app/models', 'Crm Deals Projects'), 'icon' => 'fa fa fa-bars', 'url' => ['/crm/crm-deals-projects/index'],],
                            ['label' => Yii::t('app/models', 'Crm Faqs'), 'icon' => 'fa fa-question', 'url' => ['/crm/crm-faq/index'],],
                            ['label' => Yii::t('app/models', 'Crm bugtracker'), 'icon' => 'fa fa-bug', 'url' => ['/crm/crm-bugtracker/index'],],
                            ['label' => Yii::t('app/models', 'Crm Roles'), 'icon' => 'fa fa-address-book', 'url' => ['/crm/crm-roles/index'], 'visible' => Yii::$app->user->identity->isAdmin()],
                            ['label' => Yii::t('app/models', 'Crm User Settings'), 'icon' => 'fa fa-users', 'url' => ['/crm/crm-access-settings/index'], 'visible' => \app\modules\crm\CrmModule::canAdminDep()],
                            ['label' => Yii::t('app/models', 'Crm Clients of Fired'), 'icon' => 'fa fa-users', 'url' => ['/crm/crm-fired-clients/index'], 'visible' => app\modules\crm\CrmModule::haveFiredClients()],
                        ],
                    ]
                ],
            ]
        ) ?>
    </section>
</aside>
