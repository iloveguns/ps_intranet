<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;
?>
<div class="content-wrapper" id="primary-content">
    <section class="content-header">
        <?php if(Yii::$app->params['crm_notification_header_for_all'] != 'null') : //глобальное сообщение?>
            <div class="callout callout-info">
                <h4><?= Yii::t('app', 'Tip From System')?></h4>
                <div><?= Yii::$app->params['crm_notification_header_for_all'] ?></div>
            </div>        
        <?php endif ?>
        
        <?php if (isset($this->blocks['content-header'])) : ?>
            <h1><?= $this->blocks['content-header'] ?></h1>
        <?php endif ?>

        <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?>
    </section>

    <section class="content">
        <div class="alert alert-warning hidden" role="alert" id="warning-alert-notify"></div>
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    <strong>Version 2.1(b)</strong>
</footer>