<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\spinner\Spinner;

//заебал, для массового создания задач
use kartik\datecontrol\DateControl;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

if(Yii::$app->user->identity->fkStatus->status == app\modules\user\models\UserStatus::FIRED) {
    Yii::$app->user->logout();//разлогинить
}

//без аватара нельзя
if(!Yii::$app->user->identity->photo) {
    if((Yii::$app->controller->id == 'user' && Yii::$app->controller->action->id == 'update-profile')) {}
    else {
        Yii::$app->response->redirect(['/user/user/update-profile', 'id' => Yii::$app->user->id, 'required' => true]);
    }
}

kartik\growl\GrowlAsset::register($this);//bootstrap growl только через kartik extensions
kartik\base\AnimateAsset::register($this);//для анимации growl
dmstr\web\AdminLteAsset::register($this);
app\assets\AppAsset::register($this);
app\assets\IoAsset::register($this);

//очень важные константы
$this->registerJs("
    window.uniqid = ".Yii::$app->user->id.";
    var ioconnect = 'http://".Yii::$app->params['ipServerWebSocket'].":".Yii::$app->params['ipServerWebSocketPort']."/',
        apiurl = 'http://".Yii::$app->params['ipServerWebSocket'].":".Yii::$app->params['ipServerApiPort']."/',
        getcommenturl = '".Url::to(['/comment/get-comment'])."',
        getupdatenotifyurl = '".Url::to(['/notify/get-list'])."',
        getupdatemessagesurl = '".Url::to(['/messageall/load-messages'])."',
        messageloadcount = ".Yii::$app->params['messagesLoadCount'].",
        getstructureurl = '".Url::toRoute('/user/units/structure-widget')."',
        tasks_api_sort = '" . Url::to(['/intranet/tasks/get-sort-data-tasks']) . "',//url для получения списка задач по сортировке
        tasks_m_late = '" . Yii::t('app/models', 'Task status latebytime') . "',
        tasks_api_list = '" . Url::to(['/intranet/tasks/api-list-data']) . "';//url для получения списка задач
    
    var msgchoosestructureunits = '".Yii::t('app/models', 'Choose structure units')."';
", \yii\web\View::POS_BEGIN);//новый сокет, необходим id = pk_user

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => '/faviconcrm.ico'])
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue-light sidebar-mini"><?php //skin-christmas?>
    <div style="display: none"><?=DateControl::widget([
    'name'=>'kartik-date-3', 
    'value'=>date('H:i:s'),
    'type'=>DateControl::FORMAT_DATE,
]);?>
</div>
    <div style="position: relative">
        <div class="center-over-all hidden">
            <?= Spinner::widget([//гиф лоадер на весь экран
                'preset' => Spinner::LARGE,
                'align' => 'center',
            ])?>
        </div>
<?php $this->beginBody() ?>
<div class="wrapper">
    <?= $this->render(
        'header.php',
        ['directoryAsset' => $directoryAsset]
    ) ?>

    <?= $this->render(
        'left.php',
        ['directoryAsset' => $directoryAsset]
    )
    ?>

    <?= $this->render(
        'content.php',
        ['content' => $content, 'directoryAsset' => $directoryAsset]
    ) ?>
</div>
<?php
//бутстрапное модальное окно на всякий случай
Modal::begin([
    //'header' => 'asd',
    'id' => 'myModal',
    'size' => 'modal-lg',
]);
Modal::end();

$this->endBody();
?>

<?php
//список сотрудников текущего отдела
Modal::begin([
    'options' => [
        'id' => 'crm-users-list-modal',
    ],
    'header' => '<h4>'.Yii::t('app/views', 'Select user for transfer him').'</h4>',
]);
echo '<div class="form-group">';
echo kartik\select2\Select2::widget([
    'name' => 'crm-users-list',
    'data' => \app\modules\crm\CrmModule::getAllUsersCrm(),
    'options' => ['placeholder' => Yii::t('app', 'Select')],
    'pluginOptions' => [
        'allowClear' => true
    ],
]);
echo '</div>';
echo Html::button(Yii::t('app', 'Save'), ['class' => 'btn btn-primary mt-1em']);
Modal::end();
//список сотрудников текущего отдела

//список тегов текущего отдела
Modal::begin([
    'options' => [
        'id' => 'crm-tags-list-modal',
    ],
    'header' => '<h4>'.Yii::t('app/views', 'Select tags for change').'</h4>',
]);
echo '<div class="form-group">';
echo kartik\select2\Select2::widget([
    'name' => 'crm-tags-list',
    'data' => \app\modules\crm\models\CrmTags::getAll(),
    'options' => ['placeholder' => Yii::t('app', 'Select'), 'multiple' => true],
    'pluginOptions' => [
        'tags' => true,
        'maximumInputLength' => 30
    ],
]);
echo '</div>';
echo Html::button(Yii::t('app', 'Save'), ['class' => 'btn btn-primary mt-1em']);
Modal::end();
//список тегов текущего отдела

//создание задачи
Modal::begin([
    'options' => [
        'id' => 'crm-create-task-modal',
    ],
    'header' => '<h4>Создание задачи</h4>',
]);

$model = new app\modules\crm\models\CrmTasks();
$model->date_task = \app\models\FunctionModel::getDateWParam('Y-m-d');
?>

<form id="crm-create-task-modal-form">
    <div class="row">
        <div class="col-md-6">
            <label class="control-label">Дата задачи</label>
            <?= DateControl::widget([
                'options' => [
                    'id' => 'date_task',
                ],
                'name'=>'date_task', 
                'value'=>\app\models\FunctionModel::getDateWParam('Y-m-d'),
                'type'=>DateControl::FORMAT_DATE,
            ]); ?>
        </div>
        <div class="col-md-6">
            <label class="control-label">Время</label>
            <?= Html::dropDownList('time_task', '', \app\models\FunctionModel::getTimes(true, NULL, false), ['class' => 'form-control']) ?>
        </div>
    </div>

    <label class="control-label">Сотрудник</label>
    <?= Select2::widget([
        'name' => 'id_user',
        'data' => \app\modules\crm\CrmModule::getAllUsersCrm(),
        'value' => Yii::$app->user->id,
        'options' => [
            'id' => 'id_user',
            'placeholder' => ['placeholder' => Yii::t('app', 'Select')]
        ],
    ]);?>

    <label class="control-label">Тип задачи</label>
    <?= Select2::widget([
        'name' => 'type_task',
        'data' => \app\modules\crm\models\CrmTypetask::getAll(),
        'options' => ['placeholder' => Yii::t('app', 'Select'), 'id' => 'type_task'],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]);?>

    <label class="control-label">Комментарий</label>
    <?= Html::textarea('comment', '', ['class' => 'form-control', 'rows' => 3]) ?>
    <br>
    <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success', 'id' => 'crm-create-task-modal-submit'])?>
</form>
<?php Modal::end();
$this->registerJs("
    $('#crm-create-task-modal-form').submit(function(){
        let data = $(this).serializeAssoc();
        data.ids = window.act_data.join(',')
        data.type_client = window.type_client;
        
        $.ajax({
            url: window.act_url,
            type: 'post',
            data: data,
            beforeSend: function() {
                if(data.length == 0 || !confirm(window.act_cmess)){
                    return false;
                }
                gifLoader();
            },
            success: function(data) {
                if(data.success === true) {
                    jsNotify('Сохранено');
                } else {                    
                    modelErrorShow(data.errors);
                }
                $('#crm-create-task-modal').modal('hide');
                gifLoader();
            }
        });
        
        return false;
    });", yii\web\View::POS_READY);
//создание задачи

$this->registerJsFile('@web/js/build/crmscripts.js',  ['position' => yii\web\View::POS_END]);
?>
</div>
<!-- для tinymce -->
<input id="tinymce-file-upload-all" type="file" name="tinymce-file-upload-all" style="display: none;" accept="image/*" />
<input id="tinymce-media-upload-all" type="file" name="tinymce-media-upload-all" style="display: none;" accept="video/*" />
</body>
</html>
<?php $this->endPage() ?>