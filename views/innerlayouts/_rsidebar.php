<?php
use app\components\WidgetFIlesUpload;
use app\components\WidgetRecentViews;
use app\components\WidgetSubscribe;
?>
<?php if(!$model->isNewRecord) : //если новая запись, то ajax загрузка невозможна - не к чему прикрепить ?>
    <?= WidgetFIlesUpload::widget(['model' => $model->getFiles(), 'id' => $model->getPrimaryKey(), 'name' => 'callboard', 'updateUrl' => '/intranet/'.$model->tableName().'/view', 'modelClass' => $model->getName(), 'uploadCondition' => true]) ?>
    
    <?php if($model->getName() == app\modules\intranet\models\Tasks::getName()) : //создание событий для ЗАДАЧ ?>
        <div class="panel panel-default widget-files-upload">
            <div class="panel-heading"><?= Yii::t('app/views', 'Events created')?></div>
            <div class="panel-body text-left">
                <?php
                if($models = \app\modules\intranet\models\Events::findAll(['class' => $model->getName(), 'item_id' => $model->getPrimaryKey()])) {
                    foreach ($models as $m) {
                        echo yii\helpers\Html::a($m->title, 
                            ['/intranet/events/view', 'id' => $m->pk_event],
                            ['class' => 'btn btn-primary', 'target' => '_blank']);
                    }
                    echo '<hr>';
                }
                ?>
                <?php if($model->id_author === Yii::$app->user->id) {
                    echo yii\helpers\Html::a(Yii::t('app/views', 'Add event create'), 
                        ['/intranet/events/createget', 'class' => $model->getName(), 'item_id' => $model->getPrimaryKey(), 'users' => implode(',', app\modules\user\models\Units::onlyIdUsers($model, true, true)), 'title' => $model->title],
                        ['class' => 'btn btn-primary', 'target' => '_blank']);
                } ?>
            </div>
        </div>
    <?php endif ?>

    <?= WidgetRecentViews::widget(['className' => $model->getName(), 'item_id' => $model->getPrimaryKey()]);?>

    <?php if($model->getName() == app\modules\intranet\models\Callboard::getName()) : //кнопка подписки ?>
        <?= WidgetSubscribe::widget(['model' => $model]);?>
    <?php endif ?>
<?php endif ?>