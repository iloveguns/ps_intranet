<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <ul class="sidebar-menu hidden linkbehindusertop">
                <li>
                    <?= yii\helpers\Html::a('<span class="logo-mini">СМГ</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
                </li>
            </ul>
            
            <div class="pull-left image">
                <?= Yii::$app->user->identity->getLinkAvatar('', ['class' => 'img-circle', 'alt' => Yii::$app->user->identity->username])?>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username?></p>
                <a href="#" id="userStatusWebN"><i class="fa fa-circle text-danger"></i><span>Offline</span></a>
            </div>
        </div>
        <!--<form action="<?= yii\helpers\Url::toRoute(['/intranet/default/search'])?>" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="<?= Yii::t('app', 'Search...')?>"/>
                <span class="input-group-btn">
                  <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>-->
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    [
                        'label' => Yii::t('app', 'Create'),
                        'icon' => 'fa fa-home',
                        'url' => ['/intranet/'],
                        'items' => [
                            ['label' => Yii::t('app/models', 'One Callboard'), 'icon' => 'fa fa-sticky-note-o', 'url' => ['/intranet/callboard/create'],],
                            ['label' => Yii::t('app/models', 'Task'), 'icon' => 'fa fa-tasks', 'url' => ['/intranet/tasks/create'],],
                            ['label' => Yii::t('app/models', 'One Event'), 'icon' => 'fa fa-calendar', 'url' => ['/intranet/events/create'],],                            
                            ['label' => Yii::t('app/models', 'Project'), 'icon' => 'fa fa-bars', 'url' => ['/intranet/projects/create'],],
                            ['label' => Yii::t('app/models', 'Create Blog'), 'icon' => 'fa fa-copyright', 'url' => ['/intranet/blogs/create'],],
                        ],
                    ],
                    [
                        'label' => Yii::t('app/models', 'Intranet module'),
                        'icon' => 'fa fa-home',
                        'url' => ['/intranet/default'],
                        'items' => [
                            ['label' => Yii::t('app/models', 'Callboard'), 'icon' => 'fa fa-sticky-note-o', 'url' => ['/intranet/callboard/index'],],
                            ['label' => Yii::t('app/models', 'Tasks'), 'icon' => 'fa fa-tasks', 'url' => ['/intranet/tasks/index'],],
                            ['label' => Yii::t('app/models', 'Events'), 'icon' => 'fa fa-calendar-o', 'url' => ['/intranet/events/index'],],
                            ['label' => Yii::t('app/models', 'Projects'), 'icon' => 'fa fa-bars', 'url' => ['/intranet/projects/index'],],
                            ['label' => Yii::t('app/views', 'Project Directions'), 'icon' => 'fa fa-bars', 'url' => ['/intranet/project-directions/index'],],
                            ['label' => Yii::t('app/models', 'Messages'), 'icon' => 'fa fa-envelope-o', 'url' => ['/dialog'],],
                            ['label' => Yii::t('app/models', 'My Files'), 'icon' => 'fa fa-file-archive-o', 'url' => ['/intranet/file-storage/index'],],
                            ['label' => Yii::t('app/models', 'Events Company'), 'icon' => 'fa fa-calendar', 'url' => ['/intranet/events-company/index'],],
                            ['label' => Yii::t('app/models', 'Schedule Conference Hall'), 'icon' => 'fa fa-calendar', 'url' => ['/intranet/conference-hall/index'],],
                            ['label' => Yii::t('app/models', 'Radio schedule'), 'icon' => 'fa fa-music', 'url' => ['/intranet/radio-schedule/index']],
                            ['label' => Yii::t('app/models', 'Drivers schedule'), 'icon' => 'fa fa-ambulance', 'url' => ['/intranet/driver-schedule/index'], 'visible' => (in_array(Yii::$app->user->identity->fkDivizion->pk_divizion, [1,2,3,10]) || Yii::$app->user->identity->isAdmin())],
                            ['label' => Yii::t('app/views', 'Analytics Center'), 'icon' => 'fa fa-calendar', 'url' => ['/intranet/analcenter-hall/index']],//, 'visible' => Yii::$app->user->identity->can('anal_center')
                            ['label' => Yii::t('app/models', 'Calendar Director'), 'icon' => 'fa fa-calendar', 'url' => ['/intranet/head-calendar/index'],],
                            ['label' => Yii::t('app/models', 'Bug Tracker'), 'icon' => 'fa fa-bug', 'url' => ['/intranet/bugtracker/index'],],
                            ['label' => Yii::t('app/models', 'Map of cabinets'), 'icon' => 'fa fa-street-view', 'url' => ['/user/units/plan'],],
                            ['label' => Yii::t('app/models', 'Inter map'), 'icon' => 'fa fa-globe', 'url' => ['/user/units/map'],],
                            ['label' => Yii::t('app/models', 'Blogs'), 'icon' => 'fa fa-copyright', 'url' => ['/intranet/blogs/index'],],
                            ['label' => Yii::t('app/models', 'Holidays'), 'icon' => 'fa fa-gift', 'url' => ['/admins/holidays/index'], 'visible' => Yii::$app->user->identity->can('tousers')],
                            ['label' => Yii::t('app/models', 'System Admin Query'), 'icon' => 'fa fa-wrench', 'url' => ['/intranet/sysadmquery/index'], 'visible' => app\modules\intranet\controllers\SysadmqueryController::enterCondition()],
                            ['label' => Yii::t('app/models', 'Steward Query'), 'icon' => 'fa fa-flask', 'url' => ['/intranet/stewardquery/index'], 'visible' => \app\modules\intranet\controllers\StewardqueryController::enterCondition()],
                            ['label' => Yii::t('app/views', 'Dbpartners'), 'icon' => 'fa fa-calendar', 'url' => ['/intranet/dbpartners/index'], 'visible' => Yii::$app->user->identity->can('dbpartners')],
                            ['label' => 'Внутренние документы организации', 'icon' => 'fa fa-file-text-o', 'url' => ['/intranet/blogs/view/14'],],
                        ],
                    ],
                    [
                        'label' => Yii::t('app/models', 'Admin module'),
                        'icon' => 'fa fa-user-secret',
                        'url' => ['/admins/default'],
                        'visible' => Yii::$app->user->identity->isAdmin() || Yii::$app->user->identity->can('tousers'),
                        'items' => [
                            ['label' => Yii::t('app/models', 'Techniques'), 'icon' => 'fa fa-rocket', 'url' => ['/intranet/technique/index'], 'visible' => Yii::$app->user->identity->isAdmin()],
                            ['label' => 'drags', 'icon' => 'fa fa-file-code-o', 'url' => ['/intranet/graphrelate/index'], 'visible' => Yii::$app->user->identity->isAdmin()],
                            ['label' => Yii::t('app/models', 'Logs list'), 'icon' => 'fa fa-file-code-o', 'url' => ['/admins/login-form/index'], 'visible' => Yii::$app->user->identity->isAdmin()],
                            ['label' => Yii::t('app/models', 'System'), 'icon' => 'fa fa-wrench', 'url' => ['/admins/system/index'], 'visible' => Yii::$app->user->identity->isAdmin()],
                            ['label' => Yii::t('app/models', 'Settings'), 'icon' => 'fa fa-steam', 'url' => ['/admins/settings/index'], 'visible' => Yii::$app->user->identity->isAdmin()],
                        ],
                    ],
                    [
                        'label' => Yii::t('app/models', 'Users module'),
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => Yii::t('app/models', 'Users list'), 'icon' => 'fa fa-id-card-o', 'url' => ['/user/user/index']],
                            ['label' => Yii::t('app/models', 'Units list'), 'icon' => 'fa fa-dashboard', 'url' => ['/user/units/index', 'show' => 'active']],
                            ['label' => Yii::t('app/models', 'Appointments list'), 'icon' => 'fa fa-dashboard', 'url' => ['/user/appointments/index'], 'visible' => Yii::$app->user->identity->can('tousers')],
                            ['label' => Yii::t('app/models', 'Roles list'), 'icon' => 'fa fa-dashboard', 'url' => ['/user/general-roles/index'], 'visible' => Yii::$app->user->identity->isAdmin()],
                            ['label' => Yii::t('app/models', 'Register Workers'), 'icon' => 'fa fa-user-plus', 'url' => ['/user/user/registers-workers'], 'visible' => Yii::$app->user->identity->isAdmin()],
                        ],
                    ],
                    [
                        'label' => Yii::t('app/views', 'Pollings'),
                        'icon' => 'fa fa-arrow-left',
                        'url' => ['/polling/polling/index'],
                    ],
                    [
                        'label' => Yii::t('app/views', 'Sites module'),
                        'icon' => 'fa fa-arrow-left',
                        'url' => ['/sites/sites/index'],
                        'visible' => \app\modules\sites\SitesModule::haveAccess(),
                    ],
                    [
                        'label' => Yii::t('app', 'Go to crm'),
                        'icon' => 'fa fa-arrow-left',
                        'url' => ['/crm/default/index'],
                        'visible' => app\modules\crm\CrmModule::haveAccess()
                    ],
                    [
                        'label' => Yii::t('app', 'Scroll to top'),
                        'icon' => 'fa fa-arrow-up',
                        'url' => '#totop',
                    ],
                ],
            ]
        ) ?>
    </section>
</aside>
