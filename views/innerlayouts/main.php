<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\spinner\Spinner;

if(Yii::$app->user->identity->fkStatus->status == app\modules\user\models\UserStatus::FIRED) {
    Yii::$app->user->logout();//разлогинить
}

//без аватара нельзя
if(!Yii::$app->user->identity->photo) {
    if((Yii::$app->controller->id == 'user' && Yii::$app->controller->action->id == 'update-profile')) {}
    else {
        Yii::$app->response->redirect(['/user/user/update-profile', 'id' => Yii::$app->user->id, 'required' => true]);
    }
}

if (class_exists('backend\assets\AppAsset')) {//assets
    backend\assets\AppAsset::register($this);
}

kartik\growl\GrowlAsset::register($this);//bootstrap growl только через kartik extensions
kartik\base\AnimateAsset::register($this);//для анимации growl
dmstr\web\AdminLteAsset::register($this);
app\assets\IoAsset::register($this);

//очень важные константы
$this->registerJs("
    window.uniqid = ".Yii::$app->user->id.";
    var ioconnect = 'http://".Yii::$app->params['ipServerWebSocket'].":".Yii::$app->params['ipServerWebSocketPort']."/',
        apiurl = 'http://".Yii::$app->params['ipServerWebSocket'].":".Yii::$app->params['ipServerApiPort']."/',
        getcommenturl = '".Url::to(['/comment/get-comment'])."',
        getupdatenotifyurl = '".Url::to(['/notify/get-list'])."',
        getupdatemessagesurl = '".Url::to(['/messageall/load-messages'])."',
        messageloadcount = ".Yii::$app->params['messagesLoadCount'].",
        getstructureurl = '".Url::toRoute('/user/units/structure-widget')."',
        tasks_api_sort = '" . Url::to(['/intranet/tasks/get-sort-data-tasks']) . "',//url для получения списка задач по сортировке
        tasks_m_late = '" . Yii::t('app/models', 'Task status latebytime') . "',
        tasks_api_list = '" . Url::to(['/intranet/tasks/api-list-data']) . "';//url для получения списка задач
        
    var msgchoosestructureunits = '".Yii::t('app/models', 'Choose structure units')."';
", \yii\web\View::POS_BEGIN);//новый сокет, необходим id = pk_user

app\assets\AppAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue-light sidebar-mini"><?php //skin-christmas?>
    <div style="position: relative">
        <div class="center-over-all hidden">
            <?= Spinner::widget([//гиф лоадер на весь экран
                'preset' => Spinner::LARGE,
                'align' => 'center',
            ])?>

        </div>
<?php $this->beginBody() ?>
<div class="wrapper">
    <?= $this->render(
        'header.php',
        ['directoryAsset' => $directoryAsset]
    ) ?>

    <?= $this->render(
        'left.php',
        ['directoryAsset' => $directoryAsset]
    ) ?>

    <?= $this->render(
        'content.php',
        ['content' => $content, 'directoryAsset' => $directoryAsset]
    ) ?>
</div>
<?php
//бутстрапное модальное окно на всякий случай
Modal::begin([
    'header' => '<h3></h3>',
    'id' => 'myModal',
    'size' => 'modal-lg',
]);
Modal::end();

$this->endBody();
?>
</div>
<!-- для tinymce -->
<input id="tinymce-file-upload-all" type="file" name="tinymce-file-upload-all" style="display: none;" accept="image/*" />
<input id="tinymce-media-upload-all" type="file" name="tinymce-media-upload-all" style="display: none;" accept="video/*" />
</body>
</html>
<?php $this->endPage() ?>