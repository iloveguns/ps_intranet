<?php 
//$imsender -bool - true если рендер для отправителя, для получателя - false
$classread = '';//прочитано ли сообщение
if($imsender){
    $classread = ($message->readed >= $message::READED_RECEIVER) ? '' : 'unreadReceiver';//непрочитано получателем
}else{
    $classread = ($message->readed >= $message::READED_RECEIVER) ? '' : 'unread';
}

//замена спецстрок в сообщении
if(strripos($message->text_message, '[note')) {    //перенеси В МОДЕЛЬ afterFind()
    for($i = 0; $i <= substr_count($message->text_message, '[note'); $i++){
        $id_note = (int)substr($message->text_message, strripos($message->text_message, 'note')+4, 10);//+4 = note не брать в учет
        $note = \app\modules\user\models\PersonalNotes::findOne($id_note);
        
        if($note){//если найдена заметка. может быть удалена
            $message->text_message = str_replace('[note'.$id_note.']', '<div class="box box-success"><div class="box-header with-border">'. $note->title_pnote .'</div><div class="box-body padding-10">'. $note->text_pnote .'</div></div>', $message->text_message);
        } else {//если не найдена
            $message->text_message = str_replace('[note'.$id_note.']', '<div class="box box-danger"><div class="box-header with-border">' . Yii::t('app/models', 'Note Deleted') . '</div></div>', $message->text_message);
        }
    }
}
?>
<div class="direct-chat-msg <?= ($imsender) ? '' : 'right'//свои сообщения слева?> <?=$classread?>" id="message-<?=$message->pk_message?>" data-id="<?=$message->pk_message?>">
    <div class="direct-chat-info clearfix">
        <span class="direct-chat-name pull-<?= ($imsender) ? 'left' : 'right'?>">
            <?= $message->senderUser->fullname?>
        </span>
        <span class="direct-chat-timestamp pull-<?= ($imsender) ? 'left' : 'right'?>"><?= Yii::$app->formatter->asDatetime($message->date)?></span>
    </div>
    <img class="direct-chat-img" src="<?= $message->senderUser->avatar?>" alt="message user image">
    <div class="direct-chat-text pull-<?= ($imsender) ? 'left' : 'right'?>">
        <?= $message->text_message?>
    </div>
</div>