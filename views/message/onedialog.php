<?php
if($message->receiver == Yii::$app->user->id){
    $dialogUser = $message->senderUser;
}
else{
    $dialogUser = $message->receiverUser;
}
?>

<li class="item <?= (Yii::$app->request->get('id') == $dialogUser->id_user) ? 'activehover' : ''?>" data-iduser="<?= $dialogUser->id_user?>">
    <div class="product-img">
        <?= $dialogUser->getLinkAvatar('', ['class' => 'img-circle', 'style' => 'margin-left: 5px;'])?>
        <?php if(isset($dialogUser->onlineData->online)) : ?>
            <?php if($dialogUser->onlineData->online) : //онлайн пометка?>
                <span data-toggle="tooltip" title="" class="badge bg-green has-online" data-original-title="Online">&nbsp;</span>
            <?php endif ?>
        <?php endif ?>
    </div>
    <div class="product-info">
        <i class="fa fa-times delete-dialog pull-right text-red hide" data-toggle="tooltip" data-original-title="<?= Yii::t('app', 'Remove Dialog')?>" data-delete-dialog="<?= $dialogUser->id_user?>" aria-hidden="true" ></i>
        <a href="javascript:void(0)" class="product-title">
            <span class="name_user"><?= $dialogUser->fullName?></span>
            <span class="label label-info count-message-<?= $dialogUser->id_user?> pull-right"><?=$message->getCountUnreadMessages($dialogUser->id_user)?></span>
        </a>
        <span class="product-description">
            <?= $message->miniText?>
        </span>
    </div>
</li>