<?php //рендер кол-ва непрочит сообщений в верхнее меню
use yii\helpers\Url;
date_default_timezone_set(Yii::$app->params['timeZone']);
$t = time() - strtotime($message->date);//разница времени в секундах
?>
<a href="<?= Url::to(['/intranet/messages','id' => $message->senderUser->id_user])?>">
    <div class="pull-left">
        <img src="<?= $message->senderUser->avatar?>" class="img-circle"/>
    </div>
    <h4>
        <?= $message->senderUser->lastname?> <?= $message->senderUser->name?>
        <small><i class="fa fa-clock-o"></i>
            <?= kartik\helpers\Enum::timeInterval($t)?>
        </small>
    </h4>
    <p><?= $message->miniText?></p>
</a>