"use strict";

const TelegramBot = require('node-telegram-bot-api');

const token = '216887227:AAFsZxLTQxYpU0AeduIeOJu8g7dkMphxbr8';

const bot = new TelegramBot(token, {polling: true});

const EventEmitter = require('events');

let eventEmitter = new EventEmitter();


const mysql = require('./db/mysql');

//botfather(https://web.telegram.org/#/im?p=@BotFather) -> edit commands -> get_my_id - получить ваш ид в телеграме
//https://core.telegram.org/bots/api#sendmessage - api doc
/** bot.onText(/\/get_my_id/, (msg, match) => {//отдать id написавшего
    const chatId = msg.chat.id;
    bot.sendMessage(chatId, '<b>Ваш id</b>: ' + chatId, { parse_mode : 'HTML' });
    //bot.sendPhoto(chatId, '1.png');
});*/

/**
 * получение ид в телеграме
 */
eventEmitter.on('get_id', (chatId) => {
    bot.sendMessage(chatId, '<b>Ваш id</b>: ' + chatId, { parse_mode : 'HTML' });
});

/**
 * получение топа новостей сайта Политсибру
 * за последние сутки, 10шт
 */
eventEmitter.on('get_top_new_politsibru', (chatId) => {
    mysql
        .getSqlQuery('SELECT `id_news`, `title`, `date`, `count` FROM news LEFT JOIN views ON views.news_id = news.id_news WHERE `date` >= DATE_SUB(NOW(), INTERVAL 24 HOUR) ORDER BY `count` DESC LIMIT 10;')
        .then(data => {
            let html = ['*Топ новостей сайта Политсибру за последние сутки:*'];
            
            for(let i = 0; i < data.length; i ++) {
                html.push(`${data[i].count} - [${data[i].title.substr(0,50)}](http://politsib.ru/news/${data[i].id_news})`);
            }
            
            html = html.join(' \n');
            
            bot.sendMessage(chatId, html, { parse_mode : 'Markdown' });
        })
        .catch(error => {
            bot.sendMessage(chatId, '*Ошибка, обратитесь в ит-отдел* \n ' + error, { parse_mode : 'Markdown' });
        });
});

bot.on('message', (msg) => {
    const chatId = msg.chat.id;
    
    bot.sendMessage(chatId, 'Выберите', { reply_markup : JSON.stringify({
        inline_keyboard: [
            [{ text: 'Получить свой id', callback_data: 'get_id' }],
            [{ text: 'Топ новостей сайта Политсибру', callback_data: 'get_top_new_politsibru' }],
            ]
        })
    });
});

bot.on('callback_query', function onCallbackQuery(callbackQuery) {
    eventEmitter.emit(callbackQuery.data, callbackQuery.message.chat.id);
    //bot.answerCallbackQuery(callbackQuery.id, "Теперь вы можете внести ваш id в профиль", false);
});