"use strict";

module.exports.start = () => {
    const express = require('express'),
        http = require('http'),
        app = express(),
        compression = require('compression'),
        bodyParser = require('body-parser'),
        errorlog = require('./functions').error;

    app.use(compression());
    app.disable('x-powered-by');
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true })); //jquery ajax не может без этого

    app.use((req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin', '*');

        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');

        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

        res.setHeader('Access-Control-Allow-Credentials', false);
        next();
    });

    app.use('/api', require('./api/public'));//публичное api
    app.use('/api/restricted', require('./api/restricted'));//api с ключом доступа

    app.use(function (req, res, next) {
        let err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    app.use(function (err, req, res, next) {
        let status = err.status || 500;
        res.sendStatus(status);
        errorlog(err);
        console.log(err);
    });

    let port = normalizePort(process.env.PORT || 3030);
    app.set('port', port);
    let server = http.Server(app);

    server.listen(port);
    server.on('error', onError);

    /**
     * общение с мастером
     */
    process.on('uncaughtException', (err) => {
        console.log(`Caught exception: ${err}\n`);
    });

    /**process.send('стартанул' + process.pid);

    process.on('message', (msg) => {
        console.log(msg);
    });*/
};

function normalizePort(val) {
    let port = parseInt(val, 10);

    if (isNaN(port)) {
        return val;
    }

    if (port >= 0) {
        return port;
    }

    return false;
}

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    let bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port

    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}
