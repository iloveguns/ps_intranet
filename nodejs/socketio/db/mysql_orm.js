/**
 * ORM для mysql, хуйня если честно, надо заменять и тесты переписывать?
 */
const Sequelize = require('sequelize'),
    mysql_config = require('../mysql_config');

const sequelize = new Sequelize(mysql_config.config.database, mysql_config.config.user, mysql_config.config.password, {
    host: mysql_config.config.host,
    dialect: 'mysql',
    logging: (process.env.NODE_ENV === 'development') ? console.log : false,
    define: {
        hooks: {
            /**
             * глобальный обработчик ошибок, ошибки кладутся доп атрибутом в ошибку(а можно и заменять целиком)
             *
             * .catch(err => {
             *   console.error(err.validateErrors)
             * })
             */
            validationFailed: (instance, options, err) => {
                let errorArray = [];
                for(let i = 0; i < err.errors.length; i++) {//собрать ошибки
                    errorArray.push({
                        attr : err.errors[i].path,
                        message : err.errors[i].message
                    });
                }

                err.validateErrors = errorArray;
            }
        }
    }
});

module.exports = sequelize;