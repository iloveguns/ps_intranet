"use strict";

let mysql      = require('mysql');
let mysql_config = require('../mysql_config');
let connection = mysql.createConnection(mysql_config.config);

/**
 * sql запрос в базу
 * @param {String} obj - sql запрос
 * @param {Object} data - параметр для биндинга к sql
 * @returns {Promise}
 */
exports.getSqlQuery = (sql, data) => {
    return new Promise(function(resolve, reject) {
        connection.query(sql, data, function(err, rows) {
            if(err){
                reject(err);
            } else {
                if(rows.length === 0){//пустой ответ
                    reject('empty sql query');
                } else {
                    resolve(rows);
                }
            }
        });
    });
};

/**
 * начальный формат sql запросов 'INSERT VALUES ?', {param: 1, taram: '2'}
 */
exports.formatInit = function() {
    connection.config.queryFormat = undefined;
};

/**
 * формат биндинга sql запросов 'INSERT VALUES (:param, :taram)', {param: 1, taram: '2'}
 */
exports.formatBind = function() {
    connection.config.queryFormat = function (query, values) {
        if (!values) return query;
        return query.replace(/\:(\w+)/g, function (txt, key) {
            if (values.hasOwnProperty(key)) {
                return this.escape(values[key]);
            }
            return txt;
        }.bind(this));
    };
};