<?php
namespace app\nodejs\socketio\php;

use Yii;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
Yii::getAlias('@vendor/vendor/autoload.php');

/*
 * отправка команд в socket.io ~1.*.*
 */
class sendSocketIo {
    private $client;
    public $port;

    /**
     * @param {int} $port - порт для отправки(3000)
     */
    function __construct($port = NULL){
        if($port === NULL) {
            $this->port = Yii::$app->params['ipServerWebSocketPort'];
        } else {
            $this->port = $port;
        }

        $this->client = new Client(new Version1X('http://localhost:'.$this->port));//Yii::$app->params['ipServerWebSocket']
    }

    function send($command, $data) {
        try {
            $this->client->initialize();
            $this->client->emit($command, $data);
            $this->client->close();
        }
        catch (ServerConnectionFailureException $e) {
            echo 'Server Connection Failure!!';
        }
    }
}