'use strict;'

process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');

chai.should();

chai.use(chaiHttp);

describe('Операции с комментариями', () => {
    let created_idcomment = 0;

    let data = {
        "id_user": 1,
        "item_id": 134,
        "item_class": "bugtracker",
        "text": "<p>Тестовый текст</p>",
        "reply_to": false
    };

    let wrong_data = {
        "item_id": 134,
        "item_class": "o[[",
        "text": "",
        "reply_to": "123"
    };

    describe('/POST api/comment', () => {
        it('Комментарий создается', (done) => {
            chai.request(server)
                .post('/api/comment')
                .send(data)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(true);
                    res.body.should.have.property('comment');
                    res.body.comment.should.be.a('object');
                    res.body.comment.should.have.property('id_comment');
                    res.body.comment.should.have.property('avatar_img');
                    res.body.comment.should.have.property('username');
                    res.body.comment.should.have.property('datetime');
                    res.body.comment.should.have.property('text');
                    res.body.comment.should.have.property('reply_to');

                    created_idcomment = res.body.comment.id_comment;
                    done();
                });
        });
    });

    describe('/POST api/comment', () => {
        it('Комментарий не создается', (done) => {
            chai.request(server)
                .post('/api/comment')
                .send(wrong_data)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(false);
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('validation');
                    res.body.errors.validation.should.be.a('array');
                    //можно и возвращаемый результат проверить
                    done();
                });
        });
    });

    describe('/PUT api/comment', () => {
        it('Комментарий редактируется', (done) => {
            chai.request(server)
                .put('/api/comment')
                .send({ "id_comment": created_idcomment, "text": "отредактированный" })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(true);
                    done();
                });
        });
    });

    describe('/PUT api/comment', () => {
        it('Комментарий не редактируется', (done) => {
            chai.request(server)
                .put('/api/comment')
                .send({ "id_comment": created_idcomment, "text": "" })
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });

        it('Комментарий не находит при редактировании', (done) => {
            chai.request(server)
                .put('/api/comment')
                .send({ "id_comment": -5, "text": "вполне нормальный текст" })
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                });
        });
    });

    describe('/DELETE api/comment', () => {
        it('Комментарий удаляется', (done) => {
            chai.request(server)
                .delete('/api/comment')
                .send({ "id_comment": created_idcomment })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(true);
                    done();
                });
        });
    });

    describe('/DELETE api/comment', () => {
        it('Комментарий не находит при удалении', (done) => {
            chai.request(server)
                .delete('/api/comment')
                .send({ "id_comment": "999999999999999999" })
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                });
        });
    });

});