'use strict;'

process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');

chai.should();

chai.use(chaiHttp);

describe('Операции с уведомлениями', () => {
    let created_notification = 0;

    let data = {
        "item_class": "bugtracker",
        "item_id": 100,
        "id_user": 1,
        "type": 1
    };

    let wrong_data = {
        "item_class": "",
        "item_id": 100,
        "id_user": 1,
        "type": 1
    };

    describe('/POST api/notification', () => {
        it('Уведомление создается', (done) => {
            chai.request(server)
                .post('/api/notification')
                .send(data)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(true);
                    res.body.should.have.property('id');
                    res.body.id.should.be.a('number');

                    created_notification = res.body.id;
                    done();
                });
        });

        it('Уведомление не создается', (done) => {
            chai.request(server)
                .post('/api/notification')
                .send(wrong_data)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(false);
                    res.body.should.have.property('errors');

                    done();
                });
        });
    });

    describe('/DELETE api/notification', () => {
        it('Уведомление прочитано', (done) => {
            chai.request(server)
                .delete('/api/notification')
                .send({
                    id_notification: created_notification
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(true);

                    done();
                });
        });
    });

    describe('/DELETE api/notification_del', () => {
        it('Уведомление удаляется физически', (done) => {
            chai.request(server)
                .delete('/api/notification_del')
                .send({
                    id_notification: created_notification
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(true);

                    done();
                });
        });
    });

    describe('/POST api/notification_all_send', () => {
        it('Уведомление создается и отправляется всем по классу и ид', (done) => {
            chai.request(server)
                .post('/api/notification_all_send')
                .send({//существующие связи, багтрекер с 3 людьми
                    "item_class": "bugtracker",
                    "item_id": 121,
                    "type": 1,
                    "from_user": 1,
                    "to_id": 100,
                    "to_name": "test"
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(true);

                    done();
                });
        });
    });

});