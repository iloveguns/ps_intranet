'use strict;'

process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let User = require('../models/mysql/user');
let should = chai.should();

chai.use(chaiHttp);

describe('Операции с пользователем', () => {
    let user_data = {
        username: "admin",
        password: "admin"
    };

    beforeEach((done) => { //Перед каждым тестом чистим базу
        User.destroy({ where: {}, truncate: true }).then(() => {
            done();
        });
    });

    describe('/POST api/user', () => {
        it('Пользователь создается', (done) => {
            chai.request(server)
                .post('/api/user')
                .send(user_data)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token');
                    res.body.should.have.property('success').eql(true);
                    done();
                });
        });
    });

    describe('/POST api/user/token', () => {
        it('Пользователь получает токен', (done) => {
            chai.request(server)
                .post('/api/user')
                .send(user_data)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token');
                    res.body.should.have.property('success').eql(true);
                    console.log(res.body.token);
                    done();
                });
        });
    });

});