"use strict";

const Sequelize = require('sequelize'),
    db = require('../../db/mysql_orm');

const Notification = db.define('unread_notice', {
    id_notice: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    class: {//item_class
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            len: [5, 50]
        }
    },
    item_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    id_user: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    from_user: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    type: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    readed: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    to_id: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    to_name: {
        type: Sequelize.STRING,
        allowNull: true,
        validate: {
            len: [1, 20]
        }
    }
}, {
    freezeTableName: true,
    hooks: {
        beforeValidate: (notification) => {
            if(notification.readed === undefined) {
                notification.readed = 0;
            }
        }
    },
    updatedAt: false,
    createdAt: 'datetime',
});

module.exports = Notification;