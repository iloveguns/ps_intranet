"use strict";

const Sequelize = require('sequelize'),
    db = require('../../db/mysql_orm'),
    Entities = require('html-entities').XmlEntities,
    entities = new Entities(),
    jsdom = require("jsdom"),
    { JSDOM } = jsdom,
    striptags = require('striptags');

/**
 * манипуляции с текстом коммента перед сохранением/редактированием
 * убрать пробелы и кодировать html
 * удалить пустые строки с пробелом
 * локальные требования редактора
 *
 * @param {string} text - текст комментария
 * @returns {string}    - обработанныый текст комментария
 */
function prepareComment(text) {
    let tmptext = text.replace(/\&nbsp;/g, '').replace(/(<p><\/p>)/g, "").trim().replace(/\s+/g, ' ');

    //разрешенные теги
    tmptext = striptags(tmptext, ['p', 'a', 'br', 'ol', 'ul', 'li', 'img', 'iframe', 'strong', 'em', 'video', 'source']);

    //если редактор через тег <p>
    const dom = new JSDOM(tmptext),
        lengthp = dom.window.document.getElementsByTagName('p').length;//кол-во вхождений тега <p>

    if(lengthp === 1) {//просто удаление
        tmptext = tmptext.replace( /<p>/g, "" ).replace( /<\/p>/g, "" );
    } else if(lengthp > 1) {//замена и добавление <br>
        tmptext = tmptext.replace( /<p>/g, "" ).replace( /<\/p>/g, "<br>" );

        //последний <br> не нужен
        if(tmptext.substr(-4) === '<br>') {
            tmptext = tmptext.substr(0, tmptext.length - 4);
        }
    }

    tmptext = entities.encode(tmptext);
    return tmptext;
}

const Comments = db.define('comments', {
    id_comment: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    item_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    class: {//item_class
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            len: [5, 128]
        }
    },
    id_user: {
        type: Sequelize.INTEGER
    },
    text: {
        type: Sequelize.TEXT,
        allowNull: false,
        validate: {
            len: [5]
        }
    },
    reply_to: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    deleted: {
        type: Sequelize.ENUM('0','1')
    },
    changed: {
        type: Sequelize.ENUM('0','1')
    }
}, {
    hooks: {
        beforeCreate: (comment) => {
            comment.text = prepareComment(comment.text);

            if(!comment.text) {//если после всех манипуляций текст пуст
                return Sequelize.Promise.reject({validateErrors: "comment is empty"});
            }

            if(comment.reply_to === false) {
                comment.reply_to = null;
            } else {
                comment.reply_to = parseInt(comment.reply_to);
            }
        },
        beforeUpdate: (comment) => {
            comment.text = entities.decode(comment.text);//кодирует снова, поэтому сначал раскодировать
            comment.text = prepareComment(comment.text);

            if(!comment.text) {
                return Sequelize.Promise.reject({validateErrors: "comment is empty"});
            }
        },
        afterCreate: (comment) => {
            comment.text = entities.decode(comment.text);
        },
        afterFind: (comments) => {
            if(comments) {
                let usersArr = [];//пользователи из комментариев, уникальный массив(может здесь прям их найти)
                let idCommentsArr = [];//ид комментариев

                for (let i = 0; i < comments.length; i++) {
                    if (usersArr.indexOf(comments[i].id_user) === -1 && comments[i].id_user !== null) {
                        usersArr.push(comments[i].id_user);
                    }
                    idCommentsArr.push(comments[i].id_comment);
                    comments[i].text = entities.decode(comments[i].text);
                }
                comments.usersArr = usersArr;
                comments.idCommentsArr = idCommentsArr;
            }
        }
    },
    updatedAt: false,
    createdAt: 'create_date',
});

module.exports = Comments;