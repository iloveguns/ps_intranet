"use strict;"

const Sequelize = require('sequelize');
const db = require('../../db/mysql_orm');

const User = db.define('User', {
    pk_user: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    login: {
        type: Sequelize.STRING(30),
        allowNull: false,
        validate: {
            len: [5,30]
        }
    },
    password: {
        type: Sequelize.STRING(60),
        allowNull: false,
        validate: {
            len: [5,60]
        }
    }
}, {
    updatedAt: false
});

module.exports = User;