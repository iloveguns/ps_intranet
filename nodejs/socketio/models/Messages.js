"use strict";

let messages = function(){};

messages.READED = 1; //прочитано
messages.UNREADED = 0; //непрочитано

const mysql = require('../db/mysql'),
    async = require('async'),
    Entities = require('html-entities').XmlEntities,
    entities = new Entities(),
    errorlog = require('../functions').error,
    EMPTY_SQL = require('../mysql_config').EMPTY_SQL;

mysql.formatBind();

/**
 * получение списка диалогов по ид пользователя
 * возвращает все, даже если их мильон
 *
 * @param {int} id_user - ид пользователя
 *
 * @return {Promise}
 *      @param {int} pk_dialog        - ид диалога
 *   if @param {datetime} date_dialog - дата создания диалога
 *      @param {array} users          - массив пользователей, участвующих в данном диалоге
 *      @param {object} message       - последнее сообщение диалога(полное)
 */
messages.dialogsList = (id_user) => {
    return new Promise((resolve, reject) => {
        //найти диалоги, в которых состоит пользователь
        mysql
            .getSqlQuery("SELECT `pk_dialog`, `fk_last_message`, `create_date` FROM `messages_dialog` WHERE `pk_dialog` IN " +
                "(SELECT `fk_dialog` FROM `messages_dialogs_users` WHERE `fk_user` = :id_user) ORDER BY `create_date` DESC", {
                id_user: id_user
            })
            .then(data_dialogs => {
                let findPkMessages = [], //закидывать ид сообщений для вывода в диалог (последнее сообщение)
                    findPkDialogs = []; //

                //собрать данные для поиска
                for(let i = 0; i < data_dialogs.length; i++) {
                    findPkDialogs.push(data_dialogs[i].pk_dialog);

                    /**
                     * если в диалоге написано хоть одно сообщение, добавить в массив для поиска посл сообщения
                     * у новых может и не быть
                     */
                    if(data_dialogs[i].fk_last_message !== null) {
                        findPkMessages.push(data_dialogs[i].fk_last_message);
                    }
                }

                //параллельно получать данные
                async.parallel({
                    messages: (callback) => { //сообщения
                        /** пустое при одном новом диалоге без сообщений */
                        if(findPkMessages.length === 0) {
                            callback(null, new Map());
                        } else {
                            mysql
                                .getSqlQuery("SELECT * FROM `messages_messages` WHERE `pk_message` IN (" + findPkMessages.join(',') + ")")
                                .then(all_messages => {
                                    let return_messages = new Map();

                                    for (let i = 0; i < all_messages.length; i++) {
                                        return_messages.set(all_messages[i].pk_message, {
                                            fk_user_sender: all_messages[i].fk_user_sender,
                                            create_date: all_messages[i].create_date,
                                            text: all_messages[i].text.substr(0, 50),
                                            readed: all_messages[i].readed
                                        });
                                    }

                                    callback(null, return_messages);
                                })
                                .catch(err => {
                                    callback(err);
                                });
                        }
                    },
                    users: (callback) => { //ид всех в конкретном диалоге
                        mysql
                            .getSqlQuery("SELECT `fk_dialog`, `fk_user` FROM `messages_dialogs_users` WHERE `fk_dialog` IN (" + findPkDialogs.join(',') + ")")
                            .then(all_users => {
                                let return_users = new Map();

                                for(let i = 0; i < all_users.length; i++) {
                                    if(return_users.has(all_users[i].fk_dialog)) {
                                        return_users.get(all_users[i].fk_dialog).push(all_users[i].fk_user);
                                    } else {
                                        return_users.set(all_users[i].fk_dialog, [all_users[i].fk_user]);
                                    }
                                }

                                callback(null, return_users);
                            })
                            .catch(err => {
                                callback(err);
                            });
                    },
                    info_users: (callback) => { //данные всех пользователей сразу(фото, имя), можно и в users найти
                        mysql
                            .getSqlQuery("SELECT `id_user`, `photo`, `lastname`, `name` FROM `user` WHERE `id_user` IN " +
                                "( SELECT DISTINCT(`fk_user`) FROM `messages_dialogs_users` WHERE `fk_dialog` IN (" + findPkDialogs.join(',') + ") )")
                            .then(photo_users => {
                                let return_users = [];

                                for(let i = 0; i < photo_users.length; i++) {
                                    return_users.push({
                                        id_user: photo_users[i].id_user,
                                        avatar: photo_users[i].photo,
                                        lastname: photo_users[i].lastname,
                                        name: photo_users[i].name,
                                        online: 0
                                    });
                                }

                                callback(null, return_users);
                            })
                            .catch(err => {
                                callback(err);
                            });
                    },
                    unread_count: (callback) => { //кол-во непрочитанных сообщений в каждом диалоге
                        async.map(findPkDialogs, (pk_dialog, callback) => { // по порядку конечно
                            mysql
                                .getSqlQuery("SELECT COUNT(`readed`) AS c FROM `messages_messages` WHERE `fk_dialog` = :pk_dialog AND `fk_user_sender` != :id_user AND `readed` = 0;", {
                                    id_user: id_user,
                                    pk_dialog: pk_dialog
                                })
                                .then(count_d => {
                                    callback(null, {pk_dialog, count:count_d[0].c});
                                })
                                .catch(err => {
                                    callback(err);
                                });
                        }, (err, results) => {
                            if(err) callback(err);

                            //пересобрать для удобства
                            let tmp = new Map();

                            for (let i = 0; i < results.length; i++) {
                                tmp.set(results[i].pk_dialog, results[i].count);
                            }

                            callback(null, tmp);
                        });
                    }
                }, (err, results) => {
                    if(err) {
                        errorlog(err);
                        return reject(err);
                    }

                    let returnJSON = [];

                    //последняя сборка
                    for(let i = 0; i < data_dialogs.length; i++) {
                        let tmp = {};

                        tmp.pk_dialog = data_dialogs[i].pk_dialog;
                        tmp.unread = results.unread_count.get(tmp.pk_dialog);
                        tmp.users = results.users.get(tmp.pk_dialog);
                        tmp.message = results.messages.get(data_dialogs[i].fk_last_message) || null;
                        if(tmp.message === null) {
                            tmp.date_dialog = data_dialogs[i].create_date;
                        }

                        returnJSON.push(tmp);
                    }

                    resolve({
                        dialogList: returnJSON, //основная инфа
                        infoUsers: results.info_users //инфа о всех пользователях(получить один раз и хранить на клиенте)
                    });
                });
            })
            .catch(err => {
                if(err === EMPTY_SQL) {
                    /** найти данные только для себя */
                    mysql
                        .getSqlQuery("SELECT `id_user`, `photo`, `lastname`, `name` FROM `user` WHERE `id_user` = :id_user", {
                            id_user
                        })
                        .then(row => {
                            resolve({
                                success: 'empty',
                                infoUsers : {
                                    id_user: row[0].id_user,
                                    avatar: row[0].photo,
                                    lastname: row[0].lastname,
                                    name: row[0].name,
                                    online: 0
                                }
                            });
                        })
                        .catch(err => {
                            reject(err);
                        });
                } else {
                    reject(err);
                }
            });
    });
};

/**
 * проверить, есть ли такой диалог уже
 * (1) найти пересечение ид бесед массива пользователей
 * (2) найти те, в которых только они и если нет таких - то можно создать новую
 * возвращает гарантированный результат, что есть такой точно диалог между конкретными пользователями или нет
 *
 * @param {array} users     - массив пользователей, из которых создается беседа
 *
 * @return {Promise} {bool} (false - нет диалога, true - есть, найден)
 */
messages.existDialog = (users) => {
    users.sort();

    return new Promise((resolve, reject) => { //(1)
        /** из одного не может быть создан */
        if(users.length <= 1) resolve(true);

        async.map(users, (user, callback) => {
            mysql
                .getSqlQuery("SELECT `fk_dialog` FROM `messages_dialogs_users` WHERE `fk_user` = :id_user;", {
                    id_user: user
                })
                .then(found_dialogs => {
                    let tmp = [];

                    for (let i = 0; i < found_dialogs.length; i++) {
                        tmp.push(found_dialogs[i].fk_dialog);
                    }

                    callback(null, tmp);
                })
                .catch(err => {
                    callback(err);
                });
        }, (err, results) => {
            //если не найдено - значит можно создавать(true)
            if(err === 'empty sql query') {
                return resolve(false);
            } else if(err) {
                errorlog(err);
                reject(err);
            }

            let intersectionDialogs = intersection(results);

            async.map(intersectionDialogs, (pk_dialog, callback) => { //(2)
                mysql
                    .getSqlQuery("SELECT `fk_user` FROM `messages_dialogs_users` WHERE `fk_dialog` = :pk_dialog;", {
                        pk_dialog: pk_dialog
                    })
                    .then(found_users => {
                        let tmp = [];

                        for (let i = 0; i < found_users.length; i++) {
                            tmp.push(found_users[i].fk_user);
                        }

                        callback(null, tmp);
                    })
                    .catch(err => {
                        callback(err);
                    });
            }, (err, results) => {
                if(err) reject(err);

                for (let i = 0; i < results.length; i++) {
                    if(results[i].sort().compare(users) === true) {
                        resolve(true);//можно вернуть ид диалога прям
                        break;
                    }
                }

                //если не найдено - значит можно создавать(true)
                resolve(false);
            });
        });
    });
};

/**
 * создание беседы с нуля(между двумя или более пользователями)
 * проверка перед созданием - не должно такого диалога существовать
 *
 * вернуть инфу о всех участниках
 *
 * @param {int} im          - ид создателя(свой)
 * @param {array} users     - массив пользователей, с которыми создается беседа, включая себя
 *
 * @return {Promise}
 */
messages.createDialog = (im, users) => {
    let allUsers = users.slice();
    allUsers.push(im);

    return new Promise((resolve, reject) => {
        messages.existDialog(allUsers)
            .then(response => {
                if (response === false) { //можно создавать
                    mysql
                        .getSqlQuery("INSERT INTO `messages_dialog` (`pk_dialog`, `fk_last_message`, `create_date`) VALUES (NULL, NULL, CURRENT_TIMESTAMP);")
                        .then(row => {
                            /** подготовить вставку связей, диалог - пользователи */
                            let ins = [];

                            for(let i = 0; i < allUsers.length; i++) {
                                ins.push([row.insertId, allUsers[i]]);
                            }

                            mysql.formatInit();//массовая вставка

                            mysql
                                .getSqlQuery("INSERT INTO `messages_dialogs_users`(`fk_dialog`, `fk_user`) VALUES ?;", [ins])
                                .then(() => {
                                    mysql.formatBind();

                                    /** инфа о новых участниках диалога */
                                    mysql
                                        .getSqlQuery("SELECT `id_user`, `photo`, `lastname`, `name` FROM `user` WHERE `id_user` IN (" + allUsers.join(',') + ")")
                                        .then(rows => {
                                            let infoUsers = [];

                                            for(let i = 0; i < rows.length; i++) {
                                                infoUsers.push({
                                                    id_user: rows[i].id_user,
                                                    avatar: rows[i].photo,
                                                    lastname: rows[i].lastname,
                                                    name: rows[i].name,
                                                    online: 0
                                                });
                                            }

                                            resolve({
                                                success: true,
                                                infoUsers,
                                                pk_dialog: row.insertId,
                                                users: allUsers
                                            });
                                        })
                                        .catch(err => {
                                            reject(err);
                                        });
                                })
                                .catch(err => {
                                    mysql.formatBind();
                                    reject(err);
                                });
                        })
                        .catch(err => {
                            reject(err);
                        });
                } else {
                    resolve({success: false});
                }
            })
            .catch(err => {
                reject(err);
            });
    });
};

/**
 * НАВЕСТИ ПОРЯДОК
 * создание сообщения в беседу (2)
 * проверка, может ли пользователь писать в этот диалог(есть ли он в нем) (1)
 *
 * @param {int} id_user   - ид пользователя от которого сообщение
 * @param {int} id_dialog - ид беседы
 * @param {string} text   - текст сообщения
 *
 * @return {Promise}
 *      @param {Object} - вставленное сообщение
 */

messages.createMessage = (id_user, id_dialog, text) => {
    let enttext = entities.encode(text);

    return new Promise((resolve, reject) => {
        mysql //(1)
            .getSqlQuery("SELECT `fk_user` FROM `messages_dialogs_users` WHERE `fk_dialog` = :pk_dialog AND `fk_user` = :id_user;", {
                pk_dialog: id_dialog,
                id_user  : id_user
            })
            .then(() => {
                //сохранить сообщение (2)
                mysql
                    .getSqlQuery("INSERT INTO `messages_messages`(`fk_dialog`, `fk_user_sender`, `text`) VALUES (:id_dialog, :id_user, :text);", {
                        id_dialog: id_dialog,
                        id_user  : id_user,
                        text     : enttext
                    })
                    .then(row => {
                        //обновить последнее сообщение в списке диалогов
                        mysql
                            .getSqlQuery("UPDATE `messages_dialog` SET `fk_last_message` = :id_last_message, `create_date` = CURRENT_TIMESTAMP WHERE `pk_dialog` = :id_dialog;", {
                                id_last_message: row.insertId,
                                id_dialog: id_dialog
                            });

                        //найти всех(кроме отправляющего) и отправить уведомление
                        mysql
                            .getSqlQuery("SELECT `fk_user` FROM `messages_dialogs_users` WHERE `fk_dialog` = :id_dialog AND `fk_user` != :my_id", {
                                id_dialog : id_dialog,
                                my_id     : id_user
                            })
                            .then(users => {
                                let usersArr = [];

                                for (let i = 0; i < users.length; i++) {
                                    usersArr.push(users[i].fk_user);
                                }

                                /**
                                 *  полное сообщение как в getMessagesDialog
                                 *  можно и не брать из базы, все данные почти есть
                                 */
                                mysql
                                    .getSqlQuery("SELECT `pk_message`, `fk_user_sender`, `create_date`, `text`, `readed` FROM `messages_messages` " +
                                        "WHERE `pk_message` = :pk_message;", {
                                        pk_message: row.insertId
                                    })
                                    .then(rows => {
                                        users = usersArr;
                                        users.push(id_user); //все участники диалога, включая отправителя сообщения

                                        resolve({
                                            message: rows[0],
                                            id_dialog: id_dialog,
                                            usersArr: users
                                        });
                                    })
                                    .catch(err => {
                                        reject(err);
                                    });
                            })
                            .catch(err => {
                                reject(err);
                            });
                    })
                    .catch(err => {
                        reject(err);
                    });
            })
            .catch(err => {
                if(err === 'empty sql query') {
                    return resolve(false);
                } else if(err) {
                    reject(err);
                }
            });
    });
};

/**
 * чтение сообщения
 *
 * @param {int} id_mess - ид сообщения
 * @param {int} id_user - ид пользователя
 *
 * @return {Promise}
 */
messages.readMessage = (id_mess, id_user) => {
    return new Promise((resolve, reject) => {
        mysql
            .getSqlQuery("SELECT `fk_user_sender`, `readed`, `fk_dialog` FROM `messages_messages` WHERE `pk_message` = :id_mess;", {
                id_mess: id_mess
            })
            .then(message => {
                /** уже прочитано или автор пытается свое сообщение прочесть */
                if(message[0].readed === messages.READED || message[0].fk_user_sender === id_user) {
                    resolve({success: false});
                } else {
                    mysql
                        .getSqlQuery("UPDATE `messages_messages` SET `readed` = :readed WHERE `pk_message` = :id_mess;", {
                            readed : messages.READED,
                            id_mess: id_mess
                        })
                        .then(() => {
                            mysql //параллельно
                                .getSqlQuery("SELECT `fk_user` FROM `messages_dialogs_users` WHERE `fk_dialog` = :id_dialog", {
                                    id_dialog : message[0].fk_dialog
                                })
                                .then(users => {
                                    let usersArr = [];

                                    for (let i = 0; i < users.length; i++) {
                                        usersArr.push(users[i].fk_user);
                                    }

                                    resolve({success: true, usersArr, id_dialog: message[0].fk_dialog});
                                })
                                .catch(err => {
                                    reject(err);
                                });
                        })
                        .catch(err => {
                            reject(err);
                        });
                }
            })
            .catch(err => {
                reject(err);
            });
    });
};

/**
 * получение сообщений из диалога, проверка пользователя
 *
 * @param {int} id_user - ид пользователя
 * @param {int} id_dialog - ид диалога
 * @param {int} limit - кол-во загружаемых сообщений за раз
 * @param {int} offset - отступ загружаемых сообщений
 *
 * @return {Promise}
 */
messages.getMessagesDialog = (id_user, id_dialog, limit, offset) => {
    if(isNaN(limit) || limit < 0 || limit >= 100) limit = 15;
    if(isNaN(offset)  || offset < 0) offset = 0;

    return new Promise((resolve, reject) => {
        mysql
            .getSqlQuery("SELECT `id` FROM `messages_dialogs_users` WHERE `fk_dialog` = :id_dialog AND `fk_user` = :id_user", {
                id_dialog,
                id_user
            })
            .then(() => {
                mysql
                    .getSqlQuery("SELECT `pk_message`, `fk_user_sender`, `create_date`, `text`, `readed` FROM `messages_messages` " +
                        "WHERE `fk_dialog` = :id_dialog ORDER BY `create_date` DESC LIMIT :limit OFFSET :offset;", {
                        id_dialog: id_dialog,
                        limit: limit,
                        offset: offset
                    })
                    .then(rows => {
                        resolve(rows);
                    })
                    .catch(err => {
                        if(err === EMPTY_SQL) {
                            resolve('empty');
                        } else {
                            reject(err);
                        }
                    });
            })
            .catch(err => {
                reject(err);
            });

    });
};

/**
 * поиск контактов, глобальный
 *
 * @param {String} q - поисковый запрос
 *
 * @return {Promise}
 */
messages.searchContacts = (q) => {
    return new Promise((resolve, reject) => {
        mysql
            .getSqlQuery("SELECT `id_user`, `lastname`, `name`, `photo` AS avatar FROM `user` LEFT JOIN `user_status` ON `user`.`fk_status` = `user_status`.`pk_status`" +
                "WHERE (`lastname` LIKE :q OR `name` LIKE :q) AND `status` != 9 LIMIT 5;", {
                q: `%${q}%`
            })
            .then(rows => {
                resolve(rows);
            })
            .catch(err => {
                reject(err);
            });
    });
};

module.exports = messages;

//----------

/**
 * пересечение многих массивов
 * @returns {Array}
 */
function intersection() {
    let result = [];
    let lists;

    if(arguments.length === 1) {
        lists = arguments[0];
    } else {
        lists = arguments;
    }

    for(let i = 0; i < lists.length; i++) {
        let currentList = lists[i];
        for(let y = 0; y < currentList.length; y++) {
            let currentValue = currentList[y];
            if(result.indexOf(currentValue) === -1) {
                let existsInAll = true;
                for(let x = 0; x < lists.length; x++) {
                    if(lists[x].indexOf(currentValue) === -1) {
                        existsInAll = false;
                        break;
                    }
                }
                if(existsInAll) {
                    result.push(currentValue);
                }
            }
        }
    }
    return result;
}

/**
 * сравнение массивов
 * @param {array} array
 * @returns {boolean}
 */
Array.prototype.compare = function(array) {
    if (!array) {
        return false;
    }
    if (this.length !== array.length) {
        return false;
    }
    for (let i = 0, l = this.length; i < l; i++) {
        if (this[i] instanceof Array && array[i] instanceof Array) {
            if (!this[i].compare(array[i])) {
                return false;
            }
        }
        else if (this[i] !== array[i]) {
            return false;
        }
    }
    return true;
};