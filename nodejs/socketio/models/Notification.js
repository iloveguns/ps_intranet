const Notification = require('./mysql/notification');
let notification = function(){};

//константы экспорта, типы уведомлений
notification.NEW_COMMENT = 1;//новый комментарий
notification.REPLY_COMMENT = 2;//ответ на комментарий
notification.ADD_NEW_USERTO = 3;//новый сотрудник к существующему(задаче)
notification.NEW_REGISTER_USER = 10;//новый зарегавшийся
notification.NEW_CONGRAT = 20;//новое поздравление
notification.NEW_CALLBOARD = 30;//новое уведомление
notification.NEW_TASK = 40;//новая задача
notification.NEW_LOAD_FILE = 50;//новый загруженный файл
notification.CHANGE_STATUS = 60;//смена статуса
notification.NEW_EVENT = 70;//новое событие
notification.NOTIFY_EVENT = 71;//уведомление о событии(в настройках его)
notification.NEW_BUGTRACKER = 100;//объявление в баг-трекер
notification.NEW_SYSADMINQUERY = 101;//системному админу заявку
notification.NEW_STEWARDQUERY = 102;//завхозу
notification.CRM_DONE_YOUR_TASK = 200;//выполнена порученная задача
notification.API_MAIL_SENDING_DONE = 300;//рассылка email завершена

/**
 * сохранение уведомления
 * @param {array|int} users         - массив|ид пользователей
 * @param {Object} data             - параметры уведомления
 *      @param {string} item_class  - имя комментируемой сущности
 *      @param {int} item_id        - ид комментируемой сущности
 *      @param {int} id_user        - ид пользователя кому
 *      @param {int} type           - тип уведомления
 *  необязательные
 *      @param {int} from_user      - ид пользователя от кого
 *      @param {int} to_id          - доп ид комментируемой сущности
 *      @param {int} to_name        - доп имя комментируемой сущности
 * @return {Promise}
 */
notification.create = (users, data) => {
    let notif;
    data.class = data.item_class;
    delete(data.item_class);

    if(Array.isArray(users)) {//массив, массовая вставка
        let insarr = [];

        for (let i = 0; i < users.length; i++) {
            let tmp = Object.assign({}, data);
            tmp.id_user = users[i];
            insarr.push(tmp);
        }

        notif = Notification
            .bulkCreate(insarr);
    } else {//одиночная вставка
        data.id_user = users;

        notif = Notification
            .create(data);
    }

    return new Promise((resolve, reject) => {
        notif
            .then((res) => {
                resolve(res);
            })
            .catch(err => {
                let errors;
                if(err.validateErrors) {
                    errors = {validation: err.validateErrors};
                } else {
                    errors = err;
                }
                reject(errors);
            });
    });
};

/**
 * удаление уведомления( метка об удалении )
 * @param {int} id - ид уведомления
 */
notification.delete = (id) => {
    return new Promise((resolve, reject) => {
        Notification
            .findOne({where: {id_notice: id}})
            .then(notice => {
                if(notice === null) {
                    reject({ 'code': 404 });
                } else if(notice.readed === true) {
                    resolve(notice);
                } else {
                    notice
                        .updateAttributes({
                            readed: true
                        })
                        .then(() => {
                            resolve(notice);
                        })
                        .catch(err => {
                            reject(err);
                        });
                }
            })
            .catch(err => {
                let errors;
                if(err.validateErrors) {
                    errors = {validation: err.validateErrors};
                } else {
                    errors = err;
                }
                reject(errors);
            });
    });
};

/**
 * удаление уведомления( физическое )
 * @param {int} id - ид уведомления
 */
notification.reallydelete = (id) => {
    return new Promise((resolve, reject) => {
        Notification
            .findOne({where: {id_notice: id}})
            .then(notice => {
                if(notice === null) {
                    reject({ 'code': 404 });
                } else {
                    notice
                        .destroy()
                        .then(() => {
                            resolve(notice);//инфа еще здесь
                        })
                        .catch(err => {
                            reject(err);
                        });
                }
            })
            .catch(err => {
                let errors;
                if(err.validateErrors) {
                    errors = {validation: err.validateErrors};
                } else {
                    errors = err;
                }
                reject(errors);
            });
    });
};

/**
 * найти всех, кому нужно отправить уведомление, сохранить и отправить в вебсокет
 * @param {object} data             - объект для вставки уведомлений
 *      @see notification.create
 * @param {array|int} exclude_users - сотрудник(и) для исключения(ид)
 * @param {array|int} include_users - сотрудник(и) для добавления в рассылку(ид)
 */
notification.findAll_Save_UpdateNotify = (data, exclude_users, include_users) => {
    //принудительно в массив, если не массив
    if(exclude_users === undefined) {
        exclude_users = [];
    } else if(!Array.isArray(exclude_users)) {
        exclude_users = [exclude_users];

        for(let i = 0; i < exclude_users.length; i ++) {
            exclude_users[i] = parseInt(exclude_users[i], 10);
        }
    }

    if(include_users === undefined) {
        include_users = [];
    } else if(!Array.isArray(include_users)) {
        include_users = [include_users];

        for(let i = 0; i < include_users.length; i ++) {
            include_users[i] = parseInt(include_users[i], 10);
        }
    }

    /**
     * такая тема, на объявления и блоги не слать уведомления всем, только автору
     */
    if(data.item_class === 'callboard' || data.item_class === 'blogs') {
        let attr  = '';
        if(data.item_class == 'callboard') {
            attr = 'pk_callboard';
        } else {
            attr = 'pk_blog';
        }

        //добавить отправку подписчикам
        return new Promise((resolve, reject) => {
            Notification.sequelize.query(
                'SELECT `id_author` FROM `' + data.item_class + '` WHERE `' + attr + '` = :pk AND `id_author` != :id_author',
                {
                    replacements: {
                        pk: data.item_id,
                        id_author: data.from_user //автору не надо получать уведомления на свое
                    }, type: Notification.sequelize.QueryTypes.SELECT
                }
            )
                .then((id_author) => {
                    let arrUsers = [];

                    if(id_author.length) {
                        arrUsers = [id_author[0].id_author];//автор только один
                    }
                    if(include_users) {
                        arrUsers = arrUsers.concat(include_users);
                    }
                    if(exclude_users) {
                        for (let i = 0; i < exclude_users.length; i++) {
                            if(arrUsers.includes(exclude_users[i])) {
                                arrUsers.splice(arrUsers.indexOf(exclude_users[i]), 1);
                            }
                        }
                    }

                    if(arrUsers) {
                        //сохранить в базу
                        notification
                         .create(arrUsers, data)
                         .then(() => {//отправить в вебсокет
                             process.send({ type: 'sendws', const: 'updateNotify', users: arrUsers});
                            resolve();
                        })
                         .catch(err => {
                            reject(err);
                        });
                    } else {
                        reject({ 'error' : 'Некому отсылать'});
                    }
                })
                .catch(err => {
                    reject(err);
                });
        });
    } else {
        return new Promise((resolve, reject) => {
            if(!data.item_class || !data.item_id) {
                reject({ 'error' : 'item_class или item_id'});
            }
            Notification.sequelize.query(
                'SELECT `id_user` FROM `user` LEFT JOIN `relation_to_divizion` ON user.fk_divizion = relation_to_divizion.id_divizion LEFT JOIN `user_status` ON user.fk_status = user_status.pk_status WHERE relation_to_divizion.item_id = :item_id AND relation_to_divizion.class = :class AND user_status.status != :nactive ' +
                'UNION ' +
                'SELECT `id_user` FROM `relation_to_user` WHERE `item_id` = :item_id AND `class` = :class ' +
                'UNION  ' +
                'SELECT `id_user` FROM `user` LEFT JOIN `relation_to_department` ON user.fk_department = relation_to_department.id_department LEFT JOIN `user_status` ON user.fk_status = user_status.pk_status WHERE relation_to_department.item_id = :item_id AND relation_to_department.class = :class AND user_status.status != :nactive ',
                {
                    replacements: {
                        class: data.item_class,
                        item_id: data.item_id,
                        nactive: 9//ЗАМЕНИТЬ КОНСТАНОЙ ПОЛЬЗОВАТЕЛЕЙ
                    }, type: Notification.sequelize.QueryTypes.SELECT
                }
            )
                .then(users => {
                    let arrUsers = [];

                    if(users.length) {
                        //собрать массив, отсеивая исключаемых
                        for (let i = 0; i < users.length; i++) {
                            if (exclude_users.indexOf(users[i].id_user) !== -1) continue;
                            arrUsers.push(users[i].id_user);
                        }
                    }
                    if(include_users) {
                        arrUsers = arrUsers.concat(include_users);
                    }

                    if (arrUsers) {
                        //сохранить в базу
                        notification
                            .create(arrUsers, data)
                            .then(() => {//отправить в вебсокет
                                process.send({ type: 'sendws', const: 'updateNotify', users: arrUsers});
                                resolve();
                            })
                            .catch(err => {
                                reject(err);
                            });
                    } else {
                        reject({ 'error' : 'Некому отсылать'});
                    }
                })
                .catch(err => {
                    console.log(err);
                    reject(err);
                });
        });
    }
}

module.exports = notification;