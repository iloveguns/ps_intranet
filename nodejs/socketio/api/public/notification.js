'use strict';

const router = require('express').Router(),
    Notification = require('../../models/Notification');

/**
 * создание уведомления, одиночное
 *
 * @see Notification.create
 * //@param {bool} send - отсылать ли онлайн уведомление
 *
 * @return JSON
 * {bool} success
 * {int} id - ид вставленной записи
 */
router.post('/notification', (req, res) => {
    let data = {},
        id_user = req.body.id_user;

    data.item_class = req.body.item_class;
    data.item_id = req.body.item_id;
    data.type = req.body.type;
    data.from_user = req.body.from_user;
    data.to_id = req.body.to_id;
    data.to_name = req.body.to_name;

    Notification
        .create(id_user, data)
        .then(resp => {
            res.send({success: true, id: resp.id_notice});
        })
        .catch(err => {
            res.send({success: false, errors: err});
        });
});

/**
 * создание уведомления, отправка всем
 * можно добавить пару параметров для рассылки email(1) и telegram(2)
 * @see Notification.findAll_Save_UpdateNotify
 */
router.post('/notification_all_send', (req, res) => {
    let data = {},
        exclude_users = req.body.exclude_users,
        include_users = req.body.include_users;

    data.item_class = req.body.item_class;
    data.item_id = req.body.item_id;
    data.type = req.body.type;
    data.from_user = req.body.from_user;
    data.to_id = req.body.to_id;
    data.to_name = req.body.to_name;

    Notification.findAll_Save_UpdateNotify(data, exclude_users, include_users)
    .then(() => {
        res.send({success: true});
    })
    .catch(err => {
        res.send({success: false, errors: err});
    });
});

/**
 * удаление уведомления
 *
 * @see Notification.delete
 * @param {int} id_notification - ид уведомления
 *
 * @return JSON
 * {bool} success
 */
router.delete('/notification', (req, res, next) => {
    let id = parseInt(req.body.id_notification);

    if (id) {
        Notification
            .delete(id)
            .then(() => {
                res.send({success: true});
            })
            .catch(err => {
                if (err.code === 404) {
                    let err = new Error('Не найдено уведомление при удалении');
                    err.status = 404;
                    next(err);
                } else {
                    res.send({success: false, errors: err});
                }
            });
    } else {
        let err = new Error('Не передан ид уведомления при удалении');
        err.status = 400;
        next(err);
    }
});

/**
 * физическое удаление уведомления
 *
 * @see Notification.delete
 * @param {int} id_notification - ид уведомления
 *
 * @return JSON
 * {bool} success
 */
router.delete('/notification_del', (req, res, next) => {
    let id = parseInt(req.body.id_notification);

    if (id) {
        Notification
            .reallydelete(id)
            .then(() => {
                res.send({success: true});
            })
            .catch(err => {
                if (err.code === 404) {
                    let err = new Error('Не найдено уведомление при удалении');
                    err.status = 404;
                    next(err);
                } else {
                    res.send({success: false, errors: err});
                }
            });
    } else {
        let err = new Error('Не передан ид уведомления при удалении');
        err.status = 400;
        next(err);
    }
});

module.exports = router;