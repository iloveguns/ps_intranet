'use strict;'

const router = require('express').Router(),
    bcrypt = require('bcrypt'),
    jwt = require('jwt-simple'),
    User = require('../../models/mysql/user'),
    config = require('../../config'),
    Sequelize = require('sequelize');

/**
 * создание пользователя
 * уникальность+
 */
router.post('/user', function (req, res, next){
    let username = req.body.username,
        password = req.body.password;

    if(!username && !password) {//должны быть как минимум
        let err = new Error('Не переданы параметры при регистрации "/user"');
        err.status = 400;
        next(err);
    } else if(/\s/.test(username) || /\s/.test(password)) {//пробелы в данных
        let err = new Error('Переданные параметры при регистрации некорректны "/user"');
        err.status = 400;
        next(err);
    } else if(username.length < 5 || password.length < 5 || password.length > 30) {//первичная проверка
        let err = new Error('Переданные параметры при регистрации некорректны "/user"');
        err.status = 400;
        next(err);
    } else {
        User
            .findOne({
                where: {login: username},
                attributes: ['login'],
                raw: true
            })
            .then((data) => {
                if (data === null) {
                    //создание пользователя
                    bcrypt.hash(password, 10, function (err, hash) {
                        if (err) {
                            next(err);
                        } else {
                            //валидация модели
                            User
                                .create({
                                    login: username,
                                    password: hash
                                })
                                .then(() => {
                                    const token = jwt.encode({username: username}, config.secretkey);
                                    res.send({success: true, token: token});
                                })
                                .catch(Sequelize.ValidationError, function(err) {//err.name, err.message, err.errors
                                    let errorArray = [];
                                    for(let i = 0; i < err.errors.length; i++) {//собрать ошибки
                                        errorArray.push(err.errors[i].path);
                                    }

                                    res.send({success: false, errors: [{validation: errorArray}]});
                                })
                                .catch(err => {
                                    next(err);
                                });
                        }
                    });
                } else {
                    res.send({success: false, errors: [{exists: true}]});
                }
            });
    }
});

/**
 * получение токена
 */
router.post('/user/token', function(req, res, next) {
    let username = req.body.username,
        password = req.body.password;

    User
        .findOne({
            where: {login: username},
            attributes: ['login', 'password'],
            raw: true
        })
        .then((user) => {
            bcrypt.compare(password, user.password, function(err, valid){
                if (err) {
                    next(err);
                }

                if (!valid){
                    let err = new Error('Невалидный пароль при попытке получения токена "/user/token"');
                    err.status = 401;
                    next(err);
                } else {
                    const token = jwt.encode({username: username}, config.secretkey)
                    res.send({success: true, token: token});
                }
            });
        })
        .catch(err => {
            next(err);
        });
});

module.exports = router;