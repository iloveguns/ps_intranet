"use strict";

const router = require('express').Router(),
    path = require('path'),
    fs = require('fs'),
    multer  = require('multer'),
    readChunk = require('read-chunk'),
    imageType = require('image-type'),
    crypto = require('crypto'),
    local_params = require(__dirname + '/../../local_params'),
    allowExts = ['jpg', 'jpeg', 'gif', 'png',  'mp4', 'webm', 'ogg', 'ogv'], //поддерживаемые разрешения для загрузки

    storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, __dirname + '/../../../../web/uploads/api');//в основную папку загрузок(наверх)
        },
        filename: function (req, file, cb) {
            let ext = path.extname(file.originalname);
            let filename = Date.now() + '_' + uniqueName(10) + ext;
            cb(null, filename);
        }
    }),
    upload = multer({
        storage: storage,
        fileFilter: function(req, file, cb) {
            let ext = path.extname(file.originalname);

            //проверка по рарешению файла
            if(allowExts.indexOf(ext.toLowerCase().replace(/[^a-zA-Z0-9]+/g, "")) === -1) {
                return cb(null, false);//просто пропускать
            }
            cb(null, true);
        }
    });

/**
 * получить случайную строку
 * @param {int} len - длина строки
 * @return {string}
 */
function uniqueName(len) {
    return crypto.randomBytes(Math.ceil(len/2))
        .toString('hex')
        .slice(0,len + 1).toLowerCase();
}

/**
 * загрузка изображений
 * загружает только изображения с расширением [allowExts], остальное игнорирует
 */
router.post('/upload', upload.any(), (req, res, next) => {
    let returnArr = [];

    if(req.files === undefined) {
        res.json({ success: false, errors: ['Нет данных для загрузки /upload'] });
    } else {
        for(let i = 0; i < req.files.length; i++) {
            //let fpath = req.files[i].path;

            returnArr.push({ name: req.files[i].filename, path: local_params.getPath + 'uploads/api/' + req.files[i].filename });//должен быть абсолютный путь
            
            //точная проверка еще раз, отмена в связи с большим кол-вом расширений файлов
            /** const buffer = readChunk.sync(fpath, 0, 12);
            if(buffer === null || allowExts.indexOf(imageType(buffer).ext) === -1) {
                fs.unlink(fpath, function(err){
                    if(err) next(err);
                });
            } else {
                returnArr.push({ name: req.files[i].filename, path: local_params.getPath + 'uploads/api/' + req.files[i].filename });//должен быть абсолютный путь
            }*/
        }

        //отправка ответа
        res.json({ success: true, files: returnArr });

        //вставка в таблицу
        let mysql = require(__dirname + '/../../db/mysql');
        mysql.formatInit();

        let values = [];
        for (let i = 0, len = returnArr.length; i < len; i++) {
            values.push([null, returnArr[i].name]);
        }

        mysql.getSqlQuery('INSERT INTO `api_upload` (`id`, `file`) VALUES ?;', [values])
            .then(() => {
                mysql.formatBind();
            })
            .catch((err) => {});
    }
});

module.exports = router;