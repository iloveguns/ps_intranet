'use strict';

const router = require('express').Router(),
    Comments = require('../../models/mysql/comments'),
    Notification = require('../../models/Notification'),
    async = require('async');

/**
 * получение информации о рейтинге 1 комментария
 * @param {int} item_id - ид комментария
 * @param {int} id_user - ид пользователя
 * @returns {Promise}
 */
function getRatings(item_id, id_user) {
    return new Promise((resolve, reject) => {
        Comments.sequelize.query('SELECT `item_id`, `action`, `user`.`id_user`, `photo`, `lastname`, `name` ' +
            'FROM `ratings` LEFT JOIN `user` ON `ratings`.`id_user` = `user`.`id_user` ' +
            'WHERE `item_id` IN(:item_id) AND `class` = "comments"',
            {replacements: {item_id: item_id}, type: Comments.sequelize.QueryTypes.SELECT}
        )
            .then(ratings => {
                //по ид комментария сделать массив
                let map = new Map();

                for (let i = 0; i < ratings.length; i++) {
                    //начальное состояние
                    if (map.get(ratings[i].item_id) === undefined) {
                        map.set(ratings[i].item_id, {
                            likes: 0, //кол-во лайков(int)
                            dislikes: 0, //кол-во дизлайков(int)
                            likes_id: [], //ид юзеров лайкнувших(array)
                            dislikes_id: [] //ид юзеров дизлайкнувших(array)
                        });
                    }

                    //инкремент
                    if (ratings[i].action === 1) {
                        map.get(ratings[i].item_id).likes += 1;
                        map.get(ratings[i].item_id).likes_id.push({
                            id_user: ratings[i].id_user,
                            avatar_img: ratings[i].photo,
                            username: ratings[i].lastname + ' ' + ratings[i].name,
                        });
                    } else {
                        map.get(ratings[i].item_id).dislikes += 1;
                        map.get(ratings[i].item_id).dislikes_id.push({
                            id_user: ratings[i].id_user,
                            avatar_img: ratings[i].photo,
                            username: ratings[i].lastname + ' ' + ratings[i].name,
                        });
                    }

                    //пометить действие пользователя
                    if (ratings[i].id_user === id_user) {
                        map.get(ratings[i].item_id).mine = ratings[i].action;
                    }
                }

                resolve(map);
            })
            .catch(err => {
                reject(err);
            });
    });
}

/**
 * получение комментариев
 *
 * @param {int} item_id         - ид сущности
 * @param {string} item_class   - имя сущности
 * @param {int} id_user         - ид пользователя(для лайков)
 *
 * @param {int} limit(15)       - кол-во комментариев
 * @param {int} offset(0)       - отступ в поиске комментариев
 *
 * @return {JSON}
 * {bool} success
 * {array of object} comments
 *      {int} id_comment      - ид комментария
 *      {string} avatar_img   - изображение комментатора
 *      {int} id_user         - ид комментатора
 *      {string} username     - имя комментатора(полное)
 *      {date} datetime       - datetime времени создания комментария
 *      {string} text         - текст комментария
 *      {int|null} reply_to   - ответ на комментарий(ид)
 *      {int} changed         - изменен ли
 *      {int} deleted         - удален ли
 *      {int} haveMore        - есть ли еще комментарии(кол-во)
 *      {object} ratings      - информация о лайках/дизлайках
 *          {int} like        - кол-во лайков
 *          {int} dislike     - кол-во дизлайков
 *          {int} mine        - оценка пользователя(необяз)
 */
router.get('/comment', (req, res, next) => {
    let item_id = parseInt(req.query.item_id),
        item_class = req.query.item_class,
        id_user = parseInt(req.query.id_user),
        limit = parseInt(req.query.limit) || 15,
        offset = parseInt(req.query.offset) || 0;

    if (limit > 100) limit = 100;

    if (item_id && item_class) {
        Comments
            .findAndCountAll({
                where: {item_id: item_id, class: item_class},
                attributes: ['id_comment', 'id_user', 'text', 'create_date', 'deleted', 'changed', 'reply_to'],
                limit: limit,
                offset: offset,
                order: 'id_comment DESC',
                raw: true
            })
            .then((result) => {
                let comments = result.rows,
                    haveMoreComments = result.count - (limit + offset);

                if (haveMoreComments < 0) haveMoreComments = 0;

                //нужно параллельно доставать лайки/дизлайки, пока сделаю обычно

                //если есть данные
                if (result.count > 0) {
                    async.parallel({
                        users: (callback) => {//поиск данных комментаторов
                            if(result.rows.usersArr.length) {
                                Comments.sequelize.query('SELECT `lastname`, `name`, `id_user`, `photo` FROM user WHERE id_user IN(:id_users)',
                                    {replacements: {id_users: result.rows.usersArr}, type: Comments.sequelize.QueryTypes.SELECT}
                                )
                                    .then(users => {
                                        let usersArr = [];
                                        for (let i = 0; i < users.length; i++) {
                                            usersArr[users[i].id_user] = {
                                                lastname: users[i].lastname,
                                                name: users[i].name,
                                                photo: users[i].photo
                                            }
                                        }
                                        callback(null, usersArr);
                                    })
                                    .catch(err => {
                                        console.log(err);
                                    });
                            } else {
                                callback(null, []);
                            }
                        },
                        ratings: (callback) => {//поиск информации о лайках/дизлайках комментариев
                            getRatings(result.rows.idCommentsArr, id_user).then((ratings) => {
                                callback(null, ratings);
                            });
                        }
                    }, (err, results) => {
                        for (let i = 0; i < comments.length; i++) {
                            //специфика, нулевой юзер - интранет
                            if (results.users[comments[i].id_user] === undefined) {
                                comments[i].avatar_img = '/images/nulluser.png';
                                comments[i].username = null;
                            } else {
                                comments[i].avatar_img = results.users[comments[i].id_user].photo;
                                comments[i].username = results.users[comments[i].id_user].lastname + ' ' + results.users[comments[i].id_user].name;
                            }
                            comments[i].datetime = comments[i].create_date;
                            comments[i].deleted = parseInt(comments[i].deleted);
                            comments[i].changed = parseInt(comments[i].changed);
                            delete(comments[i].create_date);

                            //добавить данные о лайках/дизлайках
                            if(results.ratings.has(comments[i].id_comment) !== undefined) {
                                comments[i].ratings = results.ratings.get(comments[i].id_comment);
                            }
                        }

                        res.send({
                            success: true,
                            haveMore: haveMoreComments,
                            comments: comments
                        });
                    });
                } else {
                    res.send({
                        success: true,
                        haveMore: haveMoreComments,
                        comments: comments
                    });
                }
            })
            .catch(err => {
                next(err);
            });
    } else {
        let err = new Error('Не переданы обязательные параметры для получения комментариев');
        err.status = 400;
        next(err);
    }
});

/**
 * создание нового комментария
 *
 * @param {int} id_user         - ид пользователя
 * @param {int} item_id         - ид комментируемой сущности
 * @param {string} item_class   - имя комментируемой сущности
 * @param {string} text         - текст комментария
 * @param {int|false} reply_to  - ответ на комментарий(ид)
 *
 * @return {JSON}
 * {bool} success
 * {object} comment
 *      {int} id_comment      - ид комментария
 *      {string} avatar_img   - изображение комментатора
 *      {int} id_user         - ид комментатора
 *      {string} username     - имя комментатора(полное)
 *      {date} datetime       - datetime времени создания комментария
 *      {string} text         - текст комментария
 *      {int|null} reply_to   - ответ на комментарий(ид)
 */
router.post('/comment', (req, res, next) => {
    let comment = {
        id_user : parseInt(req.body.id_user),
        item_id : parseInt(req.body.item_id),
        class   : req.body.item_class,
        text    : req.body.text,
        reply_to: req.body.reply_to
    };

    Comments
        .create(comment)
        .then((data) => {
            //поиск данных пользователя
            Comments.sequelize.query('SELECT `lastname`, `name`, `photo` FROM user WHERE id_user = $id_user',
                {bind: {id_user: comment.id_user}, type: Comments.sequelize.QueryTypes.SELECT}
            ).then(user => {
                res.send({
                    success: true,
                    comment: {
                        id_comment: data.id_comment,
                        avatar_img: user[0].photo,
                        id_user: comment.id_user,
                        username: user[0].lastname + ' ' + user[0].name,
                        datetime: data.create_date,
                        text: data.text,
                        reply_to: data.reply_to
                    }
                });

                //ответившему создать отдельное уведомление
                if(comment.reply_to) {
                    //найти пользователя, чей коммент и отправить уведомление об ответе на его комментарий
                    Comments
                        .findOne({
                            where: {id_comment: comment.reply_to},
                            attributes: ['id_user'],
                            raw: true
                        })
                        .then(comment_user => {
                            //ответ на свой комментарий, мало ли
                            if(comment_user.id_user !== comment.id_user) {
                                Notification
                                    .create(comment_user.id_user, {
                                        item_class: comment.class,
                                        item_id: comment.item_id,
                                        type: Notification.REPLY_COMMENT,
                                        from_user: comment.id_user,
                                        to_id: data.id_comment,
                                        to_name: 'comments'
                                    })
                                    .then(() => {//отправить в вебсокет
                                        process.send({ type: 'sendws', const: 'updateNotify', users: comment_user.id_user});
                                    });
                            }

                            //сохранение всем, кто должен получить, кроме ответа
                            Notification.findAll_Save_UpdateNotify({
                                item_class: comment.class,
                                item_id: comment.item_id,
                                type: Notification.NEW_COMMENT,
                                from_user: comment.id_user,
                                to_id: data.id_comment,//ид сохраненного комментария (ссылку можно сделать)
                                to_name: 'comments'
                            }, [comment.id_user, comment_user.id_user], []);
                        });
                } else {
                    //сохранение всем, кто должен получить
                    Notification.findAll_Save_UpdateNotify({
                        item_class: comment.class,
                        item_id: comment.item_id,
                        type: Notification.NEW_COMMENT,
                        from_user: comment.id_user,
                        to_id: data.id_comment,//ид сохраненного комментария (ссылку можно сделать)
                        to_name: 'comments'
                    }, comment.id_user, []);
                }
            });
        })
        .catch(err => {
            if (err.validateErrors) {
                res.send({success: false, errors: {validation: err.validateErrors}});
            } else {
                next(err);
            }
        });
});

/**
 * просто создать системный комментарий
 */
router.post('/syscomment', (req, res, next) => {
    let comment = {
        id_user : null,
        item_id : parseInt(req.body.item_id),
        class   : req.body.item_class,
        text    : req.body.text,
        reply_to: 0
    };
    
    let excl_users = req.body.excl_users;
    let incl_users = req.body.incl_users;
    
    Comments
        .create(comment)
        .then((data) => {
            Notification.findAll_Save_UpdateNotify({
                item_class: comment.class,
                item_id: comment.item_id,
                type: Notification.NEW_COMMENT,
                from_user: comment.id_user,
                to_id: data.id_comment,
                to_name: 'comments'
            }, excl_users, incl_users);
            
            res.send({
                success: true,
            });
        })
        .catch(err => {
            next(err);
        });
});

/**
 * редактирование комментария
 * @param {int} id_comment      - ид комментария
 * @param {string} text         - текст комментария
 *
 * @return {JSON}
 * {bool} success
 */
router.put('/comment', (req, res, next) => {
    let text = req.body.text,
        id_comment = parseInt(req.body.id_comment);

    if (id_comment && text) {
        Comments
            .findOne({where: {id_comment: id_comment}})
            .then((comment) => {
                comment
                    .updateAttributes({
                        text: text,
                        changed: '1'
                    })
                    .then(() => {
                        res.send({success: true});
                    })
                    .catch(err => {
                        if (err.validateErrors) {
                            res.send({success: false, errors: {validation: err.validateErrors}});
                        } else {
                            next(err);
                        }
                    });
            })
            .catch(() => {
                let err = new Error('Не найден комментарий');
                err.status = 404;
                next(err);
            });
    } else {
        let err = new Error('Не переданы параметры для редактирования комментария');
        err.status = 400;
        next(err);
    }
});

/**
 * удаление/восстановление комментария(метка об удалении, не физическое удаление)
 * @param {int} id_comment  - ид комментария
 * @param {int} del         - удалить(1) или восстановить(0) комментарий
 *
 * @return {JSON}
 * {bool} success
 */
router.delete('/comment', (req, res, next) => {
    let id_comment = parseInt(req.body.id_comment),
        del = parseInt(req.body.del);

    if(del === undefined) del = 1;
    del = del.toString();

    if (id_comment) {
        Comments
            .findOne({where: {id_comment: id_comment}})
            .then(comment => {
                if (comment.deleted === del) {
                    res.send({success: true});
                } else {
                    comment
                        .updateAttributes({
                            deleted: del
                        })
                        .then(() => {
                            res.send({success: true});
                        })
                        .catch(err => {
                            if (err.validateErrors) {
                                res.send({success: false, errors: {validation: err.validateErrors}});
                            } else {
                                next(err);
                            }
                        });
                }
            })
            .catch(() => {
                let err = new Error('Не найден комментарий');
                err.status = 404;
                next(err);
            });
    } else {
        let err = new Error('Не передан ид комментария');
        err.status = 400;
        next(err);
    }
});

/**
 * рейтинг(лайки/дизлайки)
 * @param {int} id_user         - ид пользователя
 * @param {int} item_id         - ид комментируемой сущности
 * @param {int} action          - 1 - лайк, 0 - дизлайк
 *
 * @return {object}
 *      {bool} success
 * class всегда comments
 */
router.post('/comment_rating', (req, res, next) => {
    let rating = {
        id_user : parseInt(req.body.id_user),
        item_id : parseInt(req.body.item_id),
        action  : parseInt(req.body.action)
    },
    errors = [];

    for (let property in rating) {
        if (rating.hasOwnProperty(property)) {
            if(rating[property] === undefined || rating[property] === NaN) {
                errors.push(property);
            }
        }
    }

    if(errors.length) {
        res.send(errors);
    } else {
        Comments.sequelize.query(
            'SELECT `action`, `pk_ratings` FROM `ratings` WHERE `class` = "comments" AND `item_id` = $item_id AND `id_user` = $id_user',
            {bind: {
                item_id: rating.item_id,
                id_user: rating.id_user,
                action: rating.action
            }
            ,type: Comments.sequelize.QueryTypes.SELECT}
        ).then(rat => {
            rat = rat[0];

            if(rat === undefined) {//вставка
                Comments.sequelize.query(
                    'INSERT INTO `ratings` (`item_id`, `class`, `id_user`, `action`) ' +
                    'VALUES ($item_id, "comments", $id_user, $action)',
                    {bind: {
                        item_id: rating.item_id,
                        id_user: rating.id_user,
                        action: rating.action
                    }}
                )
                .then(() => {
                    getRatings(rating.item_id, rating.id_user).then((ratings) => {
                        res.send({ success: true, ratings: ratings.get(rating.item_id) });
                    });
                })
                .catch(err => {
                    next(err);
                });
            } else if(rat.action === rating.action) {//ничего
                getRatings(rating.item_id, rating.id_user).then((ratings) => {
                    res.send({ success: true, ratings: ratings.get(rating.item_id) });
                });
            } else {//обновление
                Comments.sequelize.query(
                    'UPDATE `ratings` SET `action`= $action WHERE `pk_ratings` = $pk',
                    {bind: {action: rating.action, pk: rat.pk_ratings}}
                    )
                    .then(() => {
                        getRatings(rating.item_id, rating.id_user).then((ratings) => {
                            res.send({ success: true, ratings: ratings.get(rating.item_id) });
                        });
                    })
                    .catch(err => {
                        next(err);
                    });
            }
        });
    }
});

module.exports = router;