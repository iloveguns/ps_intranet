let router = require('express').Router();

router.use(require('./user'));

router.use(require('./upload'));

router.use(require('./comment'));

router.use(require('./notification'));

module.exports = router;