'use strict;'
let router = require('express').Router(),
    jwt = require('jwt-simple'),
    User = require('../../models/mysql/user'),
    config = require('../../config');

/**
 * промежуточное по для проверки на доступ перед каждым действием в api
 */
router.get('/*', function (req, res, next) {
    console.log('промежуточное по');

    if(!req.headers['x-auth']) {
        let err = new Error('Нет заголовка авторизации');
        err.status = 401;
        next(err);
    }
    try {
        let auth = jwt.decode(req.headers['x-auth'], config.secretkey);

        if(auth === null) {
            let err = new Error('Невозможно расшифровать токен');
            err.status = 401;
            next(err);
        } else {
            User
                .findOne({attributes: ['login'], where: {login: auth.username}})
                .then((data) => {
                    if (data === null) {
                        let err = new Error('По токену не найден пользователь');
                        err.status = 401;
                        next(err);
                    } else {
                        //только в одном случае разрешать дальше
                        next();
                    }
                })
                .catch((err) => {
                    next(err);
                });
        }
    } catch (err) {
        next(err);
    }
});

module.exports = router;