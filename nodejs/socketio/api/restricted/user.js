'use strict;'
let router = require('express').Router(),
    User = require('../../models/mysql/user');

router.get('/user/:username', function (req, res, next){
    let username = req.params.username;

    User
        .findOne({ where: {login: username} })
        .then((data) => {
            if(data === null) {
                let err = new Error('Не найден пользователь "/user/:username"');
                err.status = 404;
                next(err);
            } else {
                res.send(data);
            }
        })
        .catch((err) => {
            next(err);
        });
});

module.exports = router;