let router = require('express').Router();

router.use(require('./checkAccess'));//проверка на доступ

router.use(require('./user'));

module.exports = router;