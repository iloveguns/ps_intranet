#!/usr/bin/env node

const app = require('../app'),
    appSocket = require('../appSocket'),
    cluster = require('cluster'),
    numCPUs = require('os').cpus().length,
    mysql = require('../db/mysql');

if (cluster.isMaster) {
    appSocket.start();

    //функции можно описать в файле и подключать и использовать в мастере
    for (let i = 0; i < numCPUs; i++) {
        startWorker();
    }

    cluster.on('exit', (worker, code, signal) => {
        startWorker();
    });

    // обнулить онлайн всех
    mysql.getSqlQuery('UPDATE `users_online` SET `now_online`=0 WHERE 1');
    
    // никто не редактирует сейчас, все обнулить
    mysql.getSqlQuery('TRUNCATE `sites_check_edit`');
} else if (cluster.isWorker) {
    app.start();
}

/**
 * функция для спавна воркеров и привязки параметров
 */
function startWorker() {
    const worker = cluster.fork();

    worker.on('message', (msg) => {
        /**
         * socket.io
         * пример из воркера:
         * process.send({ type: 'sendws', users: [...], method: 'notify', data: {...}});
         */
        if(msg.type === 'sendws') {
            if(msg.const === 'updateNotify') { //обновление (константа)
                appSocket.send(msg.users, 'notify', {updateNotify : true});
            } else {
                appSocket.send(msg.users, msg.method, msg.data);
            }
        }
        //worker.send('все норм');//отослать ответ воркеру
    });
}