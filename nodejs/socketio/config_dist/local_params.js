'use strict;'

/**
 * локальные параметры
 */
const getIp = '109.195.33.204';
const getPath = 'http://' + getIp + '/';


module.exports = {
    getPath: getPath,
    getIp: getIp
};