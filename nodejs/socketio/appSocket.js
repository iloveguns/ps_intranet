"use strict";

const mysql = require('./db/mysql'),
    Messages = require('./models/Messages');

mysql.formatBind();

let connectClients = new Map();//здесь хранится инфа о подключенных в INT (parseInt())
let iog;//глобально(баг)

/**
 * нужен сокет ~1.*.* , т.к. php клиента нет для >2.0.0
 */
module.exports.start = () => {
    const io = require('socket.io').listen(3001);
    iog = io;

    io.on('connection', (socket) => {
        socket.join('smg_public');

        //подключение клиента
        socket.on('connectListener', (data) => {
            atConnectListener(socket, data);
        });

        //отключение клиента
        socket.on('disconnect', () => {
            atDisconnect(socket);
        });

        socket.on('showOnline', () => {
            let sendArr = [];
            
            connectClients.forEach((k,v) => {
                sendArr.push({id_user:v, count:k.countconnections});
            });
            
            socket.emit('showOnline',  sendArr);
        });

        /**
         * сообщения
         */

        /**
         * отправка уведомлений сотрудникам без сохранения
         * socket.emit('notify', {'uniqid':[3,1], updateNotify: true});
         * @param {bool} updateNotify - обновить данные с сервера(уведомления)
         */
        socket.on('notify', (data) => {
            module.exports.send(data.uniqid, 'notify', {updateNotify : data.updateNotify});
        });

        /**
         * получение списка диалогов по ид пользователя
         *
         * @see Messages.dialogsList
         */
        socket.on('dialogsList', () => {
            Messages.dialogsList(socket.id_user)
                .then(data => {
                    socket.emit('dialogsList',  data);
                })
                .catch(() => {
                    socket.emit('dialogsList',  {success: false});
                });
        });

        /**
         * получение сообщений из диалога
         *
         * @see Messages.getMessagesDialog
         */
        socket.on('getMessagesDialog', (data) => {
            Messages.getMessagesDialog(socket.id_user, data.id_dialog, data.limit, data.offset)
                .then(messages => {
                    socket.emit('getMessagesDialog',  messages);
                })
                .catch(() => {
                    socket.emit('getMessagesDialog',  {success: false});
                });
        });

        /**
         * создание сообщения в беседу
         *
         * @see Messages.createMessage
         */
        socket.on('createMessage', (data) => {
            Messages.createMessage(socket.id_user, data.id_dialog, data.text)
                .then(message => {
                    module.exports.send(message.usersArr, 'hasNewMessage', {message: message.message, id_dialog: message.id_dialog});
                })
                .catch(() => {
                    socket.emit('createMessage',  {success: false});
                });
        });

        /**
         * чтение сообщения
         *
         * @see Messages.readMessage
         */
        socket.on('readMessage', (data) => {
            Messages.readMessage(data.id, socket.id_user)
                .then(res => {
                    if(res.success === true) {
                        if(res.usersArr.length) {
                            /** всем разослать о чтении */
                            module.exports.send(res.usersArr, 'readedMessage', {id: data.id, id_dialog: res.id_dialog});
                        }
                        socket.emit('readMessage',  {id: data.id});
                    } else {
                        socket.emit('readMessage',  {success: false});
                    }
                })
                .catch(() => {
                    socket.emit('readnMessage',  {success: false});
                });
        });

        /**
         * поиск контактов
         *
         * @see Messages.searchContacts
         */
        socket.on('searchContacts', (data) => {
            Messages.searchContacts(data.q)
                .then(response => {
                    socket.emit('searchContacts',  {data: response});
                })
                .catch(err => {

                });
        });

        /**
         * проверка перед созданием диалога
         *
         * @see Messages.existDialog
         */
        socket.on('existDialog', (data) => {
            Messages.existDialog(data.users)
                .then(response => {
                    socket.emit('existDialog',  {success: response, users: data.users});
                })
                .catch(() => {
                    socket.emit('existDialog',  {success: true});
                });
        });

        /**
         * создание диалога, отослать всему участникам
         *
         * @see Messages.createDialog
         */
        socket.on('createDialog', (data) => {
            Messages.createDialog(socket.id_user, data.users)
                .then(response => {
                    module.exports.send(response.users, 'createDialog', {
                        success: response.success,
                        infoUsers: response.infoUsers,
                        pk_dialog: response.pk_dialog,
                        users: response.users
                    });
                })
                .catch(err => {

                });
        });
        
        /**
         * империя сайтов
         * проверить, занял ли кто контент, и сохранить если кто что редактирует
         * себя можно пускать, проверка на других пользователей
         */
        socket.on('sitesOpenedContent', ({uniqid, pk_content, rand}) => {
            mysql.
                getSqlQuery('SELECT `fk_user`, `lastname`, `name` FROM `sites_check_edit` LEFT JOIN `user` ON `fk_user` = `id_user` WHERE `fk_user` != :fk_user AND `fk_content` = :fk_content;',
                    { fk_user: uniqid, fk_content: pk_content })
                .then(data => {
                    socket.emit('sitesHasOpenedContent',  {
                        has     : true,
                        fk_user : data[0].fk_user,
                        lastname: data[0].lastname,
                        name    : data[0].name
                    });
                })
                .catch(err => {
                    // внести если никого нет
                    if(err === 'empty sql query') {
                        socket.emit('sitesHasOpenedContent',  { has: false });
                        
                        mysql.
                            getSqlQuery('INSERT INTO `sites_check_edit` (`fk_user`, `fk_content`, `rand`) VALUES (:fk_user, :fk_content, :rand);',
                            { fk_user: uniqid, fk_content: pk_content, rand });
                    }
                });
        });
        
        /**
         * империя сайтов
         * больше не сохранять в сокете кто что редактирует - человек ушел из контента
         */
        socket.on('sitesClosedContent', ({uniqid, pk_content, rand}) => {
            mysql.
                getSqlQuery('DELETE FROM `sites_check_edit` WHERE `fk_user` = :fk_user AND `fk_content` = :fk_content AND `rand` = :rand;',
                { fk_user: uniqid, fk_content: pk_content, rand });
        });
    });
};

/**
 * отправка через вебсокет данных
 * @param {array|int} users - массив пользователей для отправки
 * @param {string} method
 * @param {string} data
 */
module.exports.send = (users, method, data) => {
    //преобразование к массиву
    if(Array.isArray(users) === false) {
        users = [users];
    }

    for(let i = 0; i < users.length; i++) {
        //десятичное значение пользователя
        let user = connectClients.get(parseInt(users[i], 10));

        //если найдено - отправить на все подключенные соединения
        if(user !== undefined){
            user.sockets.forEach((socketid) => {
                iog.sockets.connected[socketid].emit(method, data);
            });
        }
    }
};

/**
 * при входе проверка и регистрация клиента
 * @param {socket.io} socket
 * @data {object} с клиента
 */
function atConnectListener(socket, data){
    if(!data.uniqid) data.uniqid = data.uniqhash; //сообщения
    
    //проверка на необходимые параметры
    if (!data.uniqid) {
        socket.emit('connectStatus', 'error');
    } else {
        socket.id_user = parseInt(data.uniqid, 10);//должен храниться в int

        if(connectClients.get(socket.id_user) === undefined){//еще не подсоединен
            //проверка ид на неуволенность
            mysql
                .getSqlQuery('SELECT `name` FROM `user` LEFT JOIN `user_status` ON user.fk_status = user_status.pk_status WHERE `id_user` = :id_user AND user_status.status != :fired',
                    {
                        id_user: socket.id_user,
                        fired: 9
                    })
                .then(() => {
                    //создать пользователя в массиве
                    connectClients.set(socket.id_user, {sockets : new Map(), countconnections : 0})

                    afterConnectListener(socket);
                }).then(() => {
                    //вставить даннные об онлайне
                    mysql.getSqlQuery('INSERT INTO `users_online` (`pk_user`, `now_online`) VALUES (:id_user, 1) ON DUPLICATE KEY UPDATE `last_online` = DEFAULT, `now_online` = 1', { id_user: socket.id_user });
                }).catch((err) => {
                    console.log(err);
                });
        } else {
            //уже подсоединен. добавить сокет в коллекцию сокетов пользователя. можно и не ждать синхронности
            afterConnectListener(socket, data);
        }
    }
}

/**
 * действия, после входа на сервер(аутентификация сайт)
 * @param {socket.io} socket
 */
function afterConnectListener(socket) {
    let user = connectClients.get(parseInt(socket.id_user, 10));

    user.countconnections = user.countconnections + 1;
    user.sockets.set(socket.id, socket.id);

    socket.emit('connectStatus', 'ok');
}

/**
 * выход клиента из сокета
 * @param {socket.io} socket
 */
function atDisconnect(socket){
    if(!socket.id_user) return false;

    let user = connectClients.get(parseInt(socket.id_user, 10));

    if(user){
        if(user.countconnections <= 1) {
            mysql.getSqlQuery('INSERT INTO `users_online` (`pk_user`, `now_online`) VALUES (:id_user, 0) ON DUPLICATE KEY UPDATE `last_online` = DEFAULT, `now_online` = 0', { id_user: socket.id_user });
            connectClients.delete(parseInt(socket.id_user));
        } else {
            user.sockets.delete(socket.id);
            user.countconnections = user.countconnections - 1;
        }
    }
}