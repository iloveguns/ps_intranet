const path = require('path');
const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');//минификация js

module.exports = {
    context: path.resolve(__dirname, 'src'),
    entry: {
        comments: './Comments.js',
        messages: './Messages.js'
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, '../../web/react')
    },
    watchOptions: {
        aggregateTimeout: 100
    },
    plugins: [
        new webpack.DefinePlugin({//для минификации реакт
            'process.env': {//для реакта
                NODE_ENV: JSON.stringify('production')
            }
        }),
        new UglifyJSPlugin({
            exclude: /(node_modules|bower_components)/,
            extractComments: true,
            beautify: false,
            comments: false,
            compress: {
                sequences     : true,
                booleans      : true,
                loops         : true,
                unused      : true,
                warnings    : false,
                drop_console: true
            }
        }),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.ProvidePlugin({//глобальные переменные
            'React': 'react',
            'ReactDOM': 'react-dom',
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: function (module) {
               return module.context && module.context.indexOf('node_modules') !== -1;
            }
        })
    ],
    module: {
        rules: [
        {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['es2015', 'stage-0', 'react']
                }
            }
        }
        ]
    }
};