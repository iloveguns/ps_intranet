import {
    MODAL_TOGGLE_SEARCH,
    MODAL_TOGGLE_PROFILE,
    MODAL_SEARCH_ITEMS,
    OPEN_HEADER_MENU,
    INFOUSERS_UPDATE_USER
} from '../constants';
import { searchContacts as searchContactsApi } from '../api';

/**
 * открыть/закрыть окно поиска
 */
export function toggleModalSearch() {
    return (dispatch) => {
        dispatch({
            type: MODAL_TOGGLE_SEARCH
        });
    };
}

/**
 * открыть/закрыть окно профиля
 */
export function toggleModalProfile() {
    return (dispatch) => {
        dispatch({
            type: MODAL_TOGGLE_PROFILE
        });
    };
}

export function openHeader() {
    return (dispatch) => {
        dispatch({
            type: OPEN_HEADER_MENU
        });
    };
}

/**
 * начать поиск по контактам, передать в api
 *
 * @param {String} text
 */
export function searchContacts(text) {
    return (dispatch) => {
        searchContactsApi(text);
    }
}

/**
 * слушатель на поиск контактов
 *
 * @param {Object} data - ответ сервера
 */
export function onSearchContacts(data) {
    return (dispatch) => {
        dispatch({
            type: MODAL_SEARCH_ITEMS,
            items: data.data
        });
    };
}

/**
 * слушатель на редактирование профиля
 *
 * @param {Object} data - ответ сервера
 */
export function onSaveProfile(data) {
    return (dispatch) => {
        if(data.success === true) {
            dispatch({ // закрыть модаль
                type: MODAL_TOGGLE_PROFILE
            });

            dispatch({ // внести данные
                type: INFOUSERS_UPDATE_USER,
                data
            });
        } else {

        }
    }
}