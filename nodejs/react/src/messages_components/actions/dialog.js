import {  getMessagesDialogIo } from '../api';
import {
    DIALOG_IS_LOADING,
    DIALOG_FETCH_DATA_SUCCESS,
    DIALOG_HAS_ERRORS,
    DIALOG_INCREMENT_OFFSET,
    DIALOG_HAS_LOAD_MESSAGES,
    DIALOG_NEW_MESSAGE,
    DIALOG_READED_MESSAGE
} from '../constants';

export const INCREMENT_OFFSET = 15;

/**
 * @param {boolean} bool - параметр загрузки
 */
export function isLoading (bool) {
    return {
        type: DIALOG_IS_LOADING,
        isLoading: bool
    };
}

/**
 * @param {boolean} json - результат ответа
 */
export function fetchDataSuccess (json) {
    return {
        type  : DIALOG_FETCH_DATA_SUCCESS,
        dialog_items : json
    };
}

/**
 * @param {boolean} err - параметр загрузки
 */
export function hasErrors (err) {
    return {
        type: DIALOG_HAS_ERRORS,
        errors: err
    };
}

/**
 */
export function incrOffset (offset) {
    return {
        type: DIALOG_INCREMENT_OFFSET,
        offset: offset
    };
}

/**
 * @param {boolean} bool - есть ли еще сообщения
 */
export function hasLoadMessages (bool) {
    return {
        type: DIALOG_HAS_LOAD_MESSAGES,
        hasLoadMessages: bool
    };
}

export function newMessage(message, id_dialog) {
    return {
        type: DIALOG_NEW_MESSAGE,
        message,
        id_dialog
    }
}


/**
 * получить начальный список диалогов
 * подгрузка сообщений
 */
export function loadMore(id_dialog, offset) {
    return (dispatch) => {
        dispatch(isLoading(true));

        getMessagesDialogIo(id_dialog, offset);
    };
}

/**
 * слушатель на новые сообщения
 * @param {Object} message
 */
export function onHasNewMessage(message) {
    return (dispatch) => {
        dispatch(newMessage(message.message, message.id_dialog));
    };
}

/**
 * слушатель на прочтение сообщения
 * @param {Object} data
 */
export function onReadedMessage(data) {
    return (dispatch) => {
        dispatch({
            type: DIALOG_READED_MESSAGE,
            id_dialog: data.id_dialog,
            id: data.id
        });
    };
}

/**
 * слушатель на загрузку сообщений в диалоге
 * @param {Object} data
 */
export function onGetMessagesDialog(data) {
    return (dispatch) => {
        if (data.success !== undefined && data.success === false) {
            dispatch(isLoading(false));
            dispatch(hasLoadMessages(false));
            dispatch(hasErrors(true));
        } else if(data === 'empty') {
            dispatch(isLoading(false));
        } else {
            dispatch(incrOffset(INCREMENT_OFFSET));
            dispatch(isLoading(false));

            dispatch(fetchDataSuccess(data));

            if (data.length < INCREMENT_OFFSET) { //последние сообщения
                dispatch(hasLoadMessages(false));
            }
        }
    };
}
