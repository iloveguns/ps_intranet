import {
    MESSAGE_IS_SENDING,
    MESSAGE_HAS_ERRORS,
    MESSAGE_FETCH_DATA_SUCCESS
} from '../constants';

/**
 * @param {Boolean} bool - отправляется ли
 * @param {int} id_dialog - ид диалога
 */
export function isSending (bool, id_dialog) {
    return {
        type: MESSAGE_IS_SENDING,
        isSending: bool,
        id_dialog: id_dialog
    };
}

/**
 * @param {boolean} err - параметр загрузки
 */
export function hasErrors (err) {
    return {
        type: MESSAGE_HAS_ERRORS,
        errors: err
    };
}

/**
 * @param {boolean} json - результат ответа
 */
export function fetchDataSuccess (json) {
    return {
        type  : MESSAGE_FETCH_DATA_SUCCESS,
        id_message : json
    };
}