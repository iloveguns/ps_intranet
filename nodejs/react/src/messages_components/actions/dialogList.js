import { getDialogsUserIo } from '../api';
import {
    DIALOGLIST_IS_LOADING,
    DIALOGLIST_FETCH_DATA_SUCCESS,
    DIALOGLIST_HAS_ERRORS,
    FETCH_INFO_USERS,
    DIALOGLIST_CREATED,
    DIALOGLIST_NOT_CREATED
} from '../constants';

/**
 * @param {boolean} bool - параметр загрузки
 */
export function isLoading (bool) {
    return {
        type: DIALOGLIST_IS_LOADING,
        isLoading: bool
    };
}

/**
 * @param {boolean} json - параметр загрузки
 */
export function fetchDataSuccess (json) {
    return {
        type  : DIALOGLIST_FETCH_DATA_SUCCESS,
        items : json
    };
}

/**
 * @param {boolean} err - параметр загрузки
 */
export function hasErrors (err) {
    return {
        type: DIALOGLIST_HAS_ERRORS,
        errors: err
    };
}

/**
 *
 */
export function fetchInfoUsers (json) {
    return {
        type  : FETCH_INFO_USERS,
        items : json
    };
}

/**
 * получить начальный список диалогов
 */
export function initDialogList() {
    return (dispatch) => {
        dispatch(isLoading(true));

        getDialogsUserIo()
            .then(json => {
                dispatch(isLoading(false));

                dispatch(fetchInfoUsers(json.data.infoUsers));

                if(json.success === true) {
                    dispatch(fetchDataSuccess(json.data.dialogList));
                }
            })
            .catch(() => {
                console.log('errors');
                dispatch(hasErrors(true));
            });
    }
}

/**
 * слушатель на создание диалога
 *
 * @param {Object} data - ответ сервера
 */
export function onCreateDialog(data) {
    return (dispatch) => {
        if(data.success === true) {
            dispatch({
                type: DIALOGLIST_CREATED,
                infoUsers: data.infoUsers,
                pk_dialog: data.pk_dialog,
                users: data.users
            });
        } else {
            dispatch({
                type: DIALOGLIST_NOT_CREATED
            });
        }
    };
}