export const DIALOGLIST_IS_LOADING = 'DIALOGLIST_IS_LOADING';
export const DIALOGLIST_HAS_ERRORS = 'DIALOGLIST_HAS_ERRORS';
export const DIALOGLIST_FETCH_DATA_SUCCESS = 'DIALOGLIST_FETCH_DATA_SUCCESS';
export const DIALOGLIST_CREATED = 'DIALOGLIST_CREATED';
export const DIALOGLIST_NOT_CREATED = 'DIALOGLIST_NOT_CREATED';

export const DIALOG_IS_LOADING = 'DIALOG_IS_LOADING';
export const DIALOG_FETCH_DATA_SUCCESS = 'DIALOG_FETCH_DATA_SUCCESS';
export const DIALOG_HAS_ERRORS = 'DIALOG_HAS_ERRORS';
export const DIALOG_INCREMENT_OFFSET = 'DIALOG_INCREMENT_OFFSET';
export const DIALOG_HAS_LOAD_MESSAGES = 'DIALOG_HAS_LOAD_MESSAGES';
export const DIALOG_NEW_MESSAGE = 'DIALOG_NEW_MESSAGE';
export const DIALOG_READED_MESSAGE = 'DIALOG_READED_MESSAGE';

export const FETCH_INFO_USERS = 'FETCH_INFO_USERS';
export const INFOUSERS_UPDATE_USER = 'INFOUSERS_UPDATE_USER';

export const ROUTER_CHANGE = '@@router/LOCATION_CHANGE'; //при смене значения, роутер

export const MESSAGE_IS_SENDING = 'MESSAGE_IS_SENDING';
export const MESSAGE_HAS_ERRORS = 'MESSAGE_HAS_ERRORS';
export const MESSAGE_FETCH_DATA_SUCCESS = 'MESSAGE_FETCH_DATA_SUCCESS';

export const MODAL_TOGGLE_SEARCH = 'MODAL_TOGGLE_SEARCH';
export const MODAL_TOGGLE_PROFILE = 'MODAL_TOGGLE_PROFILE';
export const MODAL_SEARCH_ITEMS = 'MODAL_SEARCH_ITEMS';

export const OPEN_HEADER_MENU = 'OPEN_HEADER_MENU';