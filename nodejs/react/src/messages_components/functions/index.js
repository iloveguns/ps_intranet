import moment from 'moment';

/**
 * подготовить фото с кем диалог(один/несколько)
 * @param {Map} infoUsers   - инфа о участниках
 * @param {Array} users     - участники диалога
 * @param {int} im          - ид себя(для исключения) (необязательный)
 * @returns {String}
 */
export function prepareReceiverImg(infoUsers, users, im = null) {
    if(users === undefined) {
        return '';
    } else {
        /** cast to array */
        if(Array.isArray(users) === false) {
            let tmp = [];
            tmp.push(users);
            users = tmp.slice();
        }

        let avatar, name, names = [], online;

        for(let i = 0; i < users.length; i++) {
            /** себя пропустить */
            if(im !== null) {
                if (im === users[i] && users.length !== 1) continue;
            }

            let infoUser = infoUsers.get(users[i]);

            /** если не нашлось - пропустить */
            if(infoUser === undefined) continue;

            name = `${infoUser.lastname} ${infoUser.name}`;
            names.push(name);

            online = infoUser.online;

            if(infoUser.avatar && infoUser.avatar.length) {
                avatar = window.ROOT_URL + infoUser.avatar;
            } else {
                avatar = "http://via.placeholder.com/48x48";
            }
        }

        return {
            avatar,
            name,
            names:names.join(', '),
            online
        };
    }
}

/**
 * форматирование времени
 * @param {Date} time
 */
export function formatTime(time) {
    if(moment(time).diff(new Date().getTime(), 'days') === 0) { //сегодня - время
        return moment(time).format('LT');
    } else { //в прошлом - дату
        return moment(time).format('L');
    }
}

/**
 * проверка на url
 *
 * @param {String} string  - строка
 * @param {String} endLine - проверка конца строки и переноса на после ссылки
 *
 * @test
 это текст https://www.google.ru/
 **** https://github.com/ ***
 https://i.ytimg.com/vi/cbprfcSVcyQ/hqdefault.jpg
 1 2 3
 4
 */
export function findUrlReplaceLink(string, endLine = '<br>'){
    const matcher = /^(?:\w+:)?\/\/([^\s\.]+\.\S{2}|localhost[\:?\d]*)\S*$/;

    let narr = string.split(' '),
        textUrl = [];

    for(let i = 0; i < narr.length; i++) {
        let count = narr[i].match(new RegExp(endLine,"gi"));

        if(count === null) {
            textUrl.push(narr[i]);
        } else {
            let endlsplit = narr[i].split(endLine);

            for(let j = 0; j < endlsplit.length; j++) {
                if(j !== endlsplit.length - 1) {
                    endlsplit[j] += endLine;
                }

                textUrl.push(endlsplit[j]);
            }
        }
    }

    for(let i = 0; i < textUrl.length; i++) {
        /** если совпадает - делать ссылку */
        if(matcher.test(textUrl[i])) {
            if (textUrl[i].endsWith(endLine)) {
                textUrl[i] = textUrl[i].replace(endLine, '');

                textUrl[i] = createLink(textUrl[i]);

                textUrl[i] += endLine;
            } else {
                textUrl[i] = createLink(textUrl[i]);
            }
        }
    }

    return textUrl.join(' ').replace(new RegExp(endLine + ' ', 'g'), endLine);
}

/**
 * создание текстовой ссылки или img ссылки
 *
 * @param {String} str - входящий текст
 * @returns {string}
 */
function createLink(str) {
    const imgMatcher = /\.(jpeg|jpg|gif|png|svg)\S*$/;
    let content = '';

    /** если совпадает - делать ссылку на изображение*/
    if(imgMatcher.test(str)) {
        content = `<img src="${str}">`;
    } else {
        content = str;
    }

    return `<a href="${str}">${content}</a>`;
}

/**
 * Собственный ид, хранящийся где-то
 *
 * @returns {Number}
 */
export function getUniqId() {
    return parseInt(localStorage.getItem('uniqid'), 10);
}

/**
 * хэш пароля, хранящийся где-то
 *
 * @returns {String}
 */
export function getUniqHash() {
    return localStorage.getItem('uniqhash');
}