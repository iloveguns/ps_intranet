import { INCREMENT_OFFSET } from '../actions/dialog';

//соединение с сокетом
export const socket = window.io.connect(window.SOCKET_URL);
socket.on('connect', () => {
    socket.emit('connectListener', {'uniqhash':localStorage.getItem('uniqhash')});
});

/**
 * ответ после идентификации на сервере
 * перенести в самый верх, отталкиваться отсюда для получения данных
 */
socket.on('connectStatus', (res) => {
    /** не идентифицирован - удалить данные, ожидать входа в систему пользователем */
    if(res === 'error') {
        localStorage.removeItem('uniqhash');
        localStorage.removeItem('uniqid');
    }
});

/**
 * регистрация, отправка данных
 *
 * @param {String} login    - логин
 * @param {String} password - пароль
 */
export function register(login, password) {
    return new Promise((resolve, reject) => {
        socket.emit('register', {login, password});

        socket.on('register', (data) => {
            socket.removeListener('register');

            if(data.success === false) {
                reject();
            } else {
                resolve({password: data.password, id: data.id});
            }
        });
    });
}

/**
 * вход, отправка данных
 *
 * @param {String} login    - логин
 * @param {String} password - пароль
 */
export function log_in(login, password) {
    return new Promise((resolve, reject) => {
        socket.emit('log_in', {login, password});

        socket.on('log_in', (data) => {
            socket.removeListener('log_in');

            if(data.success === false) {
                reject();
            } else {
                resolve({password: data.password, id: data.id});
            }
        });
    });
}

/**
 * получение списка диалогов(бесед)
 *
 * @returns {Promise}
 */
export function getDialogsUserIo() {
    return new Promise((resolve, reject) => {
        socket.emit('dialogsList');

        socket.on('dialogsList', (data) => {
            socket.removeListener('dialogsList');
            if(data.success !== undefined && data.success === false) {
                reject();
            } else if(data.success !== undefined && data.success === 'empty') {
                /** как минимум о себе информация есть */
                let infoUsers = new Map();

                infoUsers.set(data.infoUsers.id_user, {
                    avatar: data.infoUsers.avatar,
                    lastname: data.infoUsers.lastname,
                    name: data.infoUsers.name
                });

                resolve({success: false, data: {infoUsers: infoUsers}});
            } else {
                /** собрать инфу о пользователях в Map */
                let infoUsers = new Map();

                for (let i = 0; i < data.infoUsers.length; i++) {
                    infoUsers.set(data.infoUsers[i].id_user, {
                        avatar: data.infoUsers[i].avatar,
                        lastname: data.infoUsers[i].lastname,
                        name: data.infoUsers[i].name
                    });
                }
                data.infoUsers = infoUsers;

                resolve({success: true, data});
            }
        });
    });
}

/**
 * получение сообщений по ид диалога
 *
 * @param {int} id_dialog
 * @param {int} offset
 */
export function getMessagesDialogIo(id_dialog, offset) {
    socket.emit('getMessagesDialog', { id_dialog: id_dialog, limit: INCREMENT_OFFSET, offset: offset });
}

/**
 * отправка сообщения
 *
 * @param {String} text - текст
 * @param {int} id_dialog - ид диалога
 */
export function createMessage(text, id_dialog) {
    socket.emit('createMessage', { text: text, id_dialog: id_dialog });
}

/**
 * чтение сообщения
 *
 * @param {int} id_message - ид сообщения
 */
export function readMessage(id_message) {
    socket.emit('readMessage', {id: id_message});
}

/**
 * поиск контактов
 *
 * @param {String} text - поисковый запрос
 */
export function searchContacts(text) {
    socket.emit('searchContacts', {q: text});
}

/**
 * создание диалога
 *
 * @param {array} users     - массив пользователей, с которыми создается беседа(даже если 1) *
 */
export function createDialog(users) {
    socket.emit('createDialog', {users});
}

/**
 * сохранение данных профиля
 *
 * @param {String} lastname - фамилия
 * @param {String} name     - имя
 */
export function saveProfile(lastname, name) {
    socket.emit('saveProfile', {lastname, name});
}

/**
 * скрытая функция просмотра онлайна в сокете
 * document.querySelector('.messages-text_wrap > span').click()
 */
export function showOnline() {
    socket.emit('showOnline');
}