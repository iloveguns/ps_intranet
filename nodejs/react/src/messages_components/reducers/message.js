import {
    MESSAGE_IS_SENDING,
    MESSAGE_HAS_ERRORS,
    MESSAGE_FETCH_DATA_SUCCESS
} from '../constants';

const initialState = {
    id_message: null, //
    isSending: false, //состояние отправки
    has_errors: false, //ошибки
    id_dialog: null //ид диалога
};

export default function message(state = initialState, action) {
    switch (action.type) {
        case MESSAGE_IS_SENDING:
            return {
                ...state,
                isSending: action.isSending,
                id_dialog: action.id_dialog
            };

        case MESSAGE_HAS_ERRORS:
            return {
                ...state,
                has_errors: action.errors
            };

        case MESSAGE_FETCH_DATA_SUCCESS:
            return {
                ...state,
                id_message: action.id_message
            };

        default:
            return state;
    }
}
