import {
    ROUTER_CHANGE,
    DIALOG_IS_LOADING,
    DIALOG_FETCH_DATA_SUCCESS,
    DIALOG_HAS_ERRORS,
    DIALOG_INCREMENT_OFFSET,
    DIALOG_HAS_LOAD_MESSAGES,
    DIALOG_NEW_MESSAGE,
    DIALOG_READED_MESSAGE,
    DIALOGLIST_CREATED
} from '../constants';
import { cloneDeep } from 'lodash';

const initialState = {
    dialog_items : [], //сообщения в диалоге
    dialog_active : null, //активный диалог
    dialog_is_loading : false, //состояние загрузки
    dialog_has_errors : false, //есть ли ошибки
    dialog_offset : 0, //отступ загрузки сообщений
    dialog_has_load_messages : true //есть ли еще сообщения для загрузки
};

export default function dialog(state = initialState, action) {
    switch (action.type) {
        case ROUTER_CHANGE:
            /** почти начальные значения */
            if(state.dialog_active !== parseInt(action.payload.hash.substr(1), 10)) {
                return {
                    ...state,
                    dialog_active: parseInt(action.payload.hash.substr(1), 10),
                    dialog_offset: 0,
                    dialog_items: [],
                    dialog_has_load_messages: true,
                    dialog_has_errors: false
                };
            }

            return state;

        case DIALOG_IS_LOADING:
            return {
                ...state,
                dialog_is_loading: action.isLoading
            };

        case DIALOG_FETCH_DATA_SUCCESS:
            let s = state.dialog_items.slice();

            for (let i = 0; i < action.dialog_items.length; i++) {
                s.unshift(action.dialog_items[i]);
            }

            return {
                ...state,
                dialog_items: s
            };

        case DIALOG_HAS_ERRORS:
            return {
                ...state,
                dialog_has_errors: action.errors
            };

        case DIALOG_INCREMENT_OFFSET:
            return {
                ...state,
                dialog_offset: state.dialog_offset + action.offset
            };

        case DIALOG_HAS_LOAD_MESSAGES:
            return {
                ...state,
                dialog_has_load_messages: action.hasLoadMessages
            };

        case DIALOG_NEW_MESSAGE:
            /** добавлять в сообщения только если этот диалог активен */
            if(action.id_dialog === state.dialog_active) {
                let newItems = state.dialog_items.slice();
                let no = state.dialog_offset;
                newItems.push(action.message);

                return {
                    ...state,
                    dialog_items: newItems,
                    dialog_offset: no + 1
                }
            }

            return state;

        case DIALOG_READED_MESSAGE:
            if(action.id_dialog === state.dialog_active) {
                let newItems = state.dialog_items.slice();

                for(let i = 0; i< state.dialog_items.length; i++) {
                    if(state.dialog_items[i].pk_message === action.id) {
                        let iml = cloneDeep(state.dialog_items[i]);
                        iml.readed = 1;
                        newItems[i] = iml;
                        break;
                    }
                }

                return {
                    ...state,
                    dialog_items: newItems
                }
            }

            return state;

        case DIALOGLIST_CREATED:
            /** почти начальные значения */
            return {
                ...state,
                dialog_active: action.pk_dialog,
                dialog_offset: 0,
                dialog_items: [],
                dialog_has_load_messages: true,
                dialog_has_errors: false
            };

        default:
            return state;
    }
}
