import {
    MODAL_TOGGLE_SEARCH,
    MODAL_TOGGLE_PROFILE,
    MODAL_SEARCH_ITEMS,
    DIALOGLIST_CREATED,
    OPEN_HEADER_MENU
} from '../constants';

const initialState = {
    searchItems: [], //данные поиска контактов
    isOpenHeader: false, //открыто ли левое верхнее меню
    isOpenSearch: false, //открыт ли поиск контактов
    isOpenProfile: false //открыт ли профиль
};

export default function modal(state = initialState, action) {
    switch (action.type) {
        case MODAL_TOGGLE_SEARCH:
            return {
                ...state,
                isOpenSearch: !state.isOpenSearch
            };

        case MODAL_TOGGLE_PROFILE:
            return {
                ...state,
                isOpenProfile: !state.isOpenProfile
            };

        case MODAL_SEARCH_ITEMS:
            return {
                ...state,
                searchItems: action.items
            };

        case DIALOGLIST_CREATED:
            return {
                ...state,
                isOpenSearch: false
            };

        case OPEN_HEADER_MENU:
            return {
                ...state,
                isOpenHeader: !state.isOpenHeader
            };

        default:
            return state;
    }
}
