import { combineReducers } from 'redux';
import dialogsList from './dialogList';
import infoUsers from './infoUsers';
import dialog from './dialog';
import message from './message';
import modal from './modal';
//import { routerReducer} from 'react-router-redux';

export default combineReducers({
    dialogsList,
    dialog,
    infoUsers,
    message,
    modal
    //router: routerReducer //данные роутера
});