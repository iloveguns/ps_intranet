import {
    ROUTER_CHANGE,
    DIALOGLIST_IS_LOADING,
    DIALOGLIST_HAS_ERRORS,
    DIALOGLIST_FETCH_DATA_SUCCESS,
    DIALOG_NEW_MESSAGE,
    DIALOG_READED_MESSAGE,
    DIALOGLIST_CREATED
} from '../constants';
import { cloneDeep } from 'lodash';
import { getUniqId } from '../functions';

const initialState = {
    item_active      : null, //активный диалог(копия dialog)
    mapitems         : new Map(), //new
    items_users      : new Map(), //надо убрать, есть mapitems.users
    im               : getUniqId(), //ид себя
    items_is_loading : false, //состояние загрузки
    items_has_errors : false //есть ли ошибки
};

export default function dialogsList(state = initialState, action) {
    switch (action.type) {
        case ROUTER_CHANGE:
            return {
                ...state,
                item_active: parseInt(action.payload.hash.substr(1), 10)
            };

        case DIALOGLIST_FETCH_DATA_SUCCESS:
            let s = state.items_users;
            let m = cloneDeep(state.mapitems);

            for (let i = 0; i < action.items.length; i++) {
                s.set(action.items[i].pk_dialog, action.items[i].users);
                m.set(action.items[i].pk_dialog, action.items[i]);
            }

            return {
                ...state,
                items_users: s,
                mapitems: m
            };

        case DIALOGLIST_IS_LOADING:
            return {
                ...state,
                items_is_loading: action.isLoading
            };

        case DIALOGLIST_HAS_ERRORS:
            return {
                ...state,
                items_has_errors: action.errors
            };

        case DIALOG_NEW_MESSAGE:
            let nstate = cloneDeep(state.mapitems);
            let dialog = nstate.get(action.id_dialog);

            /** на свои сообщения не увеличивать */
            if(action.message.fk_user_sender !== initialState.im) {
                dialog.unread++;
            }

            dialog.message = action.message;

            /** вырезать и добавить в самый верх списка */
            let nm = new Map().set(dialog.pk_dialog, dialog);
            nstate.delete(dialog.pk_dialog);
            nstate.forEach((v, k) => {
                nm.set(k, v);
            });

            return {
                ...state,
                mapitems: nm
            };

        case DIALOG_READED_MESSAGE:
            if(action.id_dialog === state.item_active) {
                let nstate = cloneDeep(state.mapitems);
                nstate.get(action.id_dialog).unread--;

                return {
                    ...state,
                    mapitems: nstate
                };
            }

            return state;

        case DIALOGLIST_CREATED:
            let nmstate = cloneDeep(state.mapitems);

            /** добавить новый и толкнуть вверх */
            let nmn = new Map().set(action.pk_dialog, {
                users: action.users,
                pk_dialog: action.pk_dialog,
                message: null
            });
            nmstate.forEach((v, k) => {
                nmn.set(k, v);
            });

            return {
                ...state,
                mapitems: nmn,
            };

        default:
            return state;
    }
}
