import {
    FETCH_INFO_USERS,
    DIALOGLIST_CREATED,
    INFOUSERS_UPDATE_USER
} from '../constants';
import { cloneDeep } from 'lodash';

const initialState = new Map();

/**
 * хранение информации о пользователях чата
 * @param {String} avatar
 * @param {String} lastname
 * @param {String} name
 * @param {int} online
 */
export default function infoUsers(state = initialState, action) {
    switch (action.type) {
        case FETCH_INFO_USERS:
            return action.items;

        case DIALOGLIST_CREATED:
            let nstate = cloneDeep(state);

            /** добавить инфу о новых участниках */
            for(let i = 0; i < action.infoUsers.length; i++) {
                nstate.set(action.infoUsers[i].id_user, {
                    avatar: action.infoUsers[i].avatar,
                    lastname: action.infoUsers[i].lastname,
                    name: action.infoUsers[i].name,
                    online: action.infoUsers[i].online
                });
            }

            return nstate;

        /** обновление информации и пользователе */
        case INFOUSERS_UPDATE_USER:
            let nestate = cloneDeep(state);

            let a = nestate.get(action.data.id_user);
            a.lastname = action.data.lastname;
            a.name = action.data.name;

            return nestate;

        default:
            return state;
    }
}
