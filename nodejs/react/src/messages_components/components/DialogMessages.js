import React, { PureComponent } from 'react';
import Message from './Message';
import { INCREMENT_OFFSET } from '../actions/dialog';

export default class DialogList extends PureComponent {
    componentDidMount() {
        window.lastHeight = 0;
    }

    shouldComponentUpdate(nextProps) {
        /** обновлять при изменении кол-ва сообщений, прочтении сообщения. не сильно улучшает */
        if(nextProps.dialog_has_load_messages === this.props.dialog_has_load_messages &&
            nextProps.dialog_offset === this.props.dialog_offset &&
            nextProps.dialog_offset !== INCREMENT_OFFSET &&
            this.props.messages === nextProps.messages) {
            return false;
        }
        return true;
    }

    componentDidUpdate(prevProps) {
        /** определение положения скролла после загрузки сообщений */
        if(this.props.dialog_is_loading === false) {
            let top =  document.getElementById('messages-messages-wrap').scrollHeight - window.lastHeight;

            if(top > 0) {
                /** в самый низ при добавлении сообщения или при первой загрузке */
                if(prevProps.dialog_offset +1 === this.props.dialog_offset || this.props.dialog_offset <= INCREMENT_OFFSET) {
                    document.getElementById('messages-messages-wrap').scrollTop = document.getElementById('messages-messages-wrap').scrollHeight;

                    /** отследить загрузку изображений и мотануть вниз, не удаляется слушатель*/
                    let imgs = document.querySelectorAll('#messages-messages-div .messages-message_text img:not([load=true])');

                    for (let i = 0; i < imgs.length; i++) {
                        imgs[i].setAttribute('load', 'true');
                        imgs[i].addEventListener('load', () => {
                            document.getElementById('messages-messages-wrap').scrollTop = document.getElementById('messages-messages-wrap').scrollTop + imgs[i].height;
                        }, false);
                    }
                } else {
                    document.getElementById('messages-messages-wrap').scrollTop = top;
                }
            }
        }

        window.lastHeight = document.getElementById('messages-messages-wrap').scrollHeight;
        /** определение положения скролла после загрузки сообщений */

        /** ссылки в новом окне */
        let links = document.querySelectorAll('#messages-messages-div .messages-message_text a:not([target="_blank"])');

        for(let i = 0; i < links.length; i++) {
            links[i].setAttribute('target', '_blank');
        }
    }

    render() {
        return (
            <div id="messages-messages-div">
                {this.props.infoUsers.size
                    ?
                    this.props.messages.map((message) => (
                        <Message
                            dialog_info={this.props.prepareReceiverImg(this.props.infoUsers, [message.fk_user_sender], this.props.im)}
                            key={message.pk_message}
                            message={message}
                            im={this.props.im}
                        />
                    ))
                    : ''
                }
            </div>
        );
    }
}
