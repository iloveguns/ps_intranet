import React, { Component } from 'react';
import { register, log_in } from '../api';

const styles = {
    width: '400px',
    margin: '0 auto',
    marginTop: '10%',
    textAlign: 'center'
};

const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export default class Login extends Component {
    onLogin(e) {
        this.checkSend('l-l', 'l-p', log_in);

        e.preventDefault();
    }

    onRegister(e) {
        this.checkSend('r-l', 'r-p', register);

        e.preventDefault()
    }

    /**
     * проверка и выполнение функции
     *
     * @param doc_l
     * @param doc_p
     * @param func
     */
    checkSend(doc_l, doc_p, func) {
        let dl = document.getElementById(doc_l),
            dp = document.getElementById(doc_p),
            login = dl.value,
            password = dp.value.replace(/[^a-zA-Z0-9 ]/g, "");

        dp.value = password;

        if(login.length < 5 || EMAIL_REGEX.test(login) === false) { //не пойдет
            dl.classList.add('error');
        } else if(password.length < 5) {
            dp.classList.add('error');
        } else {
            func(login, password)
                .then(data => {
                    localStorage.setItem('uniqhash', data.password);
                    localStorage.setItem('uniqid', data.id);
                    window.location.reload();
                })
                .catch(() => {
                    dl.classList.add('error');
                    dp.classList.add('error');
                });
        }
    }

    render() {
        return (
            <div style={styles}>
                <h3>Зарегистрироваться</h3>
                <form onSubmit={this.onRegister.bind(this)}>
                    <input type="email" id="r-l" placeholder="email" minLength={5} maxLength={25}/><br/>
                    <input type="password" id="r-p" placeholder="пароль" minLength={5} maxLength={15}/><br/>
                    <button type="submit">Зарегистрироваться</button>
                </form>

                <h3>Войти</h3>
                <form onSubmit={this.onLogin.bind(this)}>
                    <input type="email" id="l-l" placeholder="email" minLength={5} maxLength={25}/><br/>
                    <input type="password" id="l-p" placeholder="пароль" minLength={5} maxLength={15}/><br/>
                    <button type="submit">Войти</button>
                </form>
            </div>
        );
    }
}