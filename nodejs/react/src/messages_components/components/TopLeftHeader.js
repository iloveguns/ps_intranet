import React, { Component } from 'react';
import { isUseProfile } from '../../config';
import Modal from 'react-modal';
import { createDialog, saveProfile } from '../api';

export default class TopLeftHeader extends Component {
    shouldComponentUpdate(nextProps) {
        return this.props.isOpenHeader !== nextProps.isOpenHeader || this.props.isOpenSearch !== nextProps.isOpenSearch
            || this.props.isOpenProfile !== nextProps.isOpenProfile || nextProps.searchItems.length > 0;
    }

    /**
     * поиск контактов, запрашивать у сервера
     */
    onSearchContacts() {
        let value = document.getElementById('messages-search_contacts').value;

        if(value.length > 2 && value.length < 20) {// && this.props.modal.isLoad === false
            this.props.searchContacts(value);
        }
    }

    /**
     * отправка в создание диалога, пока только одиночно
     *
     * @param {int} id - ид контакта
     */
    getCreateDialog(id, e) {
        createDialog([id]);

        e.preventDefault();
    }

    /**
     * сохранение профиля
     */
    prepareSaveProfile() {
        let lastname = document.getElementById('lastname').value.replace(/[^a-zA-Z0-9а-яА-ЯЁё ]/g, ""),
            name = document.getElementById('name').value.replace(/[^a-zA-Z0-9а-яА-ЯЁё ]/g, "");

        if(lastname.length < 5 || name < 5) {
            console.log('none');
        } else {
            saveProfile(lastname, name);
        }
    }

    render() {
        /** стили модального окна */
        Modal.defaultStyles.overlay.backgroundColor = 'rgba(0, 0, 0, 0.6)';
        const modalStyles = {content : {
            width: '30%',
            margin: '0 auto',
            padding: 0
        }};

        let page_header_class = "messages-page_header";
        if(this.props.isOpenHeader === true) page_header_class += ' open';

        return (
            <div className={page_header_class} onClick={this.props.openHeader}>
                <div className="messages-page_header_wrap">
                    Меню
                </div>
                <ul className="dropdown-menu">
                    <li onClick={this.props.toggleModalSearch}>Поиск контактов</li>
                    {isUseProfile ? <li onClick={this.props.toggleModalProfile}>Профиль</li> : ''}
                </ul>

                <Modal
                    isOpen={this.props.isOpenSearch}
                    onRequestClose={this.props.toggleModalSearch}
                    style={modalStyles}
                    contentLabel="Modal"
                >
                    <div className="md_simple_modal_body">
                        <input
                            type="search" id="messages-search_contacts" placeholder="Поиск контактов"
                            onKeyUp={this.onSearchContacts.bind(this)}
                            autoFocus={true}
                        />
                        <ul className="messages-dialogs">
                            {this.props.searchItems.map((contact) => (
                                <li className="dialog-item" key={contact.id_user}>
                                    <a href={contact.id_user} onClick={this.getCreateDialog.bind(this, contact.id_user)}>
                                        <div className="dialog-photo">
                                            <img src={window.ROOT_URL + contact.avatar}/>
                                        </div>
                                        <div className="dialog-message">
                                            <div className="dialog-with">
                                                {`${contact.lastname} ${contact.name}`}
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            ))}
                        </ul>
                    </div>
                </Modal>

                {this.props.infoUser ?
                    <Modal
                        isOpen={this.props.isOpenProfile}
                        onRequestClose={this.props.toggleModalProfile}
                        style={modalStyles}
                        contentLabel="Modal"
                    >
                        <div className="md_simple_modal_body">
                            <h4>Редактировать профиль</h4>
                            <div className="md-input-group">
                                <label className="md-input-label">Фамилия</label>
                                <input type="text" maxLength={50} id="lastname" className="md-input" defaultValue={this.props.infoUser.lastname}/>
                            </div>
                            <div className="md-input-group">
                                <label className="md-input-label">Имя</label>
                                <input type="text" maxLength={50} id="name" className="md-input" defaultValue={this.props.infoUser.name}/>
                            </div>
                        </div>
                        <div className="md_simple_modal_footer">
                            <button className="btns btn-md" onClick={this.props.toggleModalProfile}>Отмена</button>
                            <button className="btns btn-md btn-md-primary" onClick={this.prepareSaveProfile}>Сохранить</button>
                        </div>
                    </Modal>
                : ''}
            </div>
        );
    }
}