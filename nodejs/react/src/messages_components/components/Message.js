import React, { Component } from 'react';
import renderHTML from 'react-render-html';
import { readMessage } from '../api/index';
import { formatTime } from '../functions';

export default class Message extends Component {
    constructor(props) {
        super(props);

        this.handleHoverEnter = this.handleHoverEnter.bind(this);
    }

    componentDidMount() {
        /** навесить слушатель события для метки о чтении */
        if(this.props.message.readed === 0 && this.props.message.fk_user_sender !== this.props.im) {
            this.hover.addEventListener("mouseenter", this.handleHoverEnter);
        }
    }

    /**
     * пометить прочитанным и удалить слушатель
     */
    handleHoverEnter() {
        this.hover.removeEventListener("mouseenter", this.handleHoverEnter);
        readMessage(this.props.message.pk_message);
    }

    shouldComponentUpdate(nextProps) {
        /** обновлять только то сообщение, которое прочитано, фул оптимизация каждого сообщения */
        return this.props.message.readed !== nextProps.message.readed;
    }

    render() {
        return (
            <div className="messages-message_outer_wrap" ref={elem => this.hover = elem}>
                <div className="messages-message_inner_wrap">
                    <div className="messages-message">
                        {this.props.message.readed === 0 ? <i className="icon-message-status"/> : ''}
                        <a className="messages-message_link_photo">
                            <img className="messages_message_from_photo" src={this.props.dialog_info.avatar} />
                        </a>
                        <div className="messages-message-body">
                            <a href="/" className="messages-message_sender">
                                {this.props.dialog_info.name}
                            </a>
                            <div className="messages-message_text_wrap">
                                <div className="messages-message_text" dir="auto">
                                    {renderHTML(renderHTML(this.props.message.text))}
                                </div>
                            </div>
                        </div>
                        <div className="messages-message_meta pull-right">
                            {formatTime(this.props.message.create_date)}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
