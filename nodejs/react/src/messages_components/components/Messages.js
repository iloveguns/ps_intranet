import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as dialogListActions from '../actions/dialogList';
import * as dialogActions from '../actions/dialog';
import * as messageActions from '../actions/message';
import * as modalActions from '../actions/modal';

import { bindActionCreators } from 'redux';
import DialogList from './DialogList';
import { Switch, Route } from 'react-router-dom';
import TextEditor from './TextEditor';
import { prepareReceiverImg, getUniqId, getUniqHash } from '../functions';
import Login from './Login';
import Halogen from 'halogen';
import TopLeftHeader from './TopLeftHeader';
import { socket, showOnline } from '../api';

//router
import DialogMessages from './DialogMessages';
import { withRouter } from 'react-router-dom';

import 'moment/locale/ru';//глобально

class Messages extends Component {
    componentDidMount() {
        this.uniqid = getUniqId();

        /** слушатель сообщений */
        socket.on('hasNewMessage', (message) => {
            this.props.dialogActions.onHasNewMessage(message);
        });

        /** получение сообщений в диалоге */
        socket.on('getMessagesDialog', (data) => {
            this.props.dialogActions.onGetMessagesDialog(data);
        });

        /** чтение сообщения */
        socket.on('readedMessage', (data) => {
            this.props.dialogActions.onReadedMessage(data);
        });

        /** ответ по поиску контактов */
        socket.on('searchContacts', (data) => {
            this.props.modalActions.onSearchContacts(data);
        });

        /** ответ о создании диалога */
        socket.on('createDialog', (data) => {
            this.props.dialogListActions.onCreateDialog(data);
        });

        /** ответ о редактировании профиля */
        socket.on('saveProfile', (data) => {
            this.props.modalActions.onSaveProfile(data);
        });

        if(Number.isInteger(this.uniqid)) {
            /** начать загрузку списка диалогов */
            this.props.dialogListActions.initDialogList();

            /** сразу после загрузки проверить, активен ли диалог и загрузить */
            if (this.props.dialog.dialog_active) {
                this.props.dialogActions.loadMore(this.props.dialog.dialog_active);
            }
        }
    }

    /**
     * подгрузка сообщений при скролле в самый верх
     * если промотано вверх и стоит метка наличия незагруженных сообщений
     */
    _handleScroll(event) {
        if(event.target.scrollTop === 0 && this.props.dialog.dialog_has_load_messages === true && this.props.dialog.dialog_is_loading === false) {
            this.props.dialogActions.loadMore(this.props.dialog.dialog_active, this.props.dialog.dialog_offset);
        }
    }

    render() {
        /** пред логин/регистрация */
        if(getUniqHash() === null || this.uniqid === undefined) {
            return <Login/>;
        }

        /** вывод ошибки */
        if (this.props.dialogsList.items_has_errors) {
            return <p>Возникли ошибки при загрузке</p>;
        }

        /** спиннер */
        if (this.props.dialogsList.items_is_loading) {
            return <div className="spinner">
                <Halogen.ScaleLoader color="#6490b1"/>
            </div>;
        }

        let dialogList = [];
        this.props.dialogsList.mapitems.forEach((dialog, pk) => {
            dialogList.push(
            <DialogList
                dialog_info={prepareReceiverImg(this.props.infoUsers, dialog.users, this.uniqid)}
                dialog_is_loading={this.props.dialog.dialog_is_loading}
                loadMore={this.props.dialogActions.loadMore} //передать диспатченную функцию вызова загрузки диалога
                activeDialog={this.props.dialog.dialog_active}
                key={pk}
                im={this.uniqid}
                dialog={dialog}
            />)
        });

        return (
            <div id="messages-container">
                <div className="messages-dialog-wrap messages-small_block">
                    <TopLeftHeader
                        isOpenSearch={this.props.modal.isOpenSearch}
                        isOpenHeader={this.props.modal.isOpenHeader}
                        isOpenProfile={this.props.modal.isOpenProfile}
                        openHeader={this.props.modalActions.openHeader}
                        toggleModalSearch={this.props.modalActions.toggleModalSearch}
                        toggleModalProfile={this.props.modalActions.toggleModalProfile}
                        searchContacts={this.props.modalActions.searchContacts}
                        searchItems={this.props.modal.searchItems}
                        infoUser={this.props.infoUsers.get(this.uniqid)}
                    />
                    <div className="messages-flex_scroll small_scroll pull-left">
                        <ul className="messages-dialogs">
                        {dialogList}
                        </ul>
                    </div>
                </div>
                <div className="messages-wrap messages-big_block pull-left">
                    <div className="messages-page_header _mtborder">
                        {this.props.infoUsers.size
                        && this.props.dialog.dialog_active
                        && this.props.dialog.dialog_has_errors === false
                        && this.props.dialogsList.mapitems.get(this.props.dialog.dialog_active) !== undefined
                            ?
                            <div className="messages-page_header_wrap">
                                {prepareReceiverImg(this.props.infoUsers, this.props.dialogsList.mapitems.get(this.props.dialog.dialog_active).users, this.uniqid).names}
                            </div>
                            : ''
                        }
                    </div>
                    <div id="messages-messages-wrap" className="messages-flex_scroll" onScroll={this._handleScroll.bind(this)}>
                        {this.props.dialog.dialog_is_loading
                            ?
                            <div className="spinner">
                                <Halogen.ScaleLoader color="#6490b1"/>
                            </div>
                            : ''}
                        <Switch>
                            <Route path='/dialog' render={(props) => (
                                (Array.isArray(this.props.dialog.dialog_items))
                                    ?
                                    <DialogMessages
                                        dialog_is_loading={this.props.dialog.dialog_is_loading}
                                        dialog_offset={this.props.dialog.dialog_offset}
                                        dialog_has_load_messages={this.props.dialog.dialog_has_load_messages}
                                        infoUsers={this.props.infoUsers}
                                        messages={this.props.dialog.dialog_items}
                                        im={this.uniqid}
                                        prepareReceiverImg={prepareReceiverImg}
                                    />
                                    : ''
                            )}/>
                        </Switch>
                    </div>
                    <div className="messages-text_wrap">
                        {this.props.infoUsers.size
                        && this.props.dialog.dialog_active
                        && this.props.dialogsList.mapitems.get(this.props.dialog.dialog_active) !== undefined
                            ?
                            <TextEditor
                                receiver_info={prepareReceiverImg(this.props.infoUsers, this.props.dialogsList.mapitems.get(this.props.dialog.dialog_active).users, this.uniqid)}
                                sender_info={prepareReceiverImg(this.props.infoUsers, this.uniqid)}
                                activeDialog={this.props.dialog.dialog_active}
                            />
                            : ''
                        }
                        <span onClick={showOnline} />
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        dialogsList: state.dialogsList,
        dialog     : state.dialog,
        infoUsers  : state.infoUsers,
        message    : state.message,
        modal      : state.modal
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dialogListActions: bindActionCreators(dialogListActions, dispatch),
        dialogActions: bindActionCreators(dialogActions, dispatch),
        messageActions: bindActionCreators(messageActions, dispatch),
        modalActions: bindActionCreators(modalActions, dispatch)
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Messages));