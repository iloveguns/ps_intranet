import React, { Component } from 'react';
import autosize from 'autosize';
import { createMessage } from '../api/index';
import { findUrlReplaceLink } from '../functions';

export default class TextEditor extends Component {
    shouldComponentUpdate(nextProps) {
        return this.props.activeDialog !== nextProps.activeDialog;
    }

    componentDidMount() {
        autosize(document.querySelector('#messages-text_form textarea'));
    }

    componentDidUpdate() {
        document.querySelector('#messages-text_form textarea').focus();
    }

    /**
     * проверка нажатий в текст редакторе
     */
    handleChangeEditor(e) {
        /** при нажатии enter без шифта */
        if(e.charCode === 13 && e.shiftKey === false) {
            this.prepareSendMessage(e);
        }
    }

    /**
     * подготовка и отправка текстового сообщения
     */
    prepareSendMessage(e) {
        e.preventDefault();
        let text = document.querySelector('#messages-text_form textarea').value.trim().replace(/[\r\n]+/g, '<br>').replace(/\s+/g, ' ');
        text = findUrlReplaceLink(text);

        if(text.length > 0) {
            createMessage(text, this.props.activeDialog);
            document.querySelector('#messages-text_form textarea').value = '';
            autosize.update(document.querySelector('#messages-text_form textarea'));
        }
    }

    render() {
        let receiverClass = '';
        if(this.props.receiver_info.online) {
            receiverClass += ' online';
        }

        return (
            <div className="messages-text_wraps">
                <a className={receiverClass}>
                    <img className="messages-text_sender_img" src={this.props.sender_info.avatar} alt={this.props.sender_info.name} title={this.props.sender_info.name} />
                </a>
                <form id="messages-text_form">
                    <textarea placeholder="Введите сообщение" onKeyPress={this.handleChangeEditor.bind(this)} className="small_scroll" autoFocus={true}></textarea>
                    <div className="">
                        <button type="submit" className="btns btns_submit pull-right" onClick={this.prepareSendMessage.bind(this)}>SEND</button>
                    </div>
                </form>
                <a>
                    <img className="messages-text_receiver_img" src={this.props.receiver_info.avatar} alt={this.props.receiver_info.name} title={this.props.receiver_info.name} />
                </a>
            </div>
        );
    }
}
