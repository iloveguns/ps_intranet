import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import renderHTML from 'react-render-html';
import { formatTime } from '../functions';

export default class DialogList extends Component {
    /**
     * вызов загрузки сообщений в диалог при клике на другой диалог
     */
    setLoadDialog() {
        /** если не тот же диалог и не загружаются уже сообщения в диалог */
        if(this.props.activeDialog !== this.props.dialog.pk_dialog && this.props.dialog_is_loading === false) {
            this.props.loadMore(this.props.dialog.pk_dialog);
        }
    }

    shouldComponentUpdate(nextProps) {
        /** обновлять при смене активного диалога, написании сообщения */
        return !(this.props.activeDialog === nextProps.activeDialog &&
            this.props.dialog_is_loading === nextProps.dialog_is_loading &&
            this.props.dialog.unread === nextProps.dialog.unread &&
            (this.props.dialog.message !== null && nextProps.dialog.message !== null && this.props.dialog.message.text === nextProps.dialog.message.text));
    }

    render() {
        let dialogListText = '', //текст при отсутствии сообщения
            dialogTime = '',
            dialogLink = `/dialog/#${this.props.dialog.pk_dialog}`,
            dialogClass = 'dialog-item',
            dialogIM = '';

        //проверка, существует ли последнее сообщение и вариации данных
        if(this.props.dialog.message === null) {
            dialogTime = this.props.dialog.date_dialog;
        } else {
            if(this.props.im === this.props.dialog.message.fk_user_sender) {
                dialogIM = 'Вы: ';
            }

            dialogListText = renderHTML(this.props.dialog.message.text).replace(/<br\s*\/?>/gi,' ');
            dialogTime = this.props.dialog.message.create_date;

            /** если ссылка - выводить ссылку без тега а */
            let regex = /<a.*?href="(.*?)"/;

            if(regex.test(dialogListText)) {
                dialogListText = regex.exec(dialogListText)[1];
            }
        }

        //добавление класса активному диалогу
        if(this.props.activeDialog === this.props.dialog.pk_dialog) {
            dialogClass += ' active';
        }

        return (
            <li className={dialogClass}>
                <Link to={dialogLink} onClick={this.setLoadDialog.bind(this)}>
                    <div className="dialog-photo">
                        <img src={this.props.dialog_info.avatar} />
                    </div>
                    <div className="dialog-wrap_badge text-right pull-right">
                        <div className="dialog-date">
                            {formatTime(dialogTime)}
                        </div>
                        {this.props.dialog.unread > 0
                            ?
                            <div className="dialog-badge">
                                {this.props.dialog.unread}
                            </div> : ''
                        }
                    </div>
                    <div className="dialog-message">
                        <div className="dialog-with">
                            {this.props.dialog_info.names}
                        </div>
                        <div className="dialog-message-text">
                            <strong>{dialogIM}</strong>{dialogListText}
                        </div>
                    </div>
                </Link>
            </li>
        );
    }
}