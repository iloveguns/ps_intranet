/* global ReactDOM */

import TinyMCEInput from 'react-tinymce-input';
import Comment from './components/Comment';
import ShowReplyComment from './components/ShowReplyComment';
import ShowChangeComment from './components/ShowChangeComment';
import { Element, scroller } from 'react-scroll';
import Lightbox from 'react-image-lightbox';
import axios from 'axios';

axios.defaults.baseURL = window.apiurl;//глобальная переменная

export default class Comments extends React.Component {
    constructor(props) {
        super(props);

        //настройки редактора
        this.redactorConfig = {
            menubar: false,
            skin: false,
            statusbar: false,
            language: 'ru',
            plugins: 'link image code autoresize paste media lists autolink',
            toolbar: 'bold italic | link image media | bullist numlist | code',
            autoresize_bottom_margin: 0,
            autoresize_max_height: 500,
            autoresize_min_height: 60,
            remove_script_host: true,//хост не важен, картинки лежат на локале
            relative_urls: false,
            
            // видео
            media_alt_source: false,
            media_poster: false,
            /** video_template_callback: function(data) { // custom template video tag
                return '<video width="' + data.width + '" height="' + data.height + '"' + (data.poster ? ' poster="' + data.poster + '"' : '') + ' controls="controls">\n' + '<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + (data.source2 ? '<source src="' + data.source2 + '"' + (data.source2mime ? ' type="' + data.source2mime + '"' : '') + ' />\n' : '') + '</video>';
            },*/
            
            /*force_br_newlines: true,
            force_p_newlines: false,
            forced_root_block: '',*/
            file_picker_callback: function (callback, value, meta) {
                if (meta.filetype === 'image') { // загрузка и вставка изображений
                    let input = document.getElementById('tinymce-file-upload');
                    input.click();
                    input.onchange = () => {
                        //загрузить на файл сервер и отдать ссылку
                        let file = input.files[0];
                        let data = new FormData();
                        data.append('file', file);

                        axios({
                            method: 'post',
                            url: '/api/upload',
                            data: data
                        })
                        .then((res) => {
                            callback(res.data.files[0].path, {
                                alt: file.name
                            });
                        })
                        .catch(() => {
                            callback('Невозможно загрузить изображение', {
                                alt: 'Невозможно загрузить изображение'
                            });
                        });
                    };
                } else if (meta.filetype === 'media') { // загрузка и вставка видео
                    let input = document.getElementById('tinymce-media-upload');
                    input.click();
                    input.onchange = () => {
                        //загрузить на файл сервер и отдать ссылку
                        let file = input.files[0];
                        let data = new FormData();
                        data.append('file', file);

                        axios({
                            method: 'post',
                            url: '/api/upload',
                            data: data
                        })
                        .then((res) => {
                            callback(res.data.files[0].path, {
                                alt: file.name
                            });
                        })
                        .catch(() => {
                            callback('Невозможно загрузить видеофайл', {
                                alt: 'Невозможно загрузить видеофайл'
                            });
                        });
                    }
                }
            }
        };

        this.scrollerOptions = {
            duration: 500,
            delay: 100,
            offset: -60,
            smooth: true
        };
        
        //глобальные переменные
        this.id_user = window.uniqid;//ид пользователя
        this.item_id = window.react_item_id;//ид сущности
        this.item_class = window.react_item_class;//имя сущности
        this.nullUser = window.react_null_user;//имя пустого пользователя
        this.click_rating = {id: null, action: null};
        
        //параметры загрузки
        this.limit = 15;
        this.offset = 0;
        this.initRedactorText = '<p> </p>';//багище tinymce
        
        //рейтинг, для хранения состояния нажатых кнопок(нельзя повторно нажимать)
        this.clickedRating = new Map();//weakmap?
        
        this.maptips = new Map();

        this.state = {
            changing_comment    : false, //ид редактируемого комментария
            have_new_component  : false, //метка есть ли новый компонент(добавление комментария)
            commentsList        : [], //массив коментариев
            redactorText        : '', //состояние текста редактора
            reply_comment       : false, //ответ на комментарий(ид) (при создании комментария)
            show_reply_to_comment: '', //имя комментатора, на который ответ пишется
            have_more_comments  : 0, //{int} есть ли еще комментарии
            remove_imgs         : window.innerWidth < 767, //убирать ли фото в маленьком разрешении
            
            light_src           : '', //путь фото
            light_nextSrc       : '', //след фото
            light_prevSrc       : '', //пред фото
            light_selector      : '', //селектор блока с фото (ид или класс)
            light_num           : null, //положение текущего фото( 0 (1 из 5 например))
            light_isOpen        : false //состояние открытости
        };

        this.onChangeComment = this.onChangeComment.bind(this);
        this.onReplyComment = this.onReplyComment.bind(this);
        this.onDeleteComment = this.onDeleteComment.bind(this);
        this.onScroll = this.onScroll.bind(this);
        this.loadMoreComments = this.loadMoreComments.bind(this);
        this.onCancelChangeComment = this.onCancelChangeComment.bind(this);
        this.onRating = this.onRating.bind(this);
        this.setBordered = this.setBordered.bind(this);
        this.setInitClickRating = this.setInitClickRating.bind(this);
    }
    
    setInitClickRating() {
        this.click_rating = {id: null, action: null};
    }

    componentDidMount() {
        this.loadMoreComments();
    }
    
    /**
     * lightbox менять состояние на клике фото
     */
    setLightState(src, nextSrc, prevSrc, sel, num) {
        this.setState({
            light_isOpen: true,
            light_src: src,
            light_nextSrc: nextSrc,
            light_prevSrc: prevSrc,
            light_selector: sel,
            light_num: num
        });
    }
    
    /**
     * загрузка/подгрузка комментариев
     */
    loadMoreComments() {
        axios
            .get('/api/comment', {
                params: { item_id: this.item_id, item_class: this.item_class, limit: this.limit, offset: this.offset, id_user: this.id_user, }
            })
            .then((res) => {
                if(res.data.comments.length > 0) {
                    this.offset += this.limit;
                    
                    let commentsListOld = this.state.commentsList;
                    let commentsList = commentsListOld.concat(res.data.comments);
                    
                    //лайки/дизлайки сразу внести
                    for(let i = 0; i < res.data.comments.length; i++) {
                        if(res.data.comments[i].ratings !== undefined && res.data.comments[i].ratings.mine !== undefined) {
                            this.clickedRating.set(res.data.comments[i].id_comment, res.data.comments[i].ratings.mine);
                        }
                    }
                            
                    this.setState({
                        commentsList: commentsList,
                        have_more_comments: res.data.haveMore
                    });
                }
            })
            .catch((err) => {
                console.error(err);
            });
    }

    /**
     * клик кнопки отправить комментарий
     */
    handleButtonChange () {
        //отмена клика при пустом тексте
        if(!this.state.redactorText
            || this.state.redactorText.replace(/\s+/g, '') === '<p></p>'
            || this.state.redactorText.replace(/\&nbsp;/g, '').replace(/\s+/g, '') === '<p></p>'
        ) {
            return false;
        }
        
        this.clearBordered();

        let commentsListOld = this.state.commentsList;//скопировать текущий список комментов

        //редактирование комментария
        if(this.state.changing_comment !== false) {
            let changed_comment = {
                id_comment  : this.state.changing_comment,
                text        : this.state.redactorText
            };
            
            axios
                .put('/api/comment', changed_comment)
                .then(() => {
                    for(let i = 0; i < commentsListOld.length; i++) {
                        if(commentsListOld[i].id_comment === changed_comment.id_comment) {
                            commentsListOld[i].text = changed_comment.text;
                            break;
                        }
                    }
                    
                    this.onScroll(`comment-${changed_comment.id_comment}`);
                    
                    this.setState({
                        changing_comment: false,
                        redactorText: this.initRedactorText,
                        reply_comment: false,
                        show_reply_to_comment: '',
                        have_new_component: true,//где-то надо убрать после скролла
                        commentsList: commentsListOld
                    });
                })
                .catch((err) => {
                    console.error(err);
                });
        } else {//сохранение нового
            let new_comment = {
                id_user     : this.id_user,
                text        : this.state.redactorText,
                item_id     : this.item_id,
                item_class  : this.item_class,
                reply_to    : this.state.reply_comment
            };
            
            axios
                .post('/api/comment', new_comment)
                .then((res) => {
                    commentsListOld.unshift(res.data.comment);
                    
                    this.offset += 1;
                    
                    this.setState({
                        changing_comment: false,
                        redactorText: this.initRedactorText,
                        reply_comment: false,
                        show_reply_to_comment: '',
                        have_new_component: true,//где-то надо убрать после скролла
                        commentsList: commentsListOld
                    });
                })
                .catch((err) => {
                    console.error(err);
                });
        }
    }

    /**
     * фокус на редактор
     */
    focusRedactor() {
        window.tinyMCE.get(document.getElementsByClassName('textarea-tinymce')[0].id).focus();
    }

    /**
     * заскроллить к элементу(якорь)
     * @param {string} elem - имя элемента react-scroll
     */
    onScroll(elem) {
        scroller.scrollTo(elem, this.scrollerOptions);
    }

    /**
     * состояние редактора
     * @param {string} value - текст в редакторе
     */
    redactorOnChange(value) {
        this.setState({
            redactorText: value
        });
    }
    
    /**
     * @param {int} item_id - ид комментария
     * @param {int} action  - лайк(1), дизлайк(0)
     */
    onRating(item_id, action) {
        if(this.clickedRating.get(item_id, action) ===  action) {
            
        } else {
            //сохранение ид комментария кликнутого по рейтингу(глобально для него же)
            this.click_rating = {id: item_id, action: action};
            
            axios
            .post('/api/comment_rating', {
                id_user : this.id_user,
                item_id : item_id,
                action  : action
            })
            .then((response) => {
                let commentsListOld = this.state.commentsList,
                    rating = response.data.ratings;
                    
                for(let i = 0; i < commentsListOld.length; i++) {
                    if(commentsListOld[i].id_comment === item_id) {
                        //если пусто - создать
                        if(commentsListOld[i].ratings === undefined) {
                            commentsListOld[i].ratings = {
                                likes: 0,
                                dislikes: 0
                            }
                        }
                        //обновить данные у конкретного комментария
                        commentsListOld[i].ratings.likes        = rating.likes;
                        commentsListOld[i].ratings.dislikes     = rating.dislikes;
                        commentsListOld[i].ratings.likes_id     = rating.likes_id;
                        commentsListOld[i].ratings.dislikes_id  = rating.dislikes_id;
                        commentsListOld[i].ratings.mine         = rating.mine;
                        break;
                    }
                }
                
                this.clickedRating.set(item_id, action);
                
                this.setState({
                    commentsList: commentsListOld
                });
            })
            .catch((err) => {
                console.error(err);
            });
        }
    }

    /**
     * при изменении комментария
     * @param {int} id_comment - ид комментария
     * @param {string} text - текст комментария
     */
    onChangeComment(id_comment, text) {
        this.onScroll('comment-redactor');
        
        this.focusRedactor();

        this.setState({
            changing_comment: id_comment,
            redactorText: text,
            reply_comment: false,
            show_reply_to_comment: ''
        });
    }

    /**
     * ответ на комментарий
     * @param {bool|int} value - false|ид комментария
     * @param {string} text - имя комментатора
     */
    onReplyComment(value, text) {
        this.focusRedactor();

        this.setState({
            reply_comment: value,
            show_reply_to_comment: text,
            changing_comment: false
        });
    }
    
    /**
     * подсветка комментария
     * @param {string} name_comment - name комментария
     */
    setBordered(name_comment) {        
        this.clearBordered();
        
        document.querySelector('div[name=' + name_comment + '] .panel').classList.add('panelHover');
    }
    
    /**
     * очистка подсветки всех комментариев
     */
    clearBordered() {
        for(let i = 0; i < document.querySelectorAll('.panelHover').length; i++) {
            document.querySelectorAll('.panelHover')[i].classList.remove('panelHover');
        }
    }
    
    /**
     * отмена редактирования, сброс текста и ид редактируемого комментария
     */
    onCancelChangeComment() {
        this.focusRedactor();

        this.setState({
            redactorText: this.initRedactorText,
            changing_comment: false
        });
    }
    
    /**
     * при удалении/восстановлении комментария
     * @param {int} id_comment - ид комментария
     * @param {int} isdel      - удалять ли коммент(1 - да, 0 - восстановить)
     * 
     * внесены изменения в дом, при удалении происходит чепуха, коммент с картинками не удаляется
     */
    onDeleteComment(id_comment, isdel) {
        axios
            .delete('/api/comment', {
                data: { id_comment : id_comment, del : isdel }
            })
            .then(() => {
                let commentsListOld = this.state.commentsList;
                
                for(let i = 0; i < commentsListOld.length; i++) {
                    if(commentsListOld[i].id_comment === id_comment) {
                        commentsListOld[i].deleted = isdel;
                        break;
                    }
                }

                let setState = {
                    commentsList: commentsListOld
                };

                //проверка, если на удаляемый комментарий стоит метка ответа
                if(this.state.reply_comment === id_comment) {
                    setState.reply_comment = false;
                    setState.show_reply_to_comment = '';
                }

                this.clickedRating.delete(id_comment);

                this.setState(setState);
            })
            .catch((err) => {
                console.error(err);
            });
    }

    render() {
        let info_comment = '';

        if(this.state.reply_comment !== false) {
            info_comment = <ShowReplyComment
                text={this.state.show_reply_to_comment}
                reply_comment={this.state.reply_comment}
                onScroll={this.onScroll}
                onReplyComment={this.onReplyComment}
                setBordered={this.setBordered}
            />;
        }
        
        if(this.state.changing_comment !== false) {
            info_comment = <ShowChangeComment
                changing_comment={this.state.changing_comment}
                onScroll={this.onScroll}
                onCancelChangeComment={this.onCancelChangeComment}
                setBordered={this.setBordered}
            />
        }
        
        /**lightbox*/
        let light_props = {};
        if(this.state.light_nextSrc.length) {
            light_props.nextSrc = this.state.light_nextSrc;
        }
        if(this.state.light_prevSrc.length) {
            light_props.prevSrc = this.state.light_prevSrc;
        }
        /**end lightbox*/

        return (
            <Element name={'comment-redactor'}>
                <TinyMCEInput
                    textareaProps={{className: 'textarea-tinymce'}}
                    value={this.state.redactorText}
                    onChange={this.redactorOnChange.bind(this)}
                    tinymceConfig={this.redactorConfig}
                    style={{
                        marginBottom: 15
                    }}
                />
                <button
                    className="btn btn-primary pull-left"
                    onClick={this.handleButtonChange.bind(this)}
                    style={{
                        marginBottom: 15
                    }}
                >
                    Оставить комментарий
                </button>
                {info_comment}
                <div className="clearfix"></div>
                {this.state.commentsList.map((comment) => {
                    return <Comment
                        onScroll={this.onScroll}
                        onChangeComment={this.onChangeComment}
                        onDeleteComment={this.onDeleteComment}
                        onReplyComment={this.onReplyComment}
                        key={comment.id_comment}
                        comment={comment}
                        id_user={this.id_user}
                        nullUser={this.nullUser}
                        have_new_component={this.state.have_new_component}
                        remove_imgs={this.state.remove_imgs}
                        onRating={this.onRating}
                        setBordered={this.setBordered}
                        maptips={this.maptips}
                        click_rating={this.click_rating}
                        setInitClickRating={this.setInitClickRating}
                        galleryState={this.setLightState.bind(this)}
                    />
                })}
                {this.state.have_more_comments ?
                    <button className="btn btn-primary" onClick={this.loadMoreComments}>
                        Загрузить еще ({this.state.have_more_comments})
                    </button>
                : ''}
                
                {this.state.light_isOpen &&
                <Lightbox
                    mainSrc={this.state.light_src}
                    {...light_props}

                    onCloseRequest={() => this.setState({ light_isOpen: false })}
                    onMovePrevRequest={() => {
                        let countimgs = document.querySelectorAll(this.state.light_selector).length,
                            state = {
                                light_src: this.state.light_prevSrc,
                                light_nextSrc : '',
                                light_prevSrc : ''
                            };

                        if (this.state.light_num < countimgs) {
                            if(this.state.light_num > 0) {
                                state.light_num = --this.state.light_num;
                                state.light_nextSrc = document.querySelectorAll(this.state.light_selector)[state.light_num+1].src;
                                if(state.light_num > 0) {
                                    state.light_prevSrc = document.querySelectorAll(this.state.light_selector)[state.light_num - 1].src;
                                }
                            } else {
                                state.light_num = this.state.light_num;
                                state.light_nextSrc = document.querySelectorAll(this.state.light_selector)[state.light_num].src;
                            }
                        }

                        this.setState(state);
                    }}
                    onMoveNextRequest={() => {
                        let countimgs = document.querySelectorAll(this.state.light_selector).length,
                            state = {
                                light_src: this.state.light_nextSrc,
                                light_nextSrc : '',
                                light_prevSrc : ''
                            };

                        if (this.state.light_num < countimgs) {
                            state.light_num = ++this.state.light_num;
                            state.light_prevSrc = document.querySelectorAll(this.state.light_selector)[state.light_num-1].src;
                            if(state.light_num < countimgs-1) {
                                state.light_nextSrc = document.querySelectorAll(this.state.light_selector)[state.light_num+1].src;
                            }
                        }

                        this.setState(state);
                    }}

                    enableZoom={false}
                    imagePadding={50}
                    animationDuration={10}
                />
                }
            </Element>
        );
    }
}

ReactDOM.render(<Comments />, document.getElementById('comments'));