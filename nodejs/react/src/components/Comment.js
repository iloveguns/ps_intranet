import renderHTML from 'react-render-html';
import ReplyToLink from './ReplyToLink';
import ReplyLink from './ReplyLink';
import { Element } from 'react-scroll';
import TimeAgo from 'react-timeago';
import Ratings from './Ratings';

import timeagotranslate from 'react-timeago/lib/language-strings/ru';
import buildFormatter from 'react-timeago/lib/formatters/buildFormatter';

import moment from 'moment';
import 'moment/locale/ru';

const formatter = buildFormatter(timeagotranslate);

export default class Comments extends React.Component {

    componentDidMount() {
        this.lightBoxInit();
        
        this.tippySets();
                
        //спуститься к комментарию
        if (this.props.have_new_component) {
            this.props.onScroll(`comment-${this.props.comment.id_comment}`);
        }
    }
    
    /**
     * надо оптимизировать обновление, а то все обновляются сейчас
     */
    componentDidUpdate() {
        this.lightBoxInit();
        this.tippySets(false);
    }
    
    lightBoxInit() {
        //начальные состояния при клике на любое фото
        //селектор изображений
        let qsel = `#comment-post-${this.props.comment.id_comment} img`,
            countimgs = document.querySelectorAll(qsel).length;

        for(let i = 0; i < countimgs; i++) {
            let img = document.querySelectorAll(qsel)[i],
                nextSrc = '',
                prevSrc = '';

            //след фото
            if(i < countimgs-1) {
                nextSrc = document.querySelectorAll(qsel)[i+1].src;
            }

            //пред фото
            if(i > 0) {
                prevSrc = document.querySelectorAll(qsel)[i-1].src;
            }

            //состояние слайдера, передача наверх
            img.onclick = this.props.galleryState.bind(this, img.src, nextSrc, prevSrc, qsel, i);
        }
    }
    
    /**
     * создание подсказок, удаление
     * слишком много проблем
     * @param {bool} start - создавать как новые(true) или обновлять(false)
     */
    tippySets(start = true) {
        let likename = `#tippy-like-${this.props.comment.id_comment}`,
            likenamemap = `l-${this.props.comment.id_comment}`,
            dislikename = `#tippy-dislike-${this.props.comment.id_comment}`,
            dislikenamemap = `d-${this.props.comment.id_comment}`;
        
        if(this.props.comment.ratings) {//если есть данные
            //удалить сначала
            if(this.props.maptips.has(likenamemap)) {//like
                if(start === false) {
                    let t = this.props.maptips.get(likenamemap);
                    const el = document.querySelector(likename);
                    const popper = t.getPopperElement(el);
                    t.destroy(popper);
                }
                this.props.maptips.delete(likenamemap);
            }
            
            //удалить сначала
            if(this.props.maptips.has(dislikenamemap)) {//dislike
                if(start === false) {
                    let t = this.props.maptips.get(dislikenamemap);
                    const el = document.querySelector(dislikename);
                    const popper = t.getPopperElement(el);
                    t.destroy(popper);
                }
                this.props.maptips.delete(dislikenamemap);
            }
            
            if(this.props.comment.ratings.likes > 0) {//like
                const tip = tippy(likename);//создать
                this.props.maptips.set(likenamemap, tip);//поместить в массив
                
                //если ид тот же - показать подсказку
                if(this.props.comment.id_comment === this.props.click_rating.id && this.props.click_rating.action === 1) {
                    const el = document.querySelector(likename);
                    const popper = tip.getPopperElement(el);
                    tip.show(popper);
                }
            }
            
            if(this.props.comment.ratings.dislikes > 0) {//dislike
                const tip = tippy(dislikename);//создать
                this.props.maptips.set(dislikenamemap, tip);//поместить в массив
                
                //если ид тот же - показать подсказку
                if(this.props.comment.id_comment === this.props.click_rating.id  && this.props.click_rating.action === 0) {
                    const el = document.querySelector(dislikename);
                    const popper = tip.getPopperElement(el);
                    tip.show(popper);
                }
            }
        }
        this.props.setInitClickRating();
    }

    /**
     * изменение комментария, проброс наверх
     */
    changeComment(id_comment, text) {
        this.props.onChangeComment(id_comment, text);
    }

    /**
     * удаление комментария, проброс наверх
     */
    deleteComment(id_comment, isdel) {
        this.props.onDeleteComment(id_comment, isdel);
    }

    render() {
        let reply_to = '',
            comment_text = '',
            comment_deleted = this.props.comment.deleted === 1,
            limit_date = new Date().getTime() - (3 * 60 * 60 * 1000),//3 часа
            show_time = '',
            delete_comment = '',
            change_comment = '',
            username = '',
            user_avatar = '',
            my_comment = this.props.id_user === this.props.comment.id_user,
            restore_comment = '',
            show_img = '';
            
        //определение формата времени
        if(new Date(this.props.comment.datetime).getTime() > limit_date) {
            show_time = <TimeAgo
                            date={this.props.comment.datetime}
                            formatter={formatter}
                            title={moment(this.props.comment.datetime).format('lll')}
                            style={{
                                color: '#b7b7b7'
                            }}
                        />
        } else {
            show_time = <time dateTime={this.props.comment.datetime} style={{ color: '#b7b7b7' }}>{moment(this.props.comment.datetime).format('lll')}</time>
        }
    
        //ответ на комментарий
        if(this.props.comment.reply_to) {
            reply_to = <ReplyToLink 
            id_comment={this.props.comment.reply_to}
            onScroll={this.props.onScroll}
            setBordered={this.props.setBordered}
            />
        }
        
        //кнопка восстановить комментарий
        if(comment_deleted && my_comment) {
            restore_comment = <span className="react-btn-link pull-right text-green">
                                <i
                                onClick={this.deleteComment.bind(this, this.props.comment.id_comment, 0)}
                                className="fa fa-repeat"
                                title="Восстановить">
                            </i></span>;
        }
        
        //удален
        if(comment_deleted) {
            comment_text = '<p class="text-red">Комментарий удален</p>';            
        } else {
            comment_text = this.props.comment.text;
            
            //только свои комменты можно редактировать и удалять
            if(my_comment) {
                delete_comment = <span className="react-btn-link pull-right text-red">
                                <i
                                onClick={this.deleteComment.bind(this, this.props.comment.id_comment, 1)}
                                className="fa fa-times"
                                title="Удалить">
                            </i></span>;
                change_comment = <span className="react-btn-link pull-right text-blue">
                                <i
                                onClick={this.changeComment.bind(this, this.props.comment.id_comment, this.props.comment.text)}
                                className="fa fa-pencil"
                                title="Редактировать">
                            </i></span>;
            }
        }
        
        if(!this.props.comment.avatar_img) this.props.comment.avatar_img = '/images/nophoto.png';
        
        //пустой пользователь - специфика интранета
        if(this.props.comment.id_user === null) {
            username = this.props.nullUser;
            user_avatar = <img src={this.props.comment.avatar_img} alt={username} />;
        } else {
            username = <a
                href={`/user/user/profile/${this.props.comment.id_user}`}
                data-cluetip="true"
                >
                    {this.props.comment.username}
                </a>;

            user_avatar = <a 
                href={`/user/user/profile/${this.props.comment.id_user}`} 
                title={this.props.comment.username}
                data-cluetip="true"
                >
                    <img src={this.props.comment.avatar_img} alt={this.props.comment.username} />
                </a>;
        }
        
        //не показывать блок с фото
        if(this.props.remove_imgs === false) {
            show_img = <div className="col-md-3 col-lg-2 hidden-xs">
                            <figure className="thumbnail">
                                {user_avatar}
                            </figure>
                        </div>
        }

        return (
            <Element name={`comment-${this.props.comment.id_comment}`}>
                <section className="comment-list">
                    <article className="row">
                        {show_img}
                        <div className="col-md-9 col-lg-10">
                            <div className="panel panel-default arrow left">
                                <div className="panel-body">
                                    <header className="text-left">
                                        {delete_comment}
                                        {change_comment}
                                        {restore_comment}
                                        <div className="comment-user">
                                            {username}
                                            {reply_to}
                                        </div>
                                        {show_time}
                                    </header>
                                    <div className="comment-post" id={`comment-post-${this.props.comment.id_comment}`}>
                                        {renderHTML(comment_text)}
                                    </div>
                                    <div className="pull-right">
                                        <Ratings
                                            id_comment={this.props.comment.id_comment}
                                            ratings={this.props.comment.ratings}
                                            onRating={this.props.onRating}
                                        />
                                        <ReplyLink
                                            onScroll={this.props.onScroll}
                                            id_comment={this.props.comment.id_comment}
                                            username={this.props.comment.username}
                                            onReplyComment={this.props.onReplyComment}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </section>
            </Element>
        );
    }
}