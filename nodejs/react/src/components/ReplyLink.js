/**
 * кнопка ответить
 */
export default class ReplyLink extends React.Component {
    onReply(value, text) {
        this.props.onScroll('comment-redactor');
        this.props.onReplyComment(value, text);
    }

    render() {
        return (
            <span
                onClick={this.onReply.bind(this, this.props.id_comment, this.props.username)}
                title="Ответить"
                className="react-btn-link text-blue">
                <i className="fa fa-reply"></i>
            </span>
        );
    }
}