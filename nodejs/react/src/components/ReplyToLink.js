/**
 * якорная ссылка на комментарий, к которому ответ
 */
export default class ReplyToLink extends React.Component {

    toParentScroll(elem) {
        this.props.onScroll(elem);
        this.props.setBordered(elem);
    }

    render() {
        return (
            <a
                onClick={this.toParentScroll.bind(this, `comment-${this.props.id_comment}`)}
                href={`#comment-${this.props.id_comment}`}
                className="comment-reply-to"
                style={{
                    marginLeft : '10px',
                    color: '#b7b7b7'
                }}
            >
                ответ на комментарий
            </a>

        );
    }
}