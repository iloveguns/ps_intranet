/**
 * кнопка показа, что комментарий редактируется/ отмены редактирования
 */
export default class ShowChangeComment extends React.Component {

    /**
     * скролл к комментарию, на который ответ
     */
    onScroll() {
        this.props.onScroll(`comment-${this.props.changing_comment}`);
        this.props.setBordered(`comment-${this.props.changing_comment}`);
    }

    render() {
        return (
            <div className="pull-left">
                <button
                    onClick={this.onScroll.bind(this)}
                    className="btn btn-link">
                    редактирование комментария
                </button>
                <span className="react-btn-link text-red">
                    <i
                        onClick={this.props.onCancelChangeComment}
                        className="fa fa-times" 
                    ></i>
                </span>
            </div>
        );
    }
}