import renderHTML from 'react-render-html';

/**
 * кнопки лайк/дизлайк
 */
export default class Ratings extends React.Component {
    render() {
        let countlikes = '',
            countdislikes = '',
            likeClass = 'fa fa-thumbs-o-up',
            dislikeClass = 'fa fa-thumbs-o-down',
            htmlLikes = '',//html для вывода лайкнувших(просто ссылки на них)
            htmlDislikes = '';//html для вывода дизлайкнувших
        
        if(this.props.ratings) {
            if(this.props.ratings.likes > 0) {
                //создать ссылки с картинкой
                let html = [];                
                for(let i = 0; i < this.props.ratings.likes_id.length; i++) {
                    html.push(`<a href="/user/user/profile/${this.props.ratings.likes_id[i].id_user}" target="_blank" title="${this.props.ratings.likes_id[i].username}"><img width="50px" src="${this.props.ratings.likes_id[i].avatar_img}"></a>`);
                }
                
                countlikes = ` <strong>${this.props.ratings.likes}</strong>`;
                htmlLikes = <div id={`tippy-tmplike-${this.props.id_comment}`} style={{display : 'none'}}>
                                {renderHTML(html.join())}
                            </div>
            }
            
            if(this.props.ratings.dislikes > 0) {
                //создать ссылки с картинкой
                let html = [];                
                for(let i = 0; i < this.props.ratings.dislikes_id.length; i++) {
                    html.push(`<a href="/user/user/profile/${this.props.ratings.dislikes_id[i].id_user}" target="_blank" title="${this.props.ratings.dislikes_id[i].username}"><img width="50px" src="${this.props.ratings.dislikes_id[i].avatar_img}"></a>`);
                }
                
                countdislikes = ` <strong>${this.props.ratings.dislikes}</strong>`;
                htmlDislikes = <div id={`tippy-tmpdislike-${this.props.id_comment}`} style={{display : 'none'}}>
                                    {renderHTML(html.join())}
                                </div>
            }
            
            if(this.props.ratings.mine !== undefined) {
                //немножко изменить класс
                if(this.props.ratings.mine === 1) {
                    likeClass = 'fa fa-thumbs-up';
                } else {
                    dislikeClass = 'fa fa-thumbs-down';
                }
            }
        }
        
        return (
            <span>
                <span 
                    id={`tippy-like-${this.props.id_comment}`}
                    onClick={this.props.onRating.bind(this, this.props.id_comment, 1)}
                    className="react-btn-link text-green"
                    title="Нравится"
                    data-html={`tippy-tmplike-${this.props.id_comment}`}
                    data-animation="fade"
                    data-arrow="true"
                    data-interactive="true"
                    data-original-title="html"
                    >
                    <i className={likeClass}></i>{renderHTML(countlikes)}
                </span>
                <span
                    id={`tippy-dislike-${this.props.id_comment}`}
                    onClick={this.props.onRating.bind(this, this.props.id_comment, 0)}
                    className="react-btn-link text-red"
                    title="Не нравится"
                    data-html={`tippy-tmpdislike-${this.props.id_comment}`}
                    data-animation="fade"
                    data-arrow="true"
                    data-interactive="true"
                    data-original-title="html"
                    >
                    <i className={dislikeClass}></i>{renderHTML(countdislikes)}
                </span>
                {htmlLikes}
                {htmlDislikes}
            </span>
        );
    }
}