/**
 * кнопка показа, на чей комментарий будет ответ
 */
export default class ShowReplyComment extends React.Component {

    /**
     * скролл к комментарию, на который ответ
     */
    onScroll() {
        this.props.onScroll(`comment-${this.props.reply_comment}`);
        this.props.setBordered(`comment-${this.props.reply_comment}`);
    }

    onReplyComment(value) {
        this.props.onReplyComment(value);
    }

    render() {
        return (
            <div className="pull-left">
                <button
                    onClick={this.onScroll.bind(this)}
                    className="btn btn-link">
                    Ответ на комментарий от "{this.props.text}"
                </button>
                <span className="react-btn-link text-red">
                    <i
                        onClick={this.onReplyComment.bind(this, false)}
                        className="fa fa-times" 
                    ></i>
                </span>
            </div>
        );
    }
}