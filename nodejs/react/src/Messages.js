import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import Messages from './messages_components/components/Messages';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducers from './messages_components/reducers/index';
import createHistory from 'history/createBrowserHistory';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';

const history = createHistory();
const middleware = routerMiddleware(history);

const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose;

const store = createStore(
    reducers,
    composeEnhancers(
        applyMiddleware(thunk, middleware)
    )
);

render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <Messages />
        </ConnectedRouter>
    </Provider>,
    document.getElementById('messages')
);