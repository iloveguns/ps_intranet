"use strict";

const browserify = require('browserify');
const gulp = require('gulp');
const changed = require('gulp-changed');
const $ = require('gulp-load-plugins')();
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const babelify = require('babelify');

let uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    babel  = require('gulp-babel'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    rename = require('gulp-rename'),
    debug = require('gulp-debug'),
    minifyCSS = require('gulp-minify-css');

let path = '../../',//путь отсюда до туда
    jsfolder = 'web/js/',//путь к папке js
    scssfolder = 'web/scss/',//путь к папке scss
    cssfolder = 'web/css/',//путь к папке css
    babel_es = path + 'nodejs/gulp/node_modules/babel-preset-env';

/*
 * конкатенация js файлов в один и минификация
 * порядок файлов важен
 * , {since: gulp.lastRun('concatjs')}
 */
gulp.task('concatjs', (cb) => {
    gulp.src([
        //path + jsfolder + 'jquery.nestable.js',
        path + jsfolder + 'ajax.js',
        path + jsfolder + 'alerts.js',
        path + jsfolder + 'selectStructure.js',
        path + jsfolder + 'jquery.cookie.js',
        path + jsfolder + 'qtip2.js',
        path + jsfolder + 'jquery.colorbox-min.js',
        path + jsfolder + 'clipboard.min.js',
        path + jsfolder + 'tinycon.min.js',
        path + jsfolder + 'jquery.maskedinput.min.js',
        path + jsfolder + 'scripts.js'
    ])
    .pipe(changed(path + jsfolder + 'build/'))
    .pipe(debug())
    .pipe(babel({presets: [babel_es]}))
    .pipe(concat('production.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(path + jsfolder + 'build/'));
    
    cb();
});

/*
 * конкатенация одиночных js файлов и минификация КАЖДОГО
 */
gulp.task('concatsinglejs', (cb) => {
    gulp.src([path + jsfolder + 'single/*.js'])
    .pipe(changed(path + jsfolder + 'build/'))
    .pipe(babel({presets: ['../' + babel_es]}))
    .pipe(uglify())
    .pipe(gulp.dest(path + jsfolder + 'build/'));
    
    cb();
});
 
/**
 * пробую сборку с browserify
 */
gulp.task('browserify', () => {
    return browserify({
       entries: path + jsfolder + 'browserify/sites_module.js'
    })
   .bundle()
   .pipe(source('sites_module.js'))
   .pipe(buffer())
   .pipe(babel({presets: [babel_es]}))
   .pipe(uglify())
   .pipe(gulp.dest(path + jsfolder + 'build/'));
});

/*
 * компиляция sass, минификация и автопрефиксы
 */
gulp.task('sass', (cb) => {
    gulp.src(path + scssfolder + 'style.scss')
    .pipe(changed(path + cssfolder))
    .pipe(debug())
    .pipe(sass({ outputStyle : 'compressed' }).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(concat('sassstyles.min.css'))
    .pipe(gulp.dest(path + cssfolder));

    cb();
});

/**
 * css отдельные
 */
gulp.task('singlecss', (cb) => {
    gulp.src([
        path + cssfolder + 'single/*.css'
    ])
    .pipe(debug())
    .pipe(autoprefixer())
    .pipe(minifyCSS())
    .pipe(gulp.dest(path + cssfolder + 'build/'));//в отдельной папке хранить, а то задача постоянно включается
    
    cb();
});

/*
 * объединение css в 1 файл, минификация
 */
gulp.task('css', (cb) => {
    gulp.src([
        path + cssfolder + '*.css'
    ])
    .pipe(debug())
    .pipe(autoprefixer())
    .pipe(minifyCSS())
    .pipe(concat('production.min.css'))
    .pipe(gulp.dest(path + cssfolder + 'build/'));//в отдельной папке хранить, а то задача постоянно включается
    
    cb();
});

gulp.task('watch', () => {//следить за изменениями
    gulp.watch(path + cssfolder + '*.css', gulp.series('css'));
    gulp.watch(path + cssfolder + 'single/*.css', gulp.series('singlecss'));
    gulp.watch(path + scssfolder + '*.scss', gulp.series('sass'));
    gulp.watch(path + jsfolder + '*.js', gulp.series('concatjs'));
    gulp.watch(path + jsfolder + 'single/*.js', gulp.series('concatsinglejs'));
    gulp.watch(path + jsfolder + 'browserify/**/*.js', gulp.series('browserify'));
});

gulp.task('default', 
    gulp.series(
        gulp.parallel('sass'),
        gulp.parallel('concatjs', 'concatsinglejs'),
        gulp.parallel('css', 'singlecss'),
    )
);

/*сжатие картиночек*/
const imagemin = require('gulp-imagemin');

gulp.task('img', () =>
    gulp.src([path + 'web/uploads/**/*.{jpg,png,jpeg,gif}', '!' + path + 'web/uploads/thumbs/**'])
        .pipe(imagemin({
            verbose: true,
            progressive: true
        }))
        .pipe(gulp.dest(path + 'web/uploads/'))
);
/*end сжатие картиночек*/