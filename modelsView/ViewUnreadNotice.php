<?php

namespace app\modelsView;

use app\models\UnreadNotice;
use Yii;

/*
 * класс для управления данными на вывод(отдельно от api)
 */
class ViewUnreadNotice {    
    /*
     * для каждого типа можно мутить свое на основе полученных от api данных
     * @param {array} $notification - массив данных уведомления от api
     * @param {bool} $showIcon      - использовать ли fontawesome
     */    
    public function newHtmlNotify($notification, $showIcon = true) {
        /*
         * на вывод как угодно можно ебашить, API отдает все данные
         */
        $html = [];
        
        switch ($notification['notice_type']) {        
            case UnreadNotice::NEW_COMMENT://новый коммент     
                $icon = '<i class="fa fa-comment text-orange"></i>';
                
                if($notification['to']) {//к чему привязано
                    if(isset($notification['repeat']) && $notification['repeat'] > 1) { // указано кол-во множественных повторений события
                        $html[] = Yii::t('app/notifications', '{count_repeats} New Comments', [
                                'count_repeats' => (int)$notification['repeat']
                            ]);
                    } else { // обычное срабатывание
                        $html[] = Yii::t('app/notifications', 'New Comment {title}', ['title' => mb_substr(strip_tags($notification['to']['title']), 0, 20, 'UTF-8')]);
                    }
                } else {
                    $html[] = Yii::t('app/notifications', 'New Comment');
                }
                
                $to =  Yii::t('app/pretext', 'to '.$notification['notice_linked']['name']);
                $html[] = Yii::t('app/notifications', 'linked {name} {title}', ['name' => $to, 'title' => strip_tags($notification['notice_linked']['title'])]);
                
                // если множественное повторение - не надо дополнять от кого
                if($notification['from'] && !(isset($notification['repeat']) && $notification['repeat'] > 1)) {
                    $html[] = Yii::t('app/notifications', 'from {name}', ['name' => $notification['from']['name']]);
                }
                break;
            
            case UnreadNotice::NEW_CALLBOARD://новое объявление
                $icon = '<i class="fa fa-file-code-o text-orange"></i>';
                $html[] = Yii::t('app/notifications', 'New Callboard {title}', ['title' => $notification['notice_title']]);
                break;
            
            case UnreadNotice::NEW_TASK://новая задача
                $icon = '<i class="fa fa-tasks text-orange"></i>';
                $html[] = Yii::t('app/notifications', 'New Task {title}', ['title' => $notification['notice_title']]);
                break;
            
            case UnreadNotice::ADD_NEW_USERTO://добавление людей
                $icon = '<i class="fa fa-plus-circle"></i>';
                $html[] = Yii::t('app/notifications', 'You had been added');
                
                $to =  Yii::t('app/pretext', 'to '.$notification['notice_class']);
                $html[] = Yii::t('app/notifications', 'linked {name} {title}', ['name' => $to, 'title' => strip_tags($notification['notice_title'])]);
                break;
            
            case UnreadNotice::REPLY_COMMENT://ответ на коммент
                $icon = '<i class="fa fa-reply text-aqua"></i>';
                $html[] = Yii::t('app/notifications', 'New Reply Comment');
                
                $to =  Yii::t('app/pretext', 'to '.$notification['notice_linked']['name']);
                $html[] = Yii::t('app/notifications', 'linked {name} {title}', ['name' => $to, 'title' => strip_tags($notification['notice_linked']['title'])]);
                
                if($notification['from']) {
                    $html[] = Yii::t('app/notifications', 'from {name}', ['name' => $notification['from']['name']]);
                }
                break;
                
            case UnreadNotice::NEW_REGISTER_USER://новый зарегистрировавшийся сотрудник
                $icon = '<i class="fa fa-user-plus text-orange"></i>';
                $html[] = Yii::t('app/notifications', 'New Registered User');
                break;
            
            case UnreadNotice::NEW_MESSAGE://новое сообщение ИСПОЛЬЗУЕТСЯ?
                $icon = '<i class="fa fa-envelope-o text-orange"></i>';
                $html[] = Yii::t('app/notifications', 'New Message From');
                break;
            
            case UnreadNotice::NEW_CONGRAT://новое поздравление
                $icon = '<i class="fa fa-gift text-orange"></i>';
                $html[] = $notification['notice_title'];
                break;
            
            case UnreadNotice::NEW_LOAD_FILE://новый загруженный файл//надо бы сохранять и ссылку к файлу
                $icon = '<i class="fa fa-file text-orange"></i>';
                $html[] = Yii::t('app/notifications', 'New File added');
                
                $to =  Yii::t('app/pretext', 'to '.$notification['notice_linked']['name']);
                $html[] = Yii::t('app/notifications', 'linked {name} {title}', ['name' => $to, 'title' => strip_tags($notification['notice_linked']['title'])]);
                break;
            
            case UnreadNotice::CHANGE_STATUS://смена статуса
                $icon = '<i class="fa fa-compass text-aqua"></i>';
                $html[] = Yii::t('app/notifications', 'Changed Status');
                
                $to =  Yii::t('app/pretext', 'by '.$notification['notice_linked']['name']);
                $html[] = Yii::t('app/notifications', 'linked {name} {title}', ['name' => $to, 'title' => strip_tags($notification['notice_linked']['title'])]);
                break;
            
            case UnreadNotice::NEW_EVENT://новое событие
                $icon = '<i class="fa fa-calendar text-orange"></i>';
                $html[] = Yii::t('app/notifications', 'New Event {title}', ['title' => strip_tags($notification['notice_linked']['title'])]);
                break;
            
            case UnreadNotice::NOTIFY_EVENT://оповещение о событии
                $icon = '<i class="fa fa-exclamation text-aqua"></i>';
                $html[] = Yii::t('app/notifications', 'New Notify About Event {title}', ['title' => strip_tags($notification['notice_linked']['title'])]);
                break;
            
            case UnreadNotice::NEW_BUGTRACKER://создание сообщения в баг трекер
                $icon = '<i class="fa fa-bug"></i>';
                $html[] = Yii::t('app/notifications', 'New message bug tracker');
                
                if($notification['from']) {
                    $html[] = Yii::t('app/notifications', 'from {name}', ['name' => $notification['from']['name']]);
                }
                break;
            
            case UnreadNotice::NEW_SYSADMINQUERY:
                $icon = '<i class="fa fa-wrench"></i>';
                $html[] = Yii::t('app/notifications', 'New query system admin');
                
                if($notification['from']) {
                    $html[] = Yii::t('app/notifications', 'from {name}', ['name' => $notification['from']['name']]);
                }
                break;
            
            case UnreadNotice::NEW_STEWARDQUERY:
                $icon = '<i class="fa fa-flask"></i>';
                $html[] = Yii::t('app/notifications', 'New query steward');
                
                if($notification['from']) {
                    $html[] = Yii::t('app/notifications', 'from {name}', ['name' => $notification['from']['name']]);
                }
                break;
            
            case UnreadNotice::CRM_DONE_YOUR_TASK:
                $icon = '<i class="fa fa-tasks text-blue"></i>';
                $html[] = Yii::t('app/notifications', 'New done assignment task {title}', ['title' => strip_tags($notification['notice_title'])]);
                break;
            
            case UnreadNotice::API_MAIL_SENDING_DONE:
                $icon = '<i class="fa fa-envelope-o"></i>';
                $html[] = Yii::t('app/notifications', 'API_MAIL_SENDING_DONE');
                break;
            
            default :
                $html[] = 'нет уведомления для ' . $notification['notice_type'];
        }
        
        if($showIcon === true) {
            array_unshift($html, $icon);
        }
        
        return \yii\helpers\Html::a(
                implode(' ', $html),
                \yii\helpers\Url::to(['/notify/middle-notice', 'id' => $notification['notice_id']], true),
                ['title' => strip_tags(implode(' ', $html))]
            );
    }
}
