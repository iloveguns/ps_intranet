<?php

$params_init = require(__DIR__ . '/params.php');

$add_params = require(__DIR__ . '/add_params.php');
$params = array_merge($params_init, $add_params);

$modules = require(__DIR__ . '/modules.php');//настройки модулей

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'name'=>'СМГ Интранет',
    'bootstrap' => ['log', 'app\base\settings', 'assetsAutoCompress'],
    'language' => 'ru-RU',
    'defaultRoute' => 'intranet',
    'modules' => $modules,
    'components' => [
        'assetsAutoCompress' => [//ужимание, обфускация, минификация
            'class' => 'app\components\CodeMinifier',
            'enabled' => true,//включено
            'useInAjaxPjax' => true,//использовать в ajax/pjax
        ],
        'thumbnail' => [
            'class' => 'himiklab\thumbnail\EasyThumbnail',
            'cacheAlias' => 'assets/gallery_thumbnails',
        ],
        'assetManager' => [
            'linkAssets' => true,
            'class' => 'yii\web\AssetManager',
            'appendTimestamp' => true,
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'jquery.js' : 'jquery.min.js'
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [
                        YII_ENV_DEV ? 'css/bootstrap.css' : 'css/bootstrap.min.css',
                    ]
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'js/bootstrap.js' : 'js/bootstrap.min.js',
                    ]
                ]
            ],
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'timeZone' => 'Etc/GMT-0',//КАКАЯ ТО ЕБНАЯ если новосиб ставить
            //'dateFormat' => 'dd.MM.yyyy',//стандартно 01 марта 2016 г.
            'datetimeFormat' => 'php:d F Y H:i',
            'dateFormat' => 'short',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => 'RUR',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => [
                'dialog' => 'intranet/default/dialog',
                'dialog/<url:[^/]?>' => 'intranet/default/dialog',
                '<module>/<controller>/<action>/<id:\d+>' => '<module>/<controller>/<action>',//?id=6 - /6
            ]
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => '@app/messages',
                    //'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app'       => 'app.php',
                        'app/pretext' => 'pretext.php',
                        'app/models' => 'models.php',
                        'app/views' => 'views.php',
                        'app/mails' => 'mails.php',
                        'app/errors' => 'errors.php',
                        'app/analytics' => 'analytics.php',
                        'app/nulluser' => 'nulluser.php',
                        'app/notifications' => 'notifications.php',
                    ],
                ],
            ],
        ],
        'request' => [
            'cookieValidationKey' => '9IGDwLG9G-DpWSFtBkRNwWm_-P_1rG3R',
        ],
        'cache' => require(__DIR__ . '/cache.php'),
        'user' => [
            'identityClass' => 'app\modules\user\models\User',
            'enableAutoLogin' => true,
            'authTimeout' => 60*24*30,//авторизация на месяц
        ],
        'errorHandler' => [
            'errorAction' => 'intranet/default/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.timeweb.ru',
                'username' => 'intranet@altkoms.ru',
                'password' => '1234567890',
                'port' => '25',
                'encryption' => 'TLS',
            ],
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\DbTarget',//лог в базу yii\log\Target закомментировал добавление application в базу
                    'levels' => ['error', 'warning'],
                    /*'categories' => [
                        'yii\db\*',
                        'yii\web\HttpException:*',
                    ],*/
                    'except' => [
                        'yii\debug\Module::checkAccess',
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                    ],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*']
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*'],
        'generators' => [//custom templates for gii
            'crud' => [
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => [
                    'default' => '@app/components/gii/generators/crud/default',
                ]
            ],
            'model' => [
                'class' => 'yii\gii\generators\model\Generator',
                'templates' => [
                    'default' => '@app/components/gii/generators/model/default',
                ]
            ]
        ],
    ];
}

return $config;
