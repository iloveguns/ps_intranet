<?php
//app\base\settings - загрузка параметров из бд
return [
    'adminEmail' => 'slayermass@mail.ru',
    'saveModelDateTime' => 'Y-m-d H:i:s',//для сохранения моделей
    'timeZone' => "Asia/Krasnoyarsk",
    'formatterTimeZone' => 'Etc/GMT-7',
    //'default_Pagination_PageSize' => 10,
    //'enableComments' => true,//комментарии включены(в видах). можно отключить например
    'photoAtAdminUploadParams' => 'width:200px',//стили для просмотра фоток после загрузки в админке(новости, игры и тд)
    //'cacheTime' => 3600,//время сохранения в кеш в общем и в app\helpers\Data частно
    //'minDurationTask' => 60*60*24,//задачи не менее чем на 1 день(в секундах)
    'themeFolder' => '@app/views/innerlayouts',//путь до темы
    'messagesLoadCount' => 20,//загружать по n сообщений в диалог
    'useAjaxLoadComments' => TRUE,//использовать ли постепенную загрузку ajax или сразу все выводить
    'commentsLoadCount' => 15,//по сколько комментов выводить начально, 15
    'mainurl' => 'http://109.195.33.204/',
    'pageSizeList' => [10, 25, 50, 100, 1000],//в задачах странно ведет себя
    'uploadFilesFolder' => Yii::$app->urlManager->baseUrl . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'files',//папка для загрузки файлов
    'uploadsThumbsFolder' => Yii::$app->urlManager->baseUrl . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'thumbs',//папка для загрузки миниатюр картинок
];

