<?php
use \kartik\datecontrol\Module;
return [
    'datecontrol' =>  [
        'class' => 'kartik\datecontrol\Module',

        'displaySettings' => [
            Module::FORMAT_DATE => 'php:d-F-Y',
            Module::FORMAT_TIME => 'hh:mm:ss a',
            Module::FORMAT_DATETIME => 'php:d-F-Y H:i', 
        ],
        'saveSettings' => [
            Module::FORMAT_DATE => 'php:Y-m-d',//используется
            Module::FORMAT_TIME => 'php:H:i:s',
            Module::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
        ],
        'autoWidget' => true,
        'autoWidgetSettings' => [
            Module::FORMAT_DATE => ['type'=>2, 'pluginOptions'=>['autoclose'=>true, 'todayHighlight' => true]],
            Module::FORMAT_DATETIME => ['type'=>1, 'pluginOptions'=>['autoclose'=>true,'weekStart' => 1]],
            Module::FORMAT_TIME => [],
        ],
        'widgetSettings' => [
            Module::FORMAT_DATE => [
                'class' => 'yii\jui\DatePicker',
                'options' => [
                    'dateFormat' => 'php:d-M-Y',
                    'options' => ['class'=>'form-control'],
                ]
            ]
        ]
    ],
    'redactor' => [
        'class' => 'yii\redactor\RedactorModule',
        'uploadDir' => '@webroot/uploads/redactor',
        'uploadUrl' => '@web/uploads/redactor',
        'widgetClientOptions' => [
            'pastePlainText'  => true,//очистка от тегов
            'deniedTags' => ['h1', 'h2', 'br'],//игнорировать теги
            'formatting' =>  ['p', 'blockquote', 'pre', 'h3', 'h4', 'h5'],//убрал h1, h2
            'lang' => 'ru',
            'plugins' => ['fontcolor','imagemanager', 'fullscreen', 'filemanager', 'table', 'video'],
            'fileAllowExtensions' => ['zip','rar'],
            'imageAllowExtensions'=>['jpg', 'jpeg', 'png', 'gif']
        ],
    ],
    'polling' => [//голосования
        'layout' =>  '@app/views/innerlayouts/main',
        'class' => 'app\modules\polling\PollingModule',
        'as access' => [
            'class' => '\yii\filters\AccessControl',
            'rules' => [
                [
                    'controllers'=>['polling/polling', 'polling/polling-stages'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
    ],
    'sites' => [//империя сайтов
        'layout' =>  '@app/views/siteslayouts/main',
        'class' => 'app\modules\sites\SitesModule',
        'as access' => [
            'class' => '\yii\filters\AccessControl',
            'rules' => [
                [
                    'controllers'=>['sites/sites'],
                    'allow' => true,
                    'allow' => true,
                    'matchCallback' => function() {
                        return \app\modules\sites\SitesModule::canAccess();
                    }
                ],
                [
                    'controllers'=>['sites/sites-roles'],
                    'actions'=>['create', 'update', 'delete', 'index'],
                    'allow' => true,
                    'matchCallback' => function() {
                        return Yii::$app->user->identity->isAdmin();
                    }
                ],
                [
                    'controllers'=>['sites/sites-access-settings'],
                    'allow' => true,
                    'matchCallback' => function() {
                        return \app\modules\sites\SitesModule::canAccess();
                    }
                ],
            ],
        ],
    ],
    'admins' => [//модуль с различной служебной информацией
        'layout' =>  '@app/views/innerlayouts/main',
        'class' => 'app\modules\admins\AdminsModule',
        'as access' => [
            'class' => '\yii\filters\AccessControl',
            'rules' => [
                [
                    'controllers'=>['admins/login-form', 'admins/default', 'admins/system', 'admins/settings'],
                    'allow' => true,
                    'matchCallback' => function() {
                        return Yii::$app->user->identity->isAdmin();//только админ(запрещать всем кто не админ)
                    }
                ],
                [//праздники
                    'controllers'=>['admins/holidays'],
                    'actions'=>['index', 'jsoncalendar'],
                    'allow' => true,
                    'roles' => ['@']
                ],
                [//праздники crud
                    'controllers'=>['admins/holidays'],
                    'actions'=>['create', 'update', 'delete', 'view'],
                    'allow' => true,
                    'matchCallback' => function() {
                        return Yii::$app->user->identity->can('crud_holidays');
                    }
                ],
            ]
        ]
    ],
    'crm' => [//crm (отдельно)
        'layout' =>  '@app/views/crmlayouts/main',
        'class' => 'app\modules\crm\CrmModule',
        'as access' => [
            'class' => '\yii\filters\AccessControl',
            'rules' => [
                [
                    'controllers'=>['crm/crm-faq'],
                    'actions'=>['create', 'update', 'delete'],
                    'allow' => true,
                    'matchCallback' => function() {
                        return Yii::$app->user->identity->isAdmin();
                    }
                ],
                [
                    'controllers'=>['crm/crm-access-settings'],
                    'allow' => true,
                    'matchCallback' => function() {
                        return app\modules\crm\CrmModule::canAdminDep();
                    }
                ],
                [
                    'controllers'=>['crm/crm-roles'],
                    'actions'=>['create', 'update', 'delete', 'index'],
                    'allow' => true,
                    'matchCallback' => function() {
                        return Yii::$app->user->identity->isAdmin();
                    }
                ],
                [
                    'controllers'=>['crm/crm-faq'],
                    'actions'=>['index', 'view'],
                    'allow' => true,
                    'matchCallback' => function() {
                        return app\modules\crm\CrmModule::haveAccess();
                    }
                ],
                [
                    'controllers'=>['crm/crm-fired-clients'],
                    'allow' => true,
                    'matchCallback' => function() {
                        return app\modules\crm\CrmModule::canAdminDep();
                    }
                ],
                [
                    'controllers'=>['crm/crm-company', 'crm/crm-contact', 'crm/default', 'crm/crm-typetask', 'crm/crm-typedeals', 'crm/crm-tasks', 'crm/crm-deals', 'crm/crm-bugtracker', 'crm/crm-analytics', 'crm/crm-tasks-results', 'crm/crm-types-deal', 'crm/crm-deals-projects', 'crm/mail'],
                    'allow' => true,
                    'matchCallback' => function() {
                        return app\modules\crm\CrmModule::haveAccess();
                    }
                ],
            ]
        ]
    ],
    'intranet' => [//интранет и все с ним связанное
        'layout' =>  '@app/views/innerlayouts/main',//adminLTE
        'class' => 'app\modules\intranet\IntranetModule',
        'as access' => [
            'class' => '\yii\filters\AccessControl',
            'rules' => [
                [//задачи проекты
                    'controllers'=>['intranet/default', 'intranet/projects', 'intranet/tasks', 'intranet/messages', 'intranet/project-directions'],
                    'actions'=>['index','view','create','update', 'executer-complete', 'change-status', 'error', 'find-users-ajax', 'delete-dialog-ajax', 'delete-all-notices', 'delete', 'remove-user-fromtask', 'search', 'test', 'dialog', 'get-sort-data-tasks', 'getbydirection', 'api-list-data', 'analytics'],
                    'allow' => true,
                    'roles' => ['@']
                ],
                [//мероприятия компании
                    'controllers'=>['intranet/events-company', 'intranet/conference-hall'],
                    'actions'=>['index', 'get-event-by-id', 'create', 'update', 'delete'],
                    'allow' => true,
                    'roles' => ['@']
                ],
                [//расписание переговорной
                    'controllers'=>['intranet/analcenter-hall'],
                    'actions'=>['index','view','create','update', 'delete', 'jsoncalendar', 'get-event-by-id'],
                    'allow' => true,
                    'roles' => ['@']
                ],
                [//база клиентов
                    'controllers'=>['intranet/dbpartners', 'intranet/dbpartners-type-client', 'intranet/dbpartners-congratulations'],
                    'actions'=>['index','view','create','update', 'delete', 'clients'],
                    'allow' => true,
                    'roles' => ['@']
                ],
                [//расписание радио
                    'controllers'=>['intranet/radio-schedule'],
                    'actions'=>['index', 'update', 'delete', 'setsort', 'infoevent', 'report'],
                    'allow' => true,
                    'roles' => ['@']
                ],
                [//расписание водителей
                    'controllers'=>['intranet/driver-schedule'],
                    'actions'=>['index','view','create','update', 'delete', 'info', 'inactive'],
                    'allow' => true,
                    'roles' => ['@']
                ],
                [//блоги
                    'controllers'=>['intranet/blogs'],
                    'actions'=>['index','view','create','update', 'delete', 'clients', 'my'],
                    'allow' => true,
                    'roles' => ['@']
                ],
                [//письма
                    'controllers'=>['intranet/api-sendmail-templates'],
                    'actions'=>['index','view','create','update', 'delete', 'sended', 'see-sended'],
                    'allow' => true,
                    'roles' => ['@']
                ],
                [//письма сотрудники
                    'controllers'=>['intranet/api-sendmail-user-mails'],
                    'actions'=>['index','view','create','update', 'delete'],
                    'allow' => true,
                    'roles' => ['@']
                ],
                [//smtp настройки
                    'controllers'=>['intranet/api-sendmail-smtp-settings'],
                    'actions'=>['index','view','create','update', 'delete'],
                    'allow' => true,
                    'matchCallback' => function() {
                        return Yii::$app->user->identity->isAdmin();
                    }
                ],
                [
                    'controllers'=>['intranet/graphrelate'],
                    'actions'=>['index', 'save', 'view'],
                    'allow' => true,
                    'roles' => ['@']
                ],
                [//объявления
                    'controllers'=>['intranet/callboard', 'intranet/reported', 'intranet/bugtracker', 'intranet/sysadmquery', 'intranet/stewardquery'],
                    'actions'=>['index','view','create','update', 'delete'],
                    'allow' => true,
                    'roles' => ['@']
                ],
                [//файловое хранилишэ
                    'controllers'=>['intranet/file-storage'],
                    'actions'=>['index'],
                    'allow' => true,
                    'roles' => ['@']
                ],
                [//события
                    'controllers'=>['intranet/events', 'intranet/head-calendar'],
                    'actions'=>['index','view','create','update', 'delete', 'createget'],
                    'allow' => true,
                    'roles' => ['@']
                ],
                [
                    'controllers'=>['intranet/projects', 'intranet/tasks', 'intranet/callboard', 'intranet/events'],
                    'actions'=>['all-projects', 'all-tasks', 'delete', 'all-events'],
                    'allow' => true,
                    'matchCallback' => function() {
                        return Yii::$app->user->identity->isAdmin();//только админ(запрещать всем кто не админ)
                    }
                ],
                [
                    'controllers'=>['intranet/technique'],
                    'actions'=>['index','view','create','update', 'delete'],
                    'allow' => true,
                    'matchCallback' => function() {
                        return Yii::$app->user->identity->isAdmin();//только админ(запрещать всем кто не админ)
                    }
                ],
            ]
        ]
    ],
    'user' => [//все о пользователях находится там
        'layout' =>  '@app/views/innerlayouts/main',//adminLTE
        'class' => 'app\modules\user\UserModule',
        'as access' => [
            'class' => '\yii\filters\AccessControl',
            'rules' => [
                [
                    'controllers'=>['user/user'],
                    'actions'=>['dependency-divizion','dependency-department'],
                    'allow' => true,
                    'roles' => ['?', '@']
                ],
                [
                    'controllers'=>['user/unread-notice'],
                    'actions'=>['index','delete'],
                    'allow' => true,
                    'roles' => ['@']
                ],
                [//персональные заметки
                    'controllers'=>['user/personal-notes'],
                    'actions'=>['index','delete', 'view', 'create', 'update'],
                    'allow' => true,
                    'roles' => ['@']
                ],
                [
                    'controllers'=>['user/user','user/units'],
                    'actions'=>['profile','view', 'structure-widget','update-profile', 'uploadPhoto', 'update' ,'index', 'last-online', 'plan', 'getoffice', 'map', 'show-viewers-entity'],
                    'allow' => true,
                    'roles' => ['@']
                ],
                [
                    'controllers'=>['user/appointments', 'user/general-roles', 'user/user', 'user/units'],
                    'actions'=>['create', 'update','delete', 'saveunits', 'view-ajax', 'index'],
                    'allow' => true,
                    'matchCallback' => function() {
                        return Yii::$app->user->identity->can('tousers');
                    }
                ],
                [
                    'controllers'=>['user/user'],//публичный профиль сотрудника
                    'actions'=>['update', 'registers-workers', 'approve-register', 'deny-register'],
                    'allow' => true,
                    'matchCallback' => function() {
                        return Yii::$app->user->identity->isAdmin();//только админ
                    }
                ],
                [
                    'controllers'=>['user/units', 'user/general-roles'],
                    'actions'=>['index', 'delete-unit'],
                    'allow' => true,
                    'matchCallback' => function() {
                        return Yii::$app->user->identity->isAdmin();//только админ
                    }
                ],
                /*[
                    'controllers'=>['user/general-roles', 'user/user','user/units','user/appointments','user/general-roles'],
                    'allow' => true,
                    'matchCallback' => function() {
                        return Yii::$app->user->identity->isAdmin();//только админ
                    }
                ],*/
            ]
        ]
    ],
];