<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');

$params_init = require(__DIR__ . '/params.php');
$add_params = require(__DIR__ . '/add_params.php');
$params = array_merge($params_init, $add_params);

$db = require(__DIR__ . '/db.php');
$dbpg = require(__DIR__ . '/dbpg.php');

return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'language' => 'ru-RU',
    'controllerNamespace' => 'app\commands',
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'timeZone' => 'Etc/GMT-0',//КАКАЯ ТО ЕБНАЯ если новосиб ставить
            //'dateFormat' => 'dd.MM.yyyy',//стандартно 01 марта 2016 г.
            //'datetimeFormat' => 'php:Y-m-d H:i:s',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => 'RUR',
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => '@app/messages',
                    //'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app'       => 'app.php',
                        'app/pretext' => 'pretext.php',
                        'app/models' => 'models.php',
                        'app/mails' => 'mails.php',
                        'app/errors' => 'errors.php',
                        'app/nulluser' => 'nulluser.php',
                    ],
                ],
            ],
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'baseUrl' => '/',
            'hostInfo' => 'http://109.195.33.204/',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.timeweb.ru',
                'username' => 'intronet@altkoms.ru',
                'password' => '1234567890',
                'port' => '25',
                'encryption' => 'TLS',
            ],
            'useFileTransport' => false,
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'dbpg' => $dbpg,
    ],
    'params' => $params,
];
